﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using OModules.Notifications;

namespace OModules.Hubs
{
    public class ModuleComHub : Hub
    {
        private static List<ChannelEndpoint> receivers = new List<ChannelEndpoint>();
        private static List<ChannelEndpoint> senders = new List<ChannelEndpoint>();

        private static object endpointLocker = new object();

        private static IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<ModuleComHub>();

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            if (stopCalled)
            {
                Console.WriteLine(String.Format("Client {0} explicitly closed the connection.", Context.ConnectionId));
            }
            else
            {
                Console.WriteLine(String.Format("Client {0} timed out .", Context.ConnectionId));
            }


            UnregisterEndpoint(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnConnected()
        {
            return base.OnConnected();
        }

        public void UnregisterEndpoint(string connectionId)
        {
            lock (endpointLocker)
            {
                
                var endpoint = receivers.FirstOrDefault(rec => rec.ConnectionId == connectionId);
                if (endpoint != null)
                {
                    receivers.Remove(endpoint);
                }
                
            }
        }

        public void RegisterEndpoint(ChannelEndpoint endpoint)
        {
            endpoint.ConnectionId = Context.ConnectionId;
            endpoint.Client = Clients.Client(Context.ConnectionId);

            lock (endpointLocker)
            {
                if (endpoint.EndpointType == EndpointType.Receiver)
                {
                    var receiver = receivers.FirstOrDefault(rec => rec.EndPointId == endpoint.EndPointId && rec.EndpointType == EndpointType.Receiver && rec.ChannelTypeId == endpoint.ChannelTypeId);
                    if (receiver != null)
                    {
                        receivers.Remove(receiver);
                    }
                    
                    receivers.Add(endpoint);
                    
                }
                else
                {
                    var sender = senders.FirstOrDefault(send => send.EndPointId == endpoint.EndPointId && send.EndpointType == EndpointType.Sender && send.ChannelTypeId == endpoint.ChannelTypeId);
                    if (sender != null)
                    {
                        senders.Remove(sender);
                    }

                    senders.Add(endpoint);
                }

            }
        }

        public void RegisterHierarchy(string idEndpoint, string idParent)
        {
            receivers.Where(rec => rec.EndPointId == idEndpoint).ToList().ForEach(rec =>
            {
                rec.IdParent = idParent;
            });
        }

        

        public void SendInterServiceMessage(InterServiceMessage message)
        {
            if (message.IsInactive) return;
            var endpoints = receivers.Where(rec => rec.ChannelTypeId == message.ChannelId && (message.UserId == null || rec.UserId == message.UserId)).ToList();

            if (!string.IsNullOrEmpty(message.ReceiverId))
            {
                endpoints = endpoints.Where(rec => rec.EndPointId == message.ReceiverId).ToList();
            }

            endpoints = endpoints.Where(endpoint => string.IsNullOrEmpty(endpoint.IdParent) || endpoint.IdParent == message.SenderId).ToList();

            endpoints.ForEach(endPoint => endPoint.Client.addNewMessageToPage("test", message));
        }

        public static void SendInterServiceMessageFromController(InterServiceMessage message)
        {
            if (message.IsInactive) return;
            var endpoints = receivers.Where(rec => rec.ChannelTypeId == message.ChannelId && 
                (!string.IsNullOrEmpty(message.UserId) ?  rec.UserId == message.UserId : 1 == 1) &&
                (!string.IsNullOrEmpty(message.ReceiverId) ? rec.EndPointId == message.ReceiverId : 1 == 1)).ToList();

            endpoints.ForEach(endPoint => endPoint.Client.addNewMessageToPage("test", message));
        }
    }
}