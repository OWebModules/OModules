﻿using OModules.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace OModules.Services
{
    public class OModuleSync
    {
        private OntologyAppDBConnector.Globals globals;
        public async Task<ResultItem<SyncOModulesResult>> SyncOModules(OntologyAppDBConnector.IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run<ResultItem<SyncOModulesResult>>(async() =>
            {
                var result = new ResultItem<SyncOModulesResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new SyncOModulesResult()
                };

                messageOutput?.OutputInfo("Get Controllers...");
                var controllers = (from typeItem in Assembly.GetExecutingAssembly().GetTypes()
                                   where typeItem.Namespace == "OModules.Controllers"
                                        && typeItem.BaseType != null && typeItem.BaseType.Name == "AppControllerBase"
                                   select new ViewController
                                   {
                                       ControllerType = typeItem,
                                       Name = typeItem.Name
                                   }).ToList();

                messageOutput?.OutputInfo($"Have {controllers.Count} Controllers.");

                var methodCount = 0;
                messageOutput?.OutputInfo($"Get Methods.");
                foreach (var controller in controllers)
                {
                    var methods = controller.ControllerType.GetMethods().Where(method => method.IsPublic && !method.IsSpecialName).Select(method => new ControllerMethod
                    {
                        Name = method.Name,
                        Parameters = method.GetParameters().Select(paramItm => new ControllerParameter
                        {
                            Name = paramItm.Name,
                            TypeName = paramItm.ParameterType.ToString()
                        }).ToList()
                    });

                    methodCount += methods.Count();
                    controller.Methods.AddRange(methods);

                }

                messageOutput?.OutputInfo($"Have {methodCount} Methods.");

                var syncController = new OntoMsg_Module.OModuleSyncConnector(globals);

                var syncResult = await syncController.SyncViewControllers(controllers, messageOutput);

                result.ResultState = syncResult.ResultState;

                if (result.ResultState.GUID == globals.LState_Success.GUID)
                {
                    result.Result.CountControllers = syncResult.Result.ViewControllers.Count;
                    result.Result.CountActions = syncResult.Result.ViewControllers.SelectMany(viewController => viewController.Methods).Count();
                    result.Result.CountParameters = syncResult.Result.ViewControllers.SelectMany(viewController => viewController.Methods).SelectMany(method => method.Parameters).Count();

                }
                else
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                }

                return result;
            });

            return taskResult;
        }

        public OModuleSync(OntologyAppDBConnector.Globals globals)
        {
            this.globals = globals;
        }
    }
}