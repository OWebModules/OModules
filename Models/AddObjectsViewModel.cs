﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OModules.Controllers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OModules.Models
{
    public class AddObjectsViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }


        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelClass { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelClassName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inpClass { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem showClassChoose { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem saveObjects { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem removeRow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem applySaved { get; private set; }

        public List<ViewItem> AddObjectRows { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionCheckNames { get; set; }
        public string ActionSaveObjects { get; set; }

        public string ActionGetClassDropDownConfig { get; set; }

        public string ActionChangeValue { get; set; }

        public string ActionAddAddObjectsRow { get; set; }
        public string ActionRemoveEmptyObjectsRow { get; set; }

        public string ActionPasteText { get; set; }

        public string ChannelControllerMessage { get; private set; }
        public string ChannelSelectedClass { get; private set; }

        public string ChannelAddedObjects { get; private set; }

        public string ChannelAppliedObjects { get; private set; }

        public clsOntologyItem ClassItem;

        public string AddObjectsPrefix { get; private set; }

        private Regex RegexNumber = new Regex(@"\d+");

        private string templateObjectInput = @"<div>
                    <div class='add-objects-form'>
                      <input class='{0} {1}' 
                            onKeyDown='javascript:addObjectRowsContainer.handler.keyPress(event, this)' 
                            onChange='javascript:addObjectRowsContainer.handler.checkName(this);'
                            onPaste='javascript:addObjectRowsContainer.handler.paste(this);' />
                      </div>
                    </div>";


        public AddObjectsViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_AddObjects.GUID, idInstance, userId)
        {
            AddObjectsPrefix = "addObjectRow";
            labelClass.ChangeViewItemValue(ViewItemType.Content.ToString(), "Class:");
            ChannelControllerMessage = SpecialChannels.ControllerMessage;
            ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID;
            ChannelAddedObjects = Channels.LocalData.Object_AddedObjects.GUID;
            ChannelAppliedObjects = Channels.LocalData.Object_AppliedObjects.GUID;

            ResetRows();
        }

        public void ResetRows(string initName = null)
        {
            AddObjectRows = new List<ViewItem>();
            AddRow(initName);
        }

        public ViewItem AddRow(string text = null, int rowIx = -1)
        {
            var lastViewItem = AddObjectRows.LastOrDefault();
            if (text == null)
            {
                text = "";
            }

            ViewItem row = null;
            if (lastViewItem == null)
            {
                row = new ViewItem
                {
                    ViewItemId = $"{AddObjectsPrefix}0",
                    ViewItemClass = ViewItemClass.Input.ToString()
                };
                row.ChangeViewItemValue(ViewItemType.Template.ToString(), System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(string.Format(templateObjectInput, AddObjectsPrefix, row.ViewItemId))));
                row.ChangeViewItemValue(ViewItemType.PlaceHolder.ToString(), "");
                row.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                AddObjectRows.Add(row);
            }
            else
            {
                
                var placeHolder = lastViewItem.GetViewItemValueItem(ViewItemType.PlaceHolder.ToString());
                var createNew = (rowIx == -1) || (rowIx > AddObjectRows.Count - 1);
                
                if (createNew)
                {
                    var id = lastViewItem.ViewItemId;
                    var orderNo = int.Parse(RegexNumber.Match(id).Value);
                    var newOrderNo = orderNo + 1;
                    var newId = id.Replace(orderNo.ToString(), newOrderNo.ToString());

                    row = new ViewItem
                    {
                        ViewItemId = newId,
                        ViewItemClass = ViewItemClass.Input.ToString()
                    };
                    row.ChangeViewItemValue(ViewItemType.Template.ToString(), System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(string.Format(templateObjectInput, AddObjectsPrefix, row.ViewItemId))));
                    row.ActionChangeValue = ActionChangeValue;
                    AddObjectRows.Add(row);
                }
                else
                {
                    row = AddObjectRows[rowIx];
                    row.ChangeViewItemValue(ViewItemType.Content.ToString(), text);
                }
                row.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                row.ChangeViewItemValue(ViewItemType.PlaceHolder.ToString(), placeHolder.Value);

            }

            row.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem
            {
                Key = "background-color",
                Value = ""
            });
            row.ChangeViewItemValue(ViewItemType.Content.ToString(), text);

            row.ActionChangeValue = ActionChangeValue;
            
            return row;

        }

        public bool EnableSave()
        {
            var group = AddObjectRows.GroupBy(row => row.GetViewItemValueItem(ViewItemType.Content.ToString())?.Value.ToString().ToLower() ?? "").ToList();
            return group.Where(grp => !string.IsNullOrEmpty(grp.Key)).Any();
        }

        public bool EnableRemove()
        {
            var group = AddObjectRows.GroupBy(row => row.GetViewItemValueItem(ViewItemType.Content.ToString())?.Value.ToString().ToLower() ?? "").ToList();
            return group.Where(grp => string.IsNullOrEmpty(grp.Key)).Any();
        }

        public async Task<clsOntologyItem> CheckAddObjectRows(OntologyItemsModule.EditOItemController controller)
        {
            var result = controller.Globals.LState_Success.Clone();
            var group = AddObjectRows.GroupBy(row => row.GetViewItemValueItem(ViewItemType.Content.ToString())?.Value.ToString().ToLower() ?? "").ToList();
            var objectsResult = await controller.GetObjects(group.Select(grp => grp.Key).ToList(), ClassItem.GUID);
            result = objectsResult.ResultState;

            foreach (var row in AddObjectRows)
            {
                var content = row.GetViewItemValueItem(ViewItemType.Content.ToString())?.Value.ToString().ToLower() ?? "";
                if (string.IsNullOrEmpty(content))
                {
                    row.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "background-color", Value = "" });
                    continue;
                }

                var objects = objectsResult.Result.Where(obj => obj.Name.ToLower() == content).ToList();
                row.ChangeViewItemValue(ViewItemType.CSS.ToString(),
                    !objects.Any()
                        ? new CSSItem() { Key = "background-color", Value = "lightgreen" }
                        : new CSSItem() { Key = "background-color", Value = "yellow" });
            }
            
            var multiple = group.Where(grp => !string.IsNullOrEmpty(grp.Key.ToString()) && grp.Count() > 1).Select(grp => grp.Key.ToString());

            foreach (var multipleItem in multiple)
            {
                AddObjectRows.Where(row => row.GetViewItemValueItem(ViewItemType.Content.ToString())?.Value.ToString().ToLower() == multipleItem).ToList().ForEach(row => row.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem
                {
                    Key = "background-color",
                    Value = "yellow"
                }));
            }

            return result;
        }

        public ResultItem<List<clsOntologyItem>> GetObjectsToSave(OntologyAppDBConnector.Globals globals)
        {
            var result = new ResultItem<List<clsOntologyItem>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsOntologyItem>()
            };

            if (ClassItem == null)
            {
                result.ResultState = globals.LState_Error.Clone();
                result.ResultState.Additional1 = "No Class provided!";
                return result;
            }

            result.Result = AddObjectRows.Where(row => !string.IsNullOrEmpty(row.GetViewItemValueItem(ViewItemType.Content.ToString())?.Value.ToString() ?? "")).Select(row => new clsOntologyItem
            {
                GUID = globals.NewGUID,
                Name = (row.GetViewItemValueItem(ViewItemType.Content.ToString())?.Value.ToString() ?? ""),
                GUID_Parent = ClassItem.GUID,
                Type = globals.Type_Object
            }).ToList();

            return result;
        }

        public void RemoveEmptyRows()
        {
            var removeRows = new List<ViewItem>();
            for (int i = 1; i < AddObjectRows.Count; i++)
            {
                var row = AddObjectRows[i];
                if (string.IsNullOrEmpty( row.GetViewItemValueItem(ViewItemType.Content.ToString())?.Value.ToString()))
                {
                    removeRows.Add(row);
                }
            }

            AddObjectRows.RemoveAll(row => removeRows.Contains(row));
        }
    }

    

    public class ResultValidateObjects
    {
        public bool IsSuccessful { get; set; }
        public List<ViewItem> ViewItems { get; set; }
        public List<ViewItem> AddObjectRows { get; set; }

        public List<clsOntologyItem> SavedObjects { get; set; }

        public ResultViewItemsType ResultType { get; set; }
        public string ResultMessage { get; set; }
    }
}