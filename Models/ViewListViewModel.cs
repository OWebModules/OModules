﻿using ModuleManagementModule.Services;
using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ViewListViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem rootContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.TreeList)]
        public ViewItem treelist { get; private set; }

        public string fullHeight { get; private set; }

        public object setDataItems { get; set; }
        public object messageFromWebsocket { get; set; }
        public object gridItem { get; set; }
        public object gridChangeHandler { get; set; }
        public object clickedButton { get; set; }

        public List<clsObjectRel> ControllerViews { get; set; }

        public ActionConfigViewList ActionConfig { get; set; }

        public ViewListViewModel(HttpSessionStateBase session, string idInstance, string userId ) : base(session, WebViews.Config.LocalData.Object_ViewList.GUID, idInstance, userId)
        {
            fullHeight = nameof(fullHeight);

        }

    }

    public class ActionConfigViewList
    {
        public string ActionGetGridConfig { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetUrl { get; set; }
    }
}