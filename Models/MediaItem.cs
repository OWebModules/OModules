﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class MediaItem
    {
        public clsOntologyItem OFileItem { get; private set; }
        public string FilePath { get; private set; }
        public string FileName { get; private set; }

        public int ContentLength { get; private set; }

        public HttpPostedFileBase HttpPostedFile { get; private set; }

        public MediaItem(HttpPostedFileBase fileItem, string rootPath, clsOntologyItem oFileItem)
        {
            HttpPostedFile = fileItem;

            FileName = fileItem.FileName;

            ContentLength = fileItem.ContentLength;

            OFileItem = oFileItem;
            FilePath = Path.Combine(rootPath, FileName);
        }
    }
}
