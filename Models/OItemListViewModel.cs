﻿using OModules.Models.Base;
using OModules.Models.Requests;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace OModules.Models
{
    public class OItemListViewModel : OViewBaseModel
    {
        public string idView {
            get
            {
                return View.IdView;
            }
        }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem newItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonSave { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem newItemInput { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem deleteItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem applyItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem addToClipboard { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblOrderId { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpOrderId { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem selectedItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem orderAsc { get; private set; }
        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem orderDesc { get; private set; }

        public object gridItem { get; set; }

        public NewWindowViewModel newWindowViewModel { get; private set; }
        public EditWindowViewModel editWindowViewModel { get; private set; }

        public EditOrderWindowViewModel editOrderWindowViewModel { get; private set; }

        public DeleteWindowViewModel deleteWindowViewModel { get; private set; }

        public SelectedORelItemRequest selectedOItemRequest { get; set; }

        public string _idParent { get; set; }

        public object setIdParent { get; set; }

        public ObjectRelNode nodeItem { get; set; }
        public object setNodeItem { get; set; }

        public object checkViewState { get; set; }

        public object setDataItems { get; set; }

        public List<object> _itemsPreApply { get; set; }
        public object addApplyItem { get; set; }
        public object removeApplyItem { get; set; }

        public string dataTypeBool { get; set; }
        public string dataTypeLong { get; set; }
        public string dataTypeReal { get; set; }
        public string dataTypeDateTime { get; set; }
        public string dataTypeString { get; set; }
        
        public bool _isToggledListen { get; set; }
        public object isToggledListen { get; set; }
        public object initialize { get; set; }
        public string ActionGetParent { get; set; }
        public string ActionGetGridConfig { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionNewItem { get; set; }

        public string ActionSetSelectedItem { get; set; }
        public string ActionSetOrderId { get; set; }
        public string ActionCheckExistance { get; set; }
        public string ActionSaveNewItems { get; set; }

        public string ActionObjectAttributeListInit { get; set; }
        public string ActionAttributeTypeListInit { get; set; }

        public string ActionAddToClipboard { get; set; }

        public string ActionAddObjectsInit { get; set; }

        public string ActionRelationEditor { get; set; }
        public string ActionModuleStarter { get; set; }

        public string ActionEditAttributeInit { get; set; }

        public string ActionHasRelations { get; set; }

        public string ActionDuplicateObject { get; set; }

        public string ActionGetTransformedNames { get; set; }

        public string ChannelSelectedParent { get; set; }
        public string ChannelSelectedItem { get; set; }
        public string ChannelAppliedObjects { get; set; }
        public string ChannelAppliedClasses { get; set; }
        public string ChannelAppliedAttributeTypes { get; set; }
        public string ChannelAppliedRelationTypes { get; set; }
        public string ChannelSelectedRelationNode { get; set; }
        public string ChannelViewReady { get; set; }
        public string ChannelParameterList { get; set; }
        public string ChannelAddedObjects { get; set; }

        
        public OItemListViewModel(HttpSessionStateBase session, string idView, string idInstance, string userId) : base(session, idView, idInstance, userId)
        {

            lblOrderId.ChangeViewItemValue(ViewItemType.Content.ToString(), "OrderId:");
            inpOrderId.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            applyItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            newItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            newItemInput.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            addToClipboard.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            newWindowViewModel = new NewWindowViewModel();
            editWindowViewModel = new EditWindowViewModel();
            editOrderWindowViewModel = new EditOrderWindowViewModel();
            deleteWindowViewModel = new DeleteWindowViewModel();
            

            _itemsPreApply = new List<object>();

            ChannelSelectedItem = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObjects = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelAppliedClasses = Channels.LocalData.Object_AppliedClasses.GUID;
            ChannelAppliedRelationTypes = Channels.LocalData.Object_AppliedRelationTypes.GUID;
            ChannelAppliedAttributeTypes = Channels.LocalData.Object_AppliedAttributeTypes.GUID;
            ChannelSelectedRelationNode = Channels.LocalData.Object_SelectedRelationNode.GUID;
            ChannelViewReady = Channels.LocalData.Object_ViewReady.GUID;
            ChannelParameterList = Channels.LocalData.Object_ParameterList.GUID;
            ChannelAddedObjects = Channels.LocalData.Object_AddedObjects.GUID;
        }
    }

    public class DeleteWindowViewModel
    {
        public string id { get; private set; }

        public DeleteWindowViewModel()
        {
            id = "deleteWindowMessage";
        }
    }

    public class EditOrderWindowViewModel : KendoNumericInput
    {
        public string id { get; private set; }

        public string idLblValueOrderId { get; private set; }
        public string lblValueOrderId { get; private set; }

        public string idInpValueEditOrderId { get; private set; }

        public string idSaveOrderId { get; private set; }

        public string lblSaveOrderId { get; private set; }

        public string idEditMessageOrderId { get; private set; }

        public EditOrderWindowViewModel()
        {
            id = "editOrderIdItemWindow";
            idLblValueOrderId = "lblValueOrderId";

            lblValueOrderId = "Value:";

            idInpValueEditOrderId = "inpValueEditOrderId";

            idSaveOrderId = "saveOrderId";

            lblSaveOrderId = "Save";

            idEditMessageOrderId = "editMessageOrderId";
        }
    }

    public class NewWindowViewModel
    {
        public string id { get; private set; }
        public string idFrameNew { get; private set; }

        public NewWindowViewModel()
        {
            id = "newItemWindow";
            idFrameNew = "newFrame";
        }
    }

    public class EditWindowViewModel
    {
        public string id { get; private set; }

        public string idLblGuid { get; private set; }
        public string lblGuid { get; private set; }

        public string idTxtGuid { get; private set; }

        public string idCpyGuid { get; private set; }
        public string iconClassCpyGuid { get; private set; }

        public string idLblValueName { get; private set; }
        public string lblValueName { get; private set; }

        public string idInpValueEditName { get; private set; }

        public string idSaveName { get; private set; }
        public string lblSaveName { get; private set; }

        public string idEditMessageName { get; private set; }
        
        public EditWindowViewModel()
        {
            id = "editNameItemWindow";
            idLblGuid = "lblGuid";
            lblGuid = "Id:";

            idTxtGuid = "txtGuid";
            idCpyGuid = "cpyGuid";
            iconClassCpyGuid = "fa fa-files-o";

            idLblValueName = "lblValueName";
            lblValueName = "Value:";

            idInpValueEditName = "inpValueEditName";
            lblSaveName = "Save";

            idEditMessageName = "editMessageName";
        }
    }
}