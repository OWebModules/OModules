﻿using MailModule.Models;
using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class MailListViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem listen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem gridContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Window)]
        public ViewItem mailBody { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem mailContent { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Window)]
        public ViewItem attachmentListWindow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem attachmentListContent { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem gridContainerAttachments { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem gridAttachments { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbarAttachment { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem downloadAttachments { get; private set; }


        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblStart { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.DatePicker, NoDefaultValueInit = true)]
        public ViewItem datepStart { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.DatePicker, NoDefaultValueInit = true)]
        public ViewItem datepEnd { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblEnd { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblSearchText { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inputSearchText { get; private set; }


        public clsOntologyItem refItem { get; set; }
        public clsOntologyItem refParentItem { get; set; }

        public List<MailItem> MailItems { get; set; }

        public MailConnection MailConnection { get; set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetGridConfig { get; set; }
        public string ActionGetGridConfigAttachments { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionGetBody { get; set; }
        public string ActionGetAttachmentList { get; set; }
        public string ActionDownloadAttachment { get; set; }
        public string ActionRelatedMailitem { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string ChannelAppliedObject { get; set; }

        public MailListViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_MailList.GUID, idInstance, userId)
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObject = Channels.LocalData.Object_AppliedObjects.GUID;
        }
    }
}