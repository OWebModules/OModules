﻿using LocalizationModule.Models;
using ModuleManagementModule.Models;
using ModuleManagementModule.Services;
using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ModuleStarterViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem titleContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem titleLabel { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem gridParent { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Object:")]
        public ViewItem labelObjectCaption { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "-")]
        public ViewItem labelObject { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton, DefaultEnable = true)]
        public ViewItem buttonNewWindow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem divModules { get; private set; }

        public string fullHeight { get; private set; }

        public object setDataItems { get; set; }
        public object messageFromWebsocket { get; set; }
        public object gridItem { get; set; }
        public object gridChangeHandler { get; set; }
        public object clickedButton { get; set; }
        public object idObject { get; set; }
        public object validateReference { get; set; }
        public string baseUrl { get; set; }

        public ViewModuleItem lastClicked { get; set; }
        public List<GuiEntry> GuiEntries { get; set; } = new List<GuiEntry>();


        public ActionConfigModuleStarter ActionConfig { get; set; }
        public ChannelConfigModuleStarter ChannelConfig { get; private set; }

        

        public ModuleStarterViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ModuleStarter1.GUID, idInstance, userId)
        {
            fullHeight = nameof(fullHeight);

            buttonNewWindow.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            ChannelConfig = new ChannelConfigModuleStarter();
        }
    }

    public class ActionConfigModuleStarter
    {
        public string ActionGetGridConfig { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionSetViewToClass { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionRelationEditor { get; set; }

        public string ActionEmbeddedNameEditInit { get; set; }
    }

    public class ChannelConfigModuleStarter
    {
        public string ChannelSelectedObject { get; set; }

        public ChannelConfigModuleStarter()
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}