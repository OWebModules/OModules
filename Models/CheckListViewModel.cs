﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using ProcessModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class CheckListViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem openEntries { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem doneEntries { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem cancelledEntries { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem editEntries { get; private set; }

        public clsOntologyItem UserItem { get; set; }
        public clsOntologyItem GroupItem { get; set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionItemComBarInit { get; set; }

        public string ActionChecklistInit { get; set; }

        public string ActionReportInit { get; set; }

        public string ActionGetChecklistData { get; set; }

        public string ActionUpdateChecklistEntryState { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string ChannelAppliedObject { get; set; }

        public string ChannelControllerMessage { get; set; }

        public Checklist Checklist { get; set; }

        public CheckListViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_Checklist.GUID, idInstance, userId)
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObject = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelControllerMessage = SpecialChannels.ControllerMessage;

            openEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            openEntries.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            doneEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            doneEntries.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            editEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            editEntries.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            cancelledEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            cancelledEntries.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
        }
    }

    
}