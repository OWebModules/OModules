﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace OModules.Models
{
    public class CustomUserStore :
        IUserStore<ApplicationUser>,
        IUserPasswordStore<ApplicationUser>,
        IUserRoleStore<ApplicationUser>
    {
        private List<ApplicationUser> users = new List<ApplicationUser>();
        private List<RoleUsers> roles = new List<RoleUsers>();

        public Task AddToRoleAsync(ApplicationUser user, string roleName)
        {
            var role = roles.FirstOrDefault(roleUsr => roleUsr.RoleItem.Name.ToLower() == roleName.ToLower());

            if (role == null)
            {
                role = new RoleUsers { RoleItem = new IdentityRole(roleName), Users = new List<ApplicationUser>() };
                role.Users.Add(user);
                roles.Add(role);

                user.Roles.Add(new IdentityUserRole
                {
                    RoleId = role.RoleItem.Id,
                    UserId = user.Id
                });

                return Task.FromResult(0);
            }

            var usr = role.Users.FirstOrDefault(usR => usR.Id == user.Id);

            if (usr == null)
            {
                role.Users.Add(user);
            }

            return Task.FromResult(0);
        }

        public Task CreateAsync(ApplicationUser user)
        {
            users.Add(user);
            return Task.FromResult(0);
        }

        public Task DeleteAsync(ApplicationUser user)
        {
            users.Remove(user);
            return Task.FromResult(0);
        }

        public void Dispose()
        {
            users?.Clear();
            users = null;
        }

        public Task<ApplicationUser> FindByIdAsync(string userId)
        {

            var user = users?.FirstOrDefault(usr => usr.Id == userId);

            return Task.FromResult(user);
        }

        public Task<ApplicationUser> FindByNameAsync(string userName)
        {
            var user = users?.FirstOrDefault(usr => usr.UserName.ToLower() == userName.ToLower());

            return Task.FromResult(user);
        }

        public Task<string> GetPasswordHashAsync(ApplicationUser user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<IList<string>> GetRolesAsync(ApplicationUser user)
        {
            var roleItems = roles.Where(role => role.Users.Any(usr => usr.Id == user.Id)).Select(rol => rol.RoleItem.Name);

            return Task.FromResult((IList<string>)roleItems.ToList());
        }

        public Task<bool> HasPasswordAsync(ApplicationUser user)
        {
            return Task.FromResult(!string.IsNullOrEmpty(user.PasswordHash));
        }

        public Task<bool> IsInRoleAsync(ApplicationUser user, string roleName)
        {
            var role = roles.FirstOrDefault(rol => rol.RoleItem.Name.ToLower() == roleName.ToLower());

            if (role == null)
            {
                return Task.FromResult(false);
            }

            return Task.FromResult(role.Users.Any(usr => usr.Id == user.Id));
        }

        public Task RemoveFromRoleAsync(ApplicationUser user, string roleName)
        {
            var role = roles.FirstOrDefault(rol => rol.RoleItem.Name.ToLower() == roleName.ToLower());

            if (role == null)
            {
                return Task.FromResult(0);
            }

            role.Users.RemoveAll(usr => usr.Id == user.Id);

            return Task.FromResult(0);
        }

        public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public Task UpdateAsync(ApplicationUser user)
        {
            users.RemoveAll(usr => usr.Id == user.Id);
            users.Add(user);
            return Task.FromResult(0);
        }
    }
}