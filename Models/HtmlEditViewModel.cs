﻿using HtmlEditorModule.Models;
using LocalizationModule.Models;
using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OntoWebCore.Models;

namespace OModules.Models
{
    public class HtmlEditViewModel : OViewBaseModel
    {
        public string fullHeight { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem nameInput { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem listenLink { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem listenAnchor { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem sendObject { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem applyObject { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openMediaViewerImage { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openMediaViewerPDF { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openMediaViewerVideo { get; private set; }


        [ViewModel(ViewItemClass = ViewItemClass.TextArea, ViewItemId = "myTextArea", NoDefaultValueInit = true)]
        public ViewItem textArea { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Link )]
        public ViewItem openInViewer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Link)]
        public ViewItem openDiffViewer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Link)]
        public ViewItem openAutoRelator { get; private set; }


        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem moduleFunctionsWindow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem chooseTemplateWindow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem templateContent { get; private set; }


        public object clickedButton { get; set; }
        public object validateReference { get; set; }

        public clsOntologyItem oItemRef { get; set; }
        public clsOntologyItem oItemHtml { get; set; }
        public clsObjectAtt oItemHtmlContent { get; set; }

        public List<clsOntologyItem> TransformedItems { get; set; } = new List<clsOntologyItem>();
        public List<HTMLTemplateGridItem> htmlTemplates { get; set; } = new List<HTMLTemplateGridItem>();

        public ActionConfigHtmlEdit ActionConfig { get; set; }
        public ChannelConfigHtmlEdit ChannelConfig { get; private set; }

        public List<GuiEntry> GuiEntries { get; set; } = new List<GuiEntry>();



        public HtmlEditViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_HtmlEditor.GUID, idInstance, userId)
        {

            fullHeight = nameof(fullHeight);

            isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            nameInput.ChangeViewItemValue(ViewItemType.Caption.ToString(), "Name");
            textArea.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            ChannelConfig = new ChannelConfigHtmlEdit();

        }
    }

    public class ActionConfigHtmlEdit
    {
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ValidateReceivedRefForLinkOrAnchor { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetHtmlContent { get; set; }
        public string ActionSaveHtmlContent { get; set;  }
        public string ActionGetTags { get; set; }

        public string ActionRelationEditor { get; set; }

        public string ActionModuleStarterInit { get; set; }
        
        public string ActionValidateReferenceTemplate { get; set; }
        public string ActionGetGridConfigTemplate { get; set; }
        public string ActionGetTemplateContent { get; set; }

        public string ActionOpenMediaViewerImage { get; set; }
        public string ActionOpenMediaViewerPDF { get; set; }
        public string ActionOpenMediaViewerVideo { get; set; }

        public string ActionHtmlViewer { get; set; }
        public string ActionDiffViewer { get; set; }
        public string ActionAutoRelator { get; set; }

        public string ActionGetUpdatedName { get; set; }

    }

    public class ChannelConfigHtmlEdit
    {
        public string ChannelSelectedObject { get; set; }
        public string ChannelSelectedClass { get; set; }
        public string ChannelAppliedObject { get; set; }

        public string ChannelControllerMessage { get; set; }


        public ChannelConfigHtmlEdit()
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObject = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID;
            ChannelControllerMessage = SpecialChannels.ControllerMessage;
        }
    }
}