﻿using LearningModule.Models;
using OModules.Models.Base;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class QuestionAnswerViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionGetQuestions { get; set; }
        public string ActionGetPossibleAnswers { get; set; }
        public string ActionGetCorrectAnswers { get; set; }

        public string ActionHtmlViewer { get; set; }

        public string ActionMediaBookmarkManager { get; set; }

        public string ActionSaveSelfAnswer { get; set; }

        public string ActionSaveAnswerWithOfficialAnswer { get; set; }

        public string ActionEmbeddedNameEditInit { get; set; }

        public string ActionItemComBarInit { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string ChannelControllerMessage { get; set; }

        public ClassObject ClassObject { get; set; }

        public object hideTimeout;

        public List<Question> Questions { get; set; } = new List<Question>();
        public List<QuestionViewItem> QuestionViewItems { get; set; } = new List<QuestionViewItem>();
        public List<clsOntologyItem> Categories { get; set; } = new List<clsOntologyItem>();
        public bool IncludeLowerCategories { get; set; }

        public QuestionReferenceSource QuestionReferenceSource { get; set; }
        

        public QuestionAnswerViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_QuestionAnswer.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Query-Answer");

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelControllerMessage = SpecialChannels.ControllerMessage;
        }
    }
}