﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.ClassEdit
{
    public class ClassAttributeViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem classAttributeEditor { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem gridItem { get; private set; }


        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem addAttribute { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem removeAttribute { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem setMinForwTo0 { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem decreaseMinForw { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem increaseeMinForw { get; private set; }



        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem setMaxForwToInfinite { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem decreaseMaxForw { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem increaseeMaxForw { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem setMaxForw { get; private set; }


        public clsOntologyItem refItem { get; set; }


        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionClassAttributeEditorInit { get; set; }

        public string ActionGetGridConfig { get; set; }
        public string ChannelSelectedClass { get; private set; }

        public ClassAttributeViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_Class_Attributes_Edit.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Class Attributes Edit");

            ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID;
        }
    }
}