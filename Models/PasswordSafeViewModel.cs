﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class PasswordSafeViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem gridParent { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem showPassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem copyPassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem changePassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem createPassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container, NoDefaultValueInit = true)]
        public ViewItem windowNewPassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inputPassword1 { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inputPassword2 { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonOk { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button, DefaultEnable = true)]
        public ViewItem buttonCancel { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar, DefaultEnable = true)]
        public ViewItem toolbarNewPassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar, DefaultEnable = true)]
        public ViewItem btnSecurePassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inputAuthenticationPassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container, NoDefaultValueInit = true)]
        public ViewItem authenticationWindow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonOkAuthentication { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button, DefaultEnable = true)]
        public ViewItem buttonCancelAuthentication { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar, DefaultEnable = true)]
        public ViewItem toolbarAuthentication { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem alertWrongPassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container, NoDefaultValueInit = true)]
        public ViewItem changePasswordWindow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar, DefaultEnable = true)]
        public ViewItem toolbarChangePassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inputChangePassword1 { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inputChangePassword2 { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonOkChangePassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button, DefaultEnable = true)]
        public ViewItem buttonCancelChangePassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem alertDifferentPasswords { get; private set; }

        public string fullHeight { get; private set; }

        public clsOntologyItem refItem { get; set; }

        public clsOntologyItem refParentItem { get; set; }

        public string ChannelSelectedObject { get; set; }

        public bool isAuthenticated { get; set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionGetGridConfig { get; set; }
        public string ActionGetPasswordItems { get; set; }
        public string ActionGetSecurePassword { get; set; }
        public string ActionSavePassword { get; set; }
        public string ActionChangePassword { get; set; }
        public string ActionValidatePassword { get; set; }
        public string ActionDecodePassword { get; set; }
        public string ActionAuthenticationInit { get; set; }


        public PasswordSafeViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_PasswordSafe.GUID, idInstance, userId)
        {
            isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            showPassword.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            changePassword.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            createPassword.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            alertWrongPassword.ChangeViewItemValue(ViewItemType.Caption.ToString(), Primitives.Messages.General_ErrorOccured);

            fullHeight = nameof(fullHeight);
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            

        }
    }
}