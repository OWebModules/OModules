﻿using OModules.Models.Base;
using OntologyAppDBConnector.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class TFSSyncViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem outputResult { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem syncTFS { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionGetRunningState { get; set; }

        public string ActionGetToolsForToolbar { get; set; }

        public string ActionSyncTFS { get; set; }

        public string ChannelSelectedObject { get; set; }

        public ClassObject RefItem { get; set; }

        public TFSSyncViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_TFSSync.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "TFS-Sync");

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}