﻿using LogModule.Models;
using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class LogListViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "-")]
        public ViewItem referenceName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem GridItem { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetGridConfig { get; set; }




        public string ChannelSelectedObject { get; set; }
        public string ChannelAppliedObjects { get; set; }
        
        public clsOntologyItem refItem { get; set; }
        public clsOntologyItem refParentItem { get; set; }

        

        public List<LogItem> LogItems { get; set; }

        public LogListViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, OModules.WebViews.Config.LocalData.Object_LogList.GUID, idInstance, userId)
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObjects = Channels.LocalData.Object_AppliedObjects.GUID;
 
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Log-List");
        }
    }
}