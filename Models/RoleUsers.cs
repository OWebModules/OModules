﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class RoleUsers
    {
        public IdentityRole RoleItem { get; set; }
        public List<ApplicationUser> Users { get; set; }
    }
}