﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.StateMachines
{
    
    public class RelationTreeStateMachine
    {
        public string LoadState { get; set; }
        public string Init { get; private set; }
        public string DataInit { get; private set; }
        public string OtherRightTreeInit { get; private set; }

        public RelationTreeStateMachine()
        {
            Init = nameof(Init);
            DataInit = nameof(DataInit);
            OtherRightTreeInit = nameof(OtherRightTreeInit);
            LoadState = nameof(Init);
        }
    }
}