﻿using OModules.Models.Base;
using OModules.Models.Requests;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace OModules.Models
{
    public class ObjectAttributeListViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem deleteItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem applyItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem addToClipboard { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblOrderId { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpOrderId { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem orderAsc { get; private set; }
        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem orderDesc { get; private set; }

        public string ActionGetGridConfig { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetOrderId { get; set; }
        public string ActionCheckExistance { get; set; }
        public string ActionSaveNewObjects { get; set; }

        public string ActionObjectAttributeListInit { get; set; }

        public string ActionAddToClipboard { get; set; }

        public string ActionAddObjectsInit { get; set; }

        public string ActionRelationEditor { get; set; }

        public string ChannelSelectedParent { get; set; }
        public string ChannelSelectedItem { get; set; }
        public string ChannelAppliedItems { get; set; }
        public string ChannelSelectedRelationNode { get; set; }
        public string ChannelViewReady { get; set; }
        public string ChannelParameterList { get; set; }
        public string ChannelAddedObjects { get; set; }


        public ObjectAttributeListViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ObjectAttributeList.GUID, idInstance, userId)
        {

            lblOrderId.ChangeViewItemValue(ViewItemType.Content.ToString(), "OrderId:");
            inpOrderId.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            applyItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            addToClipboard.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            ChannelSelectedItem = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedItems = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelSelectedRelationNode = Channels.LocalData.Object_SelectedRelationNode.GUID;
            ChannelViewReady = Channels.LocalData.Object_ViewReady.GUID;
            ChannelParameterList = Channels.LocalData.Object_ParameterList.GUID;
            ChannelAddedObjects = Channels.LocalData.Object_AddedObjects.GUID;
        }
    }
}