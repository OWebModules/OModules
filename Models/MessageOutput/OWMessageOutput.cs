﻿using OModules.Hubs;
using OModules.Notifications;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OntologyAppDBConnector;
using OModules.Services;

namespace OModules.Models.MessageOutput
{
    public class OWMessageOutput : IMessageOutput
    {
        public event OutputMessageDelegate OutputUIMessage;
        public event OutputMessageAddedDelegate OutputUIMessageAdded;
        private Logger logger = new Logger();

        private string idInstance;

        public void OutputCritical(string message)
        {

            OutputMessage(message, MessageOutputType.Critical);
        }

        public void OutputError(string message)
        {
            OutputMessage(message, MessageOutputType.Error);
        }

        public void OutputInfo(string message)
        {
            OutputMessage(message, MessageOutputType.Info);
        }

        public void OutputMessage(string message, MessageOutputType messsageType)
        {
            var interServiceMessage = new InterServiceMessage
            {
                ChannelId = SpecialChannels.ControllerMessage,
                ReceiverId = idInstance,
                GenericParameterItems = new List<object>
                {
                    new MessageState
                    {
                        MessageType = messsageType,
                        Message = HttpUtility.HtmlEncode(message)
                    }
                }
            };
            ModuleComHub.SendInterServiceMessageFromController(interServiceMessage);
            switch (messsageType)
            {
                case MessageOutputType.Info:
                    logger.Info(message);
                    break;
                case MessageOutputType.Warning:
                    logger.Warn(message);
                    break;
                case MessageOutputType.Error:
                    logger.Error(message);
                    break;
                case MessageOutputType.Critical:
                    logger.Error(message);
                    break;
            }
        }

        public void OutputWarning(string message)
        {
            OutputMessage(message, MessageOutputType.Warning);
        }

        public OWMessageOutput(string idInstance)
        {
            this.idInstance = idInstance;
        }
    }
    public class MessageState
    {
        public MessageOutputType MessageType { get; set; }
        public string Message { get; set; }
    }

    
}