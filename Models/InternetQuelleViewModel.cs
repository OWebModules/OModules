﻿using LiteraturQuelleModule.Models;
using OModules.Models.Base;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class InternetQuelleViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelUrl { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem buttonUrl { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inputUrl { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelCreator { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem buttonCreator { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inputCreator { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelDownloadstamp { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.DateTimeInput)]
        public ViewItem inputDownloadstamp { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inputName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem hiddenIdClassPartner { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem hiddenIdClassUrl { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem hiddenReference { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem askForCreation { get; private set; }

        #region mediaList

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelUpload { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelPlayer { get; private set; }

        #endregion

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionMediaListPDFInit { get; set; }
        public string ActionSingleRelationInit { get; set; }

        public string ActionObjectListInit { get; set; }

        public string ActionLiteraturQuelleCreateInit { get; set; }
        public string ChannelSelectedObject { get; set; }
        public string ChannelSelectedClass { get; set; }
        public string ChannelAppliedObject { get; set; }
        public string ChannelMediaList { get; set; }

        public string ChannelUploadedFiles { get; set; }

        public InternetQuelleModel InternetQuelle { get; set; }

        public string IdDirection { get; set; }
        public string IdRelationType { get; set; }
        public string IdParentRelation { get; set; }
        public ClassObject refItem { get; set; }


        public InternetQuelleViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_InternetQuelle.GUID, idInstance, userId)
        {
            labelName.ChangeViewItemValue(ViewItemType.Content.ToString(), "Name:");
            labelUrl.ChangeViewItemValue(ViewItemType.Content.ToString(), "Url:");
            labelCreator.ChangeViewItemValue(ViewItemType.Content.ToString(), "Creator:");
            labelDownloadstamp.ChangeViewItemValue(ViewItemType.Content.ToString(), "Download:");
            labelUpload.ChangeViewItemValue(ViewItemType.Content.ToString(), "Upload");
            labelPlayer.ChangeViewItemValue(ViewItemType.Content.ToString(), "Player");

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID;
            ChannelAppliedObject = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelMediaList = SpecialChannels.MediaList;
            ChannelUploadedFiles = SpecialChannels.UploadedFiles;

            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Internet-Quellenangabe");
        }

    }
}