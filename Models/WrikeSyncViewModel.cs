﻿using OModules.Models.Base;
using OntologyAppDBConnector.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class WrikeSyncViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem outputResult { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem syncWrike { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem wrikeIssueList { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionGetRunningState { get; set; }

        public string ActionGetToolsForToolbar { get; set; }

        public string ActionSyncWrike { get; set; }

        public string ActionGetGridConfig { get; set; }

        public string ChannelSelectedObject { get; set; }

        public ClassObject RefItem { get; set; }

        public WrikeSyncViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_WrikeSync.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Wrike-Sync");

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}