﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class EditBoolAttributeViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem attributeArea { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.CheckBox)]
        public ViewItem boolValue { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Value")]
        public ViewItem lblName { get; set; }

        public clsTypes itemTypes { get; private set; }

        public object checkViewState { get; set; }
        public object setDataItems { get; set; }
        public object getUrlParameters { get; set; }
        public object validateReference { get; set; }

        public clsObjectAtt objectAttribute { get; set; }
        public clsOntologyItem objectItem { get; set; }
        public clsOntologyItem objectParentItem { get; set; }
        public clsOntologyItem AttributeTypeItem { get; set; }

        public object messageFromWebsocket { get; set; }
        public object connectHandler { get; set; }
        public object disconnectHandler { get; set; }


        public ActionConfigBoolAttribute ActionConfig { get; set; }
        public ChannelConfigBoolAttribute ChannelConfig { get; set; }

        public EditBoolAttributeViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_EditBoolAttribute.GUID, idInstance, userId)
        {

           
            ChannelConfig = new ChannelConfigBoolAttribute
            {
                ChannelParameterList = Channels.LocalData.Object_ParameterList.GUID
            };

            itemTypes = new clsTypes();
        }
    }

    public class ActionConfigBoolAttribute
    {
        public string ActionValidateReference { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionGetAttribute { get; set; }
        public string ActionSaveAttribute { get; set; }
    }

    public class ChannelConfigBoolAttribute
    {
        public string ChannelParameterList { get; set; }
    }
}