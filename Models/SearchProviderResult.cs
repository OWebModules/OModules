﻿using OntologyClasses.BaseClasses;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModules.Models
{

    public class SearchProviderResult
    {
        public string SearchProviderId { get; set; }
        public string SearchProvider { get; set; }

        public string SearchProviderDescription { get; set; }

        public long CountSearchResultItems { get; set; }
    }

    [KendoGridConfig(width = "100%", height = "100%", groupbable = true, autoBind = true, selectable = SelectableType.cell, scrollable = true)]
    [KendoPageable(refresh = true, pageSizes = new int[] { 5, 10, 15, 20, 50, 100, 1000 }, buttonCount = 5, pageSize = 20)]
    [KendoSortable(mode = SortType.multiple, showIndexes = true)]
    [KendoFilter(extra = true)]
    [KendoStringFilterable(contains = "Contains", startsWith = "Starts with", eq = "Is equal to", neq = "Is not equal to", isnotnull = "Not null", isnull = "Null", isempty = "Empty")]
    public class SearchProviderResultItem
    {
        public string SearchProviderId { get; set; }
        [KendoColumn(hidden = true)]
        public string SearchProvider { get; set; }

        [KendoColumn(hidden = true)]
        public string SearchProviderDescription { get; set; }

        public bool IsSuccessful { get; set; }

        public string Message { get; set; }


        public string IdSearchItem { get; set; }

        [KendoColumn(hidden = false, title = "Item", Order = 1, filterable = true)]
        public string NameSearchItem { get; set; }

        public string IdParentSearchItem { get; set; }

        [KendoColumn(hidden = false, title = "Content", Order = 2, filterable = true)]
        public string Content { get; set; }

        public bool select;
        [KendoColumn(hidden = false, Order = 3, filterable = false, title = "Select", template = "<button type='button' onclick='autoRelatorContainer.handler.selectResultItem(this);'>Select</button>")]
        public bool Select
        {
            get { return select; }
            set
            {
                if (select == value) return;

                select = value;

            }
        }

        public bool apply;
        [KendoColumn(hidden = false, Order = 4, filterable = false, title = "Apply", template = "<button type='button' onclick='autoRelatorContainer.handler.applyResultItem(this);'>Apply</button>")]
        public bool Apply
        {
            get { return apply; }
            set
            {
                if (apply == value) return;

                apply = value;

            }
        }
    }
}
