﻿using AutomationLibrary.Models;
using JsonModule.Models;
using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class JsonEditorViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem editItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem schemaItem { get; private set; }

        public clsObjectAtt editOItem { get; set; }
        public clsObjectAtt schemaOItem { get; set; }

        public GetEsIndexItemRequest GetEsIndexItemRequest { get; set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ChannelSelectedObject { get; private set; }

        public JsonEditorViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_JsonEditor.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Json-Editor");
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}