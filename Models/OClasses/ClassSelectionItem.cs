﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.OClasses
{
    public class ClassSelectionItem
    {
        public string IdClass { get; set; }
        public string SearchTerm { get; set; }
    }
}