﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TypedTaggingModule.Models;

namespace OModules.Models
{
    public class TypedOntoRelatorViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem infovis { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem log { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Splitter)]
        public ViewItem splitter { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem relationMgmt { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionGetPossibleRelations { get; set; }

        public string ActionGetPossibleRelationsBetweenNodes { get; set; }

        public string ChannelSelectedObject { get; set; }

        public clsOntologyItem refItem { get; set; }
        public clsOntologyItem refParentItem { get; set; }

        public PossibleRelations PossibleRelations { get; set; } = new PossibleRelations();


        public TypedOntoRelatorViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_TypedOntoRelator.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Typed Onto-Relator");
        }
    }
}