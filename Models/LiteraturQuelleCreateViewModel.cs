﻿using OModules.Models.Base;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class LiteraturQuelleCreateViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelQuellType { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.KendoDropDownList)]
        public ViewItem dropDownQuellType { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inputName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelReference { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonSave { get; private set; }

        public string idClassQuellTypes { get; set; }

        public ClassObject refItem;

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionGetDropDownConfigQuellTypes { get; set; }

        public LiteraturQuelleCreateViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_Literaturquelle_Creator.GUID, idInstance, userId)
        {
            SetWindowTitle("Create Literaturquelle");
        }
    }

}