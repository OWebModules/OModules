﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class FileUploadViewModel : OViewBaseModel
    {
        public string idView
        {
            get
            {
                return View.IdView;
            }
        }

        [ViewModel(ViewItemClass = ViewItemClass.KendoDropDownList)]
        public ViewItem uploadContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.FileUpload, ViewItemId = "files")]
        public ViewItem fileItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar, ViewItemId = "toolbarUpload")]
        public ViewItem toolBar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem listen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "uploadHeader")]
        public ViewItem panelHeader { get; private set; }

        public int maxUploadSize { get; private set; }

        public string ActionUpload { get; set; }
        public string ActionRemove { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string ChannelUploadedFiles { get; set; }

        public clsOntologyItem oItemReference { get; private set; }
        public clsOntologyItem oItemReferenceParent { get; private set; }

        public string idReference { get; private set; }

        public string nameReference { get; set; }

        public clsOntologyItem relationTypeItem { get; private set; }
        public string nameRelationType { get; private set; }
        public clsOntologyItem directionItem { get; private set; }

        public void SetOItemReference(clsOntologyItem oItemReference, clsOntologyItem oItemReferenceParent, clsOntologyItem oItemRelationType, clsOntologyItem oItemDirection)
        {
            this.oItemReference = oItemReference;
            this.oItemReferenceParent = oItemReferenceParent;
            idReference = oItemReference?.GUID;
            nameReference = oItemReference.Name;
            if (oItemReferenceParent != null)
            {
                nameReference += $" ({ oItemReferenceParent.Name })";
            }

            relationTypeItem = oItemRelationType;

            directionItem = oItemDirection;
        }
        
        public FileUploadViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_FileUpload1.GUID, idInstance, userId)
        {            
            
            maxUploadSize = int.MaxValue;

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelUploadedFiles = SpecialChannels.UploadedFiles;
        }
    }
}