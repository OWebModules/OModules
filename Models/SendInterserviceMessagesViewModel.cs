﻿using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class SendInterserviceMessagesViewModel : OViewBaseModel
    {

        public string ActionSendInterserviceMessage { get; set; }

        public SendInterserviceMessagesViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_SendInterserviceMessages.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Send Interserivce-Messages");
        }
    }
}