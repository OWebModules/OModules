﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class EditOitemViewModel : OViewBaseModel
    {
        private clsTypes types = new clsTypes();

        [ViewModel(ViewItemClass = ViewItemClass.KendoDropDownList, NoDefaultValueInit = true)]
        public ViewItem dropDownItemType { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolBar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem newItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem saveItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem applyItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem listenItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem gridOitems { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem parentInput { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelParent { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem selectedItemType { get; private set; }


        public ItemTypeConfig itemTypes { get; private set; }

        public object grid { get; set; }

        public KendoDropDownConfig dropDownConfig { get; set; }
        public List<EditItem> RowItems { get; set; }

        public clsOntologyItem refItem { get; set; }
        public clsOntologyItem refParentItem { get; set; }
        public string refName { get; set; }

        public object setDataItems { get; set; }

        public object setObjectId { get; set; }
        public object selectItemType { get; set; }
        public object clickedButton { get; set; }
        public object checkedItemChange { get; set; }
        public object gridChangeHandler { get; set; }
        public object beforeEditHandler { get; set; }
        public object createGrid { get; set; }
        public object cellCloseHandler { get; set; }
        public object saveHandler { get; set; }
        public object dataBoundHandler { get; set; }

        public object init { get; set; }
        public object getUrlParameters { get; set; }

        public object connectHandler { get; set; }
        public object disconnectHandler { get; set; }
        public object validateReference { get; set; }
        public object messageFromWebsocket { get; set; }

        public ChannelConfig ChannelConfig { get; set; }
        public ActionConfig ActionConfig { get; set; }

        

        public EditOitemViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_EditOItem1.GUID, idInstance, userId)
        {

            dropDownItemType.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            dropDownConfig = new KendoDropDownConfig
            {
                optionLabel = "Select a ItemType...",
                dataSource = new List<KendoDropDownItem>
                {
                    new KendoDropDownItem
                    {
                        Value = types.AttributeType,
                        Text = types.AttributeType
                    },
                    new KendoDropDownItem
                    {
                        Value = types.ClassType,
                        Text = types.ClassType
                    },
                    new KendoDropDownItem
                    {
                        Value = types.ObjectType,
                        Text = types.ObjectType
                    },
                    new KendoDropDownItem
                    {
                        Value = types.RelationType,
                        Text = types.RelationType
                    }
                }
            };

            listenItem.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);

            ChannelConfig = new ChannelConfig
            {
                ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID,
                ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID,
                ChannelParameterList = Channels.LocalData.Object_ParameterList.GUID,
                ChannelAddedObjects = Channels.LocalData.Object_AddedObjects.GUID,
                ChannelAppliedObjects = Channels.LocalData.Object_AppliedObjects.GUID
            };

            itemTypes = new ItemTypeConfig
            {
                AttributeType = types.AttributeType,
                RelationType = types.RelationType,
                ClassType = types.ClassType,
                ObjectType = types.ObjectType
            };

            

            RowItems = new List<EditItem>();


        }
    }

    public class ActionConfig
    {
        public string ActionValidateReference { get; set; }
        public string ActionGetGridConfig { get; set; }
        public string ActionAddDataRows { get; set; }
        public string ActionUpdate { get; set; }
        public string ActionDestroy { get; set; }
        public string ActionCreate { get; set; }
        public string ActionRead { get; set; }
        public string ActionSave { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
    }

    public class ChannelConfig
    {
        public string ChannelSelectedObject { get; set; }
        public string ChannelSelectedClass { get; set; }
        public string ChannelParameterList { get; set; }

        public string ChannelAddedObjects { get; set; }
        public string ChannelAppliedObjects { get; set; }
    }

    public class ItemTypeConfig
    {
        public string AttributeType { get; set; }
        public string RelationType { get; set; }
        public string ClassType { get; set; }
        public string ObjectType { get; set; }
    }
}