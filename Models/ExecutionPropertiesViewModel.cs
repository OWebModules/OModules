﻿using AutomationLibrary.Models;
using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ExecutionPropertiesViewModel : OViewBaseModel
    {

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public ExecutionPropertiesViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ExecutionProperties.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Execution-Properties");
        }
    }
}