﻿using OModules.Models.Base;
using OntologyAppDBConnector.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ObjectInsertViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inpClass { get; private set; }


        [ViewModel(ViewItemClass = ViewItemClass.TreeList, NoDefaultValueInit = true)]
        public ViewItem relationTree { get; private set; }
        


        public ClassObject refItem { get; set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetClassDropDownConfig { get; set; }

        public string ActionGetTreeConfigSelection { get; set; }


        public ObjectInsertViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_BatchInsert.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Object-Insert");
        }
    }
}