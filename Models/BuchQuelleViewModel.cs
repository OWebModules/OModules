﻿using LiteraturQuelleModule.Models;
using OModules.Models.Base;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class BuchQuelleViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelSeite { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inputSeite { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelLiteratur { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inputLiteratur { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem buttonLiteratur { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem hiddenIdClassLiteratur { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem hiddenReference { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem askForCreation { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inputName { get; private set; }


        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionLiteraturQuelleCreateInit { get; set; }
        public string ChannelSelectedObject { get; set; }
        public string ChannelSelectedClass { get; set; }
        public string ChannelAppliedObject { get; set; }

        public BuchQuelleModel BuchQuelle { get; set; }

        public string IdDirection { get; set; }
        public string IdRelationType { get; set; }
        public string IdParentRelation { get; set; }
        public ClassObject refItem { get; set; }


        public BuchQuelleViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_BuchQuelle.GUID, idInstance, userId)
        {
            labelLiteratur.ChangeViewItemValue(ViewItemType.Content.ToString(), "Literatur:");
            labelSeite.ChangeViewItemValue(ViewItemType.Content.ToString(), "Seite:");
            
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID;
            ChannelAppliedObject = Channels.LocalData.Object_AppliedObjects.GUID;

            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Buch-Quellenangabe");
        }

    }
}