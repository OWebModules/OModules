﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace OModules.Models
{
    public class CustomRoleStore :
        IRoleStore<IdentityRole>
    {
        private List<IdentityRole> roles = new List<IdentityRole>();

        
        public Task CreateAsync(IdentityRole role)
        {
            roles.Add(role);
            return Task.FromResult(0);
        }


        public Task DeleteAsync(IdentityRole role)
        {
            roles.RemoveAll(rol => rol.Name.ToLower() == role.Name.ToLower());
            return Task.FromResult(0);
        }

        

        public void Dispose()
        {
            roles.Clear();
            roles = null;
        }

        public Task<IdentityRole> FindByIdAsync(string roleId)
        {
            var role = roles.FirstOrDefault(rol => rol.Id == roleId);
            return Task.FromResult(role);
        }

        public Task<IdentityRole> FindByNameAsync(string roleName)
        {
            var role = roles.FirstOrDefault(rol => rol.Name.ToLower() == roleName.ToLower());
            return Task.FromResult(role);
        }
     
 
        public Task UpdateAsync(IdentityRole role)
        {
            roles.RemoveAll(rol => rol.Id == role.Id);
            roles.Add(role);
            return Task.FromResult(0);
        }

 
        
    }
}