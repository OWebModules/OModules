﻿using OntologyClasses.BaseClasses;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.Results
{
    public class GetClassPathResult
    {
        public List<clsOntologyItem> SearchNodes { get; set; }
        public List<ViewTreeNode> TreeNodes { get; set; }
    }
}