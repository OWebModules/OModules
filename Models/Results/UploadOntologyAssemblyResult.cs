﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.Results
{
    public class UploadOntologyAssemblyResult
    {
        public string FilePath { get; set; }
        public bool IsOk { get; set; } = true;
        public string ErrorMessage { get; set; }
        public int CountToDoAttributeTypes { get; set; }
        public int CountImportAttributeTypes { get; set; }
        public int CountToDoRelationTypes { get; set; }
        public int CountImportRelationTypes { get; set; }
        public int CountToDoClasses { get; set; }
        public int CountImportClasses { get; set; }
        public int CountToDoObjects { get; set; }
        public int CountImportObjects { get; set; }

        public int CountToDoClassAttributes { get; set; }
        public int CountImportClassAttributes { get; set; }
        public int CountToDoClassRelations { get; set; }
        public int CountImportClassRelations { get; set; }

        public int CountToDoObjectAttributes { get; set; }
        public int CountImportObjectAttributes { get; set; }
        public int CountToDoObjectRelations { get; set; }
        public int CountImportObjectRelations { get; set; }
    }
}