﻿using HtmlEditorModule.Models;
using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class HtmlHistoryViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem contentContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Splitter)]
        public ViewItem splitter { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Splitter)]
        public ViewItem diffOutput { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem gridParent { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonOpen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonRecover { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem btnDelete { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem htmlPreviewWindow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem htmlPreviewWindowContent { get; private set; }

        public clsOntologyItem oItemRef { get; set; }
        public clsOntologyItem oItemHtml { get; set; }

        public List<HtmlHistoryItemWithRaw> HtmlHistoryEntries { get; set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetGridConfig { get; set; }
        public string ActionGetHtmlCodeCombi { get; set; }

        public string ActionGetHtmlForPreview { get; set; }

        public string ActionRestoreHistoryEntry { get; set; }

        public string ActionDeleteHisotryItems { get; set; }

        public string ChannelSelectedObject { get; set; }

        public HtmlHistoryViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_HtmlHistory.GUID, idInstance, userId)
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}