﻿using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using TimeManagementModule.Models;
using System.Linq;
using System.Web;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoWebCore.Models;

namespace OModules.Models
{
    public class TimeManagementEntryListViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem gridParent { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblGroup { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.KendoDropDownList, NoDefaultValueInit = true)]
        public ViewItem inpGroup { get; private set; }


        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionGetGridConfig { get; set; }
        public string ActionTimeManagementListInit { get; set; }
        public string ActionGetDropDownConfigGroups { get; set; }

        public ClassObject ClassObject { get; set; }

        public string ChannelSelectedObject { get; set; }

        public string idClassTimeManagement { get; set; }
        public string idClassGroup { get; set; }

        

        public clsOntologyItem UserItem { get; set; }
        public clsOntologyItem GroupItem { get; set; }

        public List<TimeManagementEntryListFilter> FilterList { get; set; } = new List<TimeManagementEntryListFilter>();
        public TimeManagementEntryListFilter SelectedFilter;
        public KendoDropDownConfig dropDownConfig { get; set; }

        public TimeManagementEntryListViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_TimeManagementEntryList.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Zeiterfassungsliste");

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}