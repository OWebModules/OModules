﻿using LogModule.Models;
using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class LogEditorViewModel : OViewBaseModel
    { 

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem listen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem logEditor { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Name:")]
        public ViewItem lblName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Timestamp:")]
        public ViewItem lblTimestamp { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.DateTimeInput)]
        public ViewItem timestamp { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Message:")]
        public ViewItem lblMessage { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.TextArea)]
        public ViewItem inpMessage { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Logstate:")]
        public ViewItem lblLogstate { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Reference:")]
        public ViewItem lblReference { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "-")]
        public ViewItem referenceName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpLogstate { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem changeLogstate { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "User:")]
        public ViewItem lblUser { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpUser { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem navigationBarContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem dataItem { get; private set; }



        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionNavigationBar { get; set; }
        public string ActionSetLogItem { get; set; }


        public string ActionSaveSessionData { get; set; }

        
        public string ChannelSelectedObject { get; set; }
        public string ChannelNavigationItems { get; set; }

        public string ChannelSelectedNavigationItem { get; set; } 

        public clsOntologyItem refItem { get; set; }
        public clsOntologyItem refParentItem { get; set; }
        public clsOntologyItem userItem { get; set; }

        public List<LogItem> LogItems { get; set; }

        public LogItem SelectedLogItem { get; set; }


        public LogEditorViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_LogEditor.GUID, idInstance, userId)
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelNavigationItems = SpecialChannels.NavigationItems;
            ChannelSelectedNavigationItem = SpecialChannels.SelectedNavigationItem;

            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Log-Editor");
        }
    }
}