﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class TypedTaggingListViewModel : OViewBaseModel
    {

        public clsOntologyItem refItem { get; set; }
        public clsOntologyItem refParentItem { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem refPanel { get; set; }

        public string classLoader { get; private set; }
        public string classContentIndex { get; private set; }
        public string classTopPanel { get; private set; }
        public string classNoprint { get; private set; }
        public string classSlideButton { get; private set; }
        public string classA4 { get; private set; }

        public object getTags { get; set; }

        public object validateReference { get; set; }
        public object getUrlParameters { get; set; }
        public object setDataItems { get; set; }

        public ActionConfigTypedTaggingList ActionConfig { get; set; }
        public ChannelConfigTypedTaggingList ChannelConfig { get; set; }

        public TypedTaggingListViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_TypedTaggingList.GUID, idInstance, userId)
        {

            classLoader = "loader";
            classContentIndex = "contentIndex";
            classTopPanel = "topPanel";
            classNoprint = "noprint";
            classSlideButton = "slide-button";
            classA4 = "A4";

            ChannelConfig = new ChannelConfigTypedTaggingList();
        }
    }

    public class ActionConfigTypedTaggingList
    {
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetTags { get; set; }
        public string ActionEditObject { get; set; }


    }

    public class ChannelConfigTypedTaggingList
    {
        public string ChannelSelectedObject { get; private set; }
        public string ChannelSavedTypeTag { get; private set; }

        public string ChannelAppliedObject { get; private set; }

        public string ChannelViewReady { get; private set; }

        public ChannelConfigTypedTaggingList()
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObject = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelSavedTypeTag = SpecialChannels.SavedTypedTag;
            ChannelViewReady = Channels.LocalData.Object_ViewReady.GUID;
        }
    }
}