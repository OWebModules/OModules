﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class EditStringAttributeViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem attributeArea { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.TextArea)]
        public ViewItem strValue { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Value")]
        public ViewItem lblName { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem btnOk { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem btnClear { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem listen { get; set; }

        public clsTypes itemTypes { get; private set; }

        public object setDataItems { get; set; }
        public object getUrlParameters { get; set; }
        public object validateReference { get; set; }

        public clsObjectAtt objectAttribute { get; set; }
        public clsOntologyItem objectItem { get; set; }
        public clsOntologyItem objectParentItem { get; set; }
        public clsOntologyItem AttributeTypeItem { get; set; }

        public object messageFromWebsocket { get; set; }
        public object connectHandler { get; set; }
        public object disconnectHandler { get; set; }


        public ActionConfigStrAttribute ActionConfig { get; set; }
        public ChannelConfigStrAttribute ChannelConfig { get; set; }

        public EditStringAttributeViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_EditStringAttribute.GUID, idInstance, userId)
        {

            ChannelConfig = new ChannelConfigStrAttribute
            {
                ChannelParameterList = Channels.LocalData.Object_ParameterList.GUID,
                ChannelAddAttribute = Channels.LocalData.Object_AddAttribute.GUID
            };

            itemTypes = new clsTypes();
        }
    }

    public class ActionConfigStrAttribute
    {
        public string ActionValidateReference { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionGetAttribute { get; set; }
        public string ActionSaveAttribute { get; set; }
    }

    public class ChannelConfigStrAttribute
    {
        public string ChannelParameterList { get; set; }
        public string ChannelAddAttribute { get; set; }
    }
}