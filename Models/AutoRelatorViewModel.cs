﻿using OModules.Models.Base;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TypedTaggingModule.Models;

namespace OModules.Models
{
    

    public class AutoRelatorViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem panelUserStories { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Userstory", DefaultVisible = false)]
        public ViewItem labelUserStories { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.TextArea)]
        public ViewItem textAreaUserStories { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem title { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem btnSearchAll { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem btnSearchOntology { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem spanResult { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Anchor)]
        public ViewItem objectName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Anchor)]
        public ViewItem listenObject { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inpClass { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblOrderId { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem btnTypedTag { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inpRelation { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem btnSaveObject { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.NumericInput, NoDefaultValueInit = true)]
        public ViewItem inpOrderId { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem outputParse { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem outputAutoComplete { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem btnInsertOntology { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpAutoComplete { get; private set; }

        public clsOntologyItem refItem { get; set; }

        public clsOntologyItem refParentItem { get; set; }

        public Ontology ontology { get; set; }

        public clsObjectAtt attributeItem { get; set; }

        public clsOntologyItem classItemForSave { get; set; }

        public List<ObjectClass> AutoCompleteSource { get; set; }

        public string idClassSelected { get; set; }

        public List<TextPart> AutoCompleteRanges { get; set; } = new List<TextPart>();
        public List<TextRange> TextRanges { get; set; } = new List<TextRange>();
        public List<FoundOntologyJoin> OntologyRanges { get; set; } = new List<FoundOntologyJoin>();

        public List<ISearchProvider> SearchProviders { get; set; } = new List<ISearchProvider>();

        public List<SearchProviderResultItem> SearchProviderResultItems { get; set; } = new List<SearchProviderResultItem>();
        
        public string text { get; set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionParseText { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetTags { get; set; }
        public string ActionSaveRelatorTags { get; set; }
        public string ActionSaveRelations { get; set; }
        public string ActionGetRelationTypeDropDownConfig { get; set; }
        public string ActionGetClassDropDownConfig { get; set; }
        public string ActionGetOntoTemplate { get; set; }
        public string ActionParseAutocomplete { get; set; }
        public string ActionSaveNewObject { get; set; }
        public string ActionTypedTaggingList { get; set; }
        public string ActionSaveOntoRelations { get; set; }
        public string ActionGetAutoCompletes { get; set; }

        public string ActionEditObject { get; set; }

        public string ActionValidateReferenceTypedTaggingList { get; set; }
        public string ActionGetTagsTypedTaggingList { get; set; }
        public string ActionEditObjectTypedTaggingList { get; set; }

        public string ActionGetSearchProviders { get; set; }

        public string ActionSearch { get; set; }

        public string ActionGetGridConfigSearchProviderResult { get; set; }

        public string ActionGetTransformedNames { get; set; }

        public string ChannelSelectedObject { get; private set; }
        public string ChannelAppliedObjects { get; private set; }
        public string ChannelSelectedClass { get; private set; }

        public string ChannelSavedTypeTag { get; private set; }
        public string ChannelViewReady { get; private set; }

        public AutoRelatorViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_AutoRelator.GUID, idInstance, userId)
        {

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObjects = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID;
            ChannelSavedTypeTag = SpecialChannels.SavedTypedTag;
            ChannelViewReady = Channels.LocalData.Object_ViewReady.GUID;


        }

    }
}