﻿using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class DiffTextViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.TextArea)]
        public ViewItem baseText { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.TextArea)]
        public ViewItem newText { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Radio)]
        public ViewItem sidebyside { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Side by side")]
        public ViewItem LblSidebyside { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Radio)]
        public ViewItem inline { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Inline")]
        public ViewItem lblInline { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem contextSize { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem diffOutput { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionHtmlDiff { get; set; }

        public string ActionGetToolsForToolbar { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string ChannelSelectedCell { get; set; }

        public DiffTextViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session,WebViews.Config.LocalData.Object_DiffText.GUID, idInstance, userId)
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelSelectedCell = SpecialChannels.SelectedCell;
        }
    }
}