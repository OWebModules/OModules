﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TextParserModule;
using TextParserModule.Models;

namespace OModules.Models
{
    public class TextParserViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem gridParent { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.KendoDropDownList, NoDefaultValueInit = true)]
        public ViewItem dropDownIndex { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button, NoDefaultValueInit = true)]
        public ViewItem deleteIndex { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem query { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem search { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button, NoDefaultValueInit = true)]
        public ViewItem parse { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem saveQuery { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem import { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem configureColumns { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openScript { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem colorConfig { get; private set; }


        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionGetGridConfig { get; set; }

        public string ActionResetScrollId { get; set; }

        public string ActionParse { get; set; }
        public string ActionDeleteIndex { get; set; }
        public string ActionGetEsIndexItemRequest { get; set; }

        public string ActionJsonEditorInit { get; set; }

        public string ActionGetAutoCompletes { get; set; }

        public string ActionSavePattern { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string ChannelControllerMessage { get; set; }


        public clsOntologyItem parserItem { get; set; }
        public TextParser textParser { get; set; }
        public List<ParserField> textParserFields { get; set; }
        public string scrollId { get; set; }

        public IEnumerable<Dictionary<string,object>> lastPageValues { get; set; }

        public int lastPage { get; set; }
        public int lastPageSize { get; set; }
        public List<KendoSortRequest> lastSortRequests { get; set; } = new List<KendoSortRequest>();

        public TextParserViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_QueryResult.GUID, idInstance, userId)
        {
            isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelControllerMessage = SpecialChannels.ControllerMessage;

        }
    }
}