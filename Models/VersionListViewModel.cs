﻿using OModules.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class VersionListViewModel : OViewBaseModel
    {
        public VersionListViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_VersionList.GUID, idInstance, userId)
        {

        }
    }
}