﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using PartnerModule.Models;
using PartnerModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class PartnerViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem partnerEditors { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openNewPartner { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem newObjectFrame { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Window)]
        public ViewItem newPartnerWindow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem partnerTabs { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem addressTab { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem personalTab { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem communicationTab { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpName { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpStrasse { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpPostfach { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblName { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblStrasse { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblPostfach { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpPLZ { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenPlz { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblPLZ { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpOrt { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenOrt { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblOrt { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpLand { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenLand { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblLand { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpVorname { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblVorname { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpNachname { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblNachname { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpGeschlecht { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenGeschlecht { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblGeschlecht { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpFamilienstand { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenFamilienstand { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblFamilienstand { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblGeburtsdatum { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.DatePicker)]
        public ViewItem datepGeburtsdatum { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblTodesdatum { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.DatePicker)]
        public ViewItem datepTodesdatum { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpGebursort { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenGebursort { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblGeburtsort { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpSozialsversicherungsnummer { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenSozialsversicherungsnummer { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblSozialsversicherungsnummer { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpETin { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenETin { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblETin { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpIdentifkationsnummer { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenIndentifikationsNr { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblIdentifkationsnummer { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpSteuernummer { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenSteuernummer { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblSteuernummer { get; set; }



        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem comToolBar { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem comGrid { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenTelephone { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenFax { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenEmail { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListenUrl { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem multipleChooser { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem multipleGrid { get; set; }


        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem applyMultipleItem { get; set; }

        public string idClassETin { get; set; }
        
        public string idClassFamienstand { get; set; }
        public string idClassGeschlecht { get; set; }
        public string idClassGeburtsort { get; set; }
        public string idClassSozialversicherungsnummer { get; set; }
        public string idClassIdentificationsnummer { get; set; }
        public string idClassSteuernummer { get; set; }

        public string idClassPLZ { get; set; }
        public string idClassOrt { get; set; }
        public string idClassLand { get; set; }

        public string idClassPhone { get; set; }
        public string idClassEmail { get; set; }
        public string idClassUrl { get; set; }
        public string idClassPartner { get; set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetGridConfig { get; set; }
        public string ActionSaveEmailAddress { get; set; }
        public string ActionSaveTelephone { get; set; }
        public string ActionSaveFax { get; set; }
        public string ActionSaveUrl { get; set; }
        public string ActionSavePersonalData { get; set; }
        public string ActionSaveVorname { get; set; }
        public string ActionSaveNachname { get; set; }
        public string ActionSaveGeburtsdatum { get; set; }
        public string ActionSaveTodesdatum { get; set; }
        public string ActionSaveName { get; set; }
        public string ActionSavePlzOrt { get; set; }
        public string ActionSaveStrasse { get; set; }
        public string ActionSavePostfach { get; set; }
        public string ActionNewPartner { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string ChannelSelectedClass { get; set; }
        public string ChannelAppliedObjects { get; set; }

        public clsOntologyItem refItem { get; set; }
        public clsOntologyItem refParentItem { get; set; }
        public Partner partner { get; set; }

        public PartnerViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session,WebViews.Config.LocalData.Object_Partner1.GUID, idInstance, userId)
        {

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID;
            ChannelAppliedObjects = Channels.LocalData.Object_AppliedObjects.GUID;
        }
    }
}