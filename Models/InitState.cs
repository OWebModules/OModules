﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class InitState
    {
        public bool IsError { get; set; }
        public bool IsWarning { get; set; }
        public bool IsOk { get; set; }
        public string Message { get; set; }

        public void SetError(string message)
        {
            IsError = true;
            IsWarning = false;
            IsOk = false;
            Message = message;
        }

        public void SetWarning(string message)
        {
            IsError = false;
            IsWarning = true;
            IsOk = false;
            Message = message;
        }

        public void Clear()
        {
            IsError = false;
            IsWarning = false;
            IsOk = true;
            Message = string.Empty;
        }

        public InitState()
        {
            IsOk = true;
        }
    }
}