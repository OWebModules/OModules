﻿using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class AudioPlayerViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem audioPlayerArea { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem audioPlayer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem audioPlayerContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem listen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isCheckedAutoPlay { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem bookmark { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblBookmark { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem downloadMedia { get; private set; }

        public object setDataItems { get; set; }
        public object messageFromWebsocket { get; set; }
        public object validateReference { get; set; }
        public object getUrlParameters { get; set; }
        public object connectCallback { get; set; }
        public object disconnectCallback { get; set; }

        public ActionConfigAudioPlayer ActionConfig { get; set; }
        public ChannelConfigAudioPlayer ChannelConfig { get; private set; }

        public AudioPlayerViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_AudioPlayer.GUID, idInstance, userId)
        {

            ChannelConfig = new ChannelConfigAudioPlayer();

        }
    }

    public class ActionConfigAudioPlayer
    {
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetSessionDataUrl { get; set; }
    }

    public class ChannelConfigAudioPlayer
    {
        public string ChannelSelectedObject { get; set; }
        public string ChannelViewReady { get; set; }
        public string ChannelMediaList { get; set; }

        public ChannelConfigAudioPlayer()
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelViewReady = Channels.LocalData.Object_ViewReady.GUID;
            ChannelMediaList = SpecialChannels.MediaList;
        }
    }
}