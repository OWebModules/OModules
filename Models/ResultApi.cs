﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ResultApi<T>
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }

    public class ResultApiSimple
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }
    }
}