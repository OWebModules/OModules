﻿using OntoWebCore.Models;
using System.Collections.Generic;

namespace OModules.Models
{
    public class ResultAddRow
    {
        public bool isOk { get; set; }
        public EditItem ItemToAdd { get; set; }
        public List<Dictionary<string, object>> ChangedProperties { get; set; }
        public string Message { get; set; }
    }
}