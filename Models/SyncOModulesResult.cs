﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class SyncOModulesResult
    {
        public int CountControllers { get; set; }
        public int CountActions { get; set; }
        public int CountParameters { get; set; }
    }
}