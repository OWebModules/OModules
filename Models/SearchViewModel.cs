﻿using OItemFilterModule.Models;
using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class SearchViewModel : OViewBaseModel
    {
        public ViewItem rootContainer { get; private set; }
        public ViewItem searchArea { get; private set; }
        public ViewItem searchCriteriaRow { get; private set; }
        public ViewItem panelBar { get; private set; }
        public ViewItem resultPanel { get; private set; }
        public ViewItem resultCount { get; private set; }
        public ViewItem resultToolbar { get; private set; }
        public ViewItem grid { get; private set; }
        public ViewItem applyItem { get; private set; }

        public List<SearchItem> SearchItemList { get; private set; }

        public int loadingTimeout { get; private set; }

        public object gridItem { get; set; }
        public object addSearchButtonArea { get; set; }
        public object loadData { get; set; }
        public object loadingTimer { get; set; }
        public object lastDataItemOfGrid { get; set; }
        public object itemsToApply { get; set; }

        public List<ViewItem> ViewItems { get; private set; }
        public List<clsOntologyItem> Classes { get; set; }
        public List<clsOntologyItem> AttributeTypes { get; set; }
        public List<clsOntologyItem> RelationTypes { get; set; }

        public string fullHeight { get; private set; }

        public ActionConfigSearch ActionConfig { get; set; }
        public ChannelConfigSearch ChannelConfig { get; set; }

        public SearchViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_OItemSearch1.GUID, idInstance, userId)
        {
            fullHeight = nameof(fullHeight);

            ViewItems = new List<ViewItem>();

            rootContainer = new ViewItem
            {
                ViewItemId = nameof(rootContainer),
                ViewItemClass = ViewItemClass.Container.ToString()
            };
            ViewItems.Add(rootContainer);
            rootContainer.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);

            searchArea = new ViewItem
            {
                ViewItemId = nameof(searchArea),
                ViewItemClass = ViewItemClass.Container.ToString()
            };
            ViewItems.Add(searchArea);
            searchArea.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);

            searchCriteriaRow = new ViewItem
            {
                ViewItemId = nameof(searchCriteriaRow),
                ViewItemClass = ViewItemClass.Container.ToString()
            };
            ViewItems.Add(searchCriteriaRow);
            searchCriteriaRow.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);

            panelBar = new ViewItem
            {
                ViewItemId = nameof(panelBar),
                ViewItemClass = ViewItemClass.Container.ToString()
            };
            ViewItems.Add(panelBar);
            panelBar.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);

            resultPanel = new ViewItem
            {
                ViewItemId = nameof(resultPanel),
                ViewItemClass = ViewItemClass.Container.ToString()
            };
            ViewItems.Add(resultPanel);
            resultPanel.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);

            resultCount = new ViewItem
            {
                ViewItemId = nameof(resultCount),
                ViewItemClass = ViewItemClass.Badge.ToString()
            };
            ViewItems.Add(resultCount);
            resultCount.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);

            resultToolbar = new ViewItem
            {
                ViewItemId = nameof(resultToolbar),
                ViewItemClass = ViewItemClass.ToolBar.ToString()
            };
            ViewItems.Add(resultToolbar);
            resultToolbar.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);

            grid = new ViewItem
            {
                ViewItemId = nameof(grid),
                ViewItemClass = ViewItemClass.ToolBar.ToString()
            };
            ViewItems.Add(grid);
            grid.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            grid.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);

            applyItem = new ViewItem
            {
                ViewItemId = nameof(applyItem),
                ViewItemClass = ViewItemClass.ToolBar.ToString()
            };
            ViewItems.Add(applyItem);
            applyItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            applyItem.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);

            SearchItemList = new List<SearchItem>
            {
                new SearchItem
                {
                    id = "_firstElement",
                    add = true
                }
            };

            loadingTimeout = 500;

            ChannelConfig = new ChannelConfigSearch
            {
                ChannelAppliedObjects = Channels.LocalData.Object_AppliedObjects.GUID,
                ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID
            };
        }
    }

    public class ActionConfigSearch
    {
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetSearchRequestItem { get; set; }
        public string ActionRemoveSearchItemArea { get; set; }
        public string ActionSearchItemTextChanged { get; set; }
        public string ActionGetGridConfig { get; set; }
        public string ActionAddSearchItemArea { get; set; }
        public string ActionSearchClassChanged { get; set; }
        public string ActionSearchRelationTypeChanged { get; set; }
        public string ActionSearchAttributeTypeChanged { get; set; }
    }

    public class ChannelConfigSearch
    {
        public string ChannelSelectedObject { get; set; }
        public string ChannelAppliedObjects { get; set; }
    }
}