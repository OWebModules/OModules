﻿using OModules.Models.Base;
using OModules.Models.StateMachines;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class RelationTreeViewModel : OViewBaseModel
    {
        public string idView
        {
            get
            {
                return View.IdView;
            }
        }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem attributeEditWindow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem attributeEditIFrame { get; private set; }

        public string idListen { get; private set; }
        public object isEnabledListen { get; set; }
        public object isVisibleListen { get; set; }
        public bool _isToggledListen { get; set; }
        public object isToggledListen { get; set; }

        public string idNextOrderId { get; private set; }
        public object isEnabledNextOrderId { get; set; }
        public object isVisibleNextOrderId { get; set; }

        public string idSetNextOrderId { get; private set; }
        public object isEnabledSetNextOrderId { get; set; }
        public object isVisibleSetNextOrderId { get; set; }
        public bool _isToggledSetNextOrderId { get; set; }
        public object isToggledSetNextOrderId { get; set; }

        public string lblListenNode { get; private set; }
        public string idListenNode { get; private set; }
        public object isEnabledListenNode { get; set; }
        public object isVisibleListenNode { get; set; }
        public object idRefreshOtherBackward { get; set; }

        public string lblAddAttribute { get; private set; }
        public string idAddAttribute { get; private set; }
        public object isEnabledAddAttribute { get; set; }
        public object isVisibleAddAttribute { get; set; }

        public string idContextMenu { get; private set; }
        public object isVisibleContextMenu { get; set; }

        public string idTree { get; private set; }
        public object isEnabledTree { get; private set; }
        public object Tree { get; set; }
        public object treeItemsInit { get; set; }
        public object treeNodeAttribute { get; set; }
        public object treeNodeLeftRight { get; set; }
        public object treeNodeRightLeft { get; set; }
        public object treeNodeLeftRightOther { get; set; }
        public object treeNodeRightLeftOther { get; set; }

        public object selectedTreeNode { get; set; }
        public object listenTreeNode { get; set; }
        

        public string idToolbar { get; private set; }

        public string _idObject { get; set; }

        public object setIdObject { get; set; }

        public object checkViewState { get; set; }

        public object initialize { get; set; }
        public object treeViewDataBound { get; set; }
        public object treeViewChange { get; set; }

        public string ActionGetObject { get; set; }
        public string ActionGetTreeConfig { get; set; }
        public string ActionGetTreeItemsInit { get; set; }
        public string ActionGetTreeItemsAttribute { get; set; }
        public string ActionGetTreeItemsLeftRight { get; set; }
        public string ActionGetTreeItemsRightLeft { get; set; }
        public string ActionGetTreeItemsLeftRightOther { get; set; }
        public string ActionGetTreeItemsRightLeftOther { get; set; }
        public string ActionCheckListenNode { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionAttributeEditBool { get; set; }
        public string ActionAttributeEditDateTime { get; set; }
        public string ActionAttributeEditDouble { get; set; }
        public string ActionAttributeEditInt { get; set; }
        public string ActionAttributeEditString { get; set; }
        public string ActionGetAttributeUrl { get; set; }

        public string ActionObjectListInit { get; set; }


        public string ChannelSelectedRelationNode { get; set; }

        public string ChannelSelectedObject { get; set; }

        public string ChannelSelectedNode { get; set; }

        public string ChannelViewReady { get; set; }

        public string ChannelAppliedObject { get; set; }

        public string ChannelAddAttribute { get; set; }

        public string ChannelSelectedClassNode { get; set; }

        public RelationTreeStateMachine RelTreeStateMachine { get; private set; } = new RelationTreeStateMachine();

        public RelationTreeViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_RelationTree.GUID, idInstance, userId)
        {
            lblAddAttribute = "Add Attribute";
            idAddAttribute = "addAttribute";

            idListen = "isListen";

            lblListenNode = "Listen";
            idListenNode = "listenNode";
            idNextOrderId = "nextOrderId";
            idSetNextOrderId = "setNextOrderId";
            idTree = "relationTree";
            idToolbar = "relationToolBar";
            idContextMenu = "contextNode";
            idAddAttribute = "addAttribute";
            idRefreshOtherBackward = "refreshOtherBackward";

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelSelectedNode = Channels.LocalData.Object_SelectedRelationNode.GUID;
            ChannelSelectedRelationNode = Channels.LocalData.Object_SelectedRelationNode.GUID;
            ChannelViewReady = Channels.LocalData.Object_ViewReady.GUID;
            ChannelAppliedObject = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelAddAttribute = SpecialChannels.AddAttribute;
            ChannelSelectedClassNode = Channels.LocalData.Object_SelectedClassNode.GUID;
        }
    }
}