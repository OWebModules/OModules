﻿using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class AccountViewModel : OViewBaseModel
    {
        public ViewItem rootContainer { get; set; }
        public ViewItem contentContainer { get; set; }
        public ViewItem menu { get; set; }
        public ViewItem menuEntryPageFunc { get; set; }
        public ViewItem menuEntryLogOff { get; set; }

        public Dictionary<string, string> ViewItemCaptions { get; set; }

        public object setDataItems { get; set; }
        public object messageFromWebsocket { get; set; }

        public ActionConfigUserFunc ActionConfig {get; set;}

        public string fullHeight { get; private set; }

        public AccountViewModel(HttpSessionStateBase session, string idInstance, string idUser) : base(session, "36ec0cdd564041e88d8f21af0081ade3", idInstance, idUser)
        {
            fullHeight = nameof(fullHeight);

            ViewItems = new List<ViewItem>();

            rootContainer = new ViewItem
            {
                ViewItemId = nameof(rootContainer),
                ViewItemClass = ViewItemClass.Container.ToString()
            };
            ViewItems.Add(rootContainer);
            rootContainer.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            rootContainer.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);

            contentContainer = new ViewItem
            {
                ViewItemId = nameof(contentContainer),
                ViewItemClass = ViewItemClass.Container.ToString()
            };
            ViewItems.Add(contentContainer);
            contentContainer.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            contentContainer.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);

            menu = new ViewItem
            {
                ViewItemId = nameof(menu),
                ViewItemClass = ViewItemClass.Menu.ToString()
            };
            ViewItems.Add(menu);
            menu.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            menu.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);

            menuEntryPageFunc = new ViewItem
            {
                ViewItemId = nameof(menuEntryPageFunc),
                ViewItemClass = ViewItemClass.MenuEntry.ToString()
            };
            ViewItems.Add(menuEntryPageFunc);
            menuEntryPageFunc.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            menuEntryPageFunc.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);
            menuEntryPageFunc.ChangeViewItemValue(ViewItemType.Caption.ToString(), "User Functions");

            menuEntryLogOff = new ViewItem
            {
                ViewItemId = nameof(menuEntryLogOff),
                ViewItemClass = ViewItemClass.MenuEntry.ToString()
            };
            ViewItems.Add(menuEntryLogOff);
            menuEntryLogOff.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            menuEntryLogOff.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);
            menuEntryLogOff.ChangeViewItemValue(ViewItemType.Caption.ToString(), "Logout");

            ViewItemCaptions = new Dictionary<string, string>();
            foreach (var viewItem in ViewItems)
            {
                var caption = viewItem.ViewItemValues.FirstOrDefault(viewItm => viewItm.ViewItemType == ViewItemType.Caption.ToString());
                if (caption != null)
                {
                    ViewItemCaptions.Add(viewItem.ViewItemId, caption.Value.ToString());
                }
                
            }
        }
    }

    public class ActionConfigUserFunc
    {
        public string ActionLogOff { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
    }
}