﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ObjectTreeViewModel : OViewBaseModel
    {
        public string fullHeight { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem controlArea { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem treeToolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem objectTreeView { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container, NoDefaultValueInit = true)]
        public ViewItem menu { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container, NoDefaultValueInit = true)]
        public ViewItem addSubItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.KendoDropDownList, NoDefaultValueInit = true)]
        public ViewItem dropdRelationType { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem inpFilter { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem isToggledNextId { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem applyItems { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem classItem { get; private set; }

        public object setDataItems { get; set; }

        public bool IsClass { get; set; }

        public List<KendoTreeNode> TreeNodes { get; set; } = new List<KendoTreeNode>();

        public string ActionGetRelationTypeDropDownConfig { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetTreeConfig { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionEditOItem { get; set; }
        public string ActionSaveRelations { get; set; }
        public string ActionSearchTree { get; set; }

        public string ActionObjectTreeInit { get; set; }

        public string ActionObjectListInit { get; set; }

        public string ChannelSelectedClass { get; private set; }
        public string ChannelSelectedObject { get; private set; }
        public string ChannelAppliedObjects { get; private set; }
        public string ChannelAddedObjects { get; private set; }

        public OItemWithParent OItemWithParent { get; set; }

        public clsOntologyItem RelationType { get; set; }

        public ObjectTreeViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ObjectTree1.GUID, idInstance, userId)
        {
            fullHeight = nameof(fullHeight);

            addSubItem.ChangeViewItemValue(ViewItemType.Caption.ToString(), "Add...");

            ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID;
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObjects = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelAddedObjects = Channels.LocalData.Object_AddedObjects.GUID;

        }
    }
}