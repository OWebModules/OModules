﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ResultSimpleView<T>
    {
        public bool IsSuccessful { get; set; }
        public T Result { get; set; }
        public List<ViewItem> ViewItems { get; set; }
        public string Message { get; set; }

        public List<string> SelectorPath { get; set; }

        public List<object> DataItems { get; set; } = new List<object>();

        public ResultSimpleView()
        {
            ViewItems = new List<ViewItem>();
        }
    }
}