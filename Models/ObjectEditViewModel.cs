﻿using OModules.Models.Base;
using OModules.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ObjectEditViewModel : OViewBaseModel
    {
        public string fullHeight { get; private set; }

        public string idViewRelationTree => WebViews.Config.LocalData.Object_ObjectRelationTree.GUID;

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Id")]
        public ViewItem lblGuid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem guidInput { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Name")]
        public ViewItem lblName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem nameInput { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Splitter)]
        public ViewItem treeRestSplitter { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Splitter)]
        public ViewItem objectRelation { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Splitter)]
        public ViewItem tabStrip { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem pageAttributes { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem pageLeftRight { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem pageRightLeft { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem pageLeftRightOther { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem pageRightLeftOther { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem pageOmni { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem windowContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem windowAttribute { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem pageAttributeEdit { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem deleteItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem applyItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem addToClipboard { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem addToClipboardRelationEditor { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem deleteObject { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem selectObject { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem applyObject { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblOrderId { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.NumericInput)]
        public ViewItem inpOrderId { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem orderAsc { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem orderDesc { get; private set; }

        public object tabStripElement { get; set; }

        public object clickedButton { get; set; }

        public object windowAttributeItem { get; set; }

        public InterServiceMessage messageObjectRelationTree { get; set; }
        public InterServiceMessage messageObjectOmni { get; set; }
        public InterServiceMessage messageRelationNodeAttributes { get; set; }
        public InterServiceMessage messageRelationNodeLeftRight { get; set; }
        public InterServiceMessage messageRelationNodeRightLeft { get; set; }
        public InterServiceMessage messageRelationNodeLeftRightOther { get; set; }
        public InterServiceMessage messageRelationNodeRightLeftOther { get; set; }


        public object setDataItems { get; set; }
        public object messageFromWebsocket { get; set; }

        public object onActivateTabPage { get; set; }

        public ActionConfigObjectEdit ActionConfig { get; set; }
        public ChannelConfigObjectEdit ChannelConfig { get; private set; } 


        public ObjectEditViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_RelationEditor.GUID, idInstance, userId)
        {

            fullHeight = nameof(fullHeight);

            isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);

            pageLeftRight.ChangeViewItemValue(ViewItemType.Active.ToString(), true);

            ChannelConfig = new ChannelConfigObjectEdit();

            messageObjectOmni = new InterServiceMessage
            {
                ChannelId = Channels.LocalData.Object_SelectedObject.GUID,
                IsInactive = true
            };
            messageObjectRelationTree = new InterServiceMessage
            {
                ChannelId = Channels.LocalData.Object_SelectedObject.GUID
            };
            messageRelationNodeAttributes = new InterServiceMessage
            {
                ChannelId = Channels.LocalData.Object_SelectedRelationNode.GUID,
                IsInactive = true
            };
            messageRelationNodeLeftRight = new InterServiceMessage
            {
                ChannelId = Channels.LocalData.Object_SelectedRelationNode.GUID
            };
            messageRelationNodeLeftRightOther = new InterServiceMessage
            {
                ChannelId = Channels.LocalData.Object_SelectedRelationNode.GUID,
                IsInactive = true
            };
            messageRelationNodeRightLeft = new InterServiceMessage
            {
                ChannelId = Channels.LocalData.Object_SelectedRelationNode.GUID,
                IsInactive = true
            };
            messageRelationNodeRightLeftOther = new InterServiceMessage
            {
                ChannelId = Channels.LocalData.Object_SelectedRelationNode.GUID,
                IsInactive = true
            };

        }

        public class ActionConfigObjectEdit
        {
            public string ActionConnected { get; set; }
            public string ActionDisconnected { get; set; }
            public string ActionRelationTree { get; set; }
            public string ActionObjectAttribute { get; set; }
            public string ActionObjectLeftRight { get; set; }
            public string ActionObjectRightLeft { get; set; }
            public string ActionObjectLeftRightOther { get; set; }
            public string ActionObjectRightLeftOther { get; set; }
            public string ActionObjectOmni { get; set; }
            public string ActionValidateReference { get; set; }
            public string ActionGetTreeConfigRelationTree { get; set; }
            public string ActionGetAttributeUrlRelationTree { get; set; }
            public string ActionGetTreeItemsRightLeftOtherRelationTree { get; set; }
            
            public string ActionRelationEditor { get; set; }

            public string ActionModuleStarter { get; set; }


            public string ActionObjectAttributeListInit { get; set; }
            public string ActionGetGridConfigObjectRelationsAttribute { get; set; }

            public string ActionComponentObjectRelationLeftRightList { get; set; }
            public string ActionGetGridConfigObjectRelationsLeftRight { get; set; }

            public string ActionComponentObjectRelationRightLeftList { get; set; }
            public string ActionGetGridConfigObjectRelationsRightLeft { get; set; }

            public string ActionComponentObjectRelationLeftRightListOther { get; set; }
            public string ActionGetGridConfigObjectRelationsLeftRightListOther { get; set; }

            public string ActionComponentObjectRelationRightLeftListOther { get; set; }
            public string ActionGetGridConfigObjectRelationsRightLeftListOther { get; set; }

            public string ActionComponentObjectRelationOmni { get; set; }
            public string ActionGetGridConfigObjectRelationsOmni { get; set; }

            public string ActionCheckListenNode { get; set; }

            public string ActionSetSelectedItem { get; set; }
            public string ActionSetOrderId { get; set; }

            public string ActionAddToClipboard { get; set; }

            public string ActionRelationTreeInit { get; set; }

            public string ActionClipboardInit { get; set; }

            public string ActionEmbeddedNameEditInit { get; set; }

            public string ActionHasRelations { get; set; }

            public string ActionDeleteObjects { get; set; }

        }

        public class ChannelConfigObjectEdit
        {
            public string ChannelSelectedObject { get; set; }
            public string ChannelViewLoaded { get; set; }
            public string ChannelSelectedRelationNode { get; set; }
            public string ChannelSelectedClasssNode { get; set; }
            public string ChannelAddAttribute { get; set; }
            public string ChannelAppliedObjects { get; set; }
            public string ChannelAppliedAttributeTypes { get; set; }
            public string ChannelAppliedRelationTypes { get; set; }
            public string ChannelAppliedClasses { get; set; }

            public string ChannelAddedObjects { get; set; }

            public ChannelConfigObjectEdit()
            {
                ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
                ChannelViewLoaded = Channels.LocalData.Object_ViewReady.GUID;
                ChannelSelectedRelationNode = Channels.LocalData.Object_SelectedRelationNode.GUID;
                ChannelSelectedClasssNode = Channels.LocalData.Object_SelectedClassNode.GUID;
                ChannelAddAttribute = Channels.LocalData.Object_AddAttribute.GUID;
                ChannelAppliedObjects = Channels.LocalData.Object_AppliedObjects.GUID;
                ChannelAppliedClasses = Channels.LocalData.Object_AppliedClasses.GUID;
                ChannelAppliedAttributeTypes = Channels.LocalData.Object_AppliedAttributeTypes.GUID;
                ChannelAppliedRelationTypes = Channels.LocalData.Object_AppliedRelationTypes.GUID;
                ChannelAddedObjects = Channels.LocalData.Object_AddedObjects.GUID;
            }
        }
    }
}