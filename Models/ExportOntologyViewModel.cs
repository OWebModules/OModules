﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ExportOntologyViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Reference:")]
        public ViewItem lblReference { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "-")]
        public ViewItem referenceName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem outputOntology { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem exportOntology { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionExportOntology { get; set; }

        public string ChannelSelectedObject { get; set; }

        public clsOntologyItem RefItem { get; set; }
        public clsOntologyItem RefItemParent { get; set; }

        public ExportOntologyViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ExportOntology.GUID, idInstance, userId)
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;

            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Export Ontology");
        }
    }
}