﻿using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ImportOntologyAssemblyViewModel : OViewBaseModel
    {


        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Reference:")]
        public ViewItem lblReference { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "-")]
        public ViewItem referenceName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem outputResult { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.KendoDropDownList)]
        public ViewItem uploadContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.FileUpload, ViewItemId = "files")]
        public ViewItem fileItem { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionUpload { get; set; }

        public ImportOntologyAssemblyViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ImportOntologyAssembly.GUID, idInstance, userId)
        {

        }
    }
}