﻿using CommandLineRunModule.Models;
using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class CommandLineRunViewerViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem scriptParsed { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem scriptRaw { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblProgramingLanguage { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpProgramingLanguage { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblEncoding { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpEncoding { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblCodeParsed { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblCodeRaw { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Splitter)]
        public ViewItem splitter { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem objectTree { get; private set; }

        public string idClassCommandLineRun { get; set; }

        public clsOntologyItem refItem { get; set; }
        public clsOntologyItem refParentItem { get; set; }

        public ReportDataItem reportDataItem { get; set; }

        

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionObjectTreeInit { get; set; }

        public string ActionCheckReportDataFromSessionFile { get; set; }

        public string ChannelSelectedObject { get; set; }

        public string ChannelProcessReport { get; set; }

        public CommandLineRunViewerViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_CommandLineRunViewer.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Commandline Run Viewer");

            lblName.ChangeViewItemValue(ViewItemType.Content.ToString(), "Command Line Run:");
            lblProgramingLanguage.ChangeViewItemValue(ViewItemType.Content.ToString(), "Programming-Language:");
            lblEncoding.ChangeViewItemValue(ViewItemType.Content.ToString(), "Encoding:");
            lblCodeParsed.ChangeViewItemValue(ViewItemType.Content.ToString(), "Code (parsed):");
            lblCodeRaw.ChangeViewItemValue(ViewItemType.Content.ToString(), "Code (raw):");

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelProcessReport = SpecialChannels.ProcessReport;
        }
    }
}