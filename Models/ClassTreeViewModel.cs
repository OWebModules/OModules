﻿using OModules.Models.Base;
using OModules.Primitives;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Web;

namespace OModules.Models
{
    public class ClassTreeViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem searchBar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.BreadCrumbItem)]
        public ViewItem breadCrumbSearch { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.BreadCrumbItem)]
        public ViewItem breadCrumbItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.BreadCrumb)]
        public ViewItem breadCrumb { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ListBox)]
        public ViewItem classList { get; private set; }

        public object setDataItems { get; set; }
        public object createTree { get; set; }
        public object messageFromWebsocket { get; set; }
        public object connectHandler { get; set; }
        public object disconnectHandler { get; set; }

        public ChannelConfigClassTree ChannelConfig
        {
            get; set;
        }
        public ActionConfigClassTree ActionConfig
        {
            get; set;
        }

        public ClassTreeViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ClassTree2.GUID, idInstance, userId)
        {
           
            
            
            breadCrumb.ChangeViewItemValue(ViewItemType.Template.ToString(), $"<ol class=\"breadcrumb\"></ol>");

            breadCrumbItem.ChangeViewItemValue(ViewItemType.Template.ToString(), "<li class=\"breadcrumb-item active\" id=\"{ID}\">{CONTENT}</li>");

            breadCrumbSearch.ChangeViewItemValue(ViewItemType.Template.ToString(), $"<li class=\"breadcrumb-item\"><input type=\"text\" class=\"form-control\" id=\"{breadCrumbSearch.ViewItemId}\" placeholder=\"{ Messages.ClassTree_Label_SearchItem }\"></li>");

            classList.ChangeViewItemValue(ViewItemType.Template.ToString(), "<div><button id='#:Id#' class='btn-link'>#:Name# <span class='label #if (ChildCount > 0) {#label-primary#} else {#label-default#}#'>#:ChildCount#</span></button></div>");

            ActionConfig = new ActionConfigClassTree();
            ChannelConfig = new ChannelConfigClassTree();
        }

        
    }

    public class ActionConfigClassTree
    {
        public string SelectedClass { get; set; }
        public string GetClassPath { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }

        public string ActionInit { get; set; }

    }

    public class ChannelConfigClassTree
    {
        public string ChannelSelectedClass { get; private set; }
        public string ChannelAppliedClass { get; private set; }
        public string ChannelViewReady { get; private set; }

        public ChannelConfigClassTree()
        {
            ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID;
            ChannelAppliedClass = Channels.LocalData.Object_AppliedClasses.GUID;
            ChannelViewReady = Channels.LocalData.Object_ViewReady.GUID;
        }
    }
}