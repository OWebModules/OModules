﻿using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System.Collections.Generic;

namespace OModules.Models
{
    public class ResultGetAttribute
    {
        public bool isOk { get; set; }
        public string Message { get; set; }
        public clsObjectAtt ObjectAttribute { get; set; }
        public clsOntologyItem Object { get; set; }
        public clsOntologyItem AttributeType { get; set; }
        public ViewItem ViewItem { get; set; }

        public List<string> SelectorPath { get; set; } = new List<string>();
    }
}