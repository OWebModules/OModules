﻿using HtmlEditorModule.Models;
using LocalizationModule.Models;
using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class HtmlSearchViewModel : OViewBaseModel
    {
        public string fullHeight { get; private set; }

        
        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem searchContainer { get; private set; }
        
        public string SearchString { get; set; }
        public List<HtmlSearchResultItem> SearchResultItems { get; set; } = new List<HtmlSearchResultItem>();

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetGridConfigSearch { get; set; }


        public string ChannelSelectedObject { get; set; }

        public HtmlSearchViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_HtmlSearch.GUID, idInstance, userId)
        {

            fullHeight = nameof(fullHeight);
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;


            

        }
    }


}