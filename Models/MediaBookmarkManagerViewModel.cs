﻿using MediaViewerModule.Models;
using OModules.Models.Base;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TypedTaggingModule.Models;

namespace OModules.Models
{
    

    public class MediaBookmarkManagerViewModel : OViewBaseModel
    {
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionItemComToolBarInit { get; set; }
        public string ActionMediaBookmarkManagerInit { get; set; }
        public string ActionVideoPlayerInit { get; set; }

        public string ActionObjectListLinit { get; set; }

        public string ActionCreateMediaBookmark { get; set; }

        public string ActionDeleteMediaBookmark { get; set; }

        public string ActionRelateBookmark { get; set; }

        public string ActionRemoveReference { get; set; }

        public string ActionInitClassTree { get; set; }

        public clsOntologyItem MediaItem { get; set; }
        public clsOntologyItem RefItem { get; set; }

        public List<MediaBookmark> MediaBookmarks { get; set; } = new List<MediaBookmark>();

        public string ChannelSelectedObject { get; private set; }
        public string ChannelAppliedObjects { get; private set; }
        public string ChannelViewReady { get; private set; }

        public string ChannelSelectedClass { get; private set; }

        public string ChannelControllerMessage { get; private set; }

        public string mediaType { get; set; }

        public clsOntologyItem logstatePosition { get; set; }

        public MediaBookmarkManagerViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_MediaBookmarkManagerVideo.GUID, idInstance, userId)
        {

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObjects = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelViewReady = Channels.LocalData.Object_ViewReady.GUID;
            ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID;
            ChannelControllerMessage = SpecialChannels.ControllerMessage;
        }

    }
}