﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class LogFunctionsViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem listen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Reference:")]
        public ViewItem lblReference { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "-")]
        public ViewItem referenceName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem listView { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem pager { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container, NoDefaultValueInit = true)]
        public ViewItem toolbar { get; private set; }

        
        [ViewModel(ViewItemClass = ViewItemClass.DateTimeInput)]
        public ViewItem datepTimeStamp { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem filter { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Description:")]
        public ViewItem lblDescription { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem message { get; private set; }

        public clsOntologyItem refItem { get; set; }
        public clsOntologyItem refParentItem { get; set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionGetDataSourceLogStates { get; set; }
        public string ActionLogReference { get; set; }

        public string ActionLogListInit { get; set; }

        public string ChannelExecutedFunction { get; set; }
        public string ChannelSelectedObject { get; set; }

        public LogFunctionsViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_LogFunctions.GUID, idInstance, userId)
        {
            message.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            filter.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            datepTimeStamp.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            ChannelExecutedFunction = SpecialChannels.ExecutedFunction;
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}