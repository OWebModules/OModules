﻿using OModules.Models.Base;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeManagementModule.Models;

namespace OModules.Models
{
    public class TimeManagementEntryEditViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem buttonListenStart { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.DateTimeInput)]
        public ViewItem inpStart { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem buttonListenEnde { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.DateTimeInput)]
        public ViewItem inpEnde { get; private set; }


        [ViewModel(ViewItemClass = ViewItemClass.Radio)]
        public ViewItem radioWork { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Radio)]
        public ViewItem radioPrivate { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Radio)]
        public ViewItem radioUrlaub { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Radio)]
        public ViewItem radioKrankheit { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblRelated { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpRelated { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonListenRelated { get; private set; }
        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonRemoveRelated { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonSave { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.KendoDropDownList)]
        public ViewItem inpGroup { get; private set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionItemComBarInit { get; set; }

        public string ActionTimeManagementEntryEditInit { get; set; }

        public string ActionTimeManagementListInit { get; set; }

        public string ActionGetDropDownConfigGroups { get; set; }

        public string ActionSaveTimeManagementEntry { get; set; }

        public string ActionInitClassTree { get; set; }

        public string ActionObjectListLinit { get; set; }

        public ClassObject ClassObject { get; set; }

        public clsOntologyItem StateWork { get; set; }
        public clsOntologyItem StatePrivate { get; set; }
        public clsOntologyItem StateUrlaub { get; set; }
        public clsOntologyItem StateKrankheit { get; set; }

        public clsOntologyItem UserItem { get; set; }
        public clsOntologyItem GroupItem { get; set; }

        public string idClassGroup { get; set; }

        public DateTime LastEnd { get; set; }

        public TimeManagementEntry SelectedEntry { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string ChannelAppliedObject { get; set; }

        public string ChannelControllerMessage { get; set; }

        public TimeManagementEntryEditViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_TimeManagementEntryEdit.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Zeiterfassungs-Edit");

            lblName.ChangeViewItemValue(ViewItemType.Content.ToString(), "Name:");
            buttonListenStart.ChangeViewItemValue(ViewItemType.Content.ToString(), "Start:");
            buttonListenStart.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            buttonListenEnde.ChangeViewItemValue(ViewItemType.Content.ToString(), "Ende:");
            buttonListenEnde.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            radioWork.ChangeViewItemValue(ViewItemType.Caption.ToString(), "Work");
            radioPrivate.ChangeViewItemValue(ViewItemType.Caption.ToString(), "Private");
            radioUrlaub.ChangeViewItemValue(ViewItemType.Caption.ToString(), "Urlaub");
            radioKrankheit.ChangeViewItemValue(ViewItemType.Caption.ToString(), "Krankheit");

            lblRelated.ChangeViewItemValue(ViewItemType.Content.ToString(), "Related:");

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObject = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelControllerMessage = SpecialChannels.ControllerMessage;

            LastEnd = DateTime.Now;
        }
    }
}