﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ScriptEditorViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem listen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem script { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionGetCodeSnipplet { get; set; }

        public string ChannelSelectedObject { get; set; }

        public clsOntologyItem refItem { get; set; }
        public clsOntologyItem refParentItem { get; set; }

        public ScriptEditorViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ScriptEditor.GUID, idInstance, userId)
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}