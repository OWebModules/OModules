﻿using BillModule.Models;
using BillModule.Services;
using OModules.Models.Base;
using OntologyAppDBConnector.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    

    public class TransactionDetailViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Splitter)]
        public ViewItem splitter { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Window)]
        public ViewItem newTransactionWindow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem objectListFrame { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem mewObjectFrame { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openNewTransaction { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openTransactionList { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openPdfList { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblTransactionName{ get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inpTransactionName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label )]
        public ViewItem lblGross { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.CheckBox, NoDefaultValueInit = true)]
        public ViewItem chkGross { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblPay { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.NumericInput, NoDefaultValueInit = true, SpecialType = "System.Double")]
        public ViewItem inpPay { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblTransactionDate { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.DateTimeInput, NoDefaultValueInit = true)]
        public ViewItem dateTransactionDate { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblTransactionId { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inpTransactionId { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem btnBeleg { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblCurrency { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.KendoDropDownList, NoDefaultValueInit = true)]
        public ViewItem inpCurrency { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblTaxRate { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.KendoDropDownList, NoDefaultValueInit = true)]
        public ViewItem inpTaxRate { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblAmount { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.NumericInput, NoDefaultValueInit = true, SpecialType = "System.Double")]
        public ViewItem inpAmount { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.KendoDropDownList, NoDefaultValueInit = true)]
        public ViewItem inpUnit { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblVertragsgeber { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inpVertragsgeber { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button, NoDefaultValueInit = true)]
        public ViewItem btnListenVertragsnehmer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblVertragsnehmer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inpVertragsnehmer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button, NoDefaultValueInit = true)]
        public ViewItem btnListenVertragsgeber { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.DataItem, NoDefaultValueInit = true)]
        public ViewItem dataItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem status { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionGetGridConfig { get; set; }
        public string ActionGetPDFListUrl { get; set; }
        public string ActionGetDropDownConfigTaxrates { get; set; }
        public string ActionGetDropDownConfigUnits { get; set; }
        public string ActionGetDropDownConfigCurrencies { get; set; }
        public string ActionAppliedObject { get; set; }
        public string ActionGetTransactionData { get; set; }

        public string ActionObjectList { get; set; }

        public string ActionNewTransaction { get; set; }

        public string idClassTaxRates { get; set; }
        public string idClassUnits { get; set; }
        public string idClassCurrency { get; set; }
        public string idClassPartner { get; set; }

        public ClassObject ClassObject { get; set; }
        public TransactionItem transactionItem { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string ChannelAppliedObject { get; set; }
        public string ChannelSelectedClass { get; set; }

        public TransactionDetailViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_TransactionDetail.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Transaction-Detail");
            
            lblCurrency.ChangeViewItemValue(ViewItemType.Content.ToString(), "Währung:");
            inpCurrency.ChangeViewItemValue(ViewItemType.PlaceHolder.ToString(), "Währung");
            inpCurrency.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            lblGross.ChangeViewItemValue(ViewItemType.Content.ToString(), "Brutto:");
            chkGross.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            lblPay.ChangeViewItemValue(ViewItemType.Content.ToString(), "Betrag:");
            inpPay.ChangeViewItemValue(ViewItemType.Content.ToString(), "Betrag");
            inpPay.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            lblTaxRate.ChangeViewItemValue(ViewItemType.Content.ToString(), "Steuerrate:");
            inpTaxRate.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            lblTransactionDate.ChangeViewItemValue(ViewItemType.Content.ToString(), "Transaktionsdatum:");
            dateTransactionDate.ChangeViewItemValue(ViewItemType.PlaceHolder.ToString(), "Transaktionsdatum");
            dateTransactionDate.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            lblTransactionId.ChangeViewItemValue(ViewItemType.Content.ToString(), "Id:");
            inpTransactionId.ChangeViewItemValue(ViewItemType.PlaceHolder.ToString(), "Id");
            inpTransactionId.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            lblTransactionName.ChangeViewItemValue(ViewItemType.Content.ToString(), "Bezeichnung:");
            inpTransactionName.ChangeViewItemValue(ViewItemType.PlaceHolder.ToString(), "Bezeichnung");
            inpTransactionName.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            lblAmount.ChangeViewItemValue(ViewItemType.Content.ToString(), "Menge:");
            inpAmount.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            inpUnit.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            lblVertragsgeber.ChangeViewItemValue(ViewItemType.Content.ToString(), "Vertragsgeber:");
            inpVertragsgeber.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            btnListenVertragsgeber.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            lblVertragsnehmer.ChangeViewItemValue(ViewItemType.Content.ToString(), "Vertragsnehmer:");
            inpVertragsnehmer.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            btnListenVertragsnehmer.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObject = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID;
        }
    }
}