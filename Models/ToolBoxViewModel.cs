﻿using AutomationLibrary.Models;
using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ToolBoxViewModel : OViewBaseModel
    {
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }


        public List<KendoModuleItem> ModuleItems { get; set; } = new List<KendoModuleItem>();

        public string ActionBatchGridInit { get; set; }
        public string ActionModuleExecutionLogInit { get; set; }
        public string ActionModuleExecutionPropertiesInit { get; set; }

        public ToolBoxViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ToolBox.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Toolbox");
        }
    }
}