﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class VideoPlayerViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem mediaContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem videoPlayer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem playlistArea { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ListBox)]
        public ViewItem playlist { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem listen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton, NoDefaultValueInit = true)]
        public ViewItem isCheckedAutoPlay { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton, NoDefaultValueInit = true)]
        public ViewItem bookmark { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblBookmark { get; private set; }

        public ActionConfigVideoPlayer ActionConfig { get; set; }
        public ChannelConfigVideoPlayer ChannelConfig { get; private set; }

        public clsOntologyItem RefItem { get; set; }

        public VideoPlayerViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_VideoPlayer.GUID, idInstance, userId)
        {
            ChannelConfig = new ChannelConfigVideoPlayer();
        }

        
    }
    public class ActionConfigVideoPlayer
    {
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetSessionDataUrl { get; set; }

        public string ActionVideoPlayerInit { get; set; }
    }

    public class ChannelConfigVideoPlayer
    {
        public string ChannelSelectedObject { get; set; }
        public string ChannelViewReady { get; set; }
        public string ChannelMediaList { get; set; }

        public ChannelConfigVideoPlayer()
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelViewReady = Channels.LocalData.Object_ViewReady.GUID;
            ChannelMediaList = SpecialChannels.MediaList;
        }
    }

}