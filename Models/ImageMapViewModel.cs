﻿using LearningModule.Models;
using MediaViewerModule.Models;
using OModules.Models.Base;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ImageMapViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionImageMapInit { get; set; }


        public string ChannelSelectedObject { get; set; }
        public string ChannelControllerMessage { get; set; }

        public ClassObject ClassObject { get; set; }

        public ImageMap ImageMapItem { get; set; }


        public ImageMapViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ImageMap.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Image-Map");

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelControllerMessage = SpecialChannels.ControllerMessage;
        }
    }
}