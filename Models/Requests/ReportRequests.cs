﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.Requests
{
    public class GetDataRequest: ApiRequest
    {
        public string idReport { get; set; }
    }
}