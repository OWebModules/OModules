﻿using OModules.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.Requests
{
    public class ModuleMgmtApiSendRequest : ApiRequest
    {
        public InterServiceMessage message { get; set; }
    }
}