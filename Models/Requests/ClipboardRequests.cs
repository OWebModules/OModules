﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.Requests
{
    public class GetClipboardItemsRequest : ApiRequest
    {
        public string idReference { get; set; }
    }

    public class ClearClipboardRequest: ApiRequest
    {
        public string idRef { get; set; }
    }

    public class AddClipboardItems : ApiRequest
    {
        public List<clsOntologyItem> OItems { get; set; }
    }
}