﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.Requests
{
    public class SelectedORelItemRequest
    {
        public string uid { get; set; }
        public string IdInstance { get; set; }
        public string IdItem { get; set; }
        public string IdObject { get; set; }
        public string IdOther { get; set; }
        public string IdRelationType { get; set; }

        public long OrderId { get; set; }
    }
}