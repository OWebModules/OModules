﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.Requests
{
    public abstract class ApiRequest
    {
        public string idSession { get; set; }
        public string idInstance { get; set; }
    }
}