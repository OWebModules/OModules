﻿using BillModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.Requests
{
    public class GetTransactoinsRequest : ApiRequest
    {
        public string idMandant { get; set; }
        public DateTime? start { get; set; }
        public DateTime? end { get; set; }
    }


    public class GetTransactionDataRequest : ApiRequest
    {
        public string idFinancialTransaction { get; set; }
    }

    public class SaveTransactionItemRequest: ApiRequest
    {
        public List<TransactionItem> TransactionItems { get; set; }
    }
}