﻿using AppointmentModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.Requests
{
    public class AppointmentRequestSaveAppointment: ApiRequest
    {
        public AppointmentScheduler schedulerEvent { get; set; }
    }
}