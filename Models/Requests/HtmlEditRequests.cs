﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.Requests
{
    public class GetHtmlContentRequest : ApiRequest
    {
        public string idReference { get; set; }
    }
}