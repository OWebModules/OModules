﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.Requests
{
    public class RequestObjects : ApiRequest
    {
        public string idParent { get; set; }
        public string nameObject { get; set; }
    }

    public class RequestAttributeTypes : ApiRequest
    {
        public string idAttributeType { get; set; }
        public string nameAttributeType { get; set; }
        public string idDataType { get; set; }
    }

    public class RequestRelationTypes : ApiRequest
    {
        public string idRelationType { get; set; }
        public string nameRelationType { get; set; }
    }

    public class RequestObjectRelations : ApiRequest
    {
        public string idLeft { get; set; }
        public string nameLeft { get; set; }
        public string idRight { get; set; }
        public string nameRight { get; set; }
        public string idRelationType { get; set; }
        public string nameRelationType { get; set; }
        public string nodeType { get; set; }
    }
}