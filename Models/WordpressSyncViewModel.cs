﻿using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordPress_Module;
using WordPressSyncModule.Models;

namespace OModules.Models
{
    public class WordpressSyncViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Webservices")]
        public ViewItem webserviceLabel { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.DropDownList, NoDefaultValueInit = true)]
        public ViewItem webserviceDropdown { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Url")]
        public ViewItem baseUrlLabel { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem baseUrlInp { get; private set; }
        

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Password")]
        public ViewItem passwordLabel { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Username")]
        public ViewItem userNameLabel { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem userNameInp { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Web-Logs")]
        public ViewItem blogsLabel { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.DropDownList, NoDefaultValueInit = true)]
        public ViewItem blogsDropdown { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem passwordInp { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button, DefaultContent = "Get Blogs")]
        public ViewItem buttonGetBlogs { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button, DefaultContent = "Get Posts")]
        public ViewItem buttonPostList { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container, NoDefaultValueInit = true)]
        public ViewItem authenticationWindow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar, DefaultEnable = true)]
        public ViewItem toolbarAuthentication { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inputAuthenticationPassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem alertWrongPassword { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonOkAuthentication { get; private set; }

        public List<WebServiceItem> WebServices { get; set; }

        public WebServiceItem SelectedWebService { get; set; }

        public List<UserBlog> UserBlogs { get; set; }

        public UserBlog SelectedUserBlog { get; set; }

        public List<GridPostItem> GridPostItems { get; set; }

        public string MasterPassword { get; set; }

        public int defaultWidth { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetGridConfig { get; set; }
        public string ActionGetPostItems { get; set; }
        public string ActionGetUsernameAndPassword { get; set; }
        public string ActionValidatePassword { get; set; }
        public string ActionGetUserBlogs { get; set; }

        public WordpressSyncViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_WordPressSync.GUID, idInstance, userId)
        {
            defaultWidth = 300;
        }
    }
}