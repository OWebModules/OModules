﻿using AutomationLibrary.Models;
using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace OModules.Models
{
    public class ModuleExecutionPropertiesViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.KendoDropDownList)]
        public ViewItem configurationDropdown { get; private set; }


        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem saveConfiguration { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem deleteConfiguration { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem executeConfiguration { get; private set; }


        public List<ModuleController> ModuleItems { get; set; } = new List<ModuleController>();
        public List<ModuleConfiguration> ModuleConfigurations { get; set; } = new List<ModuleConfiguration>();

        public string ActionGetDropDownConfig { get; set; }
        public string ActionJsonEditorInit { get; set; }

        public string ActionExecute { get; set; }



        public ModuleExecutionPropertiesViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ExecutionProperties.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Execution-Properties");
        }
    }
}
