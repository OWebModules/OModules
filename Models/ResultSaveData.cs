﻿using OntoWebCore.Models;
using System.Collections.Generic;

namespace OModules.Models
{
    public class ResultSaveData
    {
        public bool isOk { get; set; }
        public string Message { get; set; }
        public List<EditItem> SaveData { get; set; }
    }
}