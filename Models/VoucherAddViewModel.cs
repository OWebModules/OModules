﻿using BillModule.VoucherParser;
using OModules.Models.Base;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class VoucherAddViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem voucherAddContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.TextArea)]
        public ViewItem voucherInput { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem newVoucherGrid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ButtonGroup)]
        public ViewItem buttonGroupSelectedVoucherParser { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonSave { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonClear { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem buttonParse { get; private set; }

        public ClassObject RefItem { get; set; }

        public List<IVoucherParser> VoucherParserList { get; set; } = new List<IVoucherParser>();

        public string ChannelSelectedObject { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetGridConfig { get; set; }

        public string ActionVoucherAddInit { get; set; }

        public VoucherAddViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_VoucherAdd.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Add Voucher");
            buttonGroupSelectedVoucherParser.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            voucherInput.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}