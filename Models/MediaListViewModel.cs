﻿using MediaViewerModule.Models;
using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class MediaListViewModel : OViewBaseModel
    {
        public string mediaListPDFViewId
        {
            get { return WebViews.Config.LocalData.Object_MediaListPDF.GUID; }
        }
        public string mediaListAudioViewId
        {
            get { return WebViews.Config.LocalData.Object_MediaListAudio.GUID; }
        }
        public string mediaListVideoViewId
        {
            get { return WebViews.Config.LocalData.Object_MediaListVideo.GUID; }
        }
        public string mediaListImagesViewId
        {
            get { return WebViews.Config.LocalData.Object_MediaListImage.GUID; }
        }

        public string audioPlayerViewId
        {
            get { return WebViews.Config.LocalData.Object_AudioPlayer.GUID; }
        }
        

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem uploadMediaWindow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem uploadMediaWindowHeader { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem uploadMediaWindowContent { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem iframeUpload { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem playerWindow { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem playerWindowHeader { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem playerWindowContent { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem iframePlayer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem gridParent { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem uploadItems { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem removeFromList { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem applyItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openPlayer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem downloadMediaList { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem nameOItem { get; private set; }

        public ActionConfigMediaList ActionConfig { get; set; }
        public ChannelConfigMediaList ChannelConfig { get; private set; }

        public MediaViewerModule.Primitives.MultimediaItemType MediaType { get; private set; }

        public OItemWithParent ObjectWithParent { get; set; }

        public List<MediaListItem> mediaList { get; set; } = new List<MediaListItem>();

        public MediaListViewModel(HttpSessionStateBase session, string idInstance, string idView, string userId) : base(session, idView, idInstance, userId)
        {

            ChannelConfig = new ChannelConfigMediaList();
            MediaType = MediaViewerModule.Primitives.MultimediaItemType.Audio;
            if (idView == mediaListPDFViewId)
            {
                MediaType = MediaViewerModule.Primitives.MultimediaItemType.PDF;
            }
            else if (idView == mediaListVideoViewId)
            {
                MediaType = MediaViewerModule.Primitives.MultimediaItemType.Video;
            }
            else if (idView == mediaListImagesViewId)
            {
                MediaType = MediaViewerModule.Primitives.MultimediaItemType.Image;
            }

        }
    }

    public class ActionConfigMediaList
    {
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetGridConfig { get; set; }
        public string ActionFileUpload { get; set; }
        public string ActionAudioPlayer { get; set; }
        public string ActionVideoPlayer { get; set; }
        public string ActionImageViewer { get; set; }
        public string ActionSaveSessionData { get; set; }
        public string ActionSaveFiles { get; set; }
        public string ActionDownloadMediaList { get; set; }

        public string ActionInitializeViewItems { get; set; }
    }

    public class ChannelConfigMediaList
    {
        public string ChannelSelectedObject { get; set; }
        public string ChannelUploadedFiles { get; set; }
        public string ChannelViewReady { get; set; }
        public string ChannelMediaList { get; set; }
        public string ChannelAppliedObjects { get; set; }

        public ChannelConfigMediaList()
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelUploadedFiles = SpecialChannels.UploadedFiles;
            ChannelViewReady = Channels.LocalData.Object_ViewReady.GUID;
            ChannelMediaList = SpecialChannels.MediaList;
            ChannelAppliedObjects = Channels.LocalData.Object_AppliedObjects.GUID;
        }
    }
}