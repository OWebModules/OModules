﻿using HtmlEditorModule.Models;
using LocalizationModule.Models;
using MediaViewerModule.Models;
using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class PDFSearchViewModel : OViewBaseModel
    {
        public string fullHeight { get; private set; }

        
        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem searchContainer { get; private set; }
        
        public string SearchString { get; set; }
        public List<PDFSearchGridItem> SearchResultItems { get; set; } = new List<PDFSearchGridItem>();

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetGridConfigSearch { get; set; }

        public string ActionInitPDFSearch { get; set; }

        public string ActionRelationEditor { get; set; }

        public string ActionModuleStarter { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string scrollId { get; set; }
        public int lastPage { get; set; }
        public int lastPageSize { get; set; }

        public List<KendoSortRequest> lastSortRequests { get; set; } = new List<KendoSortRequest>();


        public PDFSearchViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_PDF_Search.GUID, idInstance, userId)
        {

            fullHeight = nameof(fullHeight);
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;


            

        }
    }


}