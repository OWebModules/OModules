﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ObjectNameEditViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Form)]
        public ViewItem objectItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Name:")]
        public ViewItem lblName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inpName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Guid:")]
        public ViewItem lblGUID { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input, NoDefaultValueInit = true)]
        public ViewItem inpGUID { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button, NoDefaultValueInit = true)]
        public ViewItem btnSave { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton, NoDefaultValueInit = true)]
        public ViewItem btnSelect { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton, NoDefaultValueInit = true)]
        public ViewItem btnApply { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionSaveObjectOrObjectName { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string ChannelAddedObjects { get; set; }

        public string ChannelSelectedClass { get; set; }

        public string ChannelAppliedObjects { get; set; }

        public clsOntologyItem RefItem { get; set; }
        public clsOntologyItem refParentItem { get; set; }

        public ObjectNameEditViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ObjectNameEdit.GUID, idInstance, userId)
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID;
            ChannelAddedObjects = Channels.LocalData.Object_AddedObjects.GUID;
            ChannelAppliedObjects = Channels.LocalData.Object_AppliedObjects.GUID;

            inpName.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            btnSave.ChangeViewItemValue(ViewItemType.Visible.ToString(), true);
            btnSave.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            inpGUID.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            inpGUID.ChangeViewItemValue(ViewItemType.Content.ToString(), Guid.NewGuid().ToString().Replace("-",""));
            btnSelect.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Object-Name-Editor");

        }
    }
}