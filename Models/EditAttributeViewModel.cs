﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class EditAttributeViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem attributeArea { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.CheckBox)]
        public ViewItem boolValue { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "Value")]
        public ViewItem attributeLabel { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.DateTimeInput)]
        public ViewItem dateTimeValue { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.NumericInput, NoDefaultValueInit = true)]
        public ViewItem doubleValue { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.NumericInput, NoDefaultValueInit = false)]
        public ViewItem longValue { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.TextArea)]
        public ViewItem strValue { get; set; }
        public string ChannelParameterList { get; set; }

        public string ActionValidateReference { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionGetAttribute { get; set; }
        public string ActionSaveBoolAttribute { get; set; }
        public string ActionSaveDateTimeAttribute { get; set; }
        public string ActionSaveDoubleAttribute { get; set; }
        public string ActionSaveLongAttribute { get; set; }
        public string ActionSaveStringAttribute { get; set; }

        public string ActionEditAttributeInit { get; set; }

        public clsObjectAtt objectAttribute { get; set; }
        public clsOntologyItem objectItem { get; set; }
        public clsOntologyItem objectParentItem { get; set; }
        public clsOntologyItem attributeTypeItem { get; set; }

        public string dataTypeId { get; set; }

        public clsDataTypes DataTypes { get; private set; }

        public EditAttributeViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_EditAttribute.GUID, idInstance, userId)
        {
            ChannelParameterList = Channels.LocalData.Object_ParameterList.GUID;
            DataTypes = new clsDataTypes();
        }
    }
}