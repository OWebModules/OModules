﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ClipboardViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Container, NoDefaultValueInit = true)]
        public ViewItem gridParent { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid, NoDefaultValueInit = true)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToolBar, NoDefaultValueInit = true)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem apply { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem removeItems { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem clearClipboard { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionGetGridConfig { get; set; }

        public string ActionGridCreated { get; set; }

        public string ActionGridBound { get; set; }

        public string ActionClipboardItemsExist { get; set; }

        public string ActionClearClipboard { get; set; }
        public string ActionAddItems { get; set; }

        public string ActionInitClipbaord { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string ChannelAppliedObjects { get; set; }

        public string ChannelAddedItemToClipboard { get; set; }

        public clsOntologyItem refItem { get; set; }

        public List<ClipboardItem> ClipboardItems { get; set; }

        public ClipboardViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_Clipboard1.GUID, idInstance, userId)
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObjects = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelAddedItemToClipboard = SpecialChannels.AddedItemToClipboard;

        }

    }
}
