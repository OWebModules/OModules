﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class HtmlViewerViewModel : OViewBaseModel
    {

        public string classLoader { get; private set; }
        public string classContentIndex { get; private set; }
        public string classTopPanel { get; private set; }
        public string classNoprint { get; private set; }
        public string classSlideButton { get; private set; }
        public string classA4 { get; private set; }

        public string classOntologyItem { get; private set; }
        public string classContent { get; private set; }

        public clsOntologyItem refItem { get; set; }
        public clsOntologyItem refParentItem { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem refPanel { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem viewerArea { get; set; }

        public object getHtml { get; set; }
        public object messageFromWebsocket { get; set; }
        public object setDataItems { get; set; }
        public object preLoadContent { get; set; }
        public object loadContent { get; set; }
        public object addTags { get; set; }
        public object markOrUnmarkTag { get; set; }
        public object getUrlParameters { get; set; }
        public object validateReference { get; set; }

        public object toLoad { get; set; }
        public object ixToTag { get; set; }
        public object addHtmlItemToLoad { get; set; }
        public object getFirstHtmlItemToLoad { get; set; }
        public object getFirstHtmlItemToTag { get; set; }
        public object getHtmlItemById { get; set; }

        public ActionConfigHtmlViewer ActionConfig { get; set; }
        public ChannelConfigHtmlViewer ChannelConfig { get; private set; }

        public HtmlViewerViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_HtmlViewer.GUID, idInstance, userId)
        {
            
            
            classLoader = "loader";
            classContentIndex = "contentIndex";
            classTopPanel = "topPanel";
            classNoprint = "noprint";
            classSlideButton = "slide-button";
            classA4 = "A4";

            classOntologyItem = "ontologyItem";
            classContent = "Content";

            ChannelConfig = new ChannelConfigHtmlViewer();
        }
    }

    public class ActionConfigHtmlViewer
    {
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetHtml { get; set; }
        public string ActionGetRawHtml { get; set; }
        public string ActionGetTags { get; set; }
    }

    public class ChannelConfigHtmlViewer
    {
        public string ChannelSelectedObject { get; set; }

        public ChannelConfigHtmlViewer()
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}