﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models.MediaViewer
{
    public class CreateMediaBookmarkRequest
    {
        public string nameBookmark { get; set; } 
        public string pos { get; set; }
        public string idMultimediaItem { get; set; }
        public string idInstance { get; set; }
        public clsOntologyItem logState { get; set; }
    }
}