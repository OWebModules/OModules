﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ObjectRelationTreeViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem controlArea { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem treeToolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem objectTreeView { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        public clsOntologyItem RefItem { get; set; }

        private string ChannelSelectedObject { get; set; }


        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }

        public string ActionInit { get; set; }
        public string ActionGetTreeConfig { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionClipboardInit { get; set; }

        public string ActionObjectListInit { get; set; }

        public string ActionRelateItems { get; set; }

        public ObjectRelationTreeModel ObjectRelationTreeModel { get; set;}
        public List<KendoTreeNode> TreeNodes { get; set; } = new List<KendoTreeNode>();


        public ObjectRelationTreeViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ObjectRelationTree.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Object-Relation-Tree");

            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}