﻿using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ReportViewerViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton, DefaultEnable = true)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Splitter)]
        public ViewItem splitter { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem iframeObjectTree { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.IFrame)]
        public ViewItem iframeReport { get; private set; }


        public string ChannelSelectedObject { get; set; }

        public DateTime StartSync { get; set; }

       

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }

        public string ActionObjectTree { get; set; }
        public string ActionReport { get; set; }

        public string ActionModuleStarter { get; set; }

        public string idClassReports { get; set; }

        public ReportViewerViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ReportViewer.GUID, idInstance, userId)
        {
            isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}