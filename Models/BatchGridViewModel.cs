﻿using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class BatchGridViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        public string ActionGetGridConfig { get; set; }

        public string ChannelModuleSelected { get; private set; }

        public BatchGridViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_BatchGrid.GUID, idInstance, userId)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), "Batch-Grid");
            ChannelModuleSelected = SpecialChannels.SelectedToolsModule;
        }
    }
}