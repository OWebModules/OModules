﻿using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class AppointmentViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem rootContainer { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem contentContainer { get; set; }

        [ViewModel(ViewItemClass = ViewItemClass.Calendar)]
        public ViewItem scheduler { get; set; }

        public DateTime startTime { get; private set; }
        public string majorTimeHeaderTemplate { get; private set; }

        public object setDataItems { get; set; }
        public object messageFromWebsocket { get; set; }

        public string IdParentAppointment { get; set; }
        public string TypeObject { get; set; }

        public AppointmentViewTemplate ViewTemplateDay { get; private set; }
        public AppointmentViewTemplate ViewTemplateWorkWeek { get; private set; }
        public AppointmentViewTemplate ViewTemplateWeek { get; private set; }
        public AppointmentViewTemplate ViewTemplateMonth { get; private set; }

        public AppointmentActionConfig ActionConfig { get; set; }

        public AppointmentChannelConfig ChannelConfig { get; private set; }

        public string fullHeight { get; private set; }

        public AppointmentViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_Calendar.GUID, idInstance, userId)
        {
            fullHeight = nameof(fullHeight);
            startTime = DateTime.Now.AddYears(5);

            ViewTemplateDay = new AppointmentViewTemplate
            {
                type = Primitives.Configurations.AppointmentViewType_Day,
                dateHeaderTemplate = "<strong>#=kendo.toString(date, 'dd.MM')#</strong>"
            };

            ViewTemplateWorkWeek = new AppointmentViewTemplate
            {
                type = Primitives.Configurations.AppointmentViewType_WorkWeek,
                dateHeaderTemplate = "<strong>#=kendo.toString(date, 'ddd dd.MM')#</strong>"
            };

            ViewTemplateWeek = new AppointmentViewTemplate
            {
                type = Primitives.Configurations.AppointmentViewType_Week,
                dateHeaderTemplate = "<strong>#=kendo.toString(date, 'ddd dd.MM')#</strong>"
            };

            ViewTemplateMonth = new AppointmentViewTemplate
            {
                type = Primitives.Configurations.AppointmentViewType_Month,
                dateHeaderTemplate = "<strong>#=kendo.toString(date, 'ddd dd.MM')#</strong>"
            };

            ChannelConfig = new AppointmentChannelConfig();
        }
    }

    public class AppointmentViewTemplate
    {
        public string type { get; set; }
        public bool selected { get; set; }
        public string dateHeaderTemplate { get; set; }
    }

    public class AppointmentChannelConfig
    {
        public string ChannelSelectedObject { get; private set; }

        public AppointmentChannelConfig()
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }

    public class AppointmentActionConfig
    {
        
        public string ActionGetDataSource { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }

        public string ActionGetAppointments { get; set; }
        public string ActionCreateAppointent { get; set; }
        public string ActionUpdateAppointment { get; set; }
        public string ActionDestroyAppointment { get; set; }
    }
}
