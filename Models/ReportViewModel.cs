﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using ProcessModule.Models;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ReportViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem gridContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openModuleStarter { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openObjectEditor { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem refreshReport { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem syncData { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton, DefaultEnable = true)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openScript { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem applyItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem saveQuery { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inputQuery { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem processReport { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionGetGridConfig { get; set; }
        public string ActionGetObjectForSend { get; set; }
        public string ActionSyncReport { get; set; }
        public string ActionGetSyncState { get; set; }

        public string ActionInitializeViewItemsReport { get; set; }

        public string ActionModuleStarter { get; set; }

        public string ActionObjectEditor { get; set; }

        public string ActionHtmlDiff { get; set; }

        public string ActionGetAutoCompletesFilter { get; set; }

        public string ActionSavePattern { get; set; }

        public string ActionReportInit { get; set; }

        public string ActionUploadReportData { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string ChannelAppliedObject { get; set; }

        public string ChannelSelectedCell { get; set; }

        public string ChannelProcessReport { get; set; }

        public string ChannelControllerMessage { get; set; }

        public string IdAdditionalViewModel { get; set; }
        public clsOntologyItem refItem { get; set; }
        public clsOntologyItem refParentItem { get; set; }
        public ReportItem ReportItem { get; set; }
        public List<ReportField> ReportFields { get; set; }

        public List<ReportFilterItem> ReportFilters { get; set; }
        public ReportFilterItem StandardFilter { get; set; }

        public GetRowDiffResult RowDiff { get; set; }

        public List<AdditionalReportField> AdditionalFields { get; set; } = new List<AdditionalReportField>();

        public ReportViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_Report1.GUID, idInstance, userId)
        {
            isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            inputQuery.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObject = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelSelectedCell = SpecialChannels.SelectedCell;
            ChannelProcessReport = SpecialChannels.ProcessReport;
            ChannelControllerMessage = SpecialChannels.ControllerMessage;
        }
    }
}