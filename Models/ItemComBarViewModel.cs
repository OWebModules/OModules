﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ItemComBarViewModel : OViewBaseModel
    {


        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem guid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem sendItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem applyItem { get; private set; }

        public clsOntologyItem RefItem { get; set; }

        public string ActionValidateReference { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }

        public string ChannelSelectedObject { get; set; }
        public string ChannelAppliedObject { get; set; }

        public ItemComBarViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_ItemComBar.GUID, idInstance, userId)
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObject = Channels.LocalData.Object_AppliedObjects.GUID;
        }
    }
}