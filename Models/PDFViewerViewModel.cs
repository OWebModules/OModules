﻿using MediaViewerModule.Models;
using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class PDFViewerViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem mediaItemName { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblCountToSave { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem countToSave { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblCountSaved { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelNav { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem gotoFirst { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem gotoPrevious { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem gotoNext { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem gotoLast { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openAutoPDF { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openPDF { get; private set; }

        public List<MediaListItem> MediaListItems { get; set; }

        public int IxMediaListItem { get; set; }

        public ActionConfigPDFViewer ActionConfig { get; set; }
        public ChannelConfigPDFViewer ChannelConfig { get; private set; }

        public PDFViewerViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_PDFViewer.GUID, idInstance, userId)
        {

            ChannelConfig = new ChannelConfigPDFViewer();

        }
    }

    public class ActionConfigPDFViewer
    {
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionOpenPDF { get; set; }
    }

    public class ChannelConfigPDFViewer
    {
        public string ChannelSelectedObject { get; set; }

        public ChannelConfigPDFViewer()
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}