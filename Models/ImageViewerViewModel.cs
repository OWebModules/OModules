﻿using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class ImageViewerViewModel : OViewBaseModel
    {

        [ViewModel(ViewItemClass = ViewItemClass.Container, NoDefaultValueInit = true)]
        public ViewItem mediaContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container, NoDefaultValueInit = true)]
        public ViewItem galleryContainer { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other, NoDefaultValueInit = true)]
        public ViewItem galleryItem { get; private set; }

        public ActionConfigImageViewer ActionConfig { get; set; }
        public ChannelConfigImageViewer ChannelConfig { get; private set; }

        public ImageViewerViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_Image_Viewer.GUID, idInstance, userId)
        {
            ChannelConfig = new ChannelConfigImageViewer();
        }
    }

    public class ActionConfigImageViewer
    {
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionValidateReference { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionGetSessionDataUrl { get; set; }
    }

    public class ChannelConfigImageViewer
    {
        public string ChannelSelectedObject { get; set; }
        public string ChannelViewReady { get; set; }
        public string ChannelMediaList { get; set; }

        public ChannelConfigImageViewer()
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelViewReady = Channels.LocalData.Object_ViewReady.GUID;
            ChannelMediaList = SpecialChannels.MediaList;
        }
    }
}