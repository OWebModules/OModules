﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class SaveNewObjectRequest
    {
        public string text { get; set; }
        public string idClass { get; set; }
        public string idRelationType { get; set; }
        public bool saveTypedTag { get; set; }
    }
}