﻿using OModules.Models.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class LocalizedItemsViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }
        public string ActionValidateReference { get; set; }

        public string ActionItemComBarInit { get; set; }

        public string ActionLocalizeNamesInit { get; set; }


        public string ChannelSelectedObject { get; set; }
        public string ChannelAppliedObject { get; set; }

        public string ChannelControllerMessage { get; set; }

        public LocalizedItemsViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_LocalizeItems.GUID, idInstance, userId)
        {
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelAppliedObject = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelControllerMessage = SpecialChannels.ControllerMessage;
        }
    }
}