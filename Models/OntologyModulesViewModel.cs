﻿using LocalizationModule.Models;
using OModules.Models.Base;
using OModules.Models.Requests;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class OntologyModulesViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.ToolBar)]
        public ViewItem toolbar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openRelationEditor { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem openObjectTree { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem classTree { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem newItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem deleteItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem applyItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem addToClipboard { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem lblOrderId { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelSearch { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelOModules { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelOViews { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelObjectClassView { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelModuleStarter { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label)]
        public ViewItem labelObject { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Input)]
        public ViewItem inpOrderId { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Grid)]
        public ViewItem grid { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other)]
        public ViewItem selectedItem { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem orderAsc { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem orderDesc { get; private set; }

        public NewWindowViewModel newWindowViewModel { get; private set; }
        public EditWindowViewModel editWindowViewModel { get; private set; }

        public EditOrderWindowViewModel editOrderWindowViewModel { get; private set; }

        public DeleteWindowViewModel deleteWindowViewModel { get; private set; }

        public SelectedORelItemRequest selectedOItemRequest { get; set; }

        public object gridItem { get; set; }

        public string tabStrip { get; set; }
        public string idSplitterOModules { get; set; }
        public string idPanelBar { get; set; }
        public string idDivViews { get; set; }
        public string idDivModules { get; set; }
        public string idDivOModules { get; set; }
        public string idIframeOModules { get; set; }
        public string idIframeViews { get; set; }
        public string idIframeSearch { get; set; }
        public string idIframeModuleStarter { get; set; }
        public string idDivSearch { get; set; }

        public string urlClassTree { get; set; }
        public string urlObjectList { get; set; }
        public string urlSearch { get; set; }
        public string urlModuleStarter { get; set; }

        public string urlViews { get; set; }

        public string urlClassObject { get; set; }

        public object messageFromWebsocket { get; set; }

        public List<GuiEntry> GuiEntries { get; set; } = new List<GuiEntry>();

        public string ActionInitClassTree { get; set; }
        public string ActionRelationEditor {get; set;}
        public string ActionObjectTree { get; set; }
        public string ActionSetSelectedItem { get; set; }
        public string ActionSetOrderId { get; set; }
        public string ActionGetGridConfig { get; set; }
        public string ActionGetUrl { get; set; }
        public string ActionGetGridConfigViewList { get; set; }
        public string ActionGetUrlViewList { get; set; }

        public string ActionModuleStarterInit { get; set; }

        public string ActionGetSearchRequestItem { get; set; }
        public string ActionSearchClassChanged { get; set; }
        public string ActionSearchGetGridConfig { get; set; }
        public string ActionRemoveSearchItemArea { get; set; }
        public string ActionSearchItemTextChanged { get; set; }
        public string ActionAddSearchItemArea { get; set; }
        public string ActionSearchRelationTypeChanged { get; set; }
        public string ActionSearchAttributeTypeChanged { get; set; }
        public string ActionInitSearchItems { get; set; }
        public string ActionInitPDFSearch { get; set; }

        public string ActionObjectListLinit { get; set; }
        public string ActionGetGridConfigHtmlSearch { get; set; }

        public string ChannelSelectedClass { get; private set; }
        public string ChannelViewReady { get; private set; }
        public string ChannelSelectedObject { get; set; }
        public string ChannelSelectedRelationNode { get; set; }
        public string ChannelAddedObjects { get; set; }
        public string ChannelParameterList { get; set; }
        public string ChannelAppliedItems { get; set; }
        public string ChannelSelectedParent { get; set; }
        public string ChannelSelectedItem { get; set; }

        public string ChannelControllerMessage { get; set; }

        public OntologyModulesViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_Ontology_Modules.GUID, idInstance, userId)
        {
            idSplitterOModules = "splitterOmodules";
            idPanelBar = "panelBarOModules";
            idDivViews = "divViews";
            idDivOModules = "divOModules";
            idDivSearch = "divSearch";
            idIframeViews = "iframeViews";
            idIframeOModules = "iframeOMOdules";
            idDivModules = "divModules";
            idIframeSearch = nameof(idIframeSearch);
            idIframeModuleStarter = nameof(idIframeModuleStarter);

            //labelSearch = "Search";
            //labelOModules = "Ontology-Editor";
            //labelOViews = "Views";
            //labelObjectClassView = "Object-Class";
            //labelModuleStarter = "Module-Starter";

            tabStrip = nameof(tabStrip);

            isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            ChannelSelectedClass = Channels.LocalData.Object_SelectedClassNode.GUID;
            ChannelViewReady = Channels.LocalData.Object_ViewReady.GUID;
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelSelectedRelationNode = Channels.LocalData.Object_SelectedRelationNode.GUID;
            ChannelAppliedItems = Channels.LocalData.Object_AppliedObjects.GUID;
            ChannelAddedObjects = Channels.LocalData.Object_AddedObjects.GUID;
            ChannelParameterList = Channels.LocalData.Object_ParameterList.GUID;
            ChannelSelectedParent = Channels.LocalData.Object_SelectedClassNode.GUID;
            ChannelSelectedItem = Channels.LocalData.Object_SelectedObject.GUID;
            ChannelControllerMessage = SpecialChannels.ControllerMessage;

            newWindowViewModel = new NewWindowViewModel();
            editWindowViewModel = new EditWindowViewModel();
            editOrderWindowViewModel = new EditOrderWindowViewModel();
            deleteWindowViewModel = new DeleteWindowViewModel();

        }
    }

    
}