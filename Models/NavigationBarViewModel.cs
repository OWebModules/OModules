﻿using OModules.Models.Base;
using OntologyItemsModule.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class NavigationBarViewModel : OViewBaseModel
    {
        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem listenNavBar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Container)]
        public ViewItem navigationBar { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem navFirst { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem navPrevious { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem navNext { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Button)]
        public ViewItem navLast { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "0/0")]
        public ViewItem navPos { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.Label, DefaultContent = "-")]
        public ViewItem navItemName { get; private set; }

        public ItemsNavItem NavigationItem { get; set; }

        public string ChannelNavigationItems { get; set; }
        public string ChannelSelectedNavigationItem { get; set; }


        public string ActionGetSessionData { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }
        public string ActionSetViewReady { get; set; }

        public string ActionInitializeNavigationItems { get; set; }
        public string ActionGetCurrentNavigationItem { get; set; }

        public string ActionNavFirst { get; set; }
        public string ActionNavNext { get; set; }
        public string ActionNavPrevious { get; set; }
        public string ActionNavLast { get; set; }



        public NavigationBarViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_NavigationBar1.GUID /* "9c903a37ef254ae6ae37883704ece285" */, idInstance, userId)
        {
            ChannelSelectedNavigationItem = SpecialChannels.SelectedNavigationItem;
            ChannelNavigationItems = SpecialChannels.NavigationItems;
        }
    }
}