﻿using OModules.Globals;
using OModules.Primitives;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoMsg_Module.StateMachines;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Web;

namespace OModules.Models.Base
{
    public abstract class OViewBaseModel
    {
        public List<ViewItem> ViewItems
        {
            get; protected set;
        }

        [ViewModel(ViewItemClass = ViewItemClass.Window, NoDefaultValueInit = true)]
        public ViewItem windowTitle { get; protected set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other, NoDefaultValueInit = true)]
        public ViewItem infoOutput { get; protected set; }

        [ViewModel(ViewItemClass = ViewItemClass.Other, NoDefaultValueInit = true)]
        public ViewItem errorOutput { get; protected set; }

        public ControllerStateMachine StateMachine { get; private set; }
        public ViewMetaItem View { get; set; }
        public CreateViewListResult Views { get; set; }
        public InitState PageInitState { get; set; } = new InitState();
        public string IdModule { get; set; }

        public string IdInstance { get; set; }
        public string IdEndpoint { get; set; }
        public string IdSession { get; set; }
        public string idSender { get; set; }
        public string IdUser { get; set; }

        public List<string> SelectorPath { get; set; }

        public string GetJsonItem()
        {

            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);

        }

        public void SetWindowTitle(string title)
        {
            windowTitle.ChangeViewItemValue(ViewItemType.Content.ToString(), title);
        }

        public void SetActionSetViewItemValue(string action)
        {
            var properties = this.GetType().GetProperties().Where(prop => prop.PropertyType == typeof(ViewItem));

            foreach (var prop in properties)
            {
                var viewItem = (ViewItem)prop.GetValue(this);
                viewItem.ActionChangeValue = action;
            }
        }


        public OViewBaseModel(HttpSessionStateBase session, string IdView, string idInstance, string userId)
        {
            IdUser = userId;
            IdInstance = idInstance;
            IdEndpoint = Guid.NewGuid().ToString();
            IdModule = ((GuidAttribute)Assembly.GetExecutingAssembly()
                      .GetCustomAttributes(true)
                      .FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value.Replace("-", "");
            IdSession = session.SessionID;
            Views = (CreateViewListResult)HttpRuntime.Cache.Get(CacheKeys.ViewConfig);
            View = Views.ViewList.FirstOrDefault(view => view.IdView == IdView);
            StateMachine = StateMachineController.RegisterStateMachine(session, IdView, IdEndpoint);
            ViewItems = new List<ViewItem>();
            
            ViewItems = new List<ViewItem>();
            var properties = this.GetType().GetProperties().Where(prop => prop.PropertyType == typeof(ViewItem));
            foreach (PropertyInfo prop in properties)
            {
                var viewItem = new ViewItem();
                var attribute = prop.GetCustomAttribute<ViewModelAttribute>();
                if (attribute != null)
                {
                    var viewItemId = attribute.ViewItemId;
                    if (string.IsNullOrEmpty(viewItemId))
                    {
                        viewItemId = prop.Name;
                    }
                    viewItem.ViewItemId = viewItemId;
                    viewItem.ViewItemClass = attribute.ViewItemClass.ToString();
                    viewItem.SpecialType = attribute.SpecialType;
                    
                    if (!attribute.NoDefaultValueInit)
                    {
                        viewItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), attribute.DefaultEnable);
                        viewItem.ChangeViewItemValue(ViewItemType.Visible.ToString(), attribute.DefaultVisible);
                        if (!string.IsNullOrEmpty(attribute.DefaultContent))
                        {
                            viewItem.ChangeViewItemValue(ViewItemType.Content.ToString(), attribute.DefaultContent);
                        }
                        if (!string.IsNullOrEmpty(attribute.DefaultCaption))
                        {
                            viewItem.ChangeViewItemValue(ViewItemType.Caption.ToString(), attribute.DefaultCaption);
                        }
                    }
                }
                viewItem.IdViewInstance = idInstance;
                prop.SetValue(this, viewItem);
                ViewItems.Add(viewItem);
            }
            
        }
    }
}