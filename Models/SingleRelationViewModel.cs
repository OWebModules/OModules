﻿using OModules.Models.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Models
{
    public class SingleRelationViewModel : OViewBaseModel
    {
        public string idView
        {
            get
            {
                return View.IdView;
            }
        }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem relationButton { get; private set; }

        [ViewModel(ViewItemClass = ViewItemClass.ToggleButton)]
        public ViewItem isListen { get; private set; }

        public string ActionValidateReference { get; set; }
        public string ActionConnected { get; set; }
        public string ActionDisconnected { get; set; }

        public string ActionSetViewReady { get; set; }

        public string ActionInitializeViewItems { get; set; }

        public string ChannelSelectedObject { get; set; }

        public clsOntologyItem Reference { get; set; }
        public clsOntologyItem ReferenceParent { get; set; }
        public clsOntologyItem RelationType { get; set; }
        public clsOntologyItem DirectionItem { get; set; }
        public clsObjectRel Relation { get; set; }
        public clsOntologyItem RelationParentItem { get; set; }

        public SingleRelationViewModel(HttpSessionStateBase session, string idInstance, string userId) : base(session, WebViews.Config.LocalData.Object_SingleRelation.GUID, idInstance, userId)
        {            
            ChannelSelectedObject = Channels.LocalData.Object_SelectedObject.GUID;
        }
    }
}