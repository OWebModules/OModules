﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.MessageOutput;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntologyItemsModule;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TypedTaggingModule.Connectors;
using TypedTaggingModule.Models;
using SearchProviderResult = OModules.Models.SearchProviderResult;

namespace OModules.Controllers
{
    public class TypedTaggingController : AppControllerBase
    {
        

        private clsLogStates logStates = new clsLogStates();
        private clsTypes types = new clsTypes();


        private OntologyAppDBConnector.Globals GetGlobals()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            LogEndMethod(processId:processId);            return globals;
        }

        public List<ISearchProvider> GetSearchProviders()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();

            var globals = GetGlobals();

            var transforms = memoryCache.GetOrSet<List<ISearchProvider>>(Session.SessionID, "SearchProvider", () =>
            {
                return SearchProviderManager.GetSearchProviders(globals, true);
            });

            LogEndMethod(processId:processId);
            return transforms;
        }

        private SecurityModule.SecurityController GetControllerSecurity()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = GetGlobals();

            var result = new SecurityModule.SecurityController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private OntologyItemsModule.DropDownController GetDropDowncontroller()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = GetGlobals();

            var result = new OntologyItemsModule.DropDownController(globals);

            LogEndMethod(processId:processId);
            return result;

            
        }

        private TypedTaggingModule.Connectors.TypedTaggingConnector GetControllerTypedTagging()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = GetGlobals();

            var result = new TypedTaggingModule.Connectors.TypedTaggingConnector(globals);

            LogEndMethod(processId:processId);
            return result;

            
        }

        private OntologyConnector GetControllerOntology()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = GetGlobals();

            LogEndMethod(processId:processId);
            return new OntologyConnector(globals);
        }

        private TypedOntoRelatorViewModel GetViewModelTypedOntoRelator(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<TypedOntoRelatorViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new TypedOntoRelatorViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedTypedOntoRelator)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedTypedOntoRelator)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyTypedOntoRelator)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceTypedOntoRelator)),
                    ActionGetPossibleRelations = Url.Action(nameof(GetPossibleRelations)),
                    ActionGetPossibleRelationsBetweenNodes = Url.Action(nameof(GetPossibleRelationsBetweenNodes)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeValueTypedOntoRelator)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private TypedTaggingGraphOmniViewModel GetViewModelTypedTaggingGraphOmni(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<TypedTaggingGraphOmniViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new TypedTaggingGraphOmniViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedTypedTaggingGraphOmni)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedTypedTaggingGraphOmni)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyTypedTaggingGraphOmni)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceTypedTaggingGraphOmni)),
                    ActionGetOmniTags = Url.Action(nameof(GetOmniTags)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeValueTypedTaggingGraphOmni)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private TypedTaggingListViewModel GetTypedTaggingListViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<TypedTaggingListViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new TypedTaggingListViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigTypedTaggingList
                    {
                        ActionConnected = Url.Action(nameof(ConnectedTypedTaggingList)),
                        ActionDisconnected = Url.Action(nameof(DisconnectedTypedTaggingList)),
                        ActionSetViewReady = Url.Action(nameof(SetViewTypedTaggingListReady)),
                        ActionValidateReference = Url.Action(nameof(ValidateTypedTaggingListReference)),
                        ActionGetTags = Url.Action(nameof(GetTags)),
                        ActionEditObject = Url.Action(nameof(ObjectEditController.RelationEditor), nameof(ObjectEditController).Replace("Controller", ""))
                    },
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeTypedTaggingListViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private AutoRelatorViewModel GetAutoRelatorViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<AutoRelatorViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new AutoRelatorViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedAutoRelator)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedAutoRelator)),
                    ActionSetViewReady = Url.Action(nameof(SetViewAutoRelatorReady)),
                    ActionValidateReference = Url.Action(nameof(ValidateAutoRelatorReference)),
                    ActionParseText = Url.Action(nameof(ParseText)),
                    ActionSaveRelatorTags = Url.Action(nameof(SaveRelatorTags)),
                    ActionGetRelationTypeDropDownConfig = Url.Action(nameof(DropDownController.GetDropDownConfigForRelationTypes), nameof(DropDownController).Replace("Controller", "")),
                    ActionGetClassDropDownConfig = Url.Action(nameof(DropDownController.GetDropDownConfigForClasses), nameof(DropDownController).Replace("Controller", "")),
                    ActionGetOntoTemplate = Url.Action(nameof(GetOntoTemplate)),
                    ActionSaveNewObject = Url.Action(nameof(SaveNewObject)),
                    ActionTypedTaggingList = Url.Action(nameof(TypedTaggingController.TypedTaggingList), nameof(TypedTaggingController).Replace("Controller", "")),
                    ActionSaveOntoRelations = Url.Action(nameof(SaveOntoRelations)),
                    ActionGetAutoCompletes = Url.Action(nameof(GetAutoCompletes)),
                    ActionEditObject = Url.Action(nameof(ObjectEditController.RelationEditor), nameof(ObjectEditController).Replace("Controller", "")),
                    ActionGetSearchProviders = Url.Action(nameof(GetSearchProviderList)),
                    ActionSearch = Url.Action(nameof(SearchProviders)),
                    ActionGetGridConfigSearchProviderResult = Url.Action(nameof(GetGridConfigSearchProviderResult)),
                    ActionValidateReferenceTypedTaggingList = Url.Action(nameof(ValidateTypedTaggingListReference)),
                    ActionGetTagsTypedTaggingList = Url.Action(nameof(GetTags)),
                    ActionEditObjectTypedTaggingList = Url.Action(nameof(ObjectEditController.RelationEditor), nameof(ObjectEditController).Replace("Controller", "")),
                    ActionGetTransformedNames = Url.Action(nameof(GetTransformNames)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeListViewItemValueAutoRelator)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetSearchProviderList(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetAutoRelatorViewModel(idInstance);

            var resultAction = new ResultSimpleView<List<ISearchProvider>>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                Result = new List<ISearchProvider>()
            };

            viewModel.SearchProviders = GetSearchProviders();

            resultAction.Result = viewModel.SearchProviders;

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeTypedTaggingListViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetTypedTaggingListViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeListViewItemValueAutoRelator(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetAutoRelatorViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            if (viewItem.ViewItemId == viewModel.inpClass.ViewItemId)
            {
                var value = viewItem.GetViewItemValueItem(ViewItemType.SelectedIndex.ToString()).Value;
                if (value != null && value is string[])
                {
                    var values = (string[])value;
                    viewModel.idClassSelected = values[0];
                }
                
            }


            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeValueTypedOntoRelator(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTypedOntoRelator(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction.ViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeValueTypedTaggingGraphOmni(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTypedTaggingGraphOmni(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedTypedOntoRelator()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTypedOntoRelator(idInstance);

            viewItemList.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedTypedOntoRelator()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTypedOntoRelator(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceTypedOntoRelator(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTypedOntoRelator(idInstance);

            var controller = GetControllerTypedTagging();
            var refItemTask = await controller.GetOItem(refItem.GUID, types.ObjectType);

            viewModel.refItem = refItemTask.Result;

            var resultViewItems = new List<ViewItem>();
            if (viewModel.refItem != null)
            {
                var refItemParent = await controller.GetOItem(refItemTask.Result.GUID_Parent, types.ClassType);
                viewModel.refParentItem = refItemParent.Result;


                viewModel.SetWindowTitle($"{refItemTask.Result.Name} ({refItemParent.Result.Name})");
                resultViewItems.Add(viewModel.windowTitle);
            }

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceTypedTaggingGraphOmni(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTypedTaggingGraphOmni(idInstance);

            var controller = GetControllerTypedTagging();
            var refItemTask = await controller.GetOItem(refItem.GUID, types.ObjectType);

            viewModel.refItem = refItemTask.Result;

            var resultViewItems = new List<ViewItem>();
            if (viewModel.refItem != null)
            {
                var refItemParent = await controller.GetOItem(refItemTask.Result.GUID_Parent, types.ClassType);
                viewModel.refParentItem = refItemParent.Result;


                viewModel.SetWindowTitle($"{refItemTask.Result.Name} ({refItemParent.Result.Name})");
                resultViewItems.Add(viewModel.windowTitle);
            }

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetAutoCompletes(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetAutoRelatorViewModel(idInstance);
            var controller = GetControllerTypedTagging();

            var text = Request.Params["query"];

            var result = new AutoCompleteItem();

            if (viewModel.ontology == null)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            //var classes = viewModel.ontology.OntologyItems.Where(oItem => oItem.RefItem.Type == controller.Globals.Type_Class).Select(oItem => oItem.RefItem).ToList();

            var classes = new List<clsOntologyItem>();
            if (!string.IsNullOrEmpty(viewModel.idClassSelected))
            {
                var classItem = await controller.GetOItem(viewModel.idClassSelected, controller.Globals.Type_Class);
                classes.Add(classItem.Result);
            }

            var resultController = await controller.GetAutoCompletes(classes, text);
            if (resultController.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result = resultController.Result;
            
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyTypedOntoRelator(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTypedOntoRelator(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyTypedTaggingGraphOmni(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTypedOntoRelator(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedTypedTaggingList()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetTypedTaggingListViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedTypedTaggingList()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetTypedTaggingListViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedAutoRelator()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetAutoRelatorViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedAutoRelator()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetAutoRelatorViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedTypedTaggingGraphOmni()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTypedTaggingGraphOmni(idInstance);
            viewItemList.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedTypedTaggingGraphOmni()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTypedTaggingGraphOmni(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetTags(string idReference, string idInstance)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idReference)) return Json(new { }, JsonRequestBehavior.AllowGet);
            var viewModel = GetTypedTaggingListViewModel(idInstance);
            var result = new ResultGetTags
            {
                IsSuccessful = true
            };

            var controller = GetControllerTypedTagging();

            var resultGetTags = await controller.GetTags(idReference, Url.Action(nameof(AutoRelator)), Url.Action(nameof(ModuleMgmtController.ModuleStarter), nameof(ModuleMgmtController).Replace("Controller","")));

            var tags = resultGetTags.References.SelectMany(refItm => refItm.Tags).ToList();
            var classes = tags.GroupBy(tag => tag.IdTagParent).Select(tagGrp => tagGrp.Key)
                        .ToList();

            var transformProviders = GetNameTransformController(classes);

            var tagOItems = tags.Select(typedT => new clsOntologyItem
            {
                GUID = typedT.IdTag,
                Name = typedT.NameTag,
                GUID_Parent = typedT.IdTagParent,
                Type = typedT.TagType
            }).ToList();

            foreach (var transformProvider in transformProviders.Where(trans => trans.IsReferenceCompatible).ToList())
            {
                var tagOItemsToProvider =
                    tagOItems.Where(obj => transformProvider.IsResponsible(obj.GUID_Parent)).ToList();
                if (tagOItemsToProvider.Any())
                {
                    var idClass = tagOItemsToProvider.First().GUID_Parent;
                    var transFormed = await transformProvider.TransformNames(tagOItemsToProvider, idClass: idClass);
                    (from typedTag in tags
                     join transformedItem in transFormed.Result on typedTag.IdTag equals transformedItem.GUID
                     select new { typedTag, transformedItem }).ToList().ForEach(itm =>
                     {
                         itm.typedTag.EncodedNameTag = itm.transformedItem.Val_String ?? itm.transformedItem.Name;

                     });
                }

            }
            result.Tags = resultGetTags.References;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult GetPossibleRelationsBetweenNodes(string idNode, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTypedOntoRelator(idInstance);
            var result = new ResultGetTags
            {
                IsSuccessful = true
            };

            var controller = GetControllerTypedTagging();

            var relations = viewModel.PossibleRelations.PossibleORelations.Where(rel => rel.ID_Object == idNode || rel.ID_Other == idNode);

            LogEndMethod(processId:processId);
            return Json(relations, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetPossibleRelations(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTypedOntoRelator(idInstance);
            if (viewModel.refItem == null) return Json(new { }, JsonRequestBehavior.AllowGet);
            
            var result = new ResultSimpleView<List<GraphNode>>
            {
                IsSuccessful = true,
                Result = new List<GraphNode>()
            };

            var controller = GetControllerTypedTagging();

            var resultGetTags = await controller.GetPossibleObjectRelations(viewModel.refItem.GUID, Url.Action(nameof(AutoRelator)), Url.Action(nameof(ModuleMgmtController.ModuleStarter), nameof(ModuleMgmtController).Replace("Controller", "")));

            if (resultGetTags.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.PossibleRelations = resultGetTags.Result;
            result.Result = viewModel.PossibleRelations.PossibleRelationNodes;
            
            LogEndMethod(processId:processId);
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);

            return Content(json, "application/json");
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetOmniTags(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTypedTaggingGraphOmni(idInstance);
            if (viewModel.refItem == null) return Json(new { }, JsonRequestBehavior.AllowGet);

            var result = new ResultSimpleView<List<GraphNode>>
            {
                IsSuccessful = true,
                Result = new List<GraphNode>()
            };

            var controller = GetControllerTypedTagging();

            var resultGetTags = await controller.GetTypedTagRelationsOmni(viewModel.refItem.GUID, string.Empty, string.Empty);

            if (resultGetTags.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = resultGetTags.Result;

            LogEndMethod(processId:processId);
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);

            return Content(json, "application/json");
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateTypedTaggingListReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetTypedTaggingListViewModel(idInstance);

            var controller = GetControllerTypedTagging();
            var refItemTask = await controller.GetOItem(refItem.GUID, types.ObjectType);

            viewModel.refItem = refItemTask.Result;

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            
            if (viewModel.refItem != null)
            {
                var refItemParent = await controller.GetOItem(refItemTask.Result.GUID_Parent, types.ClassType);
                viewModel.refParentItem = refItemParent.Result;


                viewModel.SetWindowTitle($"{refItemTask.Result.Name} ({refItemParent.Result.Name})");
                result.ViewItems.Add(viewModel.windowTitle);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateAutoRelatorReference(string idRefItem, string content, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetAutoRelatorViewModel(idInstance);

            var controller = GetControllerTypedTagging();
            var ontologyController = GetControllerOntology();

            var refItemTask = await controller.GetOItem(idRefItem, types.ObjectType);

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>
                {
                    viewModel.windowTitle,
                    viewModel.objectName,
                    viewModel.textAreaUserStories,
                    viewModel.btnSearchAll,
                    viewModel.btnSearchOntology,
                    viewModel.btnInsertOntology,
                    viewModel.inpAutoComplete
                }
            };

            if (refItemTask == null)
            {
                viewModel.textAreaUserStories.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.btnSearchAll.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.btnSearchOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.btnInsertOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.inpAutoComplete.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                return Json(new ResultAutoRelator { IsOk = false }, JsonRequestBehavior.AllowGet);
            }

            viewModel.refItem = refItemTask.Result;


            if (viewModel.refItem == null)
            {
                viewModel.textAreaUserStories.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.btnSearchAll.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.btnSearchOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.btnInsertOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.inpAutoComplete.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                return Json(new ResultAutoRelator { IsOk = false }, JsonRequestBehavior.AllowGet);
            }



            var ontology = await ontologyController.GetOntologies(new OntologyItemsModule.Services.GetOntologyRequest(viewModel.refItem)
            {
                GetClassOntology = true,
                DirectionRefOntology = controller.Globals.Direction_RightLeft
            });

            if (ontology.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.textAreaUserStories.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.btnSearchAll.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.btnSearchOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.btnInsertOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.inpAutoComplete.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                return Json(new ResultAutoRelator { IsOk = false }, JsonRequestBehavior.AllowGet);
            }

            viewModel.ontology = ontology.Result.FirstOrDefault();

            var refItemParent = await controller.GetOItem(refItemTask.Result.GUID_Parent, types.ClassType);
            viewModel.refParentItem = refItemParent.Result;

            viewModel.SetWindowTitle($"{refItemTask.Result.Name} ({refItemParent.Result.Name})");
            viewModel.objectName.ChangeViewItemValue(ViewItemType.Content.ToString(), $"{refItemTask.Result.Name} ({refItemParent.Result.Name})");
            viewModel.textAreaUserStories.ChangeViewItemValue(ViewItemType.Content.ToString(), refItemTask.Result.Name);
            
            viewModel.textAreaUserStories.ChangeViewItemValue(ViewItemType.Enable.ToString(), string.IsNullOrEmpty(content));
            viewModel.btnSearchAll.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.btnSearchOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.btnInsertOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewModel.inpAutoComplete.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetOntoTemplate(string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<List<string>>()
            {
                IsSuccessful = true,
                Result = new List<string>()
            };

            
            var viewModel = GetAutoRelatorViewModel(idInstance);

            if (viewModel.ontology == null)
            {
                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            var controller = GetControllerTypedTagging();

            var resultTemplates = await controller.GetOntoTemplates(viewModel.ontology, viewModel.refItem);
            if (resultTemplates.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = resultTemplates.Result.Select(ontoTempl=> ontoTempl.Template).ToList();

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> AddClass(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var controller = GetControllerTypedTagging();


            return Json(new object { }, JsonRequestBehavior.AllowGet);

        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SearchProviders(string providerId, string text, string idInstance)
        {
            var processId = LogStartMethod();
            

            var globals = GetGlobals();
            var viewModel = GetAutoRelatorViewModel(idInstance);
            var searchProvider = viewModel.SearchProviders.FirstOrDefault(prov => prov.ProviderId == providerId);
            var messageOutput = new OWMessageOutput(idInstance);

            text = HttpUtility.HtmlDecode(System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(text)));

            var result = new ResultSimpleView<SearchProviderResult>
            {
                IsSuccessful = true,
                Result = new SearchProviderResult
                {
                    SearchProviderId = searchProvider.ProviderId,
                    SearchProvider = searchProvider.ProviderName,
                    SearchProviderDescription = searchProvider.ProviderDescription
                }
            };
            
            var searchResult = await searchProvider.Search(text, messageOutput);

            if (searchResult.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = searchResult.ResultState.Additional1;
            }

            viewModel.SearchProviderResultItems.AddRange(searchResult.Result.Select(obj => new SearchProviderResultItem
            {
                SearchProviderId = searchProvider.ProviderId,
                SearchProvider = searchProvider.ProviderName,
                SearchProviderDescription = searchProvider.ProviderDescription,
                IdSearchItem = obj.GUID,
                NameSearchItem = obj.Name,
                IdParentSearchItem = obj.GUID_Parent,
                Content = obj.Additional1
            }));

            result.Result.CountSearchResultItems = searchResult.Result.Count;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigSearchProviderResult()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(SearchProviderResultItem), Url.Action(nameof(GetSearchProviderResultItem)), null, null, null, false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetSearchProviderResultItem()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();
            var providerId = Request["idProvider"]?.ToString();

            var viewModel = GetAutoRelatorViewModel(instanceId);

            var result = Json(new { data = viewModel.SearchProviderResultItems.Where(itm => providerId != null ? itm.SearchProviderId == providerId : 1 == 1) }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ParseText(string idInstance, string text, bool useOntology, bool useName)
        {
            var processId = LogStartMethod();
            var viewModel = GetAutoRelatorViewModel(idInstance);
            var memoryCache = new InMemoryCache();
            var controller = GetControllerTypedTagging();

            viewModel.SearchProviderResultItems.Clear();

            if (string.IsNullOrEmpty(text))
            {
                return Json(new ResultAutoRelator { IsOk = true }, JsonRequestBehavior.AllowGet);
            }
            text = HttpUtility.HtmlDecode(System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(text)));

            if (useOntology)
            {
                var resultParseText = await controller.ParseString(viewModel.refItem, viewModel.ontology, text);
                if (resultParseText.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {

                    LogEndMethod(processId:processId);
                    return Json(new ResultAutoRelator { IsOk = false }, JsonRequestBehavior.AllowGet);
                }

                viewModel.OntologyRanges = resultParseText.Result.OntologyJoins;

                var result = new ResultOntoRelator
                {
                    IsOk = true,
                    AutoRelationAnaylze = viewModel.OntologyRanges,
                    AnalyzeText = resultParseText.Result.AnalyzeText
                };

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var resultParseText = await controller.ParseString(viewModel.refItem, useName, text, viewModel.idClassSelected);
                if (resultParseText.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    LogEndMethod(processId:processId);
                    return Json(new ResultAutoRelator { IsOk = false }, JsonRequestBehavior.AllowGet);
                }

                if (resultParseText.Result.TextRanges.Count > 100)
                {
                    viewModel.TextRanges = resultParseText.Result.TextRanges.GetRange(0,100);
                }
                else
                {
                    viewModel.TextRanges = resultParseText.Result.TextRanges;
                }

                var result = new ResultAutoRelator
                {
                    IsOk = true,
                    AutoRelationAnaylze = viewModel.TextRanges,
                    AnalyzeText = resultParseText.Result.AnalyzeText,
                    SearchByGUID = controller.Globals.is_GUID(resultParseText.Result.AnalyzeText)
                };

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            

            
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveNewObject(string idInstance, SaveNewObjectRequest request, int orderId)
        {
            var processId = LogStartMethod();            
            request.text = HttpUtility.HtmlDecode(System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(request.text)));
            var viewModel = GetAutoRelatorViewModel(idInstance);
            var memoryCache = new InMemoryCache();
            var controller = GetControllerTypedTagging();
            var securityConnector = GetControllerSecurity();
            var ontologyController = GetControllerOntology();

            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);
            var groupItem = await securityConnector.GetGroup(userId);

            var newObject = new clsOntologyItem
            {
                GUID = controller.Globals.NewGUID,
                Name = request.text,
                GUID_Parent = request.idClass,
                Type = controller.Globals.Type_Object,
                New_Item = true
            };

            newObject.Val_Long = orderId;
            var objectItems = new List<clsOntologyItem> { newObject };
            var resultSave = await controller.SaveObjectItems(objectItems);

            if (!string.IsNullOrEmpty(request.idRelationType))
            {
                var relationTypeTask = await controller.GetOItem(request.idRelationType, controller.Globals.Type_RelationType);
                var resultTask = await controller.SaveRelations(viewModel.refItem, objectItems, relationTypeTask.Result, userItem.UserItem, groupItem.GroupItem);

                var resultOnlyObjects = new ResultObjectToView<List<clsOntologyItem>>
                {
                    IsSuccessful = true,
                    ResultItem = objectItems
                };

                LogEndMethod(processId:processId);
                return Json(resultOnlyObjects, JsonRequestBehavior.AllowGet);
            }

            var resultSaveTags = await controller.SaveTags(viewModel.refItem, objectItems, userItem.UserItem, groupItem.GroupItem, true, null);

            var result = new ResultObjectToView<ConnectorResultTypedTags>
            {
                IsSuccessful = true,
                ResultItem = resultSaveTags
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveOntoRelations(string idInstance, List<string> idsOntologyJoins)
        {
            var processId = LogStartMethod();
            var viewModel = GetAutoRelatorViewModel(idInstance);
            var memoryCache = new InMemoryCache();
            var controller = GetControllerTypedTagging();
            var securityConnector = GetControllerSecurity();
            var ontologyController = GetControllerOntology();

            var userId = User.Identity.GetUserId();
            var userItem = securityConnector.GetUser(userId);
            var groupItem = securityConnector.GetGroup(userId);

            var ontologyJoins = (from oJoinId in idsOntologyJoins
                                 join oJoin in viewModel.OntologyRanges on oJoinId equals oJoin.Id
                                 select oJoin).ToList();

            var resultSave = await controller.SaveOntoRelations(ontologyJoins, viewModel.refItem);

            var success = resultSave.ResultState.GUID != controller.Globals.LState_Error.GUID;
            var result = new ResultObjectToView<List<string>>
            {
                IsSuccessful = success,
                ResultItem = resultSave.Result.Select(res => res.OntologyJoin.OntologyJoin.JoinEntity.GUID).ToList()
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetTransformNames(List<string> idsTextParts, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetAutoRelatorViewModel(idInstance);
            var controller = GetControllerTypedTagging();
            var rangeRefItems = (from refItem in viewModel.TextRanges.SelectMany(txtRange => txtRange.RangeRefItems)
                                 join idTextPart in idsTextParts on refItem.IdTextPart equals idTextPart
                                 select refItem);

            var objectItems = rangeRefItems.Select(rangeRefItem => rangeRefItem.GetObjectItem(1)).ToList();

            var resultAction = new ResultSimpleView<List<clsOntologyItem>>
            {
                IsSuccessful = true,
                Result = new List<clsOntologyItem>()
            };

            List<string> idParents = new List<string>();
            if (objectItems.Any())
            {
                idParents = objectItems.GroupBy(obj => obj.GUID_Parent).Select(grp => grp.Key).ToList();
            }

            var transformProviders = GetNameTransformController(idParents);

            if (!transformProviders.Any())
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            foreach (var idParent in idParents)
            {
                var objects = objectItems.Where(obj => obj.GUID_Parent == idParent).ToList();
                var transformProvider = transformProviders.FirstOrDefault(trans => trans.IsResponsible(idParent));
                if (transformProvider == null) continue;
                var transformed = await transformProvider.TransformNames(objects, idClass: idParent);
                if (transformed.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    resultAction.IsSuccessful = false;
                    resultAction.Message = transformed.ResultState.Additional1;
                    return Json(resultAction, JsonRequestBehavior.AllowGet);
                }

                resultAction.Result.AddRange(transformed.Result);
            }

            var result = Json(resultAction, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId: processId);
            return result;
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveRelatorTags(string idInstance, List<string> idsTextParts, string idRelationType, int orderId)
        {
            var processId = LogStartMethod();
            var viewModel = GetAutoRelatorViewModel(idInstance);
            var controller = GetControllerTypedTagging();
            var securityConnector = GetControllerSecurity();
            var ontologyController = GetControllerOntology();

            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);
            var groupItem = await securityConnector.GetGroup(userId);

            var rangeRefItems = (from refItem in viewModel.TextRanges.SelectMany(txtRange => txtRange.RangeRefItems)
                                 join idTextPart in idsTextParts on refItem.IdTextPart equals idTextPart
                                 select refItem);

            var objectItems = rangeRefItems.Select(rangeRefItem => rangeRefItem.GetObjectItem(orderId++)).ToList();

            if (!string.IsNullOrEmpty(idRelationType))
            {
                var relationTypeTask = await controller.GetOItem(idRelationType, controller.Globals.Type_RelationType);
                var resultTask = await controller.SaveRelations(viewModel.refItem, objectItems, relationTypeTask.Result, userItem.UserItem, groupItem.GroupItem);

                var resultOnlyObjects = new ResultObjectToView<ConnectorResultTypedTags>
                {
                    IsSuccessful = true,
                    ResultItem = new ConnectorResultTypedTags
                    {
                        Tags = objectItems
                    }
                };

                LogEndMethod(processId:processId);
                return Json(resultOnlyObjects, JsonRequestBehavior.AllowGet);
            }

            var resultSave = await controller.SaveObjectItems(objectItems);

            var resultSaveTags = await controller.SaveTags(viewModel.refItem, objectItems, userItem.UserItem, groupItem.GroupItem, true, null);

            var success = resultSaveTags.Result.GUID != controller.Globals.LState_Error.GUID;
            var result = new ResultObjectToView<ConnectorResultTypedTags>
            {
                IsSuccessful = success,
                ResultItem = resultSaveTags
            };

            if (!success)
            {
                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultSaveOntology = await ontologyController.SaveClassesOfObjectsToOntology(viewModel.ontology, objectItems);


            result.IsSuccessful = resultSaveOntology.GUID != controller.Globals.LState_Error.GUID;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewTypedTaggingListReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetTypedTaggingListViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewAutoRelatorReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetAutoRelatorViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult TypedTaggingList()
        {
            var processId = LogStartMethod();
            var viewModel = GetTypedTaggingListViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult AutoRelator()
        {
            var processId = LogStartMethod();
            var viewModel = GetAutoRelatorViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult TypedTaggingGraphOmni()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTypedTaggingGraphOmni(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult AutoRelatorInit()
        {
            var processId = LogStartMethod();
            var viewModel = GetAutoRelatorViewModel(Guid.NewGuid().ToString());
            var resultAction = new ResultSimpleView<AutoRelatorViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult TypedOntoRelator()
        {
            var viewModel = GetViewModelTypedOntoRelator(Guid.NewGuid().ToString());

            return View(viewModel);
        }
    }

    public class ResultAutoRelator
    {
        public bool IsOk { get; set; }
        public string AnalyzeText { get; set; }

        public bool SearchByGUID { get; set; }
        public List<TextRange> AutoRelationAnaylze { get; set; }
    }

    public class ResultOntoRelator
    {
        public bool IsOk { get; set; }
        public string AnalyzeText { get; set; }
        public List<FoundOntologyJoin> AutoRelationAnaylze { get; set; }
    }
}