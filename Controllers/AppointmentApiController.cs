﻿using AppointmentModule.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.Requests;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OModules.Controllers
{
    [Authorize]
    public class AppointmentApiController : ApiControllerBase
    {
        

        private AppointmentModule.AppointmentConnector GetController(string idSession)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(idSession, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new AppointmentModule.AppointmentConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private SecurityModule.SecurityController GetControllerSecurity(string idSession)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(idSession, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            LogEndMethod(processId:processId);
            return new SecurityModule.SecurityController(globals);


        }

        [Route("AppointmentApi/GetAppointments")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetAppointments(GetClipboardItemsRequest request)
        {
            var processId = LogStartMethod();
            var appointmentConnector = GetController(request.idSession);
            var securityConnector = GetControllerSecurity(request.idSession);


            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);

            var resultTask = await appointmentConnector.GetAppointments(userItem.UserItem);

            LogEndMethod(processId:processId);
            return Json(resultTask.Appointments);
        }

        [Route("AppointmentApi/SaveAppointment")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> SaveAppointment(AppointmentRequestSaveAppointment request)
        {
            var processId = LogStartMethod();
            var appointmentConnector = GetController(request.idSession);
            var securityConnector = GetControllerSecurity(request.idSession);
            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);

            request.schedulerEvent.IdUser = userId;
            request.schedulerEvent.NameUser = userItem.UserItem.Name;

            await appointmentConnector.SaveAppointment(request.schedulerEvent, userItem.UserItem);

            LogEndMethod(processId:processId);
            return Json(new ResultSimple { IsSuccessful = true });
        }

        [Route("AppointmentApi/SaveAppointment")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> DeleteAppointment(AppointmentRequestSaveAppointment request)
        {

            var processId = LogStartMethod();
            var appointmentConnector = GetController(request.idSession);
            var securityConnector = GetControllerSecurity(request.idSession);
            var userId = User.Identity.GetUserId();
            var userItem = securityConnector.GetUser(userId);


            LogEndMethod(processId:processId);
            return Json(new ResultSimple { IsSuccessful = true });
        }
    }
}