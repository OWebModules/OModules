﻿using AppointmentModule.Models;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using OModules.Attributes;
using OModules.Models;
using OModules.Models.Appointment;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace OModules.Controllers
{
    public class AppointmentController : AppControllerBase
    {

        private AppointmentModule.AppointmentConnector GetController()
        {
            var processId = LogStartMethod();

            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID,"OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new AppointmentModule.AppointmentConnector(globals);

            LogEndMethod(processId:processId);

            return result;
            
        }

        private SecurityModule.SecurityController GetControllerSecurity()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            LogEndMethod(processId:processId);
            return new SecurityModule.SecurityController(globals);
            

        }

        private AppointmentViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            return memoryCache.GetOrSet<AppointmentViewModel>(Session.SessionID, key, () =>
            {
                var viewModel = new AppointmentViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new AppointmentActionConfig
                    {
                        ActionGetAppointments = Url.Action(nameof(GetAppointments)),
                        ActionCreateAppointent = Url.Action(nameof(SaveAppointment)),
                        ActionUpdateAppointment = Url.Action(nameof(SaveAppointment)),
                        ActionDestroyAppointment = Url.Action(nameof(DeleteAppointment)),
                        ActionGetDataSource = Url.Action(nameof(GetSchedulerDataSource)),
                        ActionConnected = Url.Action(nameof(Connected)),
                        ActionDisconnected = Url.Action(nameof(Disconnected)),
                        ActionSetViewReady = Url.Action(nameof(SetViewReady))
                    }
                };

                viewModel.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
                LogEndMethod(processId:processId);
                return viewModel;
            });
            
        }

        [Authorize]
        // GET: Appointment
        public ActionResult Calendar()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString());
            var appointmentConnector = GetController();
            viewModel.IdParentAppointment = appointmentConnector.LocalConfig.OItem_type_appointment.GUID;
            viewModel.TypeObject = appointmentConnector.LocalConfig.Globals.Type_Object;

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var viewItemValue = result.ViewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetSchedulerDataSource(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var appointmentConnector = GetController();
            
            var dataSource = await appointmentConnector.GetDataSource(viewModel.ActionConfig.ActionGetAppointments, viewModel.ActionConfig.ActionCreateAppointent, viewModel.ActionConfig.ActionUpdateAppointment, viewModel.ActionConfig.ActionDestroyAppointment);

            LogEndMethod(processId:processId);
            return Json(dataSource.DataSource, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [JsonNetFilter]
        public async System.Threading.Tasks.Task<ActionResult> GetAppointments(string idInstance)
        {
            var processId = LogStartMethod();
            var appointmentConnector = GetController();
            var securityConnector = GetControllerSecurity();

            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);

            var resultTask = await appointmentConnector.GetAppointments(userItem.UserItem);

            LogEndMethod(processId:processId);
            return Json(resultTask.Appointments, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [JsonNetFilter]
        public async System.Threading.Tasks.Task<ActionResult> SaveAppointment(AppointmentScheduler schedulerEvent)
        {
            var processId = LogStartMethod();
            var appointmentConnector = GetController();
            var securityConnector = GetControllerSecurity();
            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);

            await appointmentConnector.SaveAppointment(schedulerEvent, userItem.UserItem);

            LogEndMethod(processId:processId);
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [JsonNetFilter]
        public ActionResult DeleteAppointment(AppointmentScheduler schedulerEvent)
        {

            var processId = LogStartMethod();
            var appointmentConnector = GetController();
            var securityConnector = GetControllerSecurity();
            var userId = User.Identity.GetUserId();
            var userItem = securityConnector.GetUser(userId);


            LogEndMethod(processId:processId);
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.rootContainer.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.contentContainer.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.scheduler.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.rootContainer);
            viewItemList.Add(viewModel.contentContainer);
            viewItemList.Add(viewModel.scheduler);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.rootContainer.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.contentContainer.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.scheduler.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            viewItemList.Add(viewModel.rootContainer);
            viewItemList.Add(viewModel.contentContainer);
            viewItemList.Add(viewModel.scheduler);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }
    }
}