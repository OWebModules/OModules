﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WordPress_Module;
using WordPressSyncModule.Models;

namespace OModules.Controllers
{
    public class WordPressSyncController : AppControllerBase
    {
        

        private SecurityModule.SecurityController GetControllerSecurity()
        {

            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new SecurityModule.SecurityController(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private WordPressSyncModule.WordpressSyncConnector GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new WordPressSyncModule.WordpressSyncConnector(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private WordpressSyncViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<WordpressSyncViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new WordpressSyncViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(Connected)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedPasswordSafe)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReady)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfigPostItems)),
                    ActionGetPostItems = Url.Action(nameof(GetPostItems)),
                    ActionGetUsernameAndPassword = Url.Action(nameof(GetUsernameAndPassword)),
                    ActionValidatePassword = Url.Action(nameof(ValidatePassword)),
                    ActionGetUserBlogs = Url.Action(nameof(GetUserBlogs)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetUsernameAndPassword(string idWebService, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var viewModel = GetViewModel(idInstance);

            result.ViewItems.Add(viewModel.baseUrlInp);
            result.ViewItems.Add(viewModel.userNameInp);
            result.ViewItems.Add(viewModel.passwordInp);
            result.ViewItems.Add(viewModel.buttonGetBlogs);
            viewModel.buttonGetBlogs.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            if (string.IsNullOrEmpty(idWebService))
            {
                viewModel.buttonGetBlogs.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            if (viewModel.WebServices == null && !viewModel.WebServices.Any())
            {
                viewModel.buttonGetBlogs.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var webService = viewModel.WebServices.FirstOrDefault(webServ => webServ.Guid == idWebService);

            if (webService == null)
            {
                viewModel.buttonGetBlogs.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.SelectedWebService = webService;
            viewModel.baseUrlInp.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.SelectedWebService.UrlItem.Name);
            viewModel.userNameInp.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.SelectedWebService.UserItem.Name);
            viewModel.passwordInp.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.SelectedWebService.Password);

            if (string.IsNullOrEmpty(webService.Password))
            {
                viewModel.buttonGetBlogs.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewModel = GetViewModel(idInstance);
            var controller = GetController();

            

            var resultGetWebservices = await controller.GetWebServices();

            var result = new ResultSimpleView<List<ViewItem>>
            {
                IsSuccessful = resultGetWebservices.ResultState.GUID == controller.Globals.LState_Success.GUID,
                Result = new List<ViewItem>()
            };

            if (resultGetWebservices.ResultState.GUID == controller.Globals.LState_Error.GUID )
            {
                result.Result = GetViewItems(viewModel, false);

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var securityController = GetControllerSecurity();

            result.Result.Add(viewModel.authenticationWindow);
            if (securityController.IsAuthenticated)
            {
                viewModel.authenticationWindow.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
            }
            else
            {
                viewModel.authenticationWindow.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
            }

            viewModel.WebServices = resultGetWebservices.Result;
            var resultGetDropdownConfig = await controller.GetDropDownConfigForWebservices(viewModel.WebServices);

            if (resultGetDropdownConfig.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.Result = GetViewItems(viewModel, false);

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result.AddRange(GetViewItems(viewModel, true));

            result.Result.Add(viewModel.webserviceDropdown);

            viewModel.webserviceDropdown.ChangeViewItemValue(ViewItemType.DataSource.ToString(), resultGetDropdownConfig.Result);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedPasswordSafe()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            
            var viewModel = GetViewModel(idInstance);

            var viewItemList = GetViewItems(viewModel, true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        private List<ViewItem> GetViewItems(WordpressSyncViewModel viewModel, bool enable)
        {
            var processId = LogStartMethod();
            var viewItemList = new List<ViewItem>();

            viewItemList.Add(viewModel.baseUrlInp);
            viewItemList.Add(viewModel.passwordInp);
            viewItemList.Add(viewModel.userNameInp);

            viewItemList.Add(viewModel.blogsDropdown);
            viewItemList.Add(viewModel.webserviceDropdown);

            viewItemList.Add(viewModel.buttonGetBlogs);
            viewItemList.Add(viewModel.buttonPostList);

            viewModel.baseUrlInp.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.passwordInp.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.userNameInp.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            viewModel.blogsDropdown.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.webserviceDropdown.ChangeViewItemValue(ViewItemType.Enable.ToString(), enable);

            viewModel.buttonGetBlogs.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.buttonPostList.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return viewItemList;
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidatePassword(string idInstance, string password)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            var controller = GetControllerSecurity();

            var userId = User.Identity.GetUserId();
            var userItemResult = await controller.GetUser(userId);

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>
                {
                    viewModel.alertWrongPassword
                }
            };
            if (userItemResult.Result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.alertWrongPassword.ChangeViewItemValue(ViewItemType.Content.ToString(), Primitives.Messages.General_ErrorOccured);
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultValidate = await controller.ValidatePassword(userItemResult.UserItem, password);

            result.IsSuccessful = resultValidate.Result.GUID != controller.Globals.LState_Error.GUID;

            if (!result.IsSuccessful)
            {
                viewModel.alertWrongPassword.ChangeViewItemValue(ViewItemType.Content.ToString(), Primitives.Messages.SecurityController_PasswordWrong);
            }

            viewModel.MasterPassword = password;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetUserBlogs(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            var secController = GetControllerSecurity();
            var controller = GetController();

            var userId = User.Identity.GetUserId();
            var userItemResult = secController.GetUser(userId);

            var baseUrl = viewModel.SelectedWebService.UrlItem.Name;
            var userName = viewModel.SelectedWebService.UserItem.Name;

            var passwordTask = await secController.GetPassword(viewModel.SelectedWebService.UserItem, viewModel.MasterPassword);

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (passwordTask.Result.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            var credentialItem = passwordTask.CredentialItems.FirstOrDefault();

            

            if (credentialItem == null)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var getUserBlogsRequest = new WordPressSyncModule.Models.GetUserBlogsRequest(baseUrl, userName, credentialItem.Password.Name_Other);
            var userBlogsResult = await controller.GetUserBlogs(getUserBlogsRequest);

            if (userBlogsResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.UserBlogs = userBlogsResult.Result;

            var dropdownConfig = await controller.GetDropDownConfigForUserBlogs(viewModel.UserBlogs);

            if (dropdownConfig.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.blogsDropdown.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.blogsDropdown.ChangeViewItemValue(ViewItemType.DataSource.ToString(), dropdownConfig.Result);

            result.ViewItems.Add(viewModel.blogsDropdown);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigPostItems()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(WordPressSyncModule.Models.GridPostItem), Url.Action(nameof(GetGridData)), null, null, null, false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGridData(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            if (viewModel.GridPostItems == null)
            {
                viewModel.GridPostItems = new List<GridPostItem>();
            }

            var resultItem = new KendoGridDataResult<GridPostItem>(viewModel.GridPostItems);
            var resultJson = Json(resultItem, JsonRequestBehavior.AllowGet);
            resultJson.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return resultJson;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetPostItems(string idInstance, string userBlogId)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            if (string.IsNullOrEmpty(userBlogId) || userBlogId == "undefined" || viewModel.UserBlogs == null)
            {
                var resultJsonEmpty = Json(result, JsonRequestBehavior.AllowGet);
                resultJsonEmpty.MaxJsonLength = int.MaxValue;
                return resultJsonEmpty;
            }

            
            var controller = GetController();
            var secController = GetControllerSecurity();

            viewModel.SelectedUserBlog = viewModel.UserBlogs.FirstOrDefault(usrBlog => usrBlog.Guid == userBlogId);

            if (viewModel.SelectedUserBlog == null)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var userId = User.Identity.GetUserId();
            var userItemResult = secController.GetUser(userId);

            var baseUrl = viewModel.SelectedWebService.UrlItem.Name;
            var userName = viewModel.SelectedWebService.UserItem.Name;

            var passwordTask = await secController.GetPassword(viewModel.SelectedWebService.UserItem, viewModel.MasterPassword);

            if (passwordTask.Result.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            var credentialItem = passwordTask.CredentialItems.FirstOrDefault();



            if (credentialItem == null)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            var getPostListRequest = new GetPostListRequest(viewModel.SelectedUserBlog, baseUrl, userName, credentialItem.Password.Name_Other);
            var resultTask = await controller.GetPostList(getPostListRequest);

            if (resultTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.GridPostItems = resultTask.Result.Select(postItm => new GridPostItem(postItm)).ToList();
            
            var resultJson = Json(result, JsonRequestBehavior.AllowGet);
            resultJson.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return resultJson;
        }

        // GET: WordPressSync
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult WordPressSync()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }
    }
}