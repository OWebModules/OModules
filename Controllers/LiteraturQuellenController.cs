﻿using LiteraturQuelleModule.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class LiteraturQuellenController : AppControllerBase
    {
        

        private InternetQuelleViewModel GetViewModelÍnternetQuelle(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<InternetQuelleViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new InternetQuelleViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedInternetQuelle)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedInternetQuelle)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyInternetQuelle)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceInternetQuelle)),
                    ActionMediaListPDFInit = Url.Action(nameof(MediaViewerController.MediaListPDFInit), nameof(MediaViewerController).Replace("Controller", "")),
                    ActionSingleRelationInit = Url.Action(nameof(ObjectEditController.SingleRelationInit), nameof(ObjectEditController).Replace("Controller", "")),
                    ActionObjectListInit = Url.Action(nameof(OItemListController.ObjectListInit), nameof(OItemListController).Replace("Controller", "")),
                    ActionLiteraturQuelleCreateInit = Url.Action(nameof(LiteraturQuelleCreatorInit)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueInternetQuelle)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private BuchQuelleViewModel GetViewModelBuchQuelle(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<BuchQuelleViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new BuchQuelleViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedBuchQuelle)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedBuchQuelle)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyBuchQuelle)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceBuchQuelle)),
                    ActionLiteraturQuelleCreateInit = Url.Action(nameof(LiteraturQuelleCreatorInit)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueBuchQuelle)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private LiteraturQuelleCreateViewModel GetViewModelLiteraturQuelleCreator(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<LiteraturQuelleCreateViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new LiteraturQuelleCreateViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedLiteraturQuelleCreator)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedLiteraturQuelleCreator)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyLiteraturQuelleCreator)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceLiteraturQuelleCreator)),
                    ActionGetDropDownConfigQuellTypes = Url.Action(nameof(DropDownController.GetDropDownConfigForObjects), nameof(DropDownController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueInternetQuelle)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private LiteraturQuelleModule.LiteraturQuelleController GetControllerLiteraturquellen()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new LiteraturQuelleModule.LiteraturQuelleController(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private SecurityModule.SecurityController GetControllerSecurity()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            LogEndMethod(processId:processId);
            return new SecurityModule.SecurityController(globals);

        }

        [Authorize]
        public ActionResult ConnectedInternetQuelle()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelÍnternetQuelle(idInstance);

            viewItemList.Add(viewModel.labelName);
            viewItemList.Add(viewModel.inputName);

            viewItemList.Add(viewModel.labelUrl);
            viewItemList.Add(viewModel.inputUrl);
            viewItemList.Add(viewModel.buttonUrl);

            viewItemList.Add(viewModel.labelCreator);
            viewItemList.Add(viewModel.inputCreator);
            viewItemList.Add(viewModel.buttonCreator);

            viewItemList.Add(viewModel.labelDownloadstamp);
            viewItemList.Add(viewModel.inputDownloadstamp);

            viewModel.buttonUrl.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.buttonCreator.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.inputDownloadstamp.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.inputName.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedInternetQuelle()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelÍnternetQuelle(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public ActionResult ConnectedBuchQuelle()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelBuchQuelle(idInstance);

            viewItemList.Add(viewModel.labelName);
            viewItemList.Add(viewModel.inputName);

            viewItemList.Add(viewModel.labelLiteratur);
            viewItemList.Add(viewModel.inputLiteratur);
            viewItemList.Add(viewModel.buttonLiteratur);

            viewItemList.Add(viewModel.labelSeite);
            viewItemList.Add(viewModel.inputSeite);

            viewModel.buttonLiteratur.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.inputName.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedBuchQuelle()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelBuchQuelle(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }



        [Authorize, HttpPost]
        public ActionResult SetViewReadyInternetQuelle(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelÍnternetQuelle(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyBuchQuelle(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelBuchQuelle(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedLiteraturQuelleCreator()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelLiteraturQuelleCreator(idInstance);

            viewItemList.Add(viewModel.labelName);
            viewItemList.Add(viewModel.inputName);

            viewItemList.Add(viewModel.labelQuellType);
            viewItemList.Add(viewModel.dropDownQuellType);

            viewModel.inputName.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.dropDownQuellType.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedLiteraturQuelleCreator()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelLiteraturQuelleCreator(idInstance);

            viewItemList.Add(viewModel.labelName);
            viewItemList.Add(viewModel.inputName);

            viewItemList.Add(viewModel.labelQuellType);
            viewItemList.Add(viewModel.dropDownQuellType);

            viewModel.inputName.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.dropDownQuellType.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyLiteraturQuelleCreator(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelLiteraturQuelleCreator(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueInternetQuelle(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelÍnternetQuelle(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            if (result.ViewItem.ViewItemId == nameof(InternetQuelleViewModel.inputName))
            {

                var name = viewItem.GetViewItemValueItem(ViewItemType.Content.ToString());
                if (name != null && name.Value != null)
                {
                    var securityConnector = GetControllerSecurity();

                    var userId = User.Identity.GetUserId();
                    var userItem = await securityConnector.GetUser(userId);
                    var groupItem = await securityConnector.GetGroup(userId);



                    var changeRequest = new SaveNameInternetQuelleRequest
                    {
                        InternetQuelle = viewModel.InternetQuelle?.InternetQuelle,
                        LiteraturQuelle = viewModel.InternetQuelle?.LiteraturQuelle,
                        Autor = viewModel.InternetQuelle?.Autor,
                        RefItem = viewModel.refItem.ObjectItem,
                        Url = viewModel.InternetQuelle?.Url,
                        LogEntry = viewModel.InternetQuelle?.LogEntry,
                        UserItem = userItem.UserItem,
                        GroupItem = groupItem.GroupItem,
                        Name = ((string[])name.Value)[0]

                    };

                    var controller = GetControllerLiteraturquellen();

                    var saveResult = await controller.SaveNameInternetQuelle(changeRequest);

                    if (saveResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        if (viewModel.InternetQuelle != null && viewModel.InternetQuelle.InternetQuelle != null)
                        {
                            result.ViewItem.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.InternetQuelle.InternetQuelle.Name);
                        }
                        else
                        {
                            result.ViewItem.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                        }
                    }
                }
                else
                {
                    if (viewModel.InternetQuelle != null && viewModel.InternetQuelle.InternetQuelle != null)
                    {
                        result.ViewItem.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.InternetQuelle.InternetQuelle.Name);
                    }
                    else
                    {
                        result.ViewItem.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                    }
                    
                }

                resultAction.ViewItems.Add(result.ViewItem);
            }
            else if (result.ViewItem.ViewItemId == nameof(InternetQuelleViewModel.inputDownloadstamp))
            {
                var downloadStampObj = viewItem.GetViewItemValueItem(ViewItemType.Content.ToString());

                if (downloadStampObj != null && downloadStampObj.Value != null)
                {
                    var downloadStamp = DateTime.Parse(((string[])downloadStampObj.Value)[0]);

                    var controller = GetControllerLiteraturquellen();
                    var securityConnector = GetControllerSecurity();

                    var userId = User.Identity.GetUserId();
                    var userItem = await securityConnector.GetUser(userId);
                    var groupItem = await securityConnector.GetGroup(userId);

                    var saveRequest = new LiteraturQuelleModule.Models.SaveDownloadStampRequest
                    {
                        downloadStamp = downloadStamp,
                        RefItem = viewModel.refItem?.ObjectItem,
                        InternetQuelle = viewModel.InternetQuelle?.InternetQuelle,
                        LiteraturQuelle = viewModel.InternetQuelle?.LiteraturQuelle,
                        UserItem = userItem.Result,
                        GroupItem = groupItem.Result
                    };
                    var resultSaveDownloadStamp = await controller.SaveDownloadStamp(saveRequest);
                }

                resultAction.ViewItems.Add(result.ViewItem);

            }
            else if (result.ViewItem.ViewItemId == nameof(InternetQuelleViewModel.hiddenReference))
            {
                var refObj = viewItem.GetViewItemValueItem(ViewItemType.Other.ToString());

                if (refObj != null && refObj.Value != null)
                {
                    var idRel = ((string[])refObj.Value)[0].ToString();

                    var controller = GetControllerLiteraturquellen();
                    var securityConnector = GetControllerSecurity();

                    var userId = User.Identity.GetUserId();
                    var userItem = await securityConnector.GetUser(userId);
                    var groupItem = await securityConnector.GetGroup(userId);

                    var referenceResult = await controller.GetOItem(idRel);

                    var saveRequest = new LiteraturQuelleModule.Models.SaveInternetRelRequest
                    {
                        IdRel = referenceResult.Result.GUID,
                        RefItem = viewModel.refItem?.ObjectItem,
                        InternetQuelle = viewModel.InternetQuelle?.InternetQuelle,
                        LiteraturQuelle = viewModel.InternetQuelle?.LiteraturQuelle,
                        UserItem = userItem.Result,
                        GroupItem = groupItem.Result
                    };
                    var resultSaveReference = await controller.SaveInternetReference(saveRequest);

                    if (resultSaveReference.ResultState.GUID == controller.Globals.LState_Success.GUID)
                    {
                        if (referenceResult.Result.GUID_Parent == controller.IdClassUrl)
                        {
                            viewModel.InternetQuelle.Url = resultSaveReference.Result.Url;
                            resultAction.ViewItems.Add(viewModel.inputUrl);
                            viewModel.inputUrl.ChangeViewItemValue(ViewItemType.Content.ToString(), resultSaveReference.Result.Url.Name);
                        }
                        else
                        {
                            viewModel.InternetQuelle.Autor = resultSaveReference.Result.Autor;
                            resultAction.ViewItems.Add(viewModel.inputCreator);
                            viewModel.inputCreator.ChangeViewItemValue(ViewItemType.Content.ToString(), resultSaveReference.Result.Autor.Name);
                        }
                        
                    }
                }

            }
            

            

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueBuchQuelle(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelBuchQuelle(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            if (result.ViewItem.ViewItemId == nameof(InternetQuelleViewModel.inputName))
            {

                var name = viewItem.GetViewItemValueItem(ViewItemType.Content.ToString());
                if (name != null && name.Value != null)
                {
                    var securityConnector = GetControllerSecurity();

                    var userId = User.Identity.GetUserId();
                    var userItem = await securityConnector.GetUser(userId);
                    var groupItem = await securityConnector.GetGroup(userId);



                    var changeRequest = new SaveNameBuchQuelleRequest
                    {
                        BuchQuelle = viewModel.BuchQuelle?.BuchQuelle,
                        LiteraturQuelle = viewModel.BuchQuelle?.LiteraturQuelle,
                        Seite = viewModel.BuchQuelle?.Seite,
                        RefItem = viewModel.refItem.ObjectItem,
                        Literatur = viewModel.BuchQuelle?.Literatur,
                        Name = ((string[])name.Value)[0]

                    };

                    var controller = GetControllerLiteraturquellen();

                    var saveResult = await controller.SaveNameBuchQuelle(changeRequest);

                    if (saveResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        if (viewModel.BuchQuelle != null && viewModel.BuchQuelle.BuchQuelle != null)
                        {
                            result.ViewItem.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.BuchQuelle.BuchQuelle.Name);
                        }
                        else
                        {
                            result.ViewItem.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                        }
                    }
                }
                else
                {
                    if (viewModel.BuchQuelle != null && viewModel.BuchQuelle.BuchQuelle != null)
                    {
                        result.ViewItem.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.BuchQuelle.BuchQuelle.Name);
                    }
                    else
                    {
                        result.ViewItem.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                    }

                }

                resultAction.ViewItems.Add(result.ViewItem);
            }
            else if (result.ViewItem.ViewItemId == nameof(BuchQuelleViewModel.inputSeite))
            {
                var seiteObj = viewItem.GetViewItemValueItem(ViewItemType.Content.ToString());

                if (seiteObj != null && seiteObj.Value != null)
                {
                    var seite = ((string[])seiteObj.Value)[0];

                    var controller = GetControllerLiteraturquellen();

                    var saveRequest = new LiteraturQuelleModule.Models.SaveSeiteRequest
                    {
                        Seiten = seite,
                        RefItem = viewModel.refItem?.ObjectItem,
                        BuchQuelle = viewModel.BuchQuelle?.BuchQuelle,
                        LiteraturQuelle = viewModel.BuchQuelle?.LiteraturQuelle
                    };
                    var resultSaveSeite = await controller.SaveSeite(saveRequest);
                }

                resultAction.ViewItems.Add(result.ViewItem);

            }
            else if (result.ViewItem.ViewItemId == nameof(BuchQuelleViewModel.hiddenReference))
            {
                var refObj = viewItem.GetViewItemValueItem(ViewItemType.Other.ToString());

                if (refObj != null && refObj.Value != null)
                {
                    var idRel = ((string[])refObj.Value)[0].ToString();

                    var controller = GetControllerLiteraturquellen();
                    
                    var referenceResult = await controller.GetOItem(idRel);

                    var saveRequest = new LiteraturQuelleModule.Models.SaveBuchRelRequest
                    {
                        IdRel = referenceResult.Result.GUID,
                        RefItem = viewModel.refItem?.ObjectItem,
                        BuchQuelle = viewModel.BuchQuelle?.BuchQuelle,
                        LiteraturQuelle = viewModel.BuchQuelle?.LiteraturQuelle,
                    };
                    var resultSaveReference = await controller.SaveBuchReference(saveRequest);

                    if (resultSaveReference.ResultState.GUID == controller.Globals.LState_Success.GUID)
                    {
                        if (referenceResult.Result.GUID_Parent == controller.IdClassLiteratur)
                        {
                            viewModel.BuchQuelle.Literatur = resultSaveReference.Result.Literatur;
                            resultAction.ViewItems.Add(viewModel.inputLiteratur);
                            viewModel.inputLiteratur.ChangeViewItemValue(ViewItemType.Content.ToString(), resultSaveReference.Result.Literatur.Name);
                        }

                    }
                }

            }




            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceInternetQuelle(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelÍnternetQuelle(idInstance);
            var controller = GetControllerLiteraturquellen();

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var resultGetRefItem = await controller.GetClassObject(refItem.GUID);

            if (resultGetRefItem.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.refItem = resultGetRefItem.Result;

            var resultGetLiteraturQuelle = await controller.GetInternetQuelle(viewModel.refItem.ObjectItem);

            if (resultGetLiteraturQuelle.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (resultGetLiteraturQuelle.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
            {
                result.ViewItems.Add(viewModel.askForCreation);
                result.ResultType = ResultViewItemsType.NoReference;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.InternetQuelle = resultGetLiteraturQuelle.Result;
            if (viewModel.InternetQuelle.InternetQuelle == null)
            {
                result.ResultType = ResultViewItemsType.NoReference;
            }
            viewModel.refItem = resultGetRefItem.Result;

            result.ViewItems.Add(viewModel.inputCreator);
            result.ViewItems.Add(viewModel.inputDownloadstamp);
            result.ViewItems.Add(viewModel.inputUrl);
            result.ViewItems.Add(viewModel.buttonCreator);
            result.ViewItems.Add(viewModel.buttonUrl);
            result.ViewItems.Add(viewModel.inputName);
            result.ViewItems.Add(viewModel.hiddenIdClassPartner);
            result.ViewItems.Add(viewModel.hiddenIdClassUrl);
            result.ViewItems.Add(viewModel.windowTitle);


            viewModel.inputCreator.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.inputDownloadstamp.ChangeViewItemValue(ViewItemType.Content.ToString(), null);
            viewModel.inputUrl.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.inputName.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.hiddenIdClassPartner.ChangeViewItemValue(ViewItemType.Content.ToString(), controller.IdClassPartner);
            viewModel.hiddenIdClassUrl.ChangeViewItemValue(ViewItemType.Content.ToString(), controller.IdClassUrl);
            viewModel.SetWindowTitle($"{viewModel.refItem.ObjectItem.Name} ({viewModel.refItem.ClassItem.Name})");

            if (viewModel.InternetQuelle != null)
            {
                if (viewModel.InternetQuelle != null)
                {
                    viewModel.inputName.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.InternetQuelle.InternetQuelle.Name);
                }
                
                if (viewModel.InternetQuelle.LogEntry != null)
                {
                    viewModel.inputDownloadstamp.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.InternetQuelle.LogEntry.DateTimeStamp);
                }

                if (viewModel.InternetQuelle.Autor != null)
                {
                    viewModel.inputCreator.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.InternetQuelle.Autor.Name);
                }

                if (viewModel.InternetQuelle.Url != null)
                {
                    viewModel.inputUrl.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.InternetQuelle.Url.Name);
                }
            }

            
            viewModel.buttonCreator.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.buttonUrl.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.inputName.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.inputDownloadstamp.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceBuchQuelle(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelBuchQuelle(idInstance);
            var controller = GetControllerLiteraturquellen();

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var resultGetRefItem = await controller.GetClassObject(refItem.GUID);

            if (resultGetRefItem.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.refItem = resultGetRefItem.Result;

            var resultGetLiteraturQuelle = await controller.GetBuchQuelle(viewModel.refItem.ObjectItem);

            if (resultGetLiteraturQuelle.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (resultGetLiteraturQuelle.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
            {
                result.ViewItems.Add(viewModel.askForCreation);
                result.ResultType = ResultViewItemsType.NoReference;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.BuchQuelle = resultGetLiteraturQuelle.Result;
            if (viewModel.BuchQuelle.BuchQuelle == null)
            {
                result.ResultType = ResultViewItemsType.NoReference;
            }

            

            viewModel.refItem = resultGetRefItem.Result;

            result.ViewItems.Add(viewModel.inputLiteratur);
            result.ViewItems.Add(viewModel.inputSeite);
            result.ViewItems.Add(viewModel.inputName);
            result.ViewItems.Add(viewModel.buttonLiteratur);
            result.ViewItems.Add(viewModel.hiddenIdClassLiteratur);
            result.ViewItems.Add(viewModel.windowTitle);


            viewModel.inputLiteratur.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.inputSeite.ChangeViewItemValue(ViewItemType.Content.ToString(), null);
            viewModel.inputName.ChangeViewItemValue(ViewItemType.Content.ToString(), null);
            viewModel.hiddenIdClassLiteratur.ChangeViewItemValue(ViewItemType.Content.ToString(), controller.IdClassLiteratur);
            viewModel.SetWindowTitle($"{viewModel.refItem.ObjectItem.Name} ({viewModel.refItem.ClassItem.Name})");

            if (viewModel.BuchQuelle != null)
            {
                if (viewModel.BuchQuelle != null)
                {
                    viewModel.inputName.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.BuchQuelle.BuchQuelle.Name);
                }

                if (viewModel.BuchQuelle.Seite != null)
                {
                    viewModel.inputSeite.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.BuchQuelle.Seite.Val_String);
                }

                if (viewModel.BuchQuelle.Literatur != null)
                {
                    viewModel.inputLiteratur.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.BuchQuelle.Literatur.Name);
                }

            }


            viewModel.buttonLiteratur.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.inputName.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.inputSeite.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceLiteraturQuelleCreator(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelLiteraturQuelleCreator(idInstance);
            var controller = GetControllerLiteraturquellen();

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var resultGetRefItem = await controller.GetClassObject(refItem.GUID);

            if (resultGetRefItem.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.refItem = resultGetRefItem.Result;

            result.ViewItems.Add(viewModel.labelReference);

            if (viewModel.refItem != null)
            {
                viewModel.labelReference.ChangeViewItemValue(ViewItemType.Content.ToString(), $"{viewModel.refItem.ObjectItem.Name} ({viewModel.refItem.ClassItem.Name})");
            }
            else
            {
                viewModel.labelReference.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        // GET: LiteraturQuellen
        public ActionResult InternetQuelle()
        {
            var viewModel = GetViewModelÍnternetQuelle(Guid.NewGuid().ToString());
            var controller = GetControllerLiteraturquellen();
            viewModel.IdDirection = controller.IdDirectionQuelle;
            viewModel.IdRelationType = controller.IdRelationTypeQuelle;
            viewModel.IdParentRelation = controller.IdParentRelationLiterarischeQuelle;
            return View(viewModel);
        }

        [Authorize]
        // GET: LiteraturQuellen
        public ActionResult BuchQuelle()
        {
            var viewModel = GetViewModelBuchQuelle(Guid.NewGuid().ToString());
            var controller = GetControllerLiteraturquellen();
            viewModel.IdDirection = controller.IdDirectionQuelle;
            viewModel.IdRelationType = controller.IdRelationTypeQuelle;
            viewModel.IdParentRelation = controller.IdParentRelationLiterarischeQuelle;
            return View(viewModel);
        }

        [Authorize]
        // GET: LiteraturQuellen
        public ActionResult LiteraturQuelleCreatorInit()
        {
            var processId = LogStartMethod();            var viewModel = GetViewModelLiteraturQuelleCreator(Guid.NewGuid().ToString());
            var controller = GetControllerLiteraturquellen();
            viewModel.idClassQuellTypes = controller.IdClassQuellType;
            var resultAction = new ResultSimpleView<LiteraturQuelleCreateViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        // GET: LiteraturQuellen
        public ActionResult Index()
        {
            return View();
        }
    }
}