﻿using HtmlEditorModule.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OModules.Models.MessageOutput;
using OModules.Primitives;
using OntoMsg_Module;
using TypedTaggingModule.Models;

namespace OModules.Controllers
{
    public class HtmlEditController : AppControllerBase
    {
        

        private clsLogStates logStates = new clsLogStates();
        private clsTypes types = new clsTypes();

        private HtmlEditorModule.HtmlEditorConnector GetControllerHtmlEdit()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new HtmlEditorModule.HtmlEditorConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private SecurityModule.SecurityController GetControllerSecurity()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            LogEndMethod(processId:processId);
            return new SecurityModule.SecurityController(globals);

        }

        private MediaViewerModule.Connectors.MediaViewerConnector GetControllerMedia()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            LogEndMethod(processId:processId);
            return new MediaViewerModule.Connectors.MediaViewerConnector(globals);

        }

        private HtmlEditViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<HtmlEditViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new HtmlEditViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigHtmlEdit
                    {
                        ActionConnected = Url.Action(nameof(Connected)),
                        ActionDisconnected = Url.Action(nameof(Disconnected)),
                        ActionSetViewReady = Url.Action(nameof(SetViewReady)),
                        ActionValidateReference = Url.Action(nameof(ValidateReference)),
                        ValidateReceivedRefForLinkOrAnchor = Url.Action(nameof(ValidateReceivedRefForLinkOrAnchor)),
                        ActionGetHtmlContent = Url.Action(nameof(GetHtmlContent)),
                        ActionSaveHtmlContent = Url.Action(nameof(SaveHtmlContent)),
                        ActionGetTags = Url.Action(nameof(GetTags)),
                        ActionRelationEditor = Url.Action(nameof(ObjectEditController.RelationEditor), nameof(ObjectEditController).Replace("Controller", "")),
                        ActionHtmlViewer = Url.Action(nameof(HtmlViewerController.DocViewer), nameof(HtmlViewerController).Replace("Controller", "")),
                        ActionDiffViewer = Url.Action(nameof(HtmlHistory)),
                        ActionModuleStarterInit = Url.Action(nameof(ModuleMgmtController.ModuleStarterInit), nameof(ModuleMgmtController).Replace("Controller", "")),
                        ActionValidateReferenceTemplate = Url.Action(nameof(ValidateReferenceHtmlTemplate)),
                        ActionGetGridConfigTemplate = Url.Action(nameof(GetGridConfigTemplate)),
                        ActionGetTemplateContent = Url.Action(nameof(GetHtmlTemplateContent)),
                        ActionAutoRelator = Url.Action(nameof(TypedTaggingController.AutoRelator), nameof(TypedTaggingController).Replace("Controller", "")),
                        ActionOpenMediaViewerImage = Url.Action(nameof(MediaViewerController.MediaListImage), nameof(MediaViewerController).Replace("Controller", "")),
                        ActionOpenMediaViewerPDF = Url.Action(nameof(MediaViewerController.MediaListPDF), nameof(MediaViewerController).Replace("Controller", "")),
                        ActionOpenMediaViewerVideo = Url.Action(nameof(MediaViewerController.MediaListVideo), nameof(MediaViewerController).Replace("Controller", "")),
                        ActionGetUpdatedName = Url.Action(nameof(GetUpdatedName))
                    },
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private HtmlHistoryViewModel GetViewModelHtmlHistory(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<HtmlHistoryViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new HtmlHistoryViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedHtmlHistory)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedHtmlHistory)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyHtmlHistory)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceHtmlHistory)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfigHtmlHistory)),
                    ActionGetHtmlCodeCombi = Url.Action(nameof(GetHTMLCodeCombi)),
                    ActionDeleteHisotryItems = Url.Action(nameof(DeleteHistoryItems)),
                    ActionGetHtmlForPreview = Url.Action(nameof(GetHtmlForPreview)),
                    ActionRestoreHistoryEntry = Url.Action(nameof(RestoreHistoryEntry)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueHtmlHistory)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private HtmlSearchViewModel GetViewModelHtmlSearch(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<HtmlSearchViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new HtmlSearchViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedHtmlSearch)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedHtmlSearch)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyHtmlSearch)),
                    ActionGetGridConfigSearch = Url.Action(nameof(GetGridConfigHtmlSearch)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueHtmlSearch)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });


            return viewModel;
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReceivedRefForLinkOrAnchor(List<clsOntologyItem> refItems, bool insertLink, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var controller = GetControllerHtmlEdit();
            var messageOutput = new OWMessageOutput(viewModel.IdInstance);

            if (refItems.Any(refItem => string.IsNullOrEmpty(refItem.GUID))) return Json(null, JsonRequestBehavior.AllowGet);
            if (viewModel.oItemHtml == null) return Json(null, JsonRequestBehavior.AllowGet);

            var securityConnector = GetControllerSecurity();
            var mediaConnector = GetControllerMedia();

            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);
            var groupItem = await securityConnector.GetGroup(userId);

            var refItemsFull = await controller.GetObjectItemsWithGuid(refItems);
            var baseUrl = Url.Action(nameof(ModuleMgmtController.ModuleStarter), nameof(ModuleMgmtController).Replace("Controller", ""));

            var result = new ResultSimpleView<List<LinkItem>>
            {
                IsSuccessful = true
            };

            var mediaType = refItemsFull.Result.All(refItem => refItem.GUID_Parent == MediaViewerModule.Connectors.MediaViewerConnector.ClassImages.GUID) ? MediaViewerModule.Primitives.MultimediaItemType.Image :
                refItems.All(refItem => refItem.GUID_Parent == MediaViewerModule.Connectors.MediaViewerConnector.ClassMediaItems.GUID) ? MediaViewerModule.Primitives.MultimediaItemType.VideoAndAudio :
                refItems.All(refItem => refItem.GUID_Parent == MediaViewerModule.Connectors.MediaViewerConnector.ClassPDF.GUID) ? MediaViewerModule.Primitives.MultimediaItemType.PDF : MediaViewerModule.Primitives.MultimediaItemType.Unknown;

            if (mediaType == MediaViewerModule.Primitives.MultimediaItemType.VideoAndAudio ||
                mediaType == MediaViewerModule.Primitives.MultimediaItemType.Image ||
                mediaType == MediaViewerModule.Primitives.MultimediaItemType.PDF)
            {

                var mediaListItemsResult = await mediaConnector.GetMediaListItems(refItems, Server.MapPath($"~/Resources/UserGroupRessources"), Url.Content($"~/Resources/UserGroupRessources"), mediaType);
                if (mediaListItemsResult.Result.GUID == controller.Globals.LState_Error.GUID)
                {
                    LogError("Error while getting MediaItems", processId: processId);
                    result.IsSuccessful = false;
                    Json(result, JsonRequestBehavior.AllowGet);
                }

                var resultMediaListItems = await controller.GetLinkItemsByMediaListItems(mediaListItemsResult.MediaListItems);
                result.IsSuccessful = resultMediaListItems.ResultState.GUID == controller.Globals.LState_Success.GUID;
                result.Result = resultMediaListItems.Result;
            }
            else
            {
                if (refItems.All(refItem => refItem.GUID_Parent == controller.ClassHtmlDocument.GUID))
                {
                    baseUrl = Url.Action(nameof(HtmlEditor));
                }

                var resultTask = await controller.SaveTags(viewModel.oItemHtml, refItemsFull.Result, userItem.UserItem, groupItem.GroupItem);

                result.IsSuccessful = resultTask.Result.GUID != controller.Globals.LState_Error.GUID;

                if (refItemsFull.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    LogError("Error while saving Tags", processId: processId);
                    result.IsSuccessful = false;
                }
                else
                {
                    var classes = resultTask.TypedTags.GroupBy(tag => tag.IdTagParent).Select(tagGrp => tagGrp.Key)
                        .ToList();

                    var transformProviders = GetNameTransformController(classes);

                    var tagOItems = resultTask.TypedTags.Select(typedT => new clsOntologyItem
                    {
                        GUID = typedT.IdTag,
                        Name = typedT.NameTag,
                        GUID_Parent = typedT.IdTagParent,
                        Type = typedT.TagType
                    }).ToList();

                    foreach (var transformProvider in transformProviders)
                    {
                        var tagOItemsToProvider =
                            tagOItems.Where(obj => transformProvider.IsResponsible(obj.GUID_Parent)).ToList();
                        if (tagOItemsToProvider.Any())
                        {
                            var idClass = tagOItemsToProvider.First().GUID_Parent;
                            var transFormed = await transformProvider.TransformNames(tagOItemsToProvider, idClass: idClass, messageOutput: messageOutput);
                            (from typedTag in resultTask.TypedTags
                             join transformedItem in transFormed.Result on typedTag.IdTag equals transformedItem.GUID
                             select new { typedTag, transformedItem }).ToList().ForEach(itm =>
                              {
                                  itm.typedTag.EncodedNameTag = itm.transformedItem.Val_String ?? itm.transformedItem.Name;
  
                              });
                        }

                    }

                    var resultTypedTags = await controller.GetLinkItemsByTypedTags(baseUrl, resultTask.TypedTags);
                    result.IsSuccessful = resultTypedTags.ResultState.GUID == controller.Globals.LState_Success.GUID;
                    result.Result = resultTypedTags.Result;


                    if (!result.IsSuccessful)
                    {
                        LogError("Error while getting LinkItems", processId: processId);
                    }
                }

            }

            LogEndMethod(processId:processId);            
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReference(clsOntologyItem refItem, string idInstance)
        {

            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            var controller = GetControllerHtmlEdit();

            var refItemTask = await controller.GetOItem(refItem.GUID, types.ObjectType);

            viewModel.oItemRef = refItemTask.Result;

            var classPathTask = await controller.GetClassPath(refItemTask.Result);

            var htmlItemTask = await controller.GetHtmlObjectByRef(refItemTask.Result);

            var resultViewItems = new List<ViewItem>();
            viewModel.oItemHtml = htmlItemTask.OItemHtmlItem;


            viewModel.nameInput.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            if (viewModel.oItemHtml != null)
            {
                viewModel.nameInput.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.oItemHtml.Name);
                viewModel.textArea.ChangeViewItemValue(ViewItemType.Refreshable.ToString(), true);
            }
            else
            {
                viewModel.textArea.ChangeViewItemValue(ViewItemType.Refreshable.ToString(), false);
                viewModel.nameInput.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.oItemRef.Name);
            }

            viewModel.textArea.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.SetWindowTitle($"{ refItemTask.Result.Name } ({classPathTask.Path})");

            viewModel.listenAnchor.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.listenLink.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.sendObject.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.applyObject.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            resultViewItems.Add(viewModel.textArea);
            resultViewItems.Add(viewModel.nameInput);
            resultViewItems.Add(viewModel.windowTitle);
            resultViewItems.Add(viewModel.listenAnchor);
            resultViewItems.Add(viewModel.listenLink);
            resultViewItems.Add(viewModel.sendObject);
            resultViewItems.Add(viewModel.applyObject);

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceHtmlTemplate(clsOntologyItem refItem, string idInstance)
        {

            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            var controller = GetControllerHtmlEdit();

            if (viewModel.oItemRef == null)
            {
                var refItemTask = await controller.GetOItem(refItem.GUID, types.ObjectType);

                viewModel.oItemRef = refItemTask.Result;

                var classPathTask = await controller.GetClassPath(refItemTask.Result);
            }

            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            if (viewModel.oItemRef == null)
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var messageOutput = new OWMessageOutput(viewModel.IdInstance);
            var transformProviders = GetNameTransformController(new List<string> { viewModel.oItemRef.GUID_Parent });

            var transformedItems = new List<clsOntologyItem>();
            foreach (var transformProvider in transformProviders)
            {
                var transFormed = await transformProvider.TransformNames(new List<clsOntologyItem> { viewModel.oItemRef }, idClass: viewModel.oItemRef.GUID_Parent, messageOutput: messageOutput);
                if (transFormed.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                    result.Message = transFormed.ResultState.Additional1;

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                transformedItems.AddRange(transFormed.Result);
            }

            viewModel.TransformedItems = transformedItems;

            var htmlItemTask = await controller.GetHtmlTemplateByRef(viewModel.oItemRef, transformedItems);

            if (htmlItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }
            viewModel.htmlTemplates = htmlItemTask.Result;


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult GetGridConfigTemplate()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(HTMLTemplateGridItem), Url.Action(nameof(GetDataTemplates)), null, null, null, false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetDataTemplates(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(new { data = viewModel.htmlTemplates }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> HtmlDiff(List<HTMLDiffItem> htmlDiffItems)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<List<HTMLDiffItem>>
            {
                IsSuccessful = true,
                Result = new List<HTMLDiffItem>()
            };

            foreach (var htmlDiffItem in htmlDiffItems)
            {
                htmlDiffItem.Html1 = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(htmlDiffItem.Html1 ?? string.Empty));
                htmlDiffItem.Html2 = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(htmlDiffItem.Html2 ?? string.Empty));
            }
            
            var controller = GetControllerHtmlEdit();

            var request = new HTMLDiffRequest(htmlDiffItems);
            var resultDiffHtml = await controller.DiffHtml(request);
            if (resultDiffHtml.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = resultDiffHtml.ResultState.Additional1;
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            foreach (var htmlDiffItem in resultDiffHtml.Result.HTMLDiffItems)
            {
                var htmlDiffItemNew = new HTMLDiffItem
                {
                    IdReference = htmlDiffItem.Result.IdReference,
                    HtmlDiff = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(htmlDiffItem.Result.HtmlDiff))
                };
                result.Result.Add(htmlDiffItemNew);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceHtmlHistory(clsOntologyItem refItem, string idInstance)
        {

            var processId = LogStartMethod();
            var viewModel = GetViewModelHtmlHistory(idInstance);
            var controller = GetControllerHtmlEdit();

            var refItemTask = await controller.GetOItem(refItem.GUID, types.ObjectType);

            viewModel.oItemRef = refItemTask.Result;

            var classPathTask = await controller.GetClassPath(refItemTask.Result);

            var htmlItemTask = await controller.GetHtmlObjectByRef(refItemTask.Result);

            var resultViewItems = new List<ViewItem>();
            viewModel.oItemHtml = htmlItemTask.OItemHtmlItem;


            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.SetWindowTitle($"{ refItemTask.Result.Name } ({classPathTask.Path})");

            resultViewItems.Add(viewModel.windowTitle);
            resultViewItems.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueHtmlHistory(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelHtmlHistory(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueHtmlSearch(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelHtmlSearch(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedHtmlHistory()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelHtmlHistory(idInstance);

            viewModel.toolbar.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            viewItemList.Add(viewModel.toolbar);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedHtmlSearch()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelHtmlSearch(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedHtmlHistory()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelHtmlHistory(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);


            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DisconnectedHtmlSearch()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelHtmlSearch(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyHtmlHistory(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelHtmlHistory(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyHtmlSearch(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelHtmlSearch(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetGridConfigHtmlHistory(string idInstance)
        {
            var viewModel = GetViewModelHtmlHistory(idInstance);


            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(HtmlHistoryItem), nameof(GetDataHtmlHistory), null, null, null, false, false, false);


            var json = Newtonsoft.Json.JsonConvert.SerializeObject(gridConfig);

            return Content(json, "application/json");
        }

        [Authorize]
        public ActionResult GetGridConfigHtmlSearch(string idInstance)
        {
            var viewModel = GetViewModelHtmlHistory(idInstance);


            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(HtmlSearchResultItem), Url.Action(nameof(GetDataHtmlSearch), nameof(HtmlEditController).Replace("Controller", "")), null, null, null, false, false, false);


            var json = Newtonsoft.Json.JsonConvert.SerializeObject(gridConfig);

            return Content(json, "application/json");
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetDataHtmlHistory(string idInstance)
        {
            var viewModel = GetViewModelHtmlHistory(idInstance);

            var controller = GetControllerHtmlEdit();

            if (viewModel.oItemHtml == null)
            {
                return Json(new List<HtmlHistoryItem>(), JsonRequestBehavior.AllowGet);
            }

            var controllerResult = await controller.GetHtmlHistory(viewModel.oItemHtml, viewModel.oItemRef);

            if (controllerResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                return Json(new List<HtmlHistoryItem>(), JsonRequestBehavior.AllowGet);
            }

            viewModel.HtmlHistoryEntries = controllerResult.Result;

            var result = new KendoGridDataResult<HtmlHistoryItem>(controllerResult.Result.Select(res => res.HistoryItem).ToList());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetUpdatedName(string idItem, string idInstance)
        {
            var viewModel = GetViewModel(idInstance);

            var controller = GetControllerHtmlEdit();

            var result = new ResultSimpleView<clsOntologyItem>
            {
                IsSuccessful = true
            };

            if (viewModel.oItemHtml == null)
            {
                result.IsSuccessful = false;
                result.Message = "No Html-Document found!";

                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controllerResult = await controller.GetHtmlHistory(viewModel.oItemHtml, viewModel.oItemRef);

            if (controllerResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                return Json(new List<HtmlHistoryItem>(), JsonRequestBehavior.AllowGet);
            }

            var oItemResult = await controller.GetOItem(idItem, controller.Globals.Type_Object);
            if (oItemResult.Result.GUID_Related == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Error while getting the Reference!";

                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var transformProviders = GetNameTransformController(new List<string> { oItemResult.Result.GUID_Parent });
            if (transformProviders.Any())
            {
                var messageOutput = new OWMessageOutput(viewModel.IdInstance);
                foreach (var transformProvider in transformProviders)
                {
                    var transFormed = await transformProvider.TransformNames(new List<clsOntologyItem> { oItemResult.Result }, encodeName: true, idClass: oItemResult.Result.GUID_Parent, messageOutput: messageOutput);
                    if (transFormed.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        result.IsSuccessful = false;
                        result.Message = transFormed.ResultState.Additional1;

                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                    var resultItem = transFormed.Result.FirstOrDefault();
                    if (resultItem != null)
                    {
                        oItemResult.Result.Mark = true;
                        result.Result = oItemResult.Result;
                    }
                    else
                    {
                        oItemResult.Result.Mark = false;
                        result.Result = oItemResult.Result;
                    }
                }
            }
            else
            {
                oItemResult.Result.Mark = false;
                result.Result = oItemResult.Result;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetDataHtmlSearch(string idInstance, string searchString)
        {
            var viewModel = GetViewModelHtmlSearch(idInstance);

            var controller = GetControllerHtmlEdit();


            if (string.IsNullOrEmpty(searchString))
            {
                return Json(new List<HtmlSearchResultItem>(), JsonRequestBehavior.AllowGet);
            }
            viewModel.SearchString = HttpUtility.HtmlEncode(System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(searchString)));

            var controllerResult = await controller.GetHtmlSearchResult(viewModel.SearchString);

            if (controllerResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                return Json(new List<HtmlHistoryItem>(), JsonRequestBehavior.AllowGet);
            }

            viewModel.SearchResultItems = controllerResult.Result;

            var result = new KendoGridDataResult<HtmlSearchResultItem>(controllerResult.Result.ToList());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DeleteHistoryItems(string idInstance, List<string> historyItemIds)
        {
            var viewModel = GetViewModelHtmlHistory(idInstance);

            var controller = GetControllerHtmlEdit();

            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            if (historyItemIds == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            if (!historyItemIds.Any())
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (viewModel.oItemHtml == null)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controllerResult = await controller.DeleteHistoryItems(historyItemIds);

            if (controllerResult.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            return Json(result, JsonRequestBehavior.AllowGet);


        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetHTMLCodeCombi(string idInstance, string idEsDoc)
        {
            var viewModel = GetViewModelHtmlHistory(idInstance);

            var controller = GetControllerHtmlEdit();

            var result = new ResultSimpleView<HtmlCodeCombi>
            {
                IsSuccessful = true,
                Result = new HtmlCodeCombi()
            };


            if (viewModel.oItemHtml == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (viewModel.HtmlHistoryEntries == null || !viewModel.HtmlHistoryEntries.Any())
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controllerResult = await controller.GetHtmlDocument(viewModel.oItemRef.GUID);

            if (controllerResult.Result.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            var htmlHistoryEntry = viewModel.HtmlHistoryEntries.FirstOrDefault(entry => entry.EsDoc.Id == idEsDoc);

            if (htmlHistoryEntry == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = new HtmlCodeCombi
            {
                IdHtmlDoc = viewModel.oItemHtml.GUID,
                IdRef = viewModel.oItemRef.GUID,
                IdHistoryEntryDoc = htmlHistoryEntry.EsDoc.Id,
                EncodedHtmlDoc = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(controllerResult.OAItemHtmlDocument.Val_String)),
                EncodedHistoryHtml = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(htmlHistoryEntry.EsDoc.Dict["Val_String"].ToString())),
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetHtmlForPreview(string idInstance, string idEsDoc)
        {
            var viewModel = GetViewModelHtmlHistory(idInstance);

            var controller = GetControllerHtmlEdit();

            var result = new ResultSimpleView<string>
            {
                IsSuccessful = true
            };


            if (viewModel.oItemHtml == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (viewModel.HtmlHistoryEntries == null || !viewModel.HtmlHistoryEntries.Any())
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var htmlHistoryEntry = viewModel.HtmlHistoryEntries.FirstOrDefault(entry => entry.EsDoc.Id == idEsDoc);

            if (htmlHistoryEntry == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(htmlHistoryEntry.EsDoc.Dict["Val_String"].ToString()));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: HtmlEdit
        [Authorize]
        public ActionResult HtmlEditor()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        // GET: HtmlEdit
        [Authorize]
        public ActionResult HtmlHistory()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelHtmlHistory(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult HtmlSearch()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelHtmlSearch(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        // GET: HtmlEdit
        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetHtmlContent(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var controller = GetControllerHtmlEdit();


            if (viewModel.oItemRef != null)
            {
                var resultTask = await controller.GetHtmlDocument(viewModel.oItemRef.GUID);

                viewModel.oItemHtmlContent = resultTask.OAItemHtmlDocument;
                var content = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(resultTask.OAItemHtmlDocument.Val_String));
                viewModel.textArea.ChangeViewItemValue(ViewItemType.Content.ToString(), content);

                LogEndMethod(processId: processId);
                return Json(viewModel.textArea, JsonRequestBehavior.AllowGet);
            }

            LogEndMethod(processId:processId);
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        // GET: HtmlEdit
        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetHtmlTemplateContent(string idInstance, string idHtmlDoc)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var controller = GetControllerHtmlEdit();

            var result = new ResultSimpleView<string>()
            {
                IsSuccessful = true
            };

            if (string.IsNullOrEmpty(idHtmlDoc))
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            
            var transformItem = viewModel.TransformedItems.FirstOrDefault(trans => trans.GUID == idHtmlDoc);
            if (transformItem == null)
            {
                var resultTask = await controller.GetOItem(idHtmlDoc, controller.Globals.Type_Object);

                if (resultTask.Result.GUID == controller.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                    return Json(result, JsonRequestBehavior.AllowGet);

                }

                var resultTaskHtmlContent = await controller.GetHtmlContent(resultTask.Result);

                if (resultTaskHtmlContent.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                    return Json(result, JsonRequestBehavior.AllowGet);

                }

                viewModel.oItemHtmlContent = resultTaskHtmlContent.Result;
                var content = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(resultTaskHtmlContent.Result.Val_String));

                result.Result = content;
            }
            else
            {
                result.Result = transformItem.Val_String;
            }
            

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetTags(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var controller = GetControllerHtmlEdit();

            var result = new ResultGetTags
            {
                IsSuccessful = true
            };

            if (viewModel.oItemHtmlContent == null)
            {
                LogError("Html-Content is lost", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var securityConnector = GetControllerSecurity();

            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);
            var groupItem = await securityConnector.GetGroup(userId);

            var guidList = await controller.GetGuidList(viewModel.oItemHtmlContent);

            var resultGetTags = await controller.CheckTags(viewModel.oItemHtml, viewModel.oItemRef, guidList.GuidList, userItem.UserItem, groupItem.GroupItem);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> RestoreHistoryEntry(string idInstance, string idEsDoc)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelHtmlHistory(idInstance);

            var controller = GetControllerHtmlEdit();

            var result = new ResultSaveHtml
            {
                IsSaved = true
            };

            if (viewModel.oItemHtml == null)
            {
                result.IsSaved = false;
                LogEndMethod(processId: processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (viewModel.HtmlHistoryEntries == null || !viewModel.HtmlHistoryEntries.Any())
            {
                result.IsSaved = false;
                LogEndMethod(processId: processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var htmlHistoryEntry = viewModel.HtmlHistoryEntries.FirstOrDefault(entry => entry.EsDoc.Id == idEsDoc);

            if (htmlHistoryEntry == null)
            {
                result.IsSaved = false;
                LogEndMethod(processId: processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var request = new RequestSaveHtml
            {
                content = htmlHistoryEntry.EsDoc.Dict["Val_String"].ToString(),
                idInstance = idInstance
            };

            var resultTask = await controller.SaveHtmlContent(request.content, viewModel.oItemHtml, viewModel.oItemRef);
            result = new ResultSaveHtml
            {
                IsSaved = resultTask.Result.GUID == controller.Globals.LState_Success.GUID
            };

            if (resultTask.Result.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while saving html", processId: processId);
            }

            LogEndMethod(processId: processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveHtmlContent(RequestSaveHtml request)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(request.idInstance);

            var controller = GetControllerHtmlEdit();

            if (viewModel.oItemRef == null) return Json(null, JsonRequestBehavior.AllowGet);
            if (request.content == null) return Json(null, JsonRequestBehavior.AllowGet);
            request.content = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(request.content ?? ""));
            var resultTask = await controller.SaveHtmlContent(request.content, viewModel.oItemHtml, viewModel.oItemRef);
            var result = new ResultSaveHtml
            {
                IsSaved = resultTask.Result.GUID == controller.Globals.LState_Success.GUID
            };

            if (resultTask.Result.GUID == controller.Globals.LState_Success.GUID)
            {
                viewModel.oItemHtml = resultTask.HtmlDocument;
                viewModel.oItemHtmlContent = resultTask.HtmlOAttribute;
                result.HtmlItem = resultTask.HtmlDocument;
            }
            else
            {
                LogError("Error while saving html", processId: processId);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.toolbar.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.nameInput.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.textArea.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.openMediaViewerImage.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.openMediaViewerPDF.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.openMediaViewerVideo.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.toolbar);
            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.textArea);
            viewItemList.Add(viewModel.openMediaViewerImage);
            viewItemList.Add(viewModel.openMediaViewerPDF);
            viewItemList.Add(viewModel.openMediaViewerVideo);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.nameInput.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.textArea.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);


            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.nameInput);
            viewItemList.Add(viewModel.textArea);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }


    }

}