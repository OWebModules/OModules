﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class FileResourceController : AppControllerBase
    {
        

        private FileResourceModule.FileResourceController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new FileResourceModule.FileResourceController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private PathResourcesViewModel GetViewModelPathResources(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<PathResourcesViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new PathResourcesViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedPathResources)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedPathResources)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyPathResources)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferencePathResources)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValuePathResources)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValuePathResources(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPathResources(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedPathResources()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelPathResources(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedPathResources()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelPathResources(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyPathResources(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPathResources(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferencePathResources(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPathResources(idInstance);

            var controller = GetController();

            var refItemTask = await controller.GetClassObject(refItem.GUID);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while getting the RefItem", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.ClassObject = refItemTask.Result;

            viewModel.SetWindowTitle(viewModel.ClassObject.ToString());
            result.ViewItems.Add(viewModel.windowTitle);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult PathResources()
        {
            var viewModel = GetViewModelPathResources(Guid.NewGuid().ToString());
            return View(viewModel);
        }
        // GET: FileResourceModule
        public ActionResult Index()
        {
            return View();
        }
    }
}