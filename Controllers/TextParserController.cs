﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using OModules.Models.MessageOutput;
using TextParserModule;
using TextParserModule.Models;
using JsonModule.Models;

namespace OModules.Controllers
{
    public class TextParserController : AppControllerBase
    {
        private TextParserModule.TextParserController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new TextParserModule.TextParserController(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private TextParserViewModel GetViewModelQueryView(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<TextParserViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new TextParserViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedQueryView)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedQueryView)),
                    ActionSetViewReady = Url.Action(nameof(SetViewQueryViewReady)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfigQueryView)),
                    ActionValidateReference = Url.Action(nameof(ValidateQueryViewReference)),
                    ActionResetScrollId = Url.Action(nameof(ResetScrollId)),
                    ActionParse = Url.Action(nameof(Parse)),
                    ActionDeleteIndex = Url.Action(nameof(DeleteIndex)),
                    ActionGetEsIndexItemRequest = Url.Action(nameof(GetEsIndexItemRequest)),
                    ActionJsonEditorInit = Url.Action(nameof(ToolboxController.JsonEditorInit), nameof(ToolboxController).Replace("Controller", "")),
                    ActionGetAutoCompletes = Url.Action(nameof(GetAutoCompletes)),
                    ActionSavePattern = Url.Action(nameof(SavePattern)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueQueryView)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueQueryView(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelQueryView(idInstance);

            if (viewItem.ViewItemId == viewModel.query.ViewItemId)
            {
                viewModel.scrollId = null;
            }
            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(viewModel.saveQuery);
            viewModel.saveQuery.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            if (result.ViewItem.ViewItemId == viewModel.query.ViewItemId)
            {
                var query = viewModel.query.GetViewItemValue(ViewItemType.Content.ToString()).ToString();
                if (!string.IsNullOrEmpty(query))
                {
                    viewModel.saveQuery.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                }
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedQueryView()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelQueryView(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedQueryView()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelQueryView(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewQueryViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelQueryView(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateQueryViewReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelQueryView(idInstance);

            var controller = GetController();

            var refItemTask = await controller.GetOItem(refItem.GUID, controller.Globals.Type_Object);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while getting the RefItem", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.parserItem = refItemTask.Result;

            viewModel.SetWindowTitle(viewModel.parserItem.Name);
            result.ViewItems.Add(viewModel.windowTitle);

            viewModel.query.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            result.ViewItems.Add(viewModel.query);
            viewModel.search.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            result.ViewItems.Add(viewModel.search);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ResetScrollId(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelQueryView(idInstance);

            viewModel.scrollId = null;

            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetGridConfigQueryView(string idInstance)
        {
            var viewModel = GetViewModelQueryView(idInstance);

            var controller = GetController();

            var result = new ResultSimpleView<Dictionary<string, object>>
            {
                IsSuccessful = true,
                Result = new Dictionary<string, object>()
            };

            var textParsers = await controller.GetTextParser(viewModel.parserItem);

            if (textParsers.ResultState.GUID == controller.Globals.LState_Error.GUID || !textParsers.Result.Any())
            {
                result.IsSuccessful = false;
                result.Result = new Dictionary<string, object>();
            };

            viewModel.textParser = textParsers.Result.First();

            var parserFields = await controller.GetParserFields(new clsOntologyItem { GUID = viewModel.textParser.IdFieldExtractor, Name = viewModel.textParser.NameFieldExtractor, GUID_Parent = viewModel.textParser.IdClassFieldExtractor, Type = controller.Globals.Type_Object });

            if (parserFields.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.textParserFields = parserFields.Result;

            var resultGridConfig = await controller.GetGridConfig(parserFields.Result, Url.Action(nameof(GetDataQueryView)));


            if (resultGridConfig.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = resultGridConfig.Result;

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);

            return Content(json, "application/json");
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DeleteIndex(string idInstance)
        {
            var processId = LogStartMethod();            var viewModel = GetViewModelQueryView(idInstance);

            var textParser = viewModel.textParser;

            var textParserController = GetController();

            var result = new ResultSimple
                { IsSuccessful = true };
            if (viewModel.textParser != null)
            {
                var deleteResult = await textParserController.DeleteIndex(textParser);
                if (deleteResult.GUID == textParserController.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                    result.Message = deleteResult.Additional1;
                }
            }
            else
            {
                var deleteResult = await textParserController.DeleteIndex(viewModel.parserItem.GUID);
                if (deleteResult.GUID == textParserController.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                    result.Message = deleteResult.Additional1;
                }
            }

            LogEndMethod(processId:processId);            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> Parse(string idInstance)
        {
            var processId = LogStartMethod();            
            var viewModel = GetViewModelQueryView(idInstance);

            var textParser = viewModel.textParser;
            var textParserFields = viewModel.textParserFields;

            var textParserController = GetController();

            ExecuteTextParserRequest request = null;
            if (viewModel.textParser == null)
            {
                request = new ExecuteTextParserRequest
                {
                    IdTextParser = viewModel.parserItem?.GUID,
                    MessageOutput = new OWMessageOutput(viewModel.IdInstance)
                };
            }
            else
            {
                request = new ExecuteTextParserRequest
                {
                    TextParser = textParser,
                    TextParserFields = textParserFields,
                    MessageOutput = new OWMessageOutput(viewModel.IdInstance)
                };
            }
            

            textParserController.ExecuteTextParser(request);

            var result = new ResultSimple
                {IsSuccessful = true};
            LogEndMethod(processId:processId);            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetDataQueryView(string idInstance, int page, int pageSize)
        {
            var viewModel = GetViewModelQueryView(idInstance);

            //var page = int.Parse(Request.Params["page"]);
            //var pageSize = int.Parse(Request.Params["pageSize"]);
            var sortKeys = Request.Params.AllKeys.Where(key => key.StartsWith("sort")).ToList();
            page -= 1;
            var sortRequests = new List<KendoSortRequest>();

            if (viewModel.lastPageSize != pageSize)
            {
                viewModel.scrollId = null;
            }

            if (viewModel.lastPage == page)
            {
                viewModel.scrollId = null;
            }

            viewModel.lastPageSize = pageSize;
            viewModel.lastPage = page;

            sortKeys.ForEach(sortKey =>
            {

                var match = Regex.Match(sortKey, @"\[\d+\]");

                if (match.Success)
                {
                    var ix = int.Parse(match.Value.Replace("[", "").Replace("]", ""));
                    var prop = sortKey.Substring(match.Index + match.Length).Replace("[", "").Replace("]", "");

                    var sortRequest = sortRequests.FirstOrDefault(sortR => sortR.Ix == ix);
                    if (sortRequest == null)
                    {
                        sortRequest = new KendoSortRequest
                        {
                            Ix = ix
                        };

                        sortRequests.Add(sortRequest);


                    }

                    
                    var propItem = typeof(KendoSortRequest).GetProperty(prop);

                    propItem.SetValue(sortRequest, Request.Params[sortKey]);
                }
            });

            

            sortRequests.ForEach(sortRequest =>
            {
                var lastSortRequest = viewModel.lastSortRequests.FirstOrDefault(lastSortR => lastSortR.Ix == sortRequest.Ix && lastSortR.field == sortRequest.field && lastSortR.dir == sortRequest.dir);
                if (lastSortRequest == null)
                {
                    viewModel.scrollId = null;
                }
            });

            if (sortRequests.Count != viewModel.lastSortRequests.Count)
            {
                viewModel.scrollId = null;
            }

            viewModel.lastSortRequests = sortRequests;

            if (viewModel.textParser == null)
            {
                return Json(new List<object>(), JsonRequestBehavior.AllowGet);
            }
            var controller = GetController();

            var queryVal = viewModel.query.GetViewItemValueItem(ViewItemType.Content.ToString());
            var query = "*";
            if (queryVal != null)
            {
                query = queryVal.Value.ToString();
            }
            var dataResult = await controller.LoadData(viewModel.textParser, viewModel.textParserFields, page, pageSize, query, viewModel.scrollId, sortRequests: sortRequests);

            if (!dataResult.IsOK)
            {
                return Json(new List<object>(), JsonRequestBehavior.AllowGet);
            }

            viewModel.scrollId = dataResult.ScrollId;

            viewModel.lastPageValues = dataResult.Documents.Select(doc => doc.Dict);
            //foreach (var request in sortRequests)
            //{
            //    dict = dict.OrderBy(keyValue => keyValue.ContainsKey(request.field) ? keyValue[request.field] : 1 == 1);
                
            //}
            var result = new
            {
                data = viewModel.lastPageValues,
                total = dataResult.TotalCount
            };
            var resultAction = Json(result, JsonRequestBehavior.AllowGet);
            resultAction.MaxJsonLength = int.MaxValue;

            return resultAction;

            //var result = await controller.LoadData(viewModel.refItem, viewModel.ReportItem, viewModel.ReportFields);


            //if (result.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            //{
            //    return Json(new List<object>(), JsonRequestBehavior.AllowGet);
            //}

            //return Json(result.Result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetEsIndexItemRequest(string idInstance, string id)
        {
            var processId = LogStartMethod();            var viewModel = GetViewModelQueryView(idInstance);

            var textParser = viewModel.textParser;

            var request = new GetEsIndexItemRequest("_id", id)
            {
                IndexTypeItem = new IndexTypeItem
                {
                    Server = textParser.NameServer,
                    Port = textParser.Port,
                    Index = textParser.NameIndexElasticSearch,
                    Type = textParser.NameEsType
                },
                MessageOutput  = new OWMessageOutput(viewModel.IdInstance)
            };
            var result = new ResultSimpleView<GetEsIndexItemRequest>
            {
                IsSuccessful = true,
                Result = request
            };
            
            LogEndMethod(processId:processId);            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetAutoCompletes(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelQueryView(idInstance);
            var controller = GetController();

            var text = Request.Params["query"];

            var result = new AutoCompleteItem();

            var resultController = await controller.GetAutoCompletes(viewModel.textParser, viewModel.textParserFields, text);
            if (resultController.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result = resultController.Result;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> SavePattern(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelQueryView(idInstance);
            var controller = GetController();

            var text = viewModel.query.GetViewItemValue(ViewItemType.Content.ToString()).ToString();

            var result = new ResultSimple
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath,
            };

            var resultController = await controller.SavePattern(viewModel.textParser, text);
            if (resultController.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = !(resultController.GUID == controller.Globals.LState_Error.GUID);
                result.Message = resultController.Additional1;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Message = "Pattern is saved!";

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        // GET: TextParser
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult QueryResult()
        {
            var viewModel = GetViewModelQueryView(Guid.NewGuid().ToString());
            return View(viewModel);
        }
    }
}

    