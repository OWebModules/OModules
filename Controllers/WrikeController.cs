﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WrikeConnectorModule.Models;

namespace OModules.Controllers
{
    public class WrikeController : AppControllerBase
    {
        private WrikeConnectorModule.WrikeConnector GetControllerWrikeSync()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });


            var result = new WrikeConnectorModule.WrikeConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private WrikeSyncViewModel GetViewModelWrikeSync(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<WrikeSyncViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new WrikeSyncViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedWrikeSync)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedWrikeSync)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyWrikeSync)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceWrikeSync)),
                    ActionGetToolsForToolbar = Url.Action(nameof(ToolboxController.GetToolsForToolbar), nameof(ToolboxController).Replace("Controller","")),
                    ActionGetRunningState = Url.Action(nameof(GetWrikeSyncState)),
                    ActionSyncWrike = Url.Action(nameof(SyncWrike)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfig)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueWrikeSync)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueWrikeSync(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelWrikeSync(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedWrikeSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelWrikeSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);


            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedWrikeSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelWrikeSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyWrikeSync(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelWrikeSync(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetWrikeSyncState(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelWrikeSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.ViewItems.Add(viewModel.syncWrike);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);

            var memoryCache = new InMemoryCache();
            var tfsSync = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<SyncIssuesResult>>>("Global", "WrikeSync", () =>
            {
                return null;
            });
            if (tfsSync == null)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "No result");
                viewModel.syncWrike.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }


            if (tfsSync.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation ||
                tfsSync.Status == System.Threading.Tasks.TaskStatus.Running)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Is Running");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                viewModel.syncWrike.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            }
            else if (tfsSync.Status == TaskStatus.RanToCompletion)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                viewModel.syncWrike.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            }
            else
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Error while running");
                viewModel.syncWrike.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceWrikeSync(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelWrikeSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var controller = GetControllerWrikeSync();

            var referenceResult = await controller.GetClassObjectReference(refItem);

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.syncWrike);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            if (referenceResult.ResultState.GUID == controller.Globals.LState_Error.GUID ||
                referenceResult.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
            {
                viewModel.syncWrike.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.SetWindowTitle("");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), referenceResult.ResultState.Additional1);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RefItem = referenceResult.Result;
            viewModel.syncWrike.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.SetWindowTitle($"{viewModel.RefItem.ObjectItem.Name} ({viewModel.RefItem.ClassItem.Name})");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<SyncIssuesResult>>>("Global", "WrikeSync", () =>
            {
                return null;
            });

            if (csvImport != null)
            {
                if (csvImport.Status == System.Threading.Tasks.TaskStatus.Running ||
                    csvImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation)
                {
                    viewModel.syncWrike.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "is running");
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                }
                else if (csvImport.Status == TaskStatus.RanToCompletion)
                {
                    if (csvImport.Result.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), csvImport.Result.ResultState.Additional1);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                    }
                    else
                    {
                        viewModel.syncWrike.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

                    }
                }
            }
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SyncWrike(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelWrikeSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetControllerWrikeSync();

            var request = new WrikeConnectorModule.Models.SyncIssuesRequest(viewModel.RefItem.ObjectItem.GUID);
            var controllerResult = Task.Run(() => controller.SyncTasks(request));

            var memoryCache = new InMemoryCache();
            var wrikeSync = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<SyncIssuesResult>>>("Global", "WrikeSync", () =>
            {
                return controllerResult;
            });


            result.ViewItems.Add(viewModel.syncWrike);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Start Creation");
            viewModel.syncWrike.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetGridConfig()
        {
            var processId = LogStartMethod();

            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(IssueGridItem), Url.Action(nameof(GetGridItems)), null, null, null, false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetGridItems()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();


            var viewModel = GetViewModelWrikeSync(instanceId);

            if (viewModel.RefItem == null)
            {
                var resultEmptyList = new KendoGridDataResult<IssueGridItem>(new List<IssueGridItem>());
                var resultEmpty = Json(resultEmptyList, JsonRequestBehavior.AllowGet);
                resultEmpty.MaxJsonLength = int.MaxValue;

                LogEndMethod(processId:processId);
                return resultEmpty;
            }

            var controller = GetControllerWrikeSync();

            var request = new WrikeConnectorModule.Models.SyncIssuesRequest(viewModel.RefItem.ObjectItem.GUID);
            var resultTask = await controller.GetGridItems(request);

            var resultItem = new KendoGridDataResult<IssueGridItem>(resultTask.Result);
            var result = Json(resultItem, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public ActionResult WrikeSync()
        {
            var viewModel = GetViewModelWrikeSync(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        // GET: Wrike
        public ActionResult Index()
        {
            return View();
        }
    }
}