﻿using BillModule.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class BillController : AppControllerBase
    {
        

        private async System.Threading.Tasks.Task<BillModule.TransactionConnector> GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new BillModule.TransactionConnector(globals);
            var initialResult = await result.Initialize();
            if (initialResult.GUID == result.Globals.LState_Error.GUID)
            {
                throw new Exception(initialResult.Additional1);
            }

            LogEndMethod(processId:processId);
            return result;

        }

        private async System.Threading.Tasks.Task<BillModule.VoucherController> GetVoucherController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new BillModule.VoucherController(globals);

            LogEndMethod(processId: processId);
            return result;

        }

        private TransactionViewModel GetViewModelTransaction(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<TransactionViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new TransactionViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedTransaction)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedTransaction)),
                    ActionSetViewReady = Url.Action(nameof(SetViewTransactionReady)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfigTransaction)),
                    ActionValidateReference = Url.Action(nameof(ValidateTransactionReference)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueTransaction)));

                return viewModelNew;
            });
            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private TransactionDetailViewModel GetViewModelTransactionDetail(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<TransactionDetailViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new TransactionDetailViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedTransactionDetail)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedTransactionDetail)),
                    ActionSetViewReady = Url.Action(nameof(SetViewTransactionDetailReady)),
                    ActionValidateReference = Url.Action(nameof(ValidateTransactionDetailReference)),
                    ActionGetDropDownConfigUnits = Url.Action(nameof(DropDownController.GetDropDownConfigForObjects), nameof(DropDownController).Replace("Controller","")),
                    ActionGetDropDownConfigTaxrates = Url.Action(nameof(DropDownController.GetDropDownConfigForObjects), nameof(DropDownController).Replace("Controller", "")),
                    ActionGetDropDownConfigCurrencies = Url.Action(nameof(DropDownController.GetDropDownConfigForObjects), nameof(DropDownController).Replace("Controller","")),
                    ActionAppliedObject = Url.Action(nameof(AppliedObject)),
                    ActionGetTransactionData = Url.Action(nameof(GetTransactionData)),
                    ActionObjectList = Url.Action(nameof(OItemListController.ObjectList), nameof(OItemListController).Replace("Controller","")),
                    ActionNewTransaction = Url.Action(nameof(EditOItemController.ObjectNameEdit), nameof(EditOItemController).Replace("Controller","")),
                    ActionGetPDFListUrl = Url.Action(nameof(GetUrlPDFList)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueTransactionDetail)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private VoucherAddViewModel GetViewModelVoucherAdd(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<VoucherAddViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new VoucherAddViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedVoucherAdd)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedVoucherAdd)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyVoucherAdd)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfigVoucherAdd)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceVoucherAdd)),
                    ActionVoucherAddInit = Url.Action(nameof(VoucherAddInit)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueVoucherAdd)));

                return viewModelNew;
            });
            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId: processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueTransaction(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTransaction(idInstance);

           
            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueTransactionDetail(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTransactionDetail(idInstance);
            var controller = await GetController();
            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            if (result.ViewItem.ViewItemId == viewModel.inpTransactionName.ViewItemId)
            {
                var value = result.ViewItem.GetViewItemValueItem(ViewItemType.Content.ToString()).Value.ToString();
                viewModel.transactionItem.TransactionName = value;

                var resultSave = await controller.SaveTransactionItem(viewModel.transactionItem);


            }
            else if (result.ViewItem.ViewItemId == viewModel.dateTransactionDate.ViewItemId)
            {
                var value = (DateTime) result.ViewItem.GetViewItemValueItem(ViewItemType.Content.ToString()).Value;
                viewModel.transactionItem.TransactionDate = value;

                var resultSave = await controller.SaveTransactionItem(viewModel.transactionItem);


            }
            else if (result.ViewItem.ViewItemId == viewModel.inpTransactionId.ViewItemId)
            {
                var value = result.ViewItem.GetViewItemValueItem(ViewItemType.Content.ToString()).Value.ToString();
                viewModel.transactionItem.Id = value;

                var resultSave = await controller.SaveTransactionItem(viewModel.transactionItem);


            }
            else if (result.ViewItem.ViewItemId == viewModel.inpUnit.ViewItemId)
            {
                var value = result.ViewItem.GetViewItemValueItem(ViewItemType.Content.ToString()).Value.ToString();
                viewModel.transactionItem.IdUnit = value;

                var resultSave = await controller.SaveTransactionItem(viewModel.transactionItem);


            }
            else if (result.ViewItem.ViewItemId == viewModel.inpAmount.ViewItemId)
            {
                var value = Convert.ToDouble(result.ViewItem.GetViewItemValueItem(ViewItemType.Content.ToString()).Value);
                viewModel.transactionItem.Amount = value;

                var resultSave = await controller.SaveTransactionItem(viewModel.transactionItem);


            }
            else if (result.ViewItem.ViewItemId == viewModel.inpTaxRate.ViewItemId)
            {
                var value = result.ViewItem.GetViewItemValueItem(ViewItemType.Content.ToString()).Value.ToString();

                viewModel.transactionItem.IdTaxRate = value;
                var resultSave = await controller.SaveTransactionItem(viewModel.transactionItem);
            }
            else if (result.ViewItem.ViewItemId == viewModel.inpCurrency.ViewItemId)
            {
                var value = result.ViewItem.GetViewItemValueItem(ViewItemType.Content.ToString()).Value.ToString();

                viewModel.transactionItem.IdCurrency = value;
                var resultSave = await controller.SaveTransactionItem(viewModel.transactionItem);
            }
            else if (result.ViewItem.ViewItemId == viewModel.inpPay.ViewItemId)
            {
                var value = Convert.ToDouble(result.ViewItem.GetViewItemValueItem(ViewItemType.Content.ToString()).Value);
                viewModel.transactionItem.ToPay = value;
                var resultSave = await controller.SaveTransactionItem(viewModel.transactionItem);
            }
            else if (result.ViewItem.ViewItemId == viewModel.chkGross.ViewItemId)
            {
                var value = (bool)result.ViewItem.GetViewItemValueItem(ViewItemType.Content.ToString()).Value;
                viewModel.transactionItem.Gross = value;

                var resultSave = await controller.SaveTransactionItem(viewModel.transactionItem);


            }

            resultAction.ViewItems.Add(viewModel.status);
            if (string.IsNullOrEmpty( viewModel.transactionItem.IdAttributeGross) ||
                string.IsNullOrEmpty(viewModel.transactionItem.IdAmount) ||
                string.IsNullOrEmpty(viewModel.transactionItem.IdAttributeToPay) ||
                string.IsNullOrEmpty(viewModel.transactionItem.IdAttributeTransactionDate) ||
                string.IsNullOrEmpty(viewModel.transactionItem.IdContractee) ||
                string.IsNullOrEmpty(viewModel.transactionItem.IdContractor) ||
                string.IsNullOrEmpty(viewModel.transactionItem.IdCurrency) ||
                string.IsNullOrEmpty(viewModel.transactionItem.IdTaxRate) ||
                string.IsNullOrEmpty(viewModel.transactionItem.IdUnit))
            {
                viewModel.status.ChangeViewItemValue(ViewItemType.Content.ToString(), "Bitte überprüfen Sie Ihre Daten.");
            }
            else
            {
                viewModel.status.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueVoucherAdd(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelVoucherAdd(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId: processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveTransaction(string idInstance)
        {
            var processId = LogStartMethod();            
            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var viewModel = GetViewModelTransactionDetail(idInstance);
            var controller = await GetController();

            var resultSave = await controller.SaveTransactionItem(viewModel.transactionItem);


            LogEndMethod(processId:processId);            
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetUrlPDFList(string idInstance)
        {
            var processId = LogStartMethod();            
            var result = new ResultSimpleView<string>
            {
                IsSuccessful = true
            };

            var viewModel = GetViewModelTransactionDetail(idInstance);

            result.Result = "about:blank";
            if (viewModel.transactionItem != null)
            {
                result.Result = $"{Url.Action(nameof(MediaViewerController.MediaListPDF), nameof(MediaViewerController).Replace("Controller", ""))}?Sender={viewModel.IdInstance}&Object={viewModel.transactionItem.IdTransaction}";
            }


            LogEndMethod(processId:processId);            
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> AppliedObject(string idObject, string idInstance)
        {
            var processId = LogStartMethod();            
            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (string.IsNullOrEmpty(idObject))
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);                
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var viewModel = GetViewModelTransactionDetail(idInstance);
            var controller = await GetController();

            var objectItem = await controller.GetOItem(idObject);

            if (objectItem.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);                
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (objectItem.Result.ObjectItem.GUID_Parent != controller.ClassPartner.GUID)
            {
                LogEndMethod(processId:processId);                
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var checkedObjectVertragsgeber = viewModel.btnListenVertragsgeber.GetViewItemValueItem(ViewItemType.Checked.ToString());
            var checkedObjectVertragsnehmer = viewModel.btnListenVertragsnehmer.GetViewItemValueItem(ViewItemType.Checked.ToString());
            if (checkedObjectVertragsgeber == null && checkedObjectVertragsnehmer == null)
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);                
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (checkedObjectVertragsgeber != null && ((bool)checkedObjectVertragsgeber.Value))
            {
                viewModel.transactionItem.IdContractor = objectItem.Result.ObjectItem.GUID;
                viewModel.transactionItem.Contractor = objectItem.Result.ObjectItem.Name;
                var resultSave = await controller.SaveTransactionItem(viewModel.transactionItem);
                result.IsSuccessful = resultSave.ResultState.GUID == controller.Globals.LState_Success.GUID;

                if (result.IsSuccessful)
                {
                    viewModel.inpVertragsgeber.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.transactionItem.Contractor);
                    result.ViewItems.Add(viewModel.inpVertragsgeber);
                }
            } else if (checkedObjectVertragsnehmer != null && ((bool)checkedObjectVertragsnehmer.Value))
            {
                viewModel.transactionItem.IdContractee = objectItem.Result.ObjectItem.GUID;
                viewModel.transactionItem.Contractee = objectItem.Result.ObjectItem.Name;
                var resultSave = await controller.SaveTransactionItem(viewModel.transactionItem);
                result.IsSuccessful = resultSave.ResultState.GUID == controller.Globals.LState_Success.GUID;
                if (result.IsSuccessful)
                {
                    viewModel.inpVertragsnehmer.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.transactionItem.Contractee);
                    result.ViewItems.Add(viewModel.inpVertragsnehmer);
                }
            }

            

            

            LogEndMethod(processId:processId);            
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedTransaction()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTransaction(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedTransactionDetail()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTransactionDetail(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewModel.openNewTransaction.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.openNewTransaction);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedVoucherAdd()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelVoucherAdd(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId: processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedTransaction()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTransaction(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public ActionResult DisconnectedTransactionDetail()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTransaction(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public ActionResult DisconnectedVoucherAdd()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelVoucherAdd(idInstance);

            LogEndMethod(processId: processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewTransactionReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTransaction(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewTransactionDetailReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTransaction(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyVoucherAdd(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelVoucherAdd(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId: processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateTransactionReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTransaction(idInstance);

            var controller = await GetController();

            var refItemTask = await controller.GetOItem(refItem.GUID);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while getting the RefItem", processId:processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.ClassObject = refItemTask.Result;

            viewModel.SetWindowTitle(viewModel.ClassObject.ToString());
            result.ViewItems.Add(viewModel.windowTitle);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateTransactionDetailReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTransactionDetail(idInstance);

            var controller = await GetController();

            var refItemTask = await controller.GetOItem(refItem.GUID);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while getting the RefItem", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.ClassObject = refItemTask.Result;
            viewModel.SetWindowTitle(viewModel.ClassObject.ToString());
            
            result.ViewItems.Add(viewModel.windowTitle);
            

            var viewItems = GetTransactionViewItems(viewModel);
            ClearViewItems(viewItems);


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceVoucherAdd(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelVoucherAdd(idInstance);

            var controller = await GetController();

            var refItemTask = await controller.GetOItem(refItem.GUID);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while getting the RefItem", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RefItem = refItemTask.Result;

            viewModel.SetWindowTitle(viewModel.RefItem.ToString());
            result.ViewItems.Add(viewModel.windowTitle);

            LogEndMethod(processId: processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async System.Threading.Tasks.Task<ActionResult> GetTransactionData(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTransactionDetail(idInstance);

            var controller = await GetController();

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var classObject = viewModel.ClassObject;

            if (classObject == null)
            {
                LogError("Error no Reference", processId: processId);
                LogEndMethod(processId:processId);                
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (viewModel.ClassObject.ObjectItem.GUID_Parent == controller.ClassFinancialTransaction.GUID)
            {
                var request = new GetTransactionsRequest(classObject.ObjectItem, null, true);
                var resultTask = await controller.GetTransactionItems(request);
                if (resultTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    LogError("Error while getting the RefItem", processId: processId);
                    LogEndMethod(processId:processId);                    
                    result.IsSuccessful = false;
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                viewModel.transactionItem = resultTask.Result.FirstOrDefault();
                viewModel.dataItem.ChangeViewItemValue(ViewItemType.DataSource.ToString(), viewModel.transactionItem);
                result.ViewItems.Add(viewModel.dataItem);

                var viewItems = GetTransactionViewItems(viewModel);
                ToggleEnableViewItems(viewItems, true);
                result.ViewItems.AddRange(viewItems);
            }



            viewModel.openPdfList.ChangeViewItemValue(ViewItemType.Enable.ToString(), viewModel.transactionItem != null && !string.IsNullOrEmpty(viewModel.transactionItem.IdTransaction));
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            result.ViewItems.Add(viewModel.isListen);
            result.ViewItems.Add(viewModel.openPdfList);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private List<ViewItem> GetTransactionViewItems(TransactionDetailViewModel viewModel)
        {
            var result = new List<ViewItem>();
            
            result.Add(viewModel.inpAmount);
            result.Add(viewModel.inpCurrency);
            result.Add(viewModel.inpPay);
            result.Add(viewModel.inpTaxRate);
            result.Add(viewModel.inpTransactionId);
            result.Add(viewModel.inpTransactionName);
            result.Add(viewModel.inpUnit);
            result.Add(viewModel.btnListenVertragsgeber);
            result.Add(viewModel.btnListenVertragsnehmer);
            result.Add(viewModel.chkGross);
            result.Add(viewModel.dateTransactionDate);
           

            return result;
            
        }

        private void ClearViewItems(List<ViewItem> viewItems)
        {
            foreach (var viewItem in viewItems)
            {
                
                if (viewItem.ViewItemClass == ViewItemClass.Input.ToString() ||
                    viewItem.ViewItemClass == ViewItemClass.KendoDropDownList.ToString())
                {
                    viewItem.ChangeViewItemValue(ViewItemType.Content.ToString(), string.Empty);
                }
            }
        }

        private void ToggleEnableViewItems(List<ViewItem> viewItems, bool enabled)
        {
            foreach (var viewItem in viewItems)
            {
                viewItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), enabled);
                if (viewItem.ViewItemClass == ViewItemClass.ToggleButton.ToString())
                {
                    viewItem.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
                }
            }
        }


        [Authorize]
        public ActionResult GetGridConfigTransaction(string idInstance)
        {
            var viewModel = GetViewModelTransaction(idInstance);

            var controller = GetController();

            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(TransactionItem), nameof(GetDataTransaction), null, null, null, false, false, false);
            

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(gridConfig);

            return Content(json, "application/json");
        }


        [Authorize]
        public ActionResult GetGridConfigVoucherAdd(string idInstance)
        {
            var viewModel = GetViewModelVoucherAdd(idInstance);

            var controller = GetController();

            var resultAction = new ResultSimpleView<Dictionary<string, object>>
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath,
                ViewItems = new List<ViewItem>
                {
                    viewModel.buttonSave,
                    viewModel.buttonClear
                }
            };

            viewModel.buttonSave.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.buttonClear.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            resultAction.Result = GridFactory.CreateKendoGridConfig(typeof(VoucherGridViewItem), nameof(GetDataVoucherAdd), nameof(CreateDataVoucherAdd), nameof(UpdateDataVoucherAdd), nameof(DeleteDataVoucherAdd), false, false, false);

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(resultAction);

            return Content(json, "application/json");
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetDataTransaction(string idInstance, DateTime? start, DateTime? end)
        {
            var viewModel = GetViewModelTransaction(idInstance);

            if (start == null)
            {
                start = DateTime.Now.AddDays(-365);
            }


            if (end == null)
            {
                end = DateTime.Now;
            }

            var dateRange = new DateRange
            {
                Start = start.Value,
                End = end.Value
            };

            var controller = await GetController();

            if (viewModel.ClassObject == null )
            {
                return Json(new List<TransactionItem>(), JsonRequestBehavior.AllowGet);
            }

            if (viewModel.ClassObject.ObjectItem.GUID_Parent != controller.ClassFinancialTransaction.GUID &&
                    !controller.BaseConfig.BaseConfigToMandanten.Any(mand => mand.ID_Other == viewModel.ClassObject.ObjectItem.GUID))
            {
                return Json(new List<TransactionItem>(), JsonRequestBehavior.AllowGet);
            }

            var request = new GetTransactionsRequest(viewModel.ClassObject.ObjectItem, dateRange);
            var controllerResult = await controller.GetTransactionItems(request);

            var result = new KendoGridDataResult<TransactionItem>(controllerResult.Result);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetDataVoucherAdd(string idInstance, DateTime? start, DateTime? end)
        {
            var viewModel = GetViewModelVoucherAdd(idInstance);

            var controller = await GetController();

            if (viewModel.RefItem == null)
            {
                return Json(new List<TransactionItem>(), JsonRequestBehavior.AllowGet);
            }

            if (viewModel.RefItem.ObjectItem.GUID_Parent != controller.ClassPartner.GUID &&
                    !controller.BaseConfig.BaseConfigToMandanten.Any(mand => mand.ID_Other == viewModel.RefItem.ObjectItem.GUID))
            {
                return Json(new List<TransactionItem>(), JsonRequestBehavior.AllowGet);
            }

            var result = new KendoGridDataResult<VoucherGridViewItem>(new List<VoucherGridViewItem>());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> CreateDataVoucherAdd(string idInstance, DateTime? start, DateTime? end)
        {
            var viewModel = GetViewModelVoucherAdd(idInstance);

            var controller = await GetController();

            if (viewModel.RefItem == null)
            {
                return Json(new List<TransactionItem>(), JsonRequestBehavior.AllowGet);
            }

            var result = new KendoGridDataResult<VoucherGridViewItem>(new List<VoucherGridViewItem>());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> UpdateDataVoucherAdd(string idInstance, DateTime? start, DateTime? end)
        {
            var viewModel = GetViewModelVoucherAdd(idInstance);

            var controller = await GetController();

            if (viewModel.RefItem == null)
            {
                return Json(new List<TransactionItem>(), JsonRequestBehavior.AllowGet);
            }

            var result = new KendoGridDataResult<VoucherGridViewItem>(new List<VoucherGridViewItem>());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DeleteDataVoucherAdd(string idInstance, DateTime? start, DateTime? end)
        {
            var viewModel = GetViewModelVoucherAdd(idInstance);

            var controller = await GetController();

            if (viewModel.RefItem == null)
            {
                return Json(new List<TransactionItem>(), JsonRequestBehavior.AllowGet);
            }

            var result = new KendoGridDataResult<VoucherGridViewItem>(new List<VoucherGridViewItem>());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: Bill
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> TransactionList()
        {
            var viewModel = GetViewModelTransaction(Guid.NewGuid().ToString());
            var controller = await GetController();

            viewModel.idClassFinancialTransaction = controller.ClassFinancialTransaction.GUID;

            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> TransactionDetail()
        {
            var viewModel = GetViewModelTransactionDetail(Guid.NewGuid().ToString());
            var controller = await GetController();

            viewModel.ActionNewTransaction += $"?Sender={viewModel.IdInstance}&Class={controller.ClassFinancialTransaction.GUID}";
            viewModel.idClassTaxRates = controller.ClassTaxRates.GUID;
            viewModel.idClassUnits = controller.ClassUnit.GUID;
            viewModel.idClassCurrency = controller.ClassCurrencies.GUID;
            viewModel.idClassPartner = controller.ClassPartner.GUID;
            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> VoucherAdd()
        {
            var viewModel = GetViewModelVoucherAdd(Guid.NewGuid().ToString());
            var controller = await GetController();

            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> VoucherAddInit(string idInstance, List<string> selectorPath)
        {
            var processId = LogStartMethod();
            var resultAction = new ResultSimpleView<VoucherAddViewModel>
            {
                IsSuccessful = true,
                SelectorPath = selectorPath
            };

            if (string.IsNullOrEmpty(idInstance))
            {
                idInstance = Guid.NewGuid().ToString();
            }

            var viewModel = GetViewModelVoucherAdd(idInstance);
            var controller = await GetVoucherController();
            viewModel.VoucherParserList = controller.GetVoucherParserList();

            viewModel.SelectorPath = selectorPath;
            resultAction.Result = viewModel;

            LogEndMethod(processId: processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }
    }
}