﻿using Microsoft.AspNet.Identity;
using OModules.Hubs;
using OModules.Models;
using OModules.Notifications;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    
    public class ClipboardController : AppControllerBase
    {
        

        private OntologyItemsModule.ClipboardController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.ClipboardController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private ClipboardViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ClipboardViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ClipboardViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(Connected)),
                    ActionDisconnected = Url.Action(nameof(Disconnected)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReady)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfig)),
                    ActionValidateReference = Url.Action(nameof(ValidateReference)),
                    ActionClearClipboard = Url.Action(nameof(ClearClipboard)),
                    ActionAddItems = Url.Action(nameof(AddClipboardItems)),
                    ActionInitClipbaord = Url.Action(nameof(ClipboardInit)),
                    ActionGridCreated = Url.Action(nameof(GridCreated)),
                    ActionGridBound = Url.Action(nameof(GridBound)),
                    ActionClipboardItemsExist = Url.Action(nameof(ClipboardItemsExist)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewItemList.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.clearClipboard);
            viewModel.clearClipboard.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var controller = GetController();

            

            var result = new ResultSimpleView<bool>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                SelectorPath = viewModel.SelectorPath
            };

            clsOntologyItem objectItem = null;
            if (refItem != null)
            {

                var refItemTask = await controller.GetOItem(refItem.GUID);

                if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    LogError("Error while getting the RefItem", processId: processId);
                    result.IsSuccessful = false;
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                if (refItemTask.Result.Any())
                {
                    objectItem = new clsOntologyItem
                    {
                        GUID_Parent = refItemTask.Result.First().GUID,
                        Type = controller.Globals.Type_Object
                    };
                }

                viewModel.refItem = refItemTask.Result.FirstOrDefault();
            }
            
            var existTask = await controller.ClipboardItemsExist(objectItem);

            if (existTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while getting the exist of ClipboardItems", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = existTask.Result;

            if (viewModel.refItem != null)
            {
                viewModel.SetWindowTitle($"{viewModel.refItem.Name} ({viewModel.refItem?.Name ?? "" })");
                result.ViewItems.Add(viewModel.windowTitle);
            }
            
            viewModel.grid.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            result.ViewItems.Add(viewModel.grid);
            
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GridCreated(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = new ResultSimple
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath
            };

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            result.ViewItems.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GridBound(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = new ResultSimple
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath
            };
            viewModel.clearClipboard.ChangeViewItemValue(ViewItemType.Enable.ToString(), viewModel.ClipboardItems.Any());
            
            result.ViewItems.Add(viewModel.clearClipboard);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfig(string idInstance)
        {
            var viewModel = GetViewModel(idInstance);


            var actionResult = new ResultSimpleView<Dictionary<string,object>>
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath
            };
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(ClipboardItem), Url.Action(nameof(GetClipboardItems)), null, null, null, false, false, false);
            actionResult.Result = gridConfig;
            return Json(actionResult, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetClipboardItems(string idInstance)
        {

            var viewModel = GetViewModel(idInstance);
            var controller = GetController();

            clsOntologyItem objectItem = null;
            if (viewModel.refItem != null)
            {
                objectItem = new clsOntologyItem
                {
                    GUID_Parent = viewModel.refItem.GUID,
                    Type = controller.Globals.Type_Object
                };
            }

            var resultTask = await controller.GetClipboardItems(objectItem);

            if (resultTask.ResultState.GUID == controller.Globals.LState_Success.GUID)
            {
                viewModel.ClipboardItems = resultTask.Result;
            }
            else
            {
                viewModel.ClipboardItems = new List<ClipboardItem>();
            }

            var resultItem = new KendoGridDataResult<ClipboardItem>(resultTask.Result);
            var result = Json(resultItem, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> ClipboardItemsExist(string idInstance)
        {

            var viewModel = GetViewModel(idInstance);
            var controller = GetController();

            var resultTask = await controller.ClipboardItemsExist(viewModel.refItem);

            var resultAction = new ResultSimpleView<bool>
            {
                IsSuccessful = resultTask.ResultState.GUID == controller.Globals.LState_Success.GUID,
                SelectorPath = viewModel.SelectorPath,
                Result = resultTask.Result
            };

            return Json(resultAction);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> AddClipboardItems(List<clsOntologyItem> oItems, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var controller = GetController();

            var result = new ResultSimpleView<List<ClipboardItem>>
            {
                IsSuccessful = true,
                Result = new List<ClipboardItem>(),
                SelectorPath = viewModel.SelectorPath
            };

            if (oItems == null || !oItems.Any())
            {
                result.IsSuccessful = false;
                result.Message = "No OItems provided!";

                return Json(result);
            }

            var resultGetOItems = await controller.GetOItems(oItems.Select(obj => obj.GUID).ToList());

            if (resultGetOItems.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Error while getting the reference items!";

                return Json(result);
            }

            var resultAdd = await controller.AddClipboardItems(resultGetOItems.Result);

            if (resultAdd.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Errors while adding clipboard items";
            }

            result.Result = resultAdd.Result;

            if (result.IsSuccessful)
            {
                var message = new InterServiceMessage
                {
                    ChannelId = SpecialChannels.AddedItemToClipboard,
                    GenericParameterItems = result.Result.ToList<object>(),
                    SessionId = Session.SessionID
                };
                ModuleComHub.SendInterServiceMessageFromController(message);
            }

            LogEndMethod(processId:processId);            return Json(result);
            
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ClearClipboard(string idInstance)
        {
            var viewModel = GetViewModel(idInstance);
            var controller = GetController();

            var resultTask = await controller.ClearClipbord();

            var result = new ResultSimple
            {
                IsSuccessful = resultTask.GUID != controller.Globals.LState_Error.GUID,
                SelectorPath = viewModel.SelectorPath
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        // GET: Clipboard
        public ActionResult Index()
        {
            var viewModel = GetViewModel(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        [Authorize]
        // GET: Clipboard
        public ActionResult ClipboardInit(string idInstance, List<string> selectorPath)
        {
            var processId = LogStartMethod();            
            var resultAction = new ResultSimpleView<ClipboardViewModel>
            {
                IsSuccessful = true,
                SelectorPath = selectorPath
            };
            if (string.IsNullOrEmpty(idInstance))
            {
                idInstance = Guid.NewGuid().ToString();
            }
            var viewModel = GetViewModel(idInstance);
            viewModel.SelectorPath = selectorPath;
            resultAction.Result = viewModel;

            LogEndMethod(processId:processId);            
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }
    }
}