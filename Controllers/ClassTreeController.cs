﻿using Microsoft.AspNet.Identity;
using OModules.Globals;
using OModules.Models;
using OModules.Models.Appointment;
using OModules.Models.OClasses;
using OModules.Models.Results;
using OModules.Services;
using OntologyItemsModule;
using OntologyItemsModule.Factories;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class ClassTreeController : AppControllerBase
    {
        

        private OntologyItemsModule.ClassTreeController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });


            var result = new OntologyItemsModule.ClassTreeController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private ClassTreeViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ClassTreeViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ClassTreeViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigClassTree
                    {
                        GetClassPath = Url.Action(nameof(GetClassPath)),
                        SelectedClass = Url.Action(nameof(SelectClass)),
                        ActionConnected = Url.Action(nameof(Connected)),
                        ActionDisconnected = Url.Action(nameof(Disconnected)),
                        ActionSetViewReady = Url.Action(nameof(SetViewReady)),
                        ActionInit = Url.Action(nameof(ClassTreeInit))
                    },
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        

        // GET: ClassTree
        [Authorize]
        public ActionResult ClassTree()
        {
            var processId = LogStartMethod();
            var model = GetViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(model);
        }

        // GET: ClassTree
        [Authorize]
        public ActionResult ClassTreeInit(string idInstance, List<string> selectorPath)
        {
            var processId = LogStartMethod();
            var resultAction = new ResultSimpleView<ClassTreeViewModel>
            {
                IsSuccessful = true,
                SelectorPath = selectorPath
            };

            if (string.IsNullOrEmpty(idInstance))
            {
                idInstance = Guid.NewGuid().ToString();
            }

            var model = GetViewModel(idInstance);
            model.SelectorPath = selectorPath;
            resultAction.Result = model;

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetClassTreeConfig(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var controller = GetController();

            var resultController = await controller.GetClassTreeConfiguration();

            var result = new ResultSimpleView<ClassTreeConfiguration>
            {
                IsSuccessful = resultController.ResultState.GUID != controller.Globals.LState_Error.GUID,
                Message = resultController.ResultState.Additional1,
                SelectorPath = viewModel.SelectorPath

            };

            if (!result.IsSuccessful)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = resultController.Result;

            LogEndMethod(processId:processId);            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetClassPath(string idInstance, string IdClass, string SearchTerm)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<GetClassPathResult>
            {
                IsSuccessful = true
            };

            var viewModel = GetViewModel(idInstance);
            result.SelectorPath = viewModel.SelectorPath;

            var classSelectionItem = new ClassSelectItem
            {
                IdClass = IdClass,
                SearchTerm = SearchTerm,
                OnlyPathItems = false
            };
            
            var classTreeController = GetController();

            var resultTask = await classTreeController.GetClassList(classSelectionItem);
            result.IsSuccessful = resultTask.Result.GUID == classTreeController.Globals.LState_Success.GUID;
            result.Message = resultTask.Result.Additional1;
            result.Result = new GetClassPathResult
            {
                SearchNodes = resultTask.ClassPathNodes,
                TreeNodes = resultTask.TreeNodes
            };

            var resultJson = Json(result, JsonRequestBehavior.AllowGet);
            resultJson.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return resultJson;
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> SelectClass()
        {
            var processId = LogStartMethod();
            var classSelectionItem = new ClassSelectItem
            {
                IdClass = Request.Params["IdClass"],
                SearchTerm = Request.Params["SearchTerm"],
                OnlyPathItems = false
            };


            var classTreeController = GetController();
            var resultTask = await classTreeController.GetJsonClassTree(classSelectionItem);



            var result = Content(resultTask.Json, "application/json");

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var viewItemValue = viewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.breadCrumb.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.classList.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.breadCrumb);
            viewItemList.Add(viewModel.classList);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.breadCrumb.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.classList.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            viewItemList.Add(viewModel.breadCrumb);
            viewItemList.Add(viewModel.classList);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }
    }
}