﻿using ActiveDirectorySyncModule.Models;
using AutomationLibrary.Models;
using BCMCSVImporter.Models;
using GitConnectorModule.Models;
using JsonModule.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.MessageOutput;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using PsScriptOutputParserModule.Models;
using SQLImporter.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.ApplicationInsights.Channel;
using TFSConnectorModule.Models;
using WrikeConnectorModule.Models;

namespace OModules.Controllers
{


    public class ToolboxController : AppControllerBase
    {
        private SecurityModule.SecurityController GetControllerSecurity()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var securityController = memoryCache.GetOrSet<SecurityModule.SecurityController>(Session.SessionID, $"{nameof(SecurityModule)}{nameof(SecurityModule.SecurityController)}", () =>
            {
                return new SecurityModule.SecurityController(globals);
            });

            LogEndMethod(processId:processId);            return securityController;
        }

        private List<JsonPropertyMetaItem> SetOrGetJsonPropertyCache(string idInstance, List<JsonPropertyMetaItem> passwordCacheItems = null, bool clearCache = false)
        {
            var processId = LogStartMethod();
            if (passwordCacheItems == null)
            {
                passwordCacheItems = new List<JsonPropertyMetaItem>();
            }
            var memoryCache = new InMemoryCache();

            var key = $"jsonPropertyCache{idInstance}";
            if (clearCache)
            {
                memoryCache.RemoveKey(Session.SessionID, key);
            }

            var result = memoryCache.GetOrSet<List<JsonPropertyMetaItem>>(Session.SessionID, key,
                () => passwordCacheItems);

            LogEndMethod(processId:processId);            return result;
        }

        private JsonModule.JsonController GetControllerJsonEditor()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var jsonEditorController = memoryCache.GetOrSet<JsonModule.JsonController>(Session.SessionID, $"{nameof(JsonModule)}{nameof(JsonModule.JsonController)}", () =>
            {
                return new JsonModule.JsonController(globals);
            });

            LogEndMethod(processId:processId);            return jsonEditorController;
        }

        private AutomationLibrary.AutomationLibraryController GetControllerAutomationLibrary()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var jsonEditorController = memoryCache.GetOrSet<AutomationLibrary.AutomationLibraryController>(Session.SessionID, $"{nameof(AutomationLibrary)}{nameof(AutomationLibrary.AutomationLibraryController)}", () =>
            {
                return new AutomationLibrary.AutomationLibraryController(globals);
            });

            LogEndMethod(processId:processId);            return jsonEditorController;
        }

        private BatchGridViewModel GetViewModelBatchGrid(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<BatchGridViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new BatchGridViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfigBatchGrid)),
                    IdSession = HttpContext.Session.SessionID
                };

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private ModuleExecutionPropertiesViewModel GetViewModelModuleExecutionProperties(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ModuleExecutionPropertiesViewModel>(Session.SessionID, $"moduleExecutionProperties{key}", () =>
            {
                var viewModelNew = new ModuleExecutionPropertiesViewModel(Session, $"moduleExecutionProperties_{key}", User.Identity.GetUserId())
                {
                    ActionGetDropDownConfig = Url.Action(nameof(GetDropDownConfigModuleExeuctionConfig)),
                    ActionJsonEditorInit = Url.Action(nameof(JsonEditorInit)),
                    ActionExecute = Url.Action(nameof(ExecuteModuleConfiguration)),
                    IdSession = HttpContext.Session.SessionID
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueModuleExeuctionProperties)));
                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private ModuleExecutionLogViewModel GetViewModelModuleExecutionLog(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ModuleExecutionLogViewModel>(Session.SessionID, $"moduleExecutionLog{key}", () =>
            {
                var viewModelNew = new ModuleExecutionLogViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfigModuleExecutionLog)),
                    IdSession = HttpContext.Session.SessionID
                };

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private JsonEditorViewModel GetViewModelJsonEditor(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<JsonEditorViewModel>(Session.SessionID, $"jsonEditor{key}", () =>
            {
                var viewModelNew = new JsonEditorViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedJsonEditor)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedJsonEditor)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyJsonEditor)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceJsonEditor)),
                    IdSession = HttpContext.Session.SessionID
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueJsonEditor)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private ToolBoxViewModel GetViewModelToolBox(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ToolBoxViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ToolBoxViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedToolBox)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedToolBox)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyToolBox)),
                    ActionBatchGridInit = Url.Action(nameof(BatchGridInit)),
                    ActionModuleExecutionLogInit = Url.Action(nameof(ModuleExecutionLogInit)),
                    ActionModuleExecutionPropertiesInit = Url.Action(nameof(ModuleExecutionPropertiesInit)),
                    IdSession = HttpContext.Session.SessionID
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueToolBox)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private DiffTextViewModel GetViewModelDiffText(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<DiffTextViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new DiffTextViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedDiffText)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedDiffText)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyDiffText)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceDiffText)),
                    ActionGetToolsForToolbar = Url.Action(nameof(GetToolsForToolbar)),
                    ActionHtmlDiff = Url.Action(nameof(HtmlEditController.HtmlDiff), nameof(HtmlEditController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueDiffText)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetDropDownConfigModuleExeuctionConfig()
        {
            var processId = LogStartMethod();
            var controller = GetControllerAutomationLibrary();

            var resultDrowDownConfig = await controller.GetDropdownConfigExecutionConfig(Url.Action(nameof(GetDropDownDataModuleExecutionConfig)), Url.Action(nameof(GetDropDownDataModuleMetaConfig)));

            LogEndMethod(processId:processId);
            return Json(resultDrowDownConfig.Result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ExecuteModuleConfiguration(string idConfiguration, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelModuleExecutionProperties(idInstance);
            var configuration = viewModel.ModuleConfigurations.FirstOrDefault(modConf => modConf.IdModuleConfiguration == idConfiguration);
            var controller = GetControllerAutomationLibrary();

            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetDropDownDataModuleExecutionConfig(string idModule, string idFunction, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelModuleExecutionProperties(idInstance);

            var controller = GetControllerAutomationLibrary();

            var request = new GetDataExecutionConfigurationRequest(idModule, idFunction)
            {
                ModuleItems = viewModel.ModuleItems
            };
            var dataResult = await controller.GetDataExecutionConfiguration(request);

            if (dataResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                Response.Status = dataResult.ResultState.Additional1;
                return null;
            }

            viewModel.ModuleConfigurations = dataResult.Result;
            viewModel.ModuleItems = request.ModuleItems;

            LogEndMethod(processId:processId);
            return Json(dataResult.Result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetDropDownDataModuleMetaConfig(string idModuleConfiguration, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelModuleExecutionProperties(idInstance);

            var controller = GetControllerAutomationLibrary();

            var configuration = viewModel.ModuleConfigurations.FirstOrDefault(config => config.IdModuleConfiguration == idModuleConfiguration);

            var securityController = GetControllerSecurity();

            

            LogEndMethod(processId:processId);
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        private BarcodeModule.BarCodeController GetControllerBarcode()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new BarcodeModule.BarCodeController(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private MediaStore_Module.MediaStoreConnector GetControllerMediaStore()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new MediaStore_Module.MediaStoreConnector(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private AutomationLibrary.AutomationLibraryController GetAutomationLibraryController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new AutomationLibrary.AutomationLibraryController(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public ActionResult GetGridConfigBatchGrid()
        {
            var processId = LogStartMethod();
            var resultGridConfig = GridFactory.CreateKendoGridConfig(typeof(KendoModuleItem), Url.Action(nameof(GetGridDataBatchGrid)), null, null, null, false, false, false);

            var result = new ResultSimpleView<Dictionary<string, object>>
            {
                IsSuccessful = true,
                Result = resultGridConfig
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetGridConfigModuleExecutionLog()
        {
            var processId = LogStartMethod();
            var resultGridConfig = GridFactory.CreateKendoGridConfig(typeof(OutputGridViewItem), Url.Action(nameof(GetGridDataModuleExecutionLog)), null, null, null, false, false, false);

            var result = new ResultSimpleView<Dictionary<string, object>>
            {
                IsSuccessful = true,
                Result = resultGridConfig
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetGridDataBatchGrid(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelToolBox(idInstance);

            var controller = GetAutomationLibraryController();

            var controllerResult = await controller.GetKendoModuleItems();

            if (controllerResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                return Json(new List<KendoModuleItem>(), JsonRequestBehavior.AllowGet);
            }

            viewModel.ModuleItems = controllerResult.Result;

            LogEndMethod(processId:processId);
            var result = new KendoGridDataResult<KendoModuleItem>(controllerResult.Result);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetGridDataModuleExecutionLog(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelToolBox(idInstance);


            LogEndMethod(processId:processId);
            var result = new KendoGridDataResult<KendoModuleItem>(new List<KendoModuleItem>());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private BCMCSVImporter.CSVToSQLScript GetControllerCSVToSQL()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });


            var result = new BCMCSVImporter.CSVToSQLScript(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private TFSConnectorModule.TFSConnector GetControllerTFSSync()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });


            var result = new TFSConnectorModule.TFSConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }


        private GeminiConnectorModule.GeminiConnector GetControllerGeminiSync()
        {
            var processId = LogStartMethod();

            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new GeminiConnectorModule.GeminiConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }



        private GitConnectorModule.GitConnector GetControllerGitSync()
        {
            var processId = LogStartMethod();

            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new GitConnectorModule.GitConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private GoogleCalendarConnector.CalendarConnector GetControllerGoogleCalendarSync()
        {
            var processId = LogStartMethod();

            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new GoogleCalendarConnector.CalendarConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private MailModule.MailConnector GetControllerMailSync()
        {
            var processId = LogStartMethod();

            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new MailModule.MailConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private JiraConnectorModule.JiraController GetControllerJiraSync()
        {
            var processId = LogStartMethod();

            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new JiraConnectorModule.JiraController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private ActiveDirectorySyncModule.ActiveDirectorySyncController GetControllerActiveDirectorySync()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });


            var result = new ActiveDirectorySyncModule.ActiveDirectorySyncController(globals);

            LogEndMethod(processId:processId);
            return result;

        }


        private PsScriptOutputParserModule.PsScriptOutputParserController GetControllerPSScriptOutput()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });


            var result = new PsScriptOutputParserModule.PsScriptOutputParserController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private SQLImporter.SQLImporterConnector GetControllerSQLImporter()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });


            var result = new SQLImporter.SQLImporterConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private CSVToSQLViewModel GetViewModelCSVToSQL(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<CSVToSQLViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new CSVToSQLViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedCSVToSQL)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedCSVToSQL)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyCSVToSQL)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceCSVToSQL)),
                    ActionDownloadSQLZip = Url.Action(nameof(DownloadSQLZip)),
                    ActionGetToolsForToolbar = Url.Action(nameof(GetToolsForToolbar)),
                    ActionGetRunningState = Url.Action(nameof(GetSaveState)),
                    ActionAuthenticationInit = Url.Action(nameof(SecurityController.AuthenticationInit), nameof(SecurityController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueCSVToSQL)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

            return viewModel;
        }

        private TFSSyncViewModel GetViewModelTFSSync(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<TFSSyncViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new TFSSyncViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedTFSSync)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedTFSSync)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyTFSSync)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceTFSSync)),
                    ActionGetToolsForToolbar = Url.Action(nameof(GetToolsForToolbar)),
                    ActionGetRunningState = Url.Action(nameof(GetTFSSyncState)),
                    ActionSyncTFS = Url.Action(nameof(SyncTFS)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueTFSSync)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

            return viewModel;
        }

        private GitSyncViewModel GetViewModelGitSync(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<GitSyncViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new GitSyncViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedGitSync)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedGitSync)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyGitSync)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceGitSync)),
                    ActionGetToolsForToolbar = Url.Action(nameof(GetToolsForToolbar)),
                    ActionGetRunningState = Url.Action(nameof(GetGitSyncState)),
                    ActionSyncGit = Url.Action(nameof(SyncGit)),
                    ActionGetGridConfigObjectList = Url.Action(nameof(OItemListController.GetGridConfigObjects), nameof(OItemListController).Replace("Controller", "")),
                    ActionNewItem = Url.Action(nameof(EditOItemController.EditOItem), nameof(EditOItemController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueGitSync)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

            return viewModel;
        }

        private GoogleCalendarSyncViewModel GetViewModelGoogleCalendarSync(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<GoogleCalendarSyncViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new GoogleCalendarSyncViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedGoogleCalendarSync)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedGoogleCalendarSync)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyGoogleCalendarSync)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceGoogleCalendarSync)),
                    ActionGetToolsForToolbar = Url.Action(nameof(GetToolsForToolbar)),
                    ActionGetRunningState = Url.Action(nameof(GetGoogleCalendarSyncState)),
                    ActionSyncGoogleCalendar = Url.Action(nameof(SyncGoogleCalendar)),
                    ActionGetGridConfigObjectList = Url.Action(nameof(OItemListController.GetGridConfigObjects), nameof(OItemListController).Replace("Controller", "")),
                    ActionNewItem = Url.Action(nameof(EditOItemController.EditOItem), nameof(EditOItemController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueGoogleCalendarSync)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

            return viewModel;
        }

        private MailSyncViewModel GetViewModelMailSync(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<MailSyncViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new MailSyncViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedMailSync)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedMailSync)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyMailSync)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceMailSync)),
                    ActionGetToolsForToolbar = Url.Action(nameof(GetToolsForToolbar)),
                    ActionGetRunningState = Url.Action(nameof(GetMailSyncState)),
                    ActionSyncMail = Url.Action(nameof(SyncMail)),
                    ActionGetGridConfigObjectList = Url.Action(nameof(OItemListController.GetGridConfigObjects), nameof(OItemListController).Replace("Controller", "")),
                    ActionNewItem = Url.Action(nameof(EditOItemController.EditOItem), nameof(EditOItemController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueMailSync)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

            return viewModel;
        }

        private JiraSyncViewModel GetViewModelJiraSync(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<JiraSyncViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new JiraSyncViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedJiraSync)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedJiraSync)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyJiraSync)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceJiraSync)),
                    ActionGetToolsForToolbar = Url.Action(nameof(GetToolsForToolbar)),
                    ActionGetRunningState = Url.Action(nameof(GetJiraSyncState)),
                    ActionSyncJira = Url.Action(nameof(SyncJira)),
                    ActionGetGridConfigObjectList = Url.Action(nameof(OItemListController.GetGridConfigObjects), nameof(OItemListController).Replace("Controller", "")),
                    ActionNewItem = Url.Action(nameof(EditOItemController.EditOItem), nameof(EditOItemController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueJiraSync)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

            return viewModel;
        }


        private GeminiSyncViewModel GetViewModelGeminiSync(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<GeminiSyncViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new GeminiSyncViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedGeminiSync)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedGeminiSync)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyGeminiSync)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceGeminiSync)),
                    ActionGetToolsForToolbar = Url.Action(nameof(GetToolsForToolbar)),
                    ActionGetRunningState = Url.Action(nameof(GetGeminiSyncState)),
                    ActionSyncGemini = Url.Action(nameof(SyncGemini)),
                    ActionAuthenticationInit = Url.Action(nameof(SecurityController.AuthenticationInit), nameof(SecurityController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueGeminiSync)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

            return viewModel;
        }

        private ActiveDirectorySyncViewModel GetViewModelActiveDirectorySync(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ActiveDirectorySyncViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ActiveDirectorySyncViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedActiveDirectorySync)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedActiveDirectorySync)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyActiveDirectorySync)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceActiveDirectorySync)),
                    ActionGetToolsForToolbar = Url.Action(nameof(GetToolsForToolbar)),
                    ActionGetRunningState = Url.Action(nameof(GetActiveDirectorySyncState)),
                    ActionSyncActiveDirectory = Url.Action(nameof(SyncActiveDirectory)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueActiveDirectorySync)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

            return viewModel;
        }


        private SQLImporterViewModel GetViewModelSQLImport(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<SQLImporterViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new SQLImporterViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedImportSQL)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedImportSQL)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyImportSQL)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceImportSQL)),
                    ActionGetToolsForToolbar = Url.Action(nameof(GetToolsForToolbar)),
                    ActionGetRunningState = Url.Action(nameof(GetImportSQLState)),
                    ActionImportSQL = Url.Action(nameof(ImportSQL)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueImportSQL)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

            return viewModel;
        }

        private PSScriptOutputViewModel GetViewModelPSScriptOutput(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<PSScriptOutputViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new PSScriptOutputViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedPSScriptOutput)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedPSScriptOutput)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyPSScriptOutput)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferencePSScriptOutput)),
                    ActionGetToolsForToolbar = Url.Action(nameof(GetToolsForToolbar)),
                    ActionGetRunningState = Url.Action(nameof(GetPSScriptOutputState)),
                    ActionGetScriptOutput = Url.Action(nameof(GetScriptOutput)),
                    ActionAuthenticationInit = Url.Action(nameof(SecurityController.AuthenticationInit), nameof(SecurityController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValuePSScriptOutput)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueToolBox(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelToolBox(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueModuleExeuctionProperties(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelModuleExecutionProperties(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueJsonEditor(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelJsonEditor(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                resultAction.IsSuccessful = false;
                resultAction.ResultMessage = "ViewItem not found!";
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }
            if (viewItem.ViewItemId == viewModel.editItem.ViewItemId)
            {
                var content = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(((string[])viewItem.GetViewItemValueItem(ViewItemType.Content.ToString()).Value)[0].ToString() ?? ""));
                var controller = GetControllerJsonEditor();

                if (viewModel.editOItem != null)
                {
                    var setRequest = new SetJsonAttributeRequest(viewModel.editOItem, content);
                    var setResult = await controller.SetJsonAttribute(setRequest);
                    if (setResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        viewModel.editItem.ChangeViewItemValue(ViewItemType.Id.ToString(), viewModel.editOItem.ID_Attribute);
                        viewModel.editItem.ChangeViewItemValue(ViewItemType.Content.ToString(), System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(viewModel.editOItem.Val_String)));
                        resultAction.IsSuccessful = false;
                        resultAction.ResultMessage = setResult.ResultState.Additional1;
                    }
                    else
                    {
                        resultAction.ResultMessage = "Json saved!";
                    }
                }
                else if (viewModel.GetEsIndexItemRequest != null)
                {
                    var dict = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(content);
                    var saveResult = await controller.SaveJsonDocument(viewModel.GetEsIndexItemRequest, dict);
                    if (saveResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        resultAction.IsSuccessful = false;
                        resultAction.ResultMessage = saveResult.ResultState.Additional1;
                    }
                    else
                    {
                        resultAction.ResultMessage = "Json saved!";
                    }
                }
                
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueDiffText(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelDiffText(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueCSVToSQL(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelCSVToSQL(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueTFSSync(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTFSSync(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueGeminiSync(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGeminiSync(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueGitSync(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGitSync(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);


            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueGoogleCalendarSync(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGoogleCalendarSync(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);


            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueMailSync(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMailSync(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);


            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueJiraSync(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelJiraSync(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);


            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueActiveDirectorySync(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelActiveDirectorySync(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }



        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValuePSScriptOutput(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPSScriptOutput(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueImportSQL(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelSQLImport(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedToolBox()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelToolBox(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedJsonEditor()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelJsonEditor(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedJsonEditor()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelJsonEditor(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedDiffText()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelDiffText(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);

            viewModel.baseText.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewItemList.Add(viewModel.baseText);
            viewModel.newText.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewItemList.Add(viewModel.newText);
            viewModel.sidebyside.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewItemList.Add(viewModel.sidebyside);
            viewModel.inline.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewItemList.Add(viewModel.inline);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedToolBox()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelToolBox(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedDiffText()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelDiffText(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedCSVToSQL()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelCSVToSQL(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);


            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedCSVToSQL()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelCSVToSQL(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public ActionResult ConnectedTFSSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTFSSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);


            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedGeminiSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelGeminiSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedGitSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelGitSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedGoogleCalendarSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelGoogleCalendarSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedMailSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelMailSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public ActionResult ConnectedJiraSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelJiraSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedActiveDirectorySync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelActiveDirectorySync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);


            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedImportSQL()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelSQLImport(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);


            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedTFSSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTFSSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedGeminiSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelGeminiSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedGitSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelGitSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedGoogleCalendarSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelGoogleCalendarSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedMailSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelMailSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedJiraSync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelJiraSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedActiveDirectorySync()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelActiveDirectorySync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }



        [Authorize]
        public ActionResult ConnectedPSScriptOutput()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelPSScriptOutput(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);


            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedPSScriptOutput()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTFSSync(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedImportSQL()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelSQLImport(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyToolBox(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelToolBox(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyJsonEditor(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelJsonEditor(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyDiffText(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelDiffText(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyCSVToSQL(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelCSVToSQL(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyTFSSync(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTFSSync(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]

        public ActionResult SetViewReadyGeminiSync(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGeminiSync(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyGitSync(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGitSync(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyGoogleCalendarSync(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGoogleCalendarSync(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyMailSync(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMailSync(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyJiraSync(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelJiraSync(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyActiveDirectorySync(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelActiveDirectorySync(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }



        [Authorize, HttpPost]
        public ActionResult SetViewReadyPSScriptOutput(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTFSSync(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyImportSQL(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelSQLImport(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult GetToolsForToolbar(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelDiffText(idInstance);

            var result = new List<ToolForToolbar>();

            result.Add(new ToolForToolbar
            {
                FontClass = "fa-file-text-o",
                Url = Url.Action(nameof(DiffText))
            });

            result.Add(new ToolForToolbar
            {
                FontClass = "",
                Caption = "CSV To SQL",
                Url = Url.Action(nameof(CSVToSQL))
            });

            result.Add(new ToolForToolbar
            {
                FontClass = "",
                Caption = "TFS Sync",
                Url = Url.Action(nameof(TFSSync))
            });

            result.Add(new ToolForToolbar
            {
                FontClass = "",
                Caption = "Sync Git Projects",
                Url = Url.Action(nameof(GitSync))
            });

            result.Add(new ToolForToolbar
            {
                FontClass = "",
                Caption = "Sync Jira",
                Url = Url.Action(nameof(JiraIssueSync))
            });

            result.Add(new ToolForToolbar
            {
                FontClass = "",
                Caption = "Wrike Sync",
                Url = Url.Action(nameof(WrikeController.WrikeSync), nameof(WrikeController).Replace("Controller", ""))
            });

            result.Add(new ToolForToolbar
            {
                FontClass = "",
                Caption = "Import SQL",
                Url = Url.Action(nameof(SQLImporter))
            });

            result.Add(new ToolForToolbar
            {
                FontClass = "",
                Caption = "Execute Powershell",
                Url = Url.Action(nameof(PSScriptOutput))
            });

            result.Add(new ToolForToolbar
            {
                FontClass = "",
                Caption = "Sync ActiveDirecotry",
                Url = Url.Action(nameof(ActiveDirectorySync))
            });

            result.Add(new ToolForToolbar
            {
                FontClass = "",
                Caption = "Sync Gemini Issues",
                Url = Url.Action(nameof(GeminiSync))
            });

            result.Add(new ToolForToolbar
            {
                FontClass = "",
                Caption = "Sync Mails",
                Url = Url.Action(nameof(MailSync))
            });

            result.Add(new ToolForToolbar
            {
                FontClass = "",
                Caption = "Sync Google Calendar",
                Url = Url.Action(nameof(GoogleCalendarSync))
            });

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceJsonEditor(clsOntologyItem refItem, GetEsIndexItemRequest getEsIndexItemRequest, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelJsonEditor(idInstance);

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var jsonController = GetControllerJsonEditor();

            if (refItem != null)
            {
                var getJsonAttributeRequest = new GetJsonAttributeRequest(refItem.GUID)
                {
                    MessageOutput = new OWMessageOutput(idInstance)
                };
                var getJsonAttributeResult = await jsonController.GetJsonAttribute(getJsonAttributeRequest);

                if (getJsonAttributeResult.ResultState.GUID == jsonController.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                    LogEndMethod(processId:processId);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                viewModel.editOItem = getJsonAttributeResult.Result.Json;
                viewModel.schemaOItem = getJsonAttributeResult.Result.Schema;

                result.ViewItems.Add(viewModel.editItem);
                result.ViewItems.Add(viewModel.schemaItem);
                viewModel.editItem.ChangeViewItemValue(ViewItemType.Id.ToString(), getJsonAttributeResult.Result.Json.ID_Attribute);
                viewModel.editItem.ChangeViewItemValue(ViewItemType.Content.ToString(), System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(getJsonAttributeResult.Result.Json.Val_String)));
                if (getJsonAttributeResult.Result.Schema != null)
                {
                    viewModel.schemaItem.ChangeViewItemValue(ViewItemType.Id.ToString(), getJsonAttributeResult.Result.Schema.ID_Attribute);
                    viewModel.schemaItem.ChangeViewItemValue(ViewItemType.Content.ToString(), System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(getJsonAttributeResult.Result.Schema.Val_String)));
                }
            }
            else
            {
                viewModel.GetEsIndexItemRequest = getEsIndexItemRequest;
                viewModel.GetEsIndexItemRequest.MessageOutput = new OWMessageOutput(idInstance);
                var getEsIndexItemResult = await jsonController.GetEsIndexItem(viewModel.GetEsIndexItemRequest);
                if (getEsIndexItemResult.ResultState.GUID == jsonController.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                    result.ResultMessage = getEsIndexItemResult.ResultState.Additional1;
                    LogEndMethod(processId:processId);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                viewModel.editItem.ChangeViewItemValue(ViewItemType.Id.ToString(), getEsIndexItemResult.Result.IdItem);
                result.ViewItems.Add(viewModel.editItem);
                var jsonValue = Newtonsoft.Json.JsonConvert.SerializeObject(getEsIndexItemResult.Result.ElasticDocument.Dict);
                viewModel.editItem.ChangeViewItemValue(ViewItemType.Content.ToString(), System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(jsonValue)));
            }
            
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceDiffText(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelDiffText(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetSaveState(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelCSVToSQL(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.ViewItems.Add(viewModel.createSQL);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<ResultPreImport>>("Global", "CSVImport", () =>
            {
                return null;
            });
            if (csvImport == null)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "No result");
                viewModel.createSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }


            if (csvImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation ||
                csvImport.Status == System.Threading.Tasks.TaskStatus.Running)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Is Running");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                viewModel.createSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            }
            else if (csvImport.Status == TaskStatus.RanToCompletion)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Url.ToString(), Url.Action(nameof(DownloadResult)));
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                viewModel.createSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            }
            else
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Error while running");
                viewModel.createSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                LogEndMethod(processId:processId);                memoryCache.RemoveKey("Global", "CSVImport");
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetPSScriptOutputState(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPSScriptOutput(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.ViewItems.Add(viewModel.getPSOutput);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<ScriptOutputResult>>("Global", "PSScriptOutput", () =>
            {
                return null;
            });
            if (csvImport == null)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "No result");
                viewModel.getPSOutput.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }


            if (csvImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation ||
                csvImport.Status == System.Threading.Tasks.TaskStatus.Running)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Is Running");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                viewModel.getPSOutput.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            }
            else if (csvImport.Status == TaskStatus.RanToCompletion)
            {

                if (string.IsNullOrEmpty(csvImport.Result.ResultState.Additional1))
                {
                    viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), HttpUtility.HtmlEncode(csvImport.Result.Output??"").Replace("\n", "<br/>"));
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                    viewModel.getPSOutput.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

                }
                else
                {
                    viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), HttpUtility.HtmlEncode(csvImport.Result.ResultState.Additional1).Replace("\n", "<br/>"));
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                    viewModel.getPSOutput.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                }
                memoryCache.RemoveKey("Global", "PSScriptOutput");

            }
            else
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Error while running");
                viewModel.getPSOutput.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                LogEndMethod(processId:processId);                memoryCache.RemoveKey("Global", "PSScriptOutput");
                return Json(result, JsonRequestBehavior.AllowGet);

            }


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> GetBarcodeFilePath(string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<string>
            {
                IsSuccessful = true
            };

            var imageRootPath = Server.MapPath($"~/Resources/UserGroupRessources");



            LogEndMethod(processId:processId);            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetTFSSyncState(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTFSSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.ViewItems.Add(viewModel.syncTFS);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);

            var memoryCache = new InMemoryCache();
            var tfsSync = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<SyncTFSResult>>>("Global", "TFSSync", () =>
            {
                return null;
            });
            if (tfsSync == null)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "No result");
                viewModel.syncTFS.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }


            if (tfsSync.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation ||
                tfsSync.Status == System.Threading.Tasks.TaskStatus.Running)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Is Running");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                viewModel.syncTFS.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            }
            else if (tfsSync.Status == TaskStatus.RanToCompletion)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                viewModel.syncTFS.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                memoryCache.RemoveKey("Global", "TFSSync");
            }
            else
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Error while running");
                viewModel.syncTFS.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                LogEndMethod(processId:processId);                memoryCache.RemoveKey("Global", "TFSSync");
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetGeminiSyncState(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGeminiSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.ViewItems.Add(viewModel.syncGemini);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);

            var memoryCache = new InMemoryCache();
            var GeminiSync = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<GeminiConnectorModule.Models.SyncIssuesResult>>>("Global", "GeminiSync", () =>
            {
                return null;
            });
            if (GeminiSync == null)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "No result");
                viewModel.syncGemini.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }


            if (GeminiSync.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation ||
                GeminiSync.Status == System.Threading.Tasks.TaskStatus.Running)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Is Running");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                viewModel.syncGemini.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            }
            else if (GeminiSync.Status == TaskStatus.RanToCompletion)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                viewModel.syncGemini.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                memoryCache.RemoveKey("Global", "GeminiSync");
            }
            else
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Error while running");
                viewModel.syncGemini.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                LogEndMethod(processId:processId);                memoryCache.RemoveKey("Global", "GeminiSync");
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetGitSyncState(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGitSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.ViewItems.Add(viewModel.syncGit);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);

            var memoryCache = new InMemoryCache();
            var GitSync = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<SyncGitProjectsResult>>>("Global", "GitSync", () =>
            {
                return null;
            });
            if (GitSync == null)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "No result");
                viewModel.syncGit.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }


            if (GitSync.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation ||
                GitSync.Status == System.Threading.Tasks.TaskStatus.Running)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Is Running");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                viewModel.syncGit.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            }
            else if (GitSync.Status == TaskStatus.RanToCompletion)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                viewModel.syncGit.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                memoryCache.RemoveKey("Global", "GitSync");
            }
            else
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Error while running");
                viewModel.syncGit.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                LogEndMethod(processId:processId);                memoryCache.RemoveKey("Global", "GitSync");
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetGoogleCalendarSyncState(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGoogleCalendarSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.ViewItems.Add(viewModel.syncGoogleCalendar);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);

            var memoryCache = new InMemoryCache();
            var GoogleCalendarSync = memoryCache.GetOrSet<System.Threading.Tasks.Task<GoogleCalendarConnector.Models.CalendarSyncResult>>("Global", "GoogleCalendarSync", () =>
            {
                return null;
            });
            if (GoogleCalendarSync == null)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "No result");
                viewModel.syncGoogleCalendar.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }


            if (GoogleCalendarSync.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation ||
                GoogleCalendarSync.Status == System.Threading.Tasks.TaskStatus.Running)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Is Running");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                viewModel.syncGoogleCalendar.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            }
            else if (GoogleCalendarSync.Status == TaskStatus.RanToCompletion)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                viewModel.syncGoogleCalendar.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                memoryCache.RemoveKey("Global", "GoogleCalendarSync");
            }
            else
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Error while running");
                viewModel.syncGoogleCalendar.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                LogEndMethod(processId:processId);                memoryCache.RemoveKey("Global", "GoogleCalendarSync");
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetMailSyncState(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMailSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.ViewItems.Add(viewModel.syncMail);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);

            var memoryCache = new InMemoryCache();
            var MailSync = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<MailModule.Models.ImportMailsResult>>>("Global", "MailSync", () =>
            {
                return null;
            });
            if (MailSync == null)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "No result");
                viewModel.syncMail.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }


            if (MailSync.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation ||
                MailSync.Status == System.Threading.Tasks.TaskStatus.Running)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Is Running");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                viewModel.syncMail.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            }
            else if (MailSync.Status == TaskStatus.RanToCompletion)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                viewModel.syncMail.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                memoryCache.RemoveKey("Global", "MailSync");
            }
            else
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Error while running");
                viewModel.syncMail.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                LogEndMethod(processId:processId);                memoryCache.RemoveKey("Global", "MailSync");
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetJiraSyncState(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelJiraSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.ViewItems.Add(viewModel.syncJira);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);

            var memoryCache = new InMemoryCache();
            var JiraSync = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<JiraConnectorModule.Models.SyncIssuesResult>>>("Global", "JiraSync", () =>
            {
                return null;
            });
            if (JiraSync == null)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "No result");
                viewModel.syncJira.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }


            if (JiraSync.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation ||
                JiraSync.Status == System.Threading.Tasks.TaskStatus.Running)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Is Running");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                viewModel.syncJira.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            }
            else if (JiraSync.Status == TaskStatus.RanToCompletion)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                viewModel.syncJira.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                memoryCache.RemoveKey("Global", "JiraSync");
            }
            else
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Error while running");
                viewModel.syncJira.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                LogEndMethod(processId:processId);                memoryCache.RemoveKey("Global", "JiraSync");
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetActiveDirectorySyncState(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelActiveDirectorySync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.ViewItems.Add(viewModel.syncActiveDirectory);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);

            var memoryCache = new InMemoryCache();
            var activeDirectorySync = memoryCache.GetOrSet<System.Threading.Tasks.Task<clsOntologyItem>>("Global", "ActiveDirectorySync", () =>
            {
                return null;
            });
            if (activeDirectorySync == null)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "No result");
                viewModel.syncActiveDirectory.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }


            if (activeDirectorySync.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation ||
                activeDirectorySync.Status == System.Threading.Tasks.TaskStatus.Running)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Is Running");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                viewModel.syncActiveDirectory.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            }
            else if (activeDirectorySync.Status == TaskStatus.RanToCompletion)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                viewModel.syncActiveDirectory.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                memoryCache.RemoveKey("Global", "ActiveDirectorySync");
            }
            else
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Error while running");
                viewModel.syncActiveDirectory.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                LogEndMethod(processId:processId);                memoryCache.RemoveKey("Global", "ActiveDirectorySync");
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetImportSQLState(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelSQLImport(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.ViewItems.Add(viewModel.importSQL);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);

            var memoryCache = new InMemoryCache();
            var sqlImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<ImportSQLResult>>("Global", "SQLImporter", () =>
            {
                return null;
            });
            if (sqlImport == null)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "No result");
                viewModel.importSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }


            if (sqlImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation ||
                sqlImport.Status == System.Threading.Tasks.TaskStatus.Running)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Is Running");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                viewModel.importSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            }
            else if (sqlImport.Status == TaskStatus.RanToCompletion)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                viewModel.importSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                memoryCache.RemoveKey("Global", "SQLImporter");
            }
            else
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Error while running");
                viewModel.importSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                LogEndMethod(processId:processId);                memoryCache.RemoveKey("Global", "SQLImporter");
                return Json(result, JsonRequestBehavior.AllowGet);

            }


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> DownloadSQLZip(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelCSVToSQL(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetControllerCSVToSQL();

            var request = new RequestInsertInto(viewModel.RefItem.ObjectItem.GUID)
            {
                ReadCSVFiles = true,
                MessageOutput = new OWMessageOutput(viewModel.IdInstance)
            };
            var controllerResult = Task.Run(() => controller.CSVImport(request));

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<ResultPreImport>>("Global", "CSVImport", () =>
           {
               return controllerResult;
           });


            result.ViewItems.Add(viewModel.createSQL);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Start Creation");
            viewModel.createSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SyncTFS(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTFSSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetControllerTFSSync();

            var request = new TFSConnectorModule.Models.SyncTFSRequest(viewModel.RefItem.ObjectItem.GUID);
            var controllerResult = Task.Run(() => controller.SyncTFS(request));

            var memoryCache = new InMemoryCache();
            var tfsSync = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<SyncTFSResult>>>("Global", "TFSSync", () =>
            {
                return controllerResult;
            });


            result.ViewItems.Add(viewModel.syncTFS);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Start Creation");
            viewModel.syncTFS.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SyncGemini(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGeminiSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetControllerGeminiSync();

            var securityController = GetControllerSecurity();

            var request = new GeminiConnectorModule.Models.SyncIssuesRequest(viewModel.RefItem.ObjectItem.GUID, securityController.CredentialItem.User.GUID, securityController.CredentialItem.Password.Name_Other)
            {
                MessageOutput = new OWMessageOutput(viewModel.IdInstance)
            };
            var controllerResult = Task.Run(() => controller.SyncIssues(request));

            var memoryCache = new InMemoryCache();
            var GeminiSync = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<GeminiConnectorModule.Models.SyncIssuesResult>>>("Global", "GeminiSync", () =>
            {
                return controllerResult;
            });


            result.ViewItems.Add(viewModel.syncGemini);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Start Creation");
            viewModel.syncGemini.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SyncGit(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGitSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetControllerGitSync();

            var request = new GitConnectorModule.Models.SyncGitProjectsRequest(viewModel.RefItem.ObjectItem.GUID);
            var controllerResult = Task.Run(() => controller.SyncGitProjects(request));

            var memoryCache = new InMemoryCache();
            var GitSync = memoryCache.GetOrSet<Task<OntoMsg_Module.Models.ResultItem<SyncGitProjectsResult>>>("Global", "GitSync", () =>
            {
                return controllerResult;
            });


            result.ViewItems.Add(viewModel.syncGit);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Start Creation");
            viewModel.syncGit.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SyncGoogleCalendar(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGoogleCalendarSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetControllerGoogleCalendarSync();

            var request = new GoogleCalendarConnector.Models.CalendarSyncRequest(viewModel.RefItem.ObjectItem.GUID);
            var controllerResult = Task.Run(() => controller.SyncCalendar(request));

            var memoryCache = new InMemoryCache();
            var googleCalendarSync = memoryCache.GetOrSet<Task<GoogleCalendarConnector.Models.CalendarSyncResult>>("Global", "GoogleCalendarSync", () =>
            {
                return controllerResult;
            });


            result.ViewItems.Add(viewModel.syncGoogleCalendar);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Start Creation");
            viewModel.syncGoogleCalendar.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SyncMail(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMailSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetControllerMailSync();

            var request = new MailModule.Models.ImportMailsRequest { IdConfiguration = viewModel.RefItem.ObjectItem.GUID, MasterPassword = "Test" };
            var controllerResult = Task.Run(() => controller.ImportImapMails(request));

            var memoryCache = new InMemoryCache();
            var MailSync = memoryCache.GetOrSet<Task<OntoMsg_Module.Models.ResultItem<MailModule.Models.ImportMailsResult>>>("Global", "MailSync", () =>
            {
                return controllerResult;
            });


            result.ViewItems.Add(viewModel.syncMail);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Start Creation");
            viewModel.syncMail.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SyncJira(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelJiraSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetControllerJiraSync();

            var request = new JiraConnectorModule.Models.SyncIssuesRequest(viewModel.RefItem.ObjectItem.GUID, "74f38566eda64c76b862e839b6a8d07d", "Test");
            var controllerResult = Task.Run(() => controller.SyncJiraIssues(request));

            var memoryCache = new InMemoryCache();
            var JiraSync = memoryCache.GetOrSet<Task<OntoMsg_Module.Models.ResultItem<JiraConnectorModule.Models.SyncIssuesResult>>>("Global", "JiraSync", () =>
            {
                return controllerResult;
            });


            result.ViewItems.Add(viewModel.syncJira);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Start Creation");
            viewModel.syncJira.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SyncActiveDirectory(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelActiveDirectorySync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetControllerActiveDirectorySync();

            var request = new ActiveDirectorySyncModule.Models.ComQueryRequest { IdPartners = new List<string> { viewModel.RefItem.ObjectItem.GUID } };
            var controllerResult = Task.Run(() => controller.UpdatePartners(request));

            var memoryCache = new InMemoryCache();
            var activeDirectorySync = memoryCache.GetOrSet<System.Threading.Tasks.Task<clsOntologyItem>>("Global", "ActiveDirectorySync", () =>
            {
                return controllerResult;
            });


            result.ViewItems.Add(viewModel.syncActiveDirectory);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Start Creation");
            viewModel.syncActiveDirectory.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ImportSQL(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelSQLImport(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetControllerSQLImporter();

            var request = new ImportSQLRequest((new CancellationTokenSource()).Token, viewModel.RefItem.ObjectItem.GUID, idUser: "6afcab29b5004467a22bb5604dd77106", password: "Test");
            var controllerResult = Task.Run(() => controller.ImportSQL(request));

            var memoryCache = new InMemoryCache();
            var tfsSync = memoryCache.GetOrSet<System.Threading.Tasks.Task<ImportSQLResult>>("Global", "SQLImporter", () =>
            {
                return controllerResult;
            });


            result.ViewItems.Add(viewModel.importSQL);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Start Creation");
            viewModel.importSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetScriptOutput(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPSScriptOutput(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.RefItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetControllerPSScriptOutput();
            var securityController = GetControllerSecurity();
            var request = new PsScriptOutputParserModule.Models.ScriptOutputRequest(viewModel.RefItem.ObjectItem.GUID, securityController.CredentialItem.User.GUID, securityController.CredentialItem.Password.Name_Other, (new CancellationTokenSource()).Token)
            {
                MessageOutput = new OWMessageOutput(viewModel.IdInstance)
            };
            var controllerResult = Task.Run(() => controller.PsScriptOutput(request));

            var memoryCache = new InMemoryCache();
            var tfsSync = memoryCache.GetOrSet<System.Threading.Tasks.Task<ScriptOutputResult>>("Global", "PSScriptOutput", () =>
            {
                return controllerResult;
            });


            result.ViewItems.Add(viewModel.getPSOutput);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "Start Creation");
            viewModel.getPSOutput.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DownloadResult()
        {
            var processId = LogStartMethod();
            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var controller = GetControllerCSVToSQL();
            var mediaStoreController = GetControllerMediaStore();
            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<ResultPreImport>>("Global", "CSVImport", () =>
            {
                return null;
            });

            if (csvImport == null || csvImport.Status != TaskStatus.RanToCompletion)
            {
                Response.StatusCode = 500;
                return Content("");
            }

            var resultZip = await controller.ZipScripts(csvImport.Result);

            if (resultZip.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                return Content("");
            }

            byte[] content;
            using (var stream = new StreamContent(resultZip.Result))
            {
                content = await stream.ReadAsByteArrayAsync();
            }

            var fileName = mediaStoreController.GetFileName(csvImport.Result.RootConfig.Name);


            Response.AddHeader("Content-Disposition", $"attachment; filename={fileName}.zip");

            memoryCache.RemoveKey("Global", "CSVImport");
            LogEndMethod(processId:processId);
            return File(content, "application/x-zip-compressed");
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceCSVToSQL(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelCSVToSQL(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var controller = GetControllerCSVToSQL();

            var referenceResult = await controller.GetClassObject(refItem.GUID);

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.createSQL);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            if (referenceResult.ResultState.GUID == controller.Globals.LState_Error.GUID ||
                referenceResult.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
            {
                viewModel.createSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.SetWindowTitle("");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), referenceResult.ResultState.Additional1);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RefItem = referenceResult.Result;
            viewModel.createSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.SetWindowTitle($"{viewModel.RefItem.ObjectItem.Name} ({viewModel.RefItem.ClassItem.Name})");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<ResultPreImport>>("Global", "CSVImport", () =>
            {
                return null;
            });

            if (csvImport != null)
            {
                if (csvImport.Status == System.Threading.Tasks.TaskStatus.Running ||
                    csvImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation)
                {
                    viewModel.createSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "is running");
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                }
                else if (csvImport.Status == TaskStatus.RanToCompletion)
                {
                    if (csvImport.Result.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), csvImport.Result.ResultState.Additional1);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                    }
                    else
                    {
                        viewModel.createSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

                    }
                }
            }
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceTFSSync(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTFSSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var controller = GetControllerTFSSync();

            var referenceResult = await controller.GetClassObject(refItem.GUID);

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.syncTFS);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            if (referenceResult.ResultState.GUID == controller.Globals.LState_Error.GUID ||
                referenceResult.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
            {
                viewModel.syncTFS.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.SetWindowTitle("");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), referenceResult.ResultState.Additional1);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RefItem = referenceResult.Result;
            viewModel.syncTFS.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.SetWindowTitle($"{viewModel.RefItem.ObjectItem.Name} ({viewModel.RefItem.ClassItem.Name})");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<SyncTFSResult>>>("Global", "TFSSync", () =>
            {
                return null;
            });

            if (csvImport != null)
            {
                if (csvImport.Status == System.Threading.Tasks.TaskStatus.Running ||
                    csvImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation)
                {
                    viewModel.syncTFS.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "is running");
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                }
                else if (csvImport.Status == TaskStatus.RanToCompletion)
                {
                    if (csvImport.Result.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), csvImport.Result.ResultState.Additional1);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                    }
                    else
                    {
                        viewModel.syncTFS.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

                    }
                }
            }
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceGeminiSync(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGeminiSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var controller = GetControllerGeminiSync();

            var referenceResult = await controller.GetClassObject(refItem.GUID);

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.syncGemini);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            if (referenceResult.ResultState.GUID == controller.Globals.LState_Error.GUID ||
                referenceResult.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
            {
                viewModel.syncGemini.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.SetWindowTitle("");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), referenceResult.ResultState.Additional1);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RefItem = referenceResult.Result;
            viewModel.syncGemini.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.SetWindowTitle($"{viewModel.RefItem.ObjectItem.Name} ({viewModel.RefItem.ClassItem.Name})");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<Task<OntoMsg_Module.Models.ResultItem<SyncIssuesResult>>>("Global", "GeminiSync", () =>
            {
                return null;
            });

            if (csvImport != null)
            {
                if (csvImport.Status == System.Threading.Tasks.TaskStatus.Running ||
                    csvImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation)
                {
                    viewModel.syncGemini.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "is running");
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                }
                else if (csvImport.Status == TaskStatus.RanToCompletion)
                {
                    if (csvImport.Result.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), csvImport.Result.ResultState.Additional1);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                    }
                    else
                    {
                        viewModel.syncGemini.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

                    }
                }
            }
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceGitSync(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGitSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var controller = GetControllerGitSync();

            var referenceResult = await controller.GetClassObject(refItem.GUID);

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.syncGit);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            if (referenceResult.ResultState.GUID == controller.Globals.LState_Error.GUID ||
                referenceResult.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
            {
                viewModel.syncGit.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.SetWindowTitle("");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), referenceResult.ResultState.Additional1);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RefItem = referenceResult.Result;
            viewModel.syncGit.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.SetWindowTitle($"{viewModel.RefItem.ObjectItem.Name} ({viewModel.RefItem.ClassItem.Name})");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<SyncGitProjectsResult>>>("Global", "GitSync", () =>
            {
                return null;
            });

            if (csvImport != null)
            {
                if (csvImport.Status == System.Threading.Tasks.TaskStatus.Running ||
                    csvImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation)
                {
                    viewModel.syncGit.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "is running");
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                }
                else if (csvImport.Status == TaskStatus.RanToCompletion)
                {
                    if (csvImport.Result.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), csvImport.Result.ResultState.Additional1);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                    }
                    else
                    {
                        viewModel.syncGit.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

                    }
                }
            }
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceGoogleCalendarSync(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelGoogleCalendarSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var controller = GetControllerGoogleCalendarSync();

            var referenceResult = await controller.GetClassObject(refItem.GUID);

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.syncGoogleCalendar);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            if (referenceResult.ResultState.GUID == controller.Globals.LState_Error.GUID ||
                referenceResult.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
            {
                viewModel.syncGoogleCalendar.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.SetWindowTitle("");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), referenceResult.ResultState.Additional1);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RefItem = referenceResult.Result;
            viewModel.syncGoogleCalendar.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.SetWindowTitle($"{viewModel.RefItem.ObjectItem.Name} ({viewModel.RefItem.ClassItem.Name})");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<GoogleCalendarConnector.Models.CalendarSyncResult>>("Global", "GoogleCalendarSync", () =>
            {
                return null;
            });

            if (csvImport != null)
            {
                if (csvImport.Status == System.Threading.Tasks.TaskStatus.Running ||
                    csvImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation)
                {
                    viewModel.syncGoogleCalendar.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "is running");
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                }
                else if (csvImport.Status == TaskStatus.RanToCompletion)
                {
                    if (csvImport.Result.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), csvImport.Result.ResultState.Additional1);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                    }
                    else
                    {
                        viewModel.syncGoogleCalendar.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

                    }
                }
            }
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceMailSync(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMailSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var controller = GetControllerMailSync();

            var referenceResult = await controller.GetClassObject(refItem.GUID);

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.syncMail);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            if (referenceResult.ResultState.GUID == controller.Globals.LState_Error.GUID ||
                referenceResult.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
            {
                viewModel.syncMail.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.SetWindowTitle("");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), referenceResult.ResultState.Additional1);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RefItem = referenceResult.Result;
            viewModel.syncMail.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.SetWindowTitle($"{viewModel.RefItem.ObjectItem.Name} ({viewModel.RefItem.ClassItem.Name})");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<MailModule.Models.ImportMailsResult>>>("Global", "MailSync", () =>
            {
                return null;
            });

            if (csvImport != null)
            {
                if (csvImport.Status == System.Threading.Tasks.TaskStatus.Running ||
                    csvImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation)
                {
                    viewModel.syncMail.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "is running");
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                }
                else if (csvImport.Status == TaskStatus.RanToCompletion)
                {
                    if (csvImport.Result.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), csvImport.Result.ResultState.Additional1);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                    }
                    else
                    {
                        viewModel.syncMail.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

                    }
                }
            }
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceJiraSync(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelJiraSync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var controller = GetControllerJiraSync();

            var referenceResult = await controller.GetClassObject(refItem.GUID);

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.syncJira);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            if (referenceResult.ResultState.GUID == controller.Globals.LState_Error.GUID ||
                referenceResult.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
            {
                viewModel.syncJira.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.SetWindowTitle("");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), referenceResult.ResultState.Additional1);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RefItem = referenceResult.Result;
            viewModel.syncJira.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.SetWindowTitle($"{viewModel.RefItem.ObjectItem.Name} ({viewModel.RefItem.ClassItem.Name})");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<JiraConnectorModule.Models.SyncIssuesResult>>>("Global", "JiraSync", () =>
            {
                return null;
            });

            if (csvImport != null)
            {
                if (csvImport.Status == System.Threading.Tasks.TaskStatus.Running ||
                    csvImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation)
                {
                    viewModel.syncJira.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "is running");
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                }
                else if (csvImport.Status == TaskStatus.RanToCompletion)
                {
                    if (csvImport.Result.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), csvImport.Result.ResultState.Additional1);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                    }
                    else
                    {
                        viewModel.syncJira.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

                    }
                }
            }
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceActiveDirectorySync(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelActiveDirectorySync(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var controller = GetControllerActiveDirectorySync();

            var referenceResult = await controller.GetClassObject(refItem.GUID);

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.syncActiveDirectory);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            if (referenceResult.ResultState.GUID == controller.Globals.LState_Error.GUID ||
                referenceResult.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
            {
                viewModel.syncActiveDirectory.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.SetWindowTitle("");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), referenceResult.ResultState.Additional1);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RefItem = referenceResult.Result;
            viewModel.syncActiveDirectory.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.SetWindowTitle($"{viewModel.RefItem.ObjectItem.Name} ({viewModel.RefItem.ClassItem.Name})");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<clsOntologyItem>>("Global", "ActiveDirectorySync", () =>
            {
                return null;
            });

            if (csvImport != null)
            {
                if (csvImport.Status == System.Threading.Tasks.TaskStatus.Running ||
                    csvImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation)
                {
                    viewModel.syncActiveDirectory.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "is running");
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                }
                else if (csvImport.Status == TaskStatus.RanToCompletion)
                {
                    if (csvImport.Result.GUID == controller.Globals.LState_Error.GUID)
                    {
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), csvImport.Result.Additional1);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                    }
                    else
                    {
                        viewModel.syncActiveDirectory.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

                    }
                }
            }
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceImportSQL(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelSQLImport(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var controller = GetControllerSQLImporter();

            var referenceResult = await controller.GetClassObjectReference(refItem);

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.importSQL);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            if (referenceResult.ResultState.GUID == controller.Globals.LState_Error.GUID ||
                referenceResult.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
            {
                viewModel.importSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.SetWindowTitle("");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), referenceResult.ResultState.Additional1);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RefItem = referenceResult.Result;
            viewModel.importSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.SetWindowTitle($"{viewModel.RefItem.ObjectItem.Name} ({viewModel.RefItem.ClassItem.Name})");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<ImportSQLResult>>>("Global", "SQLImporter", () =>
            {
                return null;
            });

            if (csvImport != null)
            {
                if (csvImport.Status == System.Threading.Tasks.TaskStatus.Running ||
                    csvImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation)
                {
                    viewModel.importSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "is running");
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                }
                else if (csvImport.Status == TaskStatus.RanToCompletion)
                {
                    if (csvImport.Result.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), csvImport.Result.ResultState.Additional1);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                    }
                    else
                    {
                        viewModel.importSQL.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

                    }
                }
            }
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }



        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferencePSScriptOutput(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPSScriptOutput(idInstance);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var controller = GetControllerPSScriptOutput();

            var referenceResult = await controller.GetClassObjectReference(refItem);

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.getPSOutput);
            result.ViewItems.Add(viewModel.outputResult);
            result.ViewItems.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            if (referenceResult.ResultState.GUID == controller.Globals.LState_Error.GUID ||
                referenceResult.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
            {
                viewModel.getPSOutput.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.SetWindowTitle("");
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), referenceResult.ResultState.Additional1);
                viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RefItem = referenceResult.Result;
            viewModel.getPSOutput.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.SetWindowTitle($"{viewModel.RefItem.ObjectItem.Name} ({viewModel.RefItem.ClassItem.Name})");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

            var memoryCache = new InMemoryCache();
            var csvImport = memoryCache.GetOrSet<System.Threading.Tasks.Task<OntoMsg_Module.Models.ResultItem<SyncTFSResult>>>("Global", "TFSSync", () =>
            {
                return null;
            });

            if (csvImport != null)
            {
                if (csvImport.Status == System.Threading.Tasks.TaskStatus.Running ||
                    csvImport.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation)
                {
                    viewModel.getPSOutput.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "is running");
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });
                    viewModel.outputResult.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
                }
                else if (csvImport.Status == TaskStatus.RanToCompletion)
                {
                    if (csvImport.Result.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), csvImport.Result.ResultState.Additional1);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Red" });
                    }
                    else
                    {
                        viewModel.getPSOutput.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
                        viewModel.outputResult.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "Color", Value = "Green" });

                    }
                }
            }
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Index()
        {
            var viewModel = GetViewModelToolBox(Guid.NewGuid().ToString());
            return View(viewModel);
        }
        [Authorize]
        public ActionResult DiffText()
        {
            var viewModel = GetViewModelDiffText(Guid.NewGuid().ToString());
            return View(viewModel);
        }


        [Authorize]
        public ActionResult TFSSync()
        {
            var viewModel = GetViewModelTFSSync(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        [Authorize]
        public ActionResult GeminiSync()
        {
            var viewModel = GetViewModelGeminiSync(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        [Authorize]
        public ActionResult GitSync()
        {
            var viewModel = GetViewModelGitSync(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        [Authorize]
        public ActionResult GoogleCalendarSync()
        {
            var viewModel = GetViewModelGoogleCalendarSync(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        [Authorize]
        public ActionResult MailSync()
        {
            var viewModel = GetViewModelMailSync(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        [Authorize]
        public ActionResult JiraIssueSync()
        {
            var viewModel = GetViewModelJiraSync(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        [Authorize]
        public ActionResult SQLImporter()
        {
            var viewModel = GetViewModelSQLImport(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        [Authorize]
        public ActionResult CSVToSQL()
        {
            var viewModel = GetViewModelCSVToSQL(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        [Authorize]
        public ActionResult PSScriptOutput()
        {
            var viewModel = GetViewModelPSScriptOutput(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ActiveDirectorySync()
        {
            var viewModel = GetViewModelActiveDirectorySync(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        [Authorize]
        public ActionResult JsonEditor()
        {
            var viewModel = GetViewModelJsonEditor(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        [Authorize]
        public ActionResult JsonEditorInit(string idParentInstance)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idParentInstance))
            {
                idParentInstance = Guid.NewGuid().ToString();
            }
            var viewModel = GetViewModelJsonEditor(idParentInstance);
            var resultAction = new ResultSimpleView<JsonEditorViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult BatchGridInit()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelBatchGrid(Guid.NewGuid().ToString());
            var resultAction = new ResultSimpleView<BatchGridViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ModuleExecutionPropertiesInit(string idParentInstance)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idParentInstance))
            {
                idParentInstance = Guid.NewGuid().ToString();
            }
            var viewModel = GetViewModelModuleExecutionProperties(idParentInstance);
            var resultAction = new ResultSimpleView<ModuleExecutionPropertiesViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ModuleExecutionLogInit(string idParentInstance)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idParentInstance))
            {
                idParentInstance = Guid.NewGuid().ToString();
            }
            var viewModel = GetViewModelModuleExecutionLog(idParentInstance);
            var resultAction = new ResultSimpleView<ModuleExecutionLogViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

    }

    public class ToolForToolbar
    {
        public string FontClass { get; set; }
        public string Caption { get; set; }
        public string Url { get; set; }
    }


}