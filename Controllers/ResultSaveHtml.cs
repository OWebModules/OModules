﻿using OntologyClasses.BaseClasses;

namespace OModules.Controllers
{
    public class ResultSaveHtml
    {
        public bool IsSaved { get; set; }
        public clsOntologyItem HtmlItem { get; set; }
    }
}