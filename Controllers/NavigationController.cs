﻿using Microsoft.AspNet.Identity;
using ModuleManagementModule.Models;
using OModules.Models;
using OModules.Primitives;
using OModules.Services;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Connectors;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class NavigationController : AppControllerBase
    {
        

        private ModuleManagementModule.ModuleManagementController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            
            var result = new ModuleManagementModule.ModuleManagementController(globals);

            LogEndMethod(processId:processId);
            return result;

            
        }

        private ViewListViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ViewListViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ViewListViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigViewList
                    {

                        ActionConnected = Url.Action(nameof(Connected)),
                        ActionDisconnected = Url.Action(nameof(Disconnected)),
                        ActionSetViewReady = Url.Action(nameof(SetViewReady)),
                        ActionGetGridConfig = Url.Action(nameof(GetGridConfig)),
                        ActionGetUrl = Url.Action(nameof(GetUrl))
                    }
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        // GET: Navigation
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult ViewList()
        {
            var processId = LogStartMethod();
            var model = GetViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(model);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetGridConfig()
        {
            var processId = LogStartMethod();
            var moduleMgmtController = GetController();

            var gridConfig = await moduleMgmtController.GetViewTreeListConfig(Url.Action(nameof(GetData)));

            LogEndMethod(processId:processId);
            return Json(gridConfig.TreeListConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetData(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var moduleMgmtController = GetController();

            var resultTask = await moduleMgmtController.GetViewItems(viewModel.Views.ViewList);
            //viewModel.ControllerViews = resultTask.ControllerViews.ControllerViewRel;

            LogEndMethod(processId:processId);
            return Json(resultTask.ViewItems, JsonRequestBehavior.AllowGet);


        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetUrl(string id, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var moduleMgmtController = GetController();
            var viewConfig = HttpRuntime.Cache.Get(CacheKeys.ViewConfig);
            var actionViewTask = await moduleMgmtController.GetActionView(viewModel.Views, id);

            var result = new ResultGetUrl
            {
                IsSuccessful = true,

            };
            if (!string.IsNullOrEmpty(actionViewTask.Action) && !string.IsNullOrEmpty(actionViewTask.View))
            {
                result.Url = Url.Action(actionViewTask.View, actionViewTask.Action);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var viewItemValue = result.ViewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }
    }

    public class ResultGetUrl
    {
        public bool IsSuccessful { get; set; }
        public string Url { get; set; }
    }
}