﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OModules.Models;
using MediaStore_Module;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoMsg_Module.Attributes;
using OntoWebCore.Controllers;
using OntoWebCore.Models;
using OModules.Services;
using Microsoft.AspNet.Identity;

namespace OModules.Controllers
{
    public class FileUploadController : AppControllerBase
    {
        

        private MediaStore_Module.FileUploadController GetController(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            
            var result = new MediaStore_Module.FileUploadController(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private FileUploadViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<FileUploadViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new FileUploadViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionUpload = Url.Action(nameof(Submit)),
                    ActionRemove = Url.Action(nameof(Remove)),
                    ActionConnected = Url.Action(nameof(Connected)),
                    ActionDisconnected = Url.Action(nameof(Disconnected)),
                    ActionValidateReference = Url.Action(nameof(ValidateReference)),
                    IdSession = HttpContext.Session.SessionID
                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        // GET: FileUpload
        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> FileUpload()
        {
            var processId = LogStartMethod();            var viewModel = GetViewModel(Guid.NewGuid().ToString());

            var controller = GetController(nameof(MediaStore_Module.FileUploadController) + HttpContext.User.Identity.Name);

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);            return View(viewModel);
        }

        
       
        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReference(string idObject, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultValidateReference
            {
                IsOk = true
            };
            if (string.IsNullOrEmpty(idObject))
            {
                result.ErrorMessage = "No Objectid provided!";
                LogError(result.ErrorMessage, processId: processId);
                result.IsOk = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            
            var viewModel = GetViewModel(idInstance);
            var controller = GetController(nameof(MediaStore_Module.FileUploadController) + HttpContext.User.Identity.Name);

            var oItemResult = await controller.GetReference(idObject, controller.Globals.Type_Object);
            if (oItemResult.Result.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsOk = false;
                result.ErrorMessage = "Error While getting the reference!";
                LogError(result.ErrorMessage, processId: processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            viewModel.SetOItemReference(oItemResult.RefItem, oItemResult.RefParentItem, oItemResult.RelationType, oItemResult.Direction);

            result.ReferenceName = viewModel.nameReference;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem)
        {
            var processId = LogStartMethod();            var viewModel = GetViewModel(Guid.NewGuid().ToString());

            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);
            
            viewItemList.Add(viewModel.listen);
            viewItemList.Add(viewModel.fileItem);

            viewModel.listen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.fileItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewItemList.Add(viewModel.listen);
            
            viewModel.listen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.fileItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> Submit(IEnumerable<HttpPostedFileBase> files, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<List<clsOntologyItem>>
            {
                IsSuccessful = true,
                Result = new List<clsOntologyItem>()
            };
            if (files != null)
            {
                var viewModel = GetViewModel(idInstance);
                var controller = GetController(nameof(MediaStore_Module.FileUploadController) + HttpContext.User.Identity.Name);

                var serviceHostResult = await controller.GetMediaServiceHost();
                if (serviceHostResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    LogError("Error while getting the fileitem", processId: processId);
                    Response.StatusCode = 500;
                    result.IsSuccessful = false;
                    result.Message = serviceHostResult.ResultState.Additional1;
                    LogEndMethod(processId:processId);                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                foreach (var fileItem in files)
                {
                    var fileOItemTask = await controller.CreateFile(fileItem.FileName);
                    

                    if (fileOItemTask.Result.GUID == controller.Globals.LState_Error.GUID)
                    {
                        LogError("Error while getting the fileitem", processId: processId);
                        Response.StatusCode = 500;
                        break;
                    }
                    var mediaItem = GetFileInfo(fileItem, fileOItemTask.FileItem);

                    // The files are not actually saved in this demo
                    fileItem.SaveAs(mediaItem.FilePath);

                    fileOItemTask.FileItem.Additional1 = mediaItem.FilePath;
                    
                    
                    var resultSaveTask = await controller.SaveFileItem(fileOItemTask.FileItem, viewModel.oItemReference, serviceHostResult.Result, true);

                    if (resultSaveTask.GUID == controller.Globals.LState_Error.GUID)
                    {
                        LogError("Error while saving the Fileitem", processId: processId);
                        Response.StatusCode = 500;
                        break;
                    }
                    result.Result.Add(fileOItemTask.FileItem);
                }
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Remove(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"

            var processId = LogStartMethod();
            if (fileNames != null)
            {

                var mediaItems = (List<MediaItem>) TempData["UploadedFiles"];
                var mediaItemsToDelete = (from mediaItem in mediaItems
                    join fileItem in fileNames on mediaItem.FilePath equals fileItem
                    select mediaItem);

                foreach (var mediaItem in mediaItemsToDelete)
                {

                    // TODO: Verify user permissions

                    if (System.IO.File.Exists(mediaItem.FilePath))
                    {
                        // The files are not actually removed in this demo
                        System.IO.File.Delete(mediaItem.FilePath);
                    }
                }
            }

            LogEndMethod(processId:processId);
            // Return an empty string to signify success
            return Content("");
        }

        private MediaItem GetFileInfo(HttpPostedFileBase file, clsOntologyItem oFileItem)
        {
            var processId = LogStartMethod();
            var mediaItem = new MediaItem(file, Server.MapPath("~/App_Data/Upload"), oFileItem);

            LogEndMethod(processId:processId);
            return mediaItem;
        }
    }

    
}