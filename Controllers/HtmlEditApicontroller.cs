﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.Requests;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TypedTaggingModule.Models;

namespace OModules.Controllers
{
    public class HtmlEditApicontroller : ApiControllerBase
    {
        

        private HtmlEditorModule.HtmlEditorConnector GetControllerHtmlEdit(string idSession)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(idSession, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new HtmlEditorModule.HtmlEditorConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private SecurityModule.SecurityController GetControllerSecurity(string idSession)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(idSession, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            LogEndMethod(processId:processId);
            return new SecurityModule.SecurityController(globals);

        }

        [Route("HtmlEditApi/GetHtmlContent")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetHtmlContent(GetHtmlContentRequest request)
        {
            var processId = LogStartMethod();
            var result = new ResultApi<clsObjectAtt>()
            {
                IsSuccessful = true
            };

            if (string.IsNullOrEmpty(request.idReference))
            {
                result.IsSuccessful = false;
                result.Message = "You have to provide a reference-id!";
                return Json(result);
            }

            var controller = GetControllerHtmlEdit(request.idSession);
            var resultTask = await controller.GetHtmlDocument(request.idReference);

            if (resultTask.Result.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Error while getting HTML content";
                return Json(result);
            }

            var htmlDocContent = new clsObjectAtt
            {
                ID_Attribute = resultTask.OAItemHtmlDocument.ID_Attribute,
                ID_Object = resultTask.OAItemHtmlDocument.ID_Object,
                ID_Class = resultTask.OAItemHtmlDocument.ID_Class,
                Val_String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(resultTask.OAItemHtmlDocument.Val_String))
            };
            result.Result = htmlDocContent;

            LogEndMethod(processId:processId);
            return Json(result);
        }

        [Route("HtmlEditApi/GetTags")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetTags(GetHtmlContentRequest request)
        {
            var processId = LogStartMethod();

            var controller = GetControllerHtmlEdit(request.idSession);

            var result = new ResultApi<List<Reference>>
            {
                IsSuccessful = true,
                Result = new List<Reference>()
            };

            var resultTask = await controller.GetHtmlDocument(request.idReference);

            if (resultTask.Result.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Error while getting HTML content";
                return Json(result);
            }

            var resultGetTags = await controller.GetTags(resultTask.OItemHtmlDocument.GUID, Url.Link("Default",new { controller = nameof(HtmlViewerController).Replace("Controller", ""), action = nameof(HtmlViewerController.DocViewer) } ), Url.Link("Default", new { controller = nameof(ModuleMgmtController).Replace("Controller", ""), action = nameof(ModuleMgmtController.ModuleStarter) } ));

            if (resultGetTags.Result.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Error while getting Tags";
                return Json(result);
            }

            result.Result = resultGetTags.References;

            LogEndMethod(processId:processId);
            return Json(result);
        }
    }
}