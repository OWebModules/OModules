﻿using MediaStore_Module.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class FileDownloadController : AppControllerBase
    {
        

        private MediaStore_Module.FileDownloadController GetController(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new MediaStore_Module.FileDownloadController(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private FileDownloadViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<FileDownloadViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new FileDownloadViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(Connected)),
                    ActionDisconnected = Url.Action(nameof(Disconnected)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReady)),
                    ActionValidateReference = Url.Action(nameof(ValidateReference)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfig)),
                    ActionGetFiles = Url.Action(nameof(GetFiles)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewItemList.Add(viewModel.isListen);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.applyItem);
            viewItemList.Add(viewModel.deleteItem);
            viewItemList.Add(viewModel.downloadFiles);
            viewItemList.Add(viewModel.grid);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.applyItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.deleteItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.downloadFiles.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.grid.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfig()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(FileDownloadItem), Url.Action(nameof(GetFileItems)), null, null, null, false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetFileItems(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            var controller = GetController(nameof(MediaStore_Module.FileDownloadController) + HttpContext.User.Identity.Name);
            if (viewModel.OItemWithParent == null)
            {
                var resultEmptyList = await controller.GetDownloadItems();


                LogError("No Reference", processId: processId);
                LogEndMethod(processId:processId);                
                return Json(resultEmptyList.Result, JsonRequestBehavior.AllowGet);
            }

            var resultTask = await controller.GetDownloadItems(viewModel.OItemWithParent);

            if (resultTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error While Getting Download-Items", processId: processId);
                Response.StatusCode = 500;
                var resultEmptyList = await controller.GetDownloadItems();

                LogEndMethod(processId:processId);                
                return Json(resultEmptyList.Result, JsonRequestBehavior.AllowGet);
            }

            var resultItem = new KendoGridDataResult<FileDownloadItem>(resultTask.Result);
            var result = Json(resultItem, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetFiles(string[] idFiles, string refName)
        {
            var processId = LogStartMethod();
            var controller = GetController(nameof(MediaStore_Module.FileDownloadController) + HttpContext.User.Identity.Name);

            var request = new GetDownloadFilesRequest(Server.MapPath("~/Resources/UserGroupRessources"), idFiles.Select(fileId => new clsOntologyItem { GUID = fileId }).ToList(), refName);
            var result = await controller.GetDownloadFiles(request);
            if (result.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                return Content("");
            }

            byte[] content;
            using (var stream = new StreamContent(result.Result))
            {
                content = await stream.ReadAsByteArrayAsync();
            }
                
            Response.AddHeader("Content-Disposition", $"attachment; filename={result.ResultState.Additional1}");

            LogEndMethod(processId:processId);
            return File(content, "application/x-zip-compressed");
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetFile(string id)
        {
            var processId = LogStartMethod();
            var controller = GetController(nameof(MediaStore_Module.FileDownloadController) + HttpContext.User.Identity.Name);

            var name = Guid.NewGuid().ToString() + ".zip";

            LogEndMethod(processId:processId);
            return await GetFiles((new List<string> { id }).ToArray(), name);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetFileUrl(string idFile)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<string>
            {
                IsSuccessful = true
            };

            if (string.IsNullOrEmpty(idFile))
            {
                result.IsSuccessful = false;
                LogError("No Fileid provided", processId: processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetController(nameof(MediaStore_Module.FileDownloadController) + HttpContext.User.Identity.Name);

            if (!controller.Globals.is_GUID(idFile))
            {
                result.IsSuccessful = false;
                LogError("Fileid is no GUID", processId: processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var fileUrl = await controller.GetFileUrl(Server.MapPath("~/Resources/UserGroupRessources"), "/Resources/UserGroupRessources", idFile);

            if (fileUrl.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                LogError("Error While getting the Url of File", processId: processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = fileUrl.Result;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var controller = GetController(nameof(MediaStore_Module.FileDownloadController) + HttpContext.User.Identity.Name);

            var refItemTask = await controller.GetOItemWithParent(refItem.GUID, controller.Globals.Type_Object);

            var resultViewItems = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            viewModel.OItemWithParent = refItemTask.Result;

            if (viewModel.OItemWithParent == null || viewModel.OItemWithParent.OItem == null)
            {
                resultViewItems.IsSuccessful = false;
                LogError("No Object or Class provided", processId: processId);
                return Json(resultViewItems, JsonRequestBehavior.AllowGet);
            }


            var windowTitle = $"{viewModel.OItemWithParent.OItem.Name}";

            if (viewModel.OItemWithParent.OItemParent != null)
            {
                LogError("No Class provided", processId: processId);
                windowTitle += $" ({viewModel.OItemWithParent.OItemParent.Name})";
            }
            viewModel.SetWindowTitle(windowTitle);
            viewModel.SetWindowTitle(viewModel.OItemWithParent.OItem.Name);
            resultViewItems.ViewItems.Add(viewModel.windowTitle);

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        // GET: FileDownload
        public ActionResult Index()
        {
            var processId = LogStartMethod();
            LogEndMethod(processId:processId);            return View();
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> FileDownload()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString());

            var controller = GetController(nameof(MediaStore_Module.FileDownloadController) + HttpContext.User.Identity.Name);

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);
            return View(viewModel);
        }
    }
}