﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OModules.Models;
using Microsoft.AspNet.Identity;
using OModules.Services;
using System.Threading.Tasks;

namespace OModules.Controllers
{
    public class OntologyModulesController : AppControllerBase
    {
        private LocalizationModule.LocalizationController GetLocalizationController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new LocalizationModule.LocalizationController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        [Authorize]
        public async Task<ActionResult> OModules()
        {
            var processId = LogStartMethod();
            var viewModel = new OntologyModulesViewModel(Session, Guid.NewGuid().ToString(), User.Identity.GetUserId())
            {

                urlViews = Url.Action("ViewList", "Navigation", null, Request.Url.Scheme),
                urlClassObject = Url.Action("ClassObject", "OntologyModules", null, Request.Url.Scheme),
                urlObjectList = Url.Action("ObjectList", "OItemList", null, Request.Url.Scheme),
                urlSearch = Url.Action("Search", "OItemSearch", null, Request.Url.Scheme),
                urlModuleStarter = Url.Action("ModuleStarter", "ModuleMgmt", null, Request.Url.Scheme),
                ActionRelationEditor = Url.Action(nameof(ObjectEditController.RelationEditor), nameof(ObjectEditController).Replace("Controller", "")),
                ActionObjectTree = Url.Action(nameof(ObjectTreeController.ObjectTree), nameof(ObjectTreeController).Replace("Controller", "")),
                ActionInitClassTree = Url.Action(nameof(ClassTreeController.ClassTreeInit), nameof(ClassTreeController).Replace("Controller", "")),
                ActionSetSelectedItem = Url.Action(nameof(OItemListController.SetCurrentItem), nameof(OItemListController).Replace("Controller", "")),
                ActionSetOrderId = Url.Action(nameof(OItemListController.SetOrderId), nameof(OItemListController).Replace("Controller", "")),
                ActionGetGridConfig = Url.Action(nameof(OItemListController.GetGridConfigObjects), nameof(OItemListController).Replace("Controller", "")),
                ActionGetGridConfigViewList = Url.Action(nameof(NavigationController.GetGridConfig), nameof(NavigationController).Replace("Controller","")),
                ActionGetUrlViewList = Url.Action(nameof(NavigationController.GetUrl), nameof(NavigationController).Replace("Controller","")),
                ActionModuleStarterInit = Url.Action(nameof(ModuleMgmtController.ModuleStarterInit), nameof(ModuleMgmtController).Replace("Controller", "")),
                ActionGetSearchRequestItem = Url.Action(nameof(OItemSearchController.GetSearchRequestItem), nameof(OItemSearchController).Replace("Controller","")),
                ActionSearchClassChanged = Url.Action(nameof(OItemSearchController.SearchClassChanged), nameof(OItemSearchController).Replace("Controller","")),
                ActionSearchGetGridConfig = Url.Action(nameof(OItemSearchController.GetGridConfig), nameof(OItemSearchController).Replace("Controller", "")),
                ActionAddSearchItemArea = Url.Action(nameof(OItemSearchController.AddSearchItemArea), nameof(OItemSearchController).Replace("Controller", "")),
                ActionRemoveSearchItemArea = Url.Action(nameof(OItemSearchController.RemoveSearchItemArea), nameof(OItemSearchController).Replace("Controller", "")),
                ActionSearchAttributeTypeChanged = Url.Action(nameof(OItemSearchController.SearchAttributeTypeChanged), nameof(OItemSearchController).Replace("Controller", "")),
                ActionSearchItemTextChanged = Url.Action(nameof(OItemSearchController.SearchItemTextChanged), nameof(OItemSearchController).Replace("Controller", "")),
                ActionSearchRelationTypeChanged = Url.Action(nameof(OItemSearchController.SearchRelationTypeChanged), nameof(OItemSearchController).Replace("Controller", "")),
                ActionInitSearchItems = Url.Action(nameof(OItemSearchController.InitSearchItems), nameof(OItemSearchController).Replace("Controller", "")),
                ActionObjectListLinit = Url.Action(nameof(OItemListController.ObjectListInit), nameof(OItemListController).Replace("Controller", "")),
                ActionInitPDFSearch = Url.Action(nameof(MediaViewerController.PDFSearchInit), nameof(MediaViewerController).Replace("Controller", "")),
                ActionGetGridConfigHtmlSearch = Url.Action(nameof(HtmlEditController.GetGridConfigHtmlSearch), nameof(HtmlEditController).Replace("Controller", ""))
            };

            var controller = GetLocalizationController();

            var request = new LocalizationModule.Models.GetLocalizedGuiEntriesRequest(viewModel.View.IdView, System.Globalization.CultureInfo.CurrentUICulture.Name);
            var controllerResult = await controller.GetLicalizedGuiEntries(request);
            
            if (controllerResult.ResultState.GUID == controller.Globals.LState_Success.GUID)
            {
                var translateResult = await controller.TranslateViewItems(viewModel, controllerResult.Result.GuiEntries);
                if (translateResult.GUID == controller.Globals.LState_Success.GUID)
                {
                    viewModel.GuiEntries = controllerResult.Result.GuiEntries;
                }
            }

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId: processId);
            LogEndMethod(processId: processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ClassObject()
        {
            var processId = LogStartMethod();
            var viewModel = new OntologyModulesViewModel(Session, Guid.NewGuid().ToString(), User.Identity.GetUserId())
            {
                urlClassTree = Url.Action("ClassTree", "ClassTree", null, Request.Url.Scheme),
                urlObjectList = Url.Action("ObjectList", "OItemList", null, Request.Url.Scheme),
                urlSearch = Url.Action("Search", "OItemSearch", null, Request.Url.Scheme),
                urlModuleStarter = Url.Action("ModuleStarter", "ModuleMgmt", null, Request.Url.Scheme),
                ActionInitClassTree = Url.Action(nameof(ClassTreeController.ClassTreeInit), nameof(ClassTreeController).Replace("Controller", ""))
            };

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        private OntologyItemsModule.OItemListController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.OItemListController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        [Authorize]
        public ActionResult Ping()
        {
            var processId = LogStartMethod();
            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}