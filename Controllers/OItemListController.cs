﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.MessageOutput;
using OModules.Models.Requests;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntologyItemsModule.Converter;
using OntologyItemsModule.Models;
using OntologyItemsModule.Services;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class OItemListController : AppControllerBase
    {
        

        private OItemListViewModel GetViewModel(string key, string idView = null)
        {
            var processId = LogStartMethod();            key = $"{idView ?? ""}{key}";
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<OItemListViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new OItemListViewModel(Session, idView, key, User.Identity.GetUserId())
                {
                    IdSession = HttpContext.Session.SessionID,
                    ActionConnected = Url.Action(nameof(Connected)),
                    ActionDisconnected = Url.Action(nameof(Disconnected)),
                    ActionNewItem = Url.Action(nameof(EditOItemController.EditOItem), nameof(EditOItemController).Replace("Controller", "")),
                    ActionSetSelectedItem = Url.Action(nameof(SetCurrentItem)),
                    ActionSetOrderId = Url.Action(nameof(SetOrderId)),
                    ActionCheckExistance = Url.Action(nameof(CheckExistanceItem)),
                    ActionSaveNewItems = Url.Action(nameof(SaveNewItems)),
                    ActionAddToClipboard = Url.Action(nameof(ClipboardController.AddClipboardItems), nameof(ClipboardController).Replace("Controller", "")),
                    ActionAddObjectsInit = Url.Action(nameof(EditOItemController.AddObjectsInit), nameof(EditOItemController).Replace("Controller", "")),
                    ActionRelationEditor = Url.Action(nameof(ObjectEditController.RelationEditor), nameof(ObjectEditController).Replace("Controller", "")),
                    ActionModuleStarter = Url.Action(nameof(ModuleMgmtController.ModuleStarter), nameof(ModuleMgmtController).Replace("Controller", "")),
                    ActionObjectAttributeListInit = Url.Action(nameof(ObjectAttributeListInit)),
                    ActionAttributeTypeListInit = Url.Action(nameof(AttributeTypeListInit)),
                    ActionEditAttributeInit = Url.Action(nameof(EditOItemController.EditAttributeInit), nameof(EditOItemController).Replace("Controller", "")),
                    ActionDuplicateObject = Url.Action(nameof(DuplicateObject)),
                    ActionGetTransformedNames = idView == WebViews.Config.LocalData.Object_Object_List.GUID ? Url.Action(nameof(GetTransformedNamesOfObjects)) : null,
                    ActionHasRelations = Url.Action(nameof(HasRelations))
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private OntologyItemsModule.OItemListController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.OItemListController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private OntologyItemsModule.ObjectEditController GetObjectEditController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.ObjectEditController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private OntologyItemsModule.OntologyConnector GetOntologyController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.OntologyConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        

        // GET: OItemList
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult AttributeTypeList()
        {
            var processId = LogStartMethod();
            var dataTypes = new clsDataTypes();
            var viewModel = GetViewModel(Guid.NewGuid().ToString(), OModules.WebViews.Config.LocalData.Object_AttributeTypeList.GUID);
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigAttributeTypes));
            viewModel.ActionHasRelations = Url.Action(nameof(HasRelationsAttributeTypes));
            viewModel.ChannelSelectedItem = Channels.LocalData.Object_SelectedAttributeType.GUID;
            viewModel.dataTypeBool = dataTypes.DType_Bool.GUID;
            viewModel.dataTypeDateTime = dataTypes.DType_DateTime.GUID;
            viewModel.dataTypeLong = dataTypes.DType_Int.GUID;
            viewModel.dataTypeReal = dataTypes.DType_Real.GUID;
            viewModel.dataTypeString = dataTypes.DType_String.GUID;
            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult AttributeTypeListInit(List<string> selectorPath, string idInstance)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idInstance))
            {
                idInstance = Guid.NewGuid().ToString();
            }
            var dataTypes = new clsDataTypes();
            var viewModel = GetViewModel(idInstance, OModules.WebViews.Config.LocalData.Object_AttributeTypeList.GUID);
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigAttributeTypes));
            viewModel.ActionHasRelations = Url.Action(nameof(HasRelationsAttributeTypes));
            viewModel.ChannelSelectedItem = Channels.LocalData.Object_SelectedAttributeType.GUID;
            viewModel.dataTypeBool = dataTypes.DType_Bool.GUID;
            viewModel.dataTypeDateTime = dataTypes.DType_DateTime.GUID;
            viewModel.dataTypeLong = dataTypes.DType_Int.GUID;
            viewModel.dataTypeReal = dataTypes.DType_Real.GUID;
            viewModel.dataTypeString = dataTypes.DType_String.GUID;
            viewModel.SelectorPath = selectorPath;

            var resultAction = new ResultSimpleView<OItemListViewModel>
            {
                IsSuccessful = true,
                Result = viewModel,
                SelectorPath = selectorPath
            };

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult RelationTypeList()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString(), WebViews.Config.LocalData.Object_RelationType_List.GUID);
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigRelationTypes));

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ObjectList()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString(), WebViews.Config.LocalData.Object_Object_List.GUID);
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedClassNode.GUID;
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjects));

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ObjectListInit()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString(), WebViews.Config.LocalData.Object_Object_List.GUID);
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedClassNode.GUID;
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjects));

            var resultAction = new ResultSimpleView<OItemListViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult OntologyExport()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString(), "f41336728e3a4c8382049d7b4edf7b80");
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedClassNode.GUID;
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjects));

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ObjectRelationLeftRightList()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString(), WebViews.Config.LocalData.Object_ObjectRel_LeftRight_List.GUID);
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedRelationNode.GUID;
            viewModel.ActionGetParent = Url.Action(nameof(GetObjectItem));
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjectRelationsLeftRight));

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ComponentObjectRelationAttributeList(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance, "6009ef41b9be459b9c2b62e89e5c155d");
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedRelationNode.GUID;
            viewModel.ActionGetParent = Url.Action(nameof(GetObjectItem));
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjectAttributes));

            LogEndMethod(processId:processId);
            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ComponentObjectRelationLeftRightList(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance, "e7542106ac9a41bc95b826d826cec5b0");
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedRelationNode.GUID;
            viewModel.ActionGetParent = Url.Action(nameof(GetObjectItem));
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjectRelationsLeftRight));

            LogEndMethod(processId:processId);
            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            return Json(result,JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public ActionResult ObjectRelationLeftRightOtherList()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString(), WebViews.Config.LocalData.Object_ObjectRel_LeftRight_Other_List.GUID);
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedRelationNode.GUID;
            viewModel.ActionGetParent = Url.Action(nameof(GetObjectItem));
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjectRelationsLeftRightOther));

            LogEndMethod(processId:processId);
            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            return View(result);
        }

        [Authorize]
        public ActionResult ComponentObjectRelationLeftRightOtherList(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel($"LeftRightOther{idInstance}", "cb405518af064996812c45f51d13b6e2");
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedRelationNode.GUID;
            viewModel.ActionGetParent = Url.Action(nameof(GetObjectItem));
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjectRelationsLeftRightOther));

            LogEndMethod(processId:processId);
            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ObjectRelationRightLeftList()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString(), WebViews.Config.LocalData.Object_ObjectRel_RightLeft_List.GUID );
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedRelationNode.GUID;
            viewModel.ActionGetParent = Url.Action(nameof(GetObjectItem));
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjectRelationsRightLeft));

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ComponentObjectRelationRightLeftList(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel($"RightLeft{idInstance}", "3579ff1dc8b047939531c0299f982394");
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedRelationNode.GUID;
            viewModel.ActionGetParent = Url.Action(nameof(GetObjectItem));
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjectRelationsRightLeft));

            LogEndMethod(processId:processId);
            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ComponentObjectRelationOmniList(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel($"RightLeft{idInstance}", "44dd044245574fb2bc82aed00e9fba7a");
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedRelationNode.GUID;
            viewModel.ActionGetParent = Url.Action(nameof(GetObjectItem));
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjectRelationsOmni));

            LogEndMethod(processId:processId);
            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ObjectRelationRightLeftOtherList()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString(), WebViews.Config.LocalData.Object_ObjectRel_RightLeft_Other_List.GUID);
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedRelationNode.GUID;
            viewModel.ActionGetParent = Url.Action(nameof(GetObjectItem));
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjectRelationsRightLeftOther));

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ComponentObjectRelationRightLeftOtherList(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel($"RightLeftOther{idInstance}", "8f1da317a4f34b608cee0eb750f0a773");
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedRelationNode.GUID;
            viewModel.ActionGetParent = Url.Action(nameof(GetObjectItem));
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjectRelationsRightLeftOther));

            LogEndMethod(processId:processId);
            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.applyItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.newItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.deleteItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.addToClipboard.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.grid.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.applyItem);
            viewItemList.Add(viewModel.newItem);
            viewItemList.Add(viewModel.deleteItem);
            viewItemList.Add(viewModel.addToClipboard);
            viewItemList.Add(viewModel.grid);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem)
        {
            var processId = LogStartMethod();
            var idInstance = viewItem.IdViewInstance;
            var viewModel = GetViewModel(idInstance);

            var viewItemResult = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewItemResult.ViewItem != null)
            {
                resultAction.ViewItems.Add(viewItemResult.ViewItem);
            }
            resultAction.ViewItems.Add(viewItemResult.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ObjectRelationOmniList()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString(), WebViews.Config.LocalData.Object_ObjectRel_Omni_List.GUID);
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedObject.GUID;
            viewModel.ActionGetParent = Url.Action(nameof(GetObjectItem));
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjectRelationsOmni));

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ObjectAttributeList()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString(), WebViews.Config.LocalData.Object_ObjectAttributeList.GUID);
            viewModel.ChannelSelectedParent = Channels.LocalData.Object_SelectedRelationNode.GUID;
            viewModel.ActionGetParent = Url.Action(nameof(GetObjectItem));
            viewModel.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjectAttributes));

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ObjectAttributeListInit(List<string> selectorPath, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<OItemListViewModel>
            {
                IsSuccessful = true
            };
            if (selectorPath == null || !selectorPath.Any())
            {
                result.IsSuccessful = false;
                result.Message = "The selector-path is invalid!";
                return Json(result);
            }
            if (string.IsNullOrEmpty(idInstance))
            {
                idInstance = Guid.NewGuid().ToString();
            }
            result.Result = GetViewModel(idInstance, WebViews.Config.LocalData.Object_ObjectAttributeList.GUID);
            result.Result.ChannelSelectedParent = Channels.LocalData.Object_SelectedRelationNode.GUID;
            result.Result.ActionGetParent = Url.Action(nameof(GetObjectItem));
            result.Result.ActionGetGridConfig = Url.Action(nameof(GetGridConfigObjectAttributes));
            result.Result.SelectorPath = selectorPath;
            result.SelectorPath = selectorPath;

            LogEndMethod(processId:processId);
            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigObjects()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(InstanceViewItem), Url.Action(nameof(GetObjects)), Url.Action(nameof(CreateInstanceViewItem)), Url.Action(nameof(UpdateInstanceItem)), Url.Action(nameof(DestroyInstanceViewItems)), false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        public ActionResult SetCurrentItem(SelectedORelItemRequest request)
        {
            var processId = LogStartMethod();
            var result = new ResultSimple
            {
                IsSuccessful = true
            };
            var viewModel = GetViewModel(request.IdInstance);

            viewModel.selectedOItemRequest = request;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> CheckExistanceItem(List<clsOntologyItem> itemsToCheck, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<List<clsOntologyItem>>
            {
                IsSuccessful = true,
                Result = new List<clsOntologyItem>()
            };
            if (!itemsToCheck.Any())
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var viewModel = GetViewModel(idInstance);
            result.SelectorPath = viewModel.SelectorPath;

            var controller = GetController();

            var existanceResult = await controller.CheckExistance(itemsToCheck);

            if (existanceResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = existanceResult.ResultState.Additional1;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = existanceResult.Result;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveNewItems(List<clsOntologyItem> itemsToSave, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<List<InstanceViewItem>>
            {
                IsSuccessful = true,
                Result = new List<InstanceViewItem>()
            };
            if (!itemsToSave.Any())
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            
            var viewModel = GetViewModel(idInstance);

            var controller = GetController();

            var saveResult = await controller.SaveNewItems(itemsToSave);

            if (saveResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = saveResult.ResultState.Additional1;
                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (viewModel.View.IdView == WebViews.Config.LocalData.Object_Object_List.GUID)
            {
                var getInstanceViewItemsResult = await controller.GetInstanceViewItems(saveResult.Result);

                if (getInstanceViewItemsResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                    result.Message = getInstanceViewItemsResult.ResultState.Additional1;
                    LogEndMethod(processId:processId);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                result.Result = getInstanceViewItemsResult.Result;
            } else if (viewModel.View.IdView == OModules.WebViews.Config.LocalData.Object_AttributeTypeList.GUID)
            {
                var resultAttributeType = new ResultSimpleView<List<AttributeTypeViewItem>>
                {
                    IsSuccessful = true,
                    Result = new List<AttributeTypeViewItem>()
                };

                var getAttributeTypeViewItemsResult = await controller.GetAttributeTypeViewItems(saveResult.Result);

                if (getAttributeTypeViewItemsResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                    result.Message = getAttributeTypeViewItemsResult.ResultState.Additional1;
                    LogEndMethod(processId:processId);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                resultAttributeType.Result = getAttributeTypeViewItemsResult.Result;
                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SetOrderId(List<SelectedORelItemRequest> requestList)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<long>
            {
                IsSuccessful = true,
                Result = 0
            };
            if (!requestList.Any())
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var request = requestList.First();
            var viewModel = GetViewModel(request.IdInstance);

            var toChange = requestList.Select(req => new OrderIdChange
            {
                IdObject = req.IdObject,
                IdOther = req.IdOther,
                IdRelationType = req.IdRelationType,
                OrderId = req.OrderId
            }).ToList();

            var controller = GetController();
            var resultChange = await controller.SetOrderId(toChange);

            if (resultChange.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Result = viewModel.selectedOItemRequest != null ? viewModel.selectedOItemRequest.OrderId : 0;
            }
            else
            {
                result.IsSuccessful = true;
                if (requestList.Count == 1 && viewModel.selectedOItemRequest != null)
                {

                    viewModel.selectedOItemRequest.OrderId = requestList.First().OrderId;
                    
                    result.Result = viewModel.selectedOItemRequest != null ? viewModel.selectedOItemRequest.OrderId : 0;
                }
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> DuplicateObject(string idInstance, string idObject, string newName)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<InstanceViewItem>
            {
                IsSuccessful = true
            };

            var viewModel = GetViewModel(idInstance);

            var oItemListController = GetController();
            var controller = GetObjectEditController();
            var messageOutput = new OWMessageOutput(idInstance);
            var duplicateObjectRequest = new DuplicateObjectRequest(idObject) { NewObjectName = newName, MessageOutput = messageOutput };
            var resultChange = await controller.DuplicateObject(duplicateObjectRequest);

            if (resultChange.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
            }
            else
            {
                result.IsSuccessful = true;
                var instanceViewItemResult = await oItemListController.GetInstanceViewItems(new List<clsOntologyItem> { resultChange.Result.NewObject });
                result.Result = instanceViewItemResult.Result.First();
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigObjectAttributes(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            var actionResult = new ResultSimpleView<Dictionary<string, object>>
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath
            }; 
            
            actionResult.Result = GridFactory.CreateKendoGridConfig(typeof(ObjectAttributeViewItem), Url.Action(nameof(GetObjectAttributes)), Url.Action(nameof(CreateObjectAttribute)), Url.Action(nameof(UpdateObejctAttribute)), Url.Action(nameof(DestroyObjectAttribute)), false, false, false);


            LogEndMethod(processId:processId);
            return Json(actionResult, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigAttributeTypes(string idInstance)
        {
            var processId = LogStartMethod();

            var viewModel = GetViewModel(idInstance);
            var resultAction = new ResultSimpleView<Dictionary<string, object>>
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath
            };

            resultAction.Result = GridFactory.CreateKendoGridConfig(typeof(AttributeTypeViewItem), Url.Action(nameof(GetAttributeTypes)), null, Url.Action(nameof(UpdateAttributeTypes)), Url.Action(nameof(DestroyAttributeTypeViewItems)), false, false, false);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigRelationTypes()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(RelationTypeViewItem), Url.Action(nameof(GetRelationTypes)), null, null, null, false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigObjectRelationsLeftRight()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(ObjectObject_Conscious), Url.Action(nameof(GetObjectRelationsLeftRight)), Url.Action(nameof(CreateObjectRelationLeftRight)), Url.Action(nameof(UpdateObejctRelationLeftRight)), Url.Action(nameof(DestroyObjectRelationLeftRight)), false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigObjectRelationsLeftRightOther()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(ObjectObject_Conscious), Url.Action(nameof(GetObjectRelationsLeftRightOther)), null, Url.Action(nameof(UpdateObejctRelationLeftRight)), Url.Action(nameof(DestroyObjectRelationLeftRight)), false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigObjectRelationsRightLeft()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(ObjectObject_Subconscious), Url.Action(nameof(GetObjectRelationsRightLeft)), null, Url.Action(nameof(UpdateObejctRelationRightLeft)), Url.Action(nameof(DestroyObjectRelationRightLeft)), false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigObjectRelationsRightLeftOther()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(ObjectObject_Subconscious), Url.Action(nameof(GetObjectRelationsRightLeftOther)), null, Url.Action(nameof(UpdateObejctRelationRightLeft)), Url.Action(nameof(DestroyObjectRelationRightLeft)), false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigObjectRelationsOmni()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(ObjectObject_Omni), Url.Action(nameof(GetObjectRelationsOmni)), null, null, Url.Action(nameof(DestroyObjectRelationOmni)), false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetClassItem()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();
            var idItem = Request["idParent"];
            if (string.IsNullOrEmpty(idItem)) return Json(null, JsonRequestBehavior.AllowGet);


            var controller = GetController();

            var oItemTask = await controller.GetOItem(idItem, controller.Globals.Type_Class);

            var result = Json(oItemTask, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetObjectItem()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();
            var idItem = Request["idParent"];
            if (string.IsNullOrEmpty(idItem)) return Json(null, JsonRequestBehavior.AllowGet);


            var controller = GetController();
            var oItemTask = await controller.GetOItem(idItem, controller.Globals.Type_Object);

            if (oItemTask == null) return Json(null, JsonRequestBehavior.AllowGet);
            var oItemClassTask = await controller.GetOItem(oItemTask.GUID_Parent, controller.Globals.Type_Class);

            oItemTask.Additional1 = oItemTask.Name + $" ({ oItemClassTask.Name })";

            var result = Json(new { data = oItemTask }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetObjects()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();
            var idParent = Request["idParent"];
            var nameObject = Request["nameFilter"];
            if (string.IsNullOrEmpty(idParent)) 
            {
                LogEndMethod(processId: processId);
                return Json(new List<object>(), JsonRequestBehavior.AllowGet); 
            }


            var controller = GetController();
            LogInfo($"Get Objects of {idParent}", processId: processId);
            var resultTask = await controller.GetObjectList(idParent, nameObject);

            LogInfo($"Count of Objects: {resultTask.GridItems.Count}", processId: processId);
            var resultItem = new KendoGridDataResult<InstanceViewItem>(resultTask.GridItems);
            var result = Json(resultItem, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetTransformedNamesOfObject(string idParent, string objectId, string idInstance)
        {
            var processId = LogStartMethod();
            
            if (string.IsNullOrEmpty(objectId))
            {
                var resultActionNoContent = new ResultSimpleView<List<clsOntologyItem>>
                {
                    IsSuccessful = true,
                    Result = new List<clsOntologyItem>()
                };

                var resultNoContent = Json(resultActionNoContent, JsonRequestBehavior.AllowGet);
                return resultNoContent;
            }

            var resultAction = await GetTransformNames(idParent, new List<string> { objectId }, idInstance);

            var result = Json(resultAction, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId: processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetTransformedNamesOfObjects(string idParent, List<string> objectIds, string idInstance)
        {
            var processId = LogStartMethod();

            var resultAction = await GetTransformNames(idParent, objectIds, idInstance);
            var result = Json(resultAction, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId: processId);
            return result;
        }

        private async System.Threading.Tasks.Task<ResultSimpleView<List<clsOntologyItem>>> GetTransformNames(string idParent, List<string> objectIds, string idInstance)
        {
            var viewModel = GetViewModel(idInstance, WebViews.Config.LocalData.Object_Object_List.GUID);

            var controller = GetController();

            var resultAction = new ResultSimpleView<List<clsOntologyItem>>
            {
                IsSuccessful = true,
                Result = new List<clsOntologyItem>()
            };

            var objectsResult = await controller.GetObjects(objectIds);
            if (objectsResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                resultAction.IsSuccessful = false;
                resultAction.Message = objectsResult.ResultState.Additional1;
                return resultAction;
            }

            var transformProvider = GetNameTransformController(new List<string> { idParent }).FirstOrDefault();

            if (transformProvider == null)
            {
                return resultAction;
            }

            var transformed = await transformProvider.TransformNames(objectsResult.Result, idClass: idParent);
            if (transformed.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                resultAction.IsSuccessful = false;
                resultAction.Message = transformed.ResultState.Additional1;
                return resultAction;
            }

            resultAction.Result = transformed.Result;

            return resultAction;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetObjectAttributes()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();
            var idObject = Request.Params["idObject"];
            var idAttributeType = Request.Params["idAttributeType"];

            if (string.IsNullOrEmpty(idObject))
            {
                var resultEmpty = Json(new { data = new List<ObjectAttributeListViewModel>() }, JsonRequestBehavior.AllowGet);
                return resultEmpty;
            }
            var nodeItem = new ObjectRelNode(NodeType.AttributeRel)
            {
                IdLeft = idObject,
                IdRight = idAttributeType
            };

            var controller = GetController();

            var resultTask = await controller.GetObjectAttributeList(nodeItem);

            var result = Json(new { data = resultTask.GridItems }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetAttributeTypes()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();

            var controller = GetController();

            var resultTask = await controller.GetAttributeTypeList();

            var result = Json(new { data = resultTask.GridItems }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DestroyAttributeTypeViewItems(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var items = models.Select(model => DictionaryToAttributeTypeList.Convert(model)).ToList();
            if (!items.Any())
            {
                Response.StatusCode = 500;
                Response.Status = "No Items provided!";
                LogError(Response.Status);
                return null;
            }

            var controller = GetController();

            var hasRelationsResult = await controller.HasAttributeTypeRelations(items.Select(itm => itm.GUID).ToList());
            if (hasRelationsResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                Response.Status = "Error while getting the inf of relations of Attribute-Types!";
                LogError(Response.Status);
                return null;
            }

            if (hasRelationsResult.Result.Where(itm => itm.HasRelations).Count() > 0 )
            {
                Response.StatusCode = 500;
                Response.Status = "There are relations of Attribute-Types!";
                LogError(Response.Status);
                return null;
            }
            var resultDelete = await controller.DeleteItems(items);

            if (resultDelete.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                Response.Status = "Error while deleting items.!";
                LogError(Response.Status);
                return null;
            }

            var attributeTypeViewItemsResult = await controller.GetAttributeTypeList();

            if (attributeTypeViewItemsResult.Result.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                Response.Status = "Error getting the deleted-items!";
                LogError(Response.Status);
                return null;
            }

            var result = Json(new { data = attributeTypeViewItemsResult.Result }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            LogEndMethod(processId: processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> UpdateAttributeTypes(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var resultChange = new ResultSimpleView<List<AttributeTypeViewItem>>();


            LogEndMethod(processId:processId);
            var result = Json(new { data = resultChange }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetRelationTypes()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();

            var controller = GetController();

            var resultTask = await controller.GetRelationTypeList();

            var result = Json(new { data = resultTask.GridItems }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetObjectRelationsLeftRight()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();

            var result = await GetObjectRelationsLeftRight(false, instanceId);

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> UpdateObejctAttribute(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var items = models.Select(model => DictionaryToObjectAttribute.Convert(model)).ToList();

            var controller = GetController();

            var resultChange = await controller.UpdateObjectAttribute(items);

            if (resultChange.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                var resultError = Json(new { data = new List<object>() }, JsonRequestBehavior.AllowGet);
                return resultError;
            }

            items = resultChange.Result;

            LogEndMethod(processId:processId);
            var result = Json(new { data = items }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> UpdateObejctRelationLeftRight(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var items = models.Select(model => DictionaryToObjectObjectConscious.Convert(model)).ToList();

            var controller = GetController();

            var resultChange = await controller.UpdateObjectRelationsLeftRight(items);

            if (resultChange.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                var resultError = Json(new { data = new List<object>() }, JsonRequestBehavior.AllowGet);
                return resultError;
            }

            items = resultChange.Result;

            LogEndMethod(processId:processId);
            var result = Json(new { data = items }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> UpdateObejctRelationRightLeft(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var items = models.Select(model => DictionaryToObjectObjectSubconscious.Convert(model)).ToList();

            var controller = GetController();

            var resultChange = await controller.UpdateObjectRelationsRightLeft(items);

            if (resultChange.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                var resultError = Json(new { data = new List<object>() }, JsonRequestBehavior.AllowGet);
                return resultError;
            }

            items = resultChange.Result;

            LogEndMethod(processId:processId);
            var result = Json(new { data = items }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> CreateInstanceViewItem(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var resultChange = new ResultSimpleView<List<ObjectObject_Conscious>>();

            var items = models.Select(model => DictionaryToObjectList.Convert(model)).ToList();

            LogEndMethod(processId:processId);
            var result = Json(new { data = resultChange }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> HasRelations(List<string> objectIds)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<List<string>>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                Result = new List<string>()
            };

            var controller = GetController();

            var resultDelete = await controller.HasObjectRelations(objectIds);

            if (resultDelete.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = resultDelete.ResultState.Additional1;
                LogEndMethod(processId:processId);
                return Json(result);
            }

            var objectsWithRelations = resultDelete.Result.Where(res => res.HasObjectRelations);
            if (objectsWithRelations.Any())
            {
                result.Result = objectsWithRelations.Select(obj => obj.IdObject).ToList();
                result.Message = $"{ objectsWithRelations.Count(obj => obj.HasObjectRelations) } objects have relations!";
            }

            LogEndMethod(processId:processId);          
            return Json(result);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> HasRelationsAttributeTypes(List<string> attributeTypeIds)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<List<string>>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                Result = new List<string>()
            };

            var controller = GetController();

            var resultDelete = await controller.HasAttributeTypeRelations(attributeTypeIds);

            if (resultDelete.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = resultDelete.ResultState.Additional1;
                LogEndMethod(processId:processId);
                return Json(result);
            }

            var attributeTypesWithRelations = resultDelete.Result.Where(res => res.HasRelations);
            if (attributeTypesWithRelations.Any())
            {
                result.Result = attributeTypesWithRelations.Select(obj => obj.IdAttributeType).ToList();
                result.Message = $"{ attributeTypesWithRelations.Count(obj => obj.HasRelations) } AttributeTypes have relations!";
            }

            LogEndMethod(processId:processId);
            return Json(result);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DestroyInstanceViewItems(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var items = models.Select(model => DictionaryToObjectList.Convert(model)).ToList();
            if (!items.Any())
            {
                Response.StatusCode = 500;
                Response.Status = "No Items provided!";
                return null;
            }

            var controller = GetController();

            var deleteRequest = new DeleteObjectsAndRelationsRequest
            {
                ObjectsToDelete = items
            };
            var resultDelete = await controller.DeleteObjectsAndRelations(deleteRequest);

            if (resultDelete.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                Response.Status = "Error getting the deleted-items!";
                return null;
            }

            LogEndMethod(processId:processId);
            var instanceViewItemsResult = await controller.GetInstanceViewItems(items);
            if (instanceViewItemsResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                Response.Status = "Error getting the deleted-items!";
                return null;
            }

            var result = Json(new { data = instanceViewItemsResult.Result }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> UpdateInstanceItem(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var itemsPre = models.Select(model => DictionaryToObjectList.Convert(model)).ToList();

            var controller = GetController();

            var resultChange = await controller.UpdateInstanceItem(itemsPre);

            if (resultChange.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                var resultError = Json(new { data = new List<object>() }, JsonRequestBehavior.AllowGet);
                return resultError;
            }

            var items = resultChange.Result;

            LogEndMethod(processId:processId);
            var result = Json(new { data = items }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> CreateObjectAttribute(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var resultChange = new ResultSimpleView<List<ObjectAttributeViewItem>>();

            var items = models.Select(model => DictionaryToObjectAttribute.Convert(model)).ToList();

            LogEndMethod(processId:processId);
            var result = Json(new { data = resultChange }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> CreateObjectRelationLeftRight(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var resultChange = new ResultSimpleView<List<ObjectObject_Conscious>>();

            var items = models.Select(model => DictionaryToObjectObjectConscious.Convert(model)).ToList();

            LogEndMethod(processId:processId);
            var result = Json(new { data = resultChange }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DestroyObjectAttribute(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var items = models.Select(model => DictionaryToObjectAttribute.Convert(model)).ToList();

            if (!items.Any())
            {
                Response.StatusCode = 500;
                Response.Status = "No Items provided!";
                return null;
            }

            var controller = GetController();

            var resultDelete = await controller.DeleteItems(items);

            if (resultDelete.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                Response.Status = "Error deleting items!";
                return null;
            }

            LogEndMethod(processId:processId);
            var result = Json(new { data = items }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DestroyObjectRelationLeftRight(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var items = models.Select(model => DictionaryToObjectObjectConscious.Convert(model)).ToList();

            if (!items.Any())
            {
                Response.StatusCode = 500;
                Response.Status = "No Items provided!";
                return null;
            }

            var controller = GetController();

            var resultDelete = await controller.DeleteItems(items);

            if (resultDelete.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                Response.Status = "Error deleting items!";
                return null;
            }

            LogEndMethod(processId:processId);
            var result = Json(new { data = items }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DestroyObjectRelationRightLeft(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var items = models.Select(model => DictionaryToObjectObjectSubconscious.Convert(model)).ToList();

            if (!items.Any())
            {
                Response.StatusCode = 500;
                Response.Status = "No Items provided!";
                return null;
            }

            var controller = GetController();

            var resultDelete = await controller.DeleteItems(items);

            if (resultDelete.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                Response.Status = "Error deleting items!";
                return null;
            }

            LogEndMethod(processId:processId);
            var result = Json(new { data = items }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DestroyObjectRelationOmni(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var items = models.Select(model => DictionaryToObjectObjectConscious.Convert(model)).ToList();

            if (!items.Any())
            {
                Response.StatusCode = 500;
                Response.Status = "No Items provided!";
                return null;
            }

            var controller = GetController();

            var resultDelete = await controller.DeleteItems(items);

            if (resultDelete.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                Response.Status = "Error deleting items!";
                return null;
            }

            LogEndMethod(processId:processId);
            var result = Json(new { data = items }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            return result;
        }



        private async System.Threading.Tasks.Task<ActionResult> GetObjectRelationsLeftRight(bool isOther, string instanceId)
        {
            var processId = LogStartMethod();
            var nodeType = int.Parse(Request.Params["NodeType"]);

            var nodeItem = new ObjectRelNode((NodeType)nodeType)
            {
                IdLeft = Request.Params["IdLeft"],
                NameLeft = Request.Params["NameLeft"],
                IdRight = Request.Params["IdRight"],
                NameRight = Request.Params["NameRight"],
                IdRelationType = Request.Params["IdRelationType"],
                NameRelationType = Request.Params["NameRelationType"]

            };

            if (nodeItem == null) return Json(new List<object>(), JsonRequestBehavior.AllowGet);


            var controller = GetController();

            var resultTask = await controller.GetObjectRelationConscious(nodeItem, isOther);

            var resultItem = new KendoGridDataResult<ObjectObject_Conscious>(resultTask.GridItems);
            var result = Json(resultItem, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetObjectRelationsLeftRightOther()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();

            LogEndMethod(processId:processId);
            return await GetObjectRelationsLeftRight(true, instanceId);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetObjectRelationsRightLeft()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();

            LogEndMethod(processId:processId);
            return await GetObjectRelationsRightLeft(false, instanceId);
        }

        private async System.Threading.Tasks.Task<ActionResult> GetObjectRelationsRightLeft(bool isOther, string instanceId)
        {
            var processId = LogStartMethod();
            var nodeType = int.Parse(Request.Params["NodeType"]);

            var nodeItem = new ObjectRelNode((NodeType)nodeType)
            {
                IdLeft = Request.Params["IdLeft"],
                NameLeft = Request.Params["NameLeft"],
                IdRight = Request.Params["IdRight"],
                NameRight = Request.Params["NameRight"],
                IdRelationType = Request.Params["IdRelationType"],
                NameRelationType = Request.Params["NameRelationType"]

            };

            if (nodeItem == null) return Json(new List<object>(), JsonRequestBehavior.AllowGet);


            var controller = GetController();

            var resultTask = await controller.GetObjectRelationSubConscious(nodeItem, isOther);

            var resultItem = new KendoGridDataResult<ObjectObject_Subconscious>(resultTask.GridItems);
            var result = Json(resultItem, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetObjectRelationsRightLeftOther()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();

            LogEndMethod(processId:processId);
            return await GetObjectRelationsRightLeft(true, instanceId);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetObjectRelationsOmni()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();
            var idObject = Request["GUID"];
            if (idObject == null) return Json(new List<object>(), JsonRequestBehavior.AllowGet);


            var controller = GetController();

            var resultTask = await controller.GetObjectRelation(idObject);

            var result = Json(new { data = resultTask.GridItems }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }
    }
}