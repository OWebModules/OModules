﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using SecurityModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class SecurityController : AppControllerBase
    {
        private SecurityModule.SecurityController GetControllerSecurity()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var securityController = memoryCache.GetOrSet<SecurityModule.SecurityController>(Session.SessionID, $"{nameof(SecurityModule)}{nameof(SecurityModule.SecurityController)}", () =>
            {
                return new SecurityModule.SecurityController(globals);
            });

            LogEndMethod(processId:processId);            return securityController;
        }

        private PasswordSafeViewModel GetPasswordSafeViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<PasswordSafeViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new PasswordSafeViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedPasswordSafe)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedPasswordSafe)),
                    ActionSetViewReady = Url.Action(nameof(SetViewPasswordSafeReady)),
                    ActionValidateReference = Url.Action(nameof(ValidatePasswordSafeReference)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfigPasswordSafeItems)),
                    ActionGetPasswordItems = Url.Action(nameof(GetPasswordItems)),
                    ActionGetSecurePassword = Url.Action(nameof(GetSecurePassword)),
                    ActionSavePassword = Url.Action(nameof(CreatePassword)),
                    ActionChangePassword = Url.Action(nameof(ChangePassword)),
                    ActionValidatePassword = Url.Action(nameof(ValidatePassword)),
                    ActionDecodePassword = Url.Action(nameof(DecodePassword)),
                    ActionAuthenticationInit = Url.Action(nameof(AuthenticationInit)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangePasswordSafeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePasswordSafeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetPasswordSafeViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> CreatePassword(string idInstance, string password)
        {
            var processId = LogStartMethod();
            var viewModel = GetPasswordSafeViewModel(idInstance);

            var securityController = GetControllerSecurity();

            var resultSavePassword = await securityController.SavePassword(viewModel.refItem, password);

            var result = new ResultSimple
            {
                IsSuccessful = resultSavePassword.ResultState.GUID == securityController.Globals.LState_Success.GUID
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangePassword(string idInstance, string idPassword, string password)
        {
            var processId = LogStartMethod();
            var viewModel = GetPasswordSafeViewModel(idInstance);

            var securityController = GetControllerSecurity();

            var resultSavePassword = await securityController.SavePassword(viewModel.refItem, password, idPassword);

            var result = new ResultSimple
            {
                IsSuccessful = resultSavePassword.ResultState.GUID == securityController.Globals.LState_Success.GUID
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedPasswordSafe()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetPasswordSafeViewModel(idInstance);

            viewItemList.Add(viewModel.authenticationWindow);
            viewItemList.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedPasswordSafe()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetPasswordSafeViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewPasswordSafeReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetPasswordSafeViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> DecodePassword(string idInstance, string idPassword)
        {
            var processId = LogStartMethod();
            var viewModel = GetPasswordSafeViewModel(idInstance);

            var controller = GetControllerSecurity();

            var result = new ResultSimpleView<string>
            {
                IsSuccessful = true
            };

            if (!controller.IsAuthenticated)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var decodeResult = await controller.DecodePassword(idPassword);

            result.IsSuccessful = decodeResult.ResultState.GUID != controller.Globals.LState_Error.GUID;
            result.Result = decodeResult.Result;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidatePasswordSafeReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetPasswordSafeViewModel(idInstance);

            var controller = GetControllerSecurity();

            if (!controller.IsAuthenticated) return Json(new List<ViewItem>(), JsonRequestBehavior.AllowGet);

            var refItemTask = await controller.GetOItem(refItem.GUID, controller.Globals.Type_Object);

            viewModel.refItem = refItemTask.Result;

            var resultViewItems = new List<ViewItem>();
            if (viewModel.refItem != null)
            {
                var refItemParent = await controller.GetOItem(viewModel.refItem.GUID_Parent, controller.Globals.Type_Class);
                viewModel.refParentItem = refItemParent.Result;


                viewModel.SetWindowTitle($"{viewModel.refItem.Name} ({viewModel.refParentItem.Name})");
                resultViewItems.Add(viewModel.windowTitle);
            }

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigPasswordSafeItems()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(PasswordSafeItem), Url.Action(nameof(GetPasswordItems)), null, null, null, false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetSecurePassword(string idInstance)
        {
            var processId = LogStartMethod();
            var password = RandomPassword.Generate(15, 15);

            var result = new ResultSimpleView<string>
            {
                IsSuccessful = true,
                Result = password
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidatePassword(string idInstance, string password)
        {
            var processId = LogStartMethod();
            var viewModel = GetPasswordSafeViewModel(idInstance);
            var controller = GetControllerSecurity();

            var userId = User.Identity.GetUserId();
            var userItemResult = await controller.GetUser(userId);

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>
                {
                    viewModel.alertWrongPassword
                }
            };
            if (userItemResult.Result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.alertWrongPassword.ChangeViewItemValue(ViewItemType.Content.ToString(), Primitives.Messages.General_ErrorOccured);
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultValidate = await controller.ValidatePassword(userItemResult.UserItem, password);

            result.IsSuccessful = resultValidate.Result.GUID != controller.Globals.LState_Error.GUID;

            if (!result.IsSuccessful)
            {
                viewModel.alertWrongPassword.ChangeViewItemValue(ViewItemType.Content.ToString(), Primitives.Messages.SecurityController_PasswordWrong);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetPasswordItems(string idInstance)
        {

            var processId = LogStartMethod();
            var viewModel = GetPasswordSafeViewModel(idInstance);
            var controller = GetControllerSecurity();
            if (viewModel.refItem == null)
            {
                var resultEmptyList = await controller.GetEmptyPasswordSafeItemList();
                return Json(resultEmptyList.Result, JsonRequestBehavior.AllowGet);
            }

            var resultTask = await controller.GetPasswordSafeItems(viewModel.refItem, viewModel.refParentItem);

            var result = Json(new { data = resultTask.Result }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        // GET: Security
        public ActionResult Index()
        {
            return View();
        }


        [Authorize]
        public ActionResult PasswordSafe()
        {
            var processId = LogStartMethod();
            var viewModel = GetPasswordSafeViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult AuthenticationInit()
        {
            var processId = LogStartMethod();
            var viewModel = GetPasswordSafeViewModel(Guid.NewGuid().ToString());
            var resultAction = new ResultSimpleView<PasswordSafeViewModel>
            {
                IsSuccessful = true
            };

            resultAction.Result = viewModel;

            var securityController = GetControllerSecurity();

            if (securityController.IsAuthenticated)
            {
                viewModel.authenticationWindow.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
            }
            else
            {
                viewModel.authenticationWindow.ChangeViewItemValue(ViewItemType.Other.ToString(), true);
            }

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }
    }
}