﻿using MediaViewerModule.Converters;
using MediaViewerModule.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.MediaViewer;
using OModules.Models.MessageOutput;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class MediaViewerController : AppControllerBase
    {
        

        private string mediaListPDFViewId = "77ea69778d4e4d9fbd2ffee8bd122636";
        private string mediaListAudioViewId = "aad87369aa564a439fe6bd727750e44b";
        private string mediaListVideoViewId = "0ecd03c9162c45839331260af4336af7";
        private string mediaListImagesViewId = "cf260392e0a24d32a2f2162e9795de0a";

        private MediaViewerModule.Connectors.MediaViewerConnector GetControllerMediaViewer()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });


            var result = new MediaViewerModule.Connectors.MediaViewerConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private MediaViewerModule.Connectors.MediaTaggingController GetControllerMediaTagging()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new MediaViewerModule.Connectors.MediaTaggingController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private TypedTaggingModule.Connectors.TypedTaggingConnector GetControllerTypedTagging()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new TypedTaggingModule.Connectors.TypedTaggingConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private MediaViewerModule.Connectors.ImageMapController GetControllerImageMap()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });


            var result = new MediaViewerModule.Connectors.ImageMapController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private MediaStore_Module.FileDownloadController GetControllerFileDownload()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new MediaStore_Module.FileDownloadController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private SecurityModule.SecurityController GetControllerSecurity()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            LogEndMethod(processId:processId);
            return new SecurityModule.SecurityController(globals);

        }

        private MediaViewerModule.Connectors.PDFSearchController GetControllerPDFSearch()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            LogEndMethod(processId:processId);
            return new MediaViewerModule.Connectors.PDFSearchController(globals);

        }

        private ImageMapViewModel GetViewModelImageMap(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ImageMapViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ImageMapViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedImageMap)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedImageMap)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyImageMap)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceImageMap)),
                    ActionImageMapInit = Url.Action(nameof(ImageMapInit)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueImageMap)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private MediaBookmarkManagerViewModel GetViewModelMediaBookmarkManager(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<MediaBookmarkManagerViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new MediaBookmarkManagerViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedMediaBookmarkManager)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedMediaBookmarkManager)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyMediaBookmarkManager)),
                    ActionItemComToolBarInit = Url.Action(nameof(ObjectEditController.ItemComBarInit), nameof(ObjectEditController).Replace("Controller", "")),
                    ActionMediaBookmarkManagerInit = Url.Action(nameof(MediaBookmarkManagerInit)),
                    ActionVideoPlayerInit = Url.Action(nameof(VideoPlayerInit)),
                    ActionObjectListLinit = Url.Action(nameof(OItemListController.ObjectListInit), nameof(OItemListController).Replace("Controller", "")),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceMediaBookmarkManager)),
                    ActionCreateMediaBookmark = Url.Action(nameof(CreateMediaBookmark)),
                    ActionDeleteMediaBookmark = Url.Action(nameof(DeleteMediaBookmark)),
                    ActionRelateBookmark = Url.Action(nameof(RelateBookmark)),
                    ActionRemoveReference = Url.Action(nameof(RemoveReference)),
                    ActionInitClassTree = Url.Action(nameof(ClassTreeController.ClassTreeInit), nameof(ClassTreeController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueMediaBookmarkManager)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private AudioPlayerViewModel GetViewModelAudioPlayer(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<AudioPlayerViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new AudioPlayerViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigAudioPlayer
                    {
                        ActionConnected = Url.Action(nameof(ConnectedAudioPlayer)),
                        ActionDisconnected = Url.Action(nameof(DisconnectedAudioPlayer)),
                        ActionSetViewReady = Url.Action(nameof(SetViewReadyAudioPlayer)),
                        ActionValidateReference = Url.Action(nameof(ValidateReferenceAudioPlayer)),
                        ActionGetSessionDataUrl = Url.Action(nameof(SessionDataController.GetSessionDataUrl), nameof(SessionDataController).Replace("Controller", ""))

                    },
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueAudioPlayer)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private PDFSearchViewModel GetViewModelPDFSearch(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<PDFSearchViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new PDFSearchViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedPDFSearch)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedPDFSearch)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyPDFSearch)),
                    ActionGetGridConfigSearch = Url.Action(nameof(GetGridConfigPDFSearch)),
                    ActionInitPDFSearch = Url.Action(nameof(PDFSearchInit)),
                    ActionRelationEditor = Url.Action(nameof(ObjectEditController.RelationEditor),
                        nameof(ObjectEditController).Replace("Controller", "")),
                    ActionModuleStarter = Url.Action(nameof(ModuleMgmtController.ModuleStarter),
                        nameof(ModuleMgmtController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValuePDFSearch)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private VideoPlayerViewModel GetViewModelVideoPlayer(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<VideoPlayerViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new VideoPlayerViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigVideoPlayer
                    {
                        ActionConnected = Url.Action(nameof(ConnectedVideoPlayer)),
                        ActionDisconnected = Url.Action(nameof(DisconnectedVideoPlayer)),
                        ActionSetViewReady = Url.Action(nameof(SetViewReadyVideoPlayer)),
                        ActionValidateReference = Url.Action(nameof(ValidateReferenceVideoPlayer)),
                        ActionGetSessionDataUrl = Url.Action(nameof(SessionDataController.GetSessionDataUrl), nameof(SessionDataController).Replace("Controller", "")),
                        ActionVideoPlayerInit = Url.Action(nameof(VideoPlayerInit))

                    },
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueVideoPlayer)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;

        }

        private ImageViewerViewModel GetViewModelImageViewer(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ImageViewerViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ImageViewerViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigImageViewer
                    {
                        ActionConnected = Url.Action(nameof(ConnectedImageViewer)),
                        ActionDisconnected = Url.Action(nameof(DisconnectedImageViewer)),
                        ActionSetViewReady = Url.Action(nameof(SetViewReadyImageViewer)),
                        ActionValidateReference = Url.Action(nameof(ValidateReferenceImageViewer)),
                        ActionGetSessionDataUrl = Url.Action(nameof(SessionDataController.GetSessionDataUrl), nameof(SessionDataController).Replace("Controller", ""))


                    },
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueImageViewer)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private PDFViewerViewModel GetViewModelPDFViewer(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<PDFViewerViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new PDFViewerViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigPDFViewer
                    {
                        ActionConnected = Url.Action(nameof(ConnectedPDFViewer)),
                        ActionDisconnected = Url.Action(nameof(DisconnectedPDFViewer)),
                        ActionSetViewReady = Url.Action(nameof(SetViewReadyPDFViewer)),
                        ActionValidateReference = Url.Action(nameof(ValidateReferencePDFViewer)),
                        ActionOpenPDF = Url.Action(nameof(OpenPDF))

                    },
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValuePDFViewer)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private MediaListViewModel GetViewModelMediaList(string key, string viewId = null)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<MediaListViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new MediaListViewModel(Session, key, viewId, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigMediaList
                    {
                        ActionConnected = Url.Action(nameof(ConnectedMediaList)),
                        ActionDisconnected = Url.Action(nameof(DisconnectedMediaList)),
                        ActionSetViewReady = Url.Action(nameof(SetViewReadyMediaList)),
                        ActionValidateReference = Url.Action(nameof(ValidateReferenceMediaList)),
                        ActionGetGridConfig = Url.Action(nameof(GetGridConfigMediaList)),
                        ActionFileUpload = Url.Action(nameof(FileUploadController.FileUpload), nameof(FileUploadController).Replace("Controller", "")),
                        ActionAudioPlayer = Url.Action(nameof(AudioPlayer)),
                        ActionVideoPlayer = Url.Action(nameof(VideoPlayer)),
                        ActionImageViewer = Url.Action(nameof(ImageViewer)),
                        ActionSaveSessionData = Url.Action(nameof(SessionDataController.SaveSessionData), nameof(SessionDataController).Replace("Controller", "")),
                        ActionDownloadMediaList = Url.Action(nameof(DownloadMediaList)),
                        ActionInitializeViewItems = Url.Action(nameof(InitializeViewItems)),
                        ActionSaveFiles = Url.Action(nameof(SaveFiles))

                    },
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueMediaList)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        // GET: MediaViewer
        public ActionResult Index()
        {
            var processId = LogStartMethod();
            LogEndMethod(processId:processId);
            return View();
        }
        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> MediaBookmarkManagerVideo()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaBookmarkManager(Guid.NewGuid().ToString());
            viewModel.mediaType = "Video";

            var controller = GetControllerMediaViewer();

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> MediaBookmarkManagerInit(string idViewModel, List<string> selectorPath, string idMediaItem, string idRefItem, string typeItem)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idViewModel))
            {
                idViewModel = Guid.NewGuid().ToString();
            }
            var viewModel = GetViewModelMediaBookmarkManager(idViewModel);

            var controller = GetControllerMediaTagging();

            viewModel.logstatePosition = controller.LogStatePosition;
            var resultAction = new ResultSimpleView<MediaBookmarkManagerViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            viewModel.SelectorPath = selectorPath ?? new List<string>();
            if (string.IsNullOrEmpty(idMediaItem) || string.IsNullOrEmpty(typeItem))
                return Json(resultAction, JsonRequestBehavior.AllowGet);

            if (string.IsNullOrEmpty(idRefItem) || string.IsNullOrEmpty(typeItem))
                return Json(resultAction, JsonRequestBehavior.AllowGet);

            var oItem = await controller.GetOItem(idMediaItem, typeItem);
            viewModel.MediaItem = oItem.Result;

            var oRefItem = await controller.GetOItem(idRefItem, typeItem);
            viewModel.RefItem = oRefItem.Result;

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
                resultAction.IsSuccessful = false;
                resultAction.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> VideoPlayer()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelVideoPlayer(Guid.NewGuid().ToString());

            var controller = GetControllerMediaViewer();

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> VideoPlayerInit(string idViewModel, List<string> selectorPath, string idItem, string typeItem)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idViewModel))
            {
                idViewModel = Guid.NewGuid().ToString();
            }
            var viewModel = GetViewModelVideoPlayer(idViewModel);

            var controller = GetControllerMediaViewer();
            var resultAction = new ResultSimpleView<VideoPlayerViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            viewModel.SelectorPath = selectorPath ?? new List<string>();
            if (string.IsNullOrEmpty(idItem) || string.IsNullOrEmpty(typeItem))
                return Json(resultAction, JsonRequestBehavior.AllowGet);

            var oItem = controller.GetOItem(idItem, typeItem);
            viewModel.RefItem = oItem.Result.OItem;

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
                resultAction.IsSuccessful = false;
                resultAction.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> AudioPlayer()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelAudioPlayer(Guid.NewGuid().ToString());

            var controller = GetControllerMediaViewer();

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> ImageViewer()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelImageViewer(Guid.NewGuid().ToString());

            var controller = GetControllerMediaViewer();

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> PDFViewer()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPDFViewer(Guid.NewGuid().ToString());

            var controller = GetControllerMediaViewer();

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);
            return View(viewModel);
        }


        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> MediaListPDF()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaList(Guid.NewGuid().ToString(), mediaListPDFViewId);

            var controller = GetControllerMediaViewer();

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> MediaListPDFInit()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaList(Guid.NewGuid().ToString(), mediaListPDFViewId);

            var controller = GetControllerMediaViewer();

            var result = await controller.CheckMediaServier();
            var resultAction = new ResultSimpleView<MediaListViewModel>
            {
                IsSuccessful = true
            };

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
                resultAction.IsSuccessful = false;
            }

            resultAction.Result = viewModel;

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> PDFSearchInit()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPDFSearch(Guid.NewGuid().ToString());

            var controller = GetControllerPDFSearch();
            var mediaListController = GetControllerMediaViewer();
            LogInfo("Check Media-Service", processId: processId);
            var result = await mediaListController.CheckMediaServier();
            LogInfo("Checked Media-Service", processId: processId);
            var resultAction = new ResultSimpleView<PDFSearchViewModel>
            {
                IsSuccessful = true
            };

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
                resultAction.IsSuccessful = false;
            }

            resultAction.Result = viewModel;

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> PDFSearch()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPDFSearch(Guid.NewGuid().ToString());

            var controller = GetControllerPDFSearch();
            var mediaListController = GetControllerMediaViewer();
            LogInfo("Check Media-Service", processId: processId);
            var result = await mediaListController.CheckMediaServier();
            LogInfo("Checked Media-Service", processId: processId);
            var resultAction = new ResultSimpleView<PDFSearchViewModel>
            {
                IsSuccessful = true
            };

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
                resultAction.IsSuccessful = false;
            }

            resultAction.Result = viewModel;

            LogEndMethod(processId: processId);
            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> MediaListAudio()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaList(Guid.NewGuid().ToString(), mediaListAudioViewId);

            var controller = GetControllerMediaViewer();

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> MediaListVideo()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaList(Guid.NewGuid().ToString(), mediaListVideoViewId);

            var controller = GetControllerMediaViewer();

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> MediaListImage()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaList(Guid.NewGuid().ToString(), mediaListImagesViewId);

            var controller = GetControllerMediaViewer();

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> ImageMapInit()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelImageMap(Guid.NewGuid().ToString());
            var resultAction = new ResultSimpleView<ImageMapViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };
            var controller = GetControllerImageMap();

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
                resultAction.IsSuccessful = false;
            }

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> ImageMap()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelImageMap(Guid.NewGuid().ToString());

            var controller = GetControllerImageMap();

            var result = await controller.CheckMediaServier();

            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                viewModel.PageInitState.IsError = true;
                viewModel.PageInitState.Message = result.Additional1;
            }

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveFiles(List<clsOntologyItem> fileItems, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaList(idInstance);
            var controller = GetControllerMediaViewer();

            var result = new ResultSimpleView<List<MediaListItem>>
            {
                IsSuccessful = true,
                Result = new List<MediaListItem>()
            };
            var mediaType = controller.ConvertOMediaTypeToIdClassMediaType(viewModel.MediaType);
            var resultSave = await controller.SaveFileItems(viewModel.ObjectWithParent.OItem, fileItems, mediaType);

            if (resultSave.Result.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while saving fileitems", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = resultSave.MediaListItems;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigPDFSearch()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(PDFSearchGridItem), Url.Action(nameof(GetPDFSearchGridViewItems)), null, null, null, true, true, true);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetPDFSearchGridViewItems(string idInstance, string search)
        {
            var viewModel = GetViewModelPDFSearch(idInstance);

            var page = int.Parse(Request.Params["page"]);
            var pageSize = int.Parse(Request.Params["pageSize"]);
            var sortKeys = Request.Params.AllKeys.Where(key => key.StartsWith("sort")).ToList();
            var sortRequests = new List<KendoSortRequest>();
            page -= 1;

            if (viewModel.lastPageSize != pageSize)
            {
                viewModel.scrollId = null;
            }

            if (viewModel.lastPage == page)
            {
                viewModel.scrollId = null;
            }

            viewModel.lastPageSize = pageSize;
            viewModel.lastPage = page;

            sortKeys.ForEach(sortKey =>
            {

                var match = Regex.Match(sortKey, @"\[\d+\]");

                if (match.Success)
                {
                    var ix = int.Parse(match.Value.Replace("[", "").Replace("]", ""));
                    var prop = sortKey.Substring(match.Index + match.Length).Replace("[", "").Replace("]", "");

                    var sortRequest = sortRequests.FirstOrDefault(sortR => sortR.Ix == ix);
                    if (sortRequest == null)
                    {
                        sortRequest = new KendoSortRequest
                        {
                            Ix = ix
                        };

                        sortRequests.Add(sortRequest);


                    }


                    var propItem = typeof(KendoSortRequest).GetProperty(prop);

                    propItem.SetValue(sortRequest, Request.Params[sortKey]);
                }
            });

            sortRequests.ForEach(sortRequest =>
            {
                var lastSortRequest = viewModel.lastSortRequests.FirstOrDefault(lastSortR => lastSortR.Ix == sortRequest.Ix && lastSortR.field == sortRequest.field && lastSortR.dir == sortRequest.dir);
                if (lastSortRequest == null)
                {
                    viewModel.scrollId = null;
                }
            });

            if (sortRequests.Count != viewModel.lastSortRequests.Count)
            {
                viewModel.scrollId = null;
            }

            viewModel.lastSortRequests = sortRequests;

            var controller = GetControllerPDFSearch();

            var searchPDFRequest = new SearchPDFItemsRequest(search)
            {
                ScrollId = viewModel.scrollId,
                PageSize = pageSize,
                Page = page,
                SortRequests = sortRequests,
                PDFViewerAction = Url.Action(nameof(PDFViewer))
            };

            var dataResult = await controller.SearchPDFItems(searchPDFRequest);

            if (dataResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                return Json(new List<object>(), JsonRequestBehavior.AllowGet);
            }

            viewModel.scrollId = dataResult.Result.ScrollId;

            var dict = dataResult.Result.GridItems;

            var result = new
            {
                data = dict,
                total = dataResult.Result.TotalCount
            };
            return Json(result, JsonRequestBehavior.AllowGet);

            //var result = await controller.LoadData(viewModel.refItem, viewModel.ReportItem, viewModel.ReportFields);


            //if (result.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            //{
            //    return Json(new List<object>(), JsonRequestBehavior.AllowGet);
            //}

            //return Json(result.Result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigMediaList()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(MediaListItem), Url.Action(nameof(GetMediaListItems)), Url.Action(nameof(CreateMediaListItems)), Url.Action(nameof(UpdateMediaListItemsAudio)), Url.Action(nameof(DeleteMediaListItems)), false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetMediaListItems(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaList(idInstance);
            var controller = GetControllerMediaViewer();
            if (viewModel.ObjectWithParent == null)
            {
                var resultEmptyList = controller.GetEmptyMediaList();
                return Json(resultEmptyList.MediaListItems, JsonRequestBehavior.AllowGet);
            }

            var resultTask = await controller.GetMediaListItems(new List<clsOntologyItem> { viewModel.ObjectWithParent.OItem }, Server.MapPath($"~/Resources/UserGroupRessources"), Url.Content($"~/Resources/UserGroupRessources"), viewModel.MediaType);

            viewModel.mediaList = resultTask.MediaListItems;

            var resultItem = new KendoGridDataResult<MediaListItem>(resultTask.MediaListItems);
            var result = Json(resultItem, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DownloadMediaList(string idInstance, string sort)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaList(idInstance);
            var controller = GetControllerFileDownload();

            if (string.IsNullOrEmpty(idInstance))
            {
                Response.StatusCode = 500;
                return Content("");
            }

            if (!viewModel.mediaList.Any())
            {
                return Content("No Files present in MediaList");
            }

            var fileList = viewModel.mediaList.OrderBy(media => media.OrderId).Select(media => new clsOntologyItem
            {
                GUID = media.IdFile,
                Name = media.NameMultimediaItem,
                GUID_Parent = controller.ClassFile.GUID,
                Type = controller.Globals.Type_Object,
                Val_Long = media.OrderId
            }).ToList();

            var request = new MediaStore_Module.Models.GetDownloadFilesRequest(Server.MapPath("~/Resources/UserGroupRessources"), fileList, viewModel.ObjectWithParent.OItem.Name);
            if (!string.IsNullOrEmpty(sort))
            {
                if (fileList.Count < 10)
                {
                    request.OrderFormat = "0";
                }
                else if (fileList.Count < 100)
                {
                    request.OrderFormat = "00";
                }
                else if (fileList.Count < 1000)
                {
                    request.OrderFormat = "000";
                }
                else if (fileList.Count < 10000)
                {
                    request.OrderFormat = "0000";
                }
                else if (fileList.Count < 100000)
                {
                    request.OrderFormat = "00000";
                }
                else if (fileList.Count < 1000000)
                {
                    request.OrderFormat = "000000";
                }
                else
                {
                    request.OrderFormat = "0000000";
                }

                if (sort == "asc")
                {
                    request.SortOrder = MediaStore_Module.Models.DownloadSort.Asc;

                }
                else
                {
                    request.SortOrder = MediaStore_Module.Models.DownloadSort.Desc;
                }
            }
            var result = await controller.GetDownloadFiles(request);
            if (result.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                return Content("");
            }

            byte[] content;
            using (var stream = new StreamContent(result.Result))
            {
                content = await stream.ReadAsByteArrayAsync();
            }

            Response.AddHeader("Content-Disposition", $"attachment; filename={result.ResultState.Additional1}");

            LogEndMethod(processId:processId);
            return File(content, "application/x-zip-compressed");

        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> CreateMediaListItems(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var controller = GetControllerMediaViewer();

            var resultEmptyList = controller.GetEmptyMediaList();



            LogEndMethod(processId:processId);
            return Json(resultEmptyList.MediaListItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> UpdateMediaListItemsAudio(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var items = models.Select(model => DictionaryToMediaListItem.Convert(model, MediaViewerModule.Primitives.MultimediaItemType.Audio)).ToList();

            var controller = GetControllerMediaViewer();


            var controllerResult = await controller.ChangeOrderIdAndName(items);
            if (controllerResult.GUID == controller.Globals.LState_Error.GUID)
            {
                var result = controller.GetEmptyMediaList();
                return Json(result.MediaListItems, JsonRequestBehavior.AllowGet);
            }

            LogEndMethod(processId:processId);
            return Json(models, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DeleteMediaListItems(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var controller = GetControllerMediaViewer();

            var resultEmptyList = controller.GetEmptyMediaList();



            LogEndMethod(processId:processId);
            return Json(resultEmptyList.MediaListItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> CreateMediaBookmark(CreateMediaBookmarkRequest request)
        {
            var processId = LogStartMethod();

            var viewModel = GetViewModelMediaBookmarkManager(request.idInstance);
            var result = new ResultSimpleView<MediaBookmark>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                SelectorPath = viewModel.SelectorPath
            };

            double second;
            if (!double.TryParse(request.pos, NumberStyles.Number, CultureInfo.InvariantCulture, out second))
            {
                result.IsSuccessful = false;
                result.Message = "The position is not valid!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var bookMark = new MediaBookmark
            {
                NameBookmark = request.nameBookmark,
                Created = DateTime.Now,
                IdMediaItem = request.idMultimediaItem,
                LogState = request.logState,
                Second = second
            };

            var controllerTagging = GetControllerMediaTagging();

            var securityController = GetControllerSecurity();
            var userId = User.Identity.GetUserId();
            var userItem = await securityController.GetUser(userId);
            bookMark.User = userItem.UserItem;
            var saveRequest = new SaveBookmarksRequest(new List<MediaBookmark> { bookMark });
            var saveBookmark = await controllerTagging.SaveBookmarks(saveRequest);

            if (saveBookmark.ResultState.GUID == controllerTagging.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Bookmark cannot be saved!!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = saveBookmark.Result.First().Result;
            viewModel.MediaBookmarks.Add(result.Result);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> DeleteMediaBookmark(string idMediaBookmark, string idInstance)
        {
            var processId = LogStartMethod();

            var viewModel = GetViewModelMediaBookmarkManager(idInstance);
            var result = new ResultSimpleView<MediaBookmark>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                SelectorPath = viewModel.SelectorPath
            };

            var mediaBookmarkToDelete = viewModel.MediaBookmarks.FirstOrDefault(bookM => bookM.IdBookmark == idMediaBookmark);

            if (mediaBookmarkToDelete == null)
            {
                result.IsSuccessful = false;
                result.Message = "The bookmark cannot be found in list!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = mediaBookmarkToDelete;

            var controller = GetControllerMediaTagging();

            var messageOutput = new OWMessageOutput(viewModel.IdInstance);
            var deleteResult = await controller.DeleteMediaBookmarks(new List<MediaBookmark> { result.Result }, messageOutput);
            if (deleteResult.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = deleteResult.Additional1;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> RelateBookmark(string idMediaBookmark, List<string> idObjects, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaBookmarkManager(idInstance);
            var result = new ResultSimpleView<MediaBookmark>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                SelectorPath = viewModel.SelectorPath
            };

            var mediaBookmarkToDelete = viewModel.MediaBookmarks.FirstOrDefault(bookM => bookM.IdBookmark == idMediaBookmark);

            if (mediaBookmarkToDelete == null)
            {
                result.IsSuccessful = false;
                result.Message = "The bookmark cannot be found in list!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = mediaBookmarkToDelete;

            var controller = GetControllerMediaTagging();
            var oItemResult = await controller.GetOItems(idObjects, controller.Globals.Type_Object);
            if (oItemResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Error while getting the objects to bookmark!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            var updateBookmark = new BookmarkRelationsUpdate { MediaBookmark = result.Result, NewReferences = oItemResult.Result };
            var relateRequest = new RelateBookmarkRequest(new List<BookmarkRelationsUpdate> { updateBookmark });
            var messageOutput = new OWMessageOutput(viewModel.IdInstance);
            relateRequest.MessageOutput = messageOutput;

            var relateResult = await controller.RelateBookmarks(relateRequest);
            if (relateResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = relateResult.ResultState.Additional1;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            result.Result = relateResult.Result.MediaBookmarks.First();
            var typedTaggingController = GetControllerTypedTagging();
            var securityConnector = GetControllerSecurity();

            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);
            var groupItem = await securityConnector.GetGroup(userId);
            //var taggingTask = await typedTaggingController.SaveTags(viewModel.RefItem, relateResult.Result.MediaBookmarks.SelectMany(bookM => bookM.References).ToList(), userItem.UserItem, groupItem.GroupItem, false, messageOutput);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> RemoveReference(string idMediaBookmark, string idReference, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<clsOntologyItem>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
            };

            if (string.IsNullOrEmpty(idMediaBookmark) || string.IsNullOrEmpty(idReference) || string.IsNullOrEmpty(idInstance))
            {
                result.IsSuccessful = false;
                result.Message = "One of the parameters is null!";
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var viewModel = GetViewModelMediaBookmarkManager(idInstance);
            result.SelectorPath = viewModel.SelectorPath;

            var mediaBookmark = viewModel.MediaBookmarks.FirstOrDefault(bookM => bookM.IdBookmark == idMediaBookmark);
            if (mediaBookmark == null)
            {
                result.IsSuccessful = false;
                result.Message = "The bookmark cannot be found!";
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var reference = mediaBookmark.References.FirstOrDefault(refItem => refItem.GUID == idReference);
            if (reference == null)
            {
                result.IsSuccessful = false;
                result.Message = "The reference cannot be found!";
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }
            var controller = GetControllerMediaTagging();

            var request = new RemoveReferencesRequest(new List<MediaBookmarkReference> { new MediaBookmarkReference
                    {
                        IdMediaBookmark = mediaBookmark.IdBookmark,
                        IdReference = reference.GUID
                    }
            });
            var messageOutput = new OWMessageOutput(viewModel.IdInstance);
            request.MessageOutput = messageOutput;

            var removeResult = await controller.RemoveRelation(request);

            result.IsSuccessful = removeResult.GUID == controller.Globals.LState_Success.GUID;
            result.Message = removeResult.Additional1;
            if (result.IsSuccessful)
            {
                mediaBookmark.References.Remove(reference);
            }
            reference.GUID_Related = mediaBookmark.IdBookmark;
            result.Result = reference;

            LogEndMethod(processId:processId);            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceAudioPlayer(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelAudioPlayer(idInstance);

            var resultViewItems = new List<ViewItem>();


            var controller = GetControllerMediaViewer();
            var securityConnector = GetControllerSecurity();

            var userId = User.Identity.GetUserId();
            var userItem = securityConnector.GetUser(userId);
            var groupItem = securityConnector.GetGroup(userId);



            var resultMediaViewerItems = await controller.GetMediaListItems(new List<clsOntologyItem> { refItem }, Server.MapPath($"~/Resources/UserGroupRessources"), Url.Content($"~/Resources/UserGroupRessources"), MediaViewerModule.Primitives.MultimediaItemType.Audio);

            LogEndMethod(processId:processId);
            return Json(resultMediaViewerItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceMediaBookmarkManager(string idMediaItem, string idReference, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaBookmarkManager(idInstance);

            viewModel.MediaBookmarks.Clear();

            var result = new ResultSimpleView<List<MediaBookmark>>
            {
                IsSuccessful = true,
                Result = new List<MediaBookmark>(),
                ViewItems = new List<ViewItem>(),
                SelectorPath = viewModel.SelectorPath
            };

            var controllerTagging = GetControllerMediaTagging();

            var getOItemResult = await controllerTagging.GetOItem(idMediaItem, controllerTagging.Globals.Type_Object);

            if (getOItemResult.ResultState.GUID == controllerTagging.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "MediaItem could not be found!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.MediaItem = getOItemResult.Result;

            var getRefItemResult = await controllerTagging.GetOItem(idReference, controllerTagging.Globals.Type_Object);

            if (getRefItemResult.ResultState.GUID == controllerTagging.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "RefItem could not be found!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RefItem = getRefItemResult.Result;

            var mediaItems = new List<MediaListItem>();

            var securityConnector = GetControllerSecurity();

            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);
            var groupItem = await securityConnector.GetGroup(userId);

            var request = new GetMediaBookmarkModelRequest(new List<string> { viewModel.MediaItem.GUID }, MediaBookmarkModelRequestType.ByMediaItemIds);
            var messageOutput = new OWMessageOutput(viewModel.IdInstance);
            request.MessageOutput = messageOutput;
            request.PostFilters = new List<MediaBookMarkPostFilter>
            {
                new MediaBookMarkPostFilter(new List<string>
                    {
                        userItem.UserItem.GUID
                    }, MediaBookmarkModelRequestType.ByUserIds)
            };
            var resultMediaBookmarks = await controllerTagging.GetMediaBookmarks(request);
            if (resultMediaBookmarks.ResultState.GUID == controllerTagging.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = resultMediaBookmarks.ResultState.Additional1;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.MediaBookmarks = resultMediaBookmarks.Result;
            result.Result = resultMediaBookmarks.Result;
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceImageViewer(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelImageViewer(idInstance);

            var resultViewItems = new List<ViewItem>();


            var controller = GetControllerMediaViewer();
            var securityConnector = GetControllerSecurity();

            var userId = User.Identity.GetUserId();
            var userItem = securityConnector.GetUser(userId);
            var groupItem = securityConnector.GetGroup(userId);


            var resultMediaViewerItems = await controller.GetMediaListItems(new List<clsOntologyItem> { refItem }, Server.MapPath($"~/Resources/UserGroupRessources"), Url.Content($"~/Resources/UserGroupRessources"), MediaViewerModule.Primitives.MultimediaItemType.Image);

            LogEndMethod(processId:processId);
            return Json(resultMediaViewerItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceVideoPlayer(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelVideoPlayer(idInstance);

            var result = new ResultSimpleView<ResultMediaListItem>
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath,
                ViewItems = new List<ViewItem>()
            };

            var controller = GetControllerMediaViewer();
            var securityConnector = GetControllerSecurity();

            var userId = User.Identity.GetUserId();
            var userItem = securityConnector.GetUser(userId);
            var groupItem = securityConnector.GetGroup(userId);

            var resultMediaViewerItems = await controller.GetMediaListItems(new List<clsOntologyItem> { refItem }, Server.MapPath($"~/Resources/UserGroupRessources"), Url.Content($"~/Resources/UserGroupRessources"), MediaViewerModule.Primitives.MultimediaItemType.Video);
            result.Result = resultMediaViewerItems;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferencePDFViewer(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPDFViewer(idInstance);

            var resultViewItems = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };


            var controller = GetControllerMediaViewer();
            var securityConnector = GetControllerSecurity();

            var userId = User.Identity.GetUserId();
            var userItem = securityConnector.GetUser(userId);
            var groupItem = securityConnector.GetGroup(userId);


            var resultMediaViewerItems = await controller.GetMediaListItems(new List<clsOntologyItem> { refItem }, Server.MapPath($"~/Resources/UserGroupRessources"), Url.Content($"~/Resources/UserGroupRessources"), MediaViewerModule.Primitives.MultimediaItemType.PDF);
            viewModel.MediaListItems = resultMediaViewerItems.MediaListItems;
            viewModel.IxMediaListItem = 0;

            resultViewItems.ViewItems.Add(viewModel.mediaItemName);
            resultViewItems.ViewItems.Add(viewModel.gotoFirst);
            resultViewItems.ViewItems.Add(viewModel.gotoPrevious);
            resultViewItems.ViewItems.Add(viewModel.gotoNext);
            resultViewItems.ViewItems.Add(viewModel.gotoLast);
            resultViewItems.ViewItems.Add(viewModel.openAutoPDF);
            resultViewItems.ViewItems.Add(viewModel.openPDF);
            resultViewItems.ViewItems.Add(viewModel.countToSave);
            resultViewItems.ViewItems.Add(viewModel.labelNav);
            resultViewItems.ViewItems.Add(viewModel.openPDF);


            ConfigureNavigation(viewModel);

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult InitializeViewItems(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaList(idInstance);

            var resultViewItems = new List<ViewItem>();

            var result = new ResultViewItems()
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };


            result.ViewItems.Add(viewModel.uploadItems);
            result.ViewItems.Add(viewModel.removeFromList);
            result.ViewItems.Add(viewModel.applyItem);
            result.ViewItems.Add(viewModel.openPlayer);
            result.ViewItems.Add(viewModel.isListen);
            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.nameOItem);
            result.ViewItems.Add(viewModel.downloadMediaList);

            viewModel.uploadItems.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.removeFromList.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.applyItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.openPlayer.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.downloadMediaList.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ValidateReferenceMediaList(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaList(idInstance);

            var resultViewItems = new List<ViewItem>();


            var controller = GetControllerMediaViewer();
            var securityConnector = GetControllerSecurity();

            var userId = User.Identity.GetUserId();
            var userItem = securityConnector.GetUser(userId);
            var groupItem = securityConnector.GetGroup(userId);

            var result = new ResultViewItems()
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };


            result.ViewItems.Add(viewModel.uploadItems);
            result.ViewItems.Add(viewModel.removeFromList);
            result.ViewItems.Add(viewModel.applyItem);
            result.ViewItems.Add(viewModel.openPlayer);
            result.ViewItems.Add(viewModel.isListen);
            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.nameOItem);

            var resultRefItem = controller.GetOItem(refItem.GUID, controller.Globals.Type_Object);
            viewModel.ObjectWithParent = resultRefItem.Result;
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.SetWindowTitle($"{viewModel.ObjectWithParent.OItem.Name} ({viewModel.ObjectWithParent.OItemParent.Name})");
            viewModel.nameOItem.ChangeViewItemValue(ViewItemType.Content.ToString(), $"{viewModel.ObjectWithParent.OItem.Name} ({viewModel.ObjectWithParent.OItemParent.Name})");

            if (resultRefItem.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Refitem cannot be loaded", processId: processId);
                result.IsSuccessful = false;
                viewModel.uploadItems.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.removeFromList.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.applyItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.openPlayer.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            }
            else
            {
                viewModel.uploadItems.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.removeFromList.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.applyItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.openPlayer.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.ObjectWithParent = resultRefItem.Result;
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult OpenPDF(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPDFViewer(idInstance);

            var mediaItem = viewModel.MediaListItems[viewModel.IxMediaListItem];

            var result = new ResultSimpleView<string>
            {
                IsSuccessful = true,
                Result = mediaItem.FileUrl
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private void ConfigureNavigation(PDFViewerViewModel viewModel)
        {
            var processId = LogStartMethod();
            if (viewModel.MediaListItems.Any())
            {
                var mediaItem = viewModel.MediaListItems[viewModel.IxMediaListItem];
                viewModel.mediaItemName.ChangeViewItemValue(ViewItemType.Content.ToString(), mediaItem.NameMultimediaItem);
                var countMediaItems = viewModel.MediaListItems.Count;

                if (viewModel.IxMediaListItem > 0)
                {
                    viewModel.gotoPrevious.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                    viewModel.gotoFirst.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                }
                else
                {
                    viewModel.gotoPrevious.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                    viewModel.gotoFirst.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                }

                if (viewModel.IxMediaListItem < countMediaItems - 1)
                {
                    viewModel.gotoNext.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                    viewModel.gotoLast.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                }
                else
                {
                    viewModel.gotoNext.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                    viewModel.gotoLast.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                }

                viewModel.lblCountToSave.ChangeViewItemValue(ViewItemType.Content.ToString(), countMediaItems.ToString());
                viewModel.lblCountSaved.ChangeViewItemValue(ViewItemType.Content.ToString(), countMediaItems.ToString());
                viewModel.labelNav.ChangeViewItemValue(ViewItemType.Content.ToString(), $"{viewModel.IxMediaListItem + 1}/{countMediaItems}");
                viewModel.openPDF.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            }
            else
            {
                viewModel.gotoPrevious.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.gotoFirst.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                viewModel.gotoNext.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                viewModel.gotoLast.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

                viewModel.lblCountToSave.ChangeViewItemValue(ViewItemType.Content.ToString(), "0");
                viewModel.lblCountSaved.ChangeViewItemValue(ViewItemType.Content.ToString(), "0");
                viewModel.labelNav.ChangeViewItemValue(ViewItemType.Content.ToString(), $"0/0");
                viewModel.openPDF.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            }

            LogEndMethod(processId:processId);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueVideoPlayer(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelVideoPlayer(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };


            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValuePDFViewer(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPDFViewer(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueMediaBookmarkManager(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaBookmarkManager(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueAudioPlayer(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelAudioPlayer(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValuePDFSearch(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPDFSearch(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueImageViewer(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelImageViewer(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueMediaList(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaList(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public ActionResult ConnectedImageMap()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelImageMap(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedImageMap()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelImageMap(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyImageMap(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelImageMap(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueImageMap(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelImageMap(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceImageMap(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelImageMap(idInstance);

            var controller = GetControllerImageMap();

            var refItemTask = await controller.GetClassObject(refItem.GUID);


            var result = new ResultSimpleView<ImageMap>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while getting the RefItem", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.ClassObject = refItemTask.Result;

            var request = new MediaViewerModule.Models.GetImageMapRequest(viewModel.ClassObject.ObjectItem.GUID, Server.MapPath($"~/Resources/UserGroupRessources"), Url.Content($"~/Resources/UserGroupRessources"))
            {
                MessageOutput = new OWMessageOutput(viewModel.IdInstance)
            };

            var controllerResult = await controller.GetImageMap(request);
            if (controllerResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while getting the RefItem", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.ImageMapItem = controllerResult.Result;

            result.Result = viewModel.ImageMapItem;
            viewModel.SetWindowTitle(viewModel.ClassObject.ToString());
            result.ViewItems.Add(viewModel.windowTitle);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedMediaBookmarkManager()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelMediaBookmarkManager(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedAudioPlayer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelAudioPlayer(idInstance);

            viewItemList.Add(viewModel.listen);

            viewModel.listen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.listen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedMediaBookmarkManager()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelMediaBookmarkManager(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedAudioPlayer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelAudioPlayer(idInstance);

            viewItemList.Add(viewModel.listen);

            viewModel.listen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.listen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedPDFSearch()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelPDFSearch(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedPDFSearch()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelPDFSearch(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedPDFViewer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelPDFViewer(idInstance);

            viewItemList.Add(viewModel.openAutoPDF);
            viewModel.openAutoPDF.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedPDFViewer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelPDFViewer(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedImageViewer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelImageViewer(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedImageViewer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelImageViewer(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedVideoPlayer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelVideoPlayer(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedVideoPlayer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelVideoPlayer(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedMediaList()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelMediaList(idInstance);

            viewItemList.Add(viewModel.uploadItems);
            viewItemList.Add(viewModel.removeFromList);
            viewItemList.Add(viewModel.applyItem);
            viewItemList.Add(viewModel.openPlayer);
            viewItemList.Add(viewModel.isListen);

            viewModel.uploadItems.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.removeFromList.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.applyItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.openPlayer.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedMediaList()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelMediaList(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyMediaBookmarkManager(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaBookmarkManager(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyAudioPlayer(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelAudioPlayer(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }


        [Authorize, HttpPost]
        public ActionResult SetViewReadyPDFSearch(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPDFSearch(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyVideoPlayer(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelVideoPlayer(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyImageViewer(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelImageViewer(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyPDFViewer(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelPDFViewer(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyMediaList(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelMediaList(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }
    }
}