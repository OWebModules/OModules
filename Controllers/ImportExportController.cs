﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.Results;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Services;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class ImportExportController : AppControllerBase
    {
        

        private OntologyItemsModule.OItemListController GetOItemListController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

          
            var result = new OntologyItemsModule.OItemListController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private OntologyItemsModule.OntologyConnector GetOntologyController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.OntologyConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private ImportExport_Module.AssemblyImporter GetOntologyAssemblyImportController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new ImportExport_Module.AssemblyImporter(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private ExportOntologyViewModel GetViewModelExportOntology(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ExportOntologyViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ExportOntologyViewModel(Session, key, User.Identity.GetUserId())
                {
                    IdSession = HttpContext.Session.SessionID,
                    ActionConnected = Url.Action(nameof(ConnectedExportOntology)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedExportOntology)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyExportOntology)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceExportOntology)),
                    ActionExportOntology = Url.Action(nameof(DownloadOntology))
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueExportOntology)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        public ImportOntologyAssemblyViewModel GetViewModelImportOntologyAssembly(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ImportOntologyAssemblyViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ImportOntologyAssemblyViewModel(Session, key, User.Identity.GetUserId())
                {
                    IdSession = HttpContext.Session.SessionID,
                    ActionConnected = Url.Action(nameof(ConnectedImportOntologyAssembly)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedImportOntologyAssembly)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyImportOntologyAssembly)),
                    ActionUpload = Url.Action(nameof(UploadOntologyAssembly))
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueImportOntologyAssembly)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        public ActionResult ConnectedExportOntology()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelExportOntology(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewModel.outputOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.exportOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.outputOntology);
            viewItemList.Add(viewModel.exportOntology);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        

        [Authorize]
        public ActionResult DisconnectedExportOntology()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelExportOntology(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.outputOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.exportOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.outputOntology);
            viewItemList.Add(viewModel.exportOntology);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedImportOntologyAssembly()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelImportOntologyAssembly(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.fileItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.fileItem);
            viewItemList.Add(viewModel.outputResult);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedImportOntologyAssembly()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelImportOntologyAssembly(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.outputResult.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.fileItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.outputResult);
            viewItemList.Add(viewModel.outputResult);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueExportOntology(ViewItem viewItem)
        {
            var processId = LogStartMethod();
            var idInstance = viewItem.IdViewInstance;
            var viewModel = GetViewModelExportOntology(idInstance);

            var viewItemResult = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewItemResult.ViewItem != null)
            {
                resultAction.ViewItems.Add(viewItemResult.ViewItem);
            }
            resultAction.ViewItems.Add(viewItemResult.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueImportOntologyAssembly(ViewItem viewItem)
        {
            var processId = LogStartMethod();
            var idInstance = viewItem.IdViewInstance;
            var viewModel = GetViewModelImportOntologyAssembly(idInstance);

            var viewItemResult = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewItemResult.ViewItem != null)
            {
                resultAction.ViewItems.Add(viewItemResult.ViewItem);
            }
            resultAction.ViewItems.Add(viewItemResult.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyExportOntology(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelExportOntology(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyImportOntologyAssembly(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelImportOntologyAssembly(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceExportOntology(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultViewItems
            {
                IsSuccessful = true
            };

            var viewModel = GetViewModelExportOntology(idInstance);

            var controller = GetOItemListController();

            var objectTask = await controller.GetOItem(refItem.GUID, controller.Globals.Type_Object);

            viewModel.RefItem = objectTask;

            var classTask = await controller.GetOItem(viewModel.RefItem.GUID_Parent, controller.Globals.Type_Class);

            viewModel.RefItemParent = classTask;

            viewModel.outputOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.exportOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.referenceName.ChangeViewItemValue(ViewItemType.Content.ToString(), $"{objectTask.Name} ({classTask.Name})");
            viewModel.SetWindowTitle($"{objectTask.Name} ({classTask.Name})");
            result.ViewItems = new List<ViewItem>
            {
                viewModel.outputOntology,
                viewModel.exportOntology,
                viewModel.referenceName,
                viewModel.windowTitle
            };

            if (viewModel.RefItem.GUID_Parent == controller.Globals.Class_Ontologies.GUID)
            {
                viewModel.outputOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.exportOntology.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DownloadOntology(string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimple
            {
                IsSuccessful = true
            };
            var viewModel = GetViewModelExportOntology(idInstance);

            var controller = GetOntologyController();

            var request = new ExportOntologyRequest(viewModel.RefItem)
            {
                ExportPath = Server.MapPath($"~/Resources/UserGroupRessources/Ontology/{Guid.NewGuid().ToString()}"),
                ZipFiles = true
            };
            var exportResult = await controller.ExportOntologies(request);

            if (exportResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                return Content("");
            }

            byte[] content;
            using (var stream = new StreamContent(exportResult.ZipStream))
            {
                content = await stream.ReadAsByteArrayAsync();
            }

            Response.AddHeader("Content-Disposition", $"attachment; filename={Path.GetFileName($"{viewModel.RefItem.Name}.zip")}");

            LogEndMethod(processId:processId);
            return File(content, "application/x-zip-compressed");

        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> UploadOntologyAssembly(IEnumerable<HttpPostedFileBase> files, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<List<UploadOntologyAssemblyResult>>
            {
                IsSuccessful = true,
                Result = new List<UploadOntologyAssemblyResult>()
            };
            if (files != null)
            {
                var viewModel = GetViewModelImportOntologyAssembly(idInstance);
                var controller = GetOntologyAssemblyImportController();

                foreach (var fileItem in files)
                {
                    
                    var resultCheck = await controller.ImportAssembly(fileItem.InputStream);
                    
                    var resultImport = new UploadOntologyAssemblyResult
                    {
                        FilePath = fileItem.FileName,
                        CountToDoAttributeTypes = resultCheck.AttributeTypesToImport.Count,
                        CountImportAttributeTypes = resultCheck.AttributeTypesImported.Count,

                        CountToDoRelationTypes = resultCheck.RelationTypesToImport.Count,
                        CountImportRelationTypes = resultCheck.RelationTypesImported.Count,

                        CountToDoClasses = resultCheck.ClassesToImport.Count,
                        CountImportClasses = resultCheck.ClassesImported.Count,


                        CountToDoObjects = resultCheck.ObjectsToImport.Count,
                        CountImportObjects = resultCheck.ObjectsImported.Count,

                        CountToDoClassAttributes = resultCheck.ClassAttsToImport.Count,
                        CountImportClassAttributes = resultCheck.ClassAttsImported.Count,

                        CountToDoClassRelations = resultCheck.ClassRelsToImport.Count,
                        CountImportClassRelations = resultCheck.ClassRelsImported.Count,

                        CountToDoObjectAttributes = resultCheck.ObjAttsToImport.Count,
                        CountImportObjectAttributes = resultCheck.ObjAttsImported.Count,

                        CountToDoObjectRelations = resultCheck.ObjRelsToImport.Count,
                        CountImportObjectRelations = resultCheck.ObjRelsImported.Count,
                    };
                    if (resultCheck.ResultState.GUID == controller.Globals.LState_Error.GUID ||
                        resultCheck.ResultState.GUID == controller.Globals.LState_Nothing.GUID)
                    {
                        resultImport.ErrorMessage = resultCheck.ResultState.Additional1;
                        resultImport.IsOk = false;
                    }
                    result.Result.Add(resultImport);
                    

                }
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: ImportExport
        public ActionResult Index()
        {
            return View();
        }

        // GET: OItemList
        [Authorize]
        public ActionResult ExportOntology()
        {
            var viewModel = GetViewModelExportOntology(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ImportOntologyAssembly()
        {
            var viewModel = GetViewModelImportOntologyAssembly(Guid.NewGuid().ToString());
            return View(viewModel);
        }
    }
}