﻿using Microsoft.AspNet.Identity;
using OItemFilterModule.Models;
using OModules.Models;
using OModules.Services;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class OItemSearchController : AppControllerBase
    {
        

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        private OItemFilterModule.OItemFilterConnector GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            
            var result = new OItemFilterModule.OItemFilterConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private SearchViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();            
            key = $"{nameof(OItemSearchController)}{key}";
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<SearchViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new SearchViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigSearch
                    {

                        ActionConnected = Url.Action(nameof(Connected)),
                        ActionDisconnected = Url.Action(nameof(Disconnected)),
                        ActionSetViewReady = Url.Action(nameof(SetViewReady)),
                        ActionGetSearchRequestItem = Url.Action(nameof(GetSearchRequestItem)),
                        ActionRemoveSearchItemArea = Url.Action(nameof(RemoveSearchItemArea)),
                        ActionSearchItemTextChanged = Url.Action(nameof(SearchItemTextChanged)),
                        ActionGetGridConfig = Url.Action(nameof(GetGridConfig)),
                        ActionAddSearchItemArea = Url.Action(nameof(AddSearchItemArea)),
                        ActionSearchClassChanged = Url.Action(nameof(SearchClassChanged)),
                        ActionSearchRelationTypeChanged = Url.Action(nameof(SearchRelationTypeChanged)),
                        ActionSearchAttributeTypeChanged = Url.Action(nameof(SearchAttributeTypeChanged))
                    }
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        // GET: OItemSearch
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Search()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var viewItemValue = result.ViewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> GetSearchRequestItem(string idInstance, SearchItem searchItem)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            var memoryCache = new InMemoryCache();
            var filterController = GetController();

            var resultTask = await filterController.GetSearchItem(searchItem);
            viewModel.SearchItemList.RemoveAll(srchItm => srchItm.id == searchItem.id);

            viewModel.SearchItemList.Add(resultTask.SearchItem);

            if (resultTask.AttributeTypes != null)
            {
                viewModel.AttributeTypes = resultTask.AttributeTypes;
            }

            if (resultTask.RelationTypes != null)
            {
                viewModel.RelationTypes = resultTask.RelationTypes;
            }

            if (resultTask.Classes != null)
            {
                viewModel.Classes = resultTask.Classes;
            }

            var result = new ResultSearchItem
            {
                IsSuccessful = true,
                SearchItem = resultTask.SearchItem
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [Authorize, HttpPost]
        public ActionResult SelectedItem(string idInstance, string idItem, string value)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            var memoryCache = new InMemoryCache();
            var filterController = GetController();

            var searchItem = viewModel.SearchItemList.FirstOrDefault(searchItm => searchItm.id == idItem);

            LogEndMethod(processId:processId);
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult RemoveSearchItemArea(string idInstance, SearchItem searchItem)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            var searchItemToRemove = viewModel.SearchItemList.FirstOrDefault(srchItm => srchItm.id == searchItem.id);

            if (searchItemToRemove == null)
            {
                searchItemToRemove = searchItem;
            }

            if (viewModel.SearchItemList.Any())
            {
                viewModel.SearchItemList.RemoveAll(srchItm => srchItm.id == searchItemToRemove.id);
                searchItemToRemove.remove = true;
            }
            else
            {
                searchItemToRemove.clear = true;
            }

            LogEndMethod(processId:processId);
            return Json(new ResultSearchItem { IsSuccessful = true, SearchItem = searchItemToRemove });
        }

        [Authorize, HttpPost]
        public ActionResult AddSearchItemArea(string idInstance, SearchItem searchItem)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            searchItem.add = true;
            viewModel.SearchItemList.Add(searchItem);

            LogEndMethod(processId:processId);
            return Json(new ResultSearchItem { IsSuccessful = true, SearchItem = searchItem });
        }

        [Authorize, HttpPost]
        public ActionResult InitSearchItems(string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<SearchItem>
            {
                IsSuccessful = true
            };

            var viewModel = GetViewModel(idInstance);
            
            result.Result = viewModel.SearchItemList.First();

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SearchItemTextChanged(string idInstance, SearchItem searchItem)
        {
            var processId = LogStartMethod();
            var result = new ResultSearchSimpley
            {
                IsSuccessful = true
            };

            if (string.IsNullOrEmpty(searchItem.searchText))
            {
                result.GetData = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var viewModel = GetViewModel(idInstance);
            var searchItemToChange = viewModel.SearchItemList.FirstOrDefault(srchItm => srchItm.id == searchItem.id);

            if (searchItemToChange == null)
            {
                result.IsSuccessful = false;
                return Json(searchItemToChange, JsonRequestBehavior.AllowGet);

            }

            
            searchItemToChange.searchText = searchItem.searchText;
            result.GetData = true;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SearchClassChanged(string idInstance, SearchItem searchItem)
        {
            var processId = LogStartMethod();
            var result = new ResultSearchSimpley
            {
                IsSuccessful = true
            };


            if (string.IsNullOrEmpty(searchItem.searchText) && string.IsNullOrEmpty(searchItem.idClass))
            {
                result.GetData = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var viewModel = GetViewModel(idInstance);
            var searchItemToChange = viewModel.SearchItemList.FirstOrDefault(srchItm => srchItm.id == searchItem.id);

            
            if (searchItemToChange == null)
            {
                result.IsSuccessful = false;
                return Json(searchItemToChange, JsonRequestBehavior.AllowGet);

            }

            
            if (!string.IsNullOrEmpty(searchItem.idClass))
            {
                searchItemToChange.idClass = searchItem.idClass;
                result.GetData = true;
            }

            if (!string.IsNullOrEmpty(searchItem.searchText))
            {
                searchItemToChange.searchText = searchItem.searchText;
                result.GetData = true;
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SearchRelationTypeChanged(string idInstance, SearchItem searchItem)
        {
            var processId = LogStartMethod();
            var result = new ResultSearchSimpley
            {
                IsSuccessful = true
            };

            var viewModel = GetViewModel(idInstance);
            var memoryCache = new InMemoryCache();
            var filterController = GetController();


            if (string.IsNullOrEmpty(searchItem.idRelationType) || !filterController.Globals.is_GUID(searchItem.idRelationType))
            {
                result.GetData = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

 
            var searchItemToChange = viewModel.SearchItemList.FirstOrDefault(srchItm => srchItm.id == searchItem.id);

            if (searchItemToChange == null)
            {
                result.IsSuccessful = false;
                return Json(searchItemToChange, JsonRequestBehavior.AllowGet);

            }
            
            searchItemToChange.idRelationType = searchItem.idRelationType;
            result.GetData = true;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SearchAttributeTypeChanged(string idInstance, SearchItem searchItem)
        {
            var processId = LogStartMethod();
            var result = new ResultSearchSimpley
            {
                IsSuccessful = true
            };

            var viewModel = GetViewModel(idInstance);
            var memoryCache = new InMemoryCache();
            var filterController = GetController();


            if (string.IsNullOrEmpty(searchItem.idAttributeType) || !filterController.Globals.is_GUID(searchItem.idAttributeType))
            {
                result.GetData = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            var searchItemToChange = viewModel.SearchItemList.FirstOrDefault(srchItm => srchItm.id == searchItem.id);

            if (searchItemToChange == null)
            {
                result.IsSuccessful = false;
                return Json(searchItemToChange, JsonRequestBehavior.AllowGet);

            }

            searchItemToChange.idAttributeType = searchItem.idAttributeType;
            result.GetData = true;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult GetGridConfig()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(ResultItem), Url.Action(nameof(GetGridDataRead)), null, null, null, false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetGridDataRead(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            var memoryCache = new InMemoryCache();
            var filterController = GetController();

            var viewItemListTask = await filterController.SearchItems(viewModel.SearchItemList, viewModel.Classes);

            var result = new KendoGridDataResult<ResultItem>(viewItemListTask.Items);
            LogEndMethod(processId:processId);
            return Json(result, "appplication/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        



        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.applyItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.grid.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.grid);
            viewItemList.Add(viewModel.applyItem);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.applyItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.grid.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            viewItemList.Add(viewModel.grid);
            viewItemList.Add(viewModel.applyItem);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }
    }


    public class ResultSearchItem
    {
        public bool IsSuccessful { get; set; }
        public SearchItem SearchItem { get; set; }
    }

    public class ResultSearchSimpley
    {
        public bool IsSuccessful { get; set; }
        public bool GetData { get; set; }

    }
}