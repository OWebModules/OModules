﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.ClassEdit;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class ClassEditController : AppControllerBase
    {

        

        private ClassAttributeViewModel GetViewModelClassAttributeEditor(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ClassAttributeViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ClassAttributeViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedClassAttributeEditor)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedClassAttributeEditor)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyClassAttributeEditor)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceClassAttributeEditor)),
                    ActionClassAttributeEditorInit = Url.Action(nameof(ClassAttributeEditorInit)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfigClassAttributes)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeValueClassAttributeEditor)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private OntologyItemsModule.ClassEditController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            LogEndMethod(processId:processId);
            return new OntologyItemsModule.ClassEditController(globals);

        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeValueClassAttributeEditor(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelClassAttributeEditor(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedClassAttributeEditor()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelClassAttributeEditor(idInstance);

            viewItemList.Add(viewModel.addAttribute);
            viewModel.addAttribute.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);


            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedClassAttributeEditor()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelClassAttributeEditor(idInstance);

            viewItemList.Add(viewModel.addAttribute);
            viewModel.addAttribute.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);


            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyClassAttributeEditor(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelClassAttributeEditor(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceClassAttributeEditor(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelClassAttributeEditor(idInstance);

            var controller = GetController();
            var refItemTask = await controller.GetClassItem(refItem.GUID);
            var resultViewItems = new ResultViewItems();

            resultViewItems.IsSuccessful = refItemTask.ResultState.GUID == controller.Globals.LState_Success.GUID;
            resultViewItems.ResultMessage = refItemTask.ResultState.Additional1;


            if (!resultViewItems.IsSuccessful)
            {
                LogEndMethod(processId:processId);
                return Json(resultViewItems, JsonRequestBehavior.AllowGet);
            }

            viewModel.refItem = refItemTask.Result;
            
            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfigClassAttributes(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelClassAttributeEditor(idInstance);
            var actionResult = new ResultSimpleView<Dictionary<string, object>>
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath
            };

            actionResult.Result = GridFactory.CreateKendoGridConfig(typeof(ClassAttributeViewItem), Url.Action(nameof(GetClassAttributes)), null, null, null, false, false, false);


            LogEndMethod(processId: processId);
            return Json(actionResult, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetClassAttributes()
        {
            var processId = LogStartMethod();
            var instanceId = Request["idInstance"].ToString();
            var idClass = Request.Params["idClass"];

            if (string.IsNullOrEmpty(idClass))
            {
                var resultEmpty = Json(new { data = new List<ClassAttributeViewModel>() }, JsonRequestBehavior.AllowGet);
                return resultEmpty;
            }

            var controller = GetController();

            var resultTask = await controller.GetClassAttributeViewItems(new clsOntologyItem
            {
                GUID = idClass
            });

            var result = Json(new { data = resultTask.Result }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId: processId);
            return result;
        }


        // GET: ClassEdit
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ClassAttributeEditor()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelClassAttributeEditor(Guid.NewGuid().ToString());

            LogEndMethod(processId: processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ClassAttributeEditorInit(string idInstance, List<string> selectorPath)
        {
            var processId = LogStartMethod();
            var resultAction = new ResultSimpleView<ClassAttributeViewModel>
            {
                IsSuccessful = true,
                SelectorPath = selectorPath,
                ViewItems = new List<ViewItem>()
            };

            if (string.IsNullOrEmpty(idInstance))
            {
                idInstance = Guid.NewGuid().ToString();
            }
            
            var viewModel = GetViewModelClassAttributeEditor(idInstance);

            resultAction.ViewItems.Add(viewModel.addAttribute);
            resultAction.ViewItems.Add(viewModel.removeAttribute);

            viewModel.addAttribute.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.removeAttribute.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            viewModel.SelectorPath = selectorPath;
            resultAction.Result = viewModel;

            LogEndMethod(processId: processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }
    }
}