﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.Requests;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OModules.Controllers
{
    public class ClipboardApiController : ApiControllerBase
    {
        

        private OntologyItemsModule.ClipboardController GetController(string idSession)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(idSession, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.ClipboardController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        [Route("ClipboardApi/GetClipboardItems")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetClipboardItems(GetClipboardItemsRequest request)
        {
            var controller = GetController(request.idSession);

            var result = new ResultApi<List<ClipboardItem>>
            {
                IsSuccessful = true,
                Result = new List<ClipboardItem>()
            };

            clsOntologyItem refItem = null;
            //if (!string.IsNullOrEmpty(request.idReference))
            //{
            //    var refItemTask = await controller.GetOItem(request.idReference);

            //    if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            //    {
            //        result.IsSuccessful = false;
            //        result.Message = "Refitem cannot be found!";
            //        return Json(result);
            //    }

            //    refItem = refItemTask.Result.ObjectItem;
            //}
            
            //var resultTask = await controller.GetClipboardItems(refItem);

            //if (resultTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            //{
            //    result.IsSuccessful = false;
            //    result.Message = "Error while reading clipboard items!";

            //    return Json(result);
            //}

            result.Result = new List<ClipboardItem>();
            return Json(result);
        }

        [Route("ClipboardApi/ClearClipboard")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> ClearClipboard(GetClipboardItemsRequest request)
        {
            var controller = GetController(request.idSession);

            var result = new ResultApiSimple
            {
                IsSuccessful = true
            };

            clsOntologyItem refItem = null;
            //if (!string.IsNullOrEmpty(request.idReference))
            //{
            //    var refItemTask = await controller.GetOItem(request.idReference);

            //    if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            //    {
            //        result.IsSuccessful = false;
            //        result.Message = "Refitem cannot be found!";
            //        return Json(result);
            //    }

            //    refItem = refItemTask.Result.ObjectItem;
            //}

            //var resultTask = await controller.ClearClipbord(refItem);

            //if (resultTask.GUID == controller.Globals.LState_Error.GUID)
            //{
            //    result.IsSuccessful = false;
            //    result.Message = "Error while reading clipboard items!";

            //    return Json(result);
            //}

            return Json(result);
        }

        [Route("ClipboardApi/AddClipboardItems")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> AddClipboardItems(AddClipboardItems request)
        {
            var controller = GetController(request.idSession);

            var result = new ResultApiSimple
            {
                IsSuccessful = true
            };

            if (request.OItems == null || !request.OItems.Any())
            {
                result.IsSuccessful = false;
                result.Message = "No OItems provided!";

                return Json(result);
            }

            var resultGetOItems = await controller.GetOItems(request.OItems.Select(obj => obj.GUID).ToList());

            if (resultGetOItems.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Error while getting the reference items!";

                return Json(result);
            }

            var resultAdd = await controller.AddClipboardItems(resultGetOItems.Result);

            if (resultAdd.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Errors while adding clipboard items";
            }
            
            return Json(result);
        }
    }
}