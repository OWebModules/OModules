﻿using LogModule;
using LogModule.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class LogController : AppControllerBase
    {
        

        private LogModule.LogController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new LogModule.LogController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private LogEditorViewModel GetLogEditorViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<LogEditorViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new LogEditorViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedLogEditor)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedLogEditor)),
                    ActionSetViewReady = Url.Action(nameof(SetViewLogEditorReady)),
                    ActionValidateReference = Url.Action(nameof(ValidateLogEditorReference)),
                    ActionNavigationBar = Url.Action(nameof(NavigationBarController.NavigationBar), nameof(NavigationBarController).Replace("Controller", "")),
                    ActionSaveSessionData = Url.Action(nameof(SessionDataController.SaveSessionData), nameof(SessionDataController).Replace("Controller", "")),
                    ActionSetLogItem = Url.Action(nameof(SetLogItem)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeLogEditorViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private LogListViewModel GetLogListViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<LogListViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new LogListViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedLogList)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedLogList)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyLogList)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceLogList)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfigLogList)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueLogList)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private LogFunctionsViewModel GetLogFunctionsViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<LogFunctionsViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new LogFunctionsViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedLogFunctions)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedLogFunctions)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyLogFunctions)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceLogFunctions)),
                    ActionGetDataSourceLogStates = Url.Action(nameof(GetDataSourceLogStates)),
                    ActionLogReference = Url.Action(nameof(ReferenceLog)),
                    ActionLogListInit = Url.Action(nameof(LogListInit)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueLogFunctions)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeLogEditorViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLogEditorViewModel(idInstance);

            var resultViewItems = ViewItemController.SetViewItemValue(viewItem, viewModel);


            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            resultAction.ViewItems = GetInteractionViewItems(idInstance);

            if (resultViewItems.ViewItem == null)
            {
                SetEnableStateOfInteractionViewItems(resultAction.ViewItems, false);
                resultAction.IsSuccessful = false;
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            if (viewModel.refItem == null)
            {
                SetEnableStateOfInteractionViewItems(resultAction.ViewItems, false);
                resultAction.IsSuccessful = false;
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            var controller = GetController();

            if (viewModel.SelectedLogItem == null)
            {
                var logItem = new LogItem();
                var nameValue = viewModel.inpName.GetViewItemValueItem(ViewItemType.Content.ToString())?.Value;
                var idLogStateValue = viewModel.inpLogstate.GetViewItemValueItem(ViewItemType.Id.ToString())?.Value;
                var nameLogStateValue = viewModel.inpLogstate.GetViewItemValueItem(ViewItemType.Content.ToString())?.Value;
                var dateTimeValue = viewModel.timestamp.GetViewItemValueItem(ViewItemType.Content.ToString())?.Value;
                var messageValue = viewModel.inpMessage.GetViewItemValueItem(ViewItemType.Content.ToString())?.Value;
                if (nameValue != null)
                {
                    logItem.NameLogEntry = nameValue.ToString();
                }
                else
                {
                    logItem.NameLogEntry = "";
                }

                if (idLogStateValue != null)
                {
                    logItem.IdLogState = idLogStateValue.ToString();
                }

                if (nameLogStateValue != null)
                {
                    logItem.NameLogState = nameLogStateValue.ToString();
                }

                if (dateTimeValue != null)
                {
                    logItem.DateTimeStamp = (DateTime)dateTimeValue;
                }
                else
                {
                    logItem.DateTimeStamp = DateTime.Now;
                }

                if (messageValue != null)
                {
                    logItem.Message = messageValue.ToString();
                }

                logItem.IdUser = viewModel.userItem.GUID;
                logItem.NameUser = viewModel.userItem.Name;

                

                var resultSave = await controller.SaveLogItem(logItem, viewModel.refItem);
                if (resultSave.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    SetEnableStateOfInteractionViewItems(resultAction.ViewItems, false);
                    return Json(resultAction, JsonRequestBehavior.AllowGet);
                }

                viewModel.SelectedLogItem = logItem;
                viewModel.LogItems = new List<LogItem>
                {
                    logItem
                };

                viewModel.dataItem.ChangeViewItemValue(ViewItemType.Other.ToString(), viewModel.LogItems.Select(logItm => new NavItem { Id = logItm.IdLogEntry, Name = logItm.NameLogEntry }));
                resultAction.ViewItems.Add(viewModel.dataItem);
            }
            else
            {
                if (resultViewItems.ViewItem.ViewItemId == viewModel.inpName.ViewItemId)
                {
                    var value = viewModel.inpName.GetViewItemValueItem(ViewItemType.Content.ToString()).Value.ToString();
                    viewModel.SelectedLogItem.NameLogEntry = value;

                    var resultSave = await controller.SaveLogItem(viewModel.SelectedLogItem, viewModel.refItem);
                    if (resultSave.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        SetEnableStateOfInteractionViewItems(resultAction.ViewItems, false);
                        return Json(resultAction, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (resultViewItems.ViewItem.ViewItemId == viewModel.inpMessage.ViewItemId)
                {
                    var value = viewModel.inpMessage.GetViewItemValueItem(ViewItemType.Content.ToString()).Value.ToString();
                    viewModel.SelectedLogItem.Message = value;

                    var resultSave = await controller.SaveLogItem(viewModel.SelectedLogItem, viewModel.refItem);
                    if (resultSave.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        SetEnableStateOfInteractionViewItems(resultAction.ViewItems, false);
                        return Json(resultAction, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueLogFunctions(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLogEditorViewModel(idInstance);

            var resultViewItems = ViewItemController.SetViewItemValue(viewItem, viewModel);

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueLogList(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLogEditorViewModel(idInstance);

            var resultViewItems = ViewItemController.SetViewItemValue(viewItem, viewModel);

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedLogEditor()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetLogFunctionsViewModel(idInstance);


            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedLogList()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetLogListViewModel(idInstance);

            viewItemList.Add(viewModel.GridItem);

            viewModel.GridItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedLogFunctions()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetLogFunctionsViewModel(idInstance);
            var securityController = GetController();

            viewItemList.Add(viewModel.listen);

            viewModel.listen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.listen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedLogEditor()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetLogEditorViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedLogList()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetLogListViewModel(idInstance);

            viewItemList.Add(viewModel.GridItem);

            viewModel.GridItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedLogFunctions()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetLogFunctionsViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewLogEditorReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLogEditorViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyLogList(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLogListViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyLogFunctions(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLogFunctionsViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetDataSourceLogStates(string idInstance)
        {
            var viewModel = GetLogFunctionsViewModel(idInstance);

            var controller = GetController();

            var dataSource = new KendoDatasource
            {
                pageSize = 21,
                transport = new KendoTransport
                {
                    read = new KendoTransportCRUD
                    {
                        dataType = "json",
                        url = Url.Action(nameof(GetDataLogStates))
                    }
                },
                schema = new KendoDataSourceSchema
                {
                    data = "data",
                    total = "total"
                }
            };

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(dataSource);

            return Content(json, "application/json");
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetDataLogStates()
        {
            var processId = LogStartMethod();
            
            var controller = GetController();

            var controllerResult = await controller.GetLogStates();

            var result = new KendoGridDataResult<LogState>(controllerResult.Result);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ReferenceLog(string idLogState, DateTime dateTimeStamp, string description,  string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            var userId = User.Identity.GetUserId();
            var controller = GetController();

            var viewModel = GetLogFunctionsViewModel(idInstance);

            if (viewModel.refItem == null)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var logEntry = new LogItem
            {
                IdLogState = idLogState,
                DateTimeStamp = dateTimeStamp,
                NameLogEntry = dateTimeStamp.ToString(),
                IdUser = userId,
                Message = description
            };


            var resultRefCreate = await controller.SaveLogItem(logEntry, viewModel.refItem);

            if (resultRefCreate.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateLogEditorReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLogEditorViewModel(idInstance);

            var controller = GetController();

            var userId = User.Identity.GetUserId();

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            result.ViewItems.Add(viewModel.windowTitle);
            var interactionViewItems = GetInteractionViewItems(idInstance);
            result.ViewItems.AddRange(interactionViewItems);

            var userTask = await controller.GetObject(userId);
            if (userTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                SetEnableStateOfInteractionViewItems(interactionViewItems, false);

                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.userItem = userTask.Result;

            var refItemTask = await controller.GetClassObject(refItem.GUID);

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                SetEnableStateOfInteractionViewItems(interactionViewItems, false);

                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.refItem = refItemTask.Result.ObjectItem;
            viewModel.refParentItem = refItemTask.Result.ClassItem;

            var controllerResult = await controller.GetLogItems(viewModel.refItem);

            result.ViewItems.Add(viewModel.dataItem);

            if (controllerResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                SetEnableStateOfInteractionViewItems(interactionViewItems, false);

                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.LogItems = controllerResult.Result;
            viewModel.dataItem.ChangeViewItemValue(ViewItemType.Other.ToString(), viewModel.LogItems.Select(logItem => new NavItem { Id = logItem.IdLogEntry, Name = logItem.NameLogEntry }));



            SetEnableStateOfInteractionViewItems(interactionViewItems, true);

            viewModel.SetWindowTitle($"{ viewModel.refItem.Name } ({viewModel.refParentItem.Name})");

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceLogFunctions(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLogFunctionsViewModel(idInstance);

            var controller = GetController();

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var refItemTask = await controller.GetClassObject(refItem.GUID);

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.refItem = refItemTask.Result.ObjectItem;
            viewModel.refParentItem = refItemTask.Result.ClassItem;

            var controllerResult = await controller.GetLogItems(viewModel.refItem);

            viewModel.SetWindowTitle($"{ viewModel.refItem.Name } ({viewModel.refParentItem.Name})");
            viewModel.referenceName.ChangeViewItemValue(ViewItemType.Content.ToString(), $"{ viewModel.refItem.Name } ({viewModel.refParentItem.Name})");

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.referenceName);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceLogList(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLogListViewModel(idInstance);

            var controller = GetController();

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var refItemTask = await controller.GetClassObject(refItem.GUID);

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.refItem = refItemTask.Result.ObjectItem;
            viewModel.refParentItem = refItemTask.Result.ClassItem;

            var controllerResult = await controller.GetLogItems(viewModel.refItem);

            viewModel.SetWindowTitle($"{ viewModel.refItem.Name } ({viewModel.refParentItem.Name})");
            viewModel.referenceName.ChangeViewItemValue(ViewItemType.Content.ToString(), $"{ viewModel.refItem.Name } ({viewModel.refParentItem.Name})");

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.referenceName);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private List<ViewItem> GetInteractionViewItems(string idInstance)
        {

            var processId = LogStartMethod();
            var viewModel = GetLogEditorViewModel(idInstance);
            var result = new List<ViewItem>();

            result.Add(viewModel.inpName);
            result.Add(viewModel.inpMessage);
            result.Add(viewModel.timestamp);
            result.Add(viewModel.changeLogstate);

            LogEndMethod(processId:processId);            return result;
        }

        private void SetEnableStateOfInteractionViewItems(List<ViewItem> viewItems, bool enable)
        {
            foreach (var viewItem in viewItems)
            {
                viewItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), enable);
            }
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SetLogItem(NavItem currentNavItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLogEditorViewModel(idInstance);

            var controller = GetController();

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            viewModel.SelectedLogItem = viewModel.LogItems.FirstOrDefault(logItm => logItm.IdLogEntry == currentNavItem.Id);

            if (viewModel.SelectedLogItem == null)
            {
                result.IsSuccessful = false;
            }

            result.ViewItems.Add(viewModel.inpName);
            result.ViewItems.Add(viewModel.timestamp);
            result.ViewItems.Add(viewModel.inpLogstate);
            result.ViewItems.Add(viewModel.inpMessage);

            viewModel.inpName.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.SelectedLogItem.NameLogEntry);
            viewModel.timestamp.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.SelectedLogItem.DateTimeStamp);
            viewModel.inpLogstate.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.SelectedLogItem.NameLogState);
            viewModel.inpMessage.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.SelectedLogItem.Message);


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetGridConfigLogList(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLogListViewModel(idInstance);

            var resultGridConfig = GridFactory.CreateKendoGridConfig(typeof(LogItem), Url.Action(nameof(GetGridData)), null, null, null, false, false, false);

            var result = new ResultSimpleView<Dictionary<string, object>>
            {
                IsSuccessful = true,
                Result = resultGridConfig
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetGridData(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLogListViewModel(idInstance);

            var controller = GetController();

            if (viewModel.refItem == null)
            {
                return Json(new List<LogItem>(), JsonRequestBehavior.AllowGet);
            }

            var controllerResult = await controller.GetLogItems(viewModel.refItem);

            if (controllerResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                return Json(new List<LogItem>(), JsonRequestBehavior.AllowGet);
            }

            viewModel.LogItems = controllerResult.Result;

            LogEndMethod(processId:processId);
            var result = new KendoGridDataResult<LogItem>(controllerResult.Result);
            var resultJson = Json(result, JsonRequestBehavior.AllowGet);
            resultJson.MaxJsonLength = int.MaxValue;
            return resultJson;
        }

        // GET: Log
        public ActionResult Index()
        {
            var processId = LogStartMethod();            LogEndMethod(processId:processId);            return View();
        }

        // GET: Log
        [Authorize]
        public ActionResult LogEditor()
        {
            var processId = LogStartMethod();
            var viewModel = GetLogEditorViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);            return View(viewModel);
        }

        // GET: Log
        [Authorize]
        public ActionResult LogFunctions()
        {
            var processId = LogStartMethod();
            var viewModel = GetLogFunctionsViewModel(Guid.NewGuid().ToString());
            LogEndMethod(processId:processId);            return View(viewModel);
        }

        [Authorize]
        public ActionResult LogListInit()
        {
            var processId = LogStartMethod();
            var viewModel = GetLogListViewModel(Guid.NewGuid().ToString());
            LogEndMethod(processId:processId);            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
    }
}