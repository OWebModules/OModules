﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class RelationTreeController : AppControllerBase
    {

        

        private RelationTreeViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel =  memoryCache.GetOrSet<RelationTreeViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new RelationTreeViewModel(Session, Guid.NewGuid().ToString(), User.Identity.GetUserId())
                {
                    IdSession = HttpContext.Session.SessionID,
                    ActionGetObject = Url.Action(nameof(GetObjectItem)),
                    ActionGetTreeConfig = Url.Action(nameof(GetTreeConfig)),
                    ActionGetTreeItemsInit = Url.Action(nameof(GetTreeItemsInit)),
                    ActionGetTreeItemsAttribute = Url.Action(nameof(GetTreeItemsAttribute)),
                    ActionGetTreeItemsLeftRight = Url.Action(nameof(GetTreeItemsRelationLeftRight)),
                    ActionGetTreeItemsRightLeft = Url.Action(nameof(GetTreeItemsRelationRightLeft)),
                    ActionGetTreeItemsLeftRightOther = Url.Action(nameof(GetTreeItemsRelationLeftOther)),
                    ActionGetTreeItemsRightLeftOther = Url.Action(nameof(GetTreeItemsRelationRightOther)),
                    ActionConnected = Url.Action(nameof(Connected)),
                    ActionDisconnected = Url.Action(nameof(Disconnected)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReady)),
                    ActionCheckListenNode = Url.Action(nameof(CheckListenNode)),
                    ActionAttributeEditBool = Url.Action(nameof(EditOItemController.EditBoolAttribute), nameof(EditOItemController).Replace("Controller", "")),
                    ActionAttributeEditDateTime = Url.Action(nameof(EditOItemController.EditDateTimeAttribute), nameof(EditOItemController).Replace("Controller", "")),
                    ActionAttributeEditDouble = Url.Action(nameof(EditOItemController.EditDoubleAttribute), nameof(EditOItemController).Replace("Controller", "")),
                    ActionAttributeEditInt = Url.Action(nameof(EditOItemController.EditLongAttribute), nameof(EditOItemController).Replace("Controller", "")),
                    ActionAttributeEditString = Url.Action(nameof(EditOItemController.EditStringAttribute), nameof(EditOItemController).Replace("Controller", "")),
                    ActionGetAttributeUrl = Url.Action(nameof(GetAttributeEditUrl)),
                    ActionObjectListInit = Url.Action(nameof(OItemListController.ObjectListInit), nameof(OItemListController).Replace("Controller", "")),
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private OntologyItemsModule.OItemListController GetControllerOItems()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.OItemListController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private OntologyItemsModule.RelationTreeController GetControllerRelationTree()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            LogEndMethod(processId:processId);
            return new OntologyItemsModule.RelationTreeController(globals);

        }

        [Authorize]
        // GET: RelationTree
        public ActionResult RelationTree()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        // GET: RelationTree
        public ActionResult RelationTreeInit()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString());

            var resultAction = new ResultSimpleView<RelationTreeViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetObjectItem()
        {
            var processId = LogStartMethod();
            var idItem = Request["idObject"];
            if (string.IsNullOrEmpty(idItem)) return Json(null, JsonRequestBehavior.AllowGet);
            
            var oItemListController = GetControllerOItems();
            var oItemTask = await oItemListController.GetOItem(idItem, oItemListController.Globals.Type_Object);

            if (oItemTask == null) return Json(null, JsonRequestBehavior.AllowGet);
            var oItemClassTask = await oItemListController.GetOItem(oItemTask.GUID_Parent, oItemListController.Globals.Type_Class);


            oItemTask.Additional1 = oItemTask.Name + $" ({ oItemClassTask.Name })";

            LogEndMethod(processId:processId);
            return Json(oItemTask, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> CheckListenNode(clsOntologyItem[] items, string itemType, ObjectRelNode relNode, bool setOrderId, long orderId)
        {
            var processId = LogStartMethod();
            var relationTreeController = GetControllerRelationTree();

            if (!items.Any())
            {
                return Json(relNode, JsonRequestBehavior.AllowGet);
            }

            var objectItemsTask = await relationTreeController.RelateItems(relNode, items, itemType, setOrderId, orderId);

            LogEndMethod(processId:processId);
            return Json(new { item = objectItemsTask.RelationNode }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetTreeConfig()
        {
            var processId = LogStartMethod();
            var treeConfig = new KendoTreeViewConfig
            {
                dataSource = new KendoHierarchicalDataSource
                {
                    schema = new KendoHierarchicalSchema
                    {
                        model = new KendoHierarchicalModel
                        {
                            id = nameof(KendoTreeNode.NodeId),
                            children = nameof(KendoTreeNode.SubNodes),
                            hasChildren = nameof(KendoTreeNode.HasChildren)
                        }
                    },
                    transport = new KendoTransport
                    {
                        read = new KendoTransportCRUD
                        {
                            dataType = "json",
                            url = Url.Action(nameof(GetTreeItemsInit))
                        }
                    }
                },
                dataTextField = nameof(KendoTreeNode.NodeName)
            };

            LogEndMethod(processId:processId);
            return Json(treeConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetTreeConfigSelection()
        {
            var treeConfig = new KendoTreeViewConfig
            {
                dataSource = new KendoHierarchicalDataSource
                {
                    schema = new KendoHierarchicalSchema
                    {
                        model = new KendoHierarchicalModel
                        {
                            id = nameof(KendoTreeNode.NodeId),
                            children = nameof(KendoTreeNode.SubNodes),
                            hasChildren = nameof(KendoTreeNode.HasChildren)
                        }
                    },
                    transport = new KendoTransport
                    {
                        read = new KendoTransportCRUD
                        {
                            dataType = "json",
                            url = Url.Action(nameof(GetSelectionTreeItemsInit))
                        }
                    }
                },
                dataTextField = nameof(KendoTreeNode.NodeName)
            };

            return Json(treeConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetAttributeEditUrl(string idObject, string idAttributeType, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<string>
            {
                IsSuccessful = true
            };

            var viewModel = GetViewModel(idInstance);
            var controller = GetControllerRelationTree();

            var resultGetUrl = await controller.GetUrlEditAttribute(viewModel.ActionAttributeEditBool,
                viewModel.ActionAttributeEditDateTime,
                viewModel.ActionAttributeEditDouble,
                viewModel.ActionAttributeEditInt,
                viewModel.ActionAttributeEditString,
                idObject,
                idAttributeType);

            if (resultGetUrl.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = resultGetUrl.Result;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        
        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetTreeItemsInit(string idObject)
        {
            var processId = LogStartMethod();
            var relationTreeController = GetControllerRelationTree();

            
            var resultTask = await relationTreeController.GetRelationTreeInit();
            var nodesList = resultTask.NodeList;

            if (!string.IsNullOrEmpty(idObject))
            {
                
                var nodeOtherRightLeft = relationTreeController.GetRelationTreeNodeOtherRightLeft();
                var attributes = relationTreeController.GetRelationTreeAttribute(idObject);
                var fullQualifiedLeftRight = relationTreeController.GetRelationTreeFullQualified(idObject, true);
                var fullQualifiedRightLeft = relationTreeController.GetRelationTreeFullQualified(idObject, false);
                var treeOther = relationTreeController.GetRelationTreeOther(idObject, true);

                await Task.WhenAll(nodeOtherRightLeft,
                    attributes,
                    fullQualifiedLeftRight,
                    fullQualifiedRightLeft,
                    treeOther);


                LogInfo("Start GetRelationTreeNodeOtherRightLeft", processId: processId);
                var resultNode = await nodeOtherRightLeft;
                nodesList.AddRange(resultNode.NodeList);
                LogInfo("End GetRelationTreeNodeOtherRightLeft", processId: processId);
                var node = nodesList[0];

                LogInfo("Start GetRelationTreeAttribute", processId: processId);
                var resultTaskAtt = await attributes;
                node.SubNodes.AddRange(resultTaskAtt.NodeList);
                LogInfo("End GetRelationTreeAttribute", processId: processId);

                node = nodesList[1];

                LogInfo("Start GetRelationTreeFullQualified Left-Right", processId: processId);
                var resultTaskLeftRight = await fullQualifiedLeftRight;
                node.SubNodes.AddRange(resultTaskLeftRight.NodeList);
                LogInfo("End GetRelationTreeFullQualified Left-Right", processId: processId);

                node = nodesList[2];

                LogInfo("Start GetRelationTreeFullQualified Right-Left", processId: processId);
                var resultTaskRightLeft = await fullQualifiedRightLeft;
                node.SubNodes.AddRange(resultTaskRightLeft.NodeList);
                LogInfo("End GetRelationTreeFullQualified Right-Left", processId: processId);

                node = nodesList[3];

                LogInfo("Start GetRelationTreeOther Left-Right", processId: processId);
                var resultTaskLeftRightOther = await treeOther;
                node.SubNodes.AddRange(resultTaskLeftRightOther.NodeList);
                LogInfo("End GetRelationTreeOther Left-Right", processId: processId);
            }

            

            var result = Json(nodesList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetSelectionTreeItemsInit(string idClass)
        {
            var relationTreeController = GetControllerRelationTree();


            var resultTask = await relationTreeController.GetRelationTreeInit();
            var nodesList = resultTask.NodeList;

            
            var node = nodesList[0];

            var resultTaskAtt = await relationTreeController.GetRelationTreeAttributeClass(idClass);

            node.SubNodes.AddRange(resultTaskAtt.NodeList);

            node = nodesList[1];
            var resultTaskLeftRight = await relationTreeController.GetRelationTreeFullQualifiedClass(idClass, true);
            node.SubNodes.AddRange(resultTaskLeftRight.NodeList);

            node = nodesList[2];
            var resultTaskRightLeft = await relationTreeController.GetRelationTreeFullQualifiedClass(idClass, false);
            node.SubNodes.AddRange(resultTaskRightLeft.NodeList);

            

            var result = Json(nodesList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetTreeItemsAttribute()
        {
            var processId = LogStartMethod();
            var idObject = Request.Params["idObject"];

            if (string.IsNullOrEmpty(idObject))
            {
                return Json(new List<object>(), JsonRequestBehavior.AllowGet);
            }
            var relationTreeController = GetControllerRelationTree();

            var resultTask = await relationTreeController.GetRelationTreeAttribute(idObject);

            var result = Json(resultTask.NodeList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }
        

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetTreeItemsRelationLeftRight()
        {
            var processId = LogStartMethod();
            var idObject = Request.Params["idObject"];

            if (string.IsNullOrEmpty(idObject))
            {
                return Json(new List<object>(), JsonRequestBehavior.AllowGet);
            }
            var memoryCache = new InMemoryCache();
            var relationTreeController = GetControllerRelationTree();

            var resultTask = await relationTreeController.GetRelationTreeFullQualified(idObject, true);

            var result = Json(resultTask.NodeList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetTreeItemsRelationRightLeft()
        {
            var processId = LogStartMethod();
            var idObject = Request.Params["idObject"];

            if (string.IsNullOrEmpty(idObject))
            {
                return Json(new List<object>(), JsonRequestBehavior.AllowGet);
            }
            var memoryCache = new InMemoryCache();
            var relationTreeController = GetControllerRelationTree();

            var resultTask = await relationTreeController.GetRelationTreeFullQualified(idObject, false);

            var result = Json(resultTask.NodeList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetTreeItemsRelationLeftOther()
        {
            var processId = LogStartMethod();
            var idObject = Request.Params["idObject"];

            if (string.IsNullOrEmpty(idObject))
            {
                return Json(new List<object>(), JsonRequestBehavior.AllowGet);
            }
            var memoryCache = new InMemoryCache();
            var relationTreeController = GetControllerRelationTree();

            var resultTask = await relationTreeController.GetRelationTreeOther(idObject, true);

            var result = Json(resultTask.NodeList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetTreeItemsRelationRightOther()
        {
            var processId = LogStartMethod();
            var idObject = Request.Params["idObject"];

            if (string.IsNullOrEmpty(idObject))
            {
                return Json(new List<object>(), JsonRequestBehavior.AllowGet);
            }
            var memoryCache = new InMemoryCache();
            var relationTreeController = GetControllerRelationTree();

            var resultTask = await relationTreeController.GetRelationTreeOther(idObject, false);

            var result = Json(resultTask.NodeList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }
    }
}