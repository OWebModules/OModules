﻿using BillModule.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.Requests;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OModules.Controllers
{
    public class BillApiController : ApiControllerBase
    {
        


        private async System.Threading.Tasks.Task<BillModule.TransactionConnector> GetController(string sessionId)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(sessionId, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new BillModule.TransactionConnector(globals);
            var initResult = await result.Initialize();
            if (initResult.GUID == result.Globals.LState_Error.GUID)
            {
                throw new Exception(initResult.Additional1);
            }

            LogEndMethod(processId:processId);
            return result;

        }
        
        [Route("BillApi/GetTransactions")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetTransactions(GetTransactoinsRequest request)
        {
            var result = new ResultApi<List<TransactionItem>>() { Result = new List<TransactionItem>() };

            if (request.idMandant == null)
            {
                result.IsSuccessful = false;
                result.Message = "The Mandant is not set!";
            }

            if (request.start == null)
            {
                request.start = DateTime.Now.AddDays(-365);
            }

            

            if (request.end == null)
            {
                request.end = DateTime.Now;
            }

            var dateRange = new DateRange
            {
                Start = request.start.Value,
                End = request.end.Value
            };

            var controller = await GetController(request.idSession);

            var refItemTask = await controller.GetOItem(request.idMandant);

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result);
            }

            if (refItemTask.Result.ClassItem.GUID != controller.ClassFinancialTransaction.GUID &&
                    !controller.BaseConfig.BaseConfigToMandanten.Any(mand => mand.ID_Other == refItemTask.Result.ObjectItem.GUID))
            {
                result.IsSuccessful = false;
                result.Message = "No mandant or no financial transaction!";
                return Json(result);
            }

            var transactionRequest = new GetTransactionsRequest(refItemTask.Result.ObjectItem, dateRange);
            var controllerResult = await controller.GetTransactionItems(transactionRequest);

            if (controllerResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result);
            }
            result.Result = controllerResult.Result;
            return Json(result);
        }

        [Route("BillApi/GetTransactionData")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetTransactionData(GetTransactionDataRequest request)
        {
            var processId = LogStartMethod();
            var controller = await GetController(request.idSession);

            var result = new ResultApi<TransactionItem>
            {
                IsSuccessful = true
            };

            if (string.IsNullOrEmpty(request.idFinancialTransaction))
            {
                result.IsSuccessful = false;
                return Json(result);
            }

            var refItemTask = await controller.GetOItem(request.idFinancialTransaction);

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result);
            }
            
            var classObject = refItemTask.Result;

            if (classObject == null)
            {
                
                LogEndMethod(processId:processId);                result.IsSuccessful = false;
                result.Message = "No financial transaction found!";
                return Json(result);
            }

            if (classObject.ObjectItem.GUID_Parent != controller.ClassFinancialTransaction.GUID)
            {
                LogEndMethod(processId:processId);                result.IsSuccessful = false;
                result.Message = "Reference is no financial transaction!";
                return Json(result);
            }

            var transactionsRequest = new GetTransactionsRequest(classObject.ObjectItem, null, true);
            var resultTask = await controller.GetTransactionItems(transactionsRequest);
            if (resultTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while getting the RefItem");
                LogEndMethod(processId:processId);                result.IsSuccessful = false;
                result.Message = "Error while getting the RefItem";
                return Json(result);
            }

            result.Result = resultTask.Result.FirstOrDefault();

            LogEndMethod(processId:processId);
            return Json(result);
        }

        [Route("BillApi/SaveTransactionItems")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> SaveTransactionItems(SaveTransactionItemRequest request)
        {
            var processId = LogStartMethod();
            var result = new ResultApi<List<TransactionItem>>
            {
                IsSuccessful= true,
                Result = new List<TransactionItem>()
            };

            if (request.TransactionItems == null || !request.TransactionItems.Any())
            {
                result.IsSuccessful = false;
                result.Message = "No transactionitems";
                return Json(result);
            }

            var controller = await GetController(request.idSession);

            foreach (var transactionItem in request.TransactionItems)
            {
                try
                {
                    var resultSave = await controller.SaveTransactionItem(transactionItem);
                    if (resultSave.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        result.IsSuccessful = false;
                        result.Message = "Save of Transactionitems is not possible!";

                        return Json(result);
                    }

                    result.Result.Add(resultSave.Result);
                }
                catch (Exception ex)
                {

                    result.IsSuccessful = false;
                    result.Message = ex.Message;

                    return Json(result);
                }
                

                
            }
            

            LogEndMethod(processId:processId);
            return Json(result);
        }
    }
}