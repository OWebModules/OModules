﻿using Newtonsoft.Json;
using OModules.Models;
using OModules.Models.Requests;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Converter;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class DropDownController : AppControllerBase
    {
        

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        private OntologyItemsModule.DropDownController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.DropDownController(globals);

            LogEndMethod(processId:processId);            
            return result;
            
            
        }

        // GET: DropDown
        [Authorize]
        public ActionResult Index()
        {
            var processId = LogStartMethod();
            LogEndMethod(processId:processId);            
            return View();
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetDropDownConfigForRelationTypes(string idInstance, List<string> selectorPath)
        {

            var processId = LogStartMethod();
            var controller = GetController();

            var resultAction = new ResultSimpleView<KendoDropDownConfig>
            {
                IsSuccessful = true,
                SelectorPath = selectorPath
            };

            var relationTypeResult = await controller.GetDropDownConfigRelationTypes();

            resultAction.IsSuccessful = controller.Globals.LState_Success.GUID == relationTypeResult.ResultState.GUID;
            resultAction.Message = relationTypeResult.ResultState.Additional1;
            resultAction.Result = relationTypeResult.Result;

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetDropDownConfigForClasses(string idInstance)
        {
            var processId = LogStartMethod();
            var controller = GetController();

            var resultDrowDownConfig = await controller.GetDropDownConfigClasses();

            LogEndMethod(processId:processId);
            return Json(resultDrowDownConfig.Result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetDropDownConfigForObjects(string idClass, List<string> selectorPath)
        {
            var processId = LogStartMethod();
            var controller = GetController();

            var resultDrowDownConfig = await controller.GetDropCownConfigObjects(idClass);

            LogEndMethod(processId:processId);
            return Json(new { SelectorPath = selectorPath, DropDownConfig = resultDrowDownConfig.Result }, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetMultiSelectConfig(GetMultiselectConfigRequest request)
        {
            var processId = LogStartMethod();
            var controller = GetController();
            var result = new ResultSimpleView<KendoMultiselectConfig>
            {
                IsSuccessful = true,
                Result = new KendoMultiselectConfig(),
                ViewItems = new List<OntoMsg_Module.Models.ViewItem>(),
                Message = ""
            };

            if (request == null || !request.IsValid(controller.Globals))
            {
                result.IsSuccessful = false;
                result.Message = request.ValidationMessage;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultMultiSelectConfig = await controller.GetMultiselectConfig(request, new CrudActionsRequest
            {
                ActionRead = Url.Action(nameof(GetObjectsForRelation)),
                ActionCreate = Url.Action(nameof(CreateObjectRelations)),
                ActionDelete = Url.Action(nameof(DestroyObjectRelations)),
                ActionUpdate = Url.Action(nameof(UpdateObjectRelations))
            });

            result.IsSuccessful = resultMultiSelectConfig.ResultState.GUID != controller.Globals.LState_Error.GUID;

            if (result.IsSuccessful)
            {
                result.Result = resultMultiSelectConfig.Result;
            }


            LogEndMethod(processId:processId);
            var jsonResult = JsonConvert.SerializeObject(result,
                            Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });

            return Content(jsonResult, "application/json");

        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetObjectsForRelation()
        {
            var processId = LogStartMethod();
            var request = new GetMultiselectConfigRequest
            {
                IdObject = Request.Params[nameof(GetMultiselectConfigRequest.IdObject)],
                IdRelationType = Request.Params[nameof(GetMultiselectConfigRequest.IdRelationType)],
                IdClassOther = Request.Params[nameof(GetMultiselectConfigRequest.IdClassOther)],
                IdDirection = Request.Params[nameof(GetMultiselectConfigRequest.IdDirection)],
            };

            var controller = GetController();

            var getRelationTypes = await controller.GetObjects(request);

            if (getRelationTypes.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                Response.Status = getRelationTypes.ResultState.Additional1;
                return null;
            }

            var resultItem = new
            {
                total = getRelationTypes.Result.Count,
                data = getRelationTypes.Result.Select(obj => new { GUID = obj.GUID, Name = obj.Name}).ToList(),
                pageSize = 10
            };
            //var result = Json(resultItem, JsonRequestBehavior.AllowGet);
            var result = Json(resultItem, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> CreateObjectRelations(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var resultChange = new List<clsOntologyItem>();

            var items = models.Select(model => DictionaryToObjectList.Convert(model)).ToList();


            var result = Json(new { data = resultChange }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> UpdateObjectRelations(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var resultChange = new List<clsOntologyItem>();

            var items = models.Select(model => DictionaryToObjectList.Convert(model)).ToList();


            var result = Json(new { data = resultChange }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DestroyObjectRelations(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            var resultChange = new List<clsOntologyItem>();

            var items = models.Select(model => DictionaryToObjectList.Convert(model)).ToList();


            var result = Json(new { data = resultChange }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }
    }
}