﻿using Elasticsearch.Net;
using Microsoft.AspNet.Identity;
using Nest;
using OModules.Models;
using OModules.Models.MessageOutput;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Models;
using ProcessModule.Models;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class ReportController : AppControllerBase
    {
        

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        public ReportModule.ReportController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new ReportModule.ReportController(globals, true);

            LogEndMethod(processId:processId);
            return result;

        }

        private ReportViewerViewModel GetReportViewerViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ReportViewerViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ReportViewerViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedReportViewer)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedReportViewer)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyReportViewer)),
                    ActionObjectTree = Url.Action(nameof(ObjectTreeController.ObjectTree), nameof(ObjectTreeController).Replace("Controller", "")),
                    ActionReport = Url.Action(nameof(ReportInit)),
                    ActionModuleStarter = Url.Action(nameof(ModuleMgmtController.ModuleStarter), nameof(ModuleMgmtController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeReportViewerViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        public ReportViewModel GetReportViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ReportViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ReportViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedReport)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedReport)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReportReady)),
                    ActionValidateReference = Url.Action(nameof(ValidateReportReference)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfigReport)),
                    ActionGetObjectForSend = Url.Action(nameof(GetObjectForSend)),
                    ActionGetSyncState = Url.Action(nameof(GetSyncState)),
                    ActionSyncReport = Url.Action(nameof(SyncReport)),
                    ActionInitializeViewItemsReport = Url.Action(nameof(InitializeViewItemsReport)),
                    ActionModuleStarter = Url.Action(nameof(ModuleMgmtController.ModuleStarter), nameof(ModuleMgmtController).Replace("Controller", "")),
                    ActionObjectEditor = Url.Action(nameof(ObjectEditController.RelationEditor), nameof(ObjectEditController).Replace("Controller", "")),
                    ActionHtmlDiff = Url.Action(nameof(HtmlEditController.HtmlDiff), nameof(HtmlEditController).Replace("Controller", "")),
                    ActionGetAutoCompletesFilter = Url.Action(nameof(GetAutoCompletesFilter)),
                    ActionSavePattern = Url.Action(nameof(SavePattern)),
                    ActionReportInit = Url.Action(nameof(ReportInit)),
                    ActionUploadReportData = Url.Action(nameof(SessionDataController.UploadReportData), nameof(SessionDataController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeReportViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult InitializeViewItemsReport(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetReportViewModel(idInstance);

            var resultViewItems = new List<ViewItem>();

            var result = new ResultViewItems()
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };


            result.ViewItems.Add(viewModel.isListen);
            result.ViewItems.Add(viewModel.openModuleStarter);
            result.ViewItems.Add(viewModel.openObjectEditor);
            result.ViewItems.Add(viewModel.refreshReport);
            result.ViewItems.Add(viewModel.syncData);
            result.ViewItems.Add(viewModel.applyItem);
            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.saveQuery);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), viewModel.refItem == null);
            viewModel.openModuleStarter.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.openObjectEditor.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.refreshReport.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.applyItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.syncData.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.saveQuery.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeReportViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetReportViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeReportViewerViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetReportViewerViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedReport()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetReportViewModel(idInstance);
            var securityController = GetController();

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedReport()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetReportViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedReportViewer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetReportViewerViewModel(idInstance);
            var securityController = GetController();

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedReportViewer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetReportViewerViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReportReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetReportViewerViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyReportViewer(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetReportViewerViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReportReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetReportViewModel(idInstance);

            var controller = GetController();

            var refItemTask = await controller.GetOItem(refItem.GUID, controller.Globals.Type_Object);

            viewModel.IdAdditionalViewModel = refItem.GUID_Related;
            viewModel.refItem = refItemTask.Result.Clone();

            var resultViewItems = new List<ViewItem>();
            if (viewModel.refItem != null)
            {
                var refItemParent = await controller.GetOItem(viewModel.refItem.GUID_Parent, controller.Globals.Type_Class);
                viewModel.refParentItem = refItemParent.Result;


                viewModel.SetWindowTitle($"{viewModel.refItem.Name} ({viewModel.refParentItem.Name})");
                viewModel.syncData.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                resultViewItems.Add(viewModel.windowTitle);
                resultViewItems.Add(viewModel.syncData);
                resultViewItems.Add(viewModel.isListen);
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            }

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetGridConfigReport(string idInstance, List<AdditionalReportField> additionalFields, string getDataUrl)
        {
            var processId = LogStartMethod();
            var viewModel = GetReportViewModel(idInstance);

            if (additionalFields != null)
            {
                foreach (var additionalField in additionalFields)
                {
                    additionalField.Template = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(additionalField.Template ?? ""));
                }
            }
            
            viewModel.AdditionalFields = additionalFields;

            var controller = GetController();

            var controllerUrl = getDataUrl ?? Url.Action(nameof(GetData));

            var resultGridConfig = await controller.GetReportConfig(viewModel.refItem, controllerUrl, additionalFields);

            var result = new ResultSimpleView<Dictionary<string, object>>
            {
                IsSuccessful = true,
                Result = new Dictionary<string, object>(),
                ViewItems = new List<ViewItem>()
            };
            if (resultGridConfig.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.ReportItem = resultGridConfig.Result.ReportItem;
            viewModel.ReportFields = resultGridConfig.Result.ReportFields;
            viewModel.RowDiff = resultGridConfig.Result.RowDiff;
            viewModel.ReportFilters = resultGridConfig.Result.ReportFilters;
            viewModel.StandardFilter = viewModel.ReportFilters.FirstOrDefault(repF => repF.Standard?.Val_Bit.Value ?? false);
            if (viewModel.StandardFilter != null)
            {
                result.ViewItems.Add(viewModel.inputQuery);
                viewModel.inputQuery.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.StandardFilter.Value.Val_String);
            }

            result.DataItems.Add(viewModel.RowDiff);

            result.Result = resultGridConfig.Result.GridConfig;

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);

            LogEndMethod(processId:processId);
            return Content(json, "application/json");
        }

        [Authorize]
        public ActionResult GetSyncState(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetReportViewModel(idInstance);
            var controller = GetController();

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>
                {
                    viewModel.syncData
                }
            };

            var memoryCache = new InMemoryCache();
            var syncResult = memoryCache.GetOrSet<System.Threading.Tasks.Task<ResultItem<SyncResult>>>("Global", "SyncReport", () =>
            {
                return null;
            });

            if (syncResult == null)
            {
                viewModel.syncData.ChangeViewItemValue(ViewItemType.AddClass.ToString(), "");
                viewModel.syncData.ChangeViewItemValue(ViewItemType.RemoveClass.ToString(), "syncerror");
                viewModel.syncData.ChangeViewItemValue(ViewItemType.RemoveClass.ToString(), "syncing");
                viewModel.syncData.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                LogEndMethod(processId:processId);                
                return Json(result, JsonRequestBehavior.AllowGet);

            }

            if (syncResult.Status == System.Threading.Tasks.TaskStatus.WaitingForActivation ||
                syncResult.Status == System.Threading.Tasks.TaskStatus.Running)
            {
                viewModel.syncData.ChangeViewItemValue(ViewItemType.AddClass.ToString(), "syncing");
                viewModel.syncData.ChangeViewItemValue(ViewItemType.RemoveClass.ToString(), "syncerror");
                viewModel.syncData.ChangeViewItemValue(ViewItemType.Caption.ToString(), "Sync is Running");
                viewModel.syncData.ChangeViewItemValue(ViewItemType.Other.ToString(), true);

            }
            else if (syncResult.Status == System.Threading.Tasks.TaskStatus.RanToCompletion)
            {
                if (syncResult.Result.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    var error = syncResult.Result.ResultState.Additional1;
                    if (string.IsNullOrEmpty(error))
                    {
                        error = "Error while syncing!";
                    }
                    viewModel.syncData.ChangeViewItemValue(ViewItemType.AddClass.ToString(), "syncerror");
                    viewModel.syncData.ChangeViewItemValue(ViewItemType.RemoveClass.ToString(), "syncing");
                    viewModel.syncData.ChangeViewItemValue(ViewItemType.Caption.ToString(), error);
                    viewModel.syncData.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                    var messageOutput = new OWMessageOutput(viewModel.IdInstance);
                    messageOutput.OutputError(error);
                }
                else
                {
                    viewModel.syncData.ChangeViewItemValue(ViewItemType.AddClass.ToString(), "");
                    viewModel.syncData.ChangeViewItemValue(ViewItemType.RemoveClass.ToString(), "syncing");
                    viewModel.syncData.ChangeViewItemValue(ViewItemType.Caption.ToString(), $"Last Sync: {syncResult.Result.Result.ToString()}");
                    viewModel.syncData.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                }
                memoryCache.RemoveKey("Global", "SyncReport");
            }
            else
            {
                var error = "Unspecified error while syncing!";
                viewModel.syncData.ChangeViewItemValue(ViewItemType.AddClass.ToString(), "syncerror");
                viewModel.syncData.ChangeViewItemValue(ViewItemType.RemoveClass.ToString(), "syncing");
                viewModel.syncData.ChangeViewItemValue(ViewItemType.Caption.ToString(), error);
                viewModel.syncData.ChangeViewItemValue(ViewItemType.Other.ToString(), false);
                memoryCache.RemoveKey("Global", "SyncReport");
                var messageOutput = new OWMessageOutput(viewModel.IdInstance);
                messageOutput.OutputError(error);
            }
                

            LogEndMethod(processId:processId);            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> SyncReport(string idReport, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<Dictionary<string, object>>
            {
                IsSuccessful = true,
                Result = new Dictionary<string, object>()
            };

            ReportViewModel viewModel = null;
            if (!string.IsNullOrEmpty(idInstance))
            {
                viewModel = GetReportViewModel(idInstance);
            }
            var controller = GetController();

            var reportOItem = await controller.GetOItem(idReport, controller.Globals.Type_Object);

            if (reportOItem.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var reportItem = await controller.GetReport(reportOItem.Result);
            if (reportItem.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            OWMessageOutput messageOutput = null;
            if (viewModel != null)
            {
                messageOutput = new OWMessageOutput(viewModel.IdInstance);
            }
            
            var controllerResult = Task.Run(() => controller.SyncReport(new SyncReportRequest { ReportItem = reportItem.Result.Report, MessageOutput = messageOutput }));

            var memoryCache = new InMemoryCache();
            var syncResult = memoryCache.GetOrSet<System.Threading.Tasks.Task<ResultItem<SyncResult>>>("Global", "SyncReport", () =>
            {
                return controllerResult;
            });

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetData(string idInstance, string query)
        {
            var processId = LogStartMethod();
            if (!string.IsNullOrEmpty(query))
            {
                query = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(query));
            }
            else
            {
                query = null;
            }

            var viewModel = GetReportViewModel(idInstance);

            var controller = GetController();

            var result = new ResultItem<List<Dictionary<string, object>>>();
            
            try
            {
                result = await controller.LoadData(viewModel.ReportItem, viewModel.ReportFields, additionalFields: viewModel.AdditionalFields, whereClaus: query);

                if (result.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    LogEndMethod(processId: processId);
                    return Json(new List<object>(), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
            }

            LogEndMethod(processId: processId);
            return Json(result.Result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetObjectForSend(List<ObjectForSendRaw> objectsForSend, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetReportViewModel(idInstance);
            var controller = GetController();

            var result = new ResultSimpleView<List<clsOntologyItem>>
            {
                IsSuccessful = true
            };

            var resultObject = await controller.GetObjectsForSend(objectsForSend, viewModel.ReportFields);
            if (resultObject.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = resultObject.Result;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetAutoCompletesFilter(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetReportViewModel(idInstance);
            var controller = GetController();

            var text = Request.Params["query"];

            var result = new AutoCompleteItem();

            if (viewModel.refItem == null || viewModel.refItem.GUID_Parent != controller.ClassReport.GUID)
            {
                result.query = text;
                result.suggestions = new List<AutoCompleteSubItem>();
                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            var getFilterItemsRequest = new GetFilterItemsRequest(FilterRequestType.GetFiltersByReport)
            {
                ReportOItem = viewModel.refItem
            };

            var resultController = await controller.GetAutoCompletes(viewModel.refItem, viewModel.ReportFields, text);
            if (resultController.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result = resultController.Result;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> SavePattern(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetReportViewModel(idInstance);
            var controller = GetController();

            var text = viewModel.inputQuery.GetViewItemValue(ViewItemType.Content.ToString()).ToString();

            var result = new ResultSimple
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath,
            };

            var messageOutput = new OWMessageOutput(viewModel.IdInstance);

            var resultController = await controller.SavePattern(viewModel.refItem, viewModel.ReportFields, text, messageOutput);
            if (resultController.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = !(resultController.GUID == controller.Globals.LState_Error.GUID);
                result.Message = resultController.Additional1;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Message = "Pattern is saved!";

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Report()
        {
            var processId = LogStartMethod();
            var viewModel = GetReportViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ReportInit(string idInstance)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idInstance ))
            {
                idInstance = Guid.NewGuid().ToString();
            }
            var viewModel = GetReportViewModel(idInstance);
            var resultAction = new ResultSimpleView<ReportViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ReportViewer()
        {
            var processId = LogStartMethod();
            var viewModel = GetReportViewerViewModel(Guid.NewGuid().ToString());

            var controller = GetController();

            viewModel.idClassReports = controller.ClassReport.GUID;

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        // GET: Report
        public ActionResult Index()
        {
            return View();
        }
    }
}