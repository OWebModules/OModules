﻿using MailModule.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class MailController : AppControllerBase
    {
        

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        private MailModule.MailConnector GetController()
        {
            var processId = LogStartMethod();            
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new MailModule.MailConnector(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private MediaStore_Module.FileDownloadController GetControllerFileDownload()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new MediaStore_Module.FileDownloadController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private MailListViewModel GetMailListViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<MailListViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new MailListViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedMailList)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedMailList)),
                    ActionSetViewReady = Url.Action(nameof(SetViewMailListReady)),
                    ActionValidateReference = Url.Action(nameof(ValidateMailListReference)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfigMailList)),
                    ActionGetBody = Url.Action(nameof(GetBody)),
                    ActionGetGridConfigAttachments = Url.Action(nameof(GetGridConfigAttachmentList)),
                    ActionDownloadAttachment = Url.Action(nameof(DownloadAttachment)),
                    ActionRelatedMailitem = Url.Action(nameof(RelateMailItem)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeMailListViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeMailListViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetMailListViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedMailList()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetMailListViewModel(idInstance);

            viewItemList.Add(viewModel.listen);

            viewModel.listen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedMailList()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetMailListViewModel(idInstance);

            viewItemList.Add(viewModel.listen);
            viewModel.listen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewMailListReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetMailListViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetGridConfigMailList(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetMailListViewModel(idInstance);

            var controller = GetController();

            var resultGridConfig = GridFactory.CreateKendoGridConfig(typeof(MailModule.Models.MailItem), Url.Action(nameof(GetGridData)), null, null, null, false, false, false);

            var result = new ResultSimpleView<Dictionary<string, object>>
            {
                IsSuccessful = true,
                Result = resultGridConfig
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetGridConfigAttachmentList(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetMailListViewModel(idInstance);

            var controller = GetController();

            var resultGridConfig = GridFactory.CreateKendoGridConfig(typeof(MailModule.Models.AttachmentViewItem), Url.Action(nameof(GetGridDataAttachments)), null, null, null, false, false, false);

            var result = new ResultSimpleView<Dictionary<string, object>>
            {
                IsSuccessful = true,
                Result = resultGridConfig
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateMailListReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetMailListViewModel(idInstance);

            var controller = GetController();

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var refItemTask = await controller.GetOItem(refItem.GUID);

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var connectionResult = await controller.GetConnection(refItemTask.Result.ObjectItem);

            if (connectionResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.MailConnection = connectionResult.Result;

            viewModel.refItem = refItemTask.Result.ObjectItem;
            viewModel.refParentItem = refItemTask.Result.ClassItem;
            
            

            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.grid);

            viewModel.SetWindowTitle($"{ viewModel.refItem.Name } ({viewModel.refParentItem.Name})");
            viewModel.grid.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetBody(string idInstance, string idMessage)
        {
            var processId = LogStartMethod();
            var viewModel = GetMailListViewModel(idInstance);

            var mailItem = viewModel.MailItems.FirstOrDefault(mailItm => mailItm.MessageId == idMessage);

            var result = new ResultSimpleView<string>
            {
                IsSuccessful = true
            };

            if (mailItem == null)
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = mailItem.GetBody();
            
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> RelateMailItem(string idInstance, string idMessage)
        {
            var processId = LogStartMethod();
            var viewModel = GetMailListViewModel(idInstance);

            var mailItem = viewModel.MailItems.FirstOrDefault(mailItm => mailItm.MessageId == idMessage);

            var result = new ResultSimpleView<MailItem>
            {
                IsSuccessful = true
            };

            if (mailItem == null)
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetController();

            var resultRelate = await controller.RelateMailItem(mailItem);

            result.IsSuccessful = resultRelate.GUID == controller.Globals.LState_Success.GUID;
            result.Result = mailItem;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DownloadAttachment(string idInstance, string idContent, string fileName)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idInstance))
            {
                Response.StatusCode = 500;
                return Content("");
            }

            var viewModel = GetMailListViewModel(idInstance);
            var downloadController = GetControllerFileDownload();
            var controller = GetController();

            var getConnectionResult = await controller.GetConnection(viewModel.refItem);

            if (getConnectionResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                return Content("");
            }
            
            var request = new MediaStore_Module.Models.SecStoreDownloadRequest(getConnectionResult.Result.MailStorePath.Name, idContent, Server.MapPath("~/Resources/UserGroupRessources"), fileName, true);

            
            var result = await downloadController.GetSecMediaStoreStream(request);
            if (result.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                Response.StatusDescription = result.ResultState.Additional1;
                return Content("");
            }

            byte[] content;
            using (var stream = new StreamContent(result.Result.AttachmentStream))
            {
                content = await stream.ReadAsByteArrayAsync();
            }

            Response.AddHeader("Content-Disposition", $"attachment; filename={fileName}");

            LogEndMethod(processId:processId);
            return File(content, "application/x-zip-compressed");

        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetGridData(string idInstance, DateTime? start, DateTime? end, string searchText)
        {
            var processId = LogStartMethod();
            var viewModel = GetMailListViewModel(idInstance);

            if (start == null)
            {
                start = DateTime.Now.AddDays(-90);
            }


            if (end == null)
            {
                end = DateTime.Now;
            }

            var dateRange = new DateRange
            {
                Start = start.Value,
                End = end.Value
            };

            var controller = GetController();

            if (viewModel.refItem == null || viewModel.MailConnection == null || viewModel.MailConnection.ElasticsearchConfig == null)
            {
                return Json(new List<MailItem>(), JsonRequestBehavior.AllowGet);
            }

            var controllerResult = await controller.GetMailItems(viewModel.MailConnection, dateRange, searchText);

            if (controllerResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                return Json(new List<MailItem>(), JsonRequestBehavior.AllowGet);
            }

            viewModel.MailItems = controllerResult.Result;

            LogEndMethod(processId:processId);
            var result = new KendoGridDataResult<MailItem>(controllerResult.Result);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetGridDataAttachments(string idInstance, string messageId)
        {
            var processId = LogStartMethod();
            var viewModel = GetMailListViewModel(idInstance);

            if (viewModel.MailItems == null || !viewModel.MailItems.Any())
            {
                var resultEmpty = new KendoGridDataResult<MailItem>(new List<MailItem>());

                LogEndMethod(processId:processId);
                return Json(resultEmpty, JsonRequestBehavior.AllowGet);
            }

            var mailItem = viewModel.MailItems.FirstOrDefault(mailItm => mailItm.MessageId == messageId);

            if (mailItem == null)
            {
                var resultEmpty = new KendoGridDataResult<MailItem>(new List<MailItem>());
                
                LogEndMethod(processId:processId);
                return Json(resultEmpty, JsonRequestBehavior.AllowGet);
            }

            LogEndMethod(processId:processId);
            var result = new KendoGridDataResult<AttachmentViewItem>(mailItem.GetAttachmentList());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: Mail
        public ActionResult Index()
        {
            var processId = LogStartMethod();            LogEndMethod(processId:processId);
            return View();
        }

        [Authorize]
        public ActionResult MailList()
        {
            var processId = LogStartMethod();            
            var viewModel = GetMailListViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }
    }
}