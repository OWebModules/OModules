﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class BatchinsertController : AppControllerBase
    {
        private OntologyItemsModule.ObjectInsertController GetControllerObjectInsert()
        {
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });
            return new OntologyItemsModule.ObjectInsertController(globals);


        }

        private ObjectInsertViewModel GetObjectInsertViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ObjectInsertViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ObjectInsertViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedObjectInsert)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedObjectInsert)),
                    ActionSetViewReady = Url.Action(nameof(SetViewObjectInsertReady)),
                    ActionValidateReference = Url.Action(nameof(ValidateObjectInsertReference)),
                    ActionGetClassDropDownConfig = Url.Action(nameof(DropDownController.GetDropDownConfigForClasses), nameof(DropDownController).Replace("Controller", "")),
                    ActionGetTreeConfigSelection = Url.Action(nameof(RelationTreeController.GetTreeConfigSelection), nameof(RelationTreeController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeObjectInsertViewItemValue)));
                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        public ActionResult ConnectedObjectInsert()
        {
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetObjectInsertViewModel(idInstance);


            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewObjectInsertReady(string idInstance)
        {
            var viewModel = GetObjectInsertViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedObjectInsert()
        {
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetObjectInsertViewModel(idInstance);


            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeObjectInsertViewItemValue(ViewItem viewItem, string idInstance)
        {
            var viewModel = GetObjectInsertViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateObjectInsertReference(string idRefItem, string content, string idInstance)
        {

            var viewModel = GetObjectInsertViewModel(idInstance);

            var controller = GetControllerObjectInsert();

            var refItemTask = await controller.GetOItem(idRefItem);

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>
                {
                    viewModel.windowTitle,
                    viewModel.inpClass,
                }
            };

            if (refItemTask == null)
            {
                viewModel.inpClass.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                
                return Json(new ResultAutoRelator { IsOk = false }, JsonRequestBehavior.AllowGet);
            }

            viewModel.refItem = refItemTask.Result;




            viewModel.SetWindowTitle($"{viewModel.refItem.ObjectItem.Name} ({viewModel.refItem.ClassItem.Name})");
            

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ObjectInsert()
        {
            var viewModel = GetObjectInsertViewModel(Guid.NewGuid().ToString());
            return View(viewModel);
        }

        // GET: Batchinsert
        public ActionResult Index()
        {
            return View();
        }
    }
}