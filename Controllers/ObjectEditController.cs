﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.MessageOutput;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static OModules.Models.ObjectEditViewModel;

namespace OModules.Controllers
{
    public class ObjectEditController : AppControllerBase
    {
        

        private clsLogStates logStates = new clsLogStates();
        private clsTypes types = new clsTypes();

        private OntologyItemsModule.OItemListController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });


            var result = new OntologyItemsModule.OItemListController(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private OntologyItemsModule.ObjectTreeController GetObjectTreeController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.ObjectTreeController(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private OntologyItemsModule.OItemListController GetControllerOItems()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.OItemListController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private ObjectEditViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ObjectEditViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ObjectEditViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigObjectEdit
                    {
                        ActionConnected = Url.Action(nameof(Connected)),
                        ActionDisconnected = Url.Action(nameof(Disconnected)),

                    },
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.ActionConfig.ActionObjectAttribute = Url.Action(nameof(OItemListController.ObjectAttributeList), "OItemList", paramItems);
                viewModelNew.ActionConfig.ActionObjectLeftRight = Url.Action(nameof(OItemListController.ObjectRelationLeftRightList), "OItemList", paramItems);
                viewModelNew.ActionConfig.ActionObjectRightLeft = Url.Action(nameof(OItemListController.ObjectRelationRightLeftList), "OItemList", paramItems);
                viewModelNew.ActionConfig.ActionObjectLeftRightOther = Url.Action(nameof(OItemListController.ObjectRelationLeftRightOtherList), "OItemList", paramItems);
                viewModelNew.ActionConfig.ActionObjectRightLeftOther = Url.Action(nameof(OItemListController.ObjectRelationRightLeftOtherList), "OItemList", paramItems);
                viewModelNew.ActionConfig.ActionObjectOmni = Url.Action(nameof(OItemListController.ObjectRelationOmniList), "OItemList", paramItems);
                viewModelNew.ActionConfig.ActionRelationTree = Url.Action("RelationTree", "RelationTree", paramItems);
                viewModelNew.ActionConfig.ActionValidateReference = Url.Action(nameof(ValidateReference));
                viewModelNew.ActionConfig.ActionGetTreeConfigRelationTree = Url.Action(nameof(RelationTreeController.GetTreeConfig), nameof(RelationTreeController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionGetAttributeUrlRelationTree = Url.Action(nameof(RelationTreeController.GetAttributeEditUrl), nameof(RelationTreeController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionCheckListenNode = Url.Action(nameof(RelationTreeController.CheckListenNode), nameof(RelationTreeController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionGetTreeItemsRightLeftOtherRelationTree = Url.Action(nameof(RelationTreeController.GetTreeItemsRelationRightOther), nameof(RelationTreeController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionRelationEditor = Url.Action(nameof(ObjectEditController.RelationEditor),
                    nameof(ObjectEditController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionModuleStarter = Url.Action(nameof(ModuleMgmtController.ModuleStarter),
                    nameof(ModuleMgmtController).Replace("Controller", ""));

                viewModelNew.ActionConfig.ActionObjectAttributeListInit = Url.Action(nameof(OItemListController.ObjectAttributeListInit), nameof(OItemListController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionGetGridConfigObjectRelationsAttribute = Url.Action(nameof(OItemListController.GetGridConfigObjectAttributes), nameof(OItemListController).Replace("Controller", ""));

                viewModelNew.ActionConfig.ActionComponentObjectRelationLeftRightList = Url.Action(nameof(OItemListController.ComponentObjectRelationLeftRightList), nameof(OItemListController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionGetGridConfigObjectRelationsLeftRight = Url.Action(nameof(OItemListController.GetGridConfigObjectRelationsLeftRight), nameof(OItemListController).Replace("Controller", ""));

                viewModelNew.ActionConfig.ActionComponentObjectRelationRightLeftList = Url.Action(nameof(OItemListController.ComponentObjectRelationRightLeftList), nameof(OItemListController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionGetGridConfigObjectRelationsRightLeft = Url.Action(nameof(OItemListController.GetGridConfigObjectRelationsRightLeft), nameof(OItemListController).Replace("Controller", ""));

                viewModelNew.ActionConfig.ActionComponentObjectRelationLeftRightListOther = Url.Action(nameof(OItemListController.ComponentObjectRelationLeftRightOtherList), nameof(OItemListController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionGetGridConfigObjectRelationsLeftRightListOther = Url.Action(nameof(OItemListController.GetGridConfigObjectRelationsLeftRightOther), nameof(OItemListController).Replace("Controller", ""));

                viewModelNew.ActionConfig.ActionComponentObjectRelationRightLeftListOther = Url.Action(nameof(OItemListController.ComponentObjectRelationRightLeftOtherList), nameof(OItemListController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionGetGridConfigObjectRelationsRightLeftListOther = Url.Action(nameof(OItemListController.GetGridConfigObjectRelationsRightLeftOther), nameof(OItemListController).Replace("Controller", ""));

                viewModelNew.ActionConfig.ActionComponentObjectRelationOmni = Url.Action(nameof(OItemListController.ComponentObjectRelationOmniList), nameof(OItemListController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionGetGridConfigObjectRelationsOmni = Url.Action(nameof(OItemListController.GetGridConfigObjectRelationsOmni), nameof(OItemListController).Replace("Controller", ""));

                viewModelNew.ActionConfig.ActionSetSelectedItem = Url.Action(nameof(OItemListController.SetCurrentItem), nameof(OItemListController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionSetOrderId = Url.Action(nameof(OItemListController.SetOrderId), nameof(OItemListController).Replace("Controller", ""));

                viewModelNew.ActionConfig.ActionAddToClipboard = Url.Action(nameof(ClipboardController.AddClipboardItems), nameof(ClipboardController).Replace("Controller", ""));

                viewModelNew.ActionConfig.ActionRelationTreeInit = Url.Action(nameof(RelationTreeController.RelationTreeInit), nameof(RelationTreeController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionClipboardInit = Url.Action(nameof(ClipboardController.ClipboardInit), nameof(ClipboardController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionEmbeddedNameEditInit = Url.Action(nameof(ObjectEditController.EmbeddedNameEditInit), nameof(ObjectEditController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionHasRelations = Url.Action(nameof(OItemListController.HasRelations), nameof(OItemListController).Replace("Controller", ""));
                viewModelNew.ActionConfig.ActionDeleteObjects = Url.Action(nameof(DeleteObjects));

                viewModelNew.messageObjectOmni.SessionId = viewModelNew.IdSession;
                viewModelNew.messageObjectRelationTree.SessionId = viewModelNew.IdSession;
                viewModelNew.messageRelationNodeAttributes.SessionId = viewModelNew.IdSession;
                viewModelNew.messageRelationNodeLeftRight.SessionId = viewModelNew.IdSession;
                viewModelNew.messageRelationNodeLeftRightOther.SessionId = viewModelNew.IdSession;
                viewModelNew.messageRelationNodeRightLeft.SessionId = viewModelNew.IdSession;
                viewModelNew.messageRelationNodeRightLeftOther.SessionId = viewModelNew.IdSession;

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }


        private ObjectRelationTreeViewModel GetViewModelObjectRelationTree(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ObjectRelationTreeViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ObjectRelationTreeViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionInit = Url.Action(nameof(ObjectRelationTreeInit)),
                    ActionConnected = Url.Action(nameof(ConnectedObjectRelationTree)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedObjectRelationTree)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyObjectRelationTree)),
                    ActionGetTreeConfig = Url.Action(nameof(GetTreeConfigObjectRelationTree)),
                    ActionClipboardInit = Url.Action(nameof(ClipboardController.ClipboardInit), nameof(ClipboardController).Replace("Controller", "")),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceObjectRelationTree)),
                    ActionObjectListInit = Url.Action(nameof(OItemListController.ObjectListInit), nameof(OItemListController).Replace("Controller", "")),
                    ActionRelateItems = Url.Action(nameof(RelateItems)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueObjectRelationTree)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private EmbeddedNameEditViewModel GetViewModelEmbeddedNameEdit(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<EmbeddedNameEditViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new EmbeddedNameEditViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedEmbeddedNameEdit)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedEmbeddedNameEdit)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyEmbeddedNameEdit)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceEmbeddedNameEdit)),
                    ActionCheckName = Url.Action(nameof(CheckNameOfOItem)),
                    ActionSaveName = Url.Action(nameof(SaveNameOfOItem)),

                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueEmbeddedNameEdit)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

            return viewModel;
        }

        private ItemComBarViewModel GetViewModelItemComBar(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ItemComBarViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ItemComBarViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedItemComBar)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedItemComBar)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyItemComBar)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceItemComBar)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueItemComBar)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private SingleRelationViewModel GetViewModelSingleRelation(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<SingleRelationViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new SingleRelationViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedSingleRelation)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedSingleRelation)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadySingleRelation)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceSingleRelation)),
                    ActionInitializeViewItems = Url.Action(nameof(InitializeViewItemsSingleRelation)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueSingleRelation)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }



        // GET: ObjectRelations
        [Authorize]
        public ActionResult RelationEditor()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ObjectRelationTree()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelObjectRelationTree(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ObjectRelationTreeInit(string idInstance, List<string> selectorPath)
        {
            var processId = LogStartMethod();

            var resultAction = new ResultSimpleView<ObjectRelationTreeViewModel>
            {
                IsSuccessful = true,
                SelectorPath = selectorPath
            };

            if (string.IsNullOrEmpty(idInstance))
            {
                idInstance = Guid.NewGuid().ToString();
            }
            var viewModel = GetViewModelObjectRelationTree(idInstance);
            viewModel.SelectorPath = selectorPath;
            resultAction.Result = viewModel;

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        // GET: ObjectRelations
        [Authorize]
        public ActionResult SingleRelationInit()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelSingleRelation(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> EmbeddedNameEditInit(string idViewModel, List<string> selectorPath, string idItem, string typeItem)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEmbeddedNameEdit(idViewModel ?? Guid.NewGuid().ToString());
            var controller = GetController();
            viewModel.SelectorPath = selectorPath ?? new List<string>();
            if (string.IsNullOrEmpty(idItem) || string.IsNullOrEmpty(typeItem))
            {
                LogEndMethod(processId: processId);
                return Json(viewModel, JsonRequestBehavior.AllowGet);
            }
                
            var oItem = await controller.GetOItem(idItem, typeItem);
            viewModel.RefItem = oItem;

            LogEndMethod(processId: processId);
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> DeleteObjects(string idViewModel, List<string> idObjects)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idViewModel ?? Guid.NewGuid().ToString());
            var controller = GetControllerOItems();

            var result = new ResultSimple
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath
            };

            var deleteObjectsAndRelationsRequest = new DeleteObjectsAndRelationsRequest
            {
                ObjectsToDelete = idObjects.Select(id => new clsOntologyItem
                {
                    GUID = id
                }).ToList()
            };
            var deleteResult = await controller.DeleteObjectsAndRelations(deleteObjectsAndRelationsRequest);
            if (deleteResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = deleteResult.ResultState.Additional1;
                LogEndMethod(processId:processId);                
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            LogEndMethod(processId:processId);            
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> ItemComBarInit(string idViewModel, List<string> selectorPath, string idItem, string typeItem)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelItemComBar(idViewModel ?? Guid.NewGuid().ToString());
            var controller = GetController();
            viewModel.SelectorPath = selectorPath ?? new List<string>();
            var oItem = await controller.GetOItem(idItem, typeItem);
            viewModel.RefItem = oItem;

            LogEndMethod(processId:processId);
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult InitializeViewItemsSingleRelation(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelSingleRelation(idInstance);

            var resultViewItems = new List<ViewItem>();

            var result = new ResultViewItems()
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };


            result.ViewItems.Add(viewModel.relationButton);
            
            viewModel.relationButton.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var controller = GetController();

            var refItemTask = await controller.GetOItem(refItem.GUID, types.ObjectType);

            var classPathTask = await controller.GetClassPath(refItemTask);

            var resultViewItems = new List<ViewItem>();
            viewModel.guidInput.ChangeViewItemValue(ViewItemType.Content.ToString(), refItemTask.GUID);
            viewModel.nameInput.ChangeViewItemValue(ViewItemType.Content.ToString(), refItemTask.Name);
            viewModel.SetWindowTitle($"{ refItemTask.Name } ({classPathTask.Path})");

            resultViewItems.Add(viewModel.guidInput);
            resultViewItems.Add(viewModel.nameInput);
            resultViewItems.Add(viewModel.windowTitle);

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceSingleRelation(clsOntologyItem refItem, string idRelationType, string idDirection, string idParentRelation, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelSingleRelation(idInstance);

            var controller = GetController();

            var resultViewItems = new List<ViewItem>();
            if (refItem == null || string.IsNullOrEmpty(idRelationType) || string.IsNullOrEmpty(idDirection) || string.IsNullOrEmpty(idParentRelation))
            {
                return Json(resultViewItems, JsonRequestBehavior.AllowGet);
            }

            var refItemTask = await controller.GetOItem(refItem.GUID, types.ObjectType);
            var refItemParentTask = await controller.GetOItem(refItemTask.GUID_Parent, types.ClassType);

            viewModel.Reference = refItemTask;
            viewModel.ReferenceParent = refItemParentTask;
            var relationTask = await controller.GetRelation(new clsObjectRel
            {
                ID_Object = idDirection == controller.Globals.Direction_LeftRight.GUID ? viewModel.Reference.GUID : null,
                ID_Other = idDirection != controller.Globals.Direction_LeftRight.GUID ? viewModel.Reference.GUID : null,
                ID_RelationType = idRelationType,
                ID_Parent_Other = idDirection == controller.Globals.Direction_LeftRight.GUID ? idParentRelation : null,
                ID_Parent_Object = idDirection != controller.Globals.Direction_LeftRight.GUID ? idParentRelation : null
            });

            if (relationTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                return Content("");
            }
            viewModel.Relation = relationTask.Result;
            
            viewModel.SetWindowTitle($"{ refItemTask.Name } ({refItemParentTask.Name})");
            if (viewModel.Relation != null)
            {
                var toolTip = idDirection == controller.Globals.Direction_LeftRight.GUID ? $"{viewModel.Relation.Name_Other} ({viewModel.Relation.Name_Parent_Other})" : $"{viewModel.Relation.Name_Object} ({viewModel.Relation.Name_Parent_Object})";

                viewModel.relationButton.ChangeViewItemValue(ViewItemType.Tooltip.ToString(), toolTip);
                viewModel.relationButton.ChangeViewItemValue(ViewItemType.AddClass.ToString(), "fa fa-link");
                viewModel.relationButton.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            }
            else
            {
                viewModel.relationButton.ChangeViewItemValue(ViewItemType.AddClass.ToString(), "fa fa-chain-broken");
                viewModel.relationButton.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            }
            viewModel.relationButton.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            resultViewItems.Add(viewModel.windowTitle);
            resultViewItems.Add(viewModel.relationButton);

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> RelateItems(string idShowObject, List<clsOntologyItem> items, string idInstance)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idInstance))
            {
                return Json(new List<KendoTreeNode>(), JsonRequestBehavior.AllowGet);
            }
            var viewModel = GetViewModelObjectRelationTree(idInstance);

            var controller = GetController();

            var treeNode = viewModel.TreeNodes.Select(node => node.GetTreeNode(idShowObject)).FirstOrDefault();

            var actionResult = new ResultSimpleView<List<KendoTreeNode>>
            {
                IsSuccessful = true,
                Result = new List<KendoTreeNode>()
            };

            var result = Json(actionResult, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceObjectRelationTree(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelObjectRelationTree(idInstance);

            var controller = GetController();

            var refItemTask = await controller.GetOItem(refItem.GUID, types.ObjectType);

            viewModel.RefItem = refItemTask;

            var classPathTask = await controller.GetClassPath(refItemTask);

            var resultViewItems = new List<ViewItem>();
            viewModel.SetWindowTitle($"{ refItemTask.Name } ({classPathTask.Path})");

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        
        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceEmbeddedNameEdit(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEmbeddedNameEdit(idInstance);

            var controller = GetController();

            var refItemTask = await controller.GetOItem(refItem.GUID, refItem.Type);

            viewModel.RefItem = refItemTask;

            var resultViewItems = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                SelectorPath = viewModel.SelectorPath
            };

            resultViewItems.ViewItems.Add(viewModel.guid);
            resultViewItems.ViewItems.Add(viewModel.name);
            resultViewItems.ViewItems.Add(viewModel.saveName);

            viewModel.guid.ChangeViewItemValue(ViewItemType.Content.ToString(), refItemTask.GUID);
            viewModel.name.ChangeViewItemValue(ViewItemType.Content.ToString(), refItemTask.Name);
            viewModel.saveName.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceItemComBar(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelItemComBar(idInstance);

            var resultViewItems = new ResultSimpleView<clsOntologyItem>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                SelectorPath = viewModel.SelectorPath
            };

            if (refItem != null)
            {
                var controller = GetController();

                var refItemTask = await controller.GetOItem(refItem.GUID, refItem.Type);

                viewModel.RefItem = refItemTask;

                var refItemParentTask = await controller.GetOItem(viewModel.RefItem.GUID_Parent, controller.Globals.Type_Class);

                var objectName = $"{refItemTask.Name} ({refItemParentTask.Name})";
                var classPathTask = await controller.GetClassPath(refItemParentTask);

                var objectPath = $"{refItemTask.Name}: {classPathTask.Path}{refItemParentTask.Name}";
                viewModel.RefItem.Additional1 = objectName;
                viewModel.RefItem.Additional2 = objectPath;
                viewModel.guid.ChangeViewItemValue(ViewItemType.Content.ToString(), refItemTask.GUID);
                resultViewItems.Result = viewModel.RefItem;
                resultViewItems.ViewItems.Add(viewModel.guid);
            }
            else
            {
                viewModel.RefItem = null;
            }
            
            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> CheckNameOfOItem(string name, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEmbeddedNameEdit(idInstance);

            var controller = GetController();

            var resultViewItems = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveNameOfOItem(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEmbeddedNameEdit(idInstance);

            var controller = GetController();

            var resultViewItems = new ResultSimpleView<clsOntologyItem>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                Result = refItem
            };

            var saveResult = await controller.SaveNewItems(new List<clsOntologyItem> { refItem });
            resultViewItems.IsSuccessful = saveResult.ResultState.GUID == controller.Globals.LState_Success.GUID;

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueSingleRelation(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelSingleRelation(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueObjectRelationTree(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelObjectRelationTree(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueEmbeddedNameEdit(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEmbeddedNameEdit(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueItemComBar(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelItemComBar(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.toolbar.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewModel.objectRelation.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.tabStrip.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewModel.pageAttributes.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.pageLeftRight.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.pageRightLeft.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.pageLeftRightOther.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.pageRightLeftOther.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.pageOmni.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.toolbar);
            viewItemList.Add(viewModel.isListen);

            viewItemList.Add(viewModel.objectRelation);
            viewItemList.Add(viewModel.tabStrip);

            viewItemList.Add(viewModel.pageAttributes);
            viewItemList.Add(viewModel.pageLeftRight);
            viewItemList.Add(viewModel.pageRightLeft);
            viewItemList.Add(viewModel.pageLeftRightOther);
            viewItemList.Add(viewModel.pageRightLeftOther);
            viewItemList.Add(viewModel.pageOmni);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedObjectRelationTree()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelObjectRelationTree(idInstance);

            viewModel.treeToolbar.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewModel.objectTreeView.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.treeToolbar);
            viewItemList.Add(viewModel.isListen);

            viewItemList.Add(viewModel.objectTreeView);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedEmbeddedNameEdit()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelEmbeddedNameEdit(idInstance);

            viewModel.toolbar.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.toolbar);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedItemComBar()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelItemComBar(idInstance);

            viewModel.toolbar.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.toolbar);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedEmbeddedNameEdit()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelEmbeddedNameEdit(idInstance);

            viewModel.toolbar.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            viewItemList.Add(viewModel.toolbar);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedItemComBar()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelItemComBar(idInstance);

            viewModel.toolbar.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            viewItemList.Add(viewModel.toolbar);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedSingleRelation()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelSingleRelation(idInstance);

            viewModel.relationButton.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);


            viewItemList.Add(viewModel.relationButton);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.objectRelation.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.tabStrip.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewModel.pageAttributes.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.pageLeftRight.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.pageRightLeft.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.pageLeftRightOther.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.pageRightLeftOther.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.pageOmni.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            viewItemList.Add(viewModel.objectRelation);
            viewItemList.Add(viewModel.tabStrip);

            viewItemList.Add(viewModel.pageAttributes);
            viewItemList.Add(viewModel.pageLeftRight);
            viewItemList.Add(viewModel.pageRightLeft);
            viewItemList.Add(viewModel.pageLeftRightOther);
            viewItemList.Add(viewModel.pageRightLeftOther);
            viewItemList.Add(viewModel.pageOmni);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedObjectRelationTree()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.objectRelation.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.tabStrip.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewModel.pageAttributes.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.pageLeftRight.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.pageRightLeft.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.pageLeftRightOther.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.pageRightLeftOther.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.pageOmni.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            viewItemList.Add(viewModel.objectRelation);
            viewItemList.Add(viewModel.tabStrip);

            viewItemList.Add(viewModel.pageAttributes);
            viewItemList.Add(viewModel.pageLeftRight);
            viewItemList.Add(viewModel.pageRightLeft);
            viewItemList.Add(viewModel.pageLeftRightOther);
            viewItemList.Add(viewModel.pageRightLeftOther);
            viewItemList.Add(viewModel.pageOmni);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedSingleRelation()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelSingleRelation(idInstance);

            viewModel.relationButton.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);


            viewItemList.Add(viewModel.relationButton);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyObjectRelationTree(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyEmbeddedNameEdit(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEmbeddedNameEdit(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyItemComBar(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelItemComBar(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadySingleRelation(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelSingleRelation(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetTreeConfigObjectRelationTree(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelObjectRelationTree(idInstance);
            var actionResult = new ResultSimpleView<KendoTreeViewConfig>
            {
                SelectorPath = viewModel.SelectorPath
            };

            actionResult.Result = new KendoTreeViewConfig
            {
                dataSource = new KendoHierarchicalDataSource
                {
                    schema = new KendoHierarchicalSchema
                    {
                        model = new KendoHierarchicalModel
                        {
                            id = nameof(KendoTreeNode.NodeId),
                            children = nameof(KendoTreeNode.SubNodes),
                            hasChildren = nameof(KendoTreeNode.HasChildren)
                        }
                    },
                    transport = new KendoTransport
                    {
                        read = new KendoTransportCRUD
                        {
                            dataType = "json",
                            url = Url.Action(nameof(GetTreeItemsObjectRelationTree))
                        }
                    }
                },
                dataTextField = nameof(KendoTreeNode.NodeName)
            };

            LogEndMethod(processId:processId);
            return Json(actionResult, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetTreeItemsObjectRelationTree(string idInstance, string idReference)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idInstance))
            {
                return Json(new List<KendoTreeNode>(), JsonRequestBehavior.AllowGet);
            }
            var viewModel = GetViewModelObjectRelationTree(idInstance);

            var controller = GetObjectTreeController();

            var messageOutput = new OWMessageOutput(viewModel.IdInstance);
            var request = new GetObjectRelationTreeRequest(idReference)
            {
                MessageOutput = messageOutput
            };

            var treeNodesResult = await controller.GetObjectRelationTree(request);
            if (treeNodesResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                return Json(new List<KendoTreeNode>(), JsonRequestBehavior.AllowGet);
            }

            viewModel.TreeNodes = treeNodesResult.Result.TreeNodes;
            viewModel.ObjectRelationTreeModel = treeNodesResult.Result.Model;

            var result = Json(treeNodesResult.Result.TreeNodes, JsonRequestBehavior.AllowGet);

            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }
    }
}