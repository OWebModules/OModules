﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Primitives;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Converter;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace OModules.Controllers
{
    public class EditOItemController : AppControllerBase
    {
        

        private clsTypes types = new clsTypes();
        private clsDataTypes dataTypes = new clsDataTypes();
        private clsLogStates logStates = new clsLogStates();

        

        private OntologyItemsModule.EditOItemController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.EditOItemController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private AddObjectsViewModel GetViewModelAddObjects(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();

            var viewModel = memoryCache.GetOrSet<AddObjectsViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new AddObjectsViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionValidateReference = Url.Action(nameof(ValidateReferenceAddObjects)),
                    ActionConnected = Url.Action(nameof(ConnectedAddObjects)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedAddObjects)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyAddObjects)),
                    ActionCheckNames = Url.Action(nameof(CheckObjects)),
                    ActionSaveObjects = Url.Action(nameof(SaveObjects)),
                    ActionGetClassDropDownConfig = Url.Action(nameof(DropDownController.GetDropDownConfigForClasses), nameof(DropDownController).Replace("Controller", "")),
                    ActionChangeValue = Url.Action(nameof(ChangeViewItemValueAddObjects)),
                    ActionAddAddObjectsRow = Url.Action(nameof(AddAddObjectsRow)),
                    ActionPasteText = Url.Action(nameof(PasteText)),
                    ActionRemoveEmptyObjectsRow = Url.Action(nameof(RemoveEmptyRows)),
                    IdSession = HttpContext.Session.SessionID
                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueAddObjects)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private ObjectNameEditViewModel GetViewModelObjectNameEdit(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();

            var viewModel = memoryCache.GetOrSet<ObjectNameEditViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ObjectNameEditViewModel(Session, key, User.Identity.GetUserId())
                {
                    
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceObjectNameEdit)),
                    ActionConnected = Url.Action(nameof(ConnectedObjectNameEdit)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedObjectNameEdit)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyObjectNameEdit)),
                    ActionSaveObjectOrObjectName = Url.Action(nameof(SaveObjectOrObjectName)),
                    IdSession = HttpContext.Session.SessionID
                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueObjectNameEdit)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private EditBoolAttributeViewModel GetViewModelBoolAttribute(string key)
        {
            var memoryCache = new InMemoryCache();

            return memoryCache.GetOrSet<EditBoolAttributeViewModel>(Session.SessionID, key, () =>
            {
                var processId = LogStartMethod();
                var viewModelNew = new EditBoolAttributeViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigBoolAttribute
                    {
                        ActionValidateReference = Url.Action(nameof(ValidateReferenceAttributeBool)),
                        ActionConnected = Url.Action(nameof(ConnectedBoolAttribute)),
                        ActionDisconnected = Url.Action(nameof(DisconnectedBoolAttribute)),
                        ActionGetAttribute = Url.Action(nameof(GetBoolAttribute)),
                        ActionSaveAttribute = Url.Action(nameof(SaveBoolAttribute))
                    },
                    IdSession = HttpContext.Session.SessionID
                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueBoolAttribute)));

                LogEndMethod(processId:processId);                return viewModelNew;
            });
            
        }

        private EditAttributeViewModel GetViewModelEditAttribute(string key)
        {
            var memoryCache = new InMemoryCache();

            return memoryCache.GetOrSet<EditAttributeViewModel>(Session.SessionID, key, () =>
            {
                var processId = LogStartMethod();
                var viewModelNew = new EditAttributeViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceEditAttribute)),
                    ActionConnected = Url.Action(nameof(ConnectedEditAttribute)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedEditAttribute)),
                    ActionGetAttribute = Url.Action(nameof(GetAttribute)),
                    ActionSaveBoolAttribute = Url.Action(nameof(SaveBoolAttribute)),
                    ActionSaveDateTimeAttribute = Url.Action(nameof(SaveDateTimeAttribute)),
                    ActionSaveDoubleAttribute = Url.Action(nameof(SaveDoubleAttribute)),
                    ActionSaveLongAttribute = Url.Action(nameof(SaveLongAttribute)),
                    ActionSaveStringAttribute = Url.Action(nameof(SaveStringAttribute)),
                    ActionEditAttributeInit = Url.Action(nameof(EditAttributeInit)),
                    IdSession = HttpContext.Session.SessionID
                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueEditAttribute)));

                LogEndMethod(processId:processId);                return viewModelNew;
            });

        }

        private EditDateTimeAttributeViewModel GetViewModelDateTimeAttribute(string key)
        {
            var memoryCache = new InMemoryCache();
            return memoryCache.GetOrSet<EditDateTimeAttributeViewModel>(Session.SessionID, key, () =>
            {
                var processId = LogStartMethod();
                var viewModelNew = new EditDateTimeAttributeViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigDateTimeAttribute
                    {
                        ActionValidateReference = Url.Action(nameof(ValidateReferenceAttributeDateTime)),
                        ActionConnected = Url.Action(nameof(ConnectedDateTimeAttribute)),
                        ActionDisconnected = Url.Action(nameof(DisconnectedDateTimeAttribute)),
                        ActionGetAttribute = Url.Action(nameof(GetDateTimeAttribute)),
                        ActionSaveAttribute = Url.Action(nameof(SaveDateTimeAttribute))
                    },
                    IdSession = HttpContext.Session.SessionID
                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueDateTimeAttribute)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });
            
        }

        private EditLongAttributeViewModel GetViewModelLongAttribute(string key)
        {
            var memoryCache = new InMemoryCache();
            return memoryCache.GetOrSet<EditLongAttributeViewModel>(Session.SessionID, key, () =>
            {
                var processId = LogStartMethod();
                var viewModelNew = new EditLongAttributeViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigLongAttribute
                    {
                        ActionValidateReference = Url.Action(nameof(ValidateReferenceAttributeLong)),
                        ActionConnected = Url.Action(nameof(ConnectedLongAttribute)),
                        ActionDisconnected = Url.Action(nameof(DisconnectedLongAttribute)),
                        ActionGetAttribute = Url.Action(nameof(GetLongAttribute)),
                        ActionSaveAttribute = Url.Action(nameof(SaveLongAttribute))
                    },
                    IdSession = HttpContext.Session.SessionID
                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueLongAttribute)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

        }

        private EditDoubleAttributeViewModel GetViewModelDoubleAttribute(string key)
        {
            var memoryCache = new InMemoryCache();
            return memoryCache.GetOrSet<EditDoubleAttributeViewModel>(Session.SessionID, key, () =>
            {
                var processId = LogStartMethod();
                var viewModelNew = new EditDoubleAttributeViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigDoubleAttribute
                    {
                        ActionValidateReference = Url.Action(nameof(ValidateReferenceAttributeDouble)),
                        ActionConnected = Url.Action(nameof(ConnectedDoubleAttribute)),
                        ActionDisconnected = Url.Action(nameof(DisconnectedDoubleAttribute)),
                        ActionGetAttribute = Url.Action(nameof(GetDoubleAttribute)),
                        ActionSaveAttribute = Url.Action(nameof(SaveDoubleAttribute))
                    },
                    IdSession = HttpContext.Session.SessionID
                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueDoubleAttribute)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

        }

        private EditStringAttributeViewModel GetViewModelStringAttribute(string key)
        {
            var memoryCache = new InMemoryCache();
            return memoryCache.GetOrSet<EditStringAttributeViewModel>(Session.SessionID, key, () =>
            {
                var processId = LogStartMethod();
                var viewModelNew = new EditStringAttributeViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigStrAttribute
                    {
                        ActionValidateReference = Url.Action(nameof(ValidateReferenceAttributeString)),
                        ActionConnected = Url.Action(nameof(ConnectedStringAttribute)),
                        ActionDisconnected = Url.Action(nameof(DisconnectedStringAttribute)),
                        ActionGetAttribute = Url.Action(nameof(GetStringAttribute)),
                        ActionSaveAttribute = Url.Action(nameof(SaveStringAttribute))
                    },
                    IdSession = HttpContext.Session.SessionID
                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueStringAttribute)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

        }

        private EditOitemViewModel GetViewModelOItem(string key)
        {
            var memoryCache = new InMemoryCache();
            return memoryCache.GetOrSet<EditOitemViewModel>(Session.SessionID, key, () =>
            {
                var processId = LogStartMethod();
                var viewModelNew = new EditOitemViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfig
                    {
                        ActionValidateReference = Url.Action(nameof(ValidateReference)),
                        ActionGetGridConfig = Url.Action(nameof(GetGridConfig)),
                        ActionAddDataRows = Url.Action(nameof(AddDataRow)),
                        ActionCreate = Url.Action(nameof(CreateInGrid)),
                        ActionUpdate = Url.Action(nameof(UpdateInGrid)),
                        ActionDestroy = Url.Action(nameof(DestroyInGrid)),
                        ActionRead = Url.Action(nameof(ReadInGrid)),
                        ActionSave = Url.Action(nameof(SaveData)),
                        ActionConnected = Url.Action(nameof(Connected)),
                        ActionDisconnected = Url.Action(nameof(Disconnected))
                    },
                    IdSession = HttpContext.Session.SessionID
                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });

        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> CheckObjects(List<string> objectNames, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<CheckObjectsResult>
            {
                IsSuccessful = true
            };

            if (objectNames == null)
            {
                result.IsSuccessful = false;
                result.Message = "Objectnames are invalid!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var viewModel = GetViewModelAddObjects(idInstance);

            if (viewModel.ClassItem == null)
            {
                result.IsSuccessful = false;
                result.Message = "No Class provided!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetController();

            var resultTask = await controller.CheckObjects(objectNames, viewModel.ClassItem.GUID);

            if (resultTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = resultTask.ResultState.Additional1;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = new CheckObjectsResult(resultTask.Result);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveObjects(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelAddObjects(idInstance);

            var resultAction = new ResultValidateObjects
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                AddObjectRows = new List<ViewItem>(),
                SavedObjects = new List<clsOntologyItem>()
            };

            var controller = GetController();
            var objectsToSaveResult = viewModel.GetObjectsToSave(controller.Globals);

            if (objectsToSaveResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                resultAction.IsSuccessful = false;
                resultAction.ResultMessage = objectsToSaveResult.ResultState.Additional1;
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            var resultTask = await controller.SaveObjects(objectsToSaveResult.Result);

            if (resultTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                resultAction.IsSuccessful = false;
                resultAction.ResultMessage = resultTask.ResultState.Additional1;
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.SavedObjects = resultTask.Result;

            viewModel.RemoveEmptyRows();

            resultAction.AddObjectRows = viewModel.AddObjectRows;

            var enableSaveButton = viewModel.EnableSave();
            var enableRemoveButton = viewModel.EnableRemove();

            resultAction.ViewItems.Add(viewModel.saveObjects);
            resultAction.ViewItems.Add(viewModel.removeRow);
            resultAction.ViewItems.Add(viewModel.applySaved);

            viewModel.saveObjects.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableSaveButton);
            viewModel.removeRow.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableRemoveButton);
            viewModel.applySaved.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableRemoveButton);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetStringAttribute(string idAttribute, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelStringAttribute(idInstance);

            
            var controller = GetController();

            var resultTask = await controller.GetAttribute(idAttribute);

            var result = new ResultGetAttribute
            {
                isOk = (resultTask.Result.GUID != logStates.LogState_Error.GUID),
                Message = (resultTask.Result.GUID == logStates.LogState_Error.GUID ? Messages.EditOItem_Error_GetAttribute : null),
                ObjectAttribute = resultTask.ObjectAttribute
            };

            viewModel.objectAttribute = result.ObjectAttribute;
            viewModel.objectItem = resultTask.ObjectItem;
            viewModel.AttributeTypeItem = resultTask.AttributeTypeItem;

            if (result.ObjectAttribute.ID_DataType != dataTypes.DType_String.GUID)
            {
                result.isOk = false;
                result.Message = Messages.EditOitem_Error_WrongType;
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveStringAttribute(string contentB64, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEditAttribute(idInstance);

            var controller = GetController();

            var content = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(contentB64));

            var resultSave = await controller.SaveObjectAttribute(viewModel.objectItem, viewModel.attributeTypeItem, content, viewModel.objectAttribute != null ? viewModel.objectAttribute.ID_Attribute : null);

            var result = new ResultSimpleView<clsObjectAtt>
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath
            };

            if (resultSave.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.objectAttribute = resultSave.Result;
            result.Result = viewModel.objectAttribute;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveBoolAttribute(bool value, string idInstance)
        {

            var processId = LogStartMethod();
            var viewModel = GetViewModelEditAttribute(idInstance);

            var controller = GetController();

            var resultSave = await controller.SaveObjectAttribute(viewModel.objectItem, viewModel.attributeTypeItem, value, viewModel.objectAttribute != null ? viewModel.objectAttribute.ID_Attribute : null);

            var result = new ResultSimpleView<clsObjectAtt>
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath
            };

            if (resultSave.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.objectAttribute = resultSave.Result;
            result.Result = viewModel.objectAttribute;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveDoubleAttribute(string value, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEditAttribute(idInstance);

            var controller = GetController();

            var result = new ResultSimpleView<clsObjectAtt>
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath
            };

            double dblValue = 0;
            if (!double.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out dblValue))
            {
                result.IsSuccessful = false;
                result.Message = "The value is not valid number!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultSave = await controller.SaveObjectAttribute(viewModel.objectItem, viewModel.attributeTypeItem, dblValue, viewModel.objectAttribute != null ? viewModel.objectAttribute.ID_Attribute : null);

            if (resultSave.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Result = resultSave.Result;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.objectAttribute = resultSave.Result;
            result.Result = viewModel.objectAttribute;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveLongAttribute(long value, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEditAttribute(idInstance);

            var controller = GetController();

            var resultSave = await controller.SaveObjectAttribute(viewModel.objectItem, viewModel.attributeTypeItem, value, viewModel.objectAttribute != null ? viewModel.objectAttribute.ID_Attribute : null);

            var result = new ResultSimpleView<clsObjectAtt>
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath
            };

            if (resultSave.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.objectAttribute = resultSave.Result;
            result.Result = viewModel.objectAttribute;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveDateTimeAttribute(DateTime value, string idInstance)
        {

            var processId = LogStartMethod();
            var viewModel = GetViewModelEditAttribute(idInstance);

            var controller = GetController();

            var resultSave = await controller.SaveObjectAttribute(viewModel.objectItem, viewModel.attributeTypeItem, value, viewModel.objectAttribute != null ? viewModel.objectAttribute.ID_Attribute : null);

            var result = new ResultSimpleView<clsObjectAtt>
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath
            };

            if (resultSave.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.objectAttribute = resultSave.Result;
            result.Result = viewModel.objectAttribute;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetAttribute(string idAttribute, string idObject, string idAttributeType, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEditAttribute(idInstance);

            var controller = GetController();

            var result = new ResultGetAttribute
            {
                isOk = true,
                SelectorPath = viewModel.SelectorPath
            };

            if (!string.IsNullOrEmpty(idAttribute))
            {
                var resultTask = await controller.GetAttribute(idAttribute);

                if (resultTask.Result.GUID == controller.Globals.LState_Success.GUID)
                {
                    viewModel.objectAttribute = resultTask.ObjectAttribute;
                    viewModel.objectItem = resultTask.ObjectItem;
                    viewModel.attributeTypeItem = resultTask.AttributeTypeItem;
                }
                else
                {
                    result.isOk = false;
                    result.Message = Messages.EditOItem_Error_GetAttribute;
                }
            }
            else if (!string.IsNullOrEmpty(idObject) && !string.IsNullOrEmpty(idAttributeType) &&
                    controller.Globals.is_GUID(idObject) && controller.Globals.is_GUID(idAttributeType))
            {
                viewModel.objectAttribute = null;
                var resulOItemTask = await controller.GetOItem(idObject, controller.Globals.Type_Object);
                if (resulOItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    result.isOk = false;
                    result.Message = resulOItemTask.ResultState.Additional1;
                }

                viewModel.objectItem = resulOItemTask.Result;

                var resulOItemAttTask = await controller.GetOItem(idAttributeType, controller.Globals.Type_AttributeType);
                if (resulOItemAttTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    result.isOk = false;
                    result.Message = resulOItemAttTask.ResultState.Additional1;
                }

                viewModel.attributeTypeItem = resulOItemAttTask.Result;
            }
            else
            {
                result.isOk = false;
                result.Message = Messages.EditOItem_Error_WrongParameters;
            }
            
            if (result.isOk)
            {
                result.ObjectAttribute = viewModel.objectAttribute;
                result.Object = viewModel.objectItem;
                result.AttributeType = viewModel.attributeTypeItem;
            }
            
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetBoolAttribute(string idAttribute, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelBoolAttribute(idInstance);

            
            var controller = GetController();

            var resultTask = await controller.GetAttribute(idAttribute);

            var result = new ResultGetAttribute
            {
                isOk = (resultTask.Result.GUID != logStates.LogState_Error.GUID),
                Message = (resultTask.Result.GUID == logStates.LogState_Error.GUID ? Messages.EditOItem_Error_GetAttribute : null),
                ObjectAttribute = resultTask.ObjectAttribute
            };

            viewModel.objectAttribute = result.ObjectAttribute;
            viewModel.objectItem = resultTask.ObjectItem;
            viewModel.AttributeTypeItem = resultTask.AttributeTypeItem;

            if (result.ObjectAttribute.ID_DataType != dataTypes.DType_Bool.GUID)
            {
                result.isOk = false;
                result.Message = Messages.EditOitem_Error_WrongType;
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetDateTimeAttribute(string idAttribute, string idInstance)
        {

            var processId = LogStartMethod();
            var viewModel = GetViewModelDateTimeAttribute(idInstance);

            
            var controller = GetController();

            var resultTask = await controller.GetAttribute(idAttribute);

            var result = new ResultGetAttribute
            {
                isOk = (resultTask.Result.GUID != logStates.LogState_Error.GUID),
                Message = (resultTask.Result.GUID == logStates.LogState_Error.GUID ? Messages.EditOItem_Error_GetAttribute : null),
                ObjectAttribute = resultTask.ObjectAttribute
            };

            viewModel.objectAttribute = result.ObjectAttribute;
            viewModel.objectItem = resultTask.ObjectItem;
            viewModel.AttributeTypeItem = resultTask.AttributeTypeItem;

            if (viewModel.objectAttribute.ID_DataType == dataTypes.DType_DateTime.GUID)
            {
                viewModel.dateTimeValue.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.objectAttribute.Val_Date.Value.ToString("yyyy-MM-dd hh:mm:ss.fff"));
                viewModel.dateTimeValue.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                result.ViewItem = viewModel.dateTimeValue;
            }

            if (result.ObjectAttribute.ID_DataType != dataTypes.DType_DateTime.GUID)
            {
                result.isOk = false;
                result.Message = Messages.EditOitem_Error_WrongType;
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetLongAttribute(string idAttribute, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelLongAttribute(idInstance);

            
            var controller = GetController();

            var resultTask = await controller.GetAttribute(idAttribute);

            var result = new ResultGetAttribute
            {
                isOk = (resultTask.Result.GUID != logStates.LogState_Error.GUID),
                Message = (resultTask.Result.GUID == logStates.LogState_Error.GUID ? Messages.EditOItem_Error_GetAttribute : null),
                ObjectAttribute = resultTask.ObjectAttribute
            };

            viewModel.objectAttribute = result.ObjectAttribute;
            viewModel.objectItem = resultTask.ObjectItem;
            viewModel.AttributeTypeItem = resultTask.AttributeTypeItem;

            if (viewModel.objectAttribute.ID_DataType == dataTypes.DType_Int.GUID)
            {
                viewModel.longValue.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.objectAttribute.Val_Lng);
                viewModel.longValue.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                result.ViewItem = viewModel.longValue;
            }

            if (result.ObjectAttribute.ID_DataType != dataTypes.DType_Int.GUID)
            {
                result.isOk = false;
                result.Message = Messages.EditOitem_Error_WrongType;
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetDoubleAttribute(string idAttribute, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelDoubleAttribute(idInstance);

            
            var controller = GetController();

            var resultTask = await controller.GetAttribute(idAttribute);

            var result = new ResultGetAttribute
            {
                isOk = (resultTask.Result.GUID != logStates.LogState_Error.GUID),
                Message = (resultTask.Result.GUID == logStates.LogState_Error.GUID ? Messages.EditOItem_Error_GetAttribute : null),
                ObjectAttribute = resultTask.ObjectAttribute
            };

            viewModel.objectAttribute = result.ObjectAttribute;
            viewModel.objectItem = resultTask.ObjectItem;
            viewModel.AttributeTypeItem = resultTask.AttributeTypeItem;

            if (viewModel.objectAttribute.ID_DataType == dataTypes.DType_Real.GUID)
            {
                viewModel.doubleValue.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.objectAttribute.Val_Double);
                viewModel.doubleValue.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                result.ViewItem = viewModel.doubleValue;
            }

            if (result.ObjectAttribute.ID_DataType != dataTypes.DType_Real.GUID)
            {
                result.isOk = false;
                result.Message = Messages.EditOitem_Error_WrongType;
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult EditAttributeInit(string idInstance, List<string> selectorPath)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idInstance))
            {
                idInstance = Guid.NewGuid().ToString();
            }
            var viewModel = GetViewModelEditAttribute(idInstance);
            viewModel.SelectorPath = selectorPath;

            var message = "";
            var result = new ResultSimpleView<EditAttributeViewModel>
            {
                IsSuccessful = true,
                Message = message,
                Result = viewModel,
                SelectorPath = selectorPath
            };

            LogEndMethod(processId:processId);            return Json(result);
        }

        [Authorize]
        public ActionResult EditBoolAttribute()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEditAttribute(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);            return View(viewModel);
        }

        [Authorize]
        public ActionResult EditDateTimeAttribute()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEditAttribute(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult EditLongAttribute()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEditAttribute(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult EditDoubleAttribute()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEditAttribute(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult EditStringAttribute()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEditAttribute(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult EditOItem()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelOItem(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ObjectNameEdit()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelObjectNameEdit(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ReadInGrid()
        {
            var processId = LogStartMethod();

            LogEndMethod(processId:processId);
            return Json(new object(), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult CreateInGrid()
        {
            var processId = LogStartMethod();
            LogEndMethod(processId:processId);            return Json(new object(), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> UpdateInGrid(IEnumerable<Dictionary<string, object>> models)
        {
            var processId = LogStartMethod();
            EditOitemViewModel viewModel = null;
            var result = new List<EditItem>();



            var controller = GetController();
            foreach (var editItemDict in models)
            {
                var editItem = DictionaryToEditItem.Convert(editItemDict);
                
                if (viewModel == null)
                {
                    viewModel = GetViewModelOItem(editItem.IdViewInstance);
                }

                var origItem = viewModel.RowItems.FirstOrDefault(rowItem => rowItem.Guid == editItem.Guid);
                if (origItem == null)
                {
                    editItem.Comment = Messages.EditOItem_Update_DataError;
                    result.Add(editItem);

                }


                if (editItem.ItemType == viewModel.itemTypes.AttributeType)
                {
                    if (string.IsNullOrEmpty(editItem.Name))
                    {
                        origItem.Save = editItem.Save;
                        origItem.Apply = editItem.Apply;
                        origItem.Name = editItem.Name;
                        origItem.Comment = Messages.EditOitem_Update_EmptyName;
                        origItem.EditItemState = EditItemState.SaveNotAllowed;
                    }
                    else
                    {
                        var resultTask = await controller.IsExistingAttributeType(editItem.Name);

                        if (resultTask.GUID == logStates.LogState_Exists.GUID)
                        {
                            origItem.Save = editItem.Save;
                            origItem.Apply = editItem.Apply;
                            origItem.Name = editItem.Name;
                            origItem.Comment = Messages.EditOItem_Update_Exists;
                            origItem.EditItemState = EditItemState.SaveNotAllowed;
                        }
                        else if (resultTask.GUID == logStates.LogState_Error.GUID)
                        {
                            origItem.Save = editItem.Save;
                            origItem.Apply = editItem.Apply;
                            origItem.Name = editItem.Name;
                            origItem.Comment = Messages.EditOItem_Update_DataError;
                            origItem.EditItemState = EditItemState.SaveNotAllowed;
                        }
                        else
                        {
                            origItem.Save = editItem.Save;
                            origItem.Apply = editItem.Apply;
                            origItem.Name = editItem.Name;
                            origItem.Comment = "";
                            origItem.EditItemState = EditItemState.None;
                        }
                    }

                    var editedItem = viewModel.RowItems.Count(rowItem => rowItem.ItemType == editItem.ItemType && rowItem.Name == editItem.Name);
                    if (editedItem > 1)
                    {

                        origItem.Comment = Messages.EditOItem_Update_Multiple;
                        origItem.EditItemState = EditItemState.SaveNotAllowed;
                    }
                }
                else if (editItem.ItemType == viewModel.itemTypes.RelationType)
                {


                    if (string.IsNullOrEmpty(editItem.Name))
                    {
                        origItem.Save = editItem.Save;
                        origItem.Apply = editItem.Apply;
                        origItem.Name = editItem.Name;
                        origItem.Comment = Messages.EditOitem_Update_EmptyName;
                        origItem.EditItemState = EditItemState.SaveNotAllowed;
                    }
                    else
                    {
                        var resultTask = await controller.IsExistingRelationType(editItem.Name);

                        if (resultTask.GUID == logStates.LogState_Exists.GUID)
                        {
                            origItem.Save = editItem.Save;
                            origItem.Apply = editItem.Apply;
                            origItem.Name = editItem.Name;
                            origItem.Comment = Messages.EditOItem_Update_Exists;
                            origItem.EditItemState = EditItemState.SaveNotAllowed;
                        }
                        else if (resultTask.GUID == logStates.LogState_Error.GUID)
                        {
                            origItem.Save = editItem.Save;
                            origItem.Apply = editItem.Apply;
                            origItem.Name = editItem.Name;
                            origItem.Comment = Messages.EditOItem_Update_DataError;
                            origItem.EditItemState = EditItemState.SaveNotAllowed;
                        }
                        else
                        {
                            origItem.Save = editItem.Save;
                            origItem.Apply = editItem.Apply;
                            origItem.Name = editItem.Name;
                            origItem.Comment = "";
                            origItem.EditItemState = EditItemState.None;
                        }
                    }

                    var editedItem = viewModel.RowItems.Count(rowItem => rowItem.ItemType == editItem.ItemType && rowItem.Name == editItem.Name);
                    if (editedItem > 1)
                    {

                        origItem.Comment = Messages.EditOItem_Update_Multiple;
                        origItem.EditItemState = EditItemState.SaveNotAllowed;
                    }




                }
                else if (editItem.ItemType == viewModel.itemTypes.ClassType)
                {


                    if (string.IsNullOrEmpty(editItem.Name))
                    {
                        origItem.Save = editItem.Save;
                        origItem.Apply = editItem.Apply;
                        origItem.Name = editItem.Name;
                        origItem.Comment = Messages.EditOitem_Update_EmptyName;
                        origItem.EditItemState = EditItemState.SaveNotAllowed;
                    }
                    else
                    {
                        var resultTask = await controller.IsExistingClass(editItem.Name);

                        if (resultTask.GUID == logStates.LogState_Exists.GUID)
                        {
                            origItem.Save = editItem.Save;
                            origItem.Apply = editItem.Apply;
                            origItem.Name = editItem.Name;
                            origItem.Comment = Messages.EditOItem_Update_Exists;
                            origItem.EditItemState = EditItemState.SaveNotAllowed;
                        }
                        else if (resultTask.GUID == logStates.LogState_Error.GUID)
                        {
                            origItem.Save = editItem.Save;
                            origItem.Apply = editItem.Apply;
                            origItem.Name = editItem.Name;
                            origItem.Comment = Messages.EditOItem_Update_DataError;
                            origItem.EditItemState = EditItemState.SaveNotAllowed;
                        }
                        else
                        {
                            origItem.Save = editItem.Save;
                            origItem.Apply = editItem.Apply;
                            origItem.Name = editItem.Name;
                            origItem.Comment = "";
                            origItem.EditItemState = EditItemState.None;
                        }
                    }

                    var editedItem = viewModel.RowItems.Count(rowItem => rowItem.ItemType == editItem.ItemType && rowItem.Name == editItem.Name);
                    if (editedItem > 1)
                    {

                        origItem.Comment = Messages.EditOItem_Update_Multiple;
                        origItem.EditItemState = EditItemState.SaveNotAllowed;
                    }




                }
                else if (editItem.ItemType == viewModel.itemTypes.ObjectType)
                {


                    if (string.IsNullOrEmpty(editItem.Name))
                    {
                        origItem.Save = editItem.Save;
                        origItem.Apply = editItem.Apply;
                        origItem.Name = editItem.Name;
                        origItem.Comment = Messages.EditOitem_Update_EmptyName;
                        origItem.EditItemState = EditItemState.SaveNotAllowed;
                    }
                    else
                    {
                        var resultTask = await controller.IsExistingObject(editItem.Name, viewModel.refParentItem.GUID);

                        if (resultTask.GUID == logStates.LogState_Exists.GUID)
                        {
                            origItem.Save = editItem.Save;
                            origItem.Apply = editItem.Apply;
                            origItem.Name = editItem.Name;
                            origItem.Comment = Messages.EditOItem_Update_Exists;
                            origItem.EditItemState = EditItemState.None;
                        }
                        else if (resultTask.GUID == logStates.LogState_Error.GUID)
                        {
                            origItem.Save = editItem.Save;
                            origItem.Apply = editItem.Apply;
                            origItem.Name = editItem.Name;
                            origItem.Comment = Messages.EditOItem_Update_DataError;
                            origItem.EditItemState = EditItemState.SaveNotAllowed;
                        }
                        else
                        {
                            origItem.Save = editItem.Save;
                            origItem.Apply = editItem.Apply;
                            origItem.Name = editItem.Name;
                            origItem.Comment = "";
                            origItem.EditItemState = EditItemState.None;
                        }
                    }



                }
                result.Add(origItem);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DestroyInGrid()
        {
            var processId = LogStartMethod();            LogEndMethod(processId:processId);            return Json(new object(), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceEditAttribute(clsOntologyItem refItem, clsOntologyItem attributeTypeItem, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultValidateReference
            {
                IsOk = true
            };

            if (refItem == null || attributeTypeItem == null || string.IsNullOrEmpty(refItem.GUID) || string.IsNullOrEmpty(attributeTypeItem.GUID))
            {
                result.IsOk = false;
            }

            var viewModel = GetViewModelEditAttribute(idInstance);
            result.SelectorPath = viewModel.SelectorPath;
            var controller = GetController();

            viewModel.objectItem = (await controller.GetOItem(refItem.GUID, controller.Globals.Type_Object)).Result;
            viewModel.objectParentItem = (await controller.GetOItem(viewModel.objectItem.GUID_Parent, controller.Globals.Type_Class)).Result;
            viewModel.attributeTypeItem = (await controller.GetOItem(attributeTypeItem.GUID, controller.Globals.Type_AttributeType)).Result;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceAttributeBool(clsOntologyItem refItem, clsOntologyItem attributeTypeItem, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultValidateReference
            {
                IsOk = true
            };

            if (refItem == null || attributeTypeItem == null || string.IsNullOrEmpty(refItem.GUID) || string.IsNullOrEmpty(attributeTypeItem.GUID))
            {
                result.IsOk = false;
            }

            var viewModel = GetViewModelBoolAttribute(idInstance);
            var controller = GetController();

            viewModel.objectItem = (await controller.GetOItem(refItem.GUID, controller.Globals.Type_Object)).Result;
            viewModel.objectParentItem = (await controller.GetOItem(viewModel.objectItem.GUID_Parent, controller.Globals.Type_Class)).Result;
            viewModel.AttributeTypeItem = (await controller.GetOItem(attributeTypeItem.GUID, controller.Globals.Type_AttributeType)).Result;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceAttributeDateTime(clsOntologyItem refItem, clsOntologyItem attributeTypeItem, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultValidateReference
            {
                IsOk = true,
                ViewItems = new List<ViewItem>()
            };

            if (refItem == null || attributeTypeItem == null || string.IsNullOrEmpty(refItem.GUID) || string.IsNullOrEmpty(attributeTypeItem.GUID))
            {
                result.IsOk = false;
            }

            var viewModel = GetViewModelDateTimeAttribute(idInstance);
            var controller = GetController();

            viewModel.objectItem = (await controller.GetOItem(refItem.GUID, controller.Globals.Type_Object)).Result;
            viewModel.objectParentItem = (await controller.GetOItem(viewModel.objectItem.GUID_Parent, controller.Globals.Type_Class)).Result;
            viewModel.AttributeTypeItem = (await controller.GetOItem(attributeTypeItem.GUID, controller.Globals.Type_AttributeType)).Result;

            if (result.IsOk)
            {
                result.ViewItems.Add(viewModel.dateTimeValue);
                viewModel.dateTimeValue.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceAttributeLong(clsOntologyItem refItem, clsOntologyItem attributeTypeItem, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultValidateReference
            {
                IsOk = true,
                ViewItems = new List<ViewItem>()
            };

            if (refItem == null || attributeTypeItem == null || string.IsNullOrEmpty(refItem.GUID) || string.IsNullOrEmpty(attributeTypeItem.GUID))
            {
                result.IsOk = false;
            }

            var viewModel = GetViewModelLongAttribute(idInstance);
            var controller = GetController();

            viewModel.objectItem = (await controller.GetOItem(refItem.GUID, controller.Globals.Type_Object)).Result;
            viewModel.objectParentItem = (await controller.GetOItem(viewModel.objectItem.GUID_Parent, controller.Globals.Type_Class)).Result;
            viewModel.AttributeTypeItem = (await controller.GetOItem(attributeTypeItem.GUID, controller.Globals.Type_AttributeType)).Result;

            viewModel.longValue.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            result.ViewItems.Add(viewModel.longValue);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceAttributeDouble(clsOntologyItem refItem, clsOntologyItem attributeTypeItem, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultValidateReference
            {
                IsOk = true
            };

            if (refItem == null || attributeTypeItem == null || string.IsNullOrEmpty(refItem.GUID) || string.IsNullOrEmpty(attributeTypeItem.GUID))
            {
                result.IsOk = false;
            }

            var viewModel = GetViewModelDoubleAttribute(idInstance);
            var controller = GetController();

            viewModel.objectItem = (await controller.GetOItem(refItem.GUID, controller.Globals.Type_Object)).Result;
            viewModel.objectParentItem = (await controller.GetOItem(viewModel.objectItem.GUID_Parent, controller.Globals.Type_Class)).Result;
            viewModel.AttributeTypeItem = (await controller.GetOItem(attributeTypeItem.GUID, controller.Globals.Type_AttributeType)).Result;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceAttributeString(clsOntologyItem refItem, clsOntologyItem attributeTypeItem, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultValidateReferenceAtt
            {
                IsOk = true
            };

            if (refItem == null || attributeTypeItem == null || string.IsNullOrEmpty(refItem.GUID) || string.IsNullOrEmpty(attributeTypeItem.GUID))
            {
                result.IsOk = false;
            }

            var viewModel = GetViewModelStringAttribute(idInstance);
            var controller = GetController();

            viewModel.objectItem = (await controller.GetOItem(refItem.GUID, controller.Globals.Type_Object)).Result;
            viewModel.objectParentItem = (await controller.GetOItem(viewModel.objectItem.GUID_Parent, controller.Globals.Type_Class)).Result;
            viewModel.AttributeTypeItem = (await controller.GetOItem(attributeTypeItem.GUID, controller.Globals.Type_AttributeType)).Result;

            result.RefItem = viewModel.objectItem;
            result.RefParentItem = viewModel.objectParentItem;
            result.AttributeTypeItem = viewModel.AttributeTypeItem;
            result.ViewItems = new List<ViewItem>();
            result.ViewItems.Add(viewModel.windowTitle);
            result.ViewItems.Add(viewModel.strValue);

            viewModel.SetWindowTitle(GetWindowTitle(viewModel.objectItem, viewModel.objectParentItem, viewModel.AttributeTypeItem));
            viewModel.strValue.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private string GetWindowTitle(clsOntologyItem objectItem, clsOntologyItem objectParentItem, clsOntologyItem attributeTypeItem)
        {
            var processId = LogStartMethod();            LogEndMethod(processId:processId);            return $"{attributeTypeItem.Name}: {objectItem.Name} ({objectParentItem.Name})";
        }

        [Authorize]
        [HttpPost]
        public ActionResult ValidateReferenceAttributeType(clsOntologyItem attributeType, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultValidateReference
            {
                IsOk = true
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultValidateReference
            {
                IsOk = true
            };
            if (refItem == null)
            {
                result.IsOk = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            
            
            var viewModel = GetViewModelOItem(idInstance);

            var controller = GetController();

            var refItemTask = await controller.GetOItem(refItem.GUID, refItem.Type);

            if (refItemTask.ResultState.GUID == logStates.LogState_Error.GUID && refItemTask.ResultState.GUID != refItem.GUID)
            {
                result.IsOk = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            result.ViewItems = new List<ViewItem>();
            viewModel.refItem = refItemTask.Result;
            if (refItemTask.Result.Type == types.ClassType)
            {
                viewModel.refParentItem = refItemTask.Result;
                result.RefParentItem = refItemTask.Result;
                viewModel.dropDownItemType.ChangeViewItemValue(ViewItemType.Content.ToString(), types.ObjectType);
                result.ViewItems.Add(viewModel.dropDownItemType);
                viewModel.newItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                result.ViewItems.Add(viewModel.newItem);

                viewModel.parentInput.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.refParentItem.Name);
                viewModel.listenItem.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.listenItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

                result.ViewItems.Add(viewModel.parentInput);
                result.ViewItems.Add(viewModel.listenItem);

                viewModel.refItem = refItemTask.Result;
                result.RefItem = viewModel.refItem;
            }
            else if (refItemTask.Result.Type == types.ObjectType)
            {
                var dataType = controller.DataTypes.FirstOrDefault(dType => dType.GUID == refItem.GUID);
                

                if (dataType != null)
                {
                    viewModel.refParentItem = dataType.Clone();
                    result.RefParentItem = dataType;
                    viewModel.dropDownItemType.ChangeViewItemValue(ViewItemType.Content.ToString(), types.AttributeType);
                    result.ViewItems.Add(viewModel.dropDownItemType);
                    viewModel.newItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                    result.ViewItems.Add(viewModel.newItem);
                }
                else
                {
                    var classItemTask = await controller.GetOItem(refItemTask.Result.GUID_Parent, types.ClassType);

                    viewModel.refParentItem = classItemTask.Result;
                    result.RefParentItem = classItemTask.Result;
                    viewModel.dropDownItemType.ChangeViewItemValue(ViewItemType.Content.ToString(), types.ObjectType);
                    result.ViewItems.Add(viewModel.dropDownItemType);
                    viewModel.newItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                    result.ViewItems.Add(viewModel.newItem);
                }

                viewModel.parentInput.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.refParentItem.Name);
                viewModel.listenItem.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.listenItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

                result.ViewItems.Add(viewModel.parentInput);
                result.ViewItems.Add(viewModel.listenItem);

                viewModel.refItem = refItemTask.Result;
                result.RefItem = viewModel.refItem;
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceObjectNameEdit(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            
            var result = new ResultSimpleView<clsOntologyItem>
            {
                IsSuccessful = true,
                Result = null,
                ViewItems = new List<ViewItem>()
            };
            if (refItem == null)
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }


            var viewModel = GetViewModelObjectNameEdit(idInstance);

            var controller = GetController();

            var guid = controller.Globals.NewGUID;
            var refItemTask = await controller.GetOItem(refItem.GUID, refItem.Type);

            if (refItemTask.ResultState.GUID == logStates.LogState_Error.GUID && refItemTask.ResultState.GUID != refItem.GUID)
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.inpGUID.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            result.ViewItems = new List<ViewItem>();
            if (refItemTask.Result.Type == types.ClassType)
            {
                viewModel.inpGUID.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.refParentItem = refItemTask.Result;
                viewModel.SetWindowTitle(viewModel.refParentItem.Name);
            }
            else if (refItemTask.Result.Type == types.ObjectType)
            {
                
                viewModel.RefItem = refItemTask.Result;
                guid = refItemTask.Result.GUID;

                var refParentItemTask = await controller.GetOItem(viewModel.RefItem.GUID_Parent, controller.Globals.Type_Class);
                if (refParentItemTask.ResultState.GUID == logStates.LogState_Error.GUID && refItemTask.ResultState.GUID != refItem.GUID)
                {
                    result.IsSuccessful = false;
                    LogEndMethod(processId:processId);                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                viewModel.refParentItem = refParentItemTask.Result;
                viewModel.SetWindowTitle($"{viewModel.RefItem.Name} ({viewModel.refParentItem.Name})");
                viewModel.inpName.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.RefItem.Name);
                
            }
            viewModel.inpName.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.inpName.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.inpGUID.ChangeViewItemValue(ViewItemType.Content.ToString(), guid);
            result.ViewItems.Add(viewModel.inpName);
            result.ViewItems.Add(viewModel.inpGUID);
            result.ViewItems.Add(viewModel.windowTitle);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceAddObjects(clsOntologyItem refItem, string initName, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultValidateObjects
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                AddObjectRows = new List<ViewItem>()
            };
            if (refItem == null)
            {
                result.IsSuccessful = false;
                result.ResultMessage = "RefItem is NULL!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var viewModel = GetViewModelAddObjects(idInstance);

            viewModel.ResetRows(initName);

            var controller = GetController();

            var refItemTask = await controller.GetOItem(refItem.GUID, refItem.Type);

            if (refItemTask.ResultState.GUID == logStates.LogState_Error.GUID && refItemTask.ResultState.GUID != refItem.GUID)
            {
                result.IsSuccessful = false;
                result.ResultMessage = refItemTask.ResultState.Additional1;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.ViewItems = new List<ViewItem>();

            viewModel.ClassItem = refItemTask.Result;

            foreach (var addObjectRow in viewModel.AddObjectRows)
            {
                addObjectRow.ChangeViewItemValue(ViewItemType.PlaceHolder.ToString(), $"Class:{viewModel.ClassItem.Name}");
                addObjectRow.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            }

            result.AddObjectRows = viewModel.AddObjectRows;
            if (refItemTask.Result.Type != types.ClassType)
            {
                result.IsSuccessful = false;
                result.ResultMessage = "Reference-Item is no class!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var checkResult = await viewModel.CheckAddObjectRows(controller);

            if (checkResult.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.ResultMessage = checkResult.Additional1;
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var enableSaveButton = viewModel.EnableSave();
            var enableRemoveButton = viewModel.EnableRemove();


            result.ViewItems.Add(viewModel.saveObjects);
            result.ViewItems.Add(viewModel.removeRow);
            result.ViewItems.Add(viewModel.applySaved);

            viewModel.saveObjects.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableSaveButton);
            viewModel.removeRow.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableRemoveButton);
            viewModel.applySaved.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableRemoveButton);
            viewModel.applySaved.ChangeViewItemValue(ViewItemType.Checked.ToString(), enableRemoveButton);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelOItem(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var viewItemValue = viewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }
            else if (viewItem.ViewItemId == nameof(viewModel.selectedItemType) && viewItem.LastValue.ViewItemType == ViewItemType.Content.ToString())
            {
                viewItemValue = viewModel.selectedItemType.GetViewItemValueItem(ViewItemType.Content.ToString());

                if (viewItemValue.Value.ToString() == types.RelationType)
                {
                    viewModel.newItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                    resultAction.ViewItems.Add(viewModel.newItem);
                }

            }

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> AddAddObjectsRow(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelAddObjects(idInstance);

            var resultAction = new ResultValidateObjects
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                AddObjectRows = new List<ViewItem>()
            };

            var viewItem = viewModel.AddRow();
            resultAction.AddObjectRows.AddRange (viewModel.AddObjectRows);
            if (viewModel.ClassItem != null)
            {
                viewItem.ChangeViewItemValue(ViewItemType.PlaceHolder.ToString(), $"Class: {viewModel.ClassItem.Name}");
            }
            var controller = GetController();

            var result = await viewModel.CheckAddObjectRows(controller);
            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                resultAction.IsSuccessful = false;
                resultAction.ResultMessage = result.Additional1;
                LogEndMethod(processId:processId);                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            var enableSaveButton = viewModel.EnableSave();
            var enableRemoveButton = viewModel.EnableRemove();

            resultAction.ViewItems.Add(viewModel.saveObjects);
            resultAction.ViewItems.Add(viewModel.removeRow);
            resultAction.ViewItems.Add(viewModel.applySaved);

            viewModel.saveObjects.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableSaveButton);
            viewModel.removeRow.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableRemoveButton);
            viewModel.applySaved.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableRemoveButton);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        public ActionResult PasteText(string pasted, string viewItemid, string idInstance)
        {
            var processId = LogStartMethod();
            var content = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(pasted));

            var viewModel = GetViewModelAddObjects(idInstance);

            var i = 0;
            for (i = 0; i < viewModel.AddObjectRows.Count; i++)
            {
                if (viewModel.AddObjectRows[i].ViewItemId == viewItemid)
                {
                    break;
                }
            }

            var splitted = content.Split('\n');

            var j = 0;
            for (j = 0; j < splitted.Length; j++)
            {
                var splitPart = splitted[j];

                if (splitPart.Length > 255)
                {
                    viewModel.AddRow(splitPart.Substring(0, 254), i+j);
                }
                else
                {
                    viewModel.AddRow(splitPart, i+j);
                }
            }
            var countItems = i + j;
            if (countItems == viewModel.AddObjectRows.Count)
            {
                viewModel.AddRow();
            }

            var resultAction = new ResultValidateObjects
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                AddObjectRows = viewModel.AddObjectRows
            };

            resultAction.AddObjectRows = viewModel.AddObjectRows;

            var enableSaveButton = viewModel.EnableSave();
            var enableRemoveButton = viewModel.EnableRemove();

            resultAction.ViewItems.Add(viewModel.saveObjects);
            resultAction.ViewItems.Add(viewModel.removeRow);
            resultAction.ViewItems.Add(viewModel.applySaved);

            viewModel.saveObjects.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableSaveButton);
            viewModel.removeRow.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableRemoveButton);
            viewModel.applySaved.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableRemoveButton);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult RemoveEmptyRows(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelAddObjects(idInstance);

            viewModel.RemoveEmptyRows();

            var resultAction = new ResultValidateObjects
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                AddObjectRows = viewModel.AddObjectRows
            };

            var enableSaveButton = viewModel.EnableSave();
            var enableRemoveButton = viewModel.EnableRemove();

            resultAction.ViewItems.Add(viewModel.saveObjects);
            resultAction.ViewItems.Add(viewModel.removeRow);
            resultAction.ViewItems.Add(viewModel.applySaved);

            viewModel.saveObjects.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableSaveButton);
            viewModel.removeRow.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableRemoveButton);
            viewModel.applySaved.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableRemoveButton);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueObjectNameEdit(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelObjectNameEdit(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var viewItemValue = result.ViewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            viewModel.btnSave.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            resultAction.ViewItems.Add(viewModel.btnSave);
            
            if (result.ViewItem.ViewItemId == viewModel.inpName.ViewItemId && result.ViewItem.LastValue.ViewItemType == ViewItemType.Content.ToString())
            {
                if (viewModel.RefItem == null)
                {
                    viewModel.btnSave.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                }
                else
                {
                    if (viewModel.RefItem.Name != result.ViewItem.LastValue.Value.ToString())
                    {
                        viewModel.btnSave.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                    }
                }
            }

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueAddObjects(string idInstance)
        {
            var processId = LogStartMethod();
            var viewItemId = Request.Params["viewItem[ViewItemId]"];
            var viewItemValue = Request.Params["viewItem[LastValue][Value]"];
            var viewItemType = Request.Params["viewItem[LastValue][ViewItemType]"];
            
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (string.IsNullOrEmpty(viewItemId))
            {
                resultAction.IsSuccessful = false;
                resultAction.ResultMessage = "No ViewItemId provided!";
                LogEndMethod(processId:processId);
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            
            var viewModel = GetViewModelAddObjects(idInstance);
            var isAddObjectsRow = false;
            if (viewItemId.StartsWith(viewModel.AddObjectsPrefix))
            {
                isAddObjectsRow = true;
            }

            ViewItem viewItem = null;

            if (!isAddObjectsRow)
            {
                viewItem = viewModel.ViewItems.FirstOrDefault(viewItm => viewItm.ViewItemId == viewItemId);
            }
            else
            {
                viewItem = viewModel.AddObjectRows.FirstOrDefault(viewItm => viewItm.ViewItemId == viewItemId);
            }

            if (viewItem == null)
            {
                resultAction.IsSuccessful = false;
                resultAction.ResultMessage = "No ViewItem found!";
                LogEndMethod(processId:processId);
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            
            var controller = GetController();
            viewItem.ChangeViewItemValue(viewItemType, viewItemValue);

            var result = await viewModel.CheckAddObjectRows(controller);
            if (result.GUID == controller.Globals.LState_Error.GUID)
            {
                resultAction.IsSuccessful = false;
                resultAction.ResultMessage = result.Additional1;
                LogEndMethod(processId:processId);                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }
            resultAction.ViewItems.AddRange(viewModel.AddObjectRows);

            var enableSaveButton = viewModel.EnableSave();
            var enableRemoveButton = viewModel.EnableRemove();


            resultAction.ViewItems.Add(viewModel.saveObjects);
            resultAction.ViewItems.Add(viewModel.removeRow);
            resultAction.ViewItems.Add(viewModel.applySaved);

            viewModel.saveObjects.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableSaveButton);
            viewModel.removeRow.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableRemoveButton);
            viewModel.applySaved.ChangeViewItemValue(ViewItemType.Enable.ToString(), enableRemoveButton);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveObjectOrObjectName(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelObjectNameEdit(idInstance);
            var controller = GetController();

            var result = new ResultSimpleView<List<clsOntologyItem>>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            var guid = viewModel.inpGUID.GetViewItemValueItem(ViewItemType.Content.ToString()).Value.ToString();
            var taskResult = await controller.SaveObject(viewModel.RefItem, viewModel.refParentItem, viewModel.inpName.LastValue.Value.ToString(), guid);

            if (taskResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

            }

            result.Result = taskResult.Result;

            viewModel.RefItem = null;
            viewModel.inpName.ChangeViewItemValue(ViewItemType.Content.ToString(), "");
            viewModel.inpName.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.inpGUID.ChangeViewItemValue(ViewItemType.Content.ToString(), controller.Globals.NewGUID);
            viewModel.btnSave.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            result.ViewItems.Add(viewModel.inpGUID);
            result.ViewItems.Add(viewModel.inpName);
            result.ViewItems.Add(viewModel.btnSave);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueEditAttribute(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelEditAttribute(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var viewItemValue = result.ViewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueBoolAttribute(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelBoolAttribute(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var viewItemValue = result.ViewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueDateTimeAttribute(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelDateTimeAttribute(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var viewItemValue = result.ViewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueLongAttribute(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelLongAttribute(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var viewItemValue = result.ViewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueDoubleAttribute(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelDoubleAttribute(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var viewItemValue = result.ViewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueStringAttribute(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelStringAttribute(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var viewItemValue = result.ViewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }


            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfig()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(EditItem), Url.Action(nameof(ReadInGrid)), Url.Action(nameof(CreateInGrid)), Url.Action(nameof(UpdateInGrid)), Url.Action(nameof(DestroyInGrid)), false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveData(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelOItem(idInstance);

            var controller = GetController();

            var dataRows = viewModel.RowItems;
            foreach (var dataRow in dataRows)
            {
                var oItem = new clsOntologyItem
                {
                    GUID = dataRow.Guid,
                    Name = dataRow.Name,
                    GUID_Parent = dataRow.GuidParent,
                    Type = dataRow.ItemType
                };

                var resultTask = await controller.SaveOItem(oItem);

                if (resultTask.GUID == logStates.LogState_Success.GUID)
                {
                    dataRow.EditItemState = EditItemState.Saved;
                    dataRow.Comment = "";
                }
                else if (resultTask.GUID == logStates.LogState_Error.GUID)
                {
                    dataRow.EditItemState = EditItemState.SaveError;
                    dataRow.Comment = Messages.EditOItem_Update_DataError;
                }
                
            }
            
            var result = new ResultSaveData
            {
                isOk = true,
                Message = "",
                SaveData = dataRows
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddDataRow(string selectedType, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultAddRow
            {
                isOk = true,
                ItemToAdd = new EditItem()
            };


            var viewModel = GetViewModelOItem(idInstance);

            var controller = GetController();

            result.ItemToAdd.IdViewInstance = viewModel.IdInstance;
            result.ItemToAdd.Guid = Guid.NewGuid().ToString().Replace("-", "");
            result.ItemToAdd.Save = true;
            
            result.ItemToAdd.EditItemState = EditItemState.None;
            

            if (selectedType == types.AttributeType)
            {
                result.ItemToAdd.GuidParent = viewModel.refParentItem?.GUID_Related;
                var selectedDataType = controller.DataTypes.FirstOrDefault(dType => dType.GUID == viewModel.refParentItem.GUID);

                if (selectedDataType == null)
                {
                    result.isOk = false;
                    result.Message = Messages.EditOItem_Error_WrongDataType;
                }
                else
                {
                    result.ItemToAdd.ItemType = selectedType;
                    viewModel.RowItems.Add(result.ItemToAdd);
                }
                    
            }
            else if (selectedType == types.RelationType)
            {
                result.ItemToAdd.ItemType = selectedType;
                viewModel.RowItems.Add(result.ItemToAdd);
            }
            else if (selectedType == types.ClassType)
            {
                result.ItemToAdd.GuidParent = viewModel.refParentItem?.GUID;
                result.ItemToAdd.NameParent = viewModel.refParentItem?.Name;
                result.ItemToAdd.ItemType = selectedType;
                viewModel.RowItems.Add(result.ItemToAdd);
            }
            else if (selectedType == types.ObjectType)
            {
                result.ItemToAdd.GuidParent = viewModel.refParentItem?.GUID;
                result.ItemToAdd.NameParent = viewModel.refParentItem?.Name;
                result.ItemToAdd.ItemType = selectedType;
                viewModel.RowItems.Add(result.ItemToAdd);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ConnectedObjectNameEdit()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelObjectNameEdit(idInstance);

            viewItemList.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedAddObjects()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelAddObjects(idInstance);

            viewItemList.Add(viewModel.isListen);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedEditAttribute()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelEditAttribute(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedBoolAttribute()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelBoolAttribute(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedDateTimeAttribute()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelDateTimeAttribute(idInstance);

            viewModel.dateTimeValue.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedLongAttribute()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelLongAttribute(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedDoubleAttribute()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelDoubleAttribute(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedStringAttribute()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelStringAttribute(idInstance);

            viewItemList.Add(viewModel.listen);

            viewModel.listen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.listen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedObjectNameEdit()
        {

            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelObjectNameEdit(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedAddObjects()
        {

            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelAddObjects(idInstance);
            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.inpClass);
            viewItemList.Add(viewModel.saveObjects);
            viewItemList.Add(viewModel.showClassChoose);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.inpClass.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.showClassChoose.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.saveObjects.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedEditAttribute()
        {

            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelEditAttribute(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedBoolAttribute()
        {

            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelBoolAttribute(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedDateTimeAttribute()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelDateTimeAttribute(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedDoubleAttribute()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelDoubleAttribute(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedLongAttribute()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelLongAttribute(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedStringAttribute()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelStringAttribute(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelOItem(idInstance);

            viewModel.listenItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.listenItem.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
            viewModel.dropDownItemType.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.listenItem);
            viewItemList.Add(viewModel.dropDownItemType);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelOItem(idInstance);

            viewModel.listenItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.applyItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.newItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.dropDownItemType.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.saveItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.gridOitems.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            viewItemList.Add(viewModel.listenItem);
            viewItemList.Add(viewModel.newItem);
            viewItemList.Add(viewModel.dropDownItemType);
            viewItemList.Add(viewModel.saveItem);
            viewItemList.Add(viewModel.gridOitems);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyObjectNameEdit(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelObjectNameEdit(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyAddObjects(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelAddObjects(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult AddObjectsInit(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelAddObjects(idInstance ?? Guid.NewGuid().ToString());
            var resultAction = new ResultSimpleView<AddObjectsViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult AddObjects()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelAddObjects(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }
    }
}