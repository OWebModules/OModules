﻿using OModules.Services;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public abstract class AppControllerBase : Controller
    {
        private Logger logger = new Logger();

        public List<INameTransform> GetNameTransformController(List<string> idsClass)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();

            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });
            var transforms = memoryCache.GetOrSet<List<INameTransform>>(Session.SessionID, "NameTransformProvider", () =>
            {
                return NameTransformManager.GetNameTransformProviders(globals, true);
            });

            var transformProvidersResult = (from idClass in idsClass
                                            from trans in transforms
                                            where trans.IsResponsible(idClass)
                                            select trans).ToList();

            LogEndMethod(processId: processId);
            return transformProvidersResult;
        }

        protected string LogStartMethod([CallerMemberName] string callerMember = "", [CallerFilePath] string fileName = "", string processId = "")
        {
            if (string.IsNullOrEmpty(processId))
            {
                processId = Guid.NewGuid().ToString();
            }
            logger.FunctionEntry($"[Url: {Request.Url}] [ProcessId: {processId}] [FileName: {fileName}] [Function: {callerMember}]");
            return processId;
        }

        protected void LogEndMethod([CallerMemberName] string callerMember = "", [CallerFilePath] string fileName = "", string processId = "")
        {
            logger.FunctionLeave($"[Url: {Request.Url}] [ProcessId: {processId}] [FileName: {fileName}] [Function: {callerMember}]");
        }

        protected void LogError(string message, [CallerMemberName] string callerMember = "", [CallerFilePath] string fileName = "", string processId = "")
        {
            logger.Error($"[Url: {Request.Url}] [ProcessId: {processId}] [FileName: {fileName}] [Function: {callerMember}]: {message}");
        }

        protected void LogInfo(string message, [CallerMemberName] string callerMember = "", [CallerFilePath] string fileName = "", string processId = "")
        {
            logger.Info($"[Url: {Request.Url}] [ProcessId: {processId}] [FileName: {fileName}] [Function: {callerMember}]: {message}");
        }
    }
}