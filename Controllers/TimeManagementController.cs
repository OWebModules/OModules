﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.MessageOutput;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoMsg_Module.Validation;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeManagementModule.Models;

namespace OModules.Controllers
{
    public class TimeManagementController : AppControllerBase
    {
        private SecurityModule.SecurityController GetControllerSecurity()
        {

            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new SecurityModule.SecurityController(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private async System.Threading.Tasks.Task<TimeManagementModule.TimeManagementController> GetController(clsOntologyItem userItem, clsOntologyItem groupItem)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new TimeManagementModule.TimeManagementController(globals);
            await result.SetBaseConfig(userItem, groupItem);

            LogEndMethod(processId:processId);
            return result;

        }

        private TimeManagementEntryListViewModel GetViewModelTimeManagementEntryList(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<TimeManagementEntryListViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new TimeManagementEntryListViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedTimeManagementEntryList)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedTimeManagementEntryList)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyTimeManagementEntryList)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfigTimeManagementEntryList)),
                    ActionValidateReference = Url.Action(nameof(ValidateTimeManagementEntryListReference)),
                    ActionTimeManagementListInit = Url.Action(nameof(TimeManagementEntryListInit)),
                    ActionGetDropDownConfigGroups = Url.Action(nameof(DropDownController.GetDropDownConfigForObjects), nameof(DropDownController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueTimeManagementEntryList)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private TimeManagementEntryEditViewModel GetViewModelTimeManagementEdit(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<TimeManagementEntryEditViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new TimeManagementEntryEditViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedTimeManagementEntryEdit)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedTimeManagementEntryEdit)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyTimeManagementEntryEdit)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceTimeManagementEntryEdit)),
                    ActionItemComBarInit = Url.Action(nameof(ObjectEditController.ItemComBarInit), nameof(ObjectEditController).Replace("Controller", "")),
                    ActionTimeManagementEntryEditInit = Url.Action(nameof(TimeManagementEntryEditInit)),
                    ActionTimeManagementListInit = Url.Action(nameof(TimeManagementEntryListInit)),
                    ActionGetDropDownConfigGroups = Url.Action(nameof(DropDownController.GetDropDownConfigForObjects), nameof(DropDownController).Replace("Controller", "")),
                    ActionInitClassTree = Url.Action(nameof(ClassTreeController.ClassTreeInit), nameof(ClassTreeController).Replace("Controller", "")),
                    ActionObjectListLinit = Url.Action(nameof(OItemListController.ObjectListInit), nameof(OItemListController).Replace("Controller", "")),
                    ActionSaveTimeManagementEntry = Url.Action(nameof(SaveSelectedEntry)),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueTimeManagementEntryEdit)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueTimeManagementEntryList(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTimeManagementEntryList(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ChangeViewItemValueTimeManagementEntryEdit(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTimeManagementEdit(idInstance);
            var controller = await GetController(viewModel.UserItem, viewModel.GroupItem);
            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                SelectorPath = viewModel.SelectorPath
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            if (result.ViewItem.ViewItemId == viewModel.inpRelated.ViewItemId && result.ViewItem.LastValue.ViewItemType == ViewItemType.Id.ToString())
            {
                if (result.ViewItem.LastValue.Value != null)
                {
                    var id = result.ViewItem.LastValue.Value.ToString();
                    var resultItem = await controller.GetOItem(id);
                    if (resultItem.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        resultAction.IsSuccessful = false;
                        resultAction.ResultMessage = "The Reference cannot be determined!";
                        LogEndMethod(processId:processId);
                        return Json(resultAction, JsonRequestBehavior.AllowGet);
                    }
                    viewModel.inpRelated.ChangeViewItemValue(ViewItemType.Content.ToString(), resultItem.Result.ObjectItem.Name);
                }
                
            }
            else if (result.ViewItem.ViewItemId == viewModel.inpGroup.ViewItemId && result.ViewItem.LastValue.ViewItemType == ViewItemType.Id.ToString())
            {
                var idGroup = result.ViewItem.LastValue.Value.ToString();
                var securityController = GetControllerSecurity();

                var groupResult = await securityController.GetOItem(idGroup, securityController.Globals.Type_Object);
                var groupItem = groupResult.Result;
                if (groupResult.Result.GUID == securityController.Globals.LState_Error.GUID || groupItem == null || groupItem.GUID_Parent != viewModel.idClassGroup)
                {
                    resultAction.IsSuccessful = false;
                    resultAction.ResultMessage = "The Group cannot be set!";
                    LogEndMethod(processId:processId);
                    return Json(resultAction, JsonRequestBehavior.AllowGet);
                }

                viewModel.GroupItem = groupItem;
            }

            resultAction.ViewItems.Add(viewModel.buttonSave);
            var validationResult = IsSaveButtonValid(viewModel);
            var messageOutput = new OWMessageOutput(viewModel.IdInstance);
            if (!validationResult.IsValid)
            {
                messageOutput.OutputError(validationResult.ToString());
            }
            else
            {
                messageOutput.OutputInfo("Validation Success!");
            }

            viewModel.buttonSave.ChangeViewItemValue(ViewItemType.Enable.ToString(), validationResult.IsEnabled);
            viewModel.buttonRemoveRelated.ChangeViewItemValue(ViewItemType.Enable.ToString(), viewModel.SelectedEntry != null && !string.IsNullOrEmpty(viewModel.SelectedEntry.IdReference));
            viewModel.buttonSave.ChangeViewItemValue(ViewItemType.CSS.ToString(), new CSSItem { Key = "border-color", Value = validationResult.IsValid ? "" : "red" });

            resultAction.ViewItems.Add(viewModel.buttonSave);
            resultAction.ViewItems.Add(result.ViewItem);
            resultAction.ViewItems.Add(viewModel.buttonRemoveRelated);
            

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        private ValidationResult IsSaveButtonValid( TimeManagementEntryEditViewModel viewModel)
        {
            var result = new ValidationResult
            {
                IsValid = true,
                IsEnabled = true,
                IsVisible = true
            };
            var idGroupValue = viewModel.inpGroup.GetViewItemValue(ViewItemType.Id.ToString())?.ToString();
            var nameValue = viewModel.inpName.GetViewItemValue(ViewItemType.Content.ToString())?.ToString();
            var startDateValue = viewModel.inpStart.GetViewItemValue(ViewItemType.Content.ToString());
            var endDateValue = viewModel.inpEnde.GetViewItemValue(ViewItemType.Content.ToString());
            var radioWorkEnabledValue = viewModel.radioWork.GetViewItemValue(ViewItemType.Checked.ToString());
            var radioPrivateEnabledValue = viewModel.radioPrivate.GetViewItemValue(ViewItemType.Checked.ToString());
            var radioUrlaubEnabledValue = viewModel.radioUrlaub.GetViewItemValue(ViewItemType.Checked.ToString());
            var radioKrankheitEnabledValue = viewModel.radioKrankheit.GetViewItemValue(ViewItemType.Checked.ToString());
            var relatedIdValue = viewModel.inpRelated.GetViewItemValue(ViewItemType.Id.ToString());

            if (viewModel.SelectedEntry != null)
            {
                viewModel.SelectedEntry.IdGroup = idGroupValue;

                viewModel.SelectedEntry.NameTimeManagement = nameValue;

                if (startDateValue != null)
                {
                    viewModel.SelectedEntry.Start = (DateTime)startDateValue;
                }
                if (endDateValue != null)
                {
                    viewModel.SelectedEntry.Ende = (DateTime)endDateValue;
                }

                if ((bool)radioWorkEnabledValue)
                {
                    viewModel.SelectedEntry.IdLogState = viewModel.StateWork.GUID;
                }
                else if ((bool)radioPrivateEnabledValue)
                {
                    viewModel.SelectedEntry.IdLogState = viewModel.StatePrivate.GUID;
                }
                else if ((bool)radioUrlaubEnabledValue)
                {
                    viewModel.SelectedEntry.IdLogState = viewModel.StateUrlaub.GUID;
                }
                else if ((bool)radioKrankheitEnabledValue)
                {
                    viewModel.SelectedEntry.IdLogState = viewModel.StateKrankheit.GUID;
                }

                viewModel.SelectedEntry.IdReference = relatedIdValue?.ToString() ?? string.Empty;

                if (result.IsEnabled)
                {
                    var validationResult = viewModel.SelectedEntry.IsValid();
                    result.IsValid = validationResult.IsValid;
                    result.IsEnabled = validationResult.IsValid;
                    if (result.IsEnabled)
                    {
                        result.IsEnabled = viewModel.SelectedEntry.IsDirty();
                    }
                    result.AddMessage(validationResult.ToString());
                }
            }

            return result;
        }

        [Authorize]
        public ActionResult ConnectedTimeManagementEntryList()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTimeManagementEntryList(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedTimeManagementEntryEdit()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTimeManagementEdit(idInstance);

            viewModel.inpName.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.inpName);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedTimeManagementEntryEdit()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTimeManagementEdit(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedTimeManagementEntryList()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelTimeManagementEntryList(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyTimeManagementEntryList(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTimeManagementEntryList(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyTimeManagementEntryEdit(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTimeManagementEdit(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateTimeManagementEntryListReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTimeManagementEntryList(idInstance);

            var controller = await GetController(viewModel.UserItem, viewModel.GroupItem);

            var refItemTask = await controller.GetOItem(refItem.GUID);


            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while getting the RefItem", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.ClassObject = refItemTask.Result;

            viewModel.SetWindowTitle(viewModel.ClassObject.ToString());
            result.ViewItems.Add(viewModel.windowTitle);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private void SetEnabledOfViewItems(List<ViewItem> viewItems, bool enabled)
        {
            foreach (var viewItem in viewItems)
            {
                viewItem.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            }
        }

        private List<ViewItem> GetDataViewItems(TimeManagementEntryEditViewModel viewModel)
        {
            return new List<ViewItem>
            {
                viewModel.inpName,
                viewModel.inpStart,
                viewModel.inpEnde,
                viewModel.buttonListenRelated,
                viewModel.radioWork,
                viewModel.radioPrivate,
                viewModel.radioKrankheit,
                viewModel.radioUrlaub
            };
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceTimeManagementEntryEdit(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelTimeManagementEdit(idInstance);

            var controller = await GetController(viewModel.UserItem, viewModel.GroupItem);

            var request = new GetTimeManagementEntryRequest(viewModel.UserItem, viewModel.GroupItem)
            {
                IdReference = refItem?.GUID,
                MessageOutput = new OWMessageOutput(viewModel.IdInstance)
            };
            var getTimeManagementEntry = await controller.GetTimeManagementEntry(request);

            var result = new ResultSimpleView<TimeManagementEntry>
            {
                IsSuccessful = getTimeManagementEntry.ResultState.GUID == controller.Globals.LState_Success.GUID,
                Message = getTimeManagementEntry.ResultState.Additional1,
                SelectorPath = viewModel.SelectorPath,
                Result = getTimeManagementEntry.Result
            };

            viewModel.SelectedEntry = getTimeManagementEntry.Result;

            result.ViewItems.Add(viewModel.inpGroup);
            result.ViewItems.Add(viewModel.inpName);
            result.ViewItems.Add(viewModel.inpRelated);
            result.ViewItems.Add(viewModel.buttonListenRelated);
            result.ViewItems.Add(viewModel.inpStart);
            result.ViewItems.Add(viewModel.inpEnde);
            result.ViewItems.Add(viewModel.radioWork);
            result.ViewItems.Add(viewModel.radioKrankheit);
            result.ViewItems.Add(viewModel.radioPrivate);
            result.ViewItems.Add(viewModel.radioUrlaub);
            result.ViewItems.Add(viewModel.buttonRemoveRelated);

            viewModel.inpGroup.ChangeViewItemValue(ViewItemType.Id.ToString(), viewModel.GroupItem.GUID);
            viewModel.inpGroup.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.inpName.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.inpName.ChangeViewItemValue(ViewItemType.Content.ToString(), result.Result.NameTimeManagement);
            viewModel.buttonListenRelated.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.inpRelated.ChangeViewItemValue(ViewItemType.Content.ToString(), result.Result.NameReference);
            viewModel.inpRelated.ChangeViewItemValue(ViewItemType.Id.ToString(), result.Result.IdReference);
            viewModel.inpStart.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.inpStart.ChangeViewItemValue(ViewItemType.Content.ToString(), result.Result.Start);
            viewModel.inpEnde.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.inpEnde.ChangeViewItemValue(ViewItemType.Content.ToString(), result.Result.Ende);
            viewModel.radioKrankheit.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.radioKrankheit.ChangeViewItemValue(ViewItemType.Checked.ToString(), result.Result.IdLogState == controller.StateKrank.GUID);
            viewModel.radioPrivate.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.radioPrivate.ChangeViewItemValue(ViewItemType.Checked.ToString(), result.Result.IdLogState == controller.StatePrivate.GUID);
            viewModel.radioUrlaub.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.radioUrlaub.ChangeViewItemValue(ViewItemType.Checked.ToString(), result.Result.IdLogState == controller.StateUrlaub.GUID);
            viewModel.radioWork.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.radioWork.ChangeViewItemValue(ViewItemType.Checked.ToString(), result.Result.IdLogState == controller.StateWork.GUID);
            viewModel.buttonRemoveRelated.ChangeViewItemValue(ViewItemType.Enable.ToString(), !string.IsNullOrEmpty(viewModel.SelectedEntry.IdReference));

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetGridConfigTimeManagementEntryList(string idInstance)
        {
            var viewModel = GetViewModelTimeManagementEntryList(idInstance);

            var controller = await GetController(viewModel.UserItem, viewModel.GroupItem);

            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(TimeManagementEntry), nameof(GetDataTimeManagementEntryList), null, null, null, false, false, false);
            var actionResult = new ResultSimpleView<Dictionary<string, object>>
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath,
                Result = gridConfig
            };

            return Json(actionResult);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetDataTimeManagementEntryList(string idInstance, string idGroup, string rangeName)
        {
            var viewModel = GetViewModelTimeManagementEntryList(idInstance);

            if (idGroup == null)
            {
                return Json(new List<TimeManagementEntry>(), JsonRequestBehavior.AllowGet);
            }
            if (viewModel.GroupItem == null || viewModel.GroupItem.GUID != idGroup)
            {
                var securityController = GetControllerSecurity();

                var groupItemResult = await securityController.GetOItem(idGroup, securityController.Globals.Type_Object);

                if (groupItemResult.ResultState.GUID == securityController.Globals.LState_Error.GUID)
                {
                    return Json(new List<TimeManagementEntry>(), JsonRequestBehavior.AllowGet);
                }

                if (groupItemResult.Result == null || groupItemResult.Result.GUID_Parent != viewModel.idClassGroup)
                {
                    return Json(new List<TimeManagementEntry>(), JsonRequestBehavior.AllowGet);
                }

                viewModel.GroupItem = groupItemResult.Result;
            }
            var range = viewModel.FilterList.FirstOrDefault(filter => filter.RangeName == rangeName);
            var controller = await GetController(viewModel.UserItem, viewModel.GroupItem);
            var secController = GetControllerSecurity();

            var userId = User.Identity.GetUserId();
            if (viewModel.UserItem == null || viewModel.UserItem.GUID != userId)
            {
                var userItemResult = await secController.GetUser(userId);

                if (userItemResult.Result.GUID == controller.Globals.LState_Error.GUID || userItemResult.Result == null)
                {
                    return Json(new List<TimeManagementEntry>(), JsonRequestBehavior.AllowGet);
                }

                viewModel.UserItem = userItemResult.UserItem;
            }
            
            var controllerResult = await controller.GetTimeManagementEntryList(viewModel.UserItem, viewModel.GroupItem, viewModel.ClassObject != null ? viewModel.ClassObject.ObjectItem : null, range.All ? null : range);
            if (controllerResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                return Json(new List<TimeManagementEntry>(), JsonRequestBehavior.AllowGet);
            }

            var result = new KendoGridDataResult<TimeManagementEntry>(controllerResult.Result.EntryList);
            var resultJson = Json(result, JsonRequestBehavior.AllowGet);
            resultJson.MaxJsonLength = int.MaxValue;
            return resultJson;
        }

        // GET: TimeManagement
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult TimeManagementEntryEdit()
        {
            var viewModel = GetViewModelTimeManagementEdit(Guid.NewGuid().ToString());

            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> TimeManagementEntryEditInit(string idInstance, List<string> selectorPath)
        {
            var processId = LogStartMethod();
            var resultAction = new ResultSimpleView<TimeManagementEntryEditViewModel>
            {
                IsSuccessful = true,
                SelectorPath = selectorPath
            };

            if (string.IsNullOrEmpty(idInstance))
            {
                idInstance = Guid.NewGuid().ToString();
            }
            var securityConnector = GetControllerSecurity();
            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);
            var groupItem = await securityConnector.GetGroup(userId);

            var viewModel = GetViewModelTimeManagementEdit(idInstance);
            viewModel.UserItem = userItem.UserItem;
            viewModel.GroupItem = groupItem.GroupItem;
            var controller = await GetController(viewModel.UserItem, viewModel.GroupItem);
            viewModel.idClassGroup = "1b1f843c19b74ae1a73f6191585ec5c6";

            viewModel.StateWork = controller.StateWork;
            viewModel.StatePrivate = controller.StatePrivate;
            viewModel.StateUrlaub = controller.StateUrlaub;
            viewModel.StateKrankheit = controller.StateKrank;
            viewModel.radioWork.ChangeViewItemValue(ViewItemType.Id.ToString(), controller.StateWork.GUID);
            viewModel.radioPrivate.ChangeViewItemValue(ViewItemType.Id.ToString(), controller.StatePrivate.GUID);
            viewModel.radioUrlaub.ChangeViewItemValue(ViewItemType.Id.ToString(), controller.StateUrlaub.GUID);
            viewModel.radioKrankheit.ChangeViewItemValue(ViewItemType.Id.ToString(), controller.StateKrank.GUID);

            viewModel.SelectorPath = selectorPath;
            resultAction.Result = viewModel;

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> SaveSelectedEntry(string idInstance)
        {
            var processId = LogStartMethod();
            var actionResult = new ResultSimpleView<TimeManagementEntry>
            {
                IsSuccessful = true
            };
            var viewModel = GetViewModelTimeManagementEdit(idInstance);
            var messageOutput = new OWMessageOutput(viewModel.IdInstance);
            actionResult.SelectorPath = viewModel.SelectorPath;

            if (viewModel.SelectedEntry == null)
            {
                
                actionResult.IsSuccessful = false;
                actionResult.Message = "No Entry found!";
                messageOutput.OutputError(actionResult.Message);
                LogEndMethod(processId:processId);                return Json(actionResult, JsonRequestBehavior.AllowGet);
            }

            var controller = await GetController(viewModel.UserItem, viewModel.GroupItem);
            var saveResult = await controller.SaveEntry(viewModel.SelectedEntry);

            if (saveResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                actionResult.IsSuccessful = false;
                actionResult.Message = saveResult.ResultState.Additional1;
                messageOutput.OutputError(actionResult.Message);
                LogEndMethod(processId:processId);                return Json(actionResult, JsonRequestBehavior.AllowGet);
            }
            viewModel.SelectedEntry = saveResult.Result;
            messageOutput.OutputInfo("Saved Entry!");
            viewModel.buttonSave.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            actionResult.ViewItems.Add(viewModel.buttonSave);
            actionResult.Result = saveResult.Result;
            LogEndMethod(processId:processId);
            return Json(actionResult, JsonRequestBehavior.AllowGet);
        }

        private async System.Threading.Tasks.Task PrepareTimeManagementEntryListViewModel(TimeManagementEntryListViewModel viewModel, List<string> selectorPath)
        {
            var securityConnector = GetControllerSecurity();
            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);
            var groupItem = await securityConnector.GetGroup(userId);

            
            viewModel.UserItem = userItem.UserItem;
            viewModel.GroupItem = groupItem.GroupItem;
            var controller = await GetController(viewModel.UserItem, viewModel.GroupItem);
            viewModel.idClassTimeManagement = controller.ClassTimeManagement.GUID;
            viewModel.idClassGroup = "1b1f843c19b74ae1a73f6191585ec5c6";

            viewModel.SelectorPath = selectorPath;
            viewModel.FilterList = controller.BaseConfig.FilterList;
            viewModel.SelectedFilter = viewModel.FilterList.FirstOrDefault(filter => filter.RangeName == "This Week");
            var dropdownItems = viewModel.FilterList.Select(filter => new KendoDropDownItem
            {
                Text = filter.RangeName,
                Value = filter.RangeName
            }).ToList();

            viewModel.dropDownConfig = new KendoDropDownConfig
            {
                optionLabel = "Select a Filter...",
                dataSource = dropdownItems
            };
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> TimeManagementEntryList()
        {
            var viewModel = GetViewModelTimeManagementEntryList(Guid.NewGuid().ToString());

            await PrepareTimeManagementEntryListViewModel(viewModel, new List<string>());

            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> TimeManagementEntryListInit(string idInstance, List<string> selectorPath)
        {
            var processId = LogStartMethod();
            var resultAction = new ResultSimpleView<TimeManagementEntryListViewModel>
            {
                IsSuccessful = true,
                SelectorPath = selectorPath
            };

            if (string.IsNullOrEmpty(idInstance))
            {
                idInstance = Guid.NewGuid().ToString();
            }

            var viewModel = GetViewModelTimeManagementEntryList(idInstance);
            await PrepareTimeManagementEntryListViewModel(viewModel, selectorPath);
            resultAction.Result = viewModel;

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }
    }
}