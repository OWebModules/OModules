﻿using MediaStore_Module.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using TypedTaggingModule.Models;

namespace OModules.Controllers
{
    public class HtmlViewerController : AppControllerBase
    {
        

        private clsLogStates logStates = new clsLogStates();
        private clsTypes types = new clsTypes();

        private HtmlEditorModule.HtmlEditorConnector GetControllerHtmlEdit(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new HtmlEditorModule.HtmlEditorConnector(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private MediaStore_Module.FileDownloadController GetFileDownloadController(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new MediaStore_Module.FileDownloadController(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private HtmlViewerViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<HtmlViewerViewModel>(Session.SessionID, key, () =>
            {

                var viewModelNew = new HtmlViewerViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigHtmlViewer
                    {
                        ActionConnected = Url.Action(nameof(Connected)),
                        ActionDisconnected = Url.Action(nameof(Disconnected)),
                        ActionSetViewReady = Url.Action(nameof(SetViewReady)),
                        ActionValidateReference = Url.Action(nameof(ValidateReference)),
                        ActionGetHtml = Url.Action(nameof(GetHtml)),
                        ActionGetRawHtml = Url.Action(nameof(GetRawHtml)),
                        ActionGetTags = Url.Action(nameof(GetTags))

                    },
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            
            var controller = GetControllerHtmlEdit(nameof(HtmlEditorModule.HtmlEditorConnector) + HttpContext.User.Identity.Name);
            var refItemTask = await controller.GetOItem(refItem.GUID, types.ObjectType);
            
            viewModel.refItem = refItemTask.Result;
            
            var resultViewItems = new List<ViewItem>();
            if (viewModel.refItem != null)
            {
                var refItemParent = await controller.GetOItem(refItemTask.Result.GUID_Parent, types.ClassType);
                viewModel.refParentItem = refItemParent.Result;


                viewModel.SetWindowTitle($"{refItemTask.Result.Name} ({refItemParent.Result.Name})");
                resultViewItems.Add(viewModel.windowTitle);
            }

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetHtml(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            var refItem = viewModel.refItem;
            var result = new ResultGetRawHtml
            {
                IsSuccessful = true
            };

            if (refItem == null)
            {
                result.IsSuccessful = true;
                result.redirectUrl = Url.Action(nameof(ToBeContinued));
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            //result.idHtmlDocument = refItem.GUID;
            //result.url = Url.Action(nameof(GetRawHtml));



            //return Json(result, JsonRequestBehavior.AllowGet);

            var controller = GetControllerHtmlEdit(nameof(HtmlEditorModule.HtmlEditorConnector) + HttpContext.User.Identity.Name);
            var htmlObject = await controller.GetHtmlDocumentForViewer(Url.Action(nameof(DocViewer)), refItem.GUID);
            if (htmlObject.Result.GUID == logStates.LogState_Error.GUID)
            {
                LogError("The page cannot be loaded", processId: processId);
                result.IsSuccessful = false;
                result.Message = "Die Seite konnte nicht geladen werden!";
                result.redirectUrl = Url.Action(nameof(ToBeContinued));
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            if (htmlObject.OItemHtmlDocument == null)
            {
                result.IsSuccessful = true;
                result.Message = "Es ist keine Seite vorhanden!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            result.htmlDocId = htmlObject.OItemHtmlDocument.GUID;
            result.refId = refItem.GUID;

            var regexMedia = new Regex("img.*src=\".*Resources\\/UserGroupRessources\\/.{2}\\/.{2}\\/.{32}(\\..{3})");
            var regexFileId = new Regex("[A-Fa-f0-9]{32}");
            var matches = regexMedia.Matches(htmlObject.OAItemHtmlDocument.Val_String);
            var fileIds = new List<string>();
            foreach (Match match in matches)
            {
                var matchId = regexFileId.Match(match.Value);

                if (!matchId.Success) continue;

                if (!fileIds.Contains(matchId.Value))
                {
                    fileIds.Add(matchId.Value);
                }
            }

            if (fileIds.Any())
            {
                var getObjectsResult = await controller.GetObjects(fileIds);
                if (getObjectsResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    Response.StatusCode = 500;
                    Response.Status = getObjectsResult.ResultState.Additional1;
                    return null;
                }

                if (getObjectsResult.Result.Any())
                {
                    var fileDownloadController = GetFileDownloadController(nameof(MediaStore_Module.FileDownloadController) + HttpContext.User.Identity.Name);
                    var request = new GetDownloadFilesRequest(Server.MapPath("~/Resources/UserGroupRessources"), getObjectsResult.Result, string.Empty) { ZipFile = false };
                    var downloadResult = await fileDownloadController.GetDownloadFiles(request);
                    if (downloadResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
                    {
                        Response.StatusCode = 500;
                        Response.Status = downloadResult.ResultState.Additional1;
                        return null;
                    }

                }
            }
            

            result.htmlBase64 = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(htmlObject.OAItemHtmlDocument.Val_String));
            result.Styles = htmlObject.OAItemStyles.Select(style => System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(style.Val_String))).ToList();
            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetTags(string idHtmlDoc, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            var result = new ResultGetTags
            {
                IsSuccessful = true
            };


            var controller = GetControllerHtmlEdit(nameof(HtmlEditorModule.HtmlEditorConnector) + HttpContext.User.Identity.Name);

            var resultGetTags = await controller.GetTags(idHtmlDoc, Url.Action(nameof(DocViewer)), Url.Action(nameof(ModuleMgmtController.ModuleStarter), nameof(ModuleMgmtController).Replace("Controller", "")));
            result.Tags = resultGetTags.References;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetRawHtml(string idItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            
            var result = new ResultGetRawHtml
            {
                IsSuccessful = true
            };


            var controller = GetControllerHtmlEdit(nameof(HtmlEditorModule.HtmlEditorConnector) + HttpContext.User.Identity.Name);
            var refItem = await controller.GetOItem(idItem, controller.Globals.Type_Object);
            var htmlObject = await controller.GetHtmlDocumentForViewer(Url.Action(nameof(DocViewer)), refItem.Result.GUID);
            if (htmlObject.Result.GUID == logStates.LogState_Error.GUID)
            {
                LogError("The page cannot be loaded", processId: processId);
                result.IsSuccessful = false;
                result.Message = "Die Seite konnte nicht geladen werden!";
                result.redirectUrl = Url.Action(nameof(ToBeContinued));
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            result.refId = idItem;
            result.htmlBase64 = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(htmlObject.OAItemHtmlDocument.Val_String));

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        // GET: HtmlViewer
        public ActionResult DocViewer()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);            return View(viewModel);
        }

        [Authorize]
        public ActionResult ToBeContinued()
        {
            var processId = LogStartMethod();
            LogEndMethod(processId:processId);            return View();
        }
    }

    public class ResultGetHtml
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }

        public string idHtmlDocument { get; set; }
        public string url { get; set; }
    }

    public class ResultGetRawHtml
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }

        public string refId { get; set; }
        public string htmlDocId { get; set; }
        public string htmlBase64 { get; set; }
        public string redirectUrl { get; set; }
        public List<string> Styles { get; set; } = new List<string>();
    }

}