﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Caching;

namespace OModules.Controllers
{
    public class InMemoryCache : ICacheService
    {
        public T GetOrSet<T>(string session, string cacheKey, Func<T> getItemCallback) where T : class
        {
            cacheKey = $"_{session}_{cacheKey}";
            T item = MemoryCache.Default.Get(cacheKey) as T;
            if (item == null)
            {
                item = getItemCallback();
                if (item != null)
                {
                    MemoryCache.Default.Add(cacheKey, item, DateTime.Now.AddMinutes(1440));
                }
            }
            return item;
        }

        public void RemoveKey(string session, string cacheKey)
        {
            cacheKey = $"_{session}_{cacheKey}";
            object item = MemoryCache.Default.Get(cacheKey);
            if (MemoryCache.Default.Contains(cacheKey))
            {
                MemoryCache.Default.Remove(cacheKey);
            }
            
        }
    }

    interface ICacheService
    {
        T GetOrSet<T>(string session, string cacheKey, Func<T> getItemCallback) where T : class;
    }
}
