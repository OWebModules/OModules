﻿using OModules.Models;
using OModules.Models.Requests;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OModules.Controllers
{
    [Authorize]
    public class OItemListApiController : ApiControllerBase
    {
        

        private OntologyItemsModule.OItemListController GetController(string idSession)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(idSession, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.OItemListController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        [Route("OItemListApi/GetAttributeTypes")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetAttributeTypes(RequestAttributeTypes request)
        {
            var processId = LogStartMethod();

            var controller = GetController(request.idSession);

            clsOntologyItem filterItem = null;
            if (!string.IsNullOrEmpty(request.idAttributeType) ||
                !string.IsNullOrEmpty(request.nameAttributeType) ||
                !string.IsNullOrEmpty(request.idDataType))
            {
                filterItem = new clsOntologyItem
                {
                    GUID = request.idAttributeType,
                    Name = request.nameAttributeType,
                    GUID_Parent = request.idDataType
                };
            }
            var resultTask = await controller.GetAttributeTypeList(filterItem);

            var result = Json(new { data = resultTask.GridItems });

            LogEndMethod(processId:processId);
            return result;
        }

        [Route("OItemListApi/GetRelationTypes")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetRelationTypes(RequestRelationTypes request)
        {
            var processId = LogStartMethod();

            var controller = GetController(request.idSession);

            clsOntologyItem filterItem = null;
            if (!string.IsNullOrEmpty(request.idRelationType) ||
                !string.IsNullOrEmpty(request.nameRelationType))
            {
                filterItem = new clsOntologyItem
                {
                    GUID = request.idRelationType,
                    Name = request.nameRelationType
                };
            }

            var resultTask = await controller.GetRelationTypeList(filterItem);

            var result = Json(new { data = resultTask.GridItems });

            LogEndMethod(processId:processId);
            return result;
        }

        [Route("OItemListApi/GetObjects")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetObjects(RequestObjects request)
        {
            var processId = LogStartMethod();
            
            if (string.IsNullOrEmpty(request.idParent)) return Json(new List<object>());


            var controller = GetController(request.idSession);
            LogInfo($"Get Objects of {request.idParent}");
            var resultTask = await controller.GetObjectList(request.idParent, request.nameObject);

            LogInfo($"Count of Objects: {resultTask.GridItems.Count}");
            var resultItem = new KendoGridDataResult<InstanceViewItem>(resultTask.GridItems);
            var result = Json(resultItem);

            LogEndMethod(processId:processId);
            return result;
        }

        [Route("OItemListApi/GetObjectAttributes")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetObjectAttributes(RequestObjectRelations request)
        {
            var processId = LogStartMethod();
            
            var iNodeType = int.Parse(request.nodeType);

            var nodeItem = new ObjectRelNode((NodeType)iNodeType)
            {
                IdLeft = request.idLeft,
                NameLeft = request.nameLeft,
                IdRight = request.idRight,
                NameRight = request.nameRight,
                IdRelationType = request.idRelationType,
                NameRelationType = request.nameRelationType

            };


            var controller = GetController(request.idSession);

            var resultTask = await controller.GetObjectAttributeList(nodeItem);

            var result = Json(new { data = resultTask.GridItems });

            LogEndMethod(processId:processId);
            return result;
        }

        [Route("OItemListApi/GetObjectRelationsLeftRight")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetObjectRelationsLeftRight(RequestObjectRelations request)
        {
            var processId = LogStartMethod();

            var result = await GetObjectRelationsLeftRight(false, request);

            LogEndMethod(processId:processId);
            return result;
        }

        [Route("OItemListApi/GetObjectRelationsLeftRightOther")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetObjectRelationsLeftRightOther(RequestObjectRelations request)
        {
            var processId = LogStartMethod();
            

            LogEndMethod(processId:processId);
            return await GetObjectRelationsLeftRight(true, request);
        }

        [Route("OItemListApi/GetObjectRelationsRightLeft")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetObjectRelationsRightLeft(RequestObjectRelations request)
        {
            var processId = LogStartMethod();

            LogEndMethod(processId:processId);
            return await GetObjectRelationsRightLeft(false, request);
        }

        [Route("OItemListApi/GetObjectRelationsRightLeftOther")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetObjectRelationsRightLeftOther(RequestObjectRelations request)
        {
            var processId = LogStartMethod();

            LogEndMethod(processId:processId);
            return await GetObjectRelationsRightLeft(true, request);
        }

        [Route("OItemListApi/GetObjectRelationsOmni")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetObjectRelationsOmni(RequestObjectRelations request)
        {
            var processId = LogStartMethod();
            
            var idParent = request.idRight;
            if (idParent == null) return Json(new List<object>());


            var controller = GetController(request.idSession);

            var resultTask = await controller.GetObjectRelation(idParent);

            var result = Json(new { data = resultTask.GridItems });

            LogEndMethod(processId:processId);
            return result;
        }

        private async System.Threading.Tasks.Task<IHttpActionResult> GetObjectRelationsLeftRight(bool isOther, RequestObjectRelations request)
        {
            var processId = LogStartMethod();
            var iNodeType = int.Parse(request.nodeType);

            var nodeItem = new ObjectRelNode((NodeType)iNodeType)
            {
                IdLeft = request.idLeft,
                NameLeft = request.nameLeft,
                IdRight = request.idRight,
                NameRight = request.nameRight,
                IdRelationType = request.idRelationType,
                NameRelationType = request.nameRelationType

            };

            if (nodeItem == null) return Json(new List<object>());


            var controller = GetController(request.idSession);

            var resultTask = await controller.GetObjectRelationConscious(nodeItem, isOther);

            var resultItem = new KendoGridDataResult<ObjectObject_Conscious>(resultTask.GridItems);
            var result = Json(resultItem);

            LogEndMethod(processId:processId);
            return result;
        }

        private async System.Threading.Tasks.Task<IHttpActionResult> GetObjectRelationsRightLeft(bool isOther, RequestObjectRelations request)
        {
            var processId = LogStartMethod();
            var nodeType = int.Parse(request.nodeType);

            var nodeItem = new ObjectRelNode((NodeType)nodeType)
            {
                IdLeft = request.idLeft,
                NameLeft = request.nameLeft,
                IdRight = request.idRight,
                NameRight = request.nameRight,
                IdRelationType = request.idRelationType,
                NameRelationType = request.nameRelationType

            };

            if (nodeItem == null) return Json(new List<object>());


            var controller = GetController(request.idSession);

            var resultTask = await controller.GetObjectRelationSubConscious(nodeItem, isOther);

            var resultItem = new KendoGridDataResult<ObjectObject_Subconscious>(resultTask.GridItems);
            var result = Json(resultItem);

            LogEndMethod(processId:processId);
            return result;
        }
    }
}