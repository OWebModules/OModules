﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyItemsModule.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    

    public class NavigationBarController : AppControllerBase
    {
        

        private NavigationBarViewModel GetNavigationBarViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<NavigationBarViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new NavigationBarViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedNavigationBar)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedNavigationBar)),
                    ActionSetViewReady = Url.Action(nameof(SetViewNavigationBarReady)),
                    ActionGetSessionData = Url.Action(nameof(SessionDataController.GetSessionDataUrl), nameof(SessionDataController).Replace("Controller", "")),
                    ActionInitializeNavigationItems = Url.Action(nameof(InitializeNavigationItems)),
                    ActionGetCurrentNavigationItem = Url.Action(nameof(GetCurrentNavigationItem)),
                    ActionNavFirst = Url.Action(nameof(NavFirst)),
                    ActionNavPrevious = Url.Action(nameof(NavPrevious)),
                    ActionNavNext = Url.Action(nameof(NavNext)),
                    ActionNavLast = Url.Action(nameof(NavLast)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeNavigationBarViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeNavigationBarViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetNavigationBarViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult NavFirst(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetNavigationBarViewModel(idInstance);

            var result = new ResultSimple
            {
                IsSuccessful = viewModel.NavigationItem.MoveFirst()
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult NavPrevious(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetNavigationBarViewModel(idInstance);

            var result = new ResultSimple
            {
                IsSuccessful = viewModel.NavigationItem.MovePrevious()
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult NavNext(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetNavigationBarViewModel(idInstance);

            var result = new ResultSimple
            {
                IsSuccessful = viewModel.NavigationItem.MoveNext()
            };


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult NavLast(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetNavigationBarViewModel(idInstance);

            var result = new ResultSimple
            {
                IsSuccessful = viewModel.NavigationItem.MoveNext()
            };


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }



        [Authorize]
        [HttpPost]
        public ActionResult InitializeNavigationItems(List<NavItem> navItems, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetNavigationBarViewModel(idInstance);

            viewModel.NavigationItem = new ItemsNavItem
            {
                ItemList = navItems
            };

            var result = new ResultViewItems
            {
                IsSuccessful = viewModel.NavigationItem != null,
                ViewItems = new List<ViewItem>()
            };

            if (!result.IsSuccessful)
            {
                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetCurrentNavigationItem(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetNavigationBarViewModel(idInstance);

            var result = new ResultSimpleView<NavItem>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.NavigationItem == null)
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var currentNavItem = viewModel.NavigationItem.CurrentNavItem;

            if (currentNavItem == null)
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = currentNavItem;

            viewModel.navFirst.ChangeViewItemValue(ViewItemType.Enable.ToString(), viewModel.NavigationItem.PreviousPossible);
            viewModel.navPrevious.ChangeViewItemValue(ViewItemType.Enable.ToString(), viewModel.NavigationItem.PreviousPossible);
            viewModel.navNext.ChangeViewItemValue(ViewItemType.Enable.ToString(), viewModel.NavigationItem.NextPossible);
            viewModel.navLast.ChangeViewItemValue(ViewItemType.Enable.ToString(), viewModel.NavigationItem.NextPossible);

            viewModel.navItemName.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.NavigationItem.CurrentItemName);
            viewModel.navPos.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.NavigationItem.NavString);

            result.ViewItems.Add(viewModel.navFirst);
            result.ViewItems.Add(viewModel.navPrevious);
            result.ViewItems.Add(viewModel.navNext);
            result.ViewItems.Add(viewModel.navLast);

            result.ViewItems.Add(viewModel.navItemName);
            result.ViewItems.Add(viewModel.navPos);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedNavigationBar()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetNavigationBarViewModel(idInstance);

            viewItemList.Add(viewModel.listenNavBar);

            viewModel.listenNavBar.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.listenNavBar.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedNavigationBar()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetNavigationBarViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewNavigationBarReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetNavigationBarViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        // GET: Log
        [Authorize]
        public ActionResult NavigationBar()
        {
            var processId = LogStartMethod();
            var viewModel = GetNavigationBarViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);            return View(viewModel);
        }

        // GET: NavigationBar
        public ActionResult Index()
        {
            return View();
        }
    }
}