﻿using System.Collections.Generic;
using TypedTaggingModule.Models;

namespace OModules.Controllers
{
    public class ResultGetTags
    {
        public bool IsSuccessful { get; set; }
        public List<Reference> Tags { get; set; }
    }
}