﻿using Microsoft.AspNet.Identity;
using OModules.Hubs;
using OModules.Models;
using OModules.Models.Requests;
using OModules.Notifications;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OModules.Controllers
{
    [Authorize]
    public class ModuleMgmtApiController : ApiControllerBase
    {
        

        private ModuleManagementModule.ModuleManagementController GetController(string sessionId)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(sessionId, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            
            var result = new ModuleManagementModule.ModuleManagementController(globals);
            LogEndMethod(processId:processId);
            return result;


        }

        private SecurityModule.SecurityController GetControllerSecurity(string idSession)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(idSession, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            LogEndMethod(processId:processId);
            return new SecurityModule.SecurityController(globals);


        }

        [Route("ModuleMgmtApi/SendInterServiceMessage")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> SendInterServiceMessage(ModuleMgmtApiSendRequest request)
        {
            var result = new ResultSimple
            {
                IsSuccessful = true
            };
            if (request == null || request.message == null)
            {
                result.IsSuccessful = false;
                return Json(result);
            }

            var securityConnector = GetControllerSecurity(request.idSession);


            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);

            request.message.UserId = userItem.UserItem.GUID;

            ModuleComHub.SendInterServiceMessageFromController(request.message);

            return Json(result);
        }
    }
}