﻿using CommandLineRunModule.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.MessageOutput;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class CommandLineRunController : AppControllerBase
    {
        

        private CommandLineRunModule.CommandLineRunController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new CommandLineRunModule.CommandLineRunController(globals);

            LogEndMethod(processId:processId);
            return new CommandLineRunModule.CommandLineRunController(globals);

        }

        private ScriptEditorViewModel GetScriptEdtiorViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ScriptEditorViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ScriptEditorViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedScriptEditor)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedScriptEditor)),
                    ActionSetViewReady = Url.Action(nameof(SetViewScriptEditorReady)),
                    ActionValidateReference = Url.Action(nameof(ValidateScriptEditorReference)),
                    ActionGetCodeSnipplet = Url.Action(nameof(GetCodeSnipplet)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeScriptEditorViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private CommandLineRunViewerViewModel GetViewModelCommandLineRunViewer(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<CommandLineRunViewerViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new CommandLineRunViewerViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedCommandLineRunViewer)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedCommandLineRunViewer)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyCommandLineRunViewer)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceCommandLineRunViewer)),
                    ActionObjectTreeInit = Url.Action(nameof(ObjectTreeController.ObjectTreeInit), nameof(ObjectTreeController).Replace("Controller","")),
                    ActionCheckReportDataFromSessionFile = Url.Action(nameof(CheckReportDataFromSessionFile)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueCommandLineRunViewer)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeScriptEditorViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetScriptEdtiorViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueCommandLineRunViewer(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelCommandLineRunViewer(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedScriptEditor()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetScriptEdtiorViewModel(idInstance);
            var securityController = GetController();

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedCommandLineRunViewer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelCommandLineRunViewer(idInstance);
            var securityController = GetController();

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedScriptEditor()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetScriptEdtiorViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedCommandLineRunViewer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelCommandLineRunViewer(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewScriptEditorReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetScriptEdtiorViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyCommandLineRunViewer(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelCommandLineRunViewer(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateScriptEditorReference(clsOntologyItem refItem, string idInstance)
        {

            var processId = LogStartMethod();
            var viewModel = GetScriptEdtiorViewModel(idInstance);

            var controller = GetController();

            var refItemTask = await controller.GetClassObject(refItem.GUID);

            viewModel.refItem = refItemTask.Result.ObjectItem;
            viewModel.refParentItem = refItemTask.Result.ClassItem;

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            result.ViewItems.Add(viewModel.windowTitle);

            viewModel.SetWindowTitle($"{ viewModel.refItem.Name } ({viewModel.refParentItem.Name})");

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceCommandLineRunViewer(clsOntologyItem refItem, string idInstance)
        {

            var processId = LogStartMethod();
            var viewModel = GetViewModelCommandLineRunViewer(idInstance);

            var controller = GetController();

            var refItemTask = await controller.GetClassObject(refItem.GUID);

            viewModel.refItem = refItemTask.Result.ObjectItem;
            viewModel.refParentItem = refItemTask.Result.ClassItem;

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (viewModel.refItem.GUID_Parent != controller.ClassComandLineRun.GUID)
            {
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }
            var resultController = await controller.GetCommandLineRun(viewModel.refItem, null, viewModel.reportDataItem);

            if (resultController.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (resultController.Result != null)
            {
                resultController.Result.Code = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(resultController.Result.Code));
                resultController.Result.CodeParsed = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(resultController.Result.CodeParsed));
                viewModel.scriptParsed.ChangeViewItemValue(ViewItemType.Other.ToString(), resultController.Result);

                result.ViewItems.Add(viewModel.scriptParsed);

                viewModel.inpEncoding.ChangeViewItemValue(ViewItemType.Content.ToString(), resultController.Result.Name_Encoding ?? "");
                viewModel.inpProgramingLanguage.ChangeViewItemValue(ViewItemType.Content.ToString(), resultController.Result.Name_ProgrammingLanguage ?? "");
                viewModel.inpName.ChangeViewItemValue(ViewItemType.Content.ToString(), resultController.Result.Name_CommandLineRun ?? "");

                result.ViewItems.Add(viewModel.inpEncoding);
                result.ViewItems.Add(viewModel.inpProgramingLanguage);
                result.ViewItems.Add(viewModel.inpName);
            }

            

            result.ViewItems.Add(viewModel.windowTitle);
            
            viewModel.SetWindowTitle($"{ viewModel.refItem.Name } ({viewModel.refParentItem.Name})");

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetCodeSnipplet(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetScriptEdtiorViewModel(idInstance);

            var controller = GetController();

            var result = new ResultSimpleView<CodeSnipplet>
            {
                IsSuccessful = true
            };


            if (viewModel.refItem == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var codeSnippletResult = await controller.GetCodeSnipplet(viewModel.refItem);

            if (codeSnippletResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = codeSnippletResult.Result;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> CheckReportDataFromSessionFile(string idInstance, string sessionFile)
        {
            var processId = LogStartMethod();            var viewModel = GetViewModelCommandLineRunViewer(idInstance);
            var controller = GetController();
            
            var uploadFilePath = Server.MapPath($"~/App_Data/Upload/{sessionFile}");
            var content = System.IO.File.ReadAllText(uploadFilePath);
            viewModel.reportDataItem = Newtonsoft.Json.JsonConvert.DeserializeObject<ReportDataItem>(content);

            var result = new ResultSimpleView<List<CommandLineRunToReport>>
            {
                IsSuccessful = true,
                Result = new List<CommandLineRunToReport>(),
                SelectorPath = viewModel.SelectorPath
            };

            if (viewModel.reportDataItem == null || viewModel.reportDataItem.reportItem == null)
            {
                LogEndMethod(processId:processId);                return Json(result);
            }

            var messageOutput = new OWMessageOutput(viewModel.IdInstance);
            var getCommandLineRunsOfReportsRequest = new GetCommandRunsToReportRequest(viewModel.reportDataItem.reportItem)
            {
                MessageOutput = messageOutput
            };

            var getCommandLineRunsOfReportsResult = await controller.GetCommandLineRunsOfReports(getCommandLineRunsOfReportsRequest, true);
            if (getCommandLineRunsOfReportsResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = getCommandLineRunsOfReportsResult.ResultState.Additional1;
                LogEndMethod(processId:processId);                return Json(result);
            }

            viewModel.reportDataItem.CommandLineRunsToReport = getCommandLineRunsOfReportsResult.Result;
            result.Result = viewModel.reportDataItem.CommandLineRunsToReport;

            LogEndMethod(processId:processId);            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: CommandLineRun
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult CommandLineRunViewer()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelCommandLineRunViewer(Guid.NewGuid().ToString());

            var controller = GetController();

            viewModel.idClassCommandLineRun = controller.ClassComandLineRun.GUID;

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ScriptEditor()
        {
            var processId = LogStartMethod();
            var viewModel = GetScriptEdtiorViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }
    }
}