﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.Requests;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OModules.Controllers
{
    public class ReportApiController : ApiControllerBase
    {
        

        private ReportModule.ReportController GetController(string idSession)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(idSession, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            

            var result = new ReportModule.ReportController(globals, true);

            LogEndMethod(processId:processId);
            return result;

        }

        [Route("ReportApi/GetData")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetData(GetDataRequest request)
        {
            var processId = LogStartMethod();
            var controller = GetController(request.idSession);

            var result = new ResultApi<List<Dictionary<string, object>>>
            {
                IsSuccessful = true
            };

            if (string.IsNullOrEmpty(request.idReport))
            {
                result.IsSuccessful = false;
                result.Message = "Report is not set!";

                return Json(result);
            }

            var refItemTask = await controller.GetOItem(request.idReport, controller.Globals.Type_Object);

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Report cannot be found!";
                return Json(result);
            }

            var resultGridConfig = await controller.GetReportConfig(refItemTask.Result, "");

            if (resultGridConfig.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Report cannot be found!";
                return Json(result);
            }

            if (resultGridConfig.Result.ReportItem == null)
            {
                if (resultGridConfig.ResultState.GUID == controller.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                    result.Message = "Report cannot be found!";
                    return Json(result);
                }
            }

            var resultLoadData = await controller.LoadData(resultGridConfig.Result.ReportItem, resultGridConfig.Result.ReportFields);


            if (resultLoadData.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Report cannot be loaded!";
                return Json(result);
            }

            result.Result = resultLoadData.Result;
            LogEndMethod(processId:processId);            return Json(result);
        }

        [Route("ReportApi/SyncReport")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> SyncReport(GetDataRequest request)
        {
            var processId = LogStartMethod();
            var result = new ResultApiSimple
            {
                IsSuccessful = true
            };

            var controller = GetController(request.idSession);

            var reportOItem = await controller.GetOItem(request.idReport, controller.Globals.Type_Object);

            if (reportOItem.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Report cannot be found!";
                return Json(result);
            }

            var reportItem = await controller.GetReport(reportOItem.Result);

            if (reportItem.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = "Report cannot be found!";
                return Json(result);
            }

            var resultSyncReport = await controller.SyncReport(new ReportModule.Models.SyncReportRequest { ReportItem = reportItem.Result.Report });

            if (resultSyncReport.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = resultSyncReport.ResultState.Additional1;
                return Json(result);
            }

            LogEndMethod(processId:processId);            return Json(result);
        }
    }
}