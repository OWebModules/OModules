﻿using Microsoft.AspNet.Identity;
using SignalR = Microsoft.AspNet.SignalR;
using ModuleManagementModule.Models;
using OModules.Hubs;
using OModules.Models;
using OModules.Notifications;
using OModules.Services;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OntologyClasses.BaseClasses;
using System.Threading.Tasks;

namespace OModules.Controllers
{
    public class ModuleMgmtController : AppControllerBase
    {

        

        private clsLogStates logStates = new clsLogStates();

        private LocalizationModule.LocalizationController GetLocalizationController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new LocalizationModule.LocalizationController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private ModuleManagementModule.ModuleManagementController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            
            var result = new ModuleManagementModule.ModuleManagementController(globals);
            LogEndMethod(processId:processId);
            return result;


        }

        private ModuleStarterViewModel GetViewModelModuleStarter(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ModuleStarterViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ModuleStarterViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigModuleStarter
                    {

                        ActionConnected = Url.Action(nameof(ConnectedModuleStarter)),
                        ActionDisconnected = Url.Action(nameof(DisconnectedModuleStarter)),
                        ActionSetViewReady = Url.Action(nameof(SetViewReady)),
                        ActionGetGridConfig = Url.Action(nameof(GetGridConfigModuleStarter)),
                        ActionSetViewToClass = Url.Action(nameof(SetViewToClass)),
                        ActionValidateReference = Url.Action(nameof(ValidateReferenceExternal)),
                        ActionRelationEditor = Url.Action(nameof(ObjectEditController.RelationEditor), nameof(ObjectEditController).Replace("Controller", "")),
                        ActionEmbeddedNameEditInit = Url.Action(nameof(ObjectEditController.EmbeddedNameEditInit), nameof(ObjectEditController).Replace("Controller", "")),
                    },

                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueModuleStarter)));
                
                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        public ActionResult SendInterServiceMessage(InterServiceMessage message)
        {
            var result = new ResultSimple
            {
                IsSuccessful = true
            };
            if (message == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
           

            ModuleComHub.SendInterServiceMessageFromController(message);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private SendInterserviceMessagesViewModel GetViewModelSendInterserviceMessage(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            return memoryCache.GetOrSet<SendInterserviceMessagesViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new SendInterserviceMessagesViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionSendInterserviceMessage = Url.Action(nameof(SendInterServiceMessage))
                };

                LogEndMethod(processId:processId);
                return viewModelNew;
            });


        }

        // GET: ModuleMgmt
        [Authorize]
        public ActionResult Index()
        {
            var processId = LogStartMethod();
            LogEndMethod(processId:processId);            return View();
        }

        [Authorize]
        public async Task<ActionResult> ModuleStarter()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelModuleStarter(Guid.NewGuid().ToString());
            viewModel.baseUrl = Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~");

            var controller = GetLocalizationController();
            var request = new LocalizationModule.Models.GetLocalizedGuiEntriesRequest(viewModel.View.IdView, System.Globalization.CultureInfo.CurrentUICulture.Name);
            var controllerResult = await controller.GetLicalizedGuiEntries(request);

            if (controllerResult.ResultState.GUID == controller.Globals.LState_Success.GUID)
            {
                var translateResult = await controller.TranslateViewItems(viewModel, controllerResult.Result.GuiEntries);
                if (translateResult.GUID == controller.Globals.LState_Success.GUID)
                {
                    viewModel.GuiEntries = controllerResult.Result.GuiEntries;
                }
            }

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public async Task<ActionResult> ModuleStarterInit()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelModuleStarter(Guid.NewGuid().ToString());
            viewModel.baseUrl = Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~");

            var controller = GetLocalizationController();
            var request = new LocalizationModule.Models.GetLocalizedGuiEntriesRequest(viewModel.View.IdView, System.Globalization.CultureInfo.CurrentUICulture.Name);
            var controllerResult = await controller.GetLicalizedGuiEntries(request);

            if (controllerResult.ResultState.GUID == controller.Globals.LState_Success.GUID)
            {
                var translateResult = await controller.TranslateViewItems(viewModel, controllerResult.Result.GuiEntries);
                if (translateResult.GUID == controller.Globals.LState_Success.GUID)
                {
                    viewModel.GuiEntries = controllerResult.Result.GuiEntries;
                }
            }

            var resultAction = new ResultSimpleView<ModuleStarterViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            LogEndMethod(processId:processId);            
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult SendInterserviceMessages()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelSendInterserviceMessage(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceExternal(string idObject)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<clsOntologyItem>
            {
                IsSuccessful = true,
                Result = new clsOntologyItem()
            };

            if (string.IsNullOrEmpty(idObject))
            {
                LogError("No objectId provided", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            var moduleMgmtController = GetController();

            var refItemTask = await moduleMgmtController.GetRefItem(idObject);

            if (refItemTask.Result.GUID == logStates.LogState_Error.GUID)
            {
                LogError("No objectId provided", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = refItemTask.Result;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueModuleStarter(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelModuleStarter(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var viewItemValue = result.ViewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedModuleStarter()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelModuleStarter(idInstance);

            viewModel.grid.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.grid);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedModuleStarter()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelModuleStarter(idInstance);

            viewModel.grid.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            
            viewItemList.Add(viewModel.grid);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelModuleStarter(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult GetGridConfigModuleStarter()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(ViewModuleItem), Url.Action(nameof(GetDataModuleStarter)), null, null, null, false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetDataModuleStarter(string idInstance, string idObject)
        {
            var processId = LogStartMethod();

            var viewModel = GetViewModelModuleStarter(idInstance);
            var moduleMgmtController = GetController();
            
            var resultTask = await moduleMgmtController.GetViewModuleItems(idObject, viewModel.Views.ViewList);

            LogEndMethod(processId:processId);
            return Json(new { data = resultTask.ViewModelItems.Where(viewModule => !string.IsNullOrEmpty(viewModule.IdView)).OrderByDescending(viewModule => viewModule.Count) }, JsonRequestBehavior.AllowGet);


        }

        [Authorize, HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SetViewToClass(string idObject, string idView)
        {
            var processId = LogStartMethod();
            var moduleMgmtController = GetController();

            var resultTask = await moduleMgmtController.SetViewToClass(idObject, idView);

            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


    }

    public class ResultSimple
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }

        public List<string> SelectorPath { get; set; } = new List<string>();

        public List<ViewItem> ViewItems { get; set; } = new List<ViewItem>();
    }

    public enum ResultViewItemsType
    {
        Normal = 0,
        NoReference = 1
    }
    public class ResultViewItems
    {
        public bool IsSuccessful { get; set; }
        public List<ViewItem> ViewItems { get; set; }

        public ResultViewItemsType ResultType { get; set; }
        public string ResultMessage { get; set; }

        public List<string> SelectorPath { get; set; } = new List<string>();
    }

    
}