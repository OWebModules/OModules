﻿using LearningModule.Factories;
using LearningModule.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.MessageOutput;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{

    public class LearningController : AppControllerBase
    {
        

        private LearningModule.LearningController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new LearningModule.LearningController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private QuestionAnswerViewModel GetViewModelQuestionAnswer(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<QuestionAnswerViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new QuestionAnswerViewModel(Session, key, User.Identity.GetUserId())
                {

                    ActionConnected = Url.Action(nameof(ConnectedQuestionAnswer)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedQuestionAnswer)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyQuestionAnswer)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceQuestionAnswer)),
                    ActionGetQuestions = Url.Action(nameof(GetQuestions)),
                    ActionGetPossibleAnswers = Url.Action(nameof(GetPossibleAnswers)),
                    ActionGetCorrectAnswers = Url.Action(nameof(GetCorrectAnswers)),
                    ActionHtmlViewer = Url.Action(nameof(HtmlViewerController.DocViewer), nameof(HtmlViewerController).Replace("Controller", "")),
                    ActionMediaBookmarkManager = Url.Action(nameof(MediaViewerController.MediaBookmarkManagerVideo), nameof(MediaViewerController).Replace("Controller", "")),
                    ActionSaveSelfAnswer = Url.Action(nameof(SaveSelfAnswer)),
                    ActionSaveAnswerWithOfficialAnswer = Url.Action(nameof(SaveAnswerWithOfficialAnswer)),
                    ActionEmbeddedNameEditInit = Url.Action(nameof(ObjectEditController.EmbeddedNameEditInit), nameof(ObjectEditController).Replace("Controller", "")),
                    ActionItemComBarInit = Url.Action(nameof(ObjectEditController.ItemComBarInit), nameof(ObjectEditController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };


                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueQuestionAnswer)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueQuestionAnswer(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelQuestionAnswer(idInstance);


            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedQuestionAnswer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelQuestionAnswer(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewItemList.Add(viewModel.isListen);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedQuestionAnswer()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModelQuestionAnswer(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyQuestionAnswer(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelQuestionAnswer(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetPossibleAnswers(string idQuestionItem, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<AnswerViewItemResult>
            {
                IsSuccessful = true,
                Result = new AnswerViewItemResult()
            };

            var viewModel = GetViewModelQuestionAnswer(idInstance);
            var question = viewModel.QuestionViewItems.FirstOrDefault(qest => qest.IdQuestion == idQuestionItem);
            if (question == null)
            {
                result.IsSuccessful = false;
                result.Message = "No question-item provided!";
                LogEndMethod(processId: processId); return Json(result, JsonRequestBehavior.AllowGet);
            }

            var controller = GetController();

            var answerResult = await LearningModule.Factories.QuestionAnswerViewItemsFactory.CreatePossibleAnswerViewItems(question, viewModel.Questions, controller.Globals);

            result.IsSuccessful = answerResult.ResultState.GUID != controller.Globals.LState_Success.GUID;
            result.Message = answerResult.ResultState.Additional1;
            result.Result = answerResult.Result;

            if (!result.IsSuccessful)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                result.ViewItems.Add(viewModel.isListen);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetCorrectAnswers(string idQuestion, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<AnswerViewItemResult>
            {
                IsSuccessful = true,
                Result = new AnswerViewItemResult()
            };

            
            var viewModel = GetViewModelQuestionAnswer(idInstance);
            var controller = GetController();

            var question = viewModel.QuestionViewItems.FirstOrDefault(quest => quest.IdQuestion == idQuestion);
           
            if (question == null)
            {
                result.IsSuccessful = false;
                result.Message = "No question-item provided!";
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }
            
            var answerResult = await LearningModule.Factories.QuestionAnswerViewItemsFactory.CreateCorrectAnswerViewItems(question, viewModel.Questions, controller.Globals);

            result.IsSuccessful = answerResult.ResultState.GUID != controller.Globals.LState_Success.GUID;
            result.Message = answerResult.ResultState.Additional1;
            result.Result = answerResult.Result;

            if (!result.IsSuccessful)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                result.ViewItems.Add(viewModel.isListen);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveSelfAnswer(string idQuestion, string content64, int answerType, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<QuestionViewItem>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (idQuestion == null)
            {
                result.IsSuccessful = false;
                result.Message = "No question provided!";
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var viewModel = GetViewModelQuestionAnswer(idInstance);
            var messageOutput = new OWMessageOutput(viewModel.IdInstance);
            var controller = GetController();

            var question = viewModel.Questions.FirstOrDefault(quest => quest.QuestionItem.GUID == idQuestion);
            if (question == null)
            {
                result.IsSuccessful = false;
                result.Message = "Question cannot be found!";
                messageOutput.OutputError(result.Message);
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            

            var categoryItem = LearningModule.Converters.IntToCategoryConverter.Convert(answerType);
            if (categoryItem == null)
            {
                result.IsSuccessful = false;
                result.Message = "Category cannot be found!";
                messageOutput.OutputError(result.Message);
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var request = new SaveSelfAnswerRequest
            {
                QuestionItem = question.QuestionItem,
                AnswerCategory = categoryItem,
                answerHtml = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(content64 ?? "")),
                MessageOutput = messageOutput
            };

            var saveResult = await controller.SaveSelfAnswer(request);

            if (saveResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = saveResult.ResultState.Additional1;
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (!result.IsSuccessful)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                result.ViewItems.Add(viewModel.isListen);
            }
            var answer = new Answer
            {
                AnswerItem = saveResult.Result.AnswerItem,
                AnswerToCategory = saveResult.Result.AnswerToCategory
            };

            var questionViewItem = await controller.CreateQuestionViewItem(question, answer, viewModel.Categories, viewModel.IncludeLowerCategories);
            if (saveResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = saveResult.ResultState.Additional1;
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = questionViewItem.Result;

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveAnswerWithOfficialAnswer(string idQuestion, List<string> idAnswers, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultSimpleView<SaveSelfAnswerResult>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (idQuestion == null)
            {
                result.IsSuccessful = false;
                result.Message = "No question provided!";
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var viewModel = GetViewModelQuestionAnswer(idInstance);
            var messageOutput = new OWMessageOutput(viewModel.IdInstance);
            var controller = GetController();

            var question = viewModel.Questions.FirstOrDefault(quest => quest.QuestionItem.GUID == idQuestion);
            if (question == null)
            {
                result.IsSuccessful = false;
                result.Message = "Question cannot be found!";
                messageOutput.OutputError(result.Message);
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var request = new SaveAnswerWithOfficialAnswerRequest(question, idAnswers)
            {
                MessageOutput = messageOutput
            };

            var saveResult = await controller.SaveAnswerWithOfficialAnswer(request);

            if (saveResult.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                result.Message = saveResult.Additional1;
                LogEndMethod(processId:processId);                return Json(result, JsonRequestBehavior.AllowGet);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetQuestions(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelQuestionAnswer(idInstance);
            var controller = GetController();

            var questionsResult = await LearningModule.Factories.QuestionAnswerViewItemsFactory.CreateQuestionViewItems(viewModel.Questions, viewModel.Categories, viewModel.IncludeLowerCategories, controller.Globals);

            var result = new ResultSimpleView<List<QuestionViewItem>>
            {
                IsSuccessful = questionsResult.ResultState.GUID == controller.Globals.LState_Success.GUID,
                Message = questionsResult.ResultState.Additional1,
                Result = questionsResult.Result
            };

            viewModel.QuestionViewItems = questionsResult.Result;

            if (!result.IsSuccessful)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
                result.ViewItems.Add(viewModel.isListen);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceQuestionAnswer(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelQuestionAnswer(idInstance);

            var controller = GetController();

            var refItemTask = await controller.GetClassObject(refItem.GUID);


            var result = new ResultSimpleView<QuestionReferenceSource>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while getting the RefItem", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.ClassObject = refItemTask.Result;

            var request = new LearningModule.Models.GetRelatedQuestionsRequest(viewModel.ClassObject.ObjectItem)
            {
                MessageOutput = new OWMessageOutput(viewModel.IdInstance)
            };

            var controllerResult = await controller.GetRelatedQuestions(request);
            if (controllerResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogError("Error while getting the RefItem", processId: processId);
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.Questions = controllerResult.Result.Questions;
            viewModel.Categories = controllerResult.Result.Categories;
            viewModel.IncludeLowerCategories = controllerResult.Result.IncludeLowerCategories;
            viewModel.QuestionReferenceSource = controllerResult.Result.ReferenceSource;

            viewModel.SetWindowTitle(viewModel.ClassObject.ToString());
            result.Result = viewModel.QuestionReferenceSource;
            result.ViewItems.Add(viewModel.windowTitle);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: Learning
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult QuestionAnswerInit()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelQuestionAnswer(Guid.NewGuid().ToString());
            var resultAction = new ResultSimpleView<QuestionAnswerViewModel>
            {
                IsSuccessful = true,
                Result = viewModel
            };

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult QuestionAnswer()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModelQuestionAnswer(Guid.NewGuid().ToString());

            var controller = GetController();

            LogEndMethod(processId:processId);
            return View(viewModel);
        }
    }
}