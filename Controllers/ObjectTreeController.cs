﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class ObjectTreeController : AppControllerBase
    {
        

        private ObjectTreeViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ObjectTreeViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ObjectTreeViewModel(Session, key, User.Identity.GetUserId())
                {
                    IdSession = HttpContext.Session.SessionID,
                    ActionGetRelationTypeDropDownConfig = Url.Action(nameof(DropDownController.GetDropDownConfigForRelationTypes), nameof(DropDownController).Replace("Controller", "")),
                    ActionConnected = Url.Action(nameof(Connected)),
                    ActionDisconnected = Url.Action(nameof(Disconnected)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReady)),
                    ActionGetTreeConfig = Url.Action(nameof(GetTreeConfig)),
                    ActionValidateReference = Url.Action(nameof(ValidateReference)),
                    ActionEditOItem = Url.Action(nameof(EditOItemController.EditOItem), nameof(EditOItemController).Replace("Controller", "")),
                    ActionSaveRelations = Url.Action(nameof(SaveObjectRelation)),
                    ActionSearchTree = Url.Action(nameof(SearchTree)),
                    ActionObjectTreeInit = Url.Action(nameof(ObjectTreeInit)),
                    ActionObjectListInit = Url.Action(nameof(OItemListController.ObjectListInit), nameof(OItemListController).Replace("Controller", ""))
                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private OntologyItemsModule.ObjectTreeController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

          
            var result = new OntologyItemsModule.ObjectTreeController(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };
            var viewItemValue = viewItem.LastValue;
            if (viewItemValue == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetTreeConfig(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var actionResult = new ResultSimpleView<KendoTreeViewConfig>
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath,
                Result = new KendoTreeViewConfig
                {
                    loadOnDemand = true,
                    dataSource = new KendoHierarchicalDataSource
                    {
                        schema = new KendoHierarchicalSchema
                        {
                            model = new KendoHierarchicalModel
                            {
                                id = nameof(KendoTreeNode.NodeId),
                                children = nameof(KendoTreeNode.SubNodes),
                                hasChildren = nameof(KendoTreeNode.HasChildren)
                            }
                        },
                        transport = new KendoTransport
                        {
                            read = new KendoTransportCRUD
                            {
                                dataType = "json",
                                url = Url.Action(nameof(GetTreeItems))
                            }
                        }
                    },
                    dataTextField = nameof(KendoTreeNode.NodeName)
                }
            };

            LogEndMethod(processId:processId);
            return Json(actionResult, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetTreeItems(string idInstance, string idRelationType)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idInstance))
            {
                return Json(new List<KendoTreeNode>(), JsonRequestBehavior.AllowGet);
            }
            var viewModel = GetViewModel(idInstance);

            var controller = GetController();

            string idClass = null;
            string idObject = null;
            if (viewModel.OItemWithParent != null)
            {
                if (viewModel.OItemWithParent.OItemParent != null)
                {
                    idObject = viewModel.OItemWithParent.OItem.GUID;
                    idClass = viewModel.OItemWithParent.OItemParent.GUID;
                }
                else
                {

                    idClass = viewModel.OItemWithParent.OItem.Type == controller.Globals.Type_Class ? viewModel.OItemWithParent.OItem.GUID : null;
                    idObject = viewModel.OItemWithParent.OItem.Type == controller.Globals.Type_Object ? viewModel.OItemWithParent.OItem.GUID : null;
                }
                
            }
            if (string.IsNullOrEmpty(idRelationType))
            {
                idRelationType = viewModel.RelationType != null ? viewModel.RelationType.GUID : null;
            }
            

            var resultTree = await controller.GetObjectTree(idObject, idClass, idRelationType, controller.Globals.Direction_LeftRight.GUID, -1);

            if (resultTree.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Response.StatusCode = 500;
                Response.Status = resultTree.ResultState.Additional1;
                return null;
            }
            viewModel.TreeNodes = resultTree.Result;

            var result = Json(resultTree.Result, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> SearchTree(string searchText, string idInstance)
        {
            var processId = LogStartMethod();
            if (string.IsNullOrEmpty(idInstance))
            {
                return Json(new List<KendoTreeNodeSearchItem>(), JsonRequestBehavior.AllowGet);
            }
            var viewModel = GetViewModel(idInstance);

            var controller = GetController();

            var result = new ResultSimpleView<List<KendoTreeNodeSearchItem>>
            {
                IsSuccessful = true,
                Result = new List<KendoTreeNodeSearchItem>(),
                SelectorPath = viewModel.SelectorPath
            };

            foreach (var treeNode in viewModel.TreeNodes)
            {
                result.Result.AddRange(await treeNode.SearchNodesByName(searchText));
            }

            var resultAction = Json(result, JsonRequestBehavior.AllowGet);
            resultAction.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return resultAction;
        }
        [Authorize]
        public ActionResult ObjectTreeInit(string idInstance, List<string> selectorPath)
        {
            var processId = LogStartMethod();
            var resultAction = new ResultSimpleView<ObjectTreeViewModel>
            {
                IsSuccessful = true,
                SelectorPath = selectorPath
            };

            if (string.IsNullOrEmpty(idInstance))
            {
                idInstance = Guid.NewGuid().ToString();
            }
            var viewModel = GetViewModel(idInstance);

            viewModel.SelectorPath = selectorPath;
            resultAction.Result = viewModel;

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ObjectTree()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        // GET: ObjectTree
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.isToggledNextId);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewModel.isToggledNextId.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewItemList.Add(viewModel.dropdRelationType);
            viewItemList.Add(viewModel.isListen);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.dropdRelationType.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReference(clsOntologyItem refItem, bool? refIsClass, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var controller = GetController();

            var result = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                SelectorPath = viewModel.SelectorPath
            };

            viewModel.IsClass = refIsClass ?? false;
            var refItemTask = await controller.GetOItem(refItem.GUID, viewModel.IsClass ? controller.Globals.Type_Class : controller.Globals.Type_Object);

            if (refItemTask.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.OItemWithParent = refItemTask.Result;
            result.ViewItems.Add(viewModel.classItem);

            viewModel.classItem.ChangeViewItemValue(ViewItemType.Other.ToString(), viewModel.IsClass ? viewModel.OItemWithParent.OItem.GUID : viewModel.OItemWithParent.OItemParent.GUID);

            var relItem = await controller.GetMostImportantRelationType(viewModel.IsClass ? viewModel.OItemWithParent.OItem.GUID : viewModel.OItemWithParent.OItemParent.GUID);

            if (relItem.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.RelationType = null;
            if (relItem.Result != null)
            {
                viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                result.ViewItems.Add(viewModel.isListen);
                viewModel.RelationType = relItem.Result;
                viewModel.dropdRelationType.ChangeViewItemValue(ViewItemType.Other.ToString(), relItem.Result);
                result.ViewItems.Add(viewModel.dropdRelationType);
            }

            

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveObjectRelation(KendoTreeNode parentNode, List<clsOntologyItem> childItems, bool nextOrderId, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var controller = GetController();

            var idClass = viewModel.IsClass ? viewModel.OItemWithParent.OItem.GUID : viewModel.OItemWithParent.OItemParent.GUID;
            var resultSave = await controller.SaveObjectRelations(idClass, parentNode, childItems, viewModel.RelationType, nextOrderId, 2);

            var result = new ResultSimpleView<List<KendoTreeNode>>
            {
                IsSuccessful = resultSave.ResultState.GUID == controller.Globals.LState_Success.GUID,
                Result = resultSave.Result,
                SelectorPath = viewModel.SelectorPath
            };

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}