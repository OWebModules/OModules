﻿using LogModule.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.MessageOutput;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Models;
using ProcessModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class ProcessController : AppControllerBase
    {
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        private SecurityModule.SecurityController GetControllerSecurity()
        {

            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new SecurityModule.SecurityController(globals);

            LogEndMethod(processId:processId);
            return result;
        }

        private ProcessModule.ChecklistController GetChecklistController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new ProcessModule.ChecklistController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private ProcessTreeViewModel GetProcessTreeViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<ProcessTreeViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ProcessTreeViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConfig = new ActionConfigProcessTree
                    {
                        ActionConnected = Url.Action(nameof(ConnectedProcessTree)),
                        ActionDisconnected = Url.Action(nameof(DisconnectedProcessTree)),
                        ActionSetViewReady = Url.Action(nameof(SetViewReadyProcessTree)),
                        ActionValidateReference = Url.Action(nameof(ValidateReferenceProcessTree)),

                    },
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueProcessTree)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private CheckListViewModel GetCheckListViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<CheckListViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new CheckListViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedChecklist)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedChecklist)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyChecklist)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceChecklist)),
                    ActionItemComBarInit = Url.Action(nameof(ObjectEditController.ItemComBarInit), nameof(ObjectEditController).Replace("Controller", "")),
                    ActionChecklistInit = Url.Action(nameof(ProcessController.ChecklistInit), nameof(ProcessController).Replace("Controller", "")),
                    ActionReportInit = Url.Action(nameof(ReportController.ReportInit), nameof(ReportController).Replace("Controller", "")),
                    ActionGetChecklistData = Url.Action(nameof(GetDataChecklist)),
                    ActionUpdateChecklistEntryState = Url.Action(nameof(UpdateChecklistEntryState)),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueChecklist)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        public ReportModule.ReportController GetReportController(bool isAspNetProject)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new ReportModule.ReportController(globals, isAspNetProject);

            LogEndMethod(processId:processId);
            return result;

        }

        public ReportViewModel GetReportViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            return memoryCache.GetOrSet<ReportViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new ReportViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ReportController.ConnectedReport), nameof(ReportController).Replace("Controller", "")),
                    ActionDisconnected = Url.Action(nameof(ReportController.DisconnectedReport), nameof(ReportController).Replace("Controller", "")),
                    ActionSetViewReady = Url.Action(nameof(ReportController.SetViewReportReady), nameof(ReportController).Replace("Controller", "")),
                    ActionValidateReference = Url.Action(nameof(ReportController.ValidateReportReference), nameof(ReportController).Replace("Controller", "")),
                    ActionGetGridConfig = Url.Action(nameof(ReportController.GetGridConfigReport), nameof(ReportController).Replace("Controller", "")),
                    ActionGetObjectForSend = Url.Action(nameof(ReportController.GetObjectForSend), nameof(ReportController).Replace("Controller", "")),
                    ActionGetSyncState = Url.Action(nameof(ReportController.GetSyncState), nameof(ReportController).Replace("Controller", "")),
                    ActionSyncReport = Url.Action(nameof(ReportController.SyncReport), nameof(ReportController).Replace("Controller", "")),
                    ActionInitializeViewItemsReport = Url.Action(nameof(ReportController.InitializeViewItemsReport), nameof(ReportController).Replace("Controller", "")),
                    ActionModuleStarter = Url.Action(nameof(ModuleMgmtController.ModuleStarter), nameof(ModuleMgmtController).Replace("Controller", "")),
                    ActionObjectEditor = Url.Action(nameof(ObjectEditController.RelationEditor), nameof(ObjectEditController).Replace("Controller", "")),
                    ActionHtmlDiff = Url.Action(nameof(HtmlEditController.HtmlDiff), nameof(HtmlEditController).Replace("Controller", "")),
                    ActionGetAutoCompletesFilter = Url.Action(nameof(ReportController.GetAutoCompletesFilter), nameof(ReportController).Replace("Controller", "")),
                    ActionSavePattern = Url.Action(nameof(ReportController.SavePattern), nameof(ReportController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ReportController.ChangeReportViewItemValue), nameof(ReportController).Replace("Controller", "")));

                LogEndMethod(processId:processId);
                return viewModelNew;
            });
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceProcessTree(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetProcessTreeViewModel(idInstance);
            
            var resultViewItems = new List<ViewItem>();

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceChecklist(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetCheckListViewModel(idInstance);
            var controller = GetChecklistController();

            var actionResult = new ResultSimpleView<Checklist>()
            {
                IsSuccessful = true
            };
            var oItemResult = await controller.GetClassObject(refItem.GUID);
            if (oItemResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                actionResult.IsSuccessful = false;
                actionResult.Message = oItemResult.ResultState.Additional1;

                LogEndMethod(processId:processId);                return Json(actionResult, JsonRequestBehavior.AllowGet);
            }

            if (oItemResult.Result.ObjectItem.GUID_Parent != controller.IdClassWorkingList)
            {
                actionResult.Message = "Provided Object is no Working-List!";
                actionResult.Result = null;
                LogEndMethod(processId:processId);                return Json(actionResult, JsonRequestBehavior.AllowGet);
            }

            actionResult.SelectorPath = viewModel.SelectorPath;

            var messageOutput = new OWMessageOutput(viewModel.IdInstance);

            var checklistRequest = new GetChecklistRequest(oItemResult.Result.ObjectItem.GUID)
            {
                MessageOutput = messageOutput
            };
            var checklistResult = await controller.GetChecklist(checklistRequest);

            if (checklistResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                actionResult.IsSuccessful = false;
                actionResult.Message = checklistResult.ResultState.Additional1;
                LogEndMethod(processId:processId);                return Json(actionResult, JsonRequestBehavior.AllowGet);
            }

            actionResult.Result = checklistResult.Result;
            viewModel.Checklist = actionResult.Result;

            viewModel.SetWindowTitle($"{oItemResult.Result.ObjectItem.Name} ({oItemResult.Result.ClassItem.Name}");
            actionResult.ViewItems.Add(viewModel.windowTitle);


            LogEndMethod(processId:processId);
            return Json(actionResult, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedProcessTree()
        {

            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetProcessTreeViewModel(idInstance);

            viewModel.toolbar.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.grid.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            
            viewItemList.Add(viewModel.toolbar);
            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.grid);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedProcessTree()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetProcessTreeViewModel(idInstance);

            viewModel.toolbar.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);
            viewModel.grid.ChangeViewItemValue(ViewItemType.Enable.ToString(), false);


            viewItemList.Add(viewModel.toolbar);
            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.grid);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedChecklist()
        {

            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetCheckListViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedChecklist()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetCheckListViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyProcessTree(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetProcessTreeViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyChecklist(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetCheckListViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetAdditionalReportFields()
        {
            var processId = LogStartMethod();
            var checklistController = GetChecklistController();

            var resultAction = new ResultSimpleView<List<AdditionalReportField>>
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                Result = new List<AdditionalReportField>()
            };

            resultAction.Result = checklistController.GetAdditionalReportFields();

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueProcessTree(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetProcessTreeViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetDataChecklist(string idInstance, string query)
        {
            var processId = LogStartMethod();
            if (!string.IsNullOrEmpty(query))
            {
                query = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(query));
            }
            else
            {
                query = null;
            }

            var viewModel = GetReportViewModel(idInstance);

            var viewModelChecklist = GetCheckListViewModel(viewModel.IdAdditionalViewModel);
            var filterOpen = (bool)viewModelChecklist.openEntries.GetViewItemValue(ViewItemType.Checked.ToString());
            var filterDone = (bool)viewModelChecklist.doneEntries.GetViewItemValue(ViewItemType.Checked.ToString());
            var filterEdit = (bool)viewModelChecklist.editEntries.GetViewItemValue(ViewItemType.Checked.ToString());
            var filterCancelled = (bool)viewModelChecklist.cancelledEntries.GetViewItemValue(ViewItemType.Checked.ToString());

            var controller = GetReportController(true);
            var checklistController = GetChecklistController();
            var messageOutput = new OWMessageOutput(viewModel.IdInstance);
            var checklistRequest = new GetChecklistRequest(viewModelChecklist.Checklist.IdChecklist)
            {
                MessageOutput = messageOutput
            };
            var checklistResult = await checklistController.GetChecklist(checklistRequest);

            if (checklistResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                return Json(new List<Dictionary<string, object>>(), JsonRequestBehavior.AllowGet);
            }

            viewModelChecklist.Checklist = checklistResult.Result;

            if (!string.IsNullOrEmpty(query))
            {
                query = $"({query}) AND ";
            }
            else
            {
                query = "";
            }

            query += $"{viewModelChecklist.Checklist.NameIdChecklistColumn} = '{viewModelChecklist.Checklist.IdChecklist}'";

            var result = await controller.LoadData(viewModel.ReportItem, viewModel.ReportFields, additionalFields: viewModel.AdditionalFields, whereClaus: query);
                                        
            foreach (var row in result.Result)
            {
                if (row.ContainsKey(viewModelChecklist.Checklist.NameReportField))
                {
                    var idReference = row[viewModelChecklist.Checklist.NameReportField].ToString();
                    var logItem = viewModelChecklist.Checklist.LogItems.Where(logItm => logItm.RelatedItems.Any(rel => rel.ID_Other == idReference)).OrderByDescending(logItm => logItm.DateTimeStamp).FirstOrDefault();
                    if (logItem != null)
                    {
                        row.Add(nameof(logItem.IdLogEntry), logItem.IdLogEntry);
                        row.Add(nameof(logItem.NameLogEntry), logItem.NameLogEntry);
                        row.Add(nameof(logItem.IdLogState), logItem.IdLogState);
                        row.Add(nameof(logItem.NameLogState), logItem.NameLogState);
                        row.Add(nameof(logItem.DateTimeStamp), logItem.DateTimeStamp);
                    }   
                }
                
            }

            if (filterOpen)
            {
                result.Result = result.Result.Where(res => !res.ContainsKey(nameof(LogItem.IdLogState))).ToList();
            } 
            else if (filterEdit)
            {
                result.Result = result.Result.Where(res => res.ContainsKey(nameof(LogItem.IdLogState)) && controller.Globals.is_GUID( res[nameof(LogItem.IdLogState)].ToString()) &&
                    res[nameof(LogItem.IdLogState)].ToString() != checklistController.LogStateError.GUID &&
                    res[nameof(LogItem.IdLogState)].ToString() != checklistController.LogStateSuccess.GUID).ToList();
            }
            else if (filterCancelled)
            {
                result.Result = result.Result.Where(res => res.ContainsKey(nameof(LogItem.IdLogState)) && res[nameof(LogItem.IdLogState)].ToString() == checklistController.LogStateError.GUID).ToList();
            }
            else if (filterDone)
            {
                result.Result = result.Result.Where(res => res.ContainsKey(nameof(LogItem.IdLogState)) && res[nameof(LogItem.IdLogState)].ToString() == checklistController.LogStateSuccess.GUID).ToList();
            }
            if (result.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                LogEndMethod(processId:processId);
                return Json(new List<object>(), JsonRequestBehavior.AllowGet);
            }

            LogEndMethod(processId:processId);
            return Json(result.Result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> UpdateChecklistEntryState(string idInstance, string idReference, string viewItemIdButton, string message)
        {
            var processId = LogStartMethod();
            var viewModel = GetCheckListViewModel(idInstance);
            if (!string.IsNullOrEmpty(message))
            {
                message = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(message));
            }

            var actionResult = new ResultSimple
            {
                IsSuccessful = true,
                SelectorPath = viewModel.SelectorPath
            };

            if (string.IsNullOrEmpty(idInstance))
            {
                actionResult.IsSuccessful = false;
                actionResult.Message = "The Instance is not valid!";
                LogEndMethod(processId:processId);                return Json(actionResult, JsonRequestBehavior.AllowGet);
            }

            if (viewModel.Checklist == null)
            {
                actionResult.IsSuccessful = false;
                actionResult.Message = "No Checklist present!";
                LogEndMethod(processId:processId);                return Json(actionResult, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(viewItemIdButton) ||
                string.IsNullOrEmpty(idReference))
            {
                actionResult.IsSuccessful = false;
                actionResult.Message = "It's not clear, which change should be done!";
                LogEndMethod(processId:processId);                return Json(actionResult, JsonRequestBehavior.AllowGet);
            }

            var controller = GetChecklistController();
            var logState = controller.LogStateSuccess;
            if (viewItemIdButton == viewModel.editEntries.ViewItemId)
            {
                logState = controller.LogStatePause;
            }
            else if (viewItemIdButton == viewModel.cancelledEntries.ViewItemId)
            {
                logState = controller.LogStateError;
            }

            var securityController = GetControllerSecurity();

            var userId = User.Identity.GetUserId();
            var userItemResult = await securityController.GetUser(userId);

            var request = new ChangeChecklistEntryStateRequest
            {
                ChecklistEntryStateItems = new List<ChecklistEntryStateItem>
                {
                    new ChecklistEntryStateItem(viewModel.Checklist.IdChecklist, idReference, logState.GUID, userItemResult.Result.GUID, message)
                },
                MessageOutput = new OWMessageOutput(idInstance)
            };

            var changeResult = await controller.ChangeChecklistEntryState(request);

            actionResult.IsSuccessful = changeResult.ResultState.GUID == controller.Globals.LState_Success.GUID;
            actionResult.Message = changeResult.ResultState.Additional1;

            LogEndMethod(processId:processId);
            return Json(actionResult, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueChecklist(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetCheckListViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>(),
                SelectorPath = viewModel.SelectorPath
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(viewModel.openEntries);
            resultAction.ViewItems.Add(viewModel.editEntries);
            resultAction.ViewItems.Add(viewModel.doneEntries);
            resultAction.ViewItems.Add(viewModel.cancelledEntries);

            if (result.ViewItem.ViewItemId == viewModel.openEntries.ViewItemId)
            {
                var stateChecked = (bool)result.ViewItem.GetViewItemValue(ViewItemType.Checked.ToString());
                if (stateChecked)
                {
                    viewModel.editEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                    viewModel.doneEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                    viewModel.cancelledEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                }
            }
            else if (result.ViewItem.ViewItemId == viewModel.editEntries.ViewItemId)
            {
                var stateChecked = (bool)result.ViewItem.GetViewItemValue(ViewItemType.Checked.ToString());
                if (stateChecked)
                {
                    viewModel.openEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                    viewModel.doneEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                    viewModel.cancelledEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                }
            }
            else if (result.ViewItem.ViewItemId == viewModel.doneEntries.ViewItemId)
            {
                var stateChecked = (bool)result.ViewItem.GetViewItemValue(ViewItemType.Checked.ToString());
                if (stateChecked)
                {
                    viewModel.openEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                    viewModel.editEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                    viewModel.cancelledEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                }
            }
            else if (result.ViewItem.ViewItemId == viewModel.cancelledEntries.ViewItemId)
            {
                var stateChecked = (bool)result.ViewItem.GetViewItemValue(ViewItemType.Checked.ToString());
                if (stateChecked)
                {
                    viewModel.openEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                    viewModel.editEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                    viewModel.doneEntries.ChangeViewItemValue(ViewItemType.Checked.ToString(), false);
                }
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        // GET: HtmlEdit
        [Authorize]
        public ActionResult ProcessTree()
        {
            var processId = LogStartMethod();
            var viewModel = GetProcessTreeViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult Checklist()
        {
            var processId = LogStartMethod();
            var viewModel = GetCheckListViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId:processId);
            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> ChecklistInit(string idInstance, List<string> selectorPath)
        {
            var processId = LogStartMethod();
            var resultAction = new ResultSimpleView<CheckListViewModel>
            {
                IsSuccessful = true,
                SelectorPath = selectorPath
            };

            if (string.IsNullOrEmpty(idInstance))
            {
                idInstance = Guid.NewGuid().ToString();
            }
            var securityConnector = GetControllerSecurity();
            var userId = User.Identity.GetUserId();
            var userItem = await securityConnector.GetUser(userId);
            var groupItem = await securityConnector.GetGroup(userId);

            var viewModel = GetCheckListViewModel(idInstance);
            viewModel.UserItem = userItem.UserItem;
            viewModel.GroupItem = groupItem.GroupItem;
            var controller = GetChecklistController();

            viewModel.SelectorPath = selectorPath;
            resultAction.Result = viewModel;

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        // GET: Process
        public ActionResult Index()
        {
            return View();
        }
    }
}