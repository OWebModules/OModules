﻿using OModules.Services;
using OntologyItemsModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OModules.Controllers
{
    [Authorize]
    public class ClassTreeApiController : ApiControllerBase
    {
        

        private OntologyItemsModule.ClassTreeController GetController(string sessionId)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(sessionId, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new OntologyItemsModule.ClassTreeController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        public IHttpActionResult Get(string id)
        {
            return Json("test");
        }

        [Route("ClassTreeApi/GetClassPath")]
        [Authorize]
        [HttpPost, HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetClassPath(string idSession, string idClass, string searchTerm)
        {
            var processId = LogStartMethod();
            
            var classSelectionItem = new ClassSelectItem
            {
                IdClass = idClass,
                SearchTerm = searchTerm,
                OnlyPathItems = false
            };


            var classTreeController = GetController(idSession);

            var resultTask = await classTreeController.GetClassList(classSelectionItem);


            var result = Json(new { searchNodes = resultTask.ClassPathNodes, treeNodes = resultTask.TreeNodes });

            LogEndMethod(processId:processId);
            return result;
        }
    }
}
