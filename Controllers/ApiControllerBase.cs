﻿using OModules.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public abstract class ApiControllerBase : ApiController
    {
        private Logger logger = new Logger();

        protected string LogStartMethod([CallerMemberName] string callerMember = "", [CallerFilePath] string fileName = "", string processId = "")
        {
            if (string.IsNullOrEmpty(processId))
            {
                processId = Guid.NewGuid().ToString();
            }
            logger.FunctionEntry($"[{processId}] [{fileName}] [{callerMember}]");
            return processId;
        }

        protected void LogEndMethod([CallerMemberName] string callerMember = "", [CallerFilePath] string fileName = "", string processId = "")
        {
            logger.FunctionLeave($"[{processId}] [{fileName}] [{callerMember}]");
        }

        protected void LogError(string message, [CallerMemberName] string callerMember = "", [CallerFilePath] string fileName = "", string processId = "")
        {
            logger.Error($"[{processId}] [{fileName}] [{callerMember}]: {message}");
        }

        protected void LogInfo(string message, [CallerMemberName] string callerMember = "", [CallerFilePath] string fileName = "", string processId = "")
        {
            logger.Info($"[{processId}] [{fileName}] [{callerMember}]: {message}");
        }
    }
}