﻿using LocalizationModule.Models;
using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Models.MessageOutput;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class LocalizationController : AppControllerBase
    {
        

        private LocalizationModule.LocalizationController GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new LocalizationModule.LocalizationController(globals);

            LogEndMethod(processId:processId);
            return result;

        }

        private LocalizedItemsViewModel GetLocalizeItemsViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<LocalizedItemsViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new LocalizedItemsViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedLocalizeItems)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedLocalizeItems)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyLocalizeItems)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceLocalizeItems)),
                    ActionItemComBarInit = Url.Action(nameof(ObjectEditController.ItemComBarInit), nameof(ObjectEditController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueLocalizeItems)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId: processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        private LocalizeNamesViewModel GetLocalizeNamesViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<LocalizeNamesViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new LocalizeNamesViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(ConnectedLocalizeNames)),
                    ActionDisconnected = Url.Action(nameof(DisconnectedLocalizeNames)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReadyLocalizeNames)),
                    ActionValidateReference = Url.Action(nameof(ValidateReferenceLocalizeNames)),
                    ActionItemComBarInit = Url.Action(nameof(ObjectEditController.ItemComBarInit), nameof(ObjectEditController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };

                var paramItems = new Models.RoutValues
                {
                    Sender = viewModelNew.IdInstance
                };

                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValueLocalizeNames)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId: processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> GetGuiEntries(string idReference, string idInstance)
        {
            var processId = LogStartMethod();
            var controller = GetController();

            var request = new LocalizationModule.Models.GetLocalizedGuiEntriesRequest(idReference, System.Globalization.CultureInfo.CurrentCulture.Name);

            var controllerResult = await controller.GetLicalizedGuiEntries(request);

            var result = new ResultSimpleView<List<GuiEntry>>
            {
                IsSuccessful = controllerResult.ResultState.GUID == controller.Globals.LState_Error.GUID,
                Result = controllerResult.Result.GuiEntries
            };

            LogEndMethod(processId:processId);            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedLocalizeItems()
        {

            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetLocalizeItemsViewModel(idInstance);

            LogEndMethod(processId: processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedLocalizeItems()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetLocalizeItemsViewModel(idInstance);

            LogEndMethod(processId: processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ConnectedLocalizeNames()
        {

            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetLocalizeNamesViewModel(idInstance);

            LogEndMethod(processId: processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DisconnectedLocalizeNames()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetLocalizeNamesViewModel(idInstance);

            LogEndMethod(processId: processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyLocalizeItems(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLocalizeItemsViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId: processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReadyLocalizeNames(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLocalizeNamesViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId: processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueLocalizeItems(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLocalizeItemsViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId: processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValueLocalizeNames(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLocalizeNamesViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId: processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceLocalizeItems(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLocalizeItemsViewModel(idInstance);
            var controller = GetController();

            var actionResult = new ResultSimple
            {
                IsSuccessful = true
            };
            var oItemResult = await controller.GetClassObject(refItem.GUID);
            if (oItemResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                actionResult.IsSuccessful = false;
                actionResult.Message = oItemResult.ResultState.Additional1;

                LogEndMethod(processId: processId); return Json(actionResult, JsonRequestBehavior.AllowGet);
            }

            actionResult.SelectorPath = viewModel.SelectorPath;

            viewModel.SetWindowTitle($"{oItemResult.Result.ObjectItem.Name} ({oItemResult.Result.ClassItem.Name}");
            actionResult.ViewItems.Add(viewModel.windowTitle);

            LogEndMethod(processId: processId);
            return Json(actionResult, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReferenceLocalizeNames(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetLocalizeNamesViewModel(idInstance);
            var controller = GetController();

            var actionResult = new ResultSimple
            {
                IsSuccessful = true
            };
            var oItemResult = await controller.GetClassObject(refItem.GUID);
            if (oItemResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                actionResult.IsSuccessful = false;
                actionResult.Message = oItemResult.ResultState.Additional1;

                LogEndMethod(processId: processId); return Json(actionResult, JsonRequestBehavior.AllowGet);
            }

            actionResult.SelectorPath = viewModel.SelectorPath;

            viewModel.SetWindowTitle($"{oItemResult.Result.ObjectItem.Name} ({oItemResult.Result.ClassItem.Name}");
            actionResult.ViewItems.Add(viewModel.windowTitle);

            LogEndMethod(processId: processId);
            return Json(actionResult, JsonRequestBehavior.AllowGet);
        }

        // GET: Translation
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult LocalizeItems()
        {
            var processId = LogStartMethod();
            var viewModel = GetLocalizeItemsViewModel(Guid.NewGuid().ToString());

            LogEndMethod(processId: processId);
            return View(viewModel);
        }

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> LocalizeNamesInit(string idInstance, List<string> selectorPath)
        {
            var processId = LogStartMethod();

            var resultAction = new ResultSimpleView<LocalizeNamesViewModel>
            {
                IsSuccessful = true,
                SelectorPath = selectorPath
            };



            LogEndMethod(processId: processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }
    }
}