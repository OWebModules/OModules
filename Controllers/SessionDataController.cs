﻿using CommandLineRunModule.Models;
using OModules.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class SessionDataController : AppControllerBase
    {
        // GET: SessionData
        public ActionResult Index()
        {
            return View();
        }

        [Authorize, HttpPost]
        public ActionResult SaveSessionData()
        {
            var data = Request.Params["data"];
            var prefix = Request.Params["prefix"];
            var idInstance = Request.Params["idInstance"];
            var extension = Request.Params["extension"];
            var result = new ResultSimple
            {
                IsSuccessful = true
            };

            var fileName = $"{prefix}{idInstance}{(!string.IsNullOrEmpty(extension) ? "." + extension : string.Empty)}";
            var filePath = Path.Combine(Server.MapPath($"~/Resources/UserGroupRessources"), "Sessions", $"_{Session.SessionID}");
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            var fileFullQPath = Path.Combine(filePath, fileName);

            System.IO.File.WriteAllText(fileFullQPath, data);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult UploadReportData(string uploadRequest)
        {
            var result = new ResultSimpleView<string>
            {
                IsSuccessful = true,
                Result = $"{Guid.NewGuid().ToString()}.json"
            };

            var uploadFilePath = Server.MapPath($"~/App_Data/Upload/{result.Result}");

            try
            {
                System.IO.File.WriteAllText(uploadFilePath, System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(uploadRequest)));
            }
            catch (Exception ex)
            {
                result.IsSuccessful = false;
                result.Message = ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult GetSessionDataUrl()
        {
            var prefix = Request.Params["prefix"];
            var idInstance = Request.Params["idInstance"];
            var extension = Request.Params["extension"];
            var result = new ResultSimpleView<string>
            {
                IsSuccessful = true
            };

            var fileName = $"{prefix}{idInstance}{(!string.IsNullOrEmpty(extension) ? "." + extension : string.Empty)}";
            var filePath = Path.Combine(Server.MapPath($"~/Resources/UserGroupRessources"), "Sessions", $"_{Session.SessionID}");
            var fileFullQPath = Path.Combine(filePath, fileName);

            if (!System.IO.File.Exists(fileFullQPath))
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.Result = Url.Content($"~/Resources/UserGroupRessources/Sessions/_{Session.SessionID}/{fileName}");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}