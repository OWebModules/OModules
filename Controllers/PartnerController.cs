﻿using Microsoft.AspNet.Identity;
using OModules.Models;
using OModules.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Controllers;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using PartnerModule.Connectors;
using PartnerModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    
    public class PartnerController : AppControllerBase
    {
        private PartnerModule.Connectors.PartnerConnector GetController()
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var globals = memoryCache.GetOrSet<OntologyAppDBConnector.Globals>(Session.SessionID, "OGlobals", () =>
            {
                return new OntologyAppDBConnector.Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            });

            var result = new PartnerModule.Connectors.PartnerConnector(globals);

            LogEndMethod(processId:processId);
            return result;
        }


        private PartnerViewModel GetViewModel(string key)
        {
            var processId = LogStartMethod();
            var memoryCache = new InMemoryCache();
            var viewModel = memoryCache.GetOrSet<PartnerViewModel>(Session.SessionID, key, () =>
            {
                var viewModelNew = new PartnerViewModel(Session, key, User.Identity.GetUserId())
                {
                    ActionConnected = Url.Action(nameof(Connected)),
                    ActionDisconnected = Url.Action(nameof(Disconnected)),
                    ActionValidateReference = Url.Action(nameof(ValidateReference)),
                    ActionSetViewReady = Url.Action(nameof(SetViewReady)),
                    ActionGetGridConfig = Url.Action(nameof(GetGridConfig)),
                    ActionSaveEmailAddress = Url.Action(nameof(SaveEmailAddress)),
                    ActionSaveTelephone = Url.Action(nameof(SaveTelNumber)),
                    ActionSaveFax = Url.Action(nameof(SaveFaxNr)),
                    ActionSaveUrl = Url.Action(nameof(SaveUrl)),
                    ActionSavePersonalData = Url.Action(nameof(SavePersonalData)),
                    ActionSaveVorname = Url.Action(nameof(SaveVorname)),
                    ActionSaveNachname = Url.Action(nameof(SaveNachname)),
                    ActionSaveName = Url.Action(nameof(SaveName)),
                    ActionSavePlzOrt = Url.Action(nameof(SavePlzOrt)),
                    ActionSaveStrasse = Url.Action(nameof(SaveStrasse)),
                    ActionSavePostfach = Url.Action(nameof(SavePostfach)),
                    ActionSaveGeburtsdatum = Url.Action(nameof(SaveGeburtsdatum)),
                    ActionSaveTodesdatum = Url.Action(nameof(SaveTodesdatum)),
                    ActionNewPartner = Url.Action(nameof(EditOItemController.ObjectNameEdit), nameof(EditOItemController).Replace("Controller", "")),
                    IdSession = HttpContext.Session.SessionID
                };
                viewModelNew.SetActionSetViewItemValue(Url.Action(nameof(ChangeViewItemValue)));

                return viewModelNew;
            });

            LogInfo($"[IdInstance: {viewModel.IdInstance}] [IdView: {viewModel.View?.IdView ?? ""}]", processId:processId);
            LogEndMethod(processId: processId);
            return viewModel;
        }


        [Authorize]
        [HttpPost]
        public ActionResult ChangeViewItemValue(ViewItem viewItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var result = ViewItemController.SetViewItemValue(viewItem, viewModel);
            var resultAction = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>()
            };

            if (result.ViewItem == null)
            {
                return Json(resultAction, JsonRequestBehavior.AllowGet);
            }

            resultAction.ViewItems.Add(result.ViewItem);

            LogEndMethod(processId:processId);
            return Json(resultAction, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Connected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            viewModel.isListen.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            viewModel.isListen.ChangeViewItemValue(ViewItemType.Checked.ToString(), true);
            viewModel.openNewPartner.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

            viewItemList.Add(viewModel.isListen);
            viewItemList.Add(viewModel.openNewPartner);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Disconnected()
        {
            var processId = LogStartMethod();
            var idInstance = Request.Params["idInstance"].ToString();
            var viewItemList = new List<ViewItem>();
            var viewModel = GetViewModel(idInstance);

            LogEndMethod(processId:processId);
            return Json(viewItemList, JsonRequestBehavior.AllowGet);
        }

        [Authorize, HttpPost]
        public ActionResult SetViewReady(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            viewModel.StateMachine.SetState(OntoMsg_Module.StateMachines.GlobalStateMachineState.ViewReady);

            LogEndMethod(processId:processId);
            return Json(viewModel.StateMachine, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ValidateReference(clsOntologyItem refItem, string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);

            var controller = GetController();

            var resultViewItems = new ResultViewItems
            {
                IsSuccessful = true,
                ViewItems = new List<ViewItem>
                {
                    viewModel.inpName,
                    viewModel.inpStrasse,
                    viewModel.inpPostfach,
                    viewModel.inpPLZ,
                    viewModel.inpOrt,
                    viewModel.inpLand,

                    viewModel.inpVorname,
                    viewModel.inpNachname,
                    viewModel.inpGeschlecht,
                    viewModel.inpFamilienstand,
                    viewModel.datepGeburtsdatum,
                    viewModel.inpGebursort,
                    viewModel.datepTodesdatum,
                    viewModel.inpGebursort,
                    viewModel.inpSozialsversicherungsnummer,
                    viewModel.inpETin,
                    viewModel.inpIdentifkationsnummer,
                    viewModel.inpSteuernummer,

                    viewModel.isListenEmail,
                    viewModel.isListenETin,
                    viewModel.isListenFamilienstand,
                    viewModel.isListenFax,
                    viewModel.isListenGebursort,
                    viewModel.isListenGeschlecht,
                    viewModel.isListenIndentifikationsNr,
                    viewModel.isListenLand,
                    viewModel.isListenOrt,
                    viewModel.isListenPlz,
                    viewModel.isListenSozialsversicherungsnummer,
                    viewModel.isListenSteuernummer,
                    viewModel.isListenTelephone,
                    viewModel.isListenUrl,
        }
            };

            var refItemTask = await controller.GetOItem(refItem.GUID, controller.LocalConfig.Globals.Type_Object);

            if (refItemTask.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                resultViewItems.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(resultViewItems, JsonRequestBehavior.AllowGet);
            }

            viewModel.refItem = refItemTask.Result;
              
            if (viewModel.refItem != null)
            {
                var refItemParent = await controller.GetOItem(viewModel.refItem.GUID_Parent, controller.LocalConfig.Globals.Type_Class);

                if (refItemParent.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
                {
                    resultViewItems.IsSuccessful = false;

                    LogEndMethod(processId:processId);
                    return Json(resultViewItems, JsonRequestBehavior.AllowGet);
                }

                viewModel.refParentItem = refItemParent.Result;

                if (viewModel.refItem.GUID_Parent != controller.LocalConfig.OItem_type_partner.GUID)
                {
                    LogEndMethod(processId:processId);
                    return Json(resultViewItems, JsonRequestBehavior.AllowGet);
                }

                viewModel.SetWindowTitle($"{viewModel.refItem.Name} ({viewModel.refParentItem.Name})");
                resultViewItems.ViewItems.Add(viewModel.windowTitle);

                var resultPartnerData = await controller.GetPartnerData(viewModel.refItem);
                if (resultPartnerData.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
                {
                    resultViewItems.IsSuccessful = false;

                    LogEndMethod(processId:processId);
                    return Json(resultViewItems, JsonRequestBehavior.AllowGet);
                }

                viewModel.partner = resultPartnerData.Result;

                viewModel.inpName.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null ? viewModel.partner.Name : "");
                viewModel.inpName.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

                viewModel.inpStrasse.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null  && viewModel.partner.Address != null? viewModel.partner.Address.NameStrasse : "");
                viewModel.inpStrasse.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

                viewModel.inpPostfach.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.Address != null ? viewModel.partner.Address.NamePostfach : "");
                viewModel.inpPostfach.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

                viewModel.inpPLZ.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.Address != null ? viewModel.partner.Address.Plz : "");

                viewModel.inpOrt.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.Address != null ? viewModel.partner.Address.NameOrt : "");

                viewModel.inpLand.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.Address != null ? viewModel.partner.Address.NameLand : "");

                viewModel.inpVorname.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.PersonalData != null ? viewModel.partner.PersonalData.NameVorname : "");
                viewModel.inpVorname.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

                viewModel.inpNachname.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.PersonalData != null ? viewModel.partner.PersonalData.NameNachname : "");
                viewModel.inpNachname.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

                viewModel.inpGeschlecht.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.PersonalData != null ? viewModel.partner.PersonalData.NameGeschlecht : "");

                viewModel.inpFamilienstand.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.PersonalData != null ? viewModel.partner.PersonalData.NameFamilienstand : "");

                if (viewModel.partner != null && viewModel.partner.PersonalData != null && viewModel.partner.PersonalData.Geburtsdatum != null)
                {
                    viewModel.datepGeburtsdatum.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner.PersonalData.Geburtsdatum ?? null);
                }
                viewModel.datepGeburtsdatum.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

                viewModel.inpGebursort.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.PersonalData != null ? viewModel.partner.PersonalData.NameGeburtsort : "");

                if (viewModel.partner != null && viewModel.partner.PersonalData != null && viewModel.partner.PersonalData.Todesdatum != null)
                {
                    viewModel.datepTodesdatum.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner.PersonalData.Todesdatum ?? null);
                }
                viewModel.datepTodesdatum.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);

                viewModel.inpGeschlecht.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.PersonalData != null ? viewModel.partner.PersonalData.NameGeschlecht : "");

                viewModel.inpSozialsversicherungsnummer.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.PersonalData != null ? viewModel.partner.PersonalData.NameSozialversicherungsNummer : "");

                viewModel.inpETin.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.PersonalData != null ? viewModel.partner.PersonalData.ETin : "");

                viewModel.inpIdentifkationsnummer.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.PersonalData != null ? viewModel.partner.PersonalData.NameIdentifikationsNummer : "");

                viewModel.inpSteuernummer.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner != null && viewModel.partner.PersonalData != null ? viewModel.partner.PersonalData.NameSteuerNummer : "");

                viewModel.isListenEmail.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListenETin.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListenFamilienstand.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListenFax.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListenGebursort.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListenGeschlecht.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListenIndentifikationsNr.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListenLand.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListenOrt.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListenPlz.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListenSozialsversicherungsnummer.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListenSteuernummer.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListenTelephone.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
                viewModel.isListenUrl.ChangeViewItemValue(ViewItemType.Enable.ToString(), true);
            }

            LogEndMethod(processId:processId);
            return Json(resultViewItems, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetGridConfig()
        {
            var processId = LogStartMethod();
            var gridConfig = GridFactory.CreateKendoGridConfig(typeof(ComGridItem), Url.Action(nameof(GetComItems)), null, null, null, false, false, false);

            LogEndMethod(processId:processId);
            return Json(gridConfig, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveVorname(string vorname, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultViewItems { IsSuccessful = true, ViewItems = new List<ViewItem>() };
            var viewModel = GetViewModel(idInstance);
            var controller = GetController();

            result.ViewItems.Add(viewModel.inpVorname);

            if (viewModel.partner == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultSaveVorname = await controller.SaveVorname(viewModel.partner, vorname);
            if (resultSaveVorname.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                viewModel.inpVorname.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner.PersonalData.NameVorname);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.inpVorname.ChangeViewItemValue(ViewItemType.Content.ToString(), vorname);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveGeburtsdatum(DateTime geburtsdatum, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultViewItems { IsSuccessful = true, ViewItems = new List<ViewItem>() };
            var viewModel = GetViewModel(idInstance);
            var controller = GetController();

            result.ViewItems.Add(viewModel.datepGeburtsdatum);

            if (viewModel.partner == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultSaveGeburtsdatum = await controller.SaveGeburtsdatum(viewModel.partner, geburtsdatum);
            if (resultSaveGeburtsdatum.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                viewModel.datepGeburtsdatum.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner.PersonalData.Geburtsdatum);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.datepGeburtsdatum.ChangeViewItemValue(ViewItemType.Content.ToString(), geburtsdatum);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveTodesdatum(DateTime todesdatum, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultViewItems { IsSuccessful = true, ViewItems = new List<ViewItem>() };
            var viewModel = GetViewModel(idInstance);
            var controller = GetController();

            result.ViewItems.Add(viewModel.datepTodesdatum);

            if (viewModel.partner == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultSaveTodesdatum = await controller.SaveTodesdatum(viewModel.partner, todesdatum);
            if (resultSaveTodesdatum.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                viewModel.datepTodesdatum.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner.PersonalData.Todesdatum);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.datepTodesdatum.ChangeViewItemValue(ViewItemType.Content.ToString(), todesdatum);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveName(string name, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultViewItems { IsSuccessful = true, ViewItems = new List<ViewItem>() };
            var viewModel = GetViewModel(idInstance);
            var controller = GetController();

            result.ViewItems.Add(viewModel.inpName);

            if (viewModel.partner == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultSaveVorname = await controller.SaveName(viewModel.partner, name);
            if (resultSaveVorname.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                viewModel.inpName.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner.PersonalData.Name);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.inpName.ChangeViewItemValue(ViewItemType.Content.ToString(), name);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveNachname(string nachname, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultViewItems { IsSuccessful = true, ViewItems = new List<ViewItem>() };
            var viewModel = GetViewModel(idInstance);
            var controller = GetController();

            result.ViewItems.Add(viewModel.inpNachname);

            if (viewModel.partner == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultSaveVorname = await controller.SaveNachname(viewModel.partner, nachname);
            if (resultSaveVorname.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                viewModel.inpNachname.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner.PersonalData.NameNachname);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.inpNachname.ChangeViewItemValue(ViewItemType.Content.ToString(), nachname);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SavePlzOrt(clsOntologyItem plzOrOrt, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultViewItems { IsSuccessful = true, ViewItems = new List<ViewItem>() };
            var viewModel = GetViewModel(idInstance);
            var controller = GetController();

            if (viewModel.partner == null)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var addressData = "";
            if (viewModel.partner.Address != null)
            {
                addressData = viewModel.partner.Address.NameOrt;
            }
            viewModel.inpOrt.ChangeViewItemValue(ViewItemType.Content.ToString(), addressData);
            result.ViewItems.Add(viewModel.inpOrt);

            addressData = "";
            if (viewModel.partner.Address != null)
            {
                addressData = viewModel.partner.Address.Plz;
            }
            viewModel.inpPLZ.ChangeViewItemValue(ViewItemType.Content.ToString(), addressData);
            result.ViewItems.Add(viewModel.inpPLZ);

            addressData = "";
            if (viewModel.partner.Address != null)
            {
                addressData = viewModel.partner.Address.IdLand;
            }
            viewModel.inpLand.ChangeViewItemValue(ViewItemType.Content.ToString(), addressData);
            result.ViewItems.Add(viewModel.inpLand);

            var resultOItem = await controller.GetOItem(plzOrOrt.GUID, controller.LocalConfig.Globals.Type_Object);

            if (resultOItem.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultSave = await controller.SavePLZOrt(viewModel.partner, resultOItem.Result);

            if (resultSave.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);

            }

            var plz = resultSave.Result.OItemPlz != null ? resultSave.Result.OItemPlz.Name : "";
            var ort = resultSave.Result.OItemOrt != null ? resultSave.Result.OItemOrt.Name : "";
            var land = resultSave.Result.OItemLand != null ? resultSave.Result.OItemLand.Name : "";

            viewModel.inpPLZ.ChangeViewItemValue(ViewItemType.Content.ToString(), plz);
            viewModel.inpOrt.ChangeViewItemValue(ViewItemType.Content.ToString(), ort);
            viewModel.inpLand.ChangeViewItemValue(ViewItemType.Content.ToString(), land);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveStrasse(string strasse, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultViewItems { IsSuccessful = true, ViewItems = new List<ViewItem>() };
            var viewModel = GetViewModel(idInstance);
            var controller = GetController();

            result.ViewItems.Add(viewModel.inpStrasse);

            if (viewModel.partner == null)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultSaveStrasse = await controller.SaveStrasse(viewModel.partner, strasse);
            if (resultSaveStrasse.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                viewModel.inpStrasse.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner.Address.NameStrasse);

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.inpStrasse.ChangeViewItemValue(ViewItemType.Content.ToString(), strasse);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SavePostfach(string postfach, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultViewItems { IsSuccessful = true, ViewItems = new List<ViewItem>() };
            var viewModel = GetViewModel(idInstance);
            var controller = GetController();

            result.ViewItems.Add(viewModel.inpStrasse);

            if (viewModel.partner == null)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultSaveStrasse = await controller.SavePostfach(viewModel.partner, postfach);
            if (resultSaveStrasse.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;
                viewModel.inpPostfach.ChangeViewItemValue(ViewItemType.Content.ToString(), viewModel.partner.Address.NamePostfach);

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            viewModel.inpPostfach.ChangeViewItemValue(ViewItemType.Content.ToString(), postfach);

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SavePersonalData(clsOntologyItem personalItem, string idInstance)
        {
            var processId = LogStartMethod();
            var result = new ResultViewItems { IsSuccessful = true, ViewItems = new List<ViewItem>() };
            var viewModel = GetViewModel(idInstance);
            var controller = GetController();

            if (viewModel.partner == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (personalItem.GUID_Parent == viewModel.idClassGeschlecht)
            {
                var personalData = "";
                if (viewModel.partner.PersonalData != null)
                {
                    personalData = viewModel.partner.PersonalData.NameGeschlecht;
                }
                viewModel.inpGeschlecht.ChangeViewItemValue(ViewItemType.Content.ToString(), personalData);
                result.ViewItems.Add(viewModel.inpGeschlecht);

            }
            else if (personalItem.GUID_Parent == viewModel.idClassFamienstand)
            {
                var personalData = "";
                if (viewModel.partner.PersonalData != null)
                {
                    personalData = viewModel.partner.PersonalData.NameFamilienstand;
                }
                viewModel.inpFamilienstand.ChangeViewItemValue(ViewItemType.Content.ToString(), personalData);
                result.ViewItems.Add(viewModel.inpFamilienstand);
            }
            else if (personalItem.GUID_Parent == viewModel.idClassGeburtsort)
            {
                var personalData = "";
                if (viewModel.partner.PersonalData != null)
                {
                    personalData = viewModel.partner.PersonalData.NameGeburtsort;
                }
                viewModel.inpFamilienstand.ChangeViewItemValue(ViewItemType.Content.ToString(), personalData);
                result.ViewItems.Add(viewModel.inpGebursort);
                
            }
            else if (personalItem.GUID_Parent == viewModel.idClassSozialversicherungsnummer)
            {
                var personalData = "";
                if (viewModel.partner.PersonalData != null)
                {
                    personalData = viewModel.partner.PersonalData.NameSozialversicherungsNummer;
                }
                viewModel.inpFamilienstand.ChangeViewItemValue(ViewItemType.Content.ToString(), personalData);
                result.ViewItems.Add(viewModel.inpSozialsversicherungsnummer);
                result.ViewItems.Add(viewModel.inpETin);
                result.ViewItems.Add(viewModel.inpIdentifkationsnummer);
                result.ViewItems.Add(viewModel.inpSteuernummer);
            }
            else if (personalItem.GUID_Parent == viewModel.idClassETin)
            {
                var personalData = "";
                if (viewModel.partner.PersonalData != null)
                {
                    personalData = viewModel.partner.PersonalData.ETin;
                }
                viewModel.inpFamilienstand.ChangeViewItemValue(ViewItemType.Content.ToString(), personalData);
                result.ViewItems.Add(viewModel.inpETin);
            }
            else if (personalItem.GUID_Parent == viewModel.idClassIdentificationsnummer)
            {
                var personalData = "";
                if (viewModel.partner.PersonalData != null)
                {
                    personalData = viewModel.partner.PersonalData.NameIdentifikationsNummer;
                }
                viewModel.inpFamilienstand.ChangeViewItemValue(ViewItemType.Content.ToString(), personalData);
                result.ViewItems.Add(viewModel.inpIdentifkationsnummer);
            }
            else if (personalItem.GUID_Parent == viewModel.idClassSteuernummer)
            {
                var personalData = "";
                if (viewModel.partner.PersonalData != null)
                {
                    personalData = viewModel.partner.PersonalData.NameSteuerNummer;
                }
                viewModel.inpFamilienstand.ChangeViewItemValue(ViewItemType.Content.ToString(), personalData);
                result.ViewItems.Add(viewModel.inpSteuernummer);
            }

            var resultOItem = await controller.GetOItem(personalItem.GUID, controller.LocalConfig.Globals.Type_Object);

            if (resultOItem.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultSave = await controller.SavePersonalData(resultOItem.Result, viewModel.partner);

            if (resultSave.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);

            }

            if (resultOItem.Result.GUID_Parent == viewModel.idClassGeschlecht)
            {
                viewModel.inpGeschlecht.ChangeViewItemValue(ViewItemType.Content.ToString(), resultOItem.Result.Name);
            }
            else if (resultOItem.Result.GUID_Parent == viewModel.idClassFamienstand)
            {
                viewModel.inpFamilienstand.ChangeViewItemValue(ViewItemType.Content.ToString(), resultOItem.Result.Name);
            }
            else if (resultOItem.Result.GUID_Parent == viewModel.idClassGeburtsort)
            {
                viewModel.inpGebursort.ChangeViewItemValue(ViewItemType.Content.ToString(), resultOItem.Result.Name);
            }
            else if (resultOItem.Result.GUID_Parent == viewModel.idClassSozialversicherungsnummer)
            {
                viewModel.inpSozialsversicherungsnummer.ChangeViewItemValue(ViewItemType.Content.ToString(), resultOItem.Result.Name);
            }
            else if (resultOItem.Result.GUID_Parent == viewModel.idClassIdentificationsnummer)
            {
                viewModel.inpIdentifkationsnummer.ChangeViewItemValue(ViewItemType.Content.ToString(), resultOItem.Result.Name);
            }
            else if (resultOItem.Result.GUID_Parent == viewModel.idClassSteuernummer)
            {
                viewModel.inpSteuernummer.ChangeViewItemValue(ViewItemType.Content.ToString(), resultOItem.Result.Name);
            }
            else if (resultOItem.Result.GUID_Parent == viewModel.idClassETin)
            {
                viewModel.inpETin.ChangeViewItemValue(ViewItemType.Content.ToString(), resultOItem.Result.Name);
            }

            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveEmailAddress(clsOntologyItem emailAddress, string idInstance)
        {
            var processId = LogStartMethod();
            var result = await SaveComItem(emailAddress, idInstance, ComItemType.Email);

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveTelNumber(clsOntologyItem telNumber, string idInstance)
        {
            var processId = LogStartMethod();
            var result = await SaveComItem(telNumber, idInstance, ComItemType.Tel);

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveFaxNr(clsOntologyItem telNumber, string idInstance)
        {
            var processId = LogStartMethod();
            var result = await SaveComItem(telNumber, idInstance, ComItemType.Fax);

            LogEndMethod(processId:processId);
            return result;
        }

        [Authorize]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SaveUrl(clsOntologyItem url, string idInstance)
        {
            var processId = LogStartMethod();
            var result = await SaveComItem(url, idInstance, ComItemType.Url);

            LogEndMethod(processId:processId);
            return result;
        }

        private async System.Threading.Tasks.Task<ActionResult> SaveComItem(clsOntologyItem comItem, string idInstance, ComItemType comItemType)
        {
            var processId = LogStartMethod();
            var result = new ResultViewItems { IsSuccessful = true, ViewItems = new List<ViewItem>() };
            var viewModel = GetViewModel(idInstance);
            var controller = GetController();
            if (viewModel.partner == null)
            {
                result.IsSuccessful = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            var resultOItem = await controller.GetOItem(comItem.GUID, controller.LocalConfig.Globals.Type_Object);

            if (resultOItem.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                result.IsSuccessful = false;

                LogEndMethod(processId:processId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (comItemType == ComItemType.Email)
            {
                var resultSave = await controller.SaveEmailAddress(resultOItem.Result, viewModel.partner);

                if (resultSave.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                }

                viewModel.comGrid.ChangeViewItemValue(ViewItemType.AddRow.ToString(), resultSave.Result);
                result.ViewItems.Add(viewModel.comGrid);
            }
            else if (comItemType == ComItemType.Fax)
            {
                var resultSave = await controller.SaveFaxNumber(resultOItem.Result, viewModel.partner);

                if (resultSave.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                }

                viewModel.comGrid.ChangeViewItemValue(ViewItemType.AddRow.ToString(), resultSave.Result);
                result.ViewItems.Add(viewModel.comGrid);
            }
            else if (comItemType == ComItemType.Tel)
            {
                var resultSave = await controller.SaveTelNumber(resultOItem.Result, viewModel.partner);

                if (resultSave.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                }

                viewModel.comGrid.ChangeViewItemValue(ViewItemType.AddRow.ToString(), resultSave.Result);
                result.ViewItems.Add(viewModel.comGrid);
            }
            else if (comItemType == ComItemType.Url)
            {
                var resultSave = await controller.SaveUrlNumber(resultOItem.Result, viewModel.partner);

                if (resultSave.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
                {
                    result.IsSuccessful = false;
                }

                viewModel.comGrid.ChangeViewItemValue(ViewItemType.AddRow.ToString(), resultSave.Result);
                result.ViewItems.Add(viewModel.comGrid);
            }


            LogEndMethod(processId:processId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        

        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> GetComItems(string idInstance)
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(idInstance);
            var controller = GetController();
            if (viewModel.refItem == null)
            {
                var resultEmptyList = controller.GetEmptyComItemList();

                LogEndMethod(processId:processId);
                return Json(resultEmptyList.Result, JsonRequestBehavior.AllowGet);
            }

            var resultTask = await controller.GetComItemList(viewModel.partner);

            if (resultTask.ResultState.GUID == controller.LocalConfig.Globals.LState_Error.GUID)
            {
                var resultEmptyList = controller.GetEmptyComItemList();

                LogEndMethod(processId:processId);
                return Json(resultEmptyList.Result, JsonRequestBehavior.AllowGet);
            }

            var resultItem = new KendoGridDataResult<ComGridItem>(resultTask.Result);
            var result = Json(resultItem, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;

            LogEndMethod(processId:processId);
            return result;
        }

        // GET: Partner
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Partner()
        {
            var processId = LogStartMethod();
            var viewModel = GetViewModel(Guid.NewGuid().ToString());
            var controller = GetController();

            viewModel.ActionNewPartner += $"?Sender={viewModel.IdInstance}&Class={controller.LocalConfig.OItem_type_partner.GUID}";

            viewModel.idClassETin = controller.LocalConfig.OItem_type_etin.GUID;
            viewModel.idClassEmail = controller.LocalConfig.OItem_type_email_address.GUID;
            viewModel.idClassFamienstand = controller.LocalConfig.OItem_type_familienstand.GUID;
            viewModel.idClassGeburtsort = controller.LocalConfig.OItem_type_ort.GUID;
            viewModel.idClassGeschlecht = controller.LocalConfig.OItem_type_geschlecht.GUID;
            viewModel.idClassIdentificationsnummer = controller.LocalConfig.OItem_type_identifkationsnummer__idnr_.GUID;
            viewModel.idClassLand = controller.LocalConfig.OItem_type_land.GUID;
            viewModel.idClassOrt = controller.LocalConfig.OItem_type_ort.GUID;
            viewModel.idClassPhone = controller.LocalConfig.OItem_type_telefonnummer.GUID;
            viewModel.idClassPLZ = controller.LocalConfig.OItem_type_postleitzahl.GUID;
            viewModel.idClassSozialversicherungsnummer = controller.LocalConfig.OItem_type_sozialversicherungsnummer.GUID;
            viewModel.idClassSteuernummer = controller.LocalConfig.OItem_type_steuernummer.GUID;
            viewModel.idClassUrl = controller.LocalConfig.OItem_type_url.GUID;
            viewModel.idClassPartner = controller.LocalConfig.OItem_type_partner.GUID;

            LogEndMethod(processId:processId);
            return View(viewModel);
        }
    }
}