﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OModules.Controllers
{
    public class ErrorController : AppControllerBase
    {
        // GET: Error
        public ActionResult NotFound()
        {
            return View();
        }
    }
}