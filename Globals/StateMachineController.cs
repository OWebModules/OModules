﻿using OntoMsg_Module.StateMachines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OModules.Globals
{
    public static class StateMachineController
    {
        public static ControllerStateMachine RegisterStateMachine(HttpSessionStateBase session, string viewId, string endPointId)
        {
            var stateMachines = (List<ControllerStateMachine>)session["stateMachines"];

            if (stateMachines == null)
            {
                stateMachines = new List<ControllerStateMachine>();
                session["stateMachines"] = stateMachines;
            }

            var stateMachine = stateMachines.FirstOrDefault(stateMach => stateMach.ViewId == viewId);

            if (stateMachine == null)
            {
                stateMachine = new ControllerStateMachine(StateMachineType.Generic, viewId, endPointId);
                stateMachines.Add(stateMachine);
            }

            return stateMachine;
        }
    }
}