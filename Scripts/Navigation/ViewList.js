﻿var viewListItem = {
    viewListContainer: null,
    treeListElement: null,
    treeList: null,
    toolbar: null,
    configuration: null,
    treeSelectionChanged: function (id) {
        $('.viewListTreeList').openPageLoader();
        $.post(viewListItem.configuration.actionGetUrl, { id: id, idInstance: viewModel.IdInstance })
            .done(function (response) {
                // stuff
                if (response.IsSuccessful && response.Url !== null) {
                    $('.viewListTreeList').closePageLoader();
                    window.open(response.Url, "_blank");
                }


            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    }
};
(function ($) {
    $.fn.createViewList = function (configuration) {
        viewListItem.configuration = configuration;
        viewListItem.viewListContainer = $(this);
        
        var toolbar = $('<div class="viewListToolbar"></div>');

        viewListItem.viewListContainer.append(toolbar);
        viewListItem.treeListElement = $('<div class="viewListTreeList"></div>');
        viewListItem.viewListContainer.append(viewListItem.treeListElement);

        viewListItem.toolbar = $(toolbar).kendoToolBar({
            items: [
                {
                    type: "buttonGroup",
                    id: "btnViewGroupFilter",
                    //ButtonGroup's items accept the same appearance configration optinos as the button control
                    buttons: [
                        { text: "All", id: "btnViewFilterAll", togglable: true, group: "controlGroup" },
                        { type: "separator" },
                        { text: "A", id: "btnViewFilterA", togglable: true, group: "controlGroup" },
                        { text: "B", id: "btnViewFilterB", togglable: true, group: "controlGroup" },
                        { text: "C", id: "btnViewFilterC", togglable: true, group: "controlGroup" },
                        { text: "D", id: "btnViewFilterD", togglable: true, group: "controlGroup" },
                        { text: "E", id: "btnViewFilterE", togglable: true, group: "controlGroup" },
                        { text: "F", id: "btnViewFilterF", togglable: true, group: "controlGroup" },
                        { text: "G", id: "btnViewFilterG", togglable: true, group: "controlGroup" },
                        { text: "H", id: "btnViewFilterH", togglable: true, group: "controlGroup" },
                        { text: "I", id: "btnViewFilterI", togglable: true, group: "controlGroup" },
                        { text: "J", id: "btnViewFilterJ", togglable: true, group: "controlGroup" },
                        { text: "K", id: "btnViewFilterK", togglable: true, group: "controlGroup" },
                        { text: "L", id: "btnViewFilterL", togglable: true, group: "controlGroup" },
                        { text: "M", id: "btnViewFilterM", togglable: true, group: "controlGroup" },
                        { text: "N", id: "btnViewFilterN", togglable: true, group: "controlGroup" },
                        { text: "O", id: "btnViewFilterO", togglable: true, group: "controlGroup" },
                        { text: "P", id: "btnViewFilterP", togglable: true, group: "controlGroup" },
                        { text: "Q", id: "btnViewFilterQ", togglable: true, group: "controlGroup" },
                        { text: "R", id: "btnViewFilterR", togglable: true, group: "controlGroup" },
                        { text: "S", id: "btnViewFilterS", togglable: true, group: "controlGroup" },
                        { text: "T", id: "btnViewFilterT", togglable: true, group: "controlGroup" },
                        { text: "U", id: "btnViewFilterU", togglable: true, group: "controlGroup" },
                        { text: "V", id: "btnViewFilterV", togglable: true, group: "controlGroup" },
                        { text: "W", id: "btnViewFilterW", togglable: true, group: "controlGroup" },
                        { text: "X", id: "btnViewFilterX", togglable: true, group: "controlGroup" },
                        { text: "Y", id: "btnViewFilterY", togglable: true, group: "controlGroup" },
                        { text: "Z", id: "btnViewFilterZ", togglable: true, group: "controlGroup" },

                    ]
                }
            ],
            toggle: function (e) {
                var btnFilter = 'btnViewFilter';
                if (e.id.startsWith(btnFilter)) {
                    var filter = e.id.replace(btnFilter, '');
                    if (filter.length == 1) {
                        var dataSource = viewListItem.treeList.data("kendoTreeList").dataSource;
                        dataSource.filter({ field: "Name", operator: "startswith", value: filter });
                    }
                    else {
                        var dataSource = viewListItem.treeList.data("kendoTreeList").dataSource;
                        dataSource.filter([]);
                    }

                }
            }
        });

        $('body').openPageLoader();
        $.post(viewListItem.configuration.actionGetGridConfig)
            .done(function (response) {
                // stuff
                console.log(response.dataSource);
                $.extend(response.dataSource.transport.read, {
                    data: function () {
                        return { idInstance: viewModel.IdInstance };
                    }
                })

                viewListItem.treeList = $(viewListItem.treeListElement).kendoTreeList({
                    dataSource: response.dataSource,
                    height: '500px',
                    width: '100%',
                    filterable: true,
                    sortable: true,
                    selectable: "row",
                    columns: [
                        { field: "Name", title: "View" }
                    ],
                    change: function (e) {
                        var selectedRows = this.select();
                        var selectedDataItems = [];
                        for (var i = 0; i < selectedRows.length; i++) {
                            var dataItem = this.dataItem(selectedRows[i]);
                            viewListItem.treeSelectionChanged(dataItem.id);
                        }
                        // selectedDataItems contains all selected data items
                    }
                });

                $('body').closePageLoader();
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    }
})(jQuery);