﻿var reportViewerElement;
var dataReportViewer;
var reportRecord = 0;
(function ($) {
    $.fn.createReportViewer = function (configuration) {
        dataReportViewer = 'report-viewer-config';
        selectorReportViewer = configuration.selector;
        $(this).data(dataReportViewer, {
            elements: {
                reportViewerContainerElement: null,
                gridContainer: null,
                grid: null,
                gridItem: null,
                dataItem: null,
                dataSource: null,
                oItem: null
            },
            data: {
                query: "",
                field: "",
                value: "",
                initBound: false
            },
            configuration: configuration,
            handler: {
                validateReference: function (oItem, validateReferenceCallback) {
                    oItem.GUID_Related = reportViewerElement.configuration.additionalIdViewModel;
                    reportViewerElement.elements.oItem = oItem;

                    $.post(reportViewerElement.configuration.viewModel.ActionValidateReference, { refItem: oItem, idInstance: reportViewerElement.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            console.log("connected", response);
                            if (reportViewerElement.configuration.setDocumentTitle) {
                                var viewItem = getViewItem(response, "windowTitle");
                                var title = getViewItemValue(viewItem, "Content");
                                document.title = title;
                            }
                            changeValuesFromServerChildren(response, [selectorReportViewer]);
                            $.post(reportViewerElement.configuration.viewModel.ActionGetGridConfig, { idInstance: reportViewerElement.configuration.viewModel.IdInstance, additionalFields: reportViewerElement.configuration.additionalFields, getDataUrl: reportViewerElement.configuration.actionGetData })
                                .done(function (response) {

                                    response.Result.dataSource.change = function (e) {
                                        if (e.action === "itemchange") {

                                        }
                                    }

                                    if (reportViewerElement.elements.dataSource === undefined ||
                                        reportViewerElement.elements.dataSource === null) {
                                        reportViewerElement.elements.dataSource = new kendo.data.DataSource(response.Result.dataSource);

                                    } else {
                                        $(reportViewerElement.elements.grid).data("kendoGrid").destroy(); // destroy the Grid

                                        $(reportViewerElement.elements.grid).empty(); // empty the Grid content (inner HTML)
                                    }

                                    $.extend(response.Result.dataSource.transport.read, {
                                        data: function () {
                                            var inputQuery = $(reportViewerElement.elements.grid).find('.inputQuery');
                                            if (inputQuery.length > 0) {
                                                reportViewerElement.data.query = utf8_to_b64($(inputQuery).val());
                                            }
                                            return {
                                                idInstance: reportViewerElement.configuration.viewModel.IdInstance,
                                                query: reportViewerElement.data.query
                                            };
                                        }
                                    })

                                    response.Result.dataSource = reportViewerElement.elements.dataSource;


                                    $.extend(response.Result, {
                                        change: function (e) {

                                            $(reportViewerElement.configuration.selector).find('.applyItem').setEnabled(false, true);
                                            $(reportViewerElement.configuration.selector).find('.applyItem').attr('title', '');
                                            $(reportViewerElement.configuration.selector).find('.openModuleStarter').setEnabled(false, true);
                                            $(reportViewerElement.configuration.selector).find('.openModuleStarter').attr('title', '');
                                            reportViewerElement.elements.gridItem = $(reportViewerElement.elements.grid).data("kendoGrid");

                                            var selected = this.select();

                                            if (selected.length == 1) {

                                                var cell = this.select();
                                                var cellIndex = cell[0].cellIndex;
                                                var grid = e.sender;
                                                var columnTitle = $(grid.thead.find('th')[cellIndex]).data("field");
                                                column = null;
                                                var columnsFound = jQuery.grep(this.columns, function (col, colIx) { return col.field == columnTitle; });
                                                if (columnsFound.length > 0) {
                                                    column = columnsFound[0];
                                                }
                                                if (column == null) return;

                                                var row = this.dataItem(cell.closest("tr"));
                                                var rowItem = [];
                                                for (var ix in this.columns) {
                                                    rowItem.push({
                                                        columnName: this.columns[ix].field,
                                                        cellValue: utf8_to_b64(row[this.columns[ix].field])
                                                    });
                                                }

                                                reportViewerElement.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                                    ChannelId: reportViewerElement.configuration.viewModel.ChannelSelectedCell,
                                                    SenderId: reportViewerElement.configuration.managerConfig.idEndpoint,
                                                    SessionId: reportViewerElement.configuration.managerConfig.idSession,
                                                    ReceiverId: reportViewerElement.configuration.viewModel.idSender,
                                                    ViewId: reportViewerElement.configuration.viewModel.View.IdView,
                                                    UserId: reportViewerElement.configuration.viewModel.IdUser,
                                                    GenericParameterItems: [$(cell).html()]
                                                });

                                                reportViewerElement.configuration.viewModel.sendObjects = [{ row: rowItem, columnNameSelected: column.field }];
                                                $.post(reportViewerElement.configuration.viewModel.ActionGetObjectForSend, { objectsForSend: reportViewerElement.configuration.viewModel.sendObjects, idInstance: reportViewerElement.configuration.viewModel.IdInstance })
                                                    .done(function (response) {
                                                        if (response.IsSuccessful) {
                                                            reportViewerElement.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                                                ChannelId: reportViewerElement.configuration.viewModel.ChannelSelectedObject,
                                                                SenderId: reportViewerElement.configuration.managerConfig.idEndpoint,
                                                                SessionId: reportViewerElement.configuration.managerConfig.idSession,
                                                                ReceiverId: reportViewerElement.configuration.viewModel.idSender,
                                                                ViewId: reportViewerElement.configuration.viewModel.View.IdView,
                                                                UserId: reportViewerElement.configuration.viewModel.IdUser,
                                                                OItems: response.Result
                                                            });

                                                            reportViewerElement.configuration.viewModel.applyObjects = response.Result;
                                                            $(reportViewerElement.configuration.selector).find('.applyItem').attr('title', response.Result.length);
                                                            var viewItem = $(reportViewerElement.configuration.selector).find('.applyItem').data("viewItem");
                                                            setEnabled([reportViewerElement.configuration.selector], viewItem, true, true);
                                                            var viewItem1 = $(reportViewerElement.configuration.selector).find('.openModuleStarter').data("viewItem");
                                                            setEnabled([reportViewerElement.configuration.selector], viewItem1, true, true);

                                                            var viewItem1 = $(reportViewerElement.configuration.selector).find('.openObjectEditor').data("viewItem");
                                                            setEnabled([reportViewerElement.configuration.selector], viewItem1, true, true);

                                                            if (response.Result.length == 1) {
                                                                $(reportViewerElement.configuration.selector).find('.openModuleStarter').attr('title', response.Result[0].Name);
                                                                var urlSelected = reportViewerElement.configuration.viewModel.ActionModuleStarter + "?Object=" + response.Result[0].GUID;
                                                                $(reportViewerElement.configuration.selector).find('.openModuleStarter').data("selectedUrl", urlSelected);

                                                                $(reportViewerElement.configuration.selector).find('.openObjectEditor').attr('title', response.Result[0].Name);
                                                                var urlSelected = reportViewerElement.configuration.viewModel.ActionObjectEditor + "?Object=" + response.Result[0].GUID;
                                                                $(reportViewerElement.configuration.selector).find('.openObjectEditor').data("selectedUrl", urlSelected);
                                                            }
                                                        }


                                                    })
                                                    .fail(function (jqxhr, textStatus, error) {
                                                        // stuff
                                                    });


                                            } else if (selected.length > 1) {
                                                reportViewerElement.configuration.viewModel.sendObjects = [];
                                                for (var ix = 0; ix < selected.length; ix++) {
                                                    var cell = selected[ix];
                                                    var cellIndex = cell.cellIndex;
                                                    var column = this.columns[cellIndex];
                                                    var row = this.dataItem(cell.closest("tr"));
                                                    var rowItem = [];

                                                    for (var jx = 0; jx < this.columns.length; jx++) {
                                                        rowItem.push({
                                                            columnName: this.columns[jx].field,
                                                            cellValue: utf8_to_b64(row[this.columns[jx].field])
                                                        });
                                                    }
                                                    reportViewerElement.configuration.viewModel.sendObjects.push({ row: rowItem, columnNameSelected: column.field });

                                                }
                                                $.post(reportViewerElement.configuration.viewModel.ActionGetObjectForSend, { objectsForSend: reportViewerElement.configuration.viewModel.sendObjects, idInstance: reportViewerElement.configuration.viewModel.IdInstance })
                                                    .done(function (response) {
                                                        if (response.IsSuccessful) {
                                                            reportViewerElement.configuration.viewModel.applyObjects = response.Result;
                                                            $(reportViewerElement.configuration.selector).find('.applyItem').attr('title', response.Result.length);
                                                            var viewItem = $(reportViewerElement.configuration.selector).find('.applyItem').data("viewItem");
                                                            setEnabled([reportViewerElement.configuration.selector], viewItem, true, true);
                                                        }


                                                    })
                                                    .fail(function (jqxhr, textStatus, error) {
                                                        // stuff
                                                    });
                                            }





                                        }, toolbar: kendo.template($("#template").html()),
                                        excel: {
                                            proxyURL: "/save"
                                        },
                                        autoBind: false,
                                        dataBinding: function () {
                                            reportRecord = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                                        },
                                        dataBound: function () {

                                            $(reportViewerElement.elements.reportViewerContainerElement).find('.inputQuery').unbind("keyup paste");
                                            $(reportViewerElement.elements.reportViewerContainerElement).find('.inputQuery').on("keyup paste", function () {
                                                if (event.keyCode === 13) {
                                                    $(selectorReportViewer).data(dataReportViewer).elements.gridItem.dataSource.read();
                                                }
                                                else {
                                                    changeValue(reportViewerElement.configuration.viewModel.inputQuery, "Content", $(this).val(), true, function (response) {
                                                        var viewItems = $.grep(response.ViewItems, function (item) {
                                                            return item.ViewItemId != reportViewerElement.configuration.viewModel.inputQuery.ViewItemId;
                                                        });
                                                        changeValuesFromServerChildren(viewItems, [selectorReportViewer]);
                                                    }, viewModel.IdInstance);
                                                }
                                            });

                                            $(reportViewerElement.elements.reportViewerContainerElement).find('.inputQuery').autocomplete({
                                                serviceUrl: reportViewerElement.configuration.viewModel.ActionGetAutoCompletesFilter + '?idInstance=' + reportViewerElement.configuration.viewModel.IdInstance,
                                                dataType: "json",
                                                deferRequestBy: 300,
                                                noCache: true,
                                                //lookup: countriesArray,
                                                lookupFilter: function (suggestion, originalQuery, queryLowerCase) {
                                                    var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
                                                    return re.test(suggestion.value);
                                                },
                                                onSelect: function (suggestion) {
                                                    $(reportViewerElement.elements.reportViewerContainerElement).find('.inputQuery').val(suggestion.filter);
                                                    changeValue(viewModel.inputQuery, "Content", suggestion.filter, true, function (resonse) {
                                                    }, viewModel.IdInstance);
                                                },
                                                onHint: function (hint) {

                                                },
                                                onInvalidateSelection: function () {

                                                },
                                                onSearchStart: function (query) {
                                                    $(reportViewerElement.elements.reportViewerContainerElement).find('.inputQuery').css('border-color', 'coral');
                                                },
                                                onSearchComplete: function (query, suggestions) {
                                                    $(reportViewerElement.elements.reportViewerContainerElement).find('.inputQuery').css('border-color', '');
                                                }
                                            });

                                            $(reportViewerElement.elements.reportViewerContainerElement).find('.buttonSave').unbind("click");
                                            $(reportViewerElement.elements.reportViewerContainerElement).find('.buttonSave').on("click", function () {
                                                $.post(reportViewerElement.configuration.viewModel.ActionSavePattern, { idInstance: reportViewerElement.configuration.viewModel.IdInstance })
                                                    .done(function (response) {
                                                        if (response.IsSuccessful) {
                                                            if (typeof response.Message !== 'undefined' && response.Message != null) {
                                                                $('.onto-status-bar').data('messageout-config').showMessage(response.Message, 2000);
                                                            }
                                                        } else {
                                                            $('.onto-status-bar').data('messageout-config').showError(response.Message);
                                                        }
                                                    })
                                                    .fail(function (jqxhr, textStatus, error) {
                                                        // stuff
                                                        $('body').closePageLoader();
                                                    });
                                            })

                                            if (typeof reportViewerElement.data.rowDiff !== 'undefined' &&
                                                reportViewerElement.data.rowDiff != null &&
                                                reportViewerElement.data.rowDiff.Field1 != null &&
                                                reportViewerElement.data.rowDiff.Field2 != null) {
                                                var rows = $(this.element).find('.k-grid-content').find('tr');

                                                var field1 = reportViewerElement.data.rowDiff.Field1.NameCol;
                                                var field2 = reportViewerElement.data.rowDiff.Field2.NameCol;
                                                $(rows).each(function (rowIx) {
                                                    var colField1 = $.grep($(this).find('td'), function (col, colIx) {
                                                        return $(col).data('field') == field1;
                                                    });

                                                    var colField2 = $.grep($(this).find('td'), function (col, colIx) {
                                                        return $(col).data('field') == field2;
                                                    });

                                                    var html1 = $(colField1).html();
                                                    var html2 = $(colField2).html();
                                                    var idReference = guid();
                                                    if (html1 != '' || html2 != '') {
                                                        var diffItem = {
                                                            IdReference: idReference,
                                                            Html1: utf8_to_b64(html1),
                                                            Html2: utf8_to_b64(html2)
                                                        }
                                                        $(colField1).attr('data-diffId', idReference);
                                                        reportViewerElement.data.htmlDiffItems.push(diffItem);
                                                    }
                                                });

                                                if (reportViewerElement.data.htmlDiffItems.length > 0) {
                                                    $.post(reportViewerElement.configuration.viewModel.ActionHtmlDiff, { htmlDiffItems: reportViewerElement.data.htmlDiffItems })
                                                        .done(function (response) {
                                                            if (response.IsSuccessful) {
                                                                response.Result.forEach(function (htmlDiffItem) {
                                                                    var idReference = htmlDiffItem.IdReference;
                                                                    var htmlDiff = b64_to_utf8(htmlDiffItem.HtmlDiff);
                                                                    var element = $(reportViewerElement.elements.grid).find('.k-grid-content').find("[data-diffId='" + idReference + "']");
                                                                    $(element).html(htmlDiff);
                                                                });
                                                            }
                                                        })
                                                        .fail(function (jqxhr, textStatus, error) {
                                                            // stuff
                                                            $('body').closePageLoader();
                                                        });
                                                }
                                            }
                                            
                                            reportViewerElement.handler.triggerEvents.dataBound(reportViewerElement, false);
                                        }

                                    })

                                    if (response.DataItems.length > 0) {
                                        reportViewerElement.data.rowDiff = response.DataItems[0];
                                        reportViewerElement.data.htmlDiffItems = [];
                                    }
                                    
                                    $(reportViewerElement.elements.grid).kendoGrid(response.Result);
                                    //setTimeout(function () {
                                    //    dataSource.read();
                                    //}, 500);

                                    changeValuesFromServerChildren(response.ViewItems, [selectorReportViewer]);
                                    reportViewerElement.elements.gridItem = $(reportViewerElement.elements.grid).data("kendoGrid");

                                    $(reportViewerElement.elements.grid).on('cellselect', function (event) {
                                        // event arguments.
                                        var args = event.args;
                                        // column data field.
                                        var datafield = event.args.datafield;
                                        // row's bound index.
                                        var rowBoundIndex = args.rowindex;
                                        // new cell value.
                                        var value = args.newvalue;
                                        // old cell value.
                                        var oldvalue = args.oldvalue;

                                    });

                                    $(reportViewerElement.elements.grid).on('cellvaluechanged', function (event) {
                                        // event arguments.
                                        var args = event.args;
                                        // column data field.
                                        var datafield = event.args.datafield;
                                        // row's bound index.
                                        var rowBoundIndex = args.rowindex;
                                        // new cell value.
                                        var value = args.newvalue;
                                        // old cell value.
                                        var oldvalue = args.oldvalue;

                                    });
                                    $(reportViewerElement.configuration.selector).find('.k-button').kendoButton({
                                        click: function () {
                                            if ($(this.element).hasClass("isListen")) {
                                                $(this.element).toggleMode(true);
                                            } else if ($(this.element).hasClass('applyItem')) {
                                                if (reportViewerElement.configuration.viewModel.applyObjects != undefined && reportViewerElement.configuration.viewModel.applyObjects != null && reportViewerElement.configuration.viewModel.applyObjects.length > 0) {
                                                    reportViewerElement.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                                        ChannelId: reportViewerElement.configuration.viewModel.ChannelAppliedObject,
                                                        SenderId: reportViewerElement.configuration.managerConfig.idEndpoint,
                                                        SessionId: reportViewerElement.configuration.managerConfig.idSession,
                                                        ReceiverId: reportViewerElement.configuration.viewModel.idSender,
                                                        ViewId: reportViewerElement.configuration.viewModel.View.IdView,
                                                        UserId: reportViewerElement.configuration.viewModel.IdUser,
                                                        OItems: reportViewerElement.configuration.viewModel.applyObjects
                                                    });

                                                }
                                            } else if ($(this.element).hasClass('openModuleStarter')) {
                                                var url = $(this.element).data("selectedUrl");
                                                window.open(url, "_blank");
                                            } else if ($(this.element).hasClass('openObjectEditor')) {
                                                var url = $(this.element).data("selectedUrl");
                                                window.open(url, "_blank");
                                            } else if ($(this.element).hasClass('syncData')) {
                                                if (reportViewerElement.elements.oItem == null) return;
                                                var button = $(reportViewerElement.configuration.selector).find('.syncData');

                                                reportViewerElement.handler.checkState();
                                                button.addClass('syncing');
                                                button.removeClass('syncerror');
                                                var startSync = new Date();
                                                $.post(reportViewerElement.configuration.viewModel.ActionSyncReport, { idReport: reportViewerElement.elements.oItem.GUID, idInstance: reportViewerElement.configuration.viewModel.IdInstance })
                                                    .done(function (response) {


                                                    })
                                                    .fail(function (jqxhr, textStatus, error) {
                                                        // stuff
                                                        button.removeClass('syncing');
                                                        button.addClass('syncerror');
                                                        button.attr('title', 'Error last Sync');
                                                        $('body').closePageLoader();
                                                    });


                                            } else if ($(this.element).hasClass('processReport')) {
                                                var data = reportViewerElement.elements.gridItem.dataSource.view();
                                                var reportItem = reportViewerElement.elements.oItem;
                                                var processReport = {
                                                    reportItem: reportItem,
                                                    data: data
                                                };
                                                var request = utf8_to_b64(JSON.stringify(processReport));
                                                $.post(reportViewerElement.configuration.viewModel.ActionUploadReportData, { uploadRequest: request })
                                                    .done(function (response) {
                                                        console.log("connected", response);
                                                        if (response.IsSuccessful) {
                                                            reportViewerElement.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                                                ChannelId: reportViewerElement.configuration.viewModel.ChannelProcessReport,
                                                                SenderId: reportViewerElement.configuration.managerConfig.idEndpoint,
                                                                SessionId: reportViewerElement.configuration.managerConfig.idSession,
                                                                ReceiverId: reportViewerElement.configuration.viewModel.idSender,
                                                                ViewId: reportViewerElement.configuration.viewModel.View.IdView,
                                                                UserId: reportViewerElement.configuration.viewModel.IdUser,
                                                                GenericParameterItems: [response.Result]
                                                            });
                                                        }
                                                    })
                                                    .fail(function (jqxhr, textStatus, error) {
                                                        // stuff
                                                        $('body').closePageLoader();
                                                    });

                                                
                                            }
                                        }
                                    });
                                    changeValuesFromServerChildren(reportViewerElement.configuration.viewModel.initViewItems, [selectorReportViewer]);
                                    reportViewerElement.elements.dataSource.read();

                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    // stuff
                                });
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                            $('body').closePageLoader();
                        });
                },
                setFilter: function (field, fieldValue, operator) {
                    if (typeof (field) === 'undefined' || typeof (fieldValue) === 'undefined') return;
                    $(selectorReportViewer).data(dataReportViewer).elements.gridItem.dataSource.filter({
                        field: field,
                        operator: operator,
                        value: fieldValue
                    });
                },
                reloadReport: function () {
                    $(selectorReportViewer).data(dataReportViewer).elements.gridItem.dataSource.read();
                },
                syncData: function () {

                },
                gridChangeHandler: function () {
                    var selectedRows = this.select();

                    if (selectedRows.length !== 1) {
                        return;
                    }

                    reportViewerElement.elements.dataItemRow = reportViewerElement.elements.gridItem.select();
                    reportViewerElement.elements.dataItem = reportViewerElement.elements.gridItem.dataItem(reportViewerElement.elements.gridItem.select());
                },
                checkState: function () {
                    setTimeout(function () {
                        $.post(reportViewerElement.configuration.viewModel.ActionGetSyncState, { idInstance: reportViewerElement.configuration.viewModel.IdInstance })
                            .done(function (response) {
                                console.log("connected", response);
                                changeValuesFromServerChildren(response.ViewItems, [selectorReportViewer]);
                                var running = getViewItemsValue(response.ViewItems, "syncData", "Other");
                                if (running) {
                                    reportViewerElement.handler.checkState();
                                }
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                                $('body').closePageLoader();
                            });
                    }, 1000);
                },
                isListenToggled: function () {
                    return $(reportViewerElement.configuration.selector).find('.isListen').isToggled();
                },
                triggerEvents: {
                    createComponent: function (reportViewerElement, finishState) {
                        if (finishState == true) {
                            if (typeof reportViewerElement.configuration.events !== 'undefined' && reportViewerElement.configuration.events !== null &&
                                typeof reportViewerElement.configuration.events.createdComponent !== 'undefined' && reportViewerElement.configuration.events.createdComponent !== null) {
                                reportViewerElement.configuration.events.createdComponent(reportViewerElement);
                            }
                        } else {
                            if (typeof reportViewerElement.configuration.events !== 'undefined' && reportViewerElement.configuration.events !== null &&
                                typeof reportViewerElement.configuration.events.createComponent !== 'undefined' && reportViewerElement.configuration.events.createComponent !== null) {
                                reportViewerElement.configuration.events.createComponent(reportViewerElement);
                            }
                        }
                    },
                    validateReference: function (reportViewerElement, oItem, finishState) {
                        if (finishState == true) {
                            if (typeof reportViewerElement.configuration.events !== 'undefined' && reportViewerElement.configuration.events !== null &&
                                typeof reportViewerElement.configuration.events.validateReference !== 'undefined' && reportViewerElement.configuration.events.validateReference !== null) {
                                reportViewerElement.configuration.events.validateReference(reportViewerElement, oItem);
                            }
                        } else {
                            if (typeof reportViewerElement.configuration.events !== 'undefined' && reportViewerElement.configuration.events !== null &&
                                typeof reportViewerElement.configuration.events.validatedReference !== 'undefined' && reportViewerElement.configuration.events.validatedReference !== null) {
                                reportViewerElement.configuration.events.validatedReference(reportViewerElement, oItem);
                            }
                        }
                    },
                    dataBound: function (reportViewerElement, finishState) {
                        if (finishState == true) {
                            if (typeof reportViewerElement.configuration.events !== 'undefined' && reportViewerElement.configuration.events !== null &&
                                typeof reportViewerElement.configuration.events.dataBinding !== 'undefined' && reportViewerElement.configuration.events.dataBinding !== null) {
                                reportViewerElement.configuration.events.dataBinding(reportViewerElement);
                            }
                        } else {
                            if (typeof reportViewerElement.configuration.events !== 'undefined' && reportViewerElement.configuration.events !== null &&
                                typeof reportViewerElement.configuration.events.dataBound !== 'undefined' && reportViewerElement.configuration.events.dataBound !== null) {
                                reportViewerElement.configuration.events.dataBound(reportViewerElement);
                            }
                        }
                    }

                }

            }
        });



        $(this).addClass('report-viewer');

        var reportViewerElement = $(this).data(dataReportViewer);

        reportViewerElement.elements.reportViewerContainerElement = this[0];
        $(reportViewerElement.elements.reportViewerContainerElement).html('');

        reportViewerElement.elements.gridContainer = $('<div class="reportviewer-grid-container" style="height:100%"></div>');
        $(reportViewerElement.elements.reportViewerContainerElement).append(reportViewerElement.elements.gridContainer);
        reportViewerElement.elements.toolbar = $('<div class="reportviewer-toolbar"></div>');
        $(reportViewerElement.elements.gridContainer).append(reportViewerElement.elements.toolbar);
        reportViewerElement.elements.grid = $('<div class="reportviewer-grid"></div>');
        $(reportViewerElement.elements.gridContainer).append(reportViewerElement.elements.grid);

        $.post(reportViewerElement.configuration.actionReportInit, { idInstance: reportViewerElement.configuration.idInstance })
            .done(function (response) {
                reportViewerElement.configuration.viewModel = response.Result;
                $.post(reportViewerElement.configuration.viewModel.ActionInitializeViewItemsReport, { idInstance: response.Result.IdInstance })
                    .done(function (response) {
                        reportViewerElement.configuration.viewModel.initViewItems = response.ViewItems;
                        changeValuesFromServerChildren(response.ViewItems, [selectorReportViewer]);
                        if (reportViewerElement.configuration.initCompleteHandler !== undefined && reportViewerElement.configuration.initCompleteHandler !== null) {
                            reportViewerElement.configuration.initCompleteHandler(reportViewerElement);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

        return reportViewerElement;
    };
})(jQuery);

