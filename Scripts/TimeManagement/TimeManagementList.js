﻿(function ($) {
    $.fn.createTimeManagementList = function (configuration) {
        var timeManagementListComponent = {
            configuration: configuration,
            data: {

            },
            elements: {
            },
            components: {
            },
            handler: {
                createComponent: function (timeManagementListComponent) {
                    timeManagementListComponent.elements.gridContainer = $(timeManagementListComponent.templates.grid);
                    $(timeManagementListComponent.elements.timeManagementListContainer).append(timeManagementListComponent.elements.gridContainer);

                    $.post(timeManagementListComponent.configuration.viewModel.ActionGetGridConfig, { idInstance: timeManagementListComponent.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            if (response.IsSuccessful) {
                                var element = getElementsBySelectorPath(response.SelectorPath);
                                var timeManagementListComponent = $(element).data('time-management-list-component');

                                timeManagementListComponent.elements.grid = $(timeManagementListComponent.elements.gridContainer).find('.grid');


                                $.extend(response.Result.dataSource.transport.read, {
                                    component: timeManagementListComponent,
                                    data: function () {
                                        return {
                                            idInstance: this.component.configuration.viewModel.IdInstance,
                                            idGroup: this.component.data.idGroup,
                                            rangeName: this.component.data.selectedFilterName
                                        };
                                    }
                                })
                                if (timeManagementListComponent.data.dataSource === undefined) {
                                    timeManagementListComponent.data.dataSource = new kendo.data.DataSource(response.Result.dataSource);

                                } else {
                                    $(timeManagementListComponent.elements.grid).data("kendoGrid").destroy(); // destroy the Grid

                                    $(timeManagementListComponent.elements.grid).empty(); // empty the Grid content (inner HTML)
                                }

                                response.Result.dataSource = timeManagementListComponent.data.dataSource;

                                response.Result.change = function () {
                                    var timeManagementListComponent = $(this.element[0]).parent().parent().data('time-management-list-component');
                                    var currentDataItem = timeManagementListComponent.components.gridItem.dataItem(this.select().closest('tr'));

                                    if (currentDataItem != undefined) {
                                        console.log(currentDataItem);
                                        var cellIndex = timeManagementListComponent.components.gridItem.cellIndex(this.select());
                                        var column = this.columns[cellIndex];
                                        if (column.field == "NameTimeManagement") {
                                            if (typeof timeManagementListComponent.configuration.events !== 'undefined' && timeManagementListComponent.configuration.events !== null &&
                                                typeof timeManagementListComponent.configuration.events.selectedRow !== 'undefined' && timeManagementListComponent.configuration.events.selectedRow !== null) {
                                                timeManagementListComponent.configuration.events.selectedRow(timeManagementListComponent, currentDataItem);
                                            }
                                            else {
                                                var items = [
                                                    {
                                                        GUID: currentDataItem.IdTimeManagement,
                                                        GUID_Parent: timeManagementListComponent.configuration.viewModel.idClassTimeManagement
                                                    }];

                                                timeManagementListComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                                    ChannelId: timeManagementListComponent.configuration.viewModel.ChannelSelectedObject,
                                                    SenderId: timeManagementListComponent.configuration.managerConfig.idEndpoint,
                                                    SessionId: timeManagementListComponent.configuration.managerConfig.idSession,
                                                    UserId: timeManagementListComponent.configuration.viewModel.IdUser,
                                                    OItems: items
                                                });
                                            }
                                        } else if (column.field == "Start") {
                                            var timeStamp = currentDataItem.Start;
                                            if (typeof timeManagementListComponent.configuration.events !== 'undefined' && timeManagementListComponent.configuration.events !== null &&
                                                typeof timeManagementListComponent.configuration.events.selectedStartOrEnd !== 'undefined' && timeManagementListComponent.configuration.events.selectedStartOrEnd !== null) {
                                                timeManagementListComponent.configuration.events.selectedStartOrEnd(timeManagementListComponent, timeStamp);
                                            }

                                        } else if (column.field == "Ende") {
                                            var timeStamp = currentDataItem.Ende;
                                            if (typeof timeManagementListComponent.configuration.events !== 'undefined' && timeManagementListComponent.configuration.events !== null &&
                                                typeof timeManagementListComponent.configuration.events.selectedStartOrEnd !== 'undefined' && timeManagementListComponent.configuration.events.selectedStartOrEnd !== null) {
                                                timeManagementListComponent.configuration.events.selectedStartOrEnd(timeManagementListComponent, timeStamp);
                                            }
                                        }
                                    }


                                };
                                response.Result.toolbar = kendo.template($(timeManagementListComponent.templates.toolbar).html());

                                $(timeManagementListComponent.elements.grid).kendoGrid(response.Result);
                                //setTimeout(function () {
                                //    dataSource.read();
                                //}, 500);

                                timeManagementListComponent.components.gridItem = $(timeManagementListComponent.elements.grid).data("kendoGrid");
                                timeManagementListComponent.components.gridItem.dataSource.sort({ field: "Start", dir: "desc" });

                                $(timeManagementListComponent.elements.gridContainer).find('.k-button').kendoButton({
                                    click: viewModel.clickedButton
                                });

                                $(timeManagementListComponent.elements.grid).on('cellselect', function (event) {
                                    // event arguments.
                                    var args = event.args;
                                    // column data field.
                                    var datafield = event.args.datafield;
                                    // row's bound index.
                                    var rowBoundIndex = args.rowindex;
                                    // new cell value.
                                    var value = args.newvalue;
                                    // old cell value.
                                    var oldvalue = args.oldvalue;

                                });

                                $(timeManagementListComponent.elements.grid).on('cellvaluechanged', function (event) {
                                    // event arguments.
                                    var args = event.args;
                                    // column data field.
                                    var datafield = event.args.datafield;
                                    // row's bound index.
                                    var rowBoundIndex = args.rowindex;
                                    // new cell value.
                                    var value = args.newvalue;
                                    // old cell value.
                                    var oldvalue = args.oldvalue;

                                });

                                $.post(timeManagementListComponent.configuration.viewModel.ActionGetDropDownConfigGroups, { idClass: timeManagementListComponent.configuration.viewModel.idClassGroup, selectorPath: timeManagementListComponent.configuration.selectorPath })
                                    .done(function (response) {
                                        var selectorPath = response.SelectorPath;
                                        var element = getElementsBySelectorPath(selectorPath);
                                        var timeManagementListComponent = $(element).data('time-management-list-component');
                                        var groupElement = $(timeManagementListComponent.elements.gridContainer).find('.inpGroup');
                                        $(groupElement).data('time-management-list-component', timeManagementListComponent);
                                        var dropDownConfig = response.DropDownConfig;

                                        $.extend(dropDownConfig, {
                                            select: function (e) {
                                                var dataItem = this.dataItem(e.item);
                                                var timeManagementListComponent = $(e.sender.element).data('time-management-list-component');
                                                timeManagementListComponent.data.idGroup = dataItem.Value;
                                                timeManagementListComponent.handler.reloadGrid(timeManagementListComponent);
                                                timeManagementListComponent.handler.triggerEvents.groupChosen(timeManagementListComponent);
                                            },
                                            dataBound: function (e) {
                                                var timeManagementListComponent = $(e.sender.element).data('time-management-list-component');
                                                timeManagementListComponent.components.groupSelect = $(groupElement).data("kendoDropDownList")
                                                timeManagementListComponent.components.groupSelect.enable(true);
                                                var group = urlParameterConfig.getParameterValue("Group");
                                                if (typeof (group) !== 'undefined') {
                                                    timeManagementListComponent.data.idGroup = group;
                                                } else {
                                                    timeManagementListComponent.data.idGroup = timeManagementListComponent.configuration.viewModel.GroupItem.GUID;
                                                }

                                                timeManagementListComponent.components.groupSelect.select(function (dataItem) {
                                                    return dataItem.Value === timeManagementListComponent.data.idGroup;
                                                });
                                                changeValuesFromServerChildren(timeManagementListComponent.configuration.viewModel.ViewItems, timeManagementListComponent.configuration.selectorPath);
                                                timeManagementListComponent.handler.reloadGrid(timeManagementListComponent);
                                                timeManagementListComponent.handler.triggerEvents.createComponent(timeManagementListComponent, 1);
                                                timeManagementListComponent.handler.triggerEvents.dropDownLoaded(timeManagementListComponent);
                                            }
                                        });

                                        $(groupElement).kendoDropDownList(dropDownConfig);
                                    })
                                    .fail(function (jqxhr, textStatus, error) {
                                        // stuff
                                    });

                                var filterElement = $(timeManagementListComponent.elements.gridContainer).find('.inpFilter');
                                $(filterElement).data('time-management-list-component', timeManagementListComponent);
                                jQuery.extend(timeManagementListComponent.configuration.viewModel.dropDownConfig, {
                                    select: function (e) {
                                        var dataItem = this.dataItem(e.item);
                                        var timeManagementListComponent = $(e.sender.element).data('time-management-list-component');
                                        timeManagementListComponent.data.selectedFilterName = dataItem.Value;
                                        timeManagementListComponent.handler.reloadGrid(timeManagementListComponent);
                                    },
                                    dataBound: function (e) {
                                        var timeManagementListComponent = $(e.sender.element).data('time-management-list-component');
                                        timeManagementListComponent.data.selectedFilterName = timeManagementListComponent.configuration.viewModel.SelectedFilter.RangeName;
                                        $($(timeManagementListComponent.elements.gridContainer).find('.inpFilter').find('input')).data('kendoDropDownList').select(function (dataItem) {
                                            return dataItem.Value === timeManagementListComponent.data.selectedFilterName;
                                        });
                                    }
                                });

                                $(filterElement).kendoDropDownList(timeManagementListComponent.configuration.viewModel.dropDownConfig);
                                timeManagementListComponent.components.filterSelect = $(filterElement).data('kendoDropDownList');

                                timeManagementListComponent.handler.triggerEvents.createComponent(timeManagementListComponent, 1);
                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                    
                },
                validateReference: function (timeManagementListComponent, oItem) {
                    timeManagementListComponent.handler.triggerEvents.validateReference(timeManagementListComponent, oItem, false);

                    timeManagementListComponent.handler.triggerEvents.validateReference(timeManagementListComponent, oItem, true);
                },
                addOrUpdateRow: function (timeManagementListComponent, timeManagementEntry) {
                    var dataItem = null;
                    var dataItems = timeManagementListComponent.components.gridItem.dataSource.data();
                    var dataItemsSearch = $.grep(dataItems, function (dataItem, ixDataItem) {
                        return dataItem.IdTimeManagement === timeManagementEntry.IdTimeManagement;
                    });
                    if (dataItemsSearch.length > 0) {
                        dataItem = dataItemsSearch[0];
                    }

                    if (dataItem !== null) {
                        dataItem.set("NameTimeManagement", timeManagementEntry.NameTimeManagement);
                        dataItem.set("IdLogState", timeManagementEntry.IdLogState);
                        dataItem.set("NameLogState", timeManagementEntry.NameLogState);
                        dataItem.set("Start", timeManagementEntry.Start);
                        dataItem.set("Ende", timeManagementEntry.Ende);
                        dataItem.set("WeekDayStart", timeManagementEntry.WeekDayStart);
                        dataItem.set("WeekDayEnd", timeManagementEntry.WeekDayEnd);
                        dataItem.set("DurationHours", timeManagementEntry.DurationHours);
                        dataItem.set("DurationMinutes", timeManagementEntry.DurationMinutes);
                        dataItem.set("DurationHoursWeek", timeManagementEntry.DurationHoursWeek);
                        dataItem.set("DurationMinutesWeek", timeManagementEntry.DurationMinutesWeek);
                        dataItem.set("ToDoHoursDay", timeManagementEntry.ToDoHoursDay);
                        dataItem.set("ToDoMinutesDay", timeManagementEntry.ToDoMinutesDay);
                        dataItem.set("ToDoHoursWeek", timeManagementEntry.ToDoHoursWeek);
                        dataItem.set("ToDoMinutesWeek", timeManagementEntry.ToDoMinutesWeek);
                        dataItem.set("ToDoEnd", timeManagementEntry.ToDoEnd);
                        dataItem.set("IdReference", timeManagementEntry.IdReference);
                        dataItem.set("NameReference", timeManagementEntry.NameReference);
                        dataItem.set("IdParentReference", timeManagementEntry.IdParentReference);
                        dataItem.set("NameParentReference", timeManagementEntry.NameParentReference);
                        dataItem.set("IdGroup", timeManagementEntry.IdGroup);
                        dataItem.set("NameGroup", timeManagementEntry.NameGroup);
                        dataItem.set("IdUser", timeManagementEntry.IdUser);
                        dataItem.set("NameUser", timeManagementEntry.NameUser);
                        dataItem.set("YearStart", timeManagementEntry.YearStart);
                        dataItem.set("MonthStart", timeManagementEntry.MonthStart);
                        dataItem.set("DayStart", timeManagementEntry.DayStart);
                        dataItem.set("WeekStart", timeManagementEntry.WeekStart);
                        dataItem.set("YearEnd", timeManagementEntry.YearEnd);
                        dataItem.set("MonthEnd", timeManagementEntry.MonthEnd);
                        dataItem.set("DayEnd", timeManagementEntry.DayEnd);
                        dataItem.set("WeekEnd", timeManagementEntry.WeekEnd);
                        dataItem.set("StartSeq", timeManagementEntry.StartSeq);
                        dataItem.set("EndSeq", timeManagementEntry.EndSeq);

                        timeManagementListComponent.components.gridItem.dataSource.read();
                    } else {
                        timeManagementListComponent.components.gridItem.dataSource.add(timeManagementEntry)
                        timeManagementListComponent.components.gridItem.dataSource.read();
                    }
                },
                triggerEvents: {
                    createComponent: function (timeManagementListComponent, finishState) {
                        if (finishState == true) {
                            if (typeof timeManagementListComponent.configuration.events !== 'undefined' && timeManagementListComponent.configuration.events !== null &&
                                typeof timeManagementListComponent.configuration.events.createdComponent !== 'undefined' && timeManagementListComponent.configuration.events.createdComponent !== null) {
                                timeManagementListComponent.configuration.events.createdComponent(timeManagementListComponent);
                            }
                        } else {
                            if (typeof timeManagementListComponent.configuration.events !== 'undefined' && timeManagementListComponent.configuration.events !== null &&
                                typeof timeManagementListComponent.configuration.events.createComponent !== 'undefined' && timeManagementListComponent.configuration.events.createComponent !== null) {
                                timeManagementListComponent.configuration.events.createComponent(timeManagementListComponent);
                            }
                        }
                    },
                    validateReference: function (timeManagementListComponent, oItem, finishState) {
                        if (finishState == true) {
                            if (typeof timeManagementListComponent.configuration.events !== 'undefined' && timeManagementListComponent.configuration.events !== null &&
                                typeof timeManagementListComponent.configuration.events.validateReference !== 'undefined' && timeManagementListComponent.configuration.events.validateReference !== null) {
                                timeManagementListComponent.configuration.events.validateReference(timeManagementListComponent, oItem);
                            }
                        } else {
                            if (typeof timeManagementListComponent.configuration.events !== 'undefined' && timeManagementListComponent.configuration.events !== null &&
                                typeof timeManagementListComponent.configuration.events.validatedReference !== 'undefined' && timeManagementListComponent.configuration.events.validatedReference !== null) {
                                timeManagementListComponent.configuration.events.validatedReference(timeManagementListComponent, oItem);
                            }
                        }
                    },
                    dropDownLoaded: function (timeManagementListComponent, finishState) {
                        if (finishState == true) {
                            if (typeof timeManagementListComponent.configuration.events !== 'undefined' && timeManagementListComponent.configuration.events !== null &&
                                typeof timeManagementListComponent.configuration.events.dropDownLoaded !== 'undefined' && timeManagementListComponent.configuration.events.dropDownLoaded !== null) {
                                timeManagementListComponent.configuration.events.dropDownLoaded(timeManagementListComponent);
                            }
                        } else {
                            if (typeof timeManagementListComponent.configuration.events !== 'undefined' && timeManagementListComponent.configuration.events !== null &&
                                typeof timeManagementListComponent.configuration.events.dropDownLoading !== 'undefined' && timeManagementListComponent.configuration.events.dropDownLoading !== null) {
                                timeManagementListComponent.configuration.events.dropDownLoading(timeManagementListComponent);
                            }
                        }
                        
                    },
                    groupChosen: function (timeManagementListComponent) {
                        if (typeof timeManagementListComponent.configuration.events !== 'undefined' && timeManagementListComponent.configuration.events !== null &&
                            typeof timeManagementListComponent.configuration.events.groupChosen !== 'undefined' && timeManagementListComponent.configuration.events.groupChosen !== null) {
                            timeManagementListComponent.configuration.events.groupChosen(timeManagementListComponent, timeManagementListComponent.data.idGroup);
                        }
                    }

                },
                reloadGrid: function (timeManagementListComponent) {
                    timeManagementListComponent.components.gridItem.dataSource.read();
                }
            },
            templates: {
                grid: `
                    <div class="gridParent">
                        <div class="grid"></div>
                    </div>
                `,
                toolbar: `
                    <div>
                        <div class="toolbar" class="toolbar">
                            <label class="lblGroup">Group:</label>
                            <input class="inpGroup" />
                            <lable class="lblFilter">Filter:</label>
                            <input class="inpFilter" />
                        </div>
                    </div>
                `
            }
        }

        timeManagementListComponent.elements.timeManagementListContainer = this;
        $(timeManagementListComponent.elements.timeManagementListContainer).addClass('time-management-list');
        $(timeManagementListComponent.elements.timeManagementListContainer).data('time-management-list-component', timeManagementListComponent);

        $.post(timeManagementListComponent.configuration.actionInit, { idInstance: timeManagementListComponent.configuration.IdInstance, selectorPath: timeManagementListComponent.configuration.selectorPath })
            .done(function (response) {
                // stuff
                console.log(response);
                if (response.IsSuccessful) {
                    var selectorPath = response.SelectorPath;
                    var element = getElementsBySelectorPath(selectorPath);
                    var timeManagementListComponent = $(element).data('time-management-list-component');
                    timeManagementListComponent.handler.triggerEvents.createComponent(timeManagementListComponent, 1);
                    timeManagementListComponent.configuration.viewModel = response.Result;
                    timeManagementListComponent.handler.createComponent(timeManagementListComponent);
                } else {

                }

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    }
}) (jQuery);