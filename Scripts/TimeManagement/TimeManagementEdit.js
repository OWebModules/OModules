﻿function applyItem(element) {
    var gridItem = $(element).closest('.k-grid').data('kendoGrid');
    var timeManagementEntryComponent = $(element).closest('.time-management-entry').data('time-management-component');
    var row = $(element).closest('tr');
    var dataItem = gridItem.dataItem(row);
    var idReference = dataItem.IdReference;
    if (typeof (idReference) === 'undefined' || idReference == null) {
        return;
    }

    var oItem = {
        GUID: idReference,
        Name: dataItem.NameReference
    };

    timeManagementEntryComponent.handler.appliedObjects(timeManagementEntryComponent, [oItem]);
}
(function ($) {
    $.fn.createTimeManagementEntry = function (configuration) {
        var timeManagementEntryComponent = {
            configuration: configuration,
            data: {
                setTimeManagementElements: function (timeManagementEntryComponent) {
                    var timeManagementEntry = timeManagementEntryComponent.data.timeManagementEntry;
                    timeManagementEntryComponent.elements.timeManagementEntryContainer.find('.inpName').val(timeManagementEntry.NameTimeManagement);
                    timeManagementEntryComponent.elements.timeManagementEntryContainer.find('.inpStart').find('input').data('kendoDateTimePicker').value(timeManagementEntry.Start);
                    timeManagementEntryComponent.elements.timeManagementEntryContainer.find('.inpEnde').find('input').data('kendoDateTimePicker').value(timeManagementEntry.Ende);
                    var radio = timeManagementEntryComponent.elements.timeManagementEntryContainer.find(`[data-id='${timeManagementEntry.IdLogState}']`);
                    $(radio).attr('checked', true);
                    timeManagementEntryComponent.elements.timeManagementEntryContainer.find('.inpRelated').val(timeManagementEntry.NameReference);
                    referenceItem = null;
                    if (typeof (timeManagementEntry.IdReference) !== 'undefined' && timeManagementEntry.IdReference !== null) {
                        referenceItem = {
                            GUID: timeManagementEntry.IdReference,
                            Name: timeManagementEntry.NameReference,
                            GUID_Parent: timeManagementEntry.IdParentReference,
                            Name_Parent: timeManagementEntry.NameParentReference
                        }
                        timeManagementEntryComponent.elements.timeManagementEntryContainer.find('.inpRelated').data('referenceItem', referenceItem);
                    }
                },
                getRadioChecked: function (timeManagementEntryComponent, radioClass) {
                    var radioElement = jQuery.grep($(timeManagementEntryComponent.elements.form).find('.state-options input'), function (elem, ixElem) {
                        return $(elem).hasClass(radioClass);
                    });
                    var checked = false;
                    if (radioElement.length > 0) {
                        checked = radioElement[0].checked;
                    }
                    return checked;
                },
                changeValue: function (timeManagementEntryComponent, viewItem, viewItemType, value) {
                    changeValueWithSelectorPath(timeManagementEntryComponent.configuration.selectorPath,
                        viewItem,
                        viewItemType,
                        value,
                        true,
                        function (response) {
                            var selectorPath = response.SelectorPath;
                            var element = getElementsBySelectorPath(selectorPath);
                            var timeManagementEntryComponent = $(element).data('time-management-component');
                            changeValuesFromServerChildren(response.ViewItems, timeManagementEntryComponent.configuration.selectorPath);
                            $(element).find('.buttonListenRelated').closePageLoader();
                        },
                        timeManagementEntryComponent.configuration.viewModel.IdInstance);

                },
                isListenForRelateion: function (timeManagementEntryComponent) {
                    var buttonToCheck = $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.buttonListenRelated');
                    return $(buttonToCheck).isToggled();
                },
                pageLoaderElements: []
            },
            elements: {
            },
            components: {
            },
            handler: {
                createComponent: function (timeManagementEntryComponent) {
                    timeManagementEntryComponent.elements.form = $(timeManagementEntryComponent.templates.form);

                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).append(timeManagementEntryComponent.elements.form);

                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.buttonListenRelated').on("mouseenter", function (e) {
                        var element = $(this);
                        var timeManagementEntryComponent = $(element).data('time-management-component');
                        if (timeManagementEntryComponent.data.isListenForRelateion(timeManagementEntryComponent)) return;
                        if (event.ctrlKey) {
                            $(element).html('<i class="fa fa-assistive-listening-systems" aria-hidden="true"></i>');
                            timeManagementEntryComponent.data.listenOrOpen = true;
                        } else {
                            $(element).html('<i class="fa fa-folder-open-o" aria-hidden="true"></i>');
                            timeManagementEntryComponent.data.listenOrOpen = false;
                        }
                    });
                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.buttonListenRelated').on("mouseleave", function (e) {
                        var element = $(this);
                        var timeManagementEntryComponent = $(element).data('time-management-component');
                        if (timeManagementEntryComponent.data.isListenForRelateion(timeManagementEntryComponent)) return;
                        $(element).html('<i class="fa fa-folder-open-o" aria-hidden="true"></i>');
                        timeManagementEntryComponent.data.listenOrOpen = false;
                    });
                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.k-button').data('time-management-component', timeManagementEntryComponent);

                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.k-button').kendoButton({
                        click: function (e) {
                            var timeManagementEntryComponent = $(e.sender.element).data('time-management-component');
                            if ($(e.sender.element).hasClass('buttonListenRelated')) {
                                if (timeManagementEntryComponent.data.listenOrOpen) {
                                    timeManagementEntryComponent.handler.toggleListenReference(e.sender.element);
                                }
                                else {
                                    timeManagementEntryComponent.components.classObjectSelector.open();
                                }
                                
                            }
                            else if ($(e.sender.element).hasClass('buttonSave')) {
                                $(e.sender.element).attr('readonly', true);
                                $('body').openPageLoader();
                                $.post(timeManagementEntryComponent.configuration.viewModel.ActionSaveTimeManagementEntry, { idInstance: timeManagementEntryComponent.configuration.IdInstance })
                                    .done(function (response) {
                                        // stuff
                                        console.log(response);
                                        if (response.IsSuccessful) {
                                            var selectorPath = response.SelectorPath;
                                            var element = getElementsBySelectorPath(selectorPath);
                                            var timeManagementEntryComponent = $(element).data('time-management-component');
                                            changeValuesFromServerChildren(response.ViewItems, timeManagementEntryComponent.configuration.selectorPath);


                                            var timeManagementListComponent = $(timeManagementEntryComponent.elements.list).data('time-management-list-component');
                                            timeManagementListComponent.handler.addOrUpdateRow(timeManagementListComponent, response.Result);
                                            timeManagementEntryComponent.handler.validateReference(timeManagementEntryComponent);
                                        } else {

                                        }
                                        $('body').closePageLoader();

                                    })
                                    .fail(function (jqxhr, textStatus, error) {
                                        // stuff
                                    });
                            }
                            else if ($(e.sender.element).hasClass('buttonRemoveRelated')) {
                                //timeManagementEntryComponent.data.changeValue(timeManagementEntryComponent,
                                //    timeManagementEntryComponent.configuration.viewModel.inpRelated,
                                //    "Content",
                                //    null
                                //);
                                timeManagementEntryComponent.data.changeValue(timeManagementEntryComponent,
                                    timeManagementEntryComponent.configuration.viewModel.inpRelated,
                                    "Id",
                                    null
                                );
                                var buttonRelated = $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.buttonListenRelated');
                                timeManagementEntryComponent.handler.toggleListenReference(buttonRelated);
                            }
                            else if ($(e.sender.element).hasClass('buttonNew')) {
                                timeManagementEntryComponent.handler.validateReference(timeManagementEntryComponent)
                            }
                            else if ($(e.sender.element).hasClass('buttonListenStart')) {
                                $(e.sender.element).toggleMode(true, true);
                                var isChecked = $(e.sender.element).isToggled();
                                if (isChecked) {
                                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.buttonListenEnde').setCheckedMode(false, true);
                                }
                                
                            }
                            else if ($(e.sender.element).hasClass('buttonListenEnde')) {
                                $(e.sender.element).toggleMode(true, true);
                                var isChecked = $(e.sender.element).isToggled();
                                if (isChecked) {
                                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.buttonListenStart').setCheckedMode(!isChecked, true);
                                }
                                
                            }
                        }
                    });

                    var groupElement = $(timeManagementEntryComponent.elements.form).find('.inpGroup');

                    var radioViewItems = jQuery.grep(timeManagementEntryComponent.configuration.viewModel.ViewItems, function (viewItem, ixViewItem) {
                        return (viewItem.ViewItemClass === 'Radio');
                    });
                    radioViewItems.forEach(item => {
                        var id = getViewItemValue(item, "Id");
                        var caption = getViewItemValue(item, "Caption");
                        var classItem = item.ViewItemId;
                        var optionHtml = timeManagementEntryComponent.templates.stateOption.replace("@ID@", id);
                        optionHtml = optionHtml.replace("@NAME@", caption);
                        optionHtml = optionHtml.replace("@CLASS@", classItem);
                        var optionElement = $(optionHtml)
                        $(optionElement).find('input').data('ViewItem', item);
                        $(optionElement).find('input').data('time-management-component', timeManagementEntryComponent);
                        $(timeManagementEntryComponent.elements.form).find('.state-options').append(optionElement);
                    });
                    
                    $(groupElement).data('time-management-component', timeManagementEntryComponent);
                    

                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.inpStart').kendoDateTimePicker({
                        value: (new Date()),
                        timeFormat: "HH:mm",
                        dateInput: true,
                        change: function () {
                            var element = $($(this)[0].element);
                            var timeManagementEntryComponent = $(element).data('time-management-component');
                            var start = new Date(timeManagementEntryComponent.elements.timeManagementEntryContainer.find('.inpStart').find('input').data('kendoDateTimePicker').value()).toJSON();

                            timeManagementEntryComponent.data.changeValue(timeManagementEntryComponent,
                                timeManagementEntryComponent.configuration.viewModel.inpStart,
                                "Content",
                                start
                            );
                            
                        }
                    });

                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.inpName').data('time-management-component', timeManagementEntryComponent);
                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.inpStart').data('time-management-component', timeManagementEntryComponent);

                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.inpEnde').kendoDateTimePicker({
                        value: (new Date()),
                        timeFormat: "HH:mm",
                        dateInput: true,
                        change: function () {
                            var element = $($(this)[0].element);
                            var timeManagementEntryComponent = $(element).data('time-management-component');
                            var ende = new Date(timeManagementEntryComponent.elements.timeManagementEntryContainer.find('.inpEnde').find('input').data('kendoDateTimePicker').value()).toJSON();

                            timeManagementEntryComponent.data.changeValue(timeManagementEntryComponent,
                                timeManagementEntryComponent.configuration.viewModel.inpEnde,
                                "Content",
                                ende
                            );
                        }
                    });

                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.inpEnde').data('time-management-component', timeManagementEntryComponent);

                    $.post(timeManagementEntryComponent.configuration.viewModel.ActionGetDropDownConfigGroups, { idClass: timeManagementEntryComponent.configuration.viewModel.idClassGroup, selectorPath: timeManagementEntryComponent.configuration.selectorPath })
                        .done(function (response) {
                            var selectorPath = response.SelectorPath;
                            var element = getElementsBySelectorPath(selectorPath);
                            var timeManagementEntryComponent = $(element).data('time-management-component');
                            var groupElement = $(timeManagementEntryComponent.elements.form).find('.inpGroup');

                            var dropDownConfig = response.DropDownConfig;
                            
                            jQuery.extend(dropDownConfig, {
                                filter: "contains",
                                suggest: true
                            });

                            $.extend(dropDownConfig, {
                                select: function (e) {
                                    var dataItem = this.dataItem(e.item);
                                    var timeManagementEntryComponent = $(e.sender.element).data('time-management-component');
                                    timeManagementEntryComponent.data.idGroup = dataItem.Value;
                                    timeManagementEntryComponent.data.changeValue(timeManagementEntryComponent,
                                        timeManagementEntryComponent.configuration.viewModel.inpGroup,
                                        "Id",
                                        timeManagementEntryComponent.data.idGroup
                                    );
                                    timeManagementEntryComponent.handler.triggerEvents.groupChosen(timeManagementEntryComponent);
                                    
                                },
                                dataBound: function (e) {
                                    var timeManagementEntryComponent = $(e.sender.element).data('time-management-component');
                                    var groupElement = $(timeManagementEntryComponent.elements.form).find('.inpGroup').find('input');
                                    timeManagementEntryComponent.components.groupSelect = $(groupElement).data("kendoDropDownList")
                                    timeManagementEntryComponent.components.groupSelect.enable(true);
                                    var group = urlParameterConfig.getParameterValue("Group");
                                    if (typeof (group) !== 'undefined') {
                                        timeManagementEntryComponent.data.changeValue(timeManagementEntryComponent,
                                            timeManagementEntryComponent.configuration.viewModel.inpGroup,
                                            "Id",
                                            group
                                        );
                                        timeManagementEntryComponent.data.idGroup = group;
                                    } else {
                                        timeManagementEntryComponent.data.idGroup = timeManagementEntryComponent.configuration.viewModel.GroupItem.GUID;
                                    }
                                    
                                    timeManagementEntryComponent.components.groupSelect.select(function (dataItem) {
                                        return dataItem.Value === timeManagementEntryComponent.data.idGroup;
                                    });
                                    changeValuesFromServerChildren(timeManagementEntryComponent.configuration.viewModel.ViewItems, timeManagementEntryComponent.configuration.selectorPath);
                                    timeManagementEntryComponent.handler.triggerEvents.createComponent(timeManagementEntryComponent, 1);
                                    timeManagementEntryComponent.handler.triggerEvents.dropDownLoaded(timeManagementEntryComponent);
                                }
                            });

                            $(groupElement).kendoDropDownList(dropDownConfig);
                            
                            
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });

                    timeManagementEntryComponent.elements.classObjectSelectorWindow = $(timeManagementEntryComponent.templates.classObjectSelectorWindowTemplate);
                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).append(timeManagementEntryComponent.elements.classObjectSelectorWindow);
                    $(timeManagementEntryComponent.elements.classObjectSelectorWindow).kendoWindow({
                        width: '1024px',
                        height: '600px',
                        title: 'New Item',
                        visible: false,
                        modal: true,
                        timeManagementEntryComponent: timeManagementEntryComponent,
                        actions: [
                            "Pin",
                            "Minimize",
                            "Maximize",
                            "Close"
                        ]
                    }).data("kendoWindow").center();

                    timeManagementEntryComponent.elements.classObjectSelector = $(timeManagementEntryComponent.templates.classObjectSelectorTemplate);
                    $(timeManagementEntryComponent.elements.classObjectSelectorWindow).find('.class-object-selector-window').append(timeManagementEntryComponent.elements.classObjectSelector);

                    timeManagementEntryComponent.components.classObjectSelector = $(timeManagementEntryComponent.elements.classObjectSelectorWindow).data('kendoWindow');
                    $(timeManagementEntryComponent.elements.classObjectSelector).find('.class-tree-container').data('time-management-component', timeManagementEntryComponent);
                    var selectorPath = ['.class-object-selector-window', '.class-tree-container']
                    var configuration = {
                        setDocumentTitle: false,
                        actionInit: viewModel.ActionInitClassTree,
                        selectorPath: selectorPath,
                        managerConfig: viewModel.managerConfig,
                        events: {
                            selectedClass: function (idClass, nameClass, classTreeComponent) {
                                var element = getElementsBySelectorPath(classTreeComponent.configuration.viewModel.SelectorPath);
                                var timeManagementEntryComponent = $(element).data('time-management-component');
                                timeManagementEntryComponent.components.objectListComponent.handler.setIdParent(idClass);
                            },
                            createdComponent: function (classTreeComponent) {
                                var element = getElementsBySelectorPath(classTreeComponent.configuration.viewModel.SelectorPath);
                                var timeManagementEntryComponent = $(element).data('time-management-component');
                                timeManagementEntryComponent.components.classTreeComponent = classTreeComponent;
                            }
                        }
                    }
                    $('.omodules-splitter').find('.class-tree-container').createClassTree(configuration);

                    $(".omodules-splitter")
                        .kendoSplitter({
                            orientation: "horizontal",
                            panes: [
                                { size: "25%" },
                                { collapsible: false }
                            ]
                        });

                    var configurationObjectList = {
                        parentComponent: timeManagementEntryComponent,
                        actionObjectListInit: viewModel.ActionObjectListLinit,
                        templateSelector: "#templateObjectList",
                        managerConfig: viewModel.managerConfig,
                        useIdParent: true,
                        handler: {
                            initCompleteHandler: function (objectListComponent, parentComponent) {
                                parentComponent.components.objectListComponent = objectListComponent;
                            }
                        },
                        events: {
                            selectedObject: function (items, parentComponent) {

                            },
                            applyItems: function (items, parentComponents) {
                                parentComponents.components.classObjectSelector.close();
                                parentComponents.handler.appliedObjects(parentComponents, items);
                            }
                        }
                    };

                    $('.object-list-container').createObjectList(configurationObjectList);

                    timeManagementEntryComponent.elements.timeManagementEntryContainer.find('.inpName').on('change', function (e) {
                        var timeManagementEntryComponent = $(event.target).data('time-management-component');
                        var name = timeManagementEntryComponent.elements.timeManagementEntryContainer.find('.inpName').val();

                        timeManagementEntryComponent.data.changeValue(timeManagementEntryComponent,
                            timeManagementEntryComponent.configuration.viewModel.inpName,
                            "Content",
                            name
                        );
                    });

                    $(timeManagementEntryComponent.elements.form).find('.state-options input').on('change', function (e) {
                        var timeManagementEntryComponent = $(e.currentTarget).data('time-management-component');
                        var workChecked = timeManagementEntryComponent.data.getRadioChecked(timeManagementEntryComponent, "radioWork");
                        var privateChecked = timeManagementEntryComponent.data.getRadioChecked(timeManagementEntryComponent, "radioPrivate");
                        var urlaubChecked = timeManagementEntryComponent.data.getRadioChecked(timeManagementEntryComponent, "radioUrlaub");
                        var krankheitChecked = timeManagementEntryComponent.data.getRadioChecked(timeManagementEntryComponent, "radioKrankheit");

                        timeManagementEntryComponent.data.changeValue(timeManagementEntryComponent,
                            timeManagementEntryComponent.configuration.viewModel.radioWork,
                            "Checked",
                            workChecked
                        );
                        timeManagementEntryComponent.data.changeValue(timeManagementEntryComponent,
                            timeManagementEntryComponent.configuration.viewModel.radioPrivate,
                            "Checked",
                            privateChecked
                        );
                        timeManagementEntryComponent.data.changeValue(timeManagementEntryComponent,
                            timeManagementEntryComponent.configuration.viewModel.radioUrlaub,
                            "Checked",
                            urlaubChecked
                        );
                        timeManagementEntryComponent.data.changeValue(timeManagementEntryComponent,
                            timeManagementEntryComponent.configuration.viewModel.radioKrankheit,
                            "Checked",
                            krankheitChecked
                        );
                    });

                    timeManagementEntryComponent.elements.list = $(timeManagementEntryComponent.templates.listTemplate);
                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).append(timeManagementEntryComponent.elements.list);

                    var selectorPathList = [];
                    timeManagementEntryComponent.configuration.selectorPath.forEach(sP => {
                        selectorPathList.push(sP);
                    });
                    selectorPathList.push(".entry-list")
                    var configurationList = {
                        setDocumentTitle: false,
                        actionInit: timeManagementEntryComponent.configuration.viewModel.ActionTimeManagementListInit,
                        selectorPath: selectorPathList,
                        managerConfig: timeManagementEntryComponent.configuration.viewModel.managerConfig,
                        events: {
                            createdComponent: function (timeManagementEntryList) {
                                
                            },
                            selectedRow: function (timeManagementListComponent, dataItem) {
                                var oItem = {
                                    GUID: dataItem.IdTimeManagement,
                                    GUID_Parent: timeManagementListComponent.configuration.viewModel.idClassTimeManagement
                                };
                                var timeManagementEntryComponent = timeManagementListComponent.elements.timeManagementListContainer.parent().data('time-management-component');
                                timeManagementEntryComponent.handler.validateReference(timeManagementEntryComponent, oItem);
                                timeManagementEntryComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                    ChannelId: timeManagementEntryComponent.configuration.viewModel.ChannelSelectedObject,
                                    SenderId: timeManagementEntryComponent.configuration.managerConfig.idEndpoint,
                                    SessionId: timeManagementEntryComponent.configuration.managerConfig.idSession,
                                    UserId: timeManagementEntryComponent.configuration.viewModel.IdUser,
                                    OItems: [oItem]
                                });
                            },
                            selectedStartOrEnd: function (timeManagementListComponent, value) {
                                value = value.toJSON();
                                var timeManagementEntryComponent = timeManagementListComponent.elements.timeManagementListContainer.parent().data('time-management-component');
                                var viewItemInput = null;
                                var viewItemButton = null;
                                
                                var isToggledStart = $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.buttonListenStart').isToggled();
                                var isToggledEnde = $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.buttonListenEnde').isToggled();
                                if (isToggledStart) {
                                    viewItemInput = timeManagementEntryComponent.configuration.viewModel.inpStart
                                    viewItemButton = timeManagementEntryComponent.configuration.viewModel.buttonListenStart
                                } else {
                                    viewItemInput = timeManagementEntryComponent.configuration.viewModel.inpEnde
                                    viewItemButton = timeManagementEntryComponent.configuration.viewModel.buttonListenEnde
                                }
                                if (isToggledStart || isToggledEnde) {
                                    changeValue(viewItemInput, "Content", value, true, function (result) {
                                        var element = getElementsBySelectorPath(result.SelectorPath);
                                        var timeManagementEntryComponent = $(element).data('time-management-component');
                                        changeValuesFromServerChildren(result.ViewItems, timeManagementEntryComponent.configuration.selectorPath);
                                    }, timeManagementEntryComponent.configuration.viewModel.IdInstance);
                                    changeValue(viewItemButton, "Checked", false, true, function (result) {
                                        var element = getElementsBySelectorPath(result.SelectorPath);
                                        var timeManagementEntryComponent = $(element).data('time-management-component');
                                        changeValuesFromServerChildren(result.ViewItems, timeManagementEntryComponent.configuration.selectorPath);
                                    }, timeManagementEntryComponent.configuration.viewModel.IdInstance);
                                }
                                
                            }
                        }
                    }
                    $(timeManagementEntryComponent.elements.list).createTimeManagementList(configurationList);
                },
                validateReference: function (timeManagementEntryComponent, oItem) {
                    $('body').openPageLoader();
                    timeManagementEntryComponent.handler.triggerEvents.validateReference(timeManagementEntryComponent, oItem, false);
                    $.post(timeManagementEntryComponent.configuration.viewModel.ActionValidateReference, { idInstance: timeManagementEntryComponent.configuration.viewModel.IdInstance, refItem: oItem })
                        .done(function (response) {
                            // stuff
                            console.log(response);
                            if (response.IsSuccessful) {
                                var selectorPath = response.SelectorPath;
                                var element = getElementsBySelectorPath(selectorPath);
                                var timeManagementEntryComponent = $(element).data('time-management-component');

                                changeValuesFromServerChildren(response.ViewItems, timeManagementEntryComponent.configuration.selectorPath);
                                //timeManagementEntryComponent.data.timeManagementEntry = response.Result;
                                //timeManagementEntryComponent.data.setTimeManagementElements(timeManagementEntryComponent);

                                timeManagementEntryComponent.handler.triggerEvents.validateReference(timeManagementEntryComponent, oItem, true);
                                $('body').closePageLoader();
                            } else {

                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });

                    
                },
                appliedObjects: function (timeManagementEntryComponent, oItems) {
                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.buttonListenRelated').toggleMode(true, false);
                    $($(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.buttonListenRelated')).html('<i class="fa fa-folder-open-o" aria-hidden="true"></i>');
                    $(timeManagementEntryComponent.elements.timeManagementEntryContainer).find('.buttonListenRelated').openPageLoader();
                    
                    if (oItems.length != 1) {
                        return;
                    }

                    var oItem = oItems[0];

                    timeManagementEntryComponent.data.changeValue(timeManagementEntryComponent,
                        timeManagementEntryComponent.configuration.viewModel.inpRelated,
                        "Id",
                        oItem.GUID
                    );

                },
                triggerEvents: {
                    createComponent: function (timeManagementEntryComponent, finishState) {
                        if (finishState == true) {
                            if (typeof timeManagementEntryComponent.configuration.events !== 'undefined' && timeManagementEntryComponent.configuration.events !== null &&
                                typeof timeManagementEntryComponent.configuration.events.createdComponent !== 'undefined' && timeManagementEntryComponent.configuration.events.createdComponent !== null) {
                                timeManagementEntryComponent.configuration.events.createdComponent(timeManagementEntryComponent);
                            }
                        } else {
                            if (typeof timeManagementEntryComponent.configuration.events !== 'undefined' && timeManagementEntryComponent.configuration.events !== null &&
                                typeof timeManagementEntryComponent.configuration.events.createComponent !== 'undefined' && timeManagementEntryComponent.configuration.events.createComponent !== null) {
                                timeManagementEntryComponent.configuration.events.createComponent(timeManagementEntryComponent);
                            }
                        }
                    },
                    validateReference: function (timeManagementEntryComponent, oItem, finishState) {
                        if (finishState == true) {
                            if (typeof timeManagementEntryComponent.configuration.events !== 'undefined' && timeManagementEntryComponent.configuration.events !== null &&
                                typeof timeManagementEntryComponent.configuration.events.validateReference !== 'undefined' && timeManagementEntryComponent.configuration.events.validateReference !== null) {
                                timeManagementEntryComponent.configuration.events.validateReference(timeManagementEntryComponent, oItem);
                            }
                        } else {
                            if (typeof timeManagementEntryComponent.configuration.events !== 'undefined' && timeManagementEntryComponent.configuration.events !== null &&
                                typeof timeManagementEntryComponent.configuration.events.validatedReference !== 'undefined' && timeManagementEntryComponent.configuration.events.validatedReference !== null) {
                                timeManagementEntryComponent.configuration.events.validatedReference(timeManagementEntryComponent, oItem);
                            }
                        }
                    },
                    dropDownLoaded: function (timeManagementEntryComponent, finishState) {
                        if (finishState == true) {
                            if (typeof timeManagementEntryComponent.configuration.events !== 'undefined' && timeManagementEntryComponent.configuration.events !== null &&
                                typeof timeManagementEntryComponent.configuration.events.dropDownLoaded !== 'undefined' && timeManagementEntryComponent.configuration.events.dropDownLoaded !== null) {
                                timeManagementEntryComponent.configuration.events.dropDownLoaded(timeManagementEntryComponent);
                            }
                        } else {
                            if (typeof timeManagementEntryComponent.configuration.events !== 'undefined' && timeManagementEntryComponent.configuration.events !== null &&
                                typeof timeManagementEntryComponent.configuration.events.dropDownLoading !== 'undefined' && timeManagementEntryComponent.configuration.events.dropDownLoading !== null) {
                                timeManagementEntryComponent.configuration.events.dropDownLoading(timeManagementEntryComponent);
                            }
                        }
                        
                    },
                    groupChosen: function (timeManagementEntryComponent) {
                        if (typeof timeManagementEntryComponent.configuration.events !== 'undefined' && timeManagementEntryComponent.configuration.events !== null &&
                            typeof timeManagementEntryComponent.configuration.events.groupChosen !== 'undefined' && timeManagementEntryComponent.configuration.events.groupChosen !== null) {
                            timeManagementEntryComponent.configuration.events.groupChosen(timeManagementEntryComponent, timeManagementEntryComponent.data.idGroup);
                        }
                    }

                },
                toggleListenReference: function (element) {
                    var timeManagementEntryComponent = $(element).data('time-management-component');
                    if (timeManagementEntryComponent.data.listenOrOpen) {
                        $(element).toggleMode(true);
                    }
                },
                addElementToPlaceholder: function (element, timeManagementEntryComponent) {
                    if (typeof (timeManagementEntryComponent.elements.placeholder1) !== 'undefined') return false;
                    $(timeManagementEntryComponent.elements.form).find('.place-holder1').append($(element));
                }
            },
            templates: {
                form: `<div class="form-horizontal">
                            <fieldset>
                                    <div class="form-group">
                                        <label class="lblGroup col-sm-2 control-label">Group:</label>
                                        <div class="col-sm-9">
                                            <input class="inpGroup form-control" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <label class="lblName control-label col-sm-2">Text:</label>
                                        <div class="col-sm-9">
                                            <input class="inpName form-control" type="text" value="">

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2" >
                                            <button class="buttonListenStart k-button btn pull-right">Start:</button>
                                        </div>
                                        
                                        <div class="col-sm-4">
                                            <input class="inpStart form-control" value="">
                                        </div>
                                        <div class="col-sm-1" >
                                            <button class="buttonListenEnde k-button btn pull-right">End:</button>
                                        </div>
                                        <div class="col-sm-4">
                                            <input class="inpEnde form-control" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-4 state-options">

                                        </div>
                                        <label class="lblRelated control-label col-sm-1">Related:</label>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <input class="inpRelated form-control" type="text" value="">
                                                <span class="input-group-btn">
                                                    <button class="buttonListenRelated k-button btn"><i class="fa fa-folder-open-o" aria-hidden="true"></i></button>
                                                    <button class="buttonRemoveRelated k-button btn"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-1 button-col"></div>
                                        <div class="col-sm-8 place-holder1 button-col">
                                        </div>
                                        <div class="col-sm-2 clearfix button-col">
                                            <button class="buttonNew k-button btn float-right"><i class="fa fa-file-o" aria-hidden="true"></i></button>
                                            <button class="buttonSave k-button btn float-right"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                            <span class="input-group-btn float-right">
                                                
                                            </span>
                                        </div>
                                        <div class="col-sm-1 button-col"></div>
                                    </div>
                                </fieldset>
                        </div>
                `,
                stateOption: `
                    <div class="custom-control custom-radio">
                        <input type="radio" class="@CLASS@ custom-control-input" name="groupOfStates" data-id="@ID@">
                        <label class="lblWork custom-control-label">@NAME@</label>
                    </div>
                `,
                classObjectSelectorWindowTemplate: `
                    <div class="class-object-selector">
                        <div class="class-object-selector-window"></div
                    </div>
                `,
                classObjectSelectorTemplate: `
                    <div class="omodules-splitter">
                        <div>
                            <div class="class-tree-container">
                            </div>
                        </div>
                        <div>
                            <div class="object-list-container">
                                <div class="object-list"></div>
                            </div>
                        </div>
                    </div>
                `,
                listTemplate: `
                    <div class="entry-list"></div>
                `
            }
        }

        timeManagementEntryComponent.elements.timeManagementEntryContainer = this;
        $(timeManagementEntryComponent.elements.timeManagementEntryContainer).addClass('time-management-entry');
        $(timeManagementEntryComponent.elements.timeManagementEntryContainer).data('time-management-component', timeManagementEntryComponent);

        $.post(timeManagementEntryComponent.configuration.actionInit, { idInstance: timeManagementEntryComponent.configuration.IdInstance, selectorPath: timeManagementEntryComponent.configuration.selectorPath })
            .done(function (response) {
                // stuff
                console.log(response);
                if (response.IsSuccessful) {
                    var selectorPath = response.SelectorPath;
                    var element = getElementsBySelectorPath(selectorPath);
                    var timeManagementEntryComponent = $(element).data('time-management-component');
                    timeManagementEntryComponent.configuration.viewModel = response.Result;
                    timeManagementEntryComponent.handler.createComponent(timeManagementEntryComponent);
                } else {

                }

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    }
}) (jQuery);