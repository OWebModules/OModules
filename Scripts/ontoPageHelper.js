﻿var ctrlFlag;
var urlParameterConfig = {
    attributeParam: null,
    classParam: null,
    objectParam: null,
    relationTypeParam: null,
    attributeTypeParam: null,
    senderParam: null,
    initialize: function () {
        urlParameterConfig.classParam = getUrlParameter("Class");
        urlParameterConfig.objectParam = getUrlParameter("Object");
        urlParameterConfig.relationTypeParam = getUrlParameter("RelationType");
        urlParameterConfig.attributeTypeParam = getUrlParameter("AttributeType");
        urlParameterConfig.attributeParam = getUrlParameter("Attribute");
        urlParameterConfig.senderParam = getUrlParameter("Sender");

        if (urlParameterConfig.senderParam != undefined && registerHierarchy != undefined) {
            registerHierarchy(viewModel.IdInstance, urlParameterConfig.senderParam);
        }
    },
    checkSender: function (senderId) {
        if (urlParameterConfig.senderParam == undefined) return true;
        return senderId == urlParameterConfig.senderParam;
    },
    getParameterValue: function (parameter) {
        var parameterValue = getUrlParameter(parameter);
        return parameterValue;
    }

}


function sendCookie() {
    var cookieText = document.cookie;
   
    if (cookieText != undefined && cookieText != null && cookieText != '') {
        ontoLog("send Cookie:" + cookieText)
        SendStringPropertyChangeWithValue("Text_Cookie", cookieText);
        ontoLog("sended Cookie")
    }
   

}

(function ($) {
   
    $.fn.openPageLoader = function () {
        kendo.ui.progress(this, true);
    }

    $.fn.closePageLoader = function () {
        kendo.ui.progress(this, false);
    }

    $.fn.createStatusBar = function (pageInitState) {
        var overlay = $('<div id="overlay"></div>');
        $('body').append(overlay);
        overlay.hide();

        $(this).addClass('onto-status-bar');
        $(this).data('messageout-config', {
            showError: function (message, hideTimout) {
                $('.onto-status-bar').addClass('onto-status-error')
                $('.onto-status-bar').removeClass('onto-status-normal')
                $('.onto-status-bar').removeClass('onto-status-warning')
                $('.onto-status-bar').html(message);
                $('.onto-status-bar').css('height', '50px');
                $('#overlay').show();

                if (hideTimout !== undefined && hideTimout !== null) {
                    setTimeout(function () {
                        $('.onto-status-bar').removeClass('onto-status-error');
                        $('.onto-status-bar').removeClass('onto-status-normal');
                        $('.onto-status-bar').removeClass('onto-status-warning');
                        $('.onto-status-bar').html('');
                        $('.onto-status-bar').css('height', '0px');
                        overlay.hide();
                    }, hideTimout);
                }
            },
            showWarning: function (message, hideTimout) {
                $('.onto-status-bar').removeClass('onto-status-error')
                $('.onto-status-bar').removeClass('onto-status-normal')
                $('.onto-status-bar').addClass('onto-status-warning')
                $('.onto-status-bar').html(message);
                $('.onto-status-bar').css('height', '50px');
                overlay.show();
                if (hideTimout !== undefined && hideTimout !== null) {
                    setTimeout(function () {
                        $('.onto-status-bar').removeClass('onto-status-error');
                        $('.onto-status-bar').removeClass('onto-status-normal');
                        $('.onto-status-bar').removeClass('onto-status-warning');
                        $('.onto-status-bar').html('');
                        $('.onto-status-bar').css('height', '0px');
                        overlay.hide();
                    }, hideTimout);
                }
            },
            showMessage: function (message, hideTimout) {
                $('.onto-status-bar').removeClass('onto-status-error')
                $('.onto-status-bar').addClass('onto-status-normal')
                $('.onto-status-bar').removeClass('onto-status-warning')
                $('.onto-status-bar').html(message);
                $('.onto-status-bar').css('height', '50px');
                if (hideTimout !== undefined && hideTimout !== null) {
                    setTimeout(function () {
                        $('.onto-status-bar').removeClass('onto-status-error');
                        $('.onto-status-bar').removeClass('onto-status-normal');
                        $('.onto-status-bar').removeClass('onto-status-warning');
                        $('.onto-status-bar').html('');
                        $('.onto-status-bar').css('height', '0px');
                        overlay.hide();
                    }, hideTimout);
                }
            },
            showOntoMessage: function (oMessage, hideTimeout) {
                if (oMessage.MessageType == 1) {
                    $('.onto-status-bar').data('messageout-config').showMessage(oMessage.Message, hideTimeout);
                } else if (oMessage.MessageType == 2) {
                    $('.onto-status-bar').data('messageout-config').showWarning(oMessage.Message, hideTimeout);
                } else if (oMessage.MessageType == 3) {
                    $('.onto-status-bar').data('messageout-config').showError(oMessage.Message, hideTimeout);
                } else if (oMessage.MessageType == 4) {
                    $('.onto-status-bar').data('messageout-config').showError(oMessage.Message, hideTimeout);
                }
            },
            clearStatus: function () {
                $('.onto-status-bar').removeClass('onto-status-error')
                $('.onto-status-bar').removeClass('onto-status-normal')
                $('.onto-status-bar').removeClass('onto-status-warning')
                $('.onto-status-bar').html('');
                $('.onto-status-bar').css('height', '0px');
                overlay.hide();
            }
        });

        $(this).data('messageout-config').clearStatus();

        if (pageInitState == undefined) return;

        if (pageInitState.IsError) {
            $('.onto-status-bar').data('messageout-config').showError(pageInitState.Message);
        }
        else if (pageInitState.IsWarning) {
            $('.onto-status-bar').data('messageout-config').showWarning(pageInitState.Message);
        }
    }
    
})(jQuery);

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

// Listen to keyboard. 
window.onkeydown = listenToKeyDown;
window.onkeyup = listenToKeyUp;

function createStatusBar(pageInitState, selector) {

    if (typeof selector === 'undefined') selector = '.onto-status-bar';
    if ($(selector).length == 0) {
        var statusBar = $('<div></div>');
        $('body').append(statusBar);
        statusBar.createStatusBar(pageInitState);
    }
    
    
}

function listenToKeyUp(e) {
    if (e.keyCode == 17)
        ctrlFlag = 0;
 
}

function listenToKeyDown(e) {
    if (e.keyCode == 17)
        ctrlFlag = 1;

}

function getElementsBySelectorPath(selectorPath) {
    var foundElements = null;
    for (var ix = 0; ix < selectorPath.length; ix++) {
        var selector = selectorPath[ix];
        if (foundElements == null) {
            foundElements = $(selector);
        }
        else {
            foundElements = $(foundElements).find(selector);
        }
    }
    return foundElements;
}
