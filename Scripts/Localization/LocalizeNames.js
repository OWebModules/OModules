﻿(function ($) {
    $.fn.createLocalizeNames = function (configuration) {
        var localizeNamesComponent = {
            configuration: configuration,
            data: {
                stateViewItems: []
            },
            elements: {

            },
            components: {

            },
            handler: {
                createComponent: function (localizeNamesComponent) {
                    
                },
                validateReference: function (localizeNamesComponent, oItem) {

                },
                triggerEvents: {
                    createComponent: function (localizeNamesComponent, finishState) {
                        if (finishState == true) {
                            if (typeof localizeNamesComponent.configuration.events !== 'undefined' && localizeNamesComponent.configuration.events !== null &&
                                typeof localizeNamesComponent.configuration.events.createdComponent !== 'undefined' && localizeNamesComponent.configuration.events.createdComponent !== null) {
                                localizeNamesComponent.configuration.events.createdComponent(localizeNamesComponent);
                            }
                        } else {
                            if (typeof localizeNamesComponent.configuration.events !== 'undefined' && localizeNamesComponent.configuration.events !== null &&
                                typeof localizeNamesComponent.configuration.events.createComponent !== 'undefined' && localizeNamesComponent.configuration.events.createComponent !== null) {
                                localizeNamesComponent.configuration.events.createComponent(localizeNamesComponent);
                            }
                        }
                    },
                    validateReference: function (localizeNamesComponent, finishState) {
                        if (finishState == true) {
                            if (typeof localizeNamesComponent.configuration.events !== 'undefined' && localizeNamesComponent.configuration.events !== null &&
                                typeof localizeNamesComponent.configuration.events.validateReference !== 'undefined' && localizeNamesComponent.configuration.events.validateReference !== null) {
                                localizeNamesComponent.configuration.events.validateReference(localizeNamesComponent, oItem);
                            }
                        } else {
                            if (typeof localizeNamesComponent.configuration.events !== 'undefined' && localizeNamesComponent.configuration.events !== null &&
                                typeof localizeNamesComponent.configuration.events.validatedReference !== 'undefined' && localizeNamesComponent.configuration.events.validatedReference !== null) {
                                localizeNamesComponent.configuration.events.validatedReference(localizeNamesComponent, oItem);
                            }
                        }
                    }
                }
            },
            templates: {
                localizeNames: `
                    <div class="localize-names"></div>
`
            }
        }

        localizeNamesComponent.elements.localizeNamesContainer = this;
        $(localizeNamesComponent.elements.localizeNamesContainer).addClass('localize-names');
        $(localizeNamesComponent.elements.localizeNamesContainer).data('localize-names-component', localizeNamesComponent);


    }
});