﻿(function ($) {
    $.fn.createHtmlSearch = function (configuration) {
        $(this).data('html-search-config', {
            configuration: configuration,
            elements: {
                toolBar: null,
                gridContainer: null,
                grid: null,
                telerikGrid: null
            },
            clickButtonHandler: null,
            gridChangeHandler: function () {
                
                var selectedItem = $('.html-search').data('html-search-config').elements.telerikGrid.dataItem($('.html-search').data('html-search-config').elements.telerikGrid.select());
                var items = [];

                if (selectedItem.IdReference != undefined && selectedItem.IdReference != null) {
                    items = [
                        {
                            GUID: selectedItem.IdReference,
                            Name: selectedItem.NameReference,
                            GUID_Parent: selectedItem.IdParentReference,
                            Type: 'Object'
                        }
                    ];
                } else {
                    items = [
                        {
                            GUID: selectedItem.IdHtmlDocument,
                            Name: selectedItem.NameHtmlDocument,
                            GUID_Parent: selectedItem.IdDocumentClass,
                            Type: 'Object'
                        }
                    ];
                }
                

                $('.html-search').data('html-search-config').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                    ChannelId: $('.html-search').data('html-search-config').configuration.channelSelectedObject,
                    SenderId: $('.html-search').data('html-search-config').configuration.managerConfig.idEndpoint,
                    SessionId: $('.html-search').data('html-search-config').configuration.managerConfig.idSession,
                    UserId: $('.html-search').data('html-search-config').configuration.idUser,
                    OItems: items
                });
            }
        });

        $(this).addClass("html-search");
        $(this).html('');
        $('.html-search').data('html-search-config').elements.toolBar = $("<div class='html-search-toolbar'></div>");
        $(this).append($('.html-search').data('html-search-config').elements.toolBar);
        $('.html-search').data('html-search-config').elements.gridContainer = $("<div class='html-search-grid-container'></div>");
        $(this).append($('.html-search').data('html-search-config').elements.gridContainer);
        $('.html-search').data('html-search-config').elements.grid = $("<div class='html-search-grid'></div>");
        $(this).append($('.html-search').data('html-search-config').elements.grid);

        $('.html-search').data('html-search-config').elements.toolBar.kendoToolBar({
            items: [
                {
                    template: '<input class="k-textbox html-search-input" style="width: 250px;" placeholder="search" />'

                },
                {
                    template: '<button class="k-button html-search-action"><i class="fa fa-search" aria-hidden="true"></i></button>'

                }


            ]
        });

        $('.html-search').find('.html-search-action').on('click', function () {
            if ($('.html-search').data('html-search-config').elements.telerikGrid !== undefined) {
                $('.html-search').data('html-search-config').elements.telerikGrid.dataSource.read();
            }
        });

        $.post($('.html-search').data('html-search-config').configuration.actionGetGridConfig)
            .done(function (response) {

                $.extend(response.dataSource, {
                    batch: true
                });

                $.extend(response.dataSource.transport.read, {
                    data: function () {

                        return {
                            IdInstance: $('.html-search').data('html-search-config').configuration.idInstance,
                            searchString: btoa($('.html-search').find('.html-search-input').val())
                        }


                    }
                })

                $.extend(response.dataSource.transport.update,
                    {
                        complete: function (e) {
                            console.log(e);
                        }
                    }
                );

                var dataSource = new kendo.data.DataSource(response.dataSource);
                response.dataSource = dataSource;
                response.dataBound = function () {
                    $('.html-search').data('html-search-config').configuration.dataBoundHandler;
                }

                $.extend(response, {
                    change: $('.html-search').data('html-search-config').gridChangeHandler,
                    filterable: {
                        mode: "row"
                    }
                });
                $('.html-search').data('html-search-config').elements.telerikGrid = $('.html-search').find('.html-search-grid').kendoGrid(response).data("kendoGrid");
                $('.k-button').kendoButton({
                    click: $('.html-search').data('html-search-config').clickApplyButtonHandler
                });


                $('.html-search').data('html-search-config').elements.telerikGrid.wrapper.find(".k-pager")
                    .before('<a href="#" class="k-link prev">Prev</a>')
                    .after('<a href="#" class="k-link next">Next</a>')
                    .parent()
                    .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                    .delegate("a.prev", "click", function () {
                        dataSource.page(dataSource.page() - 1);
                    })
                    .delegate("a.next", "click", function () {
                        dataSource.page(dataSource.page() + 1);
                    })
                    .delegate(".pagesize input", "change", function () {
                        dataSource.page(parseInt(this.value, 10));
                    })




                if ($('.html-search').data('html-search-config').configuration.gridComplete !== undefined) {
                    $('.html-search').data('html-search-config').configuration.gridComplete();
                }




            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

        if ($('.html-search').data('html-search-config').configuration !==undefined) {

            if ($('.html-search').data('html-search-config').configuration.createdCallback !==undefined) {
                $('.html-search').data('html-search-config').configuration.createdCallback();
            }

        }
    }

    
}) (jQuery);