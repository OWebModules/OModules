﻿(function ($) {
    $.fn.createTemplateChoose = function (configuration) {
        $(this).data('template-choose-config', {
            configuration: configuration,
            elements: {
                gridElement: null,
                telerikGrid: null
            },
            clickButtonHandler: null,
            validateReference: function (idObject, validateReferenceCallback) {
                $('.template-choose').openPageLoader();
                $('.template-choose').data('template-choose-config').idObject = idObject;
                var refItem = {
                    GUID: idObject
                }
                $.post($('.template-choose').data('template-choose-config').configuration.actionValidateReference, {
                    refItem: refItem,
                    idInstance: $('.template-choose').data('template-choose-config').configuration.idInstance
                })
                    .done(function (response) {
                        console.log(response);
                        if (response.IsSuccessful) {
                            $('.template-choose').data('template-choose-config').elements.telerikGrid.dataSource.read();

                        }

                        if (validateReferenceCallback !== undefined) {
                            validateReferenceCallback();
                        }
                        $('.template-choose').closePageLoader();
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
                }
        });

        $(this).addClass("template-choose");
        $(this).html('');
        $('.template-choose').data('template-choose-config').elements.gridElement = $("<div class='template-grid'></div>");
        $(this).append($('.template-choose').data('template-choose-config').elements.gridElement);

        $.post($('.template-choose').data('template-choose-config').configuration.actionGetGridConfig)
            .done(function (response) {

                $.extend(response.dataSource, {
                    batch: true
                });

                $.extend(response.dataSource.transport.read, {
                    data: function () {

                        return {
                            IdInstance: $('.template-choose').data('template-choose-config').configuration.idInstance
                        }


                    }
                })

                $.extend(response.dataSource.transport.update,
                    {
                        complete: function (e) {
                            console.log(e);
                        }
                    }
                );

                var dataSource = new kendo.data.DataSource(response.dataSource);
                response.dataSource = dataSource;
                response.dataBound = function () {
                    $('.use-template-button').on('click', function () {
                        console.log(this);
                        var row = $(this).closest('tr');
                        var dataItem = $('.template-choose').data('template-choose-config').elements.telerikGrid.dataItem(row);

                        $.post($('.template-choose').data('template-choose-config').configuration.actionGetTemplateContent, {
                            idHtmlDoc: dataItem.IdTemplate,
                            idInstance: $('.template-choose').data('template-choose-config').configuration.idInstance
                        })
                            .done(function (response) {
                                console.log(response);
                                if (response.IsSuccessful) {
                                    $('.template-choose').data('template-choose-config').configuration.templateReceived(response.Result);

                                }
                                $('.template-choose').closePageLoader();
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });
                        
                    });
                    $('.template-choose').data('template-choose-config').configuration.dataBoundHandler;
                }

                $.extend(response, {
                    change: $('.template-choose').data('template-choose-config').gridChangeHandler,
                    filterable: {
                        mode: "row"
                    }
                });
                $('.template-choose').data('template-choose-config').elements.telerikGrid = $('.template-choose').find('.template-grid').kendoGrid(response).data("kendoGrid");
                $('.k-button').kendoButton({
                    click: $('.template-choose').data('template-choose-config').clickApplyButtonHandler
                });


                $('.template-choose').data('template-choose-config').elements.telerikGrid.wrapper.find(".k-pager")
                    .before('<a href="#" class="k-link prev">Prev</a>')
                    .after('<a href="#" class="k-link next">Next</a>')
                    .parent()
                    .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                    .delegate("a.prev", "click", function () {
                        dataSource.page(dataSource.page() - 1);
                    })
                    .delegate("a.next", "click", function () {
                        dataSource.page(dataSource.page() + 1);
                    })
                    .delegate(".pagesize input", "change", function () {
                        dataSource.page(parseInt(this.value, 10));
                    })




                if ($('.template-choose').data('template-choose-config').configuration.gridComplete != undefined) {
                    $('.template-choose').data('template-choose-config').configuration.gridComplete();
                }




            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

        if ($('.template-choose').data('template-choose-config').configuration != undefined) {

            if ($('.template-choose').data('template-choose-config').configuration.createdCallback != undefined) {
                $('.template-choose').data('template-choose-config').configuration.createdCallback();
            }
            
        }
    }
})(jQuery);