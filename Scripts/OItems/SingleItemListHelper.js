﻿
function initViewModel() {
    viewModel.addApplyItem = function (item) {
        viewModel._itemsPreApply.push(item);
        viewModel.checkViewState();
    };
    viewModel.removeApplyItem = function (item) {
        viewModel._itemsPreApply = jQuery.grep(viewModel._itemsPreApply, function (itemInArray) {
            return itemInArray.IdInstance != item.IdInstance
        });

        viewModel.checkViewState();
    };

   
}


function initControls(actionGetGridConfig,
    idApply,
    idListen,
    useIdParent,
    gridChangeHandler,
    clickApplyButtonHandler) {


    $('body').openPageLoader();
    
    $.post(actionGetGridConfig)
        .done(function (response) {
            if (useIdParent) {
                $.extend(response.dataSource.transport.read, {
                    data: function () {
                        return { idParent: viewModel._idParent, idInstance: viewModel.IdInstance, nameFilter: viewModel.getFilter() };
                    }
                })
            }
            else {
                $.extend(response.dataSource.transport.read, {
                    data: function () {
                        return { idInstance: viewModel.IdInstance };
                    }
                })
            }
            
            var dataSource = new kendo.data.DataSource(response.dataSource);
            response.dataSource = dataSource;
            response.dataBound = function () {
               

                var pagerElement = this.pager.element;
                if (!pagerElement.find("#guidItem").length) {
                    pagerElement
                        .append('<span><input id="guidItem" class="k-textbox idInput" placeholder="GUID" readonly></input></span>');
                }

                $('.chkbxApply').change(function () {
                    var row = $(this).closest('tr');
                    var item = viewModel.gridItem.dataItem(row);
                    var applied = $(this).prop("checked");
                    item.Apply = applied;
                    if (item.Apply) {
                        viewModel.addApplyItem(item);
                    } else {
                        viewModel.removeApplyItem(item);
                    }

                    console.log(item);
                });

            };
            $.extend(response, {
                change: gridChangeHandler,
                filterable: {
                    mode: "row"
                }
            });
            $.extend(response, { toolbar: kendo.template($("#template").html()) });
            viewModel.gridItem = $("#" + viewModel.grid.ViewItemId).kendoGrid(response).data("kendoGrid");
            $('.k-button').kendoButton({
                click: clickApplyButtonHandler
            });

            viewModel.gridItem.one("dataBound", function (e) {
                var filterCell = e.sender.thead.find(".k-filtercell[data-field='NameInstance']");
                if (filterCell.length == 0) return;
                var filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                filterDropDown.select(3);
                filterDropDown.trigger("change");
            });

            $('body').closePageLoader();
            viewModel.setDataItems();

            
            viewModel.getUrlParameters();

            viewModel.reloadGrid();
            //if (!useIdParent) {
            //    reloadGrid();
            //}
            
        })
        .fail(function (jqxhr, textStatus, error) {
            // stuff
        });



    $('#' + viewModel.newWindowViewModel.id).kendoWindow({
        width: '1024px',
        height: '600px',
        title: 'New Item',
        visible: false,
        modal: true,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();

    $('#' + viewModel.editWindowViewModel.id).kendoWindow({
        width: '350px',
        height: '260px',
        title: 'Edit Name',
        visible: false,
        modal: true,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();
    //$('#editNameItemWindow').data("kendoWindow").open();

    $('#' + viewModel.editWindowViewModel.idInpValueEditOrderId).kendoNumericTextBox({ format: 'n0' });

    $('#' + viewModel.editOrderWindowViewModel.id).kendoWindow({
        width: '160px',
        height: '160px',
        title: 'OrderId',
        visible: false,
        modal: true,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();


    $('#' + viewModel.deleteWindowViewModel.id).kendoWindow({
        width: '160px',
        height: '160px',
        title: 'Delete',
        visible: false,
        modal:true,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();

    var slide = kendo.fx($("#slide-in-share")).slideIn("left");
    var slideVisible = true;

    $("#slide-in-handle").click(function (e) {
        if (slideVisible) {
            slide.reverse();
        } else {
            slide.play();
        }
        slideVisible = !slideVisible;
        e.preventDefault();
    });


}