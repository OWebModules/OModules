﻿(function ($) {
    $.fn.createRelationListLeftRight = function (configuration) {
        var relationListLeftRight = {
            configuration: configuration,
            elements: {
                relationListContainer: $(this),
                gridParent: null,
                grid: null,
                dialogAddToClipboardWindow: null
            },
            gridItem: null,
            nodeItem: null,
            currentDataItem: null,
            _itemsPreApply: [],
            addApplyItem: function (item) {
                $('.relation-list-left-right').data('relation-list-left-right')._itemsPreApply.push(item);
                $('.relation-list-left-right').data('relation-list-left-right').checkViewState();
            },
            removeApplyItem: function (item) {
                $('.relation-list-left-right').data('relation-list-left-right')._itemsPreApply = jQuery.grep($('.relation-list-left-right').data('relation-list-left-right')._itemsPreApply, function (itemInArray) {
                    return itemInArray.IdItem !== item.IdItem
                });

                $('.relation-list-left-right').data('relation-list-left-right').checkViewState();
            },
            setNodeItem: function(nodeItem) {
                $('.relation-list-left-right').data('relation-list-left-right').nodeItem = nodeItem;
                $('.relation-list-left-right').data('relation-list-left-right').checkViewState();
            },
            checkViewState: function () {
                $('.relation-list-left-right').find('.newItem').setEnabled($('.relation-list-left-right').data('relation-list-left-right')._idParent !== undefined, true);
                $('.relation-list-left-right').find('.applyItem').setEnabled($('.relation-list-left-right').data('relation-list-left-right')._itemsPreApply.length > 0, true);
                $('.relation-list-left-right').find('.addToClipboard').setEnabled($('.relation-list-left-right').data('relation-list-left-right')._itemsPreApply.length > 0, true);
                $('.relation-list-left-right').find('.deleteItem').setEnabled($('.relation-list-left-right').data('relation-list-left-right')._itemsPreApply.length > 0, true);
                $('.relation-list-left-right').find('.orderAsc').setEnabled($('.relation-list-left-right').data('relation-list-left-right')._itemsPreApply.length > 0, true);
                $('.relation-list-left-right').find('.orderDesc').setEnabled($('.relation-list-left-right').data('relation-list-left-right')._itemsPreApply.length > 0, true);

            },
            gridChangeHandler: function(e) {
                $('.relation-list-left-right').data('relation-list-left-right').currentDataItem = $('.relation-list-left-right').data('relation-list-left-right').gridItem.dataItem(this.select());
                var selectedItem = {
                    IdInstance: $('.relation-list-left-right').data('relation-list-left-right').configuration.idInstance,
                    IdObject: ""
                };

                if ($('.relation-list-left-right').data('relation-list-left-right').currentDataItem !== undefined) {
                    selectedItem = {
                        IdInstance: $('.relation-list-left-right').data('relation-list-left-right').configuration.idInstance,
                        IdObject: $('.relation-list-left-right').data('relation-list-left-right').currentDataItem.IdObject,
                        IdOther: $('.relation-list-left-right').data('relation-list-left-right').currentDataItem.IdOther,
                        IdRelationType: $('.relation-list-left-right').data('relation-list-left-right').currentDataItem.IdRelationType,
                        OrderId: $('.relation-list-left-right').data('relation-list-left-right').currentDataItem.OrderId
                    };

                    console.log($('.relation-list-left-right').data('relation-list-left-right').currentDataItem);
                    var items = [
                        {
                            GUID: $('.relation-list-left-right').data('relation-list-left-right').currentDataItem.IdOther
                        }];
                    $('.relation-list-left-right').data('relation-list-left-right').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: $('.relation-list-left-right').data('relation-list-left-right').configuration.channelSelectedItem,
                        SenderId: $('.relation-list-left-right').data('relation-list-left-right').configuration.managerConfig.idEndpoint,
                        SessionId: $('.relation-list-left-right').data('relation-list-left-right').configuration.managerConfig.idSession,
                        UserId: $('.relation-list-left-right').data('relation-list-left-right').configuration.idUser,
                        OItems: items
                    });
                }

                $.post($('.relation-list-left-right').data('relation-list-left-right').actionSetSelectedItem, selectedItem)
                    .done(function (response) {
                        console.log(response);

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            },
            clickButtonHandler: function(e) {
                var button = $(e.event.target).closest(".k-button");
                if (button.hasClass('applyItem')) {
                    var items = [];
                    $('.relation-list-left-right').find('.chkbxApply:checked').closest('tr').each(function () {
                        var row = $(this);
                        var item = $('.relation-list-left-right').data('relation-list-left-right').gridItem.dataItem(row);
                        console.log(item);
                        items.push({ GUID: item.IdOther });
                    });

                    console.log(items);
                    $('.relation-list-left-right').data('relation-list-left-right').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: $('.relation-list-left-right').data('relation-list-left-right').configuration.channelAppliedItems,
                        SenderId: $('.relation-list-left-right').data('relation-list-left-right').configuration.managerConfig.idEndpoint,
                        SessionId: $('.relation-list-left-right').data('relation-list-left-right').configuration.managerConfig.idSession,
                        UserId: $('.relation-list-left-right').data('relation-list-left-right').configuration.idUser,
                        OItems: items
                    });

                } else if (button.hasClass('isListen')) {
                    button.toggleMode(true);
                } else if (button.hasClass('orderAsc')) {
                    var value = button.parent().find('.inpOrderId').val();
                    items = [];
                    $('.relation-list-left-right').find('.chkbxApply:checked').closest('tr').each(function () {
                        var row = $(this);
                        var item = $('.relation-list-left-right').data('relation-list-left-right').gridItem.dataItem(row);
                        console.log(item);
                        items.push({
                            uid: item.uid,
                            IdInstance: $('.relation-list-left-right').data('relation-list-left-right').configuration.idInstance,
                            IdItem: item.IdItem,
                            IdObject: item.IdObject,
                            IdOther: item.IdOther,
                            IdRelationType: item.IdRelationType,
                            OrderId: value++
                        });
                    });

                    $.post($('.relation-list-left-right').data('relation-list-left-right').configuration.actionSetOrderId, { requestList: items })
                        .done(function (response) {
                            console.log(response);
                            if (response.IsSuccessful) {
                                for (var ix in items) {
                                    var item = items[ix];
                                    var row = $('.relation-list-left-right').data('relation-list-left-right').gridItem.tbody.find("tr[data-uid='" + item.uid + "']");
                                    var dataItem = $('.relation-list-left-right').data('relation-list-left-right').gridItem.dataItem(row);
                                    dataItem.set("OrderId", item.OrderId);

                                }
                            }


                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });


                } else if (button.hasClass('deleteItem')) {
                    var deleteItems = [];
                    if ($('.relation-list-left-right').find('.chkbxApply:checked').closest('tr').length > 0 && confirm("Do you want to delete checked rows?")) {
                        $('.relation-list-left-right').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var dataItem = $('.relation-list-left-right').data('relation-list-left-right').gridItem.dataItem(row);
                            deleteItems.push(dataItem);

                            //$('.relation-list-left-right').data('relation-list-left-right').gridItem.removeRow(row);

                        });

                        for (var ix in deleteItems) {
                            var item = deleteItems[ix];
                            $('.relation-list-left-right').data('relation-list-left-right').gridItem.dataSource.remove(item);
                        }

                        $('.relation-list-left-right').data('relation-list-left-right').gridItem.dataSource.sync();
                    }

                } else if (button.hasClass('addToClipboard')) {
                    var clipboardItems = [];
                    if ($('.relation-list-left-right').find('.chkbxApply:checked').closest('tr').length > 0) {
                        $('.relation-list-left-right').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var dataItem = $('.relation-list-left-right').data('relation-list-left-right').gridItem.dataItem(row);
                            var clipboardItem = {
                                GUID: dataItem.IdOther,
                                Name: dataItem.NameOther,
                                GUID_Parent: dataItem.IdParentOther,
                                Type: dataItem.Ontology
                            };
                            clipboardItems.push(clipboardItem);

                        });

                        $.post($('.relation-list-left-right').data('relation-list-left-right').configuration.actionAddToClipboard, { oItems: clipboardItems, idInstance: $('.relation-list-left-right').data('relation-list-left-right').configuration.idInstance })
                            .done(function (response) {
                                
                                $('.relation-list-left-right').data('relation-list-left-right').elements.dialogAddToClipboardWindow.kendoDialog({
                                    width: "450px",
                                    title: "Add to Clipboard Result",
                                    closable: false,
                                    modal: false,
                                    content: "<p>Added " + response.Result.length + " Items<p>",
                                    actions: [
                                        { text: 'OK', primary: true }
                                    ],
                                    close: function (e) {

                                    }
                                });
                                $('.relation-list-left-right').data('relation-list-left-right').elements.dialogAddToClipboardWindow.data('kendoDialog').open();
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });
                    }
                }


            },
            validateReference: function (relationNodeItem) {
                $('.relation-list-left-right').data('relation-list-left-right').setNodeItem(relationNodeItem);
                //document.title = nodeItem.NameLeft + ' -> ' + nodeItem.NameRelationType + ' -> ' + nodeItem.NameRight;
                $('.relation-list-left-right').data('relation-list-left-right').gridItem.dataSource.read();
            }
        };

        $(this).data('relation-list-left-right', relationListLeftRight);

        $.post($('.relation-list-left-right').data('relation-list-left-right').configuration.actionComponentObjectRelationLeftRightList, { idInstance: $('.relation-list-left-right').data('relation-list-left-right').configuration.idInstance })
            .done(function (response) {
                console.log(response);
                


            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

        var gridParent = $('<div class="gridParent"></div>');

        relationListLeftRight.elements.gridParent = gridParent;
        relationListLeftRight.elements.relationListContainer.append(relationListLeftRight.elements.gridParent);

        relationListLeftRight.elements.dialogAddToClipboardWindow = $('<div class="add-to-clipboard-window"></div>');
        $(this).append(relationListLeftRight.elements.dialogAddToClipboardWindow);

        var grid = $('<div class="grid"></div>');

        relationListLeftRight.elements.grid = grid;
        relationListLeftRight.elements.gridParent.append(relationListLeftRight.elements.grid);

        $.post(relationListLeftRight.configuration.actionGetGridConfig)
            .done(function (response) {
                if ($('.relation-list-left-right').data('relation-list-left-right').configuration.useIdParent) {
                    $.extend(response.dataSource.transport.read, {
                        data: function () {
                            if ($('.relation-list-left-right').data('relation-list-left-right').nodeItem !== undefined) {
                                return {
                                    IdLeft: $('.relation-list-left-right').data('relation-list-left-right').nodeItem.IdLeft,
                                    NameLeft: $('.relation-list-left-right').data('relation-list-left-right').nodeItem.NameLeft,
                                    IdRight: $('.relation-list-left-right').data('relation-list-left-right').nodeItem.IdRight,
                                    NameRight: $('.relation-list-left-right').data('relation-list-left-right').nodeItem.NameRight,
                                    IdRelationType: $('.relation-list-left-right').data('relation-list-left-right').nodeItem.IdRelationType,
                                    NameRelationType: $('.relation-list-left-right').data('relation-list-left-right').nodeItem.NameRelationType,
                                    NodeType: $('.relation-list-left-right').data('relation-list-left-right').nodeItem.NodeType,
                                    IdInstance: $('.relation-list-left-right').data('relation-list-left-right').configuration.idInstance
                                };
                            } else {
                                return {
                                    IdInstance: $('.relation-list-left-right').data('relation-list-left-right').configuration.idInstance
                                };
                            }

                        }
                    });
                }

                $.extend(response.dataSource, {
                    batch: true
                });

                response.dataSource.transport.parameterMap = function (data, type) {
                    if (type == "destroy") {
                        var deleteItems = {
                            models: []
                        };
                        data.models.forEach(function (item) {
                            deleteItems.models.push({ IdRelationType: item.IdRelationType, IdObject: item.IdObject, IdOther: item.IdOther });
                        });
                        return deleteItems;
                    }
                    if (type == "read") {
                        return data;
                    }
                    if (type == "update") {
                        var updateItems = {
                            models: []
                        };
                        data.models.forEach(function (item) {
                            updateItems.models.push({ IdRelationType: item.IdRelationType, IdObject: item.IdObject, IdOther: item.IdOther, NameOther: item.NameOther, OrderId: item.OrderId });
                        });
                        return updateItems;
                    }
                };

                $.extend(response.dataSource.transport.update,
                    {
                        complete: function (e) {
                            console.log(e);
                            
                        }
                    }
                );

                var dataSource = new kendo.data.DataSource(response.dataSource);
                response.dataSource = dataSource;
                response.dataBound = function () {
                    if ($('.relation-list-left-right').data('relation-list-left-right').dataBoundHandler !== undefined) {
                        $('.relation-list-left-right').data('relation-list-left-right').dataBoundHandler();
                    }
                    $('.relation-list-left-right').find('.chkbxApply').change(function () {

                        var row = $(this).closest('tr');
                        var item = $('.relation-list-left-right').data('relation-list-left-right').gridItem.dataItem(row);
                        var applied = $(this).prop("checked");
                        item.Apply = applied;
                        if (item.Apply) {
                            $('.relation-list-left-right').data('relation-list-left-right').addApplyItem(item);
                        } else {
                            $('.relation-list-left-right').data('relation-list-left-right').removeApplyItem(item);
                        }

                        console.log(item);
                    });

                    if ($('.relation-list-left-right').find('.k-button').data('kendoButton') === undefined) {
                        $('.relation-list-left-right').find('.k-button').kendoButton({
                            click: $('.relation-list-left-right').data('relation-list-left-right').clickButtonHandler
                        });
                    }

                    $('.relation-list-left-right').find('.button-object-edit-left').click(function () {
                        var row = $(this).closest('tr');
                        var item = $('.relation-list-left-right').data('relation-list-left-right').gridItem.dataItem(row);
                        var url = $('.relation-list-left-right').data('relation-list-left-right').configuration.actionRelationEditor;
                        url += "?Object=" + item.IdOther;
                        window.open(url, "_blank");
                    });

                    $('.relation-list-left-right').find('.button-module-starter-left').click(function () {
                        var row = $(this).closest('tr');
                        var item = $('.relation-list-left-right').data('relation-list-left-right').gridItem.dataItem(row);
                        var url = $('.relation-list-left-right').data('relation-list-left-right').configuration.actionModuleStarter;
                        url += "?Object=" + item.IdOther;
                        window.open(url, "_blank");
                    });

                };
                $.extend(response, {
                    change: $('.relation-list-left-right').data('relation-list-left-right').gridChangeHandler,
                    filterable: {
                        mode: "row"
                    },
                    cellClose: function (e) {
                        e.model.IsSaved = false;
                        $('.relation-list-left-right').data('relation-list-left-right').gridItem.saveChanges();
                    }
                });
                $.extend(response, { toolbar: kendo.template($("#templateRelationList").html()) });
                $('.relation-list-left-right').data('relation-list-left-right').gridItem = $('.relation-list-left-right').find('.grid').kendoGrid(response).data("kendoGrid");
                

                $('.relation-list-left-right').data('relation-list-left-right').gridItem.one("dataBound", function (e) {
                    var filterCell = e.sender.thead.find(".k-filtercell[data-field='NameObject']");
                    if (filterCell.length !== 0) {
                        filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                        filterDropDown.select(3);
                        filterDropDown.trigger("change");
                    }

                    filterCell = e.sender.thead.find(".k-filtercell[data-field='NameOther']");
                    if (filterCell.length !== 0) {
                        filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                        filterDropDown.select(3);
                        filterDropDown.trigger("change");
                    }
                });

                $('.relation-list-left-right').data('relation-list-left-right').gridItem.wrapper.find(".k-pager")
                    .before('<a href="#" class="k-link prev">Prev</a>')
                    .after('<a href="#" class="k-link next">Next</a>')
                    .parent()
                    .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                    .delegate("a.prev", "click", function () {
                        dataSource.page(dataSource.page() - 1);
                    })
                    .delegate("a.next", "click", function () {
                        dataSource.page(dataSource.page() + 1);
                    })
                    .delegate(".pagesize input", "change", function () {
                        dataSource.page(parseInt(this.value, 10));
                    })



                $('.relation-list-left-right').data('relation-list-left-right').elements.grid.find('.header-checkbox').change(function (ev) {
                    var checked = ev.target.checked;
                    $('.relation-list-left-right').find('.chkbxApply').each(function (idx, item) {
                        $(item).click();
                    });
                });

                $('body').closePageLoader();


                if (!$('.relation-list-left-right').data('relation-list-left-right').configuration.useIdParent) {
                    $('.relation-list-left-right').data('relation-list-left-right').gridItem.dataSource.read();
                }

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

    };

    $.fn.createRelationListLeftRightOther = function (configuration) {
        var relationListLeftRightOther = {
            configuration: configuration,
            elements: {
                relationListContainer: $(this),
                gridParent: null,
                grid: null,
                dialogAddToClipboardWindow: null

            },
            gridItem: null,
            nodeItem: null,
            currentDataItem: null,
            _itemsPreApply: [],
            addApplyItem: function (item) {
                $('.relation-list-left-right-other').data('relation-list-left-right-other')._itemsPreApply.push(item);
                $('.relation-list-left-right-other').data('relation-list-left-right-other').checkViewState();
            },
            removeApplyItem: function (item) {
                $('.relation-list-left-right-other').data('relation-list-left-right-other')._itemsPreApply = jQuery.grep($('.relation-list-left-right-other').data('relation-list-left-right-other')._itemsPreApply, function (itemInArray) {
                    return itemInArray.IdItem !== item.IdItem
                });

                $('.relation-list-left-right-other').data('relation-list-left-right-other').checkViewState();
            },
            setNodeItem: function (nodeItem) {
                $('.relation-list-left-right-other').data('relation-list-left-right-other').nodeItem = nodeItem;
                $('.relation-list-left-right-other').data('relation-list-left-right-other').checkViewState();
            },
            checkViewState: function () {
                $('.relation-list-left-right-other').find('.newItem').setEnabled($('.relation-list-left-right-other').data('relation-list-left-right-other')._idParent !== undefined, true);
                $('.relation-list-left-right-other').find('.applyItem').setEnabled($('.relation-list-left-right-other').data('relation-list-left-right-other')._itemsPreApply.length > 0, true);
                $('.relation-list-left-right-other').find('.addToClipboard').setEnabled($('.relation-list-left-right-other').data('relation-list-left-right-other')._itemsPreApply.length > 0, true);
                $('.relation-list-left-right-other').find('.deleteItem').setEnabled($('.relation-list-left-right-other').data('relation-list-left-right-other')._itemsPreApply.length > 0, true);
                $('.relation-list-left-right-other').find('.orderAsc').setEnabled($('.relation-list-left-right-other').data('relation-list-left-right-other')._itemsPreApply.length > 0, true);
                $('.relation-list-left-right-other').find('.orderDesc').setEnabled($('.relation-list-left-right-other').data('relation-list-left-right-other')._itemsPreApply.length > 0, true);

            },
            dataBoundHandler: function () {
            },
            gridChangeHandler: function (e) {
                $('.relation-list-left-right-other').data('relation-list-left-right-other').currentDataItem = $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.dataItem(this.select());
                var selectedItem = {
                    IdInstance: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.idInstance,
                    IdObject: ""
                };

                if ($('.relation-list-left-right-other').data('relation-list-left-right-other').currentDataItem !== undefined) {
                    selectedItem = {
                        IdInstance: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.idInstance,
                        IdObject: $('.relation-list-left-right-other').data('relation-list-left-right-other').currentDataItem.IdObject,
                        IdOther: $('.relation-list-left-right-other').data('relation-list-left-right-other').currentDataItem.IdOther,
                        IdRelationType: $('.relation-list-left-right-other').data('relation-list-left-right-other').currentDataItem.IdRelationType,
                        OrderId: $('.relation-list-left-right-other').data('relation-list-left-right-other').currentDataItem.OrderId
                    };

                    console.log($('.relation-list-left-right-other').data('relation-list-left-right-other').currentDataItem);
                    var items = [
                        {
                            GUID: $('.relation-list-left-right-other').data('relation-list-left-right-other').currentDataItem.IdOther
                        }];
                    $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.channelSelectedItem,
                        SenderId: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.managerConfig.idEndpoint,
                        SessionId: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.managerConfig.idSession,
                        UserId: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.idUser,
                        OItems: items
                    });
                }

                $.post($('.relation-list-left-right-other').data('relation-list-left-right-other').actionSetSelectedItem, selectedItem)
                    .done(function (response) {
                        console.log(response);

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            },
            clickButtonHandler: function (e) {
                var button = $(e.event.target).closest(".k-button");
                if (button.hasClass('applyItem')) {
                    var items = [];
                    $('.relation-list-left-right-other').find('.chkbxApply:checked').closest('tr').each(function () {
                        var row = $(this);
                        var item = $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.dataItem(row);
                        console.log(item);
                        items.push({ GUID: item.IdOther });
                    })

                    console.log(items);
                    $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.channelAppliedItems,
                        SenderId: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.managerConfig.idEndpoint,
                        SessionId: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.managerConfig.idSession,
                        UserId: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.idUser,
                        OItems: items
                    });

                } else if (button.hasClass('isListen')) {
                    button.toggleMode(true);
                } else if (button.hasClass('orderAsc')) {
                    var value = button.parent().find('.inpOrderId').getContent();
                    items = [];
                    $('.relation-list-left-right-other').find('.chkbxApply:checked').closest('tr').each(function () {
                        var row = $(this);
                        var item = $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.dataItem(row);
                        console.log(item);
                        items.push({
                            uid: item.uid,
                            IdInstance: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.idInstance,
                            IdItem: item.IdItem,
                            IdObject: item.IdObject,
                            IdOther: item.IdOther,
                            IdRelationType: item.IdRelationType,
                            OrderId: value++
                        });
                    });

                    $.post($('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.actionSetOrderId, { requestList: items })
                        .done(function (response) {
                            console.log(response);
                            if (response.IsSuccessful) {
                                for (var ix in items) {
                                    var item = items[ix];
                                    var row = $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.tbody.find("tr[data-uid='" + item.uid + "']");
                                    var dataItem = $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.dataItem(row);
                                    dataItem.set("OrderId", item.OrderId);

                                }
                            }


                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });


                } else if (button.hasClass('deleteItem')) {
                    var deleteItems = [];
                    if ($('.relation-list-left-right-other').find('.chkbxApply:checked').closest('tr').length > 0 && confirm("Do you want to delete checked rows?")) {
                        $('.relation-list-left-right-other').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var dataItem = $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.dataItem(row);
                            deleteItems.push(dataItem);

                            //$('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.removeRow(row);

                        });

                        for (var ix in deleteItems) {
                            var item = deleteItems[ix];
                            $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.dataSource.remove(item);
                        }

                        $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.dataSource.sync();
                    }

                } else if (button.hasClass('addToClipboard')) {
                    var clipboardItems = [];
                    if ($('.relation-list-left-right-other').find('.chkbxApply:checked').closest('tr').length > 0) {
                        $('.relation-list-left-right-other').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var dataItem = $('.relation-list-left-right').data('relation-list-left-right-other').gridItem.dataItem(row);
                            var clipboardItem = {
                                GUID: dataItem.IdOther,
                                Name: dataItem.NameOther,
                                GUID_Parent: dataItem.IdParentOther,
                                Type: dataItem.Ontology
                            };
                            clipboardItems.push(clipboardItem);

                        });

                        $.post($('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.actionAddToClipboard, { oItems: clipboardItems, idInstance: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.idInstance })
                            .done(function (response) {

                                $('.relation-list-left-right-other').data('relation-list-left-right-other').elements.dialogAddToClipboardWindow.kendoDialog({
                                    width: "450px",
                                    title: "Add to Clipboard Result",
                                    closable: false,
                                    modal: false,
                                    content: "<p>Added " + response.Result.length + " Items<p>",
                                    actions: [
                                        { text: 'OK', primary: true }
                                    ],
                                    close: function (e) {

                                    }
                                });
                                $('.relation-list-left-right-other').data('relation-list-left-right-other').elements.dialogAddToClipboardWindow.data('kendoDialog').open();
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });
                    }
                }


            },
            validateReference: function (relationNodeItem) {
                $('.relation-list-left-right-other').data('relation-list-left-right-other').setNodeItem(relationNodeItem);
                //document.title = nodeItem.NameLeft + ' -> ' + nodeItem.NameRelationType + ' -> ' + nodeItem.NameRight;
                $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.dataSource.read();
            }
        };

        $(this).data('relation-list-left-right-other', relationListLeftRightOther);

        $.post($('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.actionComponentObjectRelationLeftRightOtherList, { idInstance: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.idInstance })
            .done(function (response) {
                console.log(response);



            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

        var gridParent = $('<div class="gridParent"></div>');

        relationListLeftRightOther.elements.gridParent = gridParent;
        relationListLeftRightOther.elements.relationListContainer.append(relationListLeftRightOther.elements.gridParent);

        relationListLeftRightOther.elements.dialogAddToClipboardWindow = $('<div class="add-to-clipboard-window"></div>');
        $(this).append(relationListLeftRightOther.elements.dialogAddToClipboardWindow);

        var grid = $('<div class="grid"></div>');

        relationListLeftRightOther.elements.grid = grid;
        relationListLeftRightOther.elements.gridParent.append(relationListLeftRightOther.elements.grid);

        $.post(relationListLeftRightOther.configuration.actionGetGridConfig)
            .done(function (response) {
                if ($('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.useIdParent) {
                    $.extend(response.dataSource.transport.read, {
                        data: function () {
                            if ($('.relation-list-left-right-other').data('relation-list-left-right-other').nodeItem !== undefined) {
                                return {
                                    IdLeft: $('.relation-list-left-right-other').data('relation-list-left-right-other').nodeItem.IdLeft,
                                    NameLeft: $('.relation-list-left-right-other').data('relation-list-left-right-other').nodeItem.NameLeft,
                                    IdRight: $('.relation-list-left-right-other').data('relation-list-left-right-other').nodeItem.IdRight,
                                    NameRight: $('.relation-list-left-right-other').data('relation-list-left-right-other').nodeItem.NameRight,
                                    IdRelationType: $('.relation-list-left-right-other').data('relation-list-left-right-other').nodeItem.IdRelationType,
                                    NameRelationType: $('.relation-list-left-right-other').data('relation-list-left-right-other').nodeItem.NameRelationType,
                                    NodeType: $('.relation-list-left-right-other').data('relation-list-left-right-other').nodeItem.NodeType,
                                    IdInstance: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.idInstance
                                };
                            } else {
                                return {
                                    IdInstance: $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.idInstance
                                }
                            }

                        }
                    })
                }

                response.dataSource.transport.parameterMap = function (data, type) {
                    if (type == "destroy") {
                        var deleteItems = {
                            models: []
                        };
                        data.models.forEach(function (item) {
                            deleteItems.models.push({ IdRelationType: item.IdRelationType, IdObject: item.IdObject, IdOther: item.IdOther });
                        });
                        return deleteItems;
                    }
                    if (type == "read") {
                        return data;
                    }
                    if (type == "update") {
                        var updateItems = {
                            models: []
                        };
                        data.models.forEach(function (item) {
                            updateItems.models.push({ IdRelationType: item.IdRelationType, IdObject: item.IdObject, IdOther: item.IdOther, NameOther: item.NameOther, OrderId: item.OrderId });
                        });
                        return updateItems;
                    }
                };

                $.extend(response.dataSource, {
                    batch: true
                });

                $.extend(response.dataSource.transport.update,
                    {
                        complete: function (e) {
                            console.log(e);
                        }
                    }
                );

                $.extend(response.dataSource.transport.destroy,
                    {
                        complete: function (e) {
                            console.log(e);
                        }
                    }
                );

                $.extend(response.dataSource.transport.read,
                    {
                        complete: function (e) {
                            console.log(e);
                        }
                    }
                );

                var dataSource = new kendo.data.DataSource(response.dataSource);
                response.dataSource = dataSource;
                response.dataBound = function () {
                    if ($('.relation-list-left-right-other').data('relation-list-left-right-other').dataBoundHandler !== undefined) {
                        $('.relation-list-left-right-other').data('relation-list-left-right-other').dataBoundHandler();
                    }
                    $('.relation-list-left-right-other').find('.chkbxApply').change(function () {

                        var row = $(this).closest('tr');
                        var item = $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.dataItem(row);
                        var applied = $(this).prop("checked");
                        item.Apply = applied;
                        if (item.Apply) {
                            $('.relation-list-left-right-other').data('relation-list-left-right-other').addApplyItem(item);
                        } else {
                            $('.relation-list-left-right-other').data('relation-list-left-right-other').removeApplyItem(item);
                        }

                        console.log(item);
                    });

                    if ($('.relation-list-left-right-other').find('.k-button').data('kendoButton') === undefined) {
                        $('.relation-list-left-right-other').find('.k-button').kendoButton({
                            click: $('.relation-list-left-right-other').data('relation-list-left-right-other').clickButtonHandler
                        });
                    }

                    $('.relation-list-left-right-other').find('.button-object-edit-left').click(function () {
                        var row = $(this).closest('tr');
                        var item = $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.dataItem(row);
                        var url = $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.actionRelationEditor;
                        url += "?Object=" + item.IdOther;
                        window.open(url, "_blank");
                    });

                    $('.relation-list-left-right-other').find('.button-module-starter-left').click(function () {
                        var row = $(this).closest('tr');
                        var item = $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.dataItem(row);
                        var url = $('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.actionModuleStarter;
                        url += "?Object=" + item.IdOther;
                        window.open(url, "_blank");
                    });

                };
                $.extend(response, {
                    change: $('.relation-list-left-right-other').data('relation-list-left-right-other').gridChangeHandler,
                    filterable: {
                        mode: "row"
                    },
                    cellClose: function (e) {
                        e.model.IsSaved = false;
                        $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.saveChanges();
                    }
                });
                $.extend(response, { toolbar: kendo.template($("#templateRelationList").html()) });
                $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem = $('.relation-list-left-right-other').find('.grid').kendoGrid(response).data("kendoGrid");


                $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.one("dataBound", function (e) {
                    var filterCell = e.sender.thead.find(".k-filtercell[data-field='NameObject']");
                    if (filterCell.length !== 0) {
                        filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                        filterDropDown.select(3);
                        filterDropDown.trigger("change");
                    }

                    filterCell = e.sender.thead.find(".k-filtercell[data-field='NameOther']");
                    if (filterCell.length !== 0) {
                        filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                        filterDropDown.select(3);
                        filterDropDown.trigger("change");
                    }
                });

                $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.wrapper.find(".k-pager")
                    .before('<a href="#" class="k-link prev">Prev</a>')
                    .after('<a href="#" class="k-link next">Next</a>')
                    .parent()
                    .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                    .delegate("a.prev", "click", function () {
                        dataSource.page(dataSource.page() - 1);
                    })
                    .delegate("a.next", "click", function () {
                        dataSource.page(dataSource.page() + 1);
                    })
                    .delegate(".pagesize input", "change", function () {
                        dataSource.page(parseInt(this.value, 10));
                    })

                $('.relation-list-left-right-other').data('relation-list-left-right-other').elements.grid.find('.header-checkbox').change(function (ev) {
                    var checked = ev.target.checked;
                    $('.relation-list-left-right-other').find('.chkbxApply').each(function (idx, item) {
                        $(item).click();
                    });
                });

                $('body').closePageLoader();


                if (!$('.relation-list-left-right-other').data('relation-list-left-right-other').configuration.useIdParent) {
                    $('.relation-list-left-right-other').data('relation-list-left-right-other').gridItem.dataSource.read();
                }

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

    };

    $.fn.createRelationListRightLeft = function (configuration) {
        var relationListRightLeft = {
            configuration: configuration,
            elements: {
                relationListContainer: $(this),
                gridParent: null,
                grid: null,
                dialogAddToClipboardWindow: null
            },
            gridItem: null,
            nodeItem: null,
            currentDataItem: null,
            _itemsPreApply: [],
            addApplyItem: function (item) {
                $('.relation-list-right-left').data('relation-list-right-left')._itemsPreApply.push(item);
                $('.relation-list-right-left').data('relation-list-right-left').checkViewState();
            },
            removeApplyItem: function (item) {
                $('.relation-list-right-left').data('relation-list-right-left')._itemsPreApply = jQuery.grep($('.relation-list-right-left').data('relation-list-right-left')._itemsPreApply, function (itemInArray) {
                    return itemInArray.IdItem !== item.IdItem
                });

                $('.relation-list-right-left').data('relation-list-right-left').checkViewState();
            },
            setNodeItem: function (nodeItem) {
                $('.relation-list-right-left').data('relation-list-right-left').nodeItem = nodeItem;
                $('.relation-list-right-left').data('relation-list-right-left').checkViewState();
            },
            checkViewState: function () {
                $('.relation-list-right-left').find('.newItem').setEnabled($('.relation-list-right-left').data('relation-list-right-left')._idParent !== undefined, true);
                $('.relation-list-right-left').find('.applyItem').setEnabled($('.relation-list-right-left').data('relation-list-right-left')._itemsPreApply.length > 0, true);
                $('.relation-list-right-left').find('.addToClipboard').setEnabled($('.relation-list-right-left').data('relation-list-right-left')._itemsPreApply.length > 0, true);
                $('.relation-list-right-left').find('.deleteItem').setEnabled($('.relation-list-right-left').data('relation-list-right-left')._itemsPreApply.length > 0, true);
                $('.relation-list-right-left').find('.orderAsc').setEnabled($('.relation-list-right-left').data('relation-list-right-left')._itemsPreApply.length > 0, true);
                $('.relation-list-right-left').find('.orderDesc').setEnabled($('.relation-list-right-left').data('relation-list-right-left')._itemsPreApply.length > 0, true);

            },
            dataBoundHandler: function () {
            },
            gridChangeHandler: function (e) {
                $('.relation-list-right-left').data('relation-list-right-left').currentDataItem = $('.relation-list-right-left').data('relation-list-right-left').gridItem.dataItem(this.select());
                var selectedItem = {
                    IdInstance: $('.relation-list-right-left').data('relation-list-right-left').configuration.idInstance,
                    IdObject: ""
                };

                if ($('.relation-list-right-left').data('relation-list-right-left').currentDataItem !== undefined) {
                    selectedItem = {
                        IdInstance: $('.relation-list-right-left').data('relation-list-right-left').configuration.idInstance,
                        IdObject: $('.relation-list-right-left').data('relation-list-right-left').currentDataItem.IdObject,
                        IdOther: $('.relation-list-right-left').data('relation-list-right-left').currentDataItem.IdOther,
                        IdRelationType: $('.relation-list-right-left').data('relation-list-right-left').currentDataItem.IdRelationType,
                        OrderId: $('.relation-list-right-left').data('relation-list-right-left').currentDataItem.OrderId
                    };

                    console.log($('.relation-list-right-left').data('relation-list-right-left').currentDataItem);
                    var items = [
                        {
                            GUID: $('.relation-list-right-left').data('relation-list-right-left').currentDataItem.IdObject
                        }];
                    $('.relation-list-right-left').data('relation-list-right-left').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: $('.relation-list-right-left').data('relation-list-right-left').configuration.channelSelectedItem,
                        SenderId: $('.relation-list-right-left').data('relation-list-right-left').configuration.managerConfig.idEndpoint,
                        SessionId: $('.relation-list-right-left').data('relation-list-right-left').configuration.managerConfig.idSession,
                        UserId: $('.relation-list-right-left').data('relation-list-right-left').configuration.idUser,
                        OItems: items
                    });
                }

                $.post($('.relation-list-right-left').data('relation-list-right-left').actionSetSelectedItem, selectedItem)
                    .done(function (response) {
                        console.log(response);

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            },
            clickButtonHandler: function (e) {
                var button = $(e.event.target).closest(".k-button");
                if (button.hasClass('applyItem')) {
                    var items = [];
                    $('.relation-list-right-left').find('.chkbxApply:checked').closest('tr').each(function () {
                        var row = $(this);
                        var item = $('.relation-list-right-left').data('relation-list-right-left').gridItem.dataItem(row);
                        console.log(item);
                        items.push({ GUID: item.IdObject });
                    })

                    console.log(items);
                    $('.relation-list-right-left').data('relation-list-right-left').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: $('.relation-list-right-left').data('relation-list-right-left').configuration.channelAppliedObjects,
                        SenderId: $('.relation-list-right-left').data('relation-list-right-left').configuration.managerConfig.idEndpoint,
                        SessionId: $('.relation-list-right-left').data('relation-list-right-left').configuration.managerConfig.idSession,
                        UserId: $('.relation-list-right-left').data('relation-list-right-left').configuration.idUser,
                        OItems: items
                    });

                } else if (button.hasClass('isListen')) {
                    button.toggleMode(true);
                } else if (button.hasClass('orderAsc')) {
                    var value = button.parent().find('.inpOrderId').getContent();
                    items = [];
                    $('.relation-list-right-left').find('.chkbxApply:checked').closest('tr').each(function () {
                        var row = $(this);
                        var item = $('.relation-list-right-left').data('relation-list-right-left').gridItem.dataItem(row);
                        console.log(item);
                        items.push({
                            uid: item.uid,
                            IdInstance: $('.relation-list-right-left').data('relation-list-right-left').configuration.idInstance,
                            IdItem: item.IdItem,
                            IdObject: item.IdObject,
                            IdOther: item.IdOther,
                            IdRelationType: item.IdRelationType,
                            OrderId: value++
                        });
                    })

                    $.post($('.relation-list-right-left').data('relation-list-right-left').configuration.actionSetOrderId, { requestList: items })
                        .done(function (response) {
                            console.log(response);
                            if (response.IsSuccessful) {
                                for (var ix in items) {
                                    var item = items[ix];
                                    var row = $('.relation-list-right-left').data('relation-list-right-left').gridItem.tbody.find("tr[data-uid='" + item.uid + "']");
                                    var dataItem = $('.relation-list-right-left').data('relation-list-right-left').gridItem.dataItem(row);
                                    dataItem.set("OrderId", item.OrderId);

                                }
                            }


                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });


                } else if (button.hasClass('deleteItem')) {
                    var deleteItems = [];
                    if ($('.relation-list-right-left').find('.chkbxApply:checked').closest('tr').length > 0 && confirm("Do you want to delete checked rows?")) {
                        $('.relation-list-right-left').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var dataItem = $('.relation-list-right-left').data('relation-list-right-left').gridItem.dataItem(row);
                            deleteItems.push(dataItem);

                            //$('.relation-list-left-right').data('relation-list-left-right').gridItem.removeRow(row);

                        });

                        for (var ix in deleteItems) {
                            var item = deleteItems[ix];
                            $('.relation-list-right-left').data('relation-list-right-left').gridItem.dataSource.remove(item);
                        }

                        $('.relation-list-right-left').data('relation-list-right-left').gridItem.dataSource.sync();
                    }

                } else if (button.hasClass('addToClipboard')) {
                    var clipboardItems = [];
                    if ($('.relation-list-right-left').find('.chkbxApply:checked').closest('tr').length > 0) {
                        $('.relation-list-right-left').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var dataItem = $('.relation-list-right-left').data('relation-list-right-left').gridItem.dataItem(row);
                            var clipboardItem = {
                                GUID: dataItem.IdObject,
                                Name: dataItem.NameObject,
                                GUID_Parent: dataItem.IdParentObject,
                                Type: dataItem.Ontology
                            };
                            clipboardItems.push(clipboardItem);

                        });

                        $.post($('.relation-list-right-left').data('relation-list-right-left').configuration.actionAddToClipboard, { oItems: clipboardItems, idInstance: $('.relation-list-right-left').data('relation-list-right-left').configuration.idInstance })
                            .done(function (response) {

                                $('.relation-list-right-left').data('relation-list-right-left').elements.dialogAddToClipboardWindow.kendoDialog({
                                    width: "450px",
                                    title: "Add to Clipboard Result",
                                    closable: false,
                                    modal: false,
                                    content: "<p>Added " + response.Result.length + " Items<p>",
                                    actions: [
                                        { text: 'OK', primary: true }
                                    ],
                                    close: function (e) {

                                    }
                                });
                                $('.relation-list-right-left').data('relation-list-right-left').elements.dialogAddToClipboardWindow.data('kendoDialog').open();
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });
                    }
                }


            },
            validateReference: function (relationNodeItem) {
                $('.relation-list-right-left').data('relation-list-right-left').setNodeItem(relationNodeItem);
                //document.title = nodeItem.NameLeft + ' -> ' + nodeItem.NameRelationType + ' -> ' + nodeItem.NameRight;
                $('.relation-list-right-left').data('relation-list-right-left').gridItem.dataSource.read();
            }
        };

        $(this).data('relation-list-right-left', relationListRightLeft);

        $.post($('.relation-list-right-left').data('relation-list-right-left').configuration.actionComponentObjectRelationRightLeftList, { idInstance: $('.relation-list-right-left').data('relation-list-right-left').configuration.idInstance })
            .done(function (response) {
                console.log(response);
                


            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

        var gridParent = $('<div class="gridParent"></div>');

        relationListRightLeft.elements.gridParent = gridParent;
        relationListRightLeft.elements.relationListContainer.append(relationListRightLeft.elements.gridParent);

        relationListRightLeft.elements.dialogAddToClipboardWindow = $('<div class="add-to-clipboard-window"></div>');
        $(this).append(relationListRightLeft.elements.dialogAddToClipboardWindow);

        var grid = $('<div class="grid"></div>');

        relationListRightLeft.elements.grid = grid;
        relationListRightLeft.elements.gridParent.append(relationListRightLeft.elements.grid);

        $.post(relationListRightLeft.configuration.actionGetGridConfig)
            .done(function (response) {
                if ($('.relation-list-right-left').data('relation-list-right-left').configuration.useIdParent) {
                    $.extend(response.dataSource.transport.read, {
                        data: function () {
                            if ($('.relation-list-right-left').data('relation-list-right-left').nodeItem !== undefined) {
                                return {
                                    IdLeft: $('.relation-list-right-left').data('relation-list-right-left').nodeItem.IdLeft,
                                    NameLeft: $('.relation-list-right-left').data('relation-list-right-left').nodeItem.NameLeft,
                                    IdRight: $('.relation-list-right-left').data('relation-list-right-left').nodeItem.IdRight,
                                    NameRight: $('.relation-list-right-left').data('relation-list-right-left').nodeItem.NameRight,
                                    IdRelationType: $('.relation-list-right-left').data('relation-list-right-left').nodeItem.IdRelationType,
                                    NameRelationType: $('.relation-list-right-left').data('relation-list-right-left').nodeItem.NameRelationType,
                                    NodeType: $('.relation-list-right-left').data('relation-list-right-left').nodeItem.NodeType,
                                    IdInstance: $('.relation-list-right-left').data('relation-list-right-left').configuration.idInstance
                                };
                            } else {
                                return {
                                    IdInstance: $('.relation-list-right-left').data('relation-list-right-left').configuration.idInstance
                                }
                            }

                        }
                    })
                }

                $.extend(response.dataSource, {
                    batch: true
                });

                response.dataSource.transport.parameterMap = function (data, type) {
                    if (type == "destroy") {
                        var deleteItems = {
                            models: []
                        };
                        data.models.forEach(function (item) {
                            deleteItems.models.push({ IdRelationType: item.IdRelationType, IdObject: item.IdObject, IdOther: item.IdOther });
                        });
                        return deleteItems;
                    }
                    if (type == "read") {
                        return data;
                    }
                    if (type == "update") {
                        var updateItems = {
                            models: []
                        };
                        data.models.forEach(function (item) {
                            updateItems.models.push({ IdRelationType: item.IdRelationType, NameObject: item.NameObject, IdObject: item.IdObject, IdOther: item.IdOther, OrderId: item.OrderId });
                        });
                        return updateItems;
                    }
                };

                $.extend(response.dataSource.transport.update,
                    {
                        complete: function (e) {
                            console.log(e);
                            
                        }
                    }
                );

                var dataSource = new kendo.data.DataSource(response.dataSource);
                response.dataSource = dataSource;
                response.dataBound = function () {
                    if ($('.relation-list-right-left').data('relation-list-right-left').dataBoundHandler !== undefined) {
                        $('.relation-list-right-left').data('relation-list-right-left').dataBoundHandler();
                    }
                    $('.relation-list-right-left').find('.chkbxApply').change(function () {

                        var row = $(this).closest('tr');
                        var item = $('.relation-list-right-left').data('relation-list-right-left').gridItem.dataItem(row);
                        var applied = $(this).prop("checked");
                        item.Apply = applied;
                        if (item.Apply) {
                            $('.relation-list-right-left').data('relation-list-right-left').addApplyItem(item);
                        } else {
                            $('.relation-list-right-left').data('relation-list-right-left').removeApplyItem(item);
                        }

                        console.log(item);
                    });

                    $('.relation-list-right-left').find('.button-object-edit-right').click(function () {
                        var row = $(this).closest('tr');
                        var item = $('.relation-list-right-left').data('relation-list-right-left').gridItem.dataItem(row);
                        var url = $('.relation-list-right-left').data('relation-list-right-left').configuration.actionRelationEditor;
                        url += "?Object=" + item.IdObject;
                        window.open(url, "_blank");
                    });

                    $('.relation-list-right-left').find('.button-module-starter-right').click(function () {
                        var row = $(this).closest('tr');
                        var item = $('.relation-list-right-left').data('relation-list-right-left').gridItem.dataItem(row);
                        var url = $('.relation-list-right-left').data('relation-list-right-left').configuration.actionModuleStarter;
                        url += "?Object=" + item.IdObject;
                        window.open(url, "_blank");
                    });

                    if ($('.relation-list-right-left').find('.k-button').data('kendoButton') === undefined) {
                        $('.relation-list-right-left').find('.k-button').kendoButton({
                            click: $('.relation-list-right-left').data('relation-list-right-left').clickButtonHandler
                        });
                    }

                };
                $.extend(response, {
                    change: $('.relation-list-right-left').data('relation-list-right-left').gridChangeHandler,
                    filterable: {
                        mode: "row"
                    },
                    cellClose: function (e) {
                        e.model.IsSaved = false;
                        $('.relation-list-right-left').data('relation-list-right-left').gridItem.saveChanges();
                    }
                });
                $.extend(response, { toolbar: kendo.template($("#templateRelationList").html()) });
                $('.relation-list-right-left').data('relation-list-right-left').gridItem = $('.relation-list-right-left').find('.grid').kendoGrid(response).data("kendoGrid");
                

                $('.relation-list-right-left').data('relation-list-right-left').gridItem.one("dataBound", function (e) {
                    var filterCell = e.sender.thead.find(".k-filtercell[data-field='NameObject']");
                    if (filterCell.length !== 0) {
                        var filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                        filterDropDown.select(3);
                        filterDropDown.trigger("change");
                    }

                    filterCell = e.sender.thead.find(".k-filtercell[data-field='NameOther']");
                    if (filterCell.length !== 0) {
                        filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                        filterDropDown.select(3);
                        filterDropDown.trigger("change");
                    }
                });

                $('.relation-list-right-left').data('relation-list-right-left').gridItem.wrapper.find(".k-pager")
                    .before('<a href="#" class="k-link prev">Prev</a>')
                    .after('<a href="#" class="k-link next">Next</a>')
                    .parent()
                    .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                    .delegate("a.prev", "click", function () {
                        dataSource.page(dataSource.page() - 1);
                    })
                    .delegate("a.next", "click", function () {
                        dataSource.page(dataSource.page() + 1);
                    })
                    .delegate(".pagesize input", "change", function () {
                        dataSource.page(parseInt(this.value, 10));
                    })

                $('.relation-list-right-left').data('relation-list-right-left').elements.grid.find('.header-checkbox').change(function (ev) {
                    var checked = ev.target.checked;
                    $('.relation-list-right-left').find('.chkbxApply').each(function (idx, item) {
                        $(item).click();
                    });
                });

                $('body').closePageLoader();


                if (!$('.relation-list-right-left').data('relation-list-right-left').configuration.useIdParent) {
                    $('.relation-list-right-left').data('relation-list-right-left').gridItem.dataSource.read();
                }

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

    };

    $.fn.createRelationListRightLeftOther = function (configuration) {
        var relationListRightLeft = {
            configuration: configuration,
            elements: {
                relationListContainer: $(this),
                gridParent: null,
                grid: null,
                dialogAddToClipboardWindow: null
            },
            gridItem: null,
            nodeItem: null,
            currentDataItem: null,
            _itemsPreApply: [],
            addApplyItem: function (item) {
                $('.relation-list-right-left-other').data('relation-list-right-left-other')._itemsPreApply.push(item);
                $('.relation-list-right-left-other').data('relation-list-right-left-other').checkViewState();
            },
            removeApplyItem: function (item) {
                $('.relation-list-right-left-other').data('relation-list-right-left-other')._itemsPreApply = jQuery.grep($('.relation-list-right-left-other').data('relation-list-right-left-other')._itemsPreApply, function (itemInArray) {
                    return itemInArray.IdItem !== item.IdItem
                });

                $('.relation-list-right-left-other').data('relation-list-right-left-other').checkViewState();
            },
            setNodeItem: function (nodeItem) {
                $('.relation-list-right-left-other').data('relation-list-right-left-other').nodeItem = nodeItem;
                $('.relation-list-right-left-other').data('relation-list-right-left-other').checkViewState();
            },
            checkViewState: function () {
                $('.relation-list-right-left-other').find('.newItem').setEnabled($('.relation-list-right-left-other').data('relation-list-right-left-other')._idParent !== undefined, true);
                $('.relation-list-right-left-other').find('.applyItem').setEnabled($('.relation-list-right-left-other').data('relation-list-right-left-other')._itemsPreApply.length > 0, true);
                $('.relation-list-right-left-other').find('.addToClipboard').setEnabled($('.relation-list-right-left-other').data('relation-list-right-left-other')._itemsPreApply.length > 0, true);
                $('.relation-list-right-left-other').find('.deleteItem').setEnabled($('.relation-list-right-left-other').data('relation-list-right-left-other')._itemsPreApply.length > 0, true);
                $('.relation-list-right-left-other').find('.orderAsc').setEnabled($('.relation-list-right-left-other').data('relation-list-right-left-other')._itemsPreApply.length > 0, true);
                $('.relation-list-right-left-other').find('.orderDesc').setEnabled($('.relation-list-right-left-other').data('relation-list-right-left-other')._itemsPreApply.length > 0, true);

            },
            dataBoundHandler: function () {
            },
            gridChangeHandler: function (e) {
                $('.relation-list-right-left-other').data('relation-list-right-left-other').currentDataItem = $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.dataItem(this.select());
                var selectedItem = {
                    IdInstance: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.idInstance,
                    IdObject: ""
                };

                if ($('.relation-list-right-left-other').data('relation-list-right-left-other').currentDataItem !== undefined) {
                    selectedItem = {
                        IdInstance: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.idInstance,
                        IdObject: $('.relation-list-right-left-other').data('relation-list-right-left-other').currentDataItem.IdObject,
                        IdOther: $('.relation-list-right-left-other').data('relation-list-right-left-other').currentDataItem.IdOther,
                        IdRelationType: $('.relation-list-right-left-other').data('relation-list-right-left-other').currentDataItem.IdRelationType,
                        OrderId: $('.relation-list-right-left-other').data('relation-list-right-left-other').currentDataItem.OrderId
                    };

                    console.log($('.relation-list-right-left-other').data('relation-list-right-left-other').currentDataItem);
                    var items = [
                        {
                            GUID: $('.relation-list-right-left-other').data('relation-list-right-left-other').currentDataItem.IdObject
                        }];
                    $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.channelSelectedItem,
                        SenderId: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.managerConfig.idEndpoint,
                        SessionId: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.managerConfig.idSession,
                        UserId: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.idUser,
                        OItems: items
                    });
                }

                $.post($('.relation-list-right-left-other').data('relation-list-right-left-other').actionSetSelectedItem, selectedItem)
                    .done(function (response) {
                        console.log(response);

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            },
            clickButtonHandler: function (e) {
                var button = $(e.event.target).closest(".k-button");
                if (button.hasClass('applyItem')) {
                    var items = [];
                    $('.relation-list-right-left-other').find('.chkbxApply:checked').closest('tr').each(function () {
                        var row = $(this);
                        var item = $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.dataItem(row);
                        console.log(item);
                        items.push({ GUID: item.IdObject });
                    })

                    console.log(items);
                    $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.channelAppliedItems,
                        SenderId: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.managerConfig.idEndpoint,
                        SessionId: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.managerConfig.idSession,
                        UserId: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.idUser,
                        OItems: items
                    });

                } else if (button.hasClass('isListen')) {
                    button.toggleMode(true);
                } else if (button.hasClass('orderAsc')) {
                    var value = button.parent().find('.inpOrderId').getContent();
                    items = [];
                    $('.relation-list-right-left-other').find('.chkbxApply:checked').closest('tr').each(function () {
                        var row = $(this);
                        var item = $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.dataItem(row);
                        console.log(item);
                        items.push({
                            uid: item.uid,
                            IdInstance: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.idInstance,
                            IdItem: item.IdItem,
                            IdObject: item.IdObject,
                            IdOther: item.IdOther,
                            IdRelationType: item.IdRelationType,
                            OrderId: value++
                        });
                    })

                    $.post($('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.actionSetOrderId, { requestList: items })
                        .done(function (response) {
                            console.log(response);
                            if (response.IsSuccessful) {
                                for (var ix in items) {
                                    var item = items[ix];
                                    var row = $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.tbody.find("tr[data-uid='" + item.uid + "']");
                                    var dataItem = $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.dataItem(row);
                                    dataItem.set("OrderId", item.OrderId);

                                }
                            }


                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });


                } else if (button.hasClass('deleteItem')) {
                    var deleteItems = [];
                    if ($('.relation-list-right-left-other').find('.chkbxApply:checked').closest('tr').length > 0 && confirm("Do you want to delete checked rows?")) {
                        $('.relation-list-right-left-other').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var dataItem = $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.dataItem(row);
                            deleteItems.push(dataItem);

                            //$('.relation-list-left-right').data('relation-list-left-right').gridItem.removeRow(row);

                        });

                        for (var ix in deleteItems) {
                            var item = deleteItems[ix];
                            $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.dataSource.remove(item);
                        }

                        $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.dataSource.sync();
                    }

                } else if (button.hasClass('addToClipboard')) {
                    var clipboardItems = [];
                    if ($('.relation-list-right-left-other').find('.chkbxApply:checked').closest('tr').length > 0) {
                        $('.relation-list-right-left-other').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var dataItem = $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.dataItem(row);
                            var clipboardItem = {
                                GUID: dataItem.IdObject,
                                Name: dataItem.NameObject,
                                GUID_Parent: dataItem.IdParentObject,
                                Type: dataItem.Ontology
                            };
                            clipboardItems.push(clipboardItem);

                        });

                        $.post($('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.actionAddToClipboard, { oItems: clipboardItems, idInstance: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.idInstance })
                            .done(function (response) {

                                $('.relation-list-right-left-other').data('relation-list-right-left-other').elements.dialogAddToClipboardWindow.kendoDialog({
                                    width: "450px",
                                    title: "Add to Clipboard Result",
                                    closable: false,
                                    modal: false,
                                    content: "<p>Added " + response.Result.length + " Items<p>",
                                    actions: [
                                        { text: 'OK', primary: true }
                                    ],
                                    close: function (e) {

                                    }
                                });
                                $('.relation-list-right-left-other').data('relation-list-right-left-other').elements.dialogAddToClipboardWindow.data('kendoDialog').open();
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });
                    }
                }


            },
            validateReference: function (relationNodeItem) {
                $('.relation-list-right-left-other').data('relation-list-right-left-other').setNodeItem(relationNodeItem);
                //document.title = nodeItem.NameLeft + ' -> ' + nodeItem.NameRelationType + ' -> ' + nodeItem.NameRight;
                $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.dataSource.read();
            }
        };

        $(this).data('relation-list-right-left-other', relationListRightLeft);

        $.post($('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.actionComponentObjectRelationRightLeftList, { idInstance: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.idInstance })
            .done(function (response) {
                console.log(response);



            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

        var gridParent = $('<div class="gridParent"></div>');

        relationListRightLeft.elements.gridParent = gridParent;
        relationListRightLeft.elements.relationListContainer.append(relationListRightLeft.elements.gridParent);

        relationListRightLeft.elements.dialogAddToClipboardWindow = $('<div class="add-to-clipboard-window"></div>');
        $(this).append(relationListRightLeft.elements.dialogAddToClipboardWindow);

        var grid = $('<div class="grid"></div>');

        relationListRightLeft.elements.grid = grid;
        relationListRightLeft.elements.gridParent.append(relationListRightLeft.elements.grid);

        $.post(relationListRightLeft.configuration.actionGetGridConfig)
            .done(function (response) {
                if ($('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.useIdParent) {
                    $.extend(response.dataSource.transport.read, {
                        data: function () {
                            if ($('.relation-list-right-left-other').data('relation-list-right-left-other').nodeItem !== undefined) {
                                return {
                                    IdLeft: $('.relation-list-right-left-other').data('relation-list-right-left-other').nodeItem.IdLeft,
                                    NameLeft: $('.relation-list-right-left-other').data('relation-list-right-left-other').nodeItem.NameLeft,
                                    IdRight: $('.relation-list-right-left-other').data('relation-list-right-left-other').nodeItem.IdRight,
                                    NameRight: $('.relation-list-right-left-other').data('relation-list-right-left-other').nodeItem.NameRight,
                                    IdRelationType: $('.relation-list-right-left-other').data('relation-list-right-left-other').nodeItem.IdRelationType,
                                    NameRelationType: $('.relation-list-right-left-other').data('relation-list-right-left-other').nodeItem.NameRelationType,
                                    NodeType: $('.relation-list-right-left-other').data('relation-list-right-left-other').nodeItem.NodeType,
                                    IdInstance: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.idInstance
                                };
                            } else {
                                return {
                                    IdInstance: $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.idInstance
                                }
                            }

                        }
                    })
                }

                $.extend(response.dataSource, {
                    batch: true
                });

                response.dataSource.transport.parameterMap = function (data, type) {
                    if (type == "destroy") {
                        var deleteItems = {
                            models: []
                        };
                        data.models.forEach(function (item) {
                            deleteItems.models.push({ IdRelationType: item.IdRelationType, IdObject: item.IdObject, IdOther: item.IdOther });
                        });
                        return deleteItems;
                    }
                    if (type == "read") {
                        return data;
                    }
                    if (type == "update") {
                        var updateItems = {
                            models: []
                        };
                        data.models.forEach(function (item) {
                            updateItems.models.push({ IdRelationType: item.IdRelationType, IdObject: item.IdObject, NameObject: item.NameObject, IdOther: item.IdOther, OrderId: item.OrderId });
                        });
                        return updateItems;
                    }
                };

                $.extend(response.dataSource.transport.update,
                    {
                        complete: function (e) {
                            console.log(e);
                            
                        }
                    }
                );

                var dataSource = new kendo.data.DataSource(response.dataSource);
                response.dataSource = dataSource;
                response.dataBound = function () {
                    if ($('.relation-list-right-left-other').data('relation-list-right-left-other').dataBoundHandler !== undefined) {
                        $('.relation-list-right-left-other').data('relation-list-right-left-other').dataBoundHandler();
                    }
                    $('.relation-list-right-left-other').find('.chkbxApply').change(function () {

                        var row = $(this).closest('tr');
                        var item = $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.dataItem(row);
                        var applied = $(this).prop("checked");
                        item.Apply = applied;
                        if (item.Apply) {
                            $('.relation-list-right-left-other').data('relation-list-right-left-other').addApplyItem(item);
                        } else {
                            $('.relation-list-right-left-other').data('relation-list-right-left-other').removeApplyItem(item);
                        }

                        console.log(item);
                    });

                    $('.relation-list-right-left-other').find('.button-object-edit-right').click(function () {
                        var row = $(this).closest('tr');
                        var item = $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.dataItem(row);
                        var url = $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.actionRelationEditor;
                        url += "?Object=" + item.IdObject;
                        window.open(url, "_blank");
                    });

                    $('.relation-list-right-left-other').find('.button-module-starter-right').click(function () {
                        var row = $(this).closest('tr');
                        var item = $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.dataItem(row);
                        var url = $('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.actionModuleStarter;
                        url += "?Object=" + item.IdObject;
                        window.open(url, "_blank");
                    });

                    if ($('.relation-list-right-left-other').find('.k-button').data('kendoButton') === undefined) {
                        $('.relation-list-right-left-other').find('.k-button').kendoButton({
                            click: $('.relation-list-right-left-other').data('relation-list-right-left-other').clickButtonHandler
                        });
                    }

                };
                $.extend(response, {
                    change: $('.relation-list-right-left-other').data('relation-list-right-left-other').gridChangeHandler,
                    filterable: {
                        mode: "row"
                    },
                    cellClose: function (e) {
                        e.model.IsSaved = false;
                        $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.saveChanges();
                    }
                });
                $.extend(response, { toolbar: kendo.template($("#templateRelationList").html()) });
                $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem = $('.relation-list-right-left-other').find('.grid').kendoGrid(response).data("kendoGrid");

                $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.one("dataBound", function (e) {
                    var filterCell = e.sender.thead.find(".k-filtercell[data-field='NameObject']");
                    if (filterCell.length !== 0) {
                        var filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                        filterDropDown.select(3);
                        filterDropDown.trigger("change");
                    }

                    filterCell = e.sender.thead.find(".k-filtercell[data-field='NameOther']");
                    if (filterCell.length !== 0) {
                        filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                        filterDropDown.select(3);
                        filterDropDown.trigger("change");
                    }
                });

                $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.wrapper.find(".k-pager")
                    .before('<a href="#" class="k-link prev">Prev</a>')
                    .after('<a href="#" class="k-link next">Next</a>')
                    .parent()
                    .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                    .delegate("a.prev", "click", function () {
                        dataSource.page(dataSource.page() - 1);
                    })
                    .delegate("a.next", "click", function () {
                        dataSource.page(dataSource.page() + 1);
                    })
                    .delegate(".pagesize input", "change", function () {
                        dataSource.page(parseInt(this.value, 10));
                    })

                $('.relation-list-right-left-other').data('relation-list-right-left-other').elements.grid.find('.header-checkbox').change(function (ev) {
                    var checked = ev.target.checked;
                    $('.relation-list-right-left-other').find('.chkbxApply').each(function (idx, item) {
                        $(item).click();
                    });
                });

                $('body').closePageLoader();


                if (!$('.relation-list-right-left-other').data('relation-list-right-left-other').configuration.useIdParent) {
                    $('.relation-list-right-left-other').data('relation-list-right-left-other').gridItem.dataSource.read();
                }

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

    };

    $.fn.createRelationListOmni = function (configuration) {
        var relationListOmni = {
            configuration: configuration,
            elements: {
                relationListContainer: $(this),
                gridParent: null,
                grid: null,
                dialogAddToClipboardWindow: null
            },
            gridItem: null,
            object: null,
            currentDataItem: null,
            _itemsPreApply: [],
            addApplyItem: function (item) {
                $('.relation-list-omni').data('relation-list-omni')._itemsPreApply.push(item);
                $('.relation-list-omni').data('relation-list-omni').checkViewState();
            },
            removeApplyItem: function (item) {
                $('.relation-list-omni').data('relation-list-omni')._itemsPreApply = jQuery.grep($('.relation-list-omni').data('relation-list-omni')._itemsPreApply, function (itemInArray) {
                    return itemInArray.IdItem !== item.IdItem;
                });

                $('.relation-list-omni').data('relation-list-omni').checkViewState();
            },
            setObject: function (object) {
                $('.relation-list-omni').data('relation-list-omni').object = object;
                $('.relation-list-omni').data('relation-list-omni').checkViewState();
            },
            checkViewState: function () {
                $('.relation-list-omni').find('.newItem').setEnabled($('.relation-list-omni').data('relation-list-omni')._idParent !== undefined, true);
                $('.relation-list-omni').find('.applyItem').setEnabled($('.relation-list-omni').data('relation-list-omni')._itemsPreApply.length > 0, true);
                $('.relation-list-omni').find('.addToClipboard').setEnabled($('.relation-list-omni').data('relation-list-omni')._itemsPreApply.length > 0, true);
                $('.relation-list-omni').find('.deleteItem').setEnabled($('.relation-list-omni').data('relation-list-omni')._itemsPreApply.length > 0, true);
                $('.relation-list-omni').find('.orderAsc').setEnabled($('.relation-list-omni').data('relation-list-omni')._itemsPreApply.length > 0, true);
                $('.relation-list-omni').find('.orderDesc').setEnabled($('.relation-list-omni').data('relation-list-omni')._itemsPreApply.length > 0, true);

            },
            dataBoundHandler: function () {
            },
            gridChangeHandler: function (e) {
                $('.relation-list-omni').data('relation-list-omni').currentDataItem = $('.relation-list-omni').data('relation-list-omni').gridItem.dataItem(this.select());
                var selectedItem = {
                    IdInstance: $('.relation-list-omni').data('relation-list-omni').configuration.idInstance,
                    IdObject: ""
                };

                if ($('.relation-list-omni').data('relation-list-omni').currentDataItem !== undefined) {
                    selectedItem = {
                        IdInstance: $('.relation-list-omni').data('relation-list-omni').configuration.idInstance,
                        IdObject: $('.relation-list-omni').data('relation-list-omni').currentDataItem.IdObject,
                        IdOther: $('.relation-list-omni').data('relation-list-omni').currentDataItem.IdOther,
                        IdRelationType: $('.relation-list-omni').data('relation-list-omni').currentDataItem.IdRelationType,
                        OrderId: $('.relation-list-omni').data('relation-list-omni').currentDataItem.OrderId
                    };

                    console.log($('.relation-list-omni').data('relation-list-omni').currentDataItem);
                    var items = [
                        {
                            GUID: $('.relation-list-omni').data('relation-list-omni').currentDataItem.IdOther
                        }];
                    $('.relation-list-omni').data('relation-list-omni').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: $('.relation-list-omni').data('relation-list-omni').configuration.channelSelectedItem,
                        SenderId: $('.relation-list-omni').data('relation-list-omni').configuration.managerConfig.idEndpoint,
                        SessionId: $('.relation-list-omni').data('relation-list-omni').configuration.managerConfig.idSession,
                        UserId: $('.relation-list-omni').data('relation-list-omni').configuration.idUser,
                        OItems: items
                    });
                }

                $.post($('.relation-list-omni').data('relation-list-omni').actionSetSelectedItem, selectedItem)
                    .done(function (response) {
                        console.log(response);

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            },
            clickButtonHandler: function (e) {
                var button = $(e.event.target).closest(".k-button");
                if (button.hasClass('applyItem')) {
                    var items = [];
                    $('.relation-list-omni').find('.chkbxApply:checked').closest('tr').each(function () {
                        var row = $(this);
                        var item = $('.relation-list-omni').data('relation-list-omni').gridItem.dataItem(row);
                        console.log(item);
                        items.push({ GUID: item.IdOther });
                    });

                    console.log(items);
                    $('.relation-list-omni').data('relation-list-omni').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: $('.relation-list-omni').data('relation-list-omni').configuration.channelAppliedItems,
                        SenderId: $('.relation-list-omni').data('relation-list-omni').configuration.managerConfig.idEndpoint,
                        SessionId: $('.relation-list-omni').data('relation-list-omni').configuration.managerConfig.idSession,
                        UserId: $('.relation-list-omni').data('relation-list-omni').configuration.idUser,
                        OItems: items
                    });

                } else if (button.hasClass('isListen')) {
                    button.toggleMode(true);
                } else if (button.hasClass('orderAsc')) {
                    var value = button.parent().find('.inpOrderId').getContent();
                    items = [];
                    $('.relation-list-omni').find('.chkbxApply:checked').closest('tr').each(function () {
                        var row = $(this);
                        var item = $('.relation-list-omni').data('relation-list-omni').gridItem.dataItem(row);
                        console.log(item);
                        items.push({
                            uid: item.uid,
                            IdInstance: $('.relation-list-omni').data('relation-list-omni').configuration.idInstance,
                            IdItem: item.IdItem,
                            IdObject: item.IdObject,
                            IdOther: item.IdOther,
                            IdRelationType: item.IdRelationType,
                            OrderId: value++
                        });
                    });

                    $.post($('.relation-list-omni').data('relation-list-omni').configuration.actionSetOrderId, { requestList: items })
                        .done(function (response) {
                            console.log(response);
                            if (response.IsSuccessful) {
                                for (var ix in items) {
                                    var item = items[ix];
                                    var row = $('.relation-list-omni').data('relation-list-omni').gridItem.tbody.find("tr[data-uid='" + item.uid + "']");
                                    var dataItem = $('.relation-list-omni').data('relation-list-omni').gridItem.dataItem(row);
                                    dataItem.set("OrderId", item.OrderId);

                                }
                            }


                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });


                } else if (button.hasClass('deleteItem')) {
                    var deleteItems = [];
                    if ($('.relation-list-omni').find('.chkbxApply:checked').closest('tr').length > 0 && confirm("Do you want to delete checked rows?")) {
                        $('.relation-list-omni').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var dataItem = $('.relation-list-omni').data('relation-list-omni').gridItem.dataItem(row);
                            deleteItems.push(dataItem);

                            //$('.relation-list-omni').data('relation-list-omni').gridItem.removeRow(row);

                        });

                        for (var ix in deleteItems) {
                            var item = deleteItems[ix];
                            $('.relation-list-omni').data('relation-list-omni').gridItem.dataSource.remove(item);
                        }

                        $('.relation-list-omni').data('relation-list-omni').gridItem.dataSource.sync();
                    }

                } else if (button.hasClass('addToClipboard')) {
                    var clipboardItems = [];
                    if ($('.relation-list-omni').find('.chkbxApply:checked').closest('tr').length > 0) {
                        $('.relation-list-omni').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var dataItem = $('.relation-list-omni').data('relation-list-omni').gridItem.dataItem(row);
                            var clipboardItem = {
                                GUID: dataItem.IdOther,
                                Name: dataItem.NameOther,
                                GUID_Parent: dataItem.IdParentOther,
                                Type: dataItem.Ontology
                            };
                            clipboardItems.push(clipboardItem);

                        });

                        $.post($('.relation-list-omni').data('relation-list-omni').configuration.actionAddToClipboard, { oItems: clipboardItems, idInstance: $('.relation-list-omni').data('relation-list-omni').configuration.idInstance })
                            .done(function (response) {

                                $('.relation-list-omni').data('relation-list-omni').elements.dialogAddToClipboardWindow.kendoDialog({
                                    width: "450px",
                                    title: "Add to Clipboard Result",
                                    closable: false,
                                    modal: false,
                                    content: "<p>Added " + response.Result.length + " Items<p>",
                                    actions: [
                                        { text: 'OK', primary: true }
                                    ],
                                    close: function (e) {

                                    }
                                });
                                $('.relation-list-omni').data('relation-list-omni').elements.dialogAddToClipboardWindow.data('kendoDialog').open();
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });
                    }
                }


            },
            validateReference: function (oItem) {
                $('.relation-list-omni').data('relation-list-omni').setObject(oItem);
                //document.title = nodeItem.NameLeft + ' -> ' + nodeItem.NameRelationType + ' -> ' + nodeItem.NameRight;
                $('.relation-list-omni').data('relation-list-omni').gridItem.dataSource.read();
            }
        };

        $(this).data('relation-list-omni', relationListOmni);

        $.post($('.relation-list-omni').data('relation-list-omni').configuration.actionComponentObjectRelationOmniList, { idInstance: $('.relation-list-omni').data('relation-list-omni').configuration.idInstance })
            .done(function (response) {
                console.log(response);



            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

        var gridParent = $('<div class="gridParent"></div>');

        relationListOmni.elements.gridParent = gridParent;
        relationListOmni.elements.relationListContainer.append(relationListOmni.elements.gridParent);

        relationListOmni.elements.dialogAddToClipboardWindow = $('<div class="add-to-clipboard-window"></div>');
        $(this).append(relationListOmni.elements.dialogAddToClipboardWindow);

        var grid = $('<div class="grid"></div>');

        relationListOmni.elements.grid = grid;
        relationListOmni.elements.gridParent.append(relationListOmni.elements.grid);

        $.post(relationListOmni.configuration.actionGetGridConfig)
            .done(function (response) {
                if ($('.relation-list-omni').data('relation-list-omni').configuration.useIdParent) {
                    $.extend(response.dataSource.transport.read, {
                        data: function () {
                            if ($('.relation-list-omni').data('relation-list-omni').object !== undefined) {
                                return {
                                    GUID: $('.relation-list-omni').data('relation-list-omni').object.GUID,
                                    Name: $('.relation-list-omni').data('relation-list-omni').object.Name,
                                    GUID_Parent: $('.relation-list-omni').data('relation-list-omni').object.GUID_Parent,
                                    Type: $('.relation-list-omni').data('relation-list-omni').object.Type,
                                    IdInstance: $('.relation-list-omni').data('relation-list-omni').configuration.idInstance
                                };
                            } else {
                                return {
                                    IdInstance: $('.relation-list-omni').data('relation-list-omni').configuration.idInstance
                                };
                            }

                        }
                    });
                }

                $.extend(response.dataSource, {
                    batch: true
                });

                response.dataSource.transport.parameterMap = function (data, type) {
                    if (type == "destroy") {
                        var deleteItems = {
                            models: []
                        };
                        data.models.forEach(function (item) {
                            deleteItems.models.push({ IdRelationType: item.IdRelationType, IdObject: item.IdObject, IdOther: item.IdOther });
                        });
                        return deleteItems;
                    }
                    if (type == "read") {
                        return data;
                    }
                    if (type == "update") {
                        var updateItems = {
                            models: []
                        };
                        data.models.forEach(function (item) {
                            updateItems.models.push({ IdRelationType: item.IdRelationType, IdObject: item.IdObject, NameObject: item.NameObject, IdOther: item.IdOther, NameOther: item.NameOther, OrderId: item.OrderId });
                        });
                        return updateItems;
                    }
                };

                $.extend(response.dataSource.transport.update,
                    {
                        complete: function (e) {
                            console.log(e);
                            
                        }
                    }
                );

                var dataSource = new kendo.data.DataSource(response.dataSource);
                response.dataSource = dataSource;
                response.dataBound = function () {
                    if ($('.relation-list-omni').data('relation-list-omni').dataBoundHandler !== undefined) {
                        $('.relation-list-omni').data('relation-list-omni').dataBoundHandler();
                    }
                    $('.relation-list-omni').find('.chkbxApply').change(function () {

                        var row = $(this).closest('tr');
                        var item = $('.relation-list-omni').data('relation-list-omni').gridItem.dataItem(row);
                        var applied = $(this).prop("checked");
                        item.Apply = applied;
                        if (item.Apply) {
                            $('.relation-list-omni').data('relation-list-omni').addApplyItem(item);
                        } else {
                            $('.relation-list-omni').data('relation-list-omni').removeApplyItem(item);
                        }

                        console.log(item);
                    });

                    $('.relation-list-omni').find('.button-object-edit-left-omni').click(function () {
                        var row = $(this).closest('tr');
                        var item = $('.relation-list-omni').data('relation-list-omni').gridItem.dataItem(row);
                        var url = $('.relation-list-omni').data('relation-list-omni').configuration.actionRelationEditor;
                        url += "?Object=" + item.IdObject;
                        window.open(url, "_blank");
                    });

                    $('.relation-list-omni').find('.button-module-starter-left-omni').click(function () {
                        var row = $(this).closest('tr');
                        var item = $('.relation-list-omni').data('relation-list-omni').gridItem.dataItem(row);
                        var url = $('.relation-list-omni').data('relation-list-omni').configuration.actionModuleStarter;
                        url += "?Object=" + item.IdObject;
                        window.open(url, "_blank");
                    });

                    $('.relation-list-omni').find('.button-object-edit-right-omni').click(function () {
                        var row = $(this).closest('tr');
                        var item = $('.relation-list-omni').data('relation-list-omni').gridItem.dataItem(row);
                        var url = $('.relation-list-omni').data('relation-list-omni').configuration.actionRelationEditor;
                        url += "?Object=" + item.IdOther;
                        window.open(url, "_blank");
                    });

                    $('.relation-list-omni').find('.button-module-starter-right-omni').click(function () {
                        var row = $(this).closest('tr');
                        var item = $('.relation-list-omni').data('relation-list-omni').gridItem.dataItem(row);
                        var url = $('.relation-list-omni').data('relation-list-omni').configuration.actionModuleStarter;
                        url += "?Object=" + item.IdOther;
                        window.open(url, "_blank");
                    });

                    if ($('.relation-list-omni').find('.k-button').data('kendoButton') === undefined) {
                        $('.relation-list-omni').find('.k-button').kendoButton({
                            click: $('.relation-list-omni').data('relation-list-omni').clickButtonHandler
                        });
                    }

                };
                

                $.extend(response, {
                    change: $('.relation-list-omni').data('relation-list-omni').gridChangeHandler,
                    filterable: {
                        mode: "row"
                    },
                    cellClose: function (e) {
                        e.model.IsSaved = false;
                        $('.relation-list-omni').data('relation-list-omni').gridItem.saveChanges();
                    }
                });
                $.extend(response, { toolbar: kendo.template($("#templateRelationList").html()) });
                $('.relation-list-omni').data('relation-list-omni').gridItem = $('.relation-list-omni').find('.grid').kendoGrid(response).data("kendoGrid");


                $('.relation-list-omni').data('relation-list-omni').gridItem.one("dataBound", function (e) {
                    var filterCell = e.sender.thead.find(".k-filtercell[data-field='NameObject']");
                    if (filterCell.length !== 0) {
                        filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                        filterDropDown.select(3);
                        filterDropDown.trigger("change");
                    }

                    filterCell = e.sender.thead.find(".k-filtercell[data-field='NameOther']");
                    if (filterCell.length !== 0) {
                        filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                        filterDropDown.select(3);
                        filterDropDown.trigger("change");
                    }
                });

                $('.relation-list-omni').data('relation-list-omni').gridItem.wrapper.find(".k-pager")
                    .before('<a href="#" class="k-link prev">Prev</a>')
                    .after('<a href="#" class="k-link next">Next</a>')
                    .parent()
                    .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                    .delegate("a.prev", "click", function () {
                        dataSource.page(dataSource.page() - 1);
                    })
                    .delegate("a.next", "click", function () {
                        dataSource.page(dataSource.page() + 1);
                    })
                    .delegate(".pagesize input", "change", function () {
                        dataSource.page(parseInt(this.value, 10));
                    })



                $('body').closePageLoader();


                if (!$('.relation-list-omni').data('relation-list-omni').configuration.useIdParent) {
                    $('.relation-list-omni').data('relation-list-omni').gridItem.dataSource.read();
                }

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

    };

    //$.fn.createAttributeRelation = function (configuration) {
    //    var relationListAttribute = {
    //        configuration: configuration,
    //        elements: {
    //            relationListContainer: $(this),
    //            gridParent: null,
    //            grid: null,
    //            dialogAddToClipboardWindow: null,
    //            dialogEditAttributeWindow: null
    //        },
    //        gridItem: null,
    //        nodeItem: null,
    //        currentDataItem: null,
    //        _itemsPreApply: [],
    //        addApplyItem: function (item) {
    //            $('.relation-list-attribute').data('relation-list-attribute')._itemsPreApply.push(item);
    //            $('.relation-list-attribute').data('relation-list-attribute').checkViewState();
    //        },
    //        removeApplyItem: function (item) {
    //            $('.relation-list-attribute').data('relation-list-attribute')._itemsPreApply = jQuery.grep($('.relation-list-attribute').data('relation-list-attribute')._itemsPreApply, function (itemInArray) {
    //                return itemInArray.IdItem !== item.IdItem
    //            });

    //            $('.relation-list-attribute').data('relation-list-attribute').checkViewState();
    //        },
    //        setNodeItem: function (nodeItem) {
    //            $('.relation-list-attribute').data('relation-list-attribute').nodeItem = nodeItem;
    //            $('.relation-list-attribute').data('relation-list-attribute').checkViewState();
    //        },
    //        checkViewState: function () {
    //            $('.relation-list-attribute').find('.newItem').setEnabled($('.relation-list-attribute').data('relation-list-attribute')._idParent !== undefined, true);
    //            $('.relation-list-attribute').find('.applyItem').setEnabled($('.relation-list-attribute').data('relation-list-attribute')._itemsPreApply.length > 0, true);
    //            $('.relation-list-attribute').find('.addToClipboard').setEnabled($('.relation-list-attribute').data('relation-list-attribute')._itemsPreApply.length > 0, true);
    //            $('.relation-list-attribute').find('.deleteItem').setEnabled($('.relation-list-attribute').data('relation-list-attribute')._itemsPreApply.length > 0, true);
    //            $('.relation-list-attribute').find('.orderAsc').setEnabled($('.relation-list-attribute').data('relation-list-attribute')._itemsPreApply.length > 0, true);
    //            $('.relation-list-attribute').find('.orderDesc').setEnabled($('.relation-list-attribute').data('relation-list-attribute')._itemsPreApply.length > 0, true);

    //        },
    //        dataBoundHandler: function () {
    //        },
    //        gridChangeHandler: function (e) {
    //            var index = $('.relation-list-attribute').data('relation-list-attribute').gridItem.select().index();
    //            var col = this.options.columns[index].field;
    //            var currentDataItem = $('.relation-list-attribute').data('relation-list-attribute').gridItem.dataItem($('.relation-list-attribute').data('relation-list-attribute').gridItem.select().parent());
    //            if (currentDataItem !== undefined) {
    //                console.log(currentDataItem);

    //                if (col === 'Value' && currentDataItem.NameDataType === 'String') {
    //                    var value = currentDataItem.Value_String;

    //                    var dialogElement = $('#dialog');
    //                    dialogElement.kendoDialog({
    //                        width: "800px",
    //                        height: "600px",
    //                        title: "Value",
    //                        closable: true,
    //                        modal: false,
    //                        content: value,
    //                        actions: []
    //                    });

    //                    dialogElement.data("kendoDialog").open();
    //                }
    //            }

    //        },
    //        clickButtonHandler: function (e) {
    //            var button = $(e.event.target).closest(".k-button");
    //            if (button.hasClass('applyItem')) {
    //                var items = [];
    //                $('.relation-list-attribute').find('.chkbxApply:checked').closest('tr').each(function () {
    //                    var row = $(this);
    //                    var item = $('.relation-list-attribute').data('relation-list-attribute').gridItem.dataItem(row);
    //                    console.log(item);
    //                    items.push({ GUID: item.IdOther });
    //                });

    //                console.log(items);
    //                $('.relation-list-attribute').data('relation-list-attribute').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
    //                    ChannelId: $('.relation-list-attribute').data('relation-list-attribute').configuration.channelAppliedItems,
    //                    SenderId: $('.relation-list-attribute').data('relation-list-attribute').configuration.managerConfig.idEndpoint,
    //                    SessionId: $('.relation-list-attribute').data('relation-list-attribute').configuration.managerConfig.idSession,
    //                    UserId: $('.relation-list-attribute').data('relation-list-attribute').configuration.idUser,
    //                    OItems: items
    //                });

    //            } else if (button.hasClass('isListen')) {
    //                button.toggleMode(true);
    //            } else if (button.hasClass('orderAsc')) {
    //                var value = button.parent().find('.inpOrderId').getContent();
    //                items = [];
    //                $('.relation-list-attribute').find('.chkbxApply:checked').closest('tr').each(function () {
    //                    var row = $(this);
    //                    var item = $('.relation-list-attribute').data('relation-list-attribute').gridItem.dataItem(row);
    //                    console.log(item);
    //                    items.push({
    //                        uid: item.uid,
    //                        IdInstance: $('.relation-list-attribute').data('relation-list-attribute').configuration.idInstance,
    //                        IdItem: item.IdItem,
    //                        IdObject: item.IdObject,
    //                        IdOther: item.IdOther,
    //                        IdRelationType: item.IdRelationType,
    //                        OrderId: value++
    //                    });
    //                });

    //                $.post($('.relation-list-attribute').data('relation-list-attribute').configuration.actionSetOrderId, { requestList: items })
    //                    .done(function (response) {
    //                        console.log(response);
    //                        if (response.IsSuccessful) {
    //                            for (var ix in items) {
    //                                var item = items[ix];
    //                                var row = $('.relation-list-attribute').data('relation-list-attribute').gridItem.tbody.find("tr[data-uid='" + item.uid + "']");
    //                                var dataItem = $('.relation-list-attribute').data('relation-list-attribute').gridItem.dataItem(row);
    //                                dataItem.set("OrderId", item.OrderId);

    //                            }
    //                        }


    //                    })
    //                    .fail(function (jqxhr, textStatus, error) {
    //                        // stuff
    //                    });


    //            } else if (button.hasClass('deleteItem')) {
    //                var deleteItems = [];
    //                if ($('.relation-list-attribute').find('.chkbxApply:checked').closest('tr').length > 0 && confirm("Do you want to delete checked rows?")) {
    //                    $('.relation-list-attribute').find('.chkbxApply:checked').closest('tr').each(function () {
    //                        var row = $(this);
    //                        var dataItem = $('.relation-list-attribute').data('relation-list-attribute').gridItem.dataItem(row);
    //                        deleteItems.push(dataItem);

    //                        //$('.relation-list-attribute').data('relation-list-attribute').gridItem.removeRow(row);

    //                    });

    //                    for (var ix in deleteItems) {
    //                        var item = deleteItems[ix];
    //                        $('.relation-list-attribute').data('relation-list-attribute').gridItem.dataSource.remove(item);
    //                    }

    //                    $('.relation-list-attribute').data('relation-list-attribute').gridItem.dataSource.sync();
    //                }

    //            } else if (button.hasClass('addToClipboard')) {
    //                var clipboardItems = [];
    //                if ($('.relation-list-attribute').find('.chkbxApply:checked').closest('tr').length > 0) {
    //                    $('.relation-list-attribute').find('.chkbxApply:checked').closest('tr').each(function () {
    //                        var row = $(this);
    //                        var dataItem = $('.relation-list-attribute').data('relation-list-attribute').gridItem.dataItem(row);
    //                        var clipboardItem = {
    //                            GUID: dataItem.IdOther,
    //                            Name: dataItem.NameOther,
    //                            GUID_Parent: dataItem.IdParentOther,
    //                            Type: dataItem.Ontology
    //                        };
    //                        clipboardItems.push(clipboardItem);

    //                    });

    //                    $.post($('.relation-list-attribute').data('relation-list-attribute').configuration.actionAddToClipboard, { oItems: clipboardItems, idInstance: $('.relation-list-attribute').data('relation-list-attribute').configuration.idInstance })
    //                        .done(function (response) {

    //                            $('.relation-list-attribute').data('relation-list-attribute').elements.dialogAddToClipboardWindow.kendoDialog({
    //                                width: "450px",
    //                                title: "Add to Clipboard Result",
    //                                closable: false,
    //                                modal: false,
    //                                content: "<p>Added " + response.Result.length + " Items<p>",
    //                                actions: [
    //                                    { text: 'OK', primary: true }
    //                                ],
    //                                close: function (e) {

    //                                }
    //                            });
    //                            $('.relation-list-attribute').data('relation-list-attribute').elements.dialogAddToClipboardWindow.data('kendoDialog').open();
    //                        })
    //                        .fail(function (jqxhr, textStatus, error) {
    //                            // stuff
    //                        });
    //                }
    //            }


    //        },
    //        validateReference: function (relationNodeItem) {
    //            $('.relation-list-attribute').data('relation-list-attribute').setNodeItem(relationNodeItem);
    //            //document.title = nodeItem.NameLeft + ' -> ' + nodeItem.NameRelationType + ' -> ' + nodeItem.NameRight;
    //            $('.relation-list-attribute').data('relation-list-attribute').gridItem.dataSource.read();
    //        }
    //    };

    //    $(this).data('relation-list-attribute', relationListAttribute);

    //    relationListAttribute.elements.editAttribute = $('<div class="editAttribute"></div>');
    //    $(this).append(relationListAttribute.elements.editAttribute);
    //    relationListAttribute.elements.editAttribute.kendoWindow({
    //        width: '1024px',
    //        height: '600px',
    //        title: 'Edit Attribute',
    //        visible: false,
    //        modal: true,
    //        actions: [
    //            "Pin",
    //            "Minimize",
    //            "Maximize",
    //            "Close"
    //        ]
    //    }).data("kendoWindow").center();

    //    $.post($('.relation-list-attribute').data('relation-list-attribute').configuration.actionComponentObjectRelationAttributeList, { idInstance: $('.relation-list-attribute').data('relation-list-attribute').configuration.idInstance })
    //        .done(function (response) {
    //            console.log(response);



    //        })
    //        .fail(function (jqxhr, textStatus, error) {
    //            // stuff
    //        });

    //    var gridParent = $('<div class="gridParent"></div>');

    //    relationListAttribute.elements.gridParent = gridParent;
    //    relationListAttribute.elements.relationListContainer.append(relationListAttribute.elements.gridParent);

    //    var grid = $('<div class="grid"></div>');

    //    relationListAttribute.elements.grid = grid;
    //    relationListAttribute.elements.gridParent.append(relationListAttribute.elements.grid);

    //    $.post(relationListAttribute.configuration.actionGetGridConfig)
    //        .done(function (response) {
    //            if ($('.relation-list-attribute').data('relation-list-attribute').configuration.useIdParent) {
    //                $.extend(response.dataSource.transport.read, {
    //                    data: function () {
    //                        if ($('.relation-list-attribute').data('relation-list-attribute').nodeItem !== undefined) {
    //                            return {
    //                                IdLeft: $('.relation-list-attribute').data('relation-list-attribute').nodeItem.IdLeft,
    //                                NameLeft: $('.relation-list-attribute').data('relation-list-attribute').nodeItem.NameLeft,
    //                                IdRight: $('.relation-list-attribute').data('relation-list-attribute').nodeItem.IdRight,
    //                                NameRight: $('.relation-list-attribute').data('relation-list-attribute').nodeItem.NameRight,
    //                                IdRelationType: $('.relation-list-attribute').data('relation-list-attribute').nodeItem.IdRelationType,
    //                                NameRelationType: $('.relation-list-attribute').data('relation-list-attribute').nodeItem.NameRelationType,
    //                                NodeType: $('.relation-list-attribute').data('relation-list-attribute').nodeItem.NodeType,
    //                                IdInstance: $('.relation-list-attribute').data('relation-list-attribute').configuration.idInstance
    //                            };
    //                        } else {
    //                            return {
    //                                IdInstance: $('.relation-list-attribute').data('relation-list-attribute').configuration.idInstance
    //                            };
    //                        }

    //                    }
    //                });
    //            }

    //            $.extend(response.dataSource, {
    //                batch: true
    //            });

    //            $.extend(response.dataSource.transport.update,
    //                {
    //                    complete: function (e) {
    //                        console.log(e);
    //                    }
    //                }
    //            );

    //            var dataSource = new kendo.data.DataSource(response.dataSource);
    //            response.dataSource = dataSource;
    //            response.dataBound = function () {
    //                if ($('.relation-list-attribute').data('relation-list-attribute').dataBoundHandler !== undefined) {
    //                    $('.relation-list-attribute').data('relation-list-attribute').dataBoundHandler();
    //                }
    //                $('.relation-list-attribute').find('.chkbxApply').change(function () {

    //                    var row = $(this).closest('tr');
    //                    var item = $('.relation-list-attribute').data('relation-list-attribute').gridItem.dataItem(row);
    //                    var applied = $(this).prop("checked");
    //                    item.Apply = applied;
    //                    if (item.Apply) {
    //                        $('.relation-list-attribute').data('relation-list-attribute').addApplyItem(item);
    //                    } else {
    //                        $('.relation-list-attribute').data('relation-list-attribute').removeApplyItem(item);
    //                    }

    //                    console.log(item);
    //                });

    //                if ($('.relation-list-attribute').find('.k-button').data('kendoButton') === undefined) {
    //                    $('.relation-list-attribute').find('.k-button').kendoButton({
    //                        click: $('.relation-list-attribute').data('relation-list-attribute').clickButtonHandler
    //                    });
    //                }

    //                $('.relation-list-attribute').find('.button-object-atribute-edit').click(function () {
    //                    var row = $(this).closest('tr');
    //                    var item = $('.relation-list-attribute').data('relation-list-attribute').gridItem.dataItem(row);
    //                    var configuration = {
    //                        setDocumentTitle: true,
    //                        dataTypeId: item.IdDataType,
    //                        selectorPath: ['.relation-list-attribute', 'editAttribute'],
    //                        actionInit: viewModel.ActionEditAttributeInit,
    //                        managerConfig: viewModel.managerConfig,
    //                        events: {
    //                            createdComponent: function (editAttributeComponent) {
    //                                viewModel.editAttributeComponent = editAttributeComponent;
    //                                viewModel.getUrlParameters();
    //                            }
    //                        }
    //                    }

    //                    $('#@(Model.attributeArea.ViewItemId)').createEditAttribute(configuration);
    //                    $(relationListAttribute.elements.editAttribute).data("kendoWindow").open();
    //                });

    //            };
    //            $.extend(response, {
    //                change: $('.relation-list-attribute').data('relation-list-attribute').gridChangeHandler,
    //                filterable: {
    //                    mode: "row"
    //                },
    //                cellClose: function (e) {
    //                    e.model.IsSaved = false;
    //                    $('.relation-list-attribute').data('relation-list-attribute').gridItem.saveChanges();
    //                }
    //            });
    //            $.extend(response, { toolbar: kendo.template($("#templateRelationList").html()) });
    //            $('.relation-list-attribute').data('relation-list-attribute').gridItem = $('.relation-list-attribute').find('.grid').kendoGrid(response).data("kendoGrid");


    //            $('.relation-list-attribute').data('relation-list-attribute').gridItem.one("dataBound", function (e) {
    //                //var filterCell = e.sender.thead.find(".k-filtercell[data-field='Value']");
    //                //if (filterCell.length !== 0) {
    //                //    filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
    //                //    filterDropDown.select(10);
    //                //    filterDropDown.trigger("change");
    //                //}

    //            });

    //            $('.relation-list-attribute').data('relation-list-attribute').gridItem.wrapper.find(".k-pager")
    //                .before('<a href="#" class="k-link prev">Prev</a>')
    //                .after('<a href="#" class="k-link next">Next</a>')
    //                .parent()
    //                .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
    //                .delegate("a.prev", "click", function () {
    //                    dataSource.page(dataSource.page() - 1);
    //                })
    //                .delegate("a.next", "click", function () {
    //                    dataSource.page(dataSource.page() + 1);
    //                })
    //                .delegate(".pagesize input", "change", function () {
    //                    dataSource.page(parseInt(this.value, 10));
    //                });



    //            $('body').closePageLoader();


    //            if (!$('.relation-list-attribute').data('relation-list-attribute').configuration.useIdParent) {
    //                $('.relation-list-attribute').data('relation-list-attribute').gridItem.dataSource.read();
    //            }

    //        })
    //        .fail(function (jqxhr, textStatus, error) {
    //            // stuff
    //        });

    //};
})(jQuery);