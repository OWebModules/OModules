﻿(function ($) {
    $.fn.createAttributeTypeList = function (configuration) {
        var attributeTypeList = {
            configuration: configuration,
            itemsPreApply: [],
            selectedAttributeTypeId: null,
            data: {
            },
            handler: {
                addApplyItem: function (attributeTypeList, item) {
                    attributeTypeList.itemsPreApply.push(item);
                    attributeTypeList.handler.checkViewState(attributeTypeList);
                },
                removeApplyItem: function (attributeTypeList, item) {
                    attributeTypeList.itemsPreApply = jQuery.grep(attributeTypeList.itemsPreApply, function (itemInArray) {
                        return itemInArray.IdAttributeType !== item.IdAttributeType;
                    });

                    attributeTypeList.handler.checkViewState(attributeTypeList);
                },
                checkOrSetNewItem: function (attributeTypeList, newItemEnabled) {
                    var filterElement = $.grep($(attributeTypeList.elements.grid).find('.k-filtercell').find('input'), function (inp) { return ($(inp).hasClass('k-input')); });
                    if (newItemEnabled === null || typeof(newItemEnabled) === 'undefined') {
                        newItemEnabled = false;
                        if ($(filterElement).val() !== '') {
                            newItemEnabled = true;
                        }
                    }

                    setEnabled(attributeTypeList.configuration.selectorPath, attributeTypeList.configuration.viewModel.newItem, newItemEnabled, true);
                },
                checkViewState: function (attributeTypeList) {
                    attributeTypeList.handler.checkOrSetNewItem(attributeTypeList);
                    var enableNewItemInput = false;
                    var readonlyFilterElement = $(attributeTypeList.elements.filterElement).data('readonly');
                    if (typeof readonlyFilterElement === 'undefined') {
                        enableNewItemInput = false;
                    } else {
                        enableNewItemInput = !readonlyFilterElement;
                    }
                    setEnabled(attributeTypeList.configuration.selectorPath, attributeTypeList.configuration.viewModel.newItem, enableNewItemInput, true);
                    setEnabled(attributeTypeList.configuration.selectorPath, attributeTypeList.configuration.viewModel.applyItem, attributeTypeList.itemsPreApply.length > 0, true);
                    setEnabled(attributeTypeList.configuration.selectorPath, attributeTypeList.configuration.viewModel.addToClipboard, attributeTypeList.itemsPreApply.length > 0, true);
                    setEnabled(attributeTypeList.configuration.selectorPath, attributeTypeList.configuration.viewModel.deleteItem, attributeTypeList.itemsPreApply.length > 0, true);
                    $(attributeTypeList.elements.filterElement).parent().find('.btn-save').setEnabled(enableNewItemInput, true, attributeTypeList.configuration.viewModel.buttonSave);
                },
                gridChangeHandler: function () {
                    var containerElement = $(this.element).closest('.attributetype-list-container');
                    var attributeTypeList = $(containerElement).data('attributetype-list-config');
                    var currentDataItem = attributeTypeList.elements.gridItem.dataItem(this.select());
                    $(attributeTypeList.elements.grid).find('.idInput').val(currentDataItem.IdAttributeType);
                    if (currentDataItem !== undefined) {
                        console.log(currentDataItem);
                        attributeTypeList.selectedAttributeTypeId = currentDataItem.IdAttributeType;
                        var items = [
                            {
                                GUID: currentDataItem.IdAttributeType,
                                Name: currentDataItem.NameAttributeType,
                                GUID_Parent: currentDataItem.IdDataType,
                                Type: "AttributeType"
                            }];
                        if (typeof attributeTypeList.configuration.events !== 'undefined' && attributeTypeList.configuration.events !== null &&
                            typeof attributeTypeList.configuration.events.selectedAttributeType !== 'undefined' && attributeTypeList.configuration.events.selectedAttributeType !== null) {
                            attributeTypeList.configuration.events.selectedAttributeType(items, attributeTypeList.configuration.parentComponent);
                        }
                        else {
                            attributeTypeList.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                ChannelId: attributeTypeList.configuration.viewModel.ChannelSelectedItem,
                                SenderId: attributeTypeList.configuration.viewModel.IdEndpoint,
                                SessionId: attributeTypeList.configuration.viewModel.IdInstance,
                                UserId: attributeTypeList.configuration.viewModel.IdUser,
                                OItems: items
                            });
                        }
                        
                    }
                },
                errorhandler: function (e) {

                },
                clickFunctionButtonHandler: function (e) {

                    var button = $(e.event.target).closest(".k-button");
                    var attributeTypeList = $(button).data('attributetype-list-config');
                    if (button.hasClass('applyItem')) {
                        var items = [];
                        attributeTypeList.elements.grid.find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var item = attributeTypeList.elements.gridItem.dataItem(row);
                            console.log(item);
                            items.push({
                                GUID: item.IdAttributeType,
                                Name: item.NameAttributeType,
                                GUID_Parent: item.IdDataType,
                                Type: "AttributeType"
                            });
                        });

                        console.log(items);
                        if (typeof attributeTypeList.configuration.events !== 'undefined' &&
                            typeof attributeTypeList.configuration.events.applyItems !== 'undefined') {
                            attributeTypeList.configuration.events.applyItems(items, attributeTypeList.configuration.parentComponent);
                        }
                        else {
                            attributeTypeList.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                ChannelId: attributeTypeList.configuration.viewModel.ChannelAppliedAttributeTypes,
                                SenderId: attributeTypeList.configuration.viewModel.IdEndpoint,
                                SessionId: attributeTypeList.configuration.viewModel.IdInstance,
                                UserId: attributeTypeList.configuration.viewModel.IdUser,
                                OItems: items
                            });
                        }

                    } else if (button.hasClass('addToClipboard')) {
                        var clipboardItems = [];
                        attributeTypeList.elements.grid.find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var item = attributeTypeList.elements.gridItem.dataItem(row);
                            console.log(item);
                            clipboardItems.push({
                                GUID: item.IdAttributeType,
                                GUID_Parent: item.IdDataType,
                                Type: "AttributeType"
                            });
                        });

                        $.post(attributeTypeList.configuration.viewModel.ActionAddToClipboard, { oItems: clipboardItems, idInstance: attributeTypeList.configuration.viewModel.IdInstance })
                            .done(function (response) {

                                attributeTypeList.elements.dialogAddToClipboardWindow.kendoDialog({
                                    width: "450px",
                                    title: "Add to Clipboard Result",
                                    closable: false,
                                    modal: false,
                                    content: "<p>Added " + response.Result.length + " Items<p>",
                                    actions: [
                                        { text: 'OK', primary: true }
                                    ],
                                    close: function (e) {

                                    }
                                });

                                attributeTypeList.elements.dialogAddToClipboardWindow.data('kendoDialog').open();
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });

                    } else if (button.hasClass('deleteItem')) {
                        var deleteItems = [];
                        var deleteIds = [];

                        if (attributeTypeList.elements.grid.find('.chkbxApply:checked').closest('tr').length > 0 && confirm("Do you want to delete checked rows?")) {
                            attributeTypeList.elements.grid.find('.chkbxApply:checked').closest('tr').each(function () {
                                var row = $(this);
                                var dataItem = attributeTypeList.elements.gridItem.dataItem(row);
                                deleteItems.push(dataItem);

                                deleteIds.push(dataItem.IdAttributeType);

                                //$('.relation-list-attribute').data('relation-list-attribute').gridItem.removeRow(row);

                            });

                            attributeTypeList.data.deleteIds = deleteIds;
                            attributeTypeList.data.deleteItems = deleteItems;
                            attributeTypeList.data.forceDelete = false;

                            $('body').openPageLoader();
                            $.post(attributeTypeList.configuration.viewModel.ActionHasRelations, { attributeTypeIds: deleteIds })
                                .done(function (response) {
                                    if (response.IsSuccessful) {
                                        if (response.Result) {
                                            attributeTypeList.data.forceDelete = false;
                                            if (confirm(response.Message + ". Do you want to force delete?")) {
                                                attributeTypeList.data.forceDelete = true;
                                            }

                                            for (var ix in attributeTypeList.data.deleteItems) {
                                                var item = attributeTypeList.data.deleteItems[ix];
                                                if (!attributeTypeList.data.forceDelete) {
                                                    var itemsContained = $.grep(response.Result, function (itm) {
                                                        return itm == item.IdAttributeType;
                                                    });
                                                    if (itemsContained.length > 0) {
                                                        continue;
                                                    }
                                                }
                                                attributeTypeList.elements.gridItem.dataSource.remove(item);
                                            }

                                            $('body').closePageLoader();

                                            attributeTypeList.elements.gridItem.dataSource.sync();
                                        }
                                        else {

                                        }
                                    }
                                    else {

                                    }
                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    // stuff
                                });


                        }

                    } else if (button.hasClass('btn-save')) {
                        var attributeTypeList = $(button).data('attributetype-list-config');
                        var itemName = $(attributeTypeList.elements.filterElement).val();

                        if (itemName === '') {
                            return;
                        }

                        var dataType = $("input:radio[name=dataTypes]:checked").val();
                        if (dataType == 'bool') {
                            dataType = attributeTypeList.configuration.viewModel.dataTypeBool;
                        } else if (dataType == 'datetime') {
                            dataType = attributeTypeList.configuration.viewModel.dataTypeDateTime;
                        } else if (dataType == 'long') {
                            dataType = attributeTypeList.configuration.viewModel.dataTypeLong;
                        } else if (dataType == 'real') {
                            dataType = attributeTypeList.configuration.viewModel.dataTypeRal;
                        } else if (dataType == 'string') {
                            dataType = attributeTypeList.configuration.viewModel.dataTypeString;
                        }
                        else {
                            return;
                        }
                        var itemsToSave = [{
                            Name: itemName,
                            GUID_Parent: dataType,
                            Type: "AttributeType"
                        }];

                        $.post(attributeTypeList.configuration.viewModel.ActionSaveNewItems, { itemsToSave: itemsToSave, idInstance: attributeTypeList.configuration.viewModel.IdInstance })
                            .done(function (response) {
                                if (response.IsSuccessful) {
                                    $(filterElement).val('');
                                    $(filterElement).css('background-color', '');
                                    attributeTypeList.handler.checkOrSetNewItem(attributeTypeList, false);
                                    attributeTypeList.handler.checkViewState(attributeTypeList);
                                    var itemSaved = response.Result[0];
                                    attributeTypeList.handler.addGridItem(itemSaved);
                                    attributeTypeList.elements.gridItem.dataSource.filter({
                                        field: 'IdItem',
                                        operator: 'eq',
                                        value: itemSaved.IdItem
                                    });
                                }

                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });

                    } else if (button.hasClass('btn-cancel')) {
                        var attributeTypeList = $(button).data('attributetype-list-config');
                        $(attributeTypeList.elements.windowNew).data('kendoWindow').close();
                    }

                },
                reloadGrid: function (attributeTypeList) {
                    var filterElement = $.grep(attributeTypeList.elements.grid.find('.k-filtercell').find('input'), function (inp) { return ($(inp).hasClass('k-input')); });
                    $(filterElement).css('background-color', '');
                    $(filterElement).css('color', '');
                    if (attributeTypeList.elements.gridItem !== undefined && attributeTypeList.elements.gridItem !== null) {
                        if (attributeTypeList.handler.getFilter(attributeTypeList) !== undefined && attributeTypeList.handler.getFilter() !== "") {
                            attributeTypeList.elements.gridItem.dataSource.filter([]);
                        }

                        attributeTypeList.elements.gridItem.dataSource.read();
                    }
                },
                getFilter: function (attributeTypeList) {
                    var filter = attributeTypeList.elements.gridItem.dataSource.filter();
                    if (filter === undefined) return;

                    if (attributeTypeList.elements.gridItem.dataSource.filter().filters.length > 0) {
                        return attributeTypeList.elements.gridItem.dataSource.filter().filters[0].value;
                    }
                    else {
                        return "";
                    }

                },
                addGridItem: function (gridItem) {
                    attributeTypeList.elements.gridItem.dataSource.add(gridItem);
                },
                changeNewItemInput: function (e) {
                    var attributeTypeList = $(this).data('attributetype-list-config');
                    attributeTypeList.elements.filterElement = $(this);
                    $(attributeTypeList.elements.filterElement).data('readonly', true);
                    attributeTypeList.handler.checkOrSetNewItem(attributeTypeList, false);
                    if (attributeTypeList.configuration.viewModel.newItemTimout !== undefined) {
                        clearTimeout(attributeTypeList.configuration.viewModel.newItemTimout);
                        $(attributeTypeList.elements.filterElement).css('background-color', '');
                        $(attributeTypeList.elements.filterElement).css('color', '');
                        attributeTypeList.handler.checkViewState(attributeTypeList);
                    }
                    attributeTypeList.configuration.viewModel.newItemTimout = setTimeout(function () {
                        var itemName = $(attributeTypeList.elements.filterElement).val();

                        if (itemName === '') {
                            return;
                        }
                        var itemsToCheck = [{
                            Name: itemName,
                            Type: "AttributeType"
                        }];
                        $.post(attributeTypeList.configuration.viewModel.ActionCheckExistance, { itemsToCheck: itemsToCheck, idInstance: attributeTypeList.configuration.viewModel.IdInstance })
                            .done(function (response) {
                                if (response.IsSuccessful) {
                                    var attributeTypeList = $(getElementsBySelectorPath(response.SelectorPath)).data('attributetype-list-config');
                                    attributeTypeList.handler.checkOrSetNewItem(attributeTypeList, true);
                                    var itemChecked = response.Result[0];
                                    if (itemChecked.Mark === true) {
                                        $(attributeTypeList.elements.filterElement).data('readonly', true);
                                        $(attributeTypeList.elements.filterElement).css('background-color', 'red');
                                        $(attributeTypeList.elements.filterElement).css('color', 'black');
                                    }
                                    else {
                                        $(attributeTypeList.elements.filterElement).data('readonly', false);
                                        $(attributeTypeList.elements.filterElement).css('background-color', 'green');
                                        $(attributeTypeList.elements.filterElement).css('color', 'white');
                                    }
                                    attributeTypeList.handler.checkViewState(attributeTypeList);
                                }

                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });

                    }, 500, attributeTypeList);
                },
                saveNewItem: function (e) {
                    var element = e.target;
                    var parentElement = $(element).closest('button');
                    if (typeof (parentElement) !== 'undefined') {
                        element = parentElement;
                    }
                    
                    var attributeTypeList = $(element).data('attributetype-list-config');
                    var itemName = $(attributeTypeList.elements.filterElement).val();

                    if (itemName === '') {
                        return;
                    }

                    $(attributeTypeList.elements.windowNew).find('.input-name').val(itemName).trigger('input');
                    $(attributeTypeList.elements.windowNew).data('kendoWindow').open();

                    
                },
                openNewItemsWindow: function () {
                    
                }
            },
            elements: {
                grid: null,
                gridItem: null,
                windowNew: null,
                windowEdit: null,
                windowEditOrderId: null,
                windowDelete: null
            }
        };

        attributeTypeList.elements.grid = $('<div class="attributeTypeList-grid"></div>');
        $(this).addClass('attributetype-list');
        $(this).append(attributeTypeList.elements.grid);

        $(this).openPageLoader();

        var newWindowCode = `
            <div class='new-attributeType-window'>
                <div style="overflow: hidden;" class="new-window-content">
                    <div class="add-attribute-types">
                        <label>Name</label><br>
                        <input type="text" class="input-name" class="form-control"><br>
                        <label>Datatype</label>
                        <div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input bool-datatype" name="dataTypes" value="bool">
                          <label class="custom-control-label">Bool</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input int-datatype" name="dataTypes" value="long">
                          <label class="custom-control-label">Long</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input real-datatype" name="dataTypes" value="real">
                          <label class="custom-control-label">Real</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input datetime-datatype" name="dataTypes"  value="datetime">
                          <label class="custom-control-label">DateTime</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input string-datatype" name="dataTypes" value="string" checked>
                          <label class="custom-control-label">String</label>
                        </div>
                        <div class="btn-group" role="group" aria-label="Acknowledge">
                          <button type="button" class="btn btn-secondary k-button btn-save">Save</button>
                          <button type="button" class="btn btn-secondary k-button btn-cancel">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>`;

        attributeTypeList.elements.windowNew = $(newWindowCode);
        $(this).append(attributeTypeList.elements.windowNew);
        $(attributeTypeList.elements.windowNew).find('.btn-save').data('attributetype-list-config', attributeTypeList);
        $(attributeTypeList.elements.windowNew).find('.btn-cancel').data('attributetype-list-config', attributeTypeList);
        $(attributeTypeList.elements.windowNew).find('.k-button').data('attributetype-list-config', attributeTypeList);
        $(attributeTypeList.elements.windowNew).find('.k-button').kendoButton({
            click: attributeTypeList.handler.clickFunctionButtonHandler
        });

        attributeTypeList.elements.windowNew.kendoWindow({
            width: '222px',
            height: '287px',
            title: 'New Items',
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Minimize",
                "Maximize",
                "Close"
            ]
        }).data("kendoWindow").center();

        var editWindowCode = `
            <div class='edit-window'>
                <div class="k-content">
                    <ul class="fieldlist">
                        <li>
                            <label class='lbl-guid'>GUID:</label>
                            <input class='txt-guid' type="text" class="k-textbox editControl">
                            <button class='k-button copy-guid'><i class="icon-coppy-guid)" aria-hidden="true"></i></button>

                        </li>
                        <li>
                            <label class='lbl-name'>Value:</label>
                            <input type="text" class="k-textbox editControl inp-name"/>
                        </li>
                        <li>
                            <button class="k-button save-name">Save</button>
                        </li>
                    </ul>
                </div>

                <div class='message-name'></div>

            </div>`;

        attributeTypeList.elements.windowEdit = $(editWindowCode);
        $(this).append(attributeTypeList.elements.windowEdit);
        $(attributeTypeList.elements.windowEdit).kendoWindow({
            width: '350px',
            height: '260px',
            title: 'Edit Name',
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Minimize",
                "Maximize",
                "Close"
            ]
        }).data("kendoWindow").center();


        var editWindowOrderId = `
            <div class='edit-window-orderid'>
                <div class="k-content">
                    <ul class="fieldlist">
                        <li>
                            <label class='lbl-orderid' >Value:</label>
                            <input class='inp-orderid' type="number" value="1" step="1" style="width: 100%;" />

                        </li>
                        <li>
                            <button class="save-orderid k-button">Save</button>
                        </li>
                    </ul>
                </div>
                <div class='message-orderid'></div>
            </div>`;

        attributeTypeList.elements.windowEditOrderId = $(editWindowOrderId);
        $(this).append(attributeTypeList.elements.windowEditOrderId);
        attributeTypeList.elements.windowEditOrderId.find('.inp-orderid').kendoNumericTextBox({ format: 'n0' });

        attributeTypeList.elements.windowEditOrderId.kendoWindow({
            width: '160px',
            height: '160px',
            title: 'OrderId',
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Minimize",
                "Maximize",
                "Close"
            ]
        }).data("kendoWindow").center();

        var deleteWindow = `
            <div class='delete-window'>
                <div style="overflow: hidden;" class'message-delete'>

                </div>
            </div>`;

        attributeTypeList.elements.windowDelete = $(deleteWindow);
        $(this).append(attributeTypeList.elements.windowDelete);

        attributeTypeList.elements.windowDelete.kendoWindow({
            width: '160px',
            height: '160px',
            title: 'Delete',
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Minimize",
                "Maximize",
                "Close"
            ]
        }).data("kendoWindow").center();

        attributeTypeList.elements.dialogAddToClipboardWindow = $('<div class="add-to-clipboard-window"></div>');
        $(this).append(attributeTypeList.elements.dialogAddToClipboardWindow);


        $(this).data('attributetype-list-config', attributeTypeList);

        $.post(attributeTypeList.configuration.actionAttributeTypeListInit, { selectorPath: attributeTypeList.configuration.selectorPath, idInstance: attributeTypeList.configuration.idInstance})
            .done(function (response) {
                var attributeTypeList = $(getElementsBySelectorPath(response.SelectorPath)).data('attributetype-list-config');
                attributeTypeList.configuration.viewModel = response.Result;
                if (attributeTypeList.configuration.handler !== undefined && attributeTypeList.configuration !== null && attributeTypeList.configuration.handler.initCompleteHandler !== undefined && attributeTypeList.configuration.handler.initCompleteHandler !== null) {
                    attributeTypeList.configuration.handler.initCompleteHandler(attributeTypeList, attributeTypeList.configuration.parentComponent);
                }

                $.post(attributeTypeList.configuration.viewModel.ActionGetGridConfig, { idInstance: attributeTypeList.configuration.viewModel.IdInstance })
                    .done(function (response) {
                        var attributeTypeList = $(getElementsBySelectorPath(response.SelectorPath)).data('attributetype-list-config');
                        $.extend(response.Result.dataSource.transport.read, {
                            data: function () {
                                var attributeTypeList = $(getElementsBySelectorPath(this.selectorPath)).data('attributetype-list-config');
                                var idInstance = attributeTypeList.configuration.viewModel.IdInstance;
                                return { idInstance: idInstance};
                            }
                        });

                        response.Result.dataSource.transport.parameterMap = function (data, type) {
                            if (type == "destroy") {
                                var deleteItems = {
                                    models: []
                                };
                                data.models.forEach(function (item) {
                                    deleteItems.models.push({ IdAttributeType: item.IdAttributeType });
                                });
                                return deleteItems;
                            }
                            if (type == "read") {
                                return data;
                            }
                            if (type == "update") {
                                var updateItems = {
                                    models: []
                                };
                                data.models.forEach(function (item) {
                                    updateItems.models.push({ IdAttributeType: item.IdAttributeType, NameAttributeType: item.NameAttributeType });
                                });
                                return updateItems;
                            }
                        };
                        
                        $.extend(response.Result.dataSource, {
                            batch: true
                        });

                        $.extend(response.Result.dataSource.transport.update,
                            {
                                complete: function (e) {
                                    console.log(e);
                                }
                            }
                        );

                        $.extend(response.Result.dataSource.transport.destroy,
                            {
                                complete: function (e) {
                                    console.log(e);
                                }
                            }
                        );

                        $.extend(response.Result.dataSource.transport.read,
                            {
                                complete: function (e) {
                                    console.log(e);
                                },
                                selectorPath: response.SelectorPath
                            }
                        );

                        var dataSource = new kendo.data.DataSource(response.Result.dataSource);
                        dataSource.bind("error", function (e) {
                            console.log(e);
                            attributeTypeList.elements.gridItem.cancelChanges();
                            var dialogElement = $('#dialog');
                            dialogElement.kendoDialog({
                                width: "300px",
                                height: "150px",
                                title: "Value",
                                closable: true,
                                modal: false,
                                content: "An error occured!",
                                actions: []
                            });

                            dialogElement.data("kendoDialog").open();
                        });
                        response.Result.dataSource = dataSource;
                        response.Result.dataBound = function () {


                            var pagerElement = this.pager.element;
                            if (!pagerElement.find(".idInput").length) {
                                pagerElement
                                    .append('<span><input class="k-textbox idInput" placeholder="GUID" readonly></input></span>');
                            }

                            $('.attributetype-list').find('.chkbxApply').change(function () {
                                var containerElement = $(this).closest('.attributetype-list-container');
                                var attributeTypeList = $(containerElement).data('attributetype-list-config');
                                var row = $(this).closest('tr');
                                var item = attributeTypeList.elements.gridItem.dataItem(row);
                                var applied = $(this).prop("checked");
                                item.Apply = applied;
                                if (item.Apply) {
                                    attributeTypeList.handler.addApplyItem(attributeTypeList, item);
                                } else {
                                    attributeTypeList.handler.removeApplyItem(attributeTypeList, item);
                                }

                                console.log(item);
                            });

                        };
                        $.extend(response.Result, {
                            change: attributeTypeList.handler.gridChangeHandler,
                            filterable: {
                                mode: "row"
                            },
                            filter: function (e) {
                                if (e.filter === null) {
                                    var filterElement = $.grep($('.attributetype-list').find('.k-filtercell').find('input'), function (inp) { return ($(inp).hasClass('k-input')); });
                                    $(filterElement).css('background-color', '');
                                    $(filterElement).css('color', '');
                                    attributeTypeList.handler.checkOrSetNewItem(attributeTypeList, false);
                                }
                            },
                            cellClose: function (e) {
                                e.model.IsSaved = false;
                                attributeTypeList.elements.gridItem.saveChanges();
                            }
                        });
                        var template = attributeTypeList.configuration.templateSelector;
                        if (typeof template === "undefined") {
                            template = "#template";
                        }
                        $.extend(response.Result, { toolbar: kendo.template($(template).html()) });
                        attributeTypeList.elements.gridItem = $(attributeTypeList.elements.grid).kendoGrid(response.Result).data("kendoGrid");

                        changeValuesFromServerChildren(attributeTypeList.configuration.viewModel.ViewItems, attributeTypeList.configuration.viewModel.SelectorPath);
                        var filterElement = $.grep($('.attributetype-list').find('.k-filtercell').find('input'), function (inp) { return ($(inp).hasClass('k-input')); });
                        $(filterElement).data('attributetype-list-config', attributeTypeList);
                        $(filterElement).unbind('input', attributeTypeList.handler.changeNewItemInput);
                        $(filterElement).unbind('paste', attributeTypeList.handler.changeNewItemInput);
                        $(filterElement).bind('input', attributeTypeList.handler.changeNewItemInput);
                        $(filterElement).bind('paste', attributeTypeList.handler.changeNewItemInput);

                        $(attributeTypeList.elements.windowNew).find('.input-name').data('attributetype-list-config', attributeTypeList);
                        $(attributeTypeList.elements.windowNew).find('.input-name').unbind('input', attributeTypeList.handler.changeNewItemInput);
                        $(attributeTypeList.elements.windowNew).find('.input-name').bind('input', attributeTypeList.handler.changeNewItemInput);
                        $(attributeTypeList.elements.windowNew).find('.input-name').unbind('paste', attributeTypeList.handler.changeNewItemInput);
                        $(attributeTypeList.elements.windowNew).find('.input-name').bind('paste', attributeTypeList.handler.changeNewItemInput);

                        $('.attributetype-list').find('.newItem').unbind('click', attributeTypeList.handler.saveNewItem);
                        $('.attributetype-list').find('.newItem').bind('click', attributeTypeList.handler.saveNewItem);

                        attributeTypeList.elements.gridItem.one("dataBound", function (e) {
                            var filterElement = $.grep($('.attributetype-list').find('.k-filtercell').find('input'), function (inp) { return ($(inp).hasClass('k-input')); });
                            $(filterElement).css('background-color', '');
                            $(filterElement).css('color', '');
                            attributeTypeList.handler.checkOrSetNewItem(attributeTypeList, false);

                            var filterCell = e.sender.thead.find(".k-filtercell[data-field='NameAttributeType']");


                            if (filterCell.length === 0) return;
                            var filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                            filterDropDown.select(3);
                            filterDropDown.trigger("change");
                            $('.attributetype-list').find('.k-button').data('attributetype-list-config', attributeTypeList);
                            $('.attributetype-list').find('.k-button').kendoButton({
                                click: attributeTypeList.handler.clickFunctionButtonHandler
                            });
                        });

                        attributeTypeList.handler.reloadGrid(attributeTypeList);

                        if (attributeTypeList.configuration.callbackGridCreated !== undefined && attributeTypeList.configuration.callbackGridCreated !== null) {
                            attributeTypeList.configuration.callbackGridCreated(attributeTypeList.elements.gridItem);
                        }

                        attributeTypeList.elements.grid.find('.header-checkbox').change(function (ev) {
                            var checked = ev.target.checked;
                            $('.attributetype-list').find('.chkbxApply').each(function (idx, item) {
                                $(item).click();
                            });
                        });

                        $('body').closePageLoader();

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
        


    };
})(jQuery);