﻿(function ($) {
    $.fn.createObjectList = function (configuration) {
        var objectList = {
            configuration: configuration,
            itemsPreApply: [],
            idParent: null,
            selectedObjectId: null,
            data: {
            },
            handler: {
                addApplyItem: function (item) {
                    $('.object-list').data('object-list-config').itemsPreApply.push(item);
                    $('.object-list').data('object-list-config').handler.checkViewState();
                },
                removeApplyItem: function (item) {
                    $('.object-list').data('object-list-config').itemsPreApply = jQuery.grep($('.object-list').data('object-list-config').itemsPreApply, function (itemInArray) {
                        return itemInArray.IdInstance !== item.IdInstance;
                    });

                    $('.object-list').data('object-list-config').handler.checkViewState();
                },
                checkOrSetNewItem: function (newItemEnabled) {
                    var filterElement = $.grep($('.object-list').find('.k-filtercell').find('input'), function (inp) { return ($(inp).hasClass('k-input')); });
                    if (newItemEnabled === null || newItemEnabled === undefined) {
                        newItemEnabled = false;
                        if ($('.object-list').data('object-list-config').idParent !== undefined && $(filterElement).val() !== '') {
                            newItemEnabled = true;
                        }
                    }
                    
                    
                    setEnabled(['.object-list'], $('.object-list').data('object-list-config').configuration.viewModel.newItem, newItemEnabled, true);
                },
                checkOrSetDuplicateObjectSave: function (objectList, duplicateObjectEnabled) {
                    var duplicateObjectInput = $(objectList.elements.dialogDuplicateObject).find('.new-name');
                    var duplicateObjectSave = $(objectList.elements.dialogDuplicateObject).find('.btn-duplicate');
                    $(duplicateObjectInput).css('background-color', '');
                    $(duplicateObjectInput).css('color', '');

                    if (duplicateObjectEnabled === null || duplicateObjectEnabled === undefined) {
                        duplicateObjectEnabled = false;
                        if ($(duplicateObjectInput).val() !== '') {
                            duplicateObjectEnabled = true;
                        }
                    }
                    if (duplicateObjectEnabled) {
                        $(duplicateObjectSave).removeAttr('disabled');
                    } else {
                        $(duplicateObjectSave).attr('disabled', 'disabled');
                    }
                },
                checkViewState: function () {
                    $('.object-list').data('object-list-config').handler.checkOrSetNewItem();
                    setEnabled(['.object-list'], $('.object-list').data('object-list-config').configuration.viewModel.newItemInput, $('.object-list').data('object-list-config').idParent !== undefined, true);
                    setEnabled(['.object-list'], $('.object-list').data('object-list-config').configuration.viewModel.applyItem, $('.object-list').data('object-list-config').itemsPreApply.length > 0, true);
                    setEnabled(['.object-list'], $('.object-list').data('object-list-config').configuration.viewModel.addToClipboard, $('.object-list').data('object-list-config').itemsPreApply.length > 0, true);
                    setEnabled(['.object-list'], $('.object-list').data('object-list-config').configuration.viewModel.deleteItem, $('.object-list').data('object-list-config').itemsPreApply.length > 0, true);
                },
                gridChangeHandler: function () {
                    var currentDataItem = $('.object-list').data('object-list-config').elements.gridItem.dataItem(this.select());
                    $('.object-list').find('.idInput').val(currentDataItem.IdInstance);
                    if (currentDataItem !== undefined) {
                        console.log(currentDataItem);
                        $('.object-list').data('object-list-config').selectedObjectId = currentDataItem.IdInstance;
                        var items = [
                            {
                                GUID: currentDataItem.IdInstance,
                                Name: currentDataItem.NameInstance,
                                GUID_Parent: currentDataItem.IdClass,
                                Type: "Object"
                            }];
                        if (typeof $('.object-list').data('object-list-config').configuration.events !== 'undefined' && $('.object-list').data('object-list-config').configuration.events !== null &&
                            typeof $('.object-list').data('object-list-config').configuration.events.selectedObject !== 'undefined' && $('.object-list').data('object-list-config').configuration.events.selectedObject !== null) {
                            $('.object-list').data('object-list-config').configuration.events.selectedObject(items, $('.object-list').data('object-list-config').configuration.parentComponent);
                        }
                        else {
                            $('.object-list').data('object-list-config').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                ChannelId: $('.object-list').data('object-list-config').configuration.viewModel.ChannelSelectedItem,
                                SenderId: $('.object-list').data('object-list-config').configuration.viewModel.IdEndpoint,
                                SessionId: $('.object-list').data('object-list-config').configuration.viewModel.IdInstance,
                                UserId: $('.object-list').data('object-list-config').configuration.viewModel.IdUser,
                                OItems: items
                            });
                        }
                        
                    }
                },
                errorhandler: function (e) {

                },
                clickFunctionButtonHandler: function (e) {

                    var button = $(e.event.target).closest(".k-button");
                    if (button.hasClass('applyItem')) {
                        var items = [];
                        $('.object-list').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var item = $('.object-list').data('object-list-config').elements.gridItem.dataItem(row);
                            console.log(item);
                            items.push({
                                GUID: item.IdInstance,
                                Name: item.NameInstance,
                                GUID_Parent: item.IdClass,
                                Type: "Object"
                            });
                        });

                        console.log(items);
                        if (typeof $('.object-list').data('object-list-config').configuration.events !== 'undefined' &&
                            typeof $('.object-list').data('object-list-config').configuration.events.applyItems !== 'undefined') {
                            $('.object-list').data('object-list-config').configuration.events.applyItems(items, $('.object-list').data('object-list-config').configuration.parentComponent);
                        }
                        else {
                            $('.object-list').data('object-list-config').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                ChannelId: $('.object-list').data('object-list-config').configuration.viewModel.ChannelAppliedObjects,
                                SenderId: $('.object-list').data('object-list-config').configuration.viewModel.IdEndpoint,
                                SessionId: $('.object-list').data('object-list-config').configuration.viewModel.IdInstance,
                                UserId: $('.object-list').data('object-list-config').configuration.viewModel.IdUser,
                                OItems: items
                            });
                        }

                    } else if (button.hasClass('addToClipboard')) {
                        var clipboardItems = [];
                        $('.object-list').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var item = $('.object-list').data('object-list-config').elements.gridItem.dataItem(row);
                            console.log(item);
                            clipboardItems.push({
                                GUID: item.IdInstance,
                                GUID_Parent: item.IdClass
                            });
                        });

                        $.post(objectList.configuration.viewModel.ActionAddToClipboard, { oItems: clipboardItems, idInstance: $('.object-list').data('object-list-config').configuration.viewModel.IdInstance })
                            .done(function (response) {

                                $('.object-list').data('object-list-config').elements.dialogAddToClipboardWindow.kendoDialog({
                                    width: "450px",
                                    title: "Add to Clipboard Result",
                                    closable: false,
                                    modal: false,
                                    content: "<p>Added " + response.Result.length + " Items<p>",
                                    actions: [
                                        { text: 'OK', primary: true }
                                    ],
                                    close: function (e) {

                                    }
                                });

                                $('.object-list').data('object-list-config').elements.dialogAddToClipboardWindow.data('kendoDialog').open();
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });

                    } else if (button.hasClass('deleteItem')) {
                        var deleteItems = [];
                        var deleteIds = [];

                        if ($('.object-list').find('.chkbxApply:checked').closest('tr').length > 0 && confirm("Do you want to delete checked rows?")) {
                            $('.object-list').find('.chkbxApply:checked').closest('tr').each(function () {
                                var row = $(this);
                                var dataItem = $('.object-list').data('object-list-config').elements.gridItem.dataItem(row);
                                deleteItems.push(dataItem);

                                deleteIds.push(dataItem.IdInstance);

                                //$('.relation-list-attribute').data('relation-list-attribute').gridItem.removeRow(row);

                            });

                            $('.object-list').data('object-list-config').data.deleteIds = deleteIds;
                            $('.object-list').data('object-list-config').data.deleteItems = deleteItems;
                            $('.object-list').data('object-list-config').data.forceDelete = false;

                            $('body').openPageLoader();
                            $.post(objectList.configuration.viewModel.ActionHasRelations, { objectIds: deleteIds })
                                .done(function (response) {
                                    if (response.IsSuccessful) {
                                        if (response.Result) {
                                            $('.object-list').data('object-list-config').data.forceDelete = false;
                                            if (confirm(response.Message + ". Do you want to force delete?")) {
                                               $('.object-list').data('object-list-config').data.forceDelete = true;
                                            } 

                                            for (var ix in $('.object-list').data('object-list-config').data.deleteItems) {
                                                var item = $('.object-list').data('object-list-config').data.deleteItems[ix];
                                                if (!$('.object-list').data('object-list-config').data.forceDelete) {
                                                    var itemsContained = $.grep(response.Result, function (itm) {
                                                        return itm == item.IdInstance;
                                                    });
                                                    if (itemsContained.length > 0) {
                                                        continue;
                                                    }
                                                }
                                                $('.object-list').data('object-list-config').elements.gridItem.dataSource.remove(item);
                                            }

                                            $('body').closePageLoader();

                                            $('.object-list').data('object-list-config').elements.gridItem.dataSource.sync();
                                        }
                                        else {

                                        }
                                    }
                                    else {

                                    }
                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    // stuff
                                });

                            
                        }

                    }

                },
                reloadGrid: function () {
                    var filterElement = $.grep($('.object-list').find('.k-filtercell').find('input'), function (inp) { return ($(inp).hasClass('k-input')); });
                    $(filterElement).css('background-color', '');
                    $(filterElement).css('color', '');
                    if ($('.object-list').data('object-list-config').elements.gridItem !== undefined && $('.object-list').data('object-list-config').elements.gridItem !== null) {
                        if ($('.object-list').data('object-list-config').handler.getFilter() !== undefined && $('.object-list').data('object-list-config').handler.getFilter() !== "") {
                            $('.object-list').data('object-list-config').elements.gridItem.dataSource.filter([]);
                        }

                        $('.object-list').data('object-list-config').elements.gridItem.dataSource.read();
                    }
                },
                getFilter: function () {
                    var filter = $('.object-list').data('object-list-config').elements.gridItem.dataSource.filter();
                    if (filter === undefined) return;

                    if ($('.object-list').data('object-list-config').elements.gridItem.dataSource.filter().filters.length > 0) {
                        return $('.object-list').data('object-list-config').elements.gridItem.dataSource.filter().filters[0].value;
                    }
                    else {
                        return "";
                    }

                },
                setIdParent: function (idParent) {
                    $('.object-list').data('object-list-config').idParent = idParent;
                    $('.object-list').data('object-list-config').handler.checkViewState();
                    $('.object-list').data('object-list-config').handler.reloadGrid();
                },
                addGridItem: function (gridItem) {
                    $('.object-list').data('object-list-config').elements.gridItem.dataSource.add(gridItem);
                },
                changeNewItemInput: function (e) {
                    var filterElement = $.grep($('.object-list').find('.k-filtercell').find('input'), function (inp) { return ($(inp).hasClass('k-input')); });
                    $('.object-list').data('object-list-config').handler.checkOrSetNewItem(false);
                    if ($('.object-list').data('object-list-config').configuration.viewModel.newItemTimout !== undefined) {
                        clearTimeout($('.object-list').data('object-list-config').configuration.viewModel.newItemTimout);
                        $(filterElement).css('background-color', '');
                        $(filterElement).css('color', '');
                        $('.object-list').data('object-list-config').handler.checkViewState();
                    }
                    $('.object-list').data('object-list-config').configuration.viewModel.newItemTimout = setTimeout(function () {
                        var itemName = $(filterElement).val();

                        if (itemName === '') {
                            return;
                        }
                        var itemsToCheck = [{
                            Name: itemName,
                            GUID_Parent: $('.object-list').data('object-list-config').idParent,
                            Type: "Object"
                        }];
                        $.post(objectList.configuration.viewModel.ActionCheckExistance, { itemsToCheck: itemsToCheck, idInstance: $('.object-list').data('object-list-config').configuration.viewModel.IdInstance })
                            .done(function (response) {
                                if (response.IsSuccessful) {
                                    $('.object-list').data('object-list-config').handler.checkOrSetNewItem(true);
                                    var itemChecked = response.Result[0];
                                    if (itemChecked.Mark === true) {
                                        $(filterElement).css('background-color', 'yellow');
                                        $(filterElement).css('color', 'black');
                                    }
                                    else {
                                        $(filterElement).css('background-color', 'green');
                                        $(filterElement).css('color', 'white');
                                    }
                                    $('.object-list').data('object-list-config').handler.checkViewState();
                                }

                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });

                    }, 500);
                },
                changeDuplicateObjectInput: function (e) {
                    var objectList = $(e.target).closest('.duplicate-object-dialog').data('object-list-config');
                    var duplicateObjectInput = $(objectList.elements.dialogDuplicateObject).find('.new-name');
                    objectList.handler.checkOrSetDuplicateObjectSave(objectList, false);
                    if (objectList.configuration.viewModel.duplicateObjectTimeOut !== undefined) {
                        clearTimeout(objectList.configuration.viewModel.duplicateObjectTimeOut);
                        $(duplicateObjectInput).css('background-color', '');
                        $(duplicateObjectInput).css('color', '');
                    }
                    objectList.configuration.viewModel.duplicateObjectTimeOut = setTimeout(function () {
                        var itemName = $(duplicateObjectInput).val();

                        if (itemName === '') {
                            $(duplicateObjectInput).css('background-color', '');
                            $(duplicateObjectInput).css('color', '');
                            return;
                        }
                        var itemsToCheck = [{
                            Name: itemName,
                            GUID_Parent: objectList.idParent,
                            Type: "Object"
                        }];
                        $.post(objectList.configuration.viewModel.ActionCheckExistance, { itemsToCheck: itemsToCheck, idInstance: objectList.configuration.viewModel.IdInstance })
                            .done(function (response) {
                                if (response.IsSuccessful) {
                                    $('.object-list').data('object-list-config').handler.checkOrSetDuplicateObjectSave($('.object-list').data('object-list-config'), true);
                                    var itemChecked = response.Result[0];
                                    if (itemChecked.Mark === true) {
                                        $(duplicateObjectInput).css('background-color', 'yellow');
                                        $(duplicateObjectInput).css('color', 'black');
                                    }
                                    else {
                                        $(duplicateObjectInput).css('background-color', 'green');
                                        $(duplicateObjectInput).css('color', 'white');
                                    }
                                }

                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });

                    }, 500, duplicateObjectInput, objectList);
                },
                saveNewItem: function (e) {
                    var filterElement = $.grep($('.object-list').find('.k-filtercell').find('input'), function (inp) { return ($(inp).hasClass('k-input')); });
                    var itemName = $(filterElement).val();

                    if (itemName === '') {
                        return;
                    }
                    var itemsToSave = [{
                        Name: itemName,
                        GUID_Parent: $('.object-list').data('object-list-config').idParent,
                        Type: "Object"
                    }];

                    $.post(objectList.configuration.viewModel.ActionSaveNewItems, { itemsToSave: itemsToSave, idInstance: $('.object-list').data('object-list-config').configuration.viewModel.IdInstance })
                        .done(function (response) {
                            if (response.IsSuccessful) {
                                $(filterElement).val('');
                                $(filterElement).css('background-color', '');
                                $('.object-list').data('object-list-config').handler.checkOrSetNewItem(false);
                                $('.object-list').data('object-list-config').handler.checkViewState();
                                var itemSaved = response.Result[0];
                                $('.object-list').data('object-list-config').handler.addGridItem(itemSaved);
                                $('.object-list').data('object-list-config').elements.gridItem.dataSource.filter({
                                    field: 'IdItem',
                                    operator: 'eq',
                                    value: itemSaved.IdItem
                                });
                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                duplicateObject: function (e) {
                    var objectList = $(e.target).closest('.duplicate-object-dialog').data('object-list-config');
                    var instanceItem = $(e.target).closest('.duplicate-object-dialog').data('selected-instance');
                    var newName = $(objectList.elements.dialogDuplicateObject).find('.new-name').val();

                    $.post(objectList.configuration.viewModel.ActionDuplicateObject, { idObject: instanceItem.IdInstance, newName: newName, idInstance: objectList.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            if (response.IsSuccessful) {
                                var itemSaved = response.Result;
                                $('.object-list').data('object-list-config').handler.addGridItem(itemSaved);
                                $($('.object-list').data('object-list-config').elements.dialogDuplicateObject).data("kendoWindow").close();
                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                cancelDuplicateObject: function (e) {
                    var objectList = $(e.target).closest('.duplicate-object-dialog').data('object-list-config');
                    var name = $(objectList.elements.dialogDuplicateObject).find('.new-name').val();

                    
                    if (typeof name !== 'undefined' && name !== '') {
                        kendo.confirm("Are you sure that you want to proceed?").then(function () {
                            $(objectList.elements.dialogDuplicateObject).data("kendoWindow").close();
                        }, function () {
                            
                        });
                    } else {
                        $(objectList.elements.dialogDuplicateObject).data("kendoWindow").close();
                    }
                    
                },
                openNewItemsWindow: function () {
                    var oItem = {
                        GUID: $('.object-list').data('object-list-config').idParent,
                        Type: 'Class'
                    };
                    $('.object-list').data('object-list-config').configuration.viewModel.addObjects.handler.validateReference(oItem);
                    $('.object-list').data('object-list-config').elements.windowNew.data('kendoWindow').open();
                    $('.object-list').data('object-list-config').configuration.viewModel.addObjects.handler.setButtonPosition();
                }
            },
            elements: {
                grid: null,
                gridItem: null,
                windowNew: null,
                windowEdit: null,
                windowEditOrderId: null,
                windowDelete: null
            },
            templates: {
                duplicateItemDialogTemplate: `
<div class="duplicate-object-dialog">
    <form>
      <div class="form-group">
        <label>New Name</label>
        <input type="text" class="form-control new-name" placeholder="" style="width: 95%;">
      </div>
      <button type="button" class="btn btn-primary btn-duplicate">Duplicate</button>
      <button type="button" class="btn btn-primary btn-cancel-duplicate">Cancel</button>
    </form>
</div>
`
            }
        };

        objectList.elements.grid = $('<div class="objectlist-grid"></div>');
        $(this).addClass('object-list');
        $(this).append(objectList.elements.grid);

        $(this).openPageLoader();

        var newWindowCode = `
            <div class='new-window'>
                <div style="overflow: hidden;" class="new-window-content">
                    <div class="add-objects">

                    </div>
                </div>
            </div>`;

        objectList.elements.windowNew = $(newWindowCode);
        $(this).append(objectList.elements.windowNew);
        objectList.elements.windowNew.kendoWindow({
            width: '1024px',
            height: '600px',
            title: 'New Items',
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Minimize",
                "Maximize",
                "Close"
            ]
        }).data("kendoWindow").center();

        var editWindowCode = `
            <div class='edit-window'>
                <div class="k-content">
                    <ul class="fieldlist">
                        <li>
                            <label class='lbl-guid'>GUID:</label>
                            <input class='txt-guid' type="text" class="k-textbox editControl">
                            <button class='k-button copy-guid'><i class="icon-coppy-guid)" aria-hidden="true"></i></button>

                        </li>
                        <li>
                            <label class='lbl-name'>Value:</label>
                            <input type="text" class="k-textbox editControl inp-name"/>
                        </li>
                        <li>
                            <button class="k-button save-name">Save</button>
                        </li>
                    </ul>
                </div>

                <div class='message-name'></div>

            </div>`;

        objectList.elements.windowEdit = $(editWindowCode);
        $(this).append(objectList.elements.windowEdit);
        $(objectList.elements.windowEdit).kendoWindow({
            width: '350px',
            height: '260px',
            title: 'Edit Name',
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Minimize",
                "Maximize",
                "Close"
            ]
        }).data("kendoWindow").center();


        var editWindowOrderId = `
            <div class='edit-window-orderid'>
                <div class="k-content">
                    <ul class="fieldlist">
                        <li>
                            <label class='lbl-orderid' >Value:</label>
                            <input class='inp-orderid' type="number" value="1" step="1" style="width: 100%;" />

                        </li>
                        <li>
                            <button class="save-orderid k-button">Save</button>
                        </li>
                    </ul>
                </div>
                <div class='message-orderid'></div>
            </div>`;

        objectList.elements.windowEditOrderId = $(editWindowOrderId);
        objectList.elements.dialogDuplicateObject = $(objectList.templates.duplicateItemDialogTemplate);
        $(objectList.elements.dialogDuplicateObject).data('object-list-config', objectList);
        $(this).append(objectList.elements.dialogDuplicateObject);
        $(this).append(objectList.elements.windowEditOrderId);
        objectList.elements.windowEditOrderId.find('.inp-orderid').kendoNumericTextBox({ format: 'n0' });

        $(objectList.elements.dialogDuplicateObject).kendoWindow({
            width: '600px',
            height: '200px',
            title: 'Duplicate Object',
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Minimize",
                "Maximize",
                "Close"
            ]
        }).data("kendoWindow").center();

        $(objectList.elements.dialogDuplicateObject).find('.new-name').bind('input', $(objectList.elements.dialogDuplicateObject).data('object-list-config').handler.changeDuplicateObjectInput);
        $(objectList.elements.dialogDuplicateObject).find('.new-name').bind('paste', $(objectList.elements.dialogDuplicateObject).data('object-list-config').handler.changeDuplicateObjectInput);
        $(objectList.elements.dialogDuplicateObject).find('.btn-duplicate').bind('click', objectList.handler.duplicateObject);
        $(objectList.elements.dialogDuplicateObject).find('.btn-cancel-duplicate').bind('click', objectList.handler.cancelDuplicateObject);
        $(objectList.elements.dialogDuplicateObject).data('object-list-config').handler.checkOrSetDuplicateObjectSave(objectList, false);

        objectList.elements.windowEditOrderId.kendoWindow({
            width: '160px',
            height: '160px',
            title: 'OrderId',
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Minimize",
                "Maximize",
                "Close"
            ]
        }).data("kendoWindow").center();

        var deleteWindow = `
            <div class='delete-window'>
                <div style="overflow: hidden;" class'message-delete'>

                </div>
            </div>`;

        objectList.elements.windowDelete = $(deleteWindow);
        $(this).append(objectList.elements.windowDelete);

        objectList.elements.windowDelete.kendoWindow({
            width: '160px',
            height: '160px',
            title: 'Delete',
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Minimize",
                "Maximize",
                "Close"
            ]
        }).data("kendoWindow").center();

        objectList.elements.dialogAddToClipboardWindow = $('<div class="add-to-clipboard-window"></div>');
        $(this).append(objectList.elements.dialogAddToClipboardWindow);


        $(this).data('object-list-config', objectList);

        $.post($('.object-list').data('object-list-config').configuration.actionObjectListInit)
            .done(function (response) {
                $('.object-list').data('object-list-config').configuration.viewModel = response.Result;
                if ($('.object-list').data('object-list-config').configuration.handler !== undefined && $('.object-list').data('object-list-config').configuration !== null && $('.object-list').data('object-list-config').configuration.handler.initCompleteHandler !== undefined && $('.object-list').data('object-list-config').configuration.handler.initCompleteHandler !== null) {
                    $('.object-list').data('object-list-config').configuration.handler.initCompleteHandler(objectList, objectList.configuration.parentComponent);
                }

                $.post(objectList.configuration.viewModel.ActionGetGridConfig)
                    .done(function (response) {
                        if ($('.object-list').data('object-list-config').configuration.useIdParent) {
                            $.extend(response.dataSource.transport.read, {
                                data: function () {
                                    return { idParent: $('.object-list').data('object-list-config').idParent, idInstance: $('.object-list').data('object-list-config').configuration.viewModel.IdInstance, nameFilter: $('.object-list').data('object-list-config').handler.getFilter() };
                                }
                            });
                        }
                        else {
                            $.extend(response.dataSource.transport.read, {
                                data: function () {
                                    return { idInstance: $('.object-list').data('object-list-config').configuration.viewModel.IdInstance };
                                }
                            });
                        }

                        response.dataSource.transport.parameterMap = function (data, type) {
                            if (type == "destroy") {
                                var deleteItems = {
                                    models: []
                                };
                                data.models.forEach(function (item) {
                                    deleteItems.models.push({ IdInstance: item.IdInstance });
                                });
                                return deleteItems;
                            }
                            if (type == "read") {
                                return data;
                            }
                            if (type == "update") {
                                var updateItems = {
                                    models: []
                                };
                                data.models.forEach(function (item) {
                                    updateItems.models.push({ IdInstance: item.IdInstance, NameInstance: item.NameInstance });
                                });
                                return updateItems;
                            }
                        };
                        
                        $.extend(response.dataSource, {
                            batch: true
                        });

                        $.extend(response.dataSource.transport.update,
                            {
                                complete: function (e) {
                                    console.log(e);
                                }
                            }
                        );

                        $.extend(response.dataSource.transport.destroy,
                            {
                                complete: function (e) {
                                    console.log(e);
                                }
                            }
                        );

                        $.extend(response.dataSource.transport.read,
                            {
                                complete: function (e) {
                                    console.log(e);
                                }
                            }
                        );

                        var dataSource = new kendo.data.DataSource(response.dataSource);
                        dataSource.bind("error", function (e) {
                            console.log(e);
                            $('.object-list').data('object-list-config').elements.gridItem.cancelChanges();
                            var dialogElement = $('#dialog');
                            dialogElement.kendoDialog({
                                width: "300px",
                                height: "150px",
                                title: "Value",
                                closable: true,
                                modal: false,
                                content: "An error occured!",
                                actions: []
                            });

                            dialogElement.data("kendoDialog").open();
                        });
                        response.dataSource = dataSource;
                        response.dataBound = function () {


                            var pagerElement = this.pager.element;
                            if (!pagerElement.find(".idInput").length) {
                                pagerElement
                                    .append('<span><input class="k-textbox idInput" placeholder="GUID" readonly></input></span>');
                            }

                            $('.object-list').find('.chkbxApply').change(function () {
                                var row = $(this).closest('tr');
                                var item = $('.object-list').data('object-list-config').elements.gridItem.dataItem(row);
                                var applied = $(this).prop("checked");
                                item.Apply = applied;
                                if (item.Apply) {
                                    $('.object-list').data('object-list-config').handler.addApplyItem(item);
                                } else {
                                    $('.object-list').data('object-list-config').handler.removeApplyItem(item);
                                }

                                console.log(item);
                            });

                            $('.object-list').find('.button-object-edit').click(function () {
                                var row = $(this).closest('tr');
                                var item = $('.object-list').data('object-list-config').elements.gridItem.dataItem(row);
                                var url = $('.object-list').data('object-list-config').configuration.viewModel.ActionRelationEditor;
                                url += "?Object=" + item.IdInstance;
                                window.open(url, "_blank");
                            });

                            $('.object-list').find('.button-module-starter').click(function () {
                                var row = $(this).closest('tr');
                                var item = $('.object-list').data('object-list-config').elements.gridItem.dataItem(row);
                                var url = $('.object-list').data('object-list-config').configuration.viewModel.ActionModuleStarter;
                                url += "?Object=" + item.IdInstance;
                                window.open(url, "_blank");
                            });

                            $('.object-list').find('.button-object-duplicate').click(function () {
                                var row = $(this).closest('tr');
                                var item = $('.object-list').data('object-list-config').elements.gridItem.dataItem(row);
                                $(objectList.elements.dialogDuplicateObject).find('.new-name').val('');
                                $(objectList.elements.dialogDuplicateObject).data('selected-instance', item);
                                objectList.handler.checkOrSetDuplicateObjectSave(objectList, false);
                                $(objectList.elements.dialogDuplicateObject).find('.new-name').val(item.NameInstance);
                                $(objectList.elements.dialogDuplicateObject).data("kendoWindow").open();
                            });
                        };
                        $.extend(response, {
                            change: $('.object-list').data('object-list-config').handler.gridChangeHandler,
                            filterable: {
                                mode: "row"
                            },
                            filter: function (e) {
                                if (e.filter === null) {
                                    var filterElement = $.grep($('.object-list').find('.k-filtercell').find('input'), function (inp) { return ($(inp).hasClass('k-input')); });
                                    $(filterElement).css('background-color', '');
                                    $(filterElement).css('color', '');
                                    $('.object-list').data('object-list-config').handler.checkOrSetNewItem(false);
                                }
                            },
                            cellClose: function (e) {
                                e.model.IsSaved = false;
                                $('.object-list').data('object-list-config').elements.gridItem.saveChanges();
                            }
                        });
                        var template = $('.object-list').data('object-list-config').configuration.templateSelector;
                        if (typeof template === "undefined") {
                            template = "#template";
                        }
                        $.extend(response, { toolbar: kendo.template($(template).html()) });
                        $('.object-list').data('object-list-config').elements.gridItem = $($('.object-list').data('object-list-config').elements.grid).kendoGrid(response).data("kendoGrid");

                        $($('.object-list').data('object-list-config').elements.grid).kendoTooltip({
                            filter: ".name-cell",
                            position: "top",
                            animation: {
                                open: {
                                    effects: "zoom",
                                    duration: 150
                                }
                            },
                            showAfter: 500,
                            width: "500",
                            autoHide: true,
                            content: {
                                url: objectList.configuration.viewModel.ActionGetTransformedNames
                            },
                            requestStart: function (e) {
                                var objectIds = [];
                                var idItem = $('.object-list').data('object-list-config').elements.gridItem.dataItem(e.target.parent()).IdInstance;
                                if (typeof idItem === 'undefined') {
                                    e.options.url = '';
                                } else {
                                    objectIds.push(idItem);
                                    e.options.type = "POST";
                                    e.options.data = { idParent: $('.object-list').data('object-list-config').idParent, objectIds: objectIds, idInstance: $('.object-list').data('object-list-config').configuration.viewModel.IdInstance }
                                }
                            }, contentLoad: function () {
                                var tooltipItem = $.parseJSON(this.content.html());

                                if (tooltipItem.Result.length > 0) {
                                    var encodedContent = tooltipItem.Result[0].Val_String
                                    var decodedContent = b64_to_utf8(encodedContent);
                                    const options = { defaultProtocol: 'https' };
                                    decodedContent = linkifyHtml(decodedContent, options);
                                    $(this.content).html(decodedContent);

                                }
                                else {
                                    $(this.content).html('');
                                }
                                
                            }
                        }).data("kendoTooltip");

                        $('.add-objects').createAddSingleObject({
                            actionAddSingleObjectInit: $('.object-list').data('object-list-config').configuration.viewModel.ActionAddObjectsInit,
                            managerConfig: $('.object-list').data('object-list-config').configuration.managerConfig,
                            events: {
                                createCompleted: function (addObjectsContainer) {
                                    $('.object-list').data('object-list-config').configuration.viewModel.addObjects = addObjectsContainer;
                                    
                                },
                                applyItems: function (items) {
                                    $('.object-list').data('object-list-config').elements.windowNew.data('kendoWindow').close();
                                    if (typeof $('.object-list').data('object-list-config').configuration.events !== 'undefined' &&
                                        typeof $('.object-list').data('object-list-config').configuration.events.applyItems !== 'undefined') {
                                        $('.object-list').data('object-list-config').configuration.events.applyItems(items, $('.object-list').data('object-list-config').configuration.parentComponent);
                                    }
                                    else {
                                        $('.object-list').data('object-list-config').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                            ChannelId: $('.object-list').data('object-list-config').configuration.viewModel.ChannelAppliedIObjects,
                                            SenderId: $('.object-list').data('object-list-config').configuration.viewModel.IdEndpoint,
                                            SessionId: $('.object-list').data('object-list-config').configuration.viewModel.IdInstance,
                                            UserId: $('.object-list').data('object-list-config').configuration.viewModel.IdUser,
                                            OItems: items
                                        });
                                    }
                                    
                                }
                            }
                        });

                        changeValuesFromServerChildren($('.object-list').data('object-list-config').configuration.viewModel.ViewItems, ['.object-list']);
                        var filterElement = $.grep($('.object-list').find('.k-filtercell').find('input'), function (inp) { return ($(inp).hasClass('k-input')); });

                        $(filterElement).unbind('input', $('.object-list').data('object-list-config').handler.changeNewItemInput);
                        $(filterElement).unbind('paste', $('.object-list').data('object-list-config').handler.changeNewItemInput);
                        $(filterElement).bind('input', $('.object-list').data('object-list-config').handler.changeNewItemInput);
                        $(filterElement).bind('paste', $('.object-list').data('object-list-config').handler.changeNewItemInput);

                        $(filterElement).unbind('dblclick', $('.object-list').data('object-list-config').handler.openNewItemsWindow);
                        $(filterElement).bind('dblclick', $('.object-list').data('object-list-config').handler.openNewItemsWindow);
                        
                        $('.object-list').find('.newItem').unbind('click', $('.object-list').data('object-list-config').handler.saveNewItem);
                        $('.object-list').find('.newItem').bind('click', $('.object-list').data('object-list-config').handler.saveNewItem);

                        $('.object-list').data('object-list-config').elements.gridItem.one("dataBound", function (e) {
                            var filterElement = $.grep($('.object-list').find('.k-filtercell').find('input'), function (inp) { return ($(inp).hasClass('k-input')); });
                            $(filterElement).css('background-color', '');
                            $(filterElement).css('color', '');
                            $('.object-list').data('object-list-config').handler.checkOrSetNewItem(false);

                            var filterCell = e.sender.thead.find(".k-filtercell[data-field='NameInstance']");


                            if (filterCell.length === 0) return;
                            var filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                            filterDropDown.select(3);
                            filterDropDown.trigger("change");

                            $('.object-list').find('.k-button').kendoButton({
                                click: $('.object-list').data('object-list-config').handler.clickFunctionButtonHandler
                            });
                        });

                        $('.object-list').data('object-list-config').handler.reloadGrid();

                        if ($('.object-list').data('object-list-config').configuration.callbackGridCreated !== undefined && $('.object-list').data('object-list-config').configuration.callbackGridCreated !== null) {
                            $('.object-list').data('object-list-config').configuration.callbackGridCreated($('.object-list').data('object-list-config').elements.gridItem);
                        }

                        $('.object-list').data('object-list-config').elements.grid.find('.header-checkbox').change(function (ev) {
                            var checked = ev.target.checked;
                            $('.object-list').find('.chkbxApply').each(function (idx, item) {
                                $(item).click();
                            });
                        });

                        $('body').closePageLoader();

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
        


    };
})(jQuery);