﻿(function ($) {
    $.fn.createAttributeRelation = function (configuration) {
        var relationListAttribute = {
            configuration: configuration,
            components: {

            },
            elements: {
                relationListAttribute: null
            },
            data: {
                itemsPreApply: []
            },
            handler: {
                createComponent: function (relationListAttribute) {
                    relationListAttribute.elements.editAttribute = $(relationListAttribute.templates.editAttribute);
                    $(relationListAttribute.elements.relationListContainer).append(relationListAttribute.elements.editAttribute);
                    relationListAttribute.elements.editAttribute.kendoWindow({
                        width: '1024px',
                        height: '600px',
                        title: 'Edit Attribute',
                        visible: false,
                        modal: true,
                        actions: [
                            "Pin",
                            "Minimize",
                            "Maximize",
                            "Close"
                        ]
                    }).data("kendoWindow").center();

                    relationListAttribute.elements.gridParent = $(relationListAttribute.templates.gridParent);
                    $(relationListAttribute.elements.relationListContainer).append(relationListAttribute.elements.gridParent);

                    relationListAttribute.elements.grid = $(relationListAttribute.templates.grid);
                    $(relationListAttribute.elements.gridParent).append(relationListAttribute.elements.grid);

                    $.post(relationListAttribute.configuration.viewModel.ActionGetGridConfig, { idInstance: relationListAttribute.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            var element = getElementsBySelectorPath(response.SelectorPath);
                            var relationListAttribute = $(element).data('relation-list-attribute');
                            if (!relationListAttribute.configuration.useIdParent) {
                                $.extend(response.Result.dataSource.transport.read,
                                    {
                                        relationListAttributeComponent: relationListAttribute,
                                        data: function (e) {
                                            return {
                                                idInstance: this.relationListAttributeComponent.configuration.viewModel.IdInstance,
                                                idObject: this.relationListAttributeComponent.data.idObject,
                                                idAttributeType: this.relationListAttributeComponent.data.idAttributeType
                                            };
                                        }
                                    });
                            } 

                            $.extend(response.Result.dataSource, {
                                batch: true
                            });

                            response.Result.dataSource.transport.parameterMap = function (data, type) {
                                if (type == "destroy") {
                                    var deleteItems = {
                                        models: []
                                    };
                                    data.models.forEach(function (item) {
                                        deleteItems.models.push({ IdAttribute: item.IdAttribute });
                                    });
                                    return deleteItems;
                                }
                                if (type == "read") {
                                    return data;
                                }
                                if (type == "update") {
                                    var updateItems = {
                                        models: []
                                    };
                                    data.models.forEach(function (item) {
                                        updateItems.models.push({ IdAttribute: item.IdAttribute, OrderId: item.OrderId });
                                    });
                                    return updateItems;
                                }
                            };

                            $.extend(response.Result.dataSource.transport.update,
                                {
                                    complete: function (e) {
                                        console.log(e);
                                    }
                                }
                            );

                            relationListAttribute.data.dataSource = new kendo.data.DataSource(response.Result.dataSource);
                            response.Result.dataSource = relationListAttribute.data.dataSource;
                            response.Result.dataBound = function () {
                                var relationListAttribute = $($(this)[0].element).parent().parent().data('relation-list-attribute');
                                
                                $(relationListAttribute.elements.grid).find('.chkbxApply').change(function () {
                                    var relationListAttribute = $(this).closest('.object-attribute-list').data('relation-list-attribute');

                                    var row = $(this).closest('tr');
                                    var item = relationListAttribute.components.gridItem.dataItem(row);
                                    var applied = $(this).prop("checked");
                                    item.Apply = applied;
                                    if (item.Apply) {
                                        relationListAttribute.handler.addApplyItem(relationListAttribute, item);
                                    } else {
                                        relationListAttribute.handler.removeApplyItem(relationListAttribute, item);
                                    }

                                    console.log(item);
                                });

                                if ($(relationListAttribute.elements.grid).find('.k-button').data('kendoButton') === undefined) {
                                    $(relationListAttribute.elements.grid).find('.k-button').kendoButton({
                                        click: relationListAttribute.handler.clickButtonHandler
                                    });
                                }

                                $(relationListAttribute.elements.grid).find('.button-object-atribute-edit').click(function () {
                                    
                                });

                            };
                            $.extend(response.Result, {
                                change: relationListAttribute.handler.gridChangeHandler,
                                filterable: {
                                    mode: "row"
                                },
                                cellClose: function (e) {
                                    e.model.IsSaved = false;
                                    var relationListAttribute = $(this.element).parent().parent().data('relation-list-attribute');
                                    relationListAttribute.components.gridItem.saveChanges();
                                }
                            });
                            $.extend(response.Result, { toolbar: kendo.template($(relationListAttribute.templates.toolbar).html()) });
                            relationListAttribute.components.gridItem = relationListAttribute.elements.grid.kendoGrid(response.Result).data("kendoGrid");


                            relationListAttribute.components.gridItem.one("dataBound", function (e) {
                                //var filterCell = e.sender.thead.find(".k-filtercell[data-field='Value']");
                                //if (filterCell.length !== 0) {
                                //    filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                                //    filterDropDown.select(10);
                                //    filterDropDown.trigger("change");
                                //}

                            });

                            relationListAttribute.components.gridItem.wrapper.find(".k-pager")
                                .before('<a href="#" class="k-link prev">Prev</a>')
                                .after('<a href="#" class="k-link next">Next</a>')
                                .parent()
                                .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                                .delegate("a.prev", "click", function () {
                                    dataSource.page(dataSource.page() - 1);
                                })
                                .delegate("a.next", "click", function () {
                                    dataSource.page(dataSource.page() + 1);
                                })
                                .delegate(".pagesize input", "change", function () {
                                    dataSource.page(parseInt(this.value, 10));
                                });



                            $('body').closePageLoader();

                            if (!relationListAttribute.configuration.useIdParent) {
                                relationListAttribute.components.gridItem.dataSource.read();
                            }

                            if (typeof relationListAttribute.configuration.events !== 'undefined' && relationListAttribute.configuration.events !== null &&
                                typeof relationListAttribute.configuration.events.createdComponent !== 'undefined' && relationListAttribute.configuration.events.createdComponent !== null) {
                                relationListAttribute.configuration.events.createdComponent(relationListAttribute);
                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                addApplyItem: function (relationListAttribute, item) {
                    relationListAttribute.data.itemsPreApply.push(item);
                    relationListAttribute.handler.checkViewState(relationListAttribute);
                },
                removeApplyItem: function (relationListAttribute, item) {
                    relationListAttribute.data.itemsPreApply = jQuery.grep(relationListAttribute.data.itemsPreApply, function (itemInArray) {
                        return itemInArray.IdItem !== item.IdItem
                    });
                    relationListAttribute.handler.checkViewState(relationListAttribute);
                },
                setNodeItem: function (relationListAttribute, nodeItem) {
                    relationListAttribute.data.nodeItem = nodeItem;
                    relationListAttribute.handler.checkViewState(relationListAttribute);
                },
                checkViewState: function (relationListAttribute) {
                    relationListAttribute.elements.editAttribute.find('.newItem').setEnabled(relationListAttribute.data.idParent !== undefined, true);
                    relationListAttribute.elements.editAttribute.find('.applyItem').setEnabled(relationListAttribute.data.itemsPreApply.length > 0, true);
                    relationListAttribute.elements.editAttribute.find('.addToClipboard').setEnabled(relationListAttribute.data.itemsPreApply.length > 0, true);
                    relationListAttribute.elements.editAttribute.find('.deleteItem').setEnabled(relationListAttribute.data.itemsPreApply.length > 0, true);
                    relationListAttribute.elements.editAttribute.find('.orderAsc').setEnabled(relationListAttribute.data.itemsPreApply.length > 0, true);
                    relationListAttribute.elements.editAttribute.find('.orderDesc').setEnabled(relationListAttribute.data.itemsPreApply.length > 0, true);

                },
                gridChangeHandler: function (e) {
                    e.preventDefault();
                    var relationListAttribute = $(this.element).parent().parent().data('relation-list-attribute');
                    var index = relationListAttribute.components.gridItem.select().index();
                    var col = this.options.columns[index].field;
                    var currentDataItem = relationListAttribute.components.gridItem.dataItem(relationListAttribute.components.gridItem.select().parent());
                    relationListAttribute.data.idAttribute = currentDataItem.IdAttribute;
                    relationListAttribute.data.nameDataType = currentDataItem.NameDataType;
                    if (currentDataItem !== undefined) {
                        console.log(currentDataItem);
                        relationListAttribute.data.dataItem = currentDataItem;
                        if (col === 'Value') {
                            relationListAttribute.handler.editAttribute(relationListAttribute);
                        }
                    }
                    if (typeof (relationListAttribute.configuration.events.dataBound) !== 'undefined' && relationListAttribute.configuration.events.dataBound !== null) {
                        relationListAttribute.configuration.events.dataBound.events.dataBound();
                    }

                },
                editAttribute: function (relationListAttribute) {
                    var configuration = {
                        setDocumentTitle: true,
                        parentComponent: relationListAttribute,
                        selectorPath: '.editAttribute',
                        actionInit: relationListAttribute.configuration.viewModel.ActionEditAttributeInit,
                        managerConfig: relationListAttribute.configuration.managerConfig,
                        events: {
                            createdComponent: function (editAttributeComponent) {
                                var relationListAttribute = editAttributeComponent.configuration.parentComponent;
                                editAttributeComponent.handler.validateReference(editAttributeComponent,
                                    relationListAttribute.data.idAttribute,
                                    relationListAttribute.data.idObject,
                                    relationListAttribute.data.idAttributeType);
                            },
                            changedValue: function (editAttributeComponent, changedAttribute, valueMember, value, valueNamed) {
                                editAttributeComponent.configuration.parentComponent.components.gridItem.dataSource.read();
                            }
                        }
                    }

                    $(relationListAttribute.elements.editAttribute).createEditAttribute(configuration);
                    $(relationListAttribute.elements.editAttribute).data("kendoWindow").open();
    
                },
                clickButtonHandler: function (e) {
                    var button = $(e.event.target).closest(".k-button");
                    if (button.hasClass('applyItem')) {
                        var items = [];
                        $('.relation-list-attribute').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var item = $('.relation-list-attribute').data('relation-list-attribute').components.gridItem.dataItem(row);
                            console.log(item);
                            items.push({ GUID: item.IdOther });
                        });

                        console.log(items);
                        $('.relation-list-attribute').data('relation-list-attribute').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                            ChannelId: $('.relation-list-attribute').data('relation-list-attribute').configuration.channelAppliedItems,
                            SenderId: $('.relation-list-attribute').data('relation-list-attribute').configuration.managerConfig.idEndpoint,
                            SessionId: $('.relation-list-attribute').data('relation-list-attribute').configuration.managerConfig.idSession,
                            UserId: $('.relation-list-attribute').data('relation-list-attribute').configuration.idUser,
                            OItems: items
                        });

                    } else if (button.hasClass('isListen')) {
                        button.toggleMode(true);
                    } else if (button.hasClass('orderAsc')) {
                        var value = button.parent().find('.inpOrderId').getContent();
                        items = [];
                        $('.relation-list-attribute').find('.chkbxApply:checked').closest('tr').each(function () {
                            var row = $(this);
                            var item = $('.relation-list-attribute').data('relation-list-attribute').components.gridItem.dataItem(row);
                            console.log(item);
                            items.push({
                                uid: item.uid,
                                IdInstance: $('.relation-list-attribute').data('relation-list-attribute').configuration.idInstance,
                                IdItem: item.IdItem,
                                IdObject: item.IdObject,
                                IdOther: item.IdOther,
                                IdRelationType: item.IdRelationType,
                                OrderId: value++
                            });
                        });

                        $.post($('.relation-list-attribute').data('relation-list-attribute').configuration.actionSetOrderId, { requestList: items })
                            .done(function (response) {
                                console.log(response);
                                if (response.IsSuccessful) {
                                    for (var ix in items) {
                                        var item = items[ix];
                                        var row = $('.relation-list-attribute').data('relation-list-attribute').components.gridItem.tbody.find("tr[data-uid='" + item.uid + "']");
                                        var dataItem = $('.relation-list-attribute').data('relation-list-attribute').components.gridItem.dataItem(row);
                                        dataItem.set("OrderId", item.OrderId);

                                    }
                                }


                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });


                    } else if (button.hasClass('deleteItem')) {
                        var deleteItems = [];
                        if ($('.relation-list-attribute').find('.chkbxApply:checked').closest('tr').length > 0 && confirm("Do you want to delete checked rows?")) {
                            $('.relation-list-attribute').find('.chkbxApply:checked').closest('tr').each(function () {
                                var row = $(this);
                                var dataItem = $('.relation-list-attribute').data('relation-list-attribute').components.gridItem.dataItem(row);
                                deleteItems.push(dataItem);

                                //$('.relation-list-attribute').data('relation-list-attribute').gridItem.removeRow(row);

                            });

                            for (var ix in deleteItems) {
                                var item = deleteItems[ix];
                                $('.relation-list-attribute').data('relation-list-attribute').components.gridItem.dataSource.remove(item);
                            }

                            $('.relation-list-attribute').data('relation-list-attribute').components.gridItem.dataSource.sync();
                        }

                    } else if (button.hasClass('addToClipboard')) {
                        var clipboardItems = [];
                        if ($('.relation-list-attribute').find('.chkbxApply:checked').closest('tr').length > 0) {
                            $('.relation-list-attribute').find('.chkbxApply:checked').closest('tr').each(function () {
                                var row = $(this);
                                var dataItem = $('.relation-list-attribute').data('relation-list-attribute').components.gridItem.dataItem(row);
                                var clipboardItem = {
                                    GUID: dataItem.IdOther,
                                    Name: dataItem.NameOther,
                                    GUID_Parent: dataItem.IdParentOther,
                                    Type: dataItem.Ontology
                                };
                                clipboardItems.push(clipboardItem);

                            });

                            $.post($('.relation-list-attribute').data('relation-list-attribute').configuration.actionAddToClipboard, { oItems: clipboardItems, idInstance: $('.relation-list-attribute').data('relation-list-attribute').configuration.idInstance })
                                .done(function (response) {

                                    $('.relation-list-attribute').data('relation-list-attribute').elements.dialogAddToClipboardWindow.kendoDialog({
                                        width: "450px",
                                        title: "Add to Clipboard Result",
                                        closable: false,
                                        modal: false,
                                        content: "<p>Added " + response.Result.length + " Items<p>",
                                        actions: [
                                            { text: 'OK', primary: true }
                                        ],
                                        close: function (e) {

                                        }
                                    });
                                    $('.relation-list-attribute').data('relation-list-attribute').elements.dialogAddToClipboardWindow.data('kendoDialog').open();
                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    // stuff
                                });
                        }
                    }


                },
                validateReference: function (relationListAttribute, idObject, idAttributeType) {
                    relationListAttribute.data.idObject = idObject;
                    relationListAttribute.data.idAttributeType = idAttributeType;
                    relationListAttribute.components.gridItem.dataSource.read();
                }
            },
            templates: {
                editAttribute: '<div class="editAttribute"></div>',
                gridParent: '<div class="gridParent"></div>',
                grid: '<div class="grid"></div>',
                toolbar: `
                    <script type="text/x-kendo-template" id="template">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button class="k-button newItem"><i class="fa fa-file" aria-hidden="true"></i></button>
                            <button class="k-button deleteItem"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>
                            <button class="k-button applyItem"><i class="fa fa-check-circle" aria-hidden="true"></i></button>
                            <button class="k-button addToClipboard"><i class="fa fa-clipboard" aria-hidden="true"></i></button>
                            <button class="k-button isListen"><i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></button>
                        </div>
                    </script>
                `
            }
        };

        $(this).addClass('object-attribute-list');
        $(this).data('relation-list-attribute', relationListAttribute);
        relationListAttribute.elements.relationListContainer = $(this);

        $.post(relationListAttribute.configuration.actionInit, { idInstance: relationListAttribute.configuration.idInstance, selectorPath: relationListAttribute.configuration.selectorPath })
            .done(function (response) {
                // stuff
                console.log(response);
                if (response.IsSuccessful) {
                    var selectorPath = response.SelectorPath;
                    var element = getElementsBySelectorPath(selectorPath);
                    var relationListAttribute = $(element).data('relation-list-attribute');
                    relationListAttribute.configuration.viewModel = response.Result;
                    relationListAttribute.handler.createComponent(relationListAttribute);
                } else {

                }

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });




    }
})(jQuery);