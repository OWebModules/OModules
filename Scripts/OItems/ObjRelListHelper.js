﻿var viewModel;

function initViewModel() {
    viewModel.addApplyItem = function (item) {
        viewModel._itemsPreApply.push(item);
        viewModel.checkViewState();
    };
    viewModel.removeApplyItem = function (item) {
        viewModel._itemsPreApply = jQuery.grep(viewModel._itemsPreApply, function (itemInArray) {
            return itemInArray.IdItem != item.IdItem
        });

        viewModel.checkViewState();
    };
}


function initControls(actionGetGridConfig,
    idApply,
    idListen,
    useIdParent,
    gridChangeHandler,
    clickApplyButtonHandler,
    dataBoundHandler) {
    var cellCloseHandler = function (e) {
        e.model.IsSaved = false;
        viewModel.gridItem.saveChanges();
    }

    $('body').openPageLoader();
    $.post(actionGetGridConfig)
        .done(function (response) {
            if (useIdParent) {
                $.extend(response.dataSource.transport.read, {
                    data: function () {
                        if (viewModel.nodeItem != undefined) {
                            return {
                                IdLeft: viewModel.nodeItem.IdLeft,
                                NameLeft: viewModel.nodeItem.NameLeft,
                                IdRight: viewModel.nodeItem.IdRight,
                                NameRight: viewModel.nodeItem.NameRight,
                                IdRelationType: viewModel.nodeItem.IdRelationType,
                                NameRelationType: viewModel.nodeItem.NameRelationType,
                                NodeType: viewModel.nodeItem.NodeType,
                                IdInstance: viewModel.IdInstance
                            };
                        } else {
                            return {
                                IdInstance: viewModel.IdInstance
                            }
                        }
                        
                    }
                })
            }

            $.extend(response.dataSource, {
                batch: true
            });
            
            $.extend(response.dataSource.transport.update,
                {
                    complete: function (e) {
                        console.log(e);
                    }
                }
            );
            
            var dataSource = new kendo.data.DataSource(response.dataSource);
            response.dataSource = dataSource;
            response.dataBound = function () {
                if (dataBoundHandler != undefined) {
                    dataBoundHandler();
                }
                $('.chkbxApply').change(function () {

                    var row = $(this).closest('tr');
                    var item = viewModel.gridItem.dataItem(row);
                    var applied = $(this).prop("checked");
                    item.Apply = applied;
                    if (item.Apply) {
                        viewModel.addApplyItem(item);
                    } else {
                        viewModel.removeApplyItem(item);
                    }

                    console.log(item);
                });

            };
            $.extend(response, {
                change: gridChangeHandler,
                filterable: {
                    mode: "row"
                },
                cellClose: cellCloseHandler
            });
            $.extend(response, { toolbar: kendo.template($("#template").html()) });
            viewModel.gridItem = $("#" + viewModel.grid.ViewItemId).kendoGrid(response).data("kendoGrid");
            $('.k-button').kendoButton({
                click: clickApplyButtonHandler
            });

            viewModel.gridItem.one("dataBound", function (e) {
                var filterCell = e.sender.thead.find(".k-filtercell[data-field='NameObject']");
                if (filterCell.length != 0) {
                    var filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                    filterDropDown.select(3);
                    filterDropDown.trigger("change");
                }

                filterCell = e.sender.thead.find(".k-filtercell[data-field='NameOther']");
                if (filterCell.length != 0) {
                    var filterDropDown = filterCell.find("[data-role='dropdownlist']").data("kendoDropDownList");
                    filterDropDown.select(3);
                    filterDropDown.trigger("change");
                }
            });

            viewModel.gridItem.wrapper.find(".k-pager")
                .before('<a href="#" class="k-link prev">Prev</a>')
                .after('<a href="#" class="k-link next">Next</a>')
                .parent()
                .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                .delegate("a.prev", "click", function () {
                    dataSource.page(dataSource.page() - 1);
                })
                .delegate("a.next", "click", function () {
                    dataSource.page(dataSource.page() + 1);
                })
                .delegate(".pagesize input", "change", function () {
                    dataSource.page(parseInt(this.value, 10));
                })

            

            $('body').closePageLoader();
            viewModel.setDataItems();

            for (var ix in viewModel.ViewItems) {
                var viewItem = viewModel.ViewItems[ix];

                for (var jx in viewItem.ViewItemValues) {
                    var viewItemValue = viewItem.ViewItemValues[jx];

                    if (viewItemValue.ViewItemType == "Enable") {
                        $('#' + viewItem.ViewItemId).setEnabled(viewItemValue.Value, false);
                    }
                    else if (viewItemValue.ViewItemType == "Visible") {
                        $('#' + viewItem.ViewItemId).setVisible(viewItemValue.Value, false);
                    }
                    else if (viewItemValue.ViewItemType == "Checked") {
                        $('#' + viewItem.ViewItemId).setCheckedMode(viewItemValue.Value, false);
                    }
                    else if (viewItemValue.ViewItemType == "Content") {
                        $('#' + viewItem.ViewItemId).setContent(viewItemValue.Value, false);
                    }
                }
            }

            viewModel.getUrlParameters();

            if (!useIdParent) {
                viewModel.reloadGrid();
            }
            
        })
        .fail(function (jqxhr, textStatus, error) {
            // stuff
        });



    $('#' + viewModel.newWindowViewModel.id).kendoWindow({
        width: '600px',
        height: '600px',
        title: 'New Item',
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();

    $('#' + viewModel.editWindowViewModel.id).kendoWindow({
        width: '350px',
        height: '260px',
        title: 'Edit Name',
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();
    //$('#editNameItemWindow').data("kendoWindow").open();

    $('#' + viewModel.editWindowViewModel.idInpValueEditOrderId).kendoNumericTextBox({ format: 'n0' });

    $('#' + viewModel.editOrderWindowViewModel.id).kendoWindow({
        width: '160px',
        height: '160px',
        title: 'OrderId',
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();


    $('#' + viewModel.deleteWindowViewModel.id).kendoWindow({
        width: '160px',
        height: '160px',
        title: 'Delete',
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();

    var slide = kendo.fx($("#slide-in-share")).slideIn("left");
    var slideVisible = true;

    $("#slide-in-handle").click(function (e) {
        if (slideVisible) {
            slide.reverse();
        } else {
            slide.play();
        }
        slideVisible = !slideVisible;
        e.preventDefault();
    });


}