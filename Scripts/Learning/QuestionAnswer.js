﻿var questionAnswerContainer;
var defaultCateogry;
(function ($) {
    $.fn.createQestionAnswerComponent = function (configuration) {
        $('body').openPageLoader();
        $(this).data("question-answer-config", {
            elements: {
                questionAnswerContainerElement: null,
                questionAnswerElement: null
            },
            configuration: configuration,
            handler: {
                validateReference: function (oItem) {
                    $('body').openPageLoader();
                    questionAnswerContainer.configuration.viewModel.oItem = oItem;
                    questionAnswerContainer.handler.createControl(questionAnswerContainer.elements.questionAnswerContainerElement);
                    $.post(questionAnswerContainer.configuration.viewModel.ActionValidateReference, { refItem: oItem, idInstance: questionAnswerContainer.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            console.log("connected", response);
                            changeValuesFromServerChildren(response.ViewItems, [ ".question-answer" ]);

                            if (response.Result == 1) {// Typed-Tagging
                                var src = questionAnswerContainer.configuration.viewModel.ActionHtmlViewer + '?Object=' + questionAnswerContainer.configuration.viewModel.oItem.GUID + '&Sender=' + questionAnswerContainer.configuration.viewModel.managerConfig.idEndpoint;
                                $('.doc-viewer').find('iframe').attr('src', src);   
                            }
                            else if (response.Result = 2) { // Media-Item
                                var src = questionAnswerContainer.configuration.viewModel.ActionMediaBookmarkManager + '?Object=' + questionAnswerContainer.configuration.viewModel.oItem.GUID + '&Sender=' + questionAnswerContainer.configuration.viewModel.managerConfig.idEndpoint;
                                $('.doc-viewer').find('iframe').attr('src', src);
                            }
                            questionAnswerContainer.handler.getQuestions();
                            
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                            $('body').closePageLoader();
                        });
                },
                getQuestionHtml: function (question) {
                    var questionHtml = questionTemplate.replace("@ID_QUESTIION@", question.IdQuestion);
                    questionHtml = questionHtml.replace("@HTML@", question.QuestionHtml);
                    questionHtml = questionHtml.replace("@WIDTH@", question.WidthNumber);
                    questionHtml = questionHtml.replace("@ID@", question.Id);
                    var categoryCount = '';
                    for (var ix in question.CategoryCounts) {
                        var catCount = question.CategoryCounts[ix];
                        if (categoryCount != '') {
                            categoryCount += '|';
                        }
                        categoryCount += catCount.Text;
                    }
                    var countString = question.NumberCategoriesInSequence + '/' + question.CountOfAnswers
                    questionHtml = questionHtml.replace("@QUESTION_STATE@", question.Category).replace("@STATE_STYLE@", question.BadgeClass).replace("@LAST_CATEGORY_NUMBER@", countString).replace("@CATEGORY_COUNT@", categoryCount);
                    return questionHtml;
                },
                setQuestionHtml: function (question) {
                    var questionElement = $('#' + question.IdQuestion);
                    var categoryCount = '';
                    for (var ix in question.CategoryCounts) {
                        var catCount = question.CategoryCounts[ix];
                        if (categoryCount != '') {
                            categoryCount += '|';
                        }
                        categoryCount += catCount.Text;
                    }
                    var countString = question.NumberCategoriesInSequence + '/' + question.CountOfAnswers

                    var stateBadge = $(questionElement).find('.state-badge');
                    $(stateBadge).removeClass('badge-info');
                    $(stateBadge).removeClass('badge-secondary');
                    $(stateBadge).removeClass('badge-success');
                    $(stateBadge).removeClass('badge-warning');

                    $(stateBadge).addClass(question.BadgeClass);
                    $(stateBadge).html(question.Category);

                    var categoryBadge = $(questionElement).find('.category-badge');
                    $(categoryBadge).html(countString);

                    var countBadge = $(questionElement).find('.category-count');
                    $(countBadge).html(categoryCount);


                },
                getQuestions: function () {
                    $.post(questionAnswerContainer.configuration.viewModel.ActionGetQuestions, { idInstance: questionAnswerContainer.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            console.log("connected", response);
                            if (response.IsSuccessful) {
                                for (var ix in response.Result) {
                                    var question = response.Result[ix];
                                    var questionHtml = questionAnswerContainer.handler.getQuestionHtml(question);
                                    
                                    var questionElement = $(questionHtml);
                                    $(questionElement).data('question-item', question);
                                    $(questionAnswerContainer.elements.questionAnswerContainerElement).find('.questions').append(questionElement);
                                    var configurationEmbeddedNameEdit = {
                                        selectorPath: ['#' + question.IdQuestion, '.question-text'],
                                        idItem: question.IdQuestion,
                                        typeItem: 'Object',
                                        showWithoutEvent: true,
                                        actions: {
                                            init: viewModel.ActionEmbeddedNameEditInit
                                        },
                                        events: {
                                            completed: function (component) {
                                                var selectorPath = [];
                                                for (var i = 0; i < component.configuration.selectorPath.length; i++) {
                                                    var selector = component.configuration.selectorPath[i];
                                                    selectorPath.push(selector);
                                                }
                                                var elements = getElementsBySelectorPath(component.configuration.selectorPath);
                                                selectorPath.push('.cancel-edit');
                                            },
                                            savedName: function (selectorPath, name) {

                                                foundElements = getElementsBySelectorPath(selectorPath);
                                                if (foundElements.length > 0) {
                                                    $(foundElements).find('label').html(name);
                                                }
                                                
                                            }
                                        }
                                    }
                                    $('#' + question.IdQuestion).find('.question-text').createEmbeddedNameEdit(configurationEmbeddedNameEdit);
                                    if (question.IsMultipleChoice) {
                                        $(questionElement).openPageLoader();
                                        $.post(questionAnswerContainer.configuration.viewModel.ActionGetPossibleAnswers, { idQuestionItem: question.IdQuestion, idInstance: questionAnswerContainer.configuration.viewModel.IdInstance })
                                            .done(function (response) {
                                                console.log("connected", response);
                                                var marginLeft = response.Result.MarginLeft;
                                                var answerContainer = $(answerContainerTemplate.replace("@MARGIN_LEFT@", marginLeft));
                                                $('#' + response.Result.IdQuestion).append(answerContainer);
                                                var template = possibleAnswerTemplateRadio;
                                                if (response.Result.MultipleAnswers) {
                                                    template = possibleAnswerTemplateCheck;
                                                }

                                                for (var ix in response.Result.AnswerList) {
                                                    var answer = response.Result.AnswerList[ix];
                                                    var answerElement = $(template.replace("@ID_ANSWER@", answer.IdAnswer).replace("@ID_QUESTION@", response.Result.IdQuestion).replace("@VALUE@", answer.IdAnswer).replace("@ANSWER@", answer.AnswerHtml));
                                                    $(answerContainer).find('.possible-answers').append(answerElement);
                                                }
                                                changeValuesFromServerChildren(response.ViewItems, [ ".question-answer" ]);

                                                $('body').closePageLoader();
                                            })
                                            .fail(function (jqxhr, textStatus, error) {
                                                // stuff
                                                $('body').closePageLoader();
                                            });
                                    } else {
                                        var selfAnswerContainer = $(selfAnswerContainerTemplate.replace("@MARGIN_LEFT@", question.WidthNumber));
                                        $('#' + question.IdQuestion).append(selfAnswerContainer);
                                        var selfAnswer = $(htmlEditorTemplate.replace("@ID_QUESTION@", question.IdQuestion));
                                        $(selfAnswerContainer).find('.self-answer').append(selfAnswer);

                                        var useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;

                                        tinymce.init({
                                            selector: 'textarea#answer_' + question.IdQuestion,
                                            browser_spellcheck: true,
                                            menubar: false,
                                            plugins: [
                                                'advlist autolink lists link image charmap print preview anchor',
                                                'searchreplace visualblocks code fullscreen',
                                                'insertdatetime media table paste code help wordcount'
                                            ],
                                            toolbar: 'undo redo | formatselect | ' +
                                                'bold italic backcolor | alignleft aligncenter ' +
                                                'alignright alignjustify | bullist numlist outdent indent | ' +
                                                'removeformat | help',
                                            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
                                        });
                                    }   
                                }
                                if (typeof defaultCateogry !== 'undefined') {
                                    questionAnswerContainer.handler.filterQuestions(questionAnswerContainer, defaultCateogry);
                                }
                            }
                            else {
                                questionAnswerContainer.configuration.events.errorOccurred(response.Result.Message);
                            }
                            changeValuesFromServerChildren(response.ViewItems, [ ".question-answer" ]);
                            $('body').closePageLoader();
                            questionAnswerContainer.configuration.events.questionLoaded();
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                            $('body').closePageLoader();
                        });
                },
                selectQuestion: function (element) {
                    idQuestion = $(element).closest('div').parent().attr('id');

                    questionAnswerContainer.configuration.viewModel.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: questionAnswerContainer.configuration.viewModel.ChannelSelectedObject,
                        SenderId: questionAnswerContainer.configuration.viewModel.managerConfig.idEndpoint,
                        SessionId: questionAnswerContainer.configuration.viewModel.managerConfig.idSession,
                        UserId: questionAnswerContainer.configuration.viewModel.managerConfig.idUser,
                        OItems: [
                            {
                                GUID: idQuestion
                            }]
                    });
                },
                createControl: function (element) {
                    $('body').openPageLoader();
                    questionAnswerContainer.elements.questionAnswerContainerElement = $(element);
                    tinymce.remove();
                    $(element).html('');
                    var containerHtml = componentTemplate;
                    questionAnswerContainer.elements.questionAnswerElement = $(containerHtml);
                    $(questionAnswerContainer.elements.questionAnswerContainerElement).append(questionAnswerContainer.elements.questionAnswerElement);
                    $("#question-viewer-splitter")
                        .kendoSplitter({
                            orientation: "horizontal",
                            panes: [
                                { size: "50%" },
                                { collapsible: true, collapsed: true }
                            ]
                        });

                    if (questionAnswerContainer.configuration.toolbar !== undefined && questionAnswerContainer.configuration.toolbar !== null) {
                        if ($('#not-done').length == 0) {

                            $(questionAnswerContainer.configuration.toolbar).data("kendoToolBar").add({
                                type: "splitButton", //setting the command type is mandatory
                                text: "Category", //define the text of the primary button
                                icon: "", //set icon of the primary button
                                menuButtons: [ //define the drop-down options
                                    { id: "not-done", text: "not done" },
                                    { id: "not-known", text: "not known" },
                                    { id: "known", text: "known" },
                                    { id: "internal", text: "internalized" }
                                ]
                            });

                            $('#not-done').data("question-answer-config", questionAnswerContainer);
                            $('#not-known').data("question-answer-config", questionAnswerContainer);
                            $('#known').data("question-answer-config", questionAnswerContainer);
                            $('#internal').data("question-answer-config", questionAnswerContainer);

                            $('#not-done').click(function () {
                                var questionAnswerContainer = $(this).data("question-answer-config");
                                questionAnswerContainer.handler.filterQuestions(questionAnswerContainer, $(this).attr('id'));
                            });

                            $('#not-known').click(function () {
                                var questionAnswerContainer = $(this).data("question-answer-config");
                                questionAnswerContainer.handler.filterQuestions(questionAnswerContainer, $(this).attr('id'));
                            });

                            $('#known').click(function () {
                                var questionAnswerContainer = $(this).data("question-answer-config");
                                questionAnswerContainer.handler.filterQuestions(questionAnswerContainer, $(this).attr('id'));
                            });

                            $('#internal').click(function () {
                                var questionAnswerContainer = $(this).data("question-answer-config");
                                questionAnswerContainer.handler.filterQuestions(questionAnswerContainer, $(this).attr('id'));
                            });
                        }
                    }
                },
                filterQuestions: function (questionAnswerContainer, questionType) {
                    
                    $($(questionAnswerContainer.configuration.toolbar).find('.k-split-button > .k-button')[0]).html(questionType);
                    var questions = $(questionAnswerContainer.elements.questionAnswerContainerElement).find('.question')
                    for (var i = 0; i < questions.length; i++) {
                        var question = questions[i];
                        var questionElement = $(question).data('question-item');
                        var idCategoryToTest = null;
                        if (questionType === 'not-known') {
                            idCategoryToTest = questionElement.IdCategoryNotKnown;
                            if (questionElement.IdCategory === idCategoryToTest) {
                                $(question).show();
                            } else {
                                $(question).hide();
                            }
                        } else if (questionType === 'known') {
                            idCategoryToTest = questionElement.IdCategroyKnown;
                            if (questionElement.IdCategory === idCategoryToTest) {
                                $(question).show();
                            } else {
                                $(question).hide();
                            }
                        } else if (questionType === 'internal') {
                            idCategoryToTest = questionElement.IdCategoryInternalized;
                            if (questionElement.IdCategory === idCategoryToTest) {
                                $(question).show();
                            } else {
                                $(question).hide();
                            }
                        } else if (questionType === 'not-done') {
                            if (questionElement.IdCategory === null ||
                                questionElement.IdCategory === questionElement.IdCategoryOfficial) {
                                $(question).show();
                            } else {
                                $(question).hide();
                            }
                        } else if (questionType === 'official') {
                            var officials = $.grep(questionElement.CategoryCounts, function (cat) { return cat.ShortCut == 'OF' });
                            if (officials.length > 0) {
                                $(question).show();
                            } else {
                                $(question).hide();
                            }
                        }

                        

                    }
                },
                showSolution: function (element) {
                    var questionElement = $(element).parent().parent().parent();
                    var question = $(questionElement).data('question-item');
                    var correctAnswerElement = $(questionElement).find('.correct-answer');
                    if (correctAnswerElement.length > 0) {
                        if ($(correctAnswerElement).is(":visible")) {
                            $(correctAnswerElement).hide();
                        } else {
                            $(correctAnswerElement).show();
                        }
                    }
                    else {
                        $('body').openPageLoader();
                        $.post(questionAnswerContainer.configuration.viewModel.ActionGetCorrectAnswers, { idQuestion: question.IdQuestion, idInstance: questionAnswerContainer.configuration.viewModel.IdInstance })
                            .done(function (response) {
                                console.log("connected", response);
                                var marginLeft = response.Result.MarginLeft;
                                var answerContainer = $(correctAnswerContainerTemplate.replace("@MARGIN_LEFT@", marginLeft));
                                $('#' + response.Result.IdQuestion).find('.possible-answers, .self-answer').append(answerContainer);
                                var template = correctAnswerTemplate;

                                if (response.Result.AnswerList.length == 1 && response.Result.AnswerList[0] === null) {
                                    questionAnswerContainer.configuration.events.log("No Answers found!");
                                }
                                else if (response.Result.AnswerList.length > 0) {
                                    for (var ix in response.Result.AnswerList) {
                                        var answer = response.Result.AnswerList[ix];
                                        var answerElement = $(template.replace("@ANSWER@", answer.AnswerHtml));
                                        $(answerContainer).append(answerElement);
                                    }
                                }
                                else {
                                    var answerElement1 = $(template.replace("@ANSWER@", ''));
                                    $(answerContainer).append(answerElement1);
                                }

                                changeValuesFromServerChildren(response.ViewItems, [ ".question-answer" ]);

                                $('body').closePageLoader();
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                                $('body').closePageLoader();
                            });
                    }

                },
                saveSelfAnswer: function (element, type) {
                    var textArea = $(element).parent().parent().find('textarea');
                    var idTextArea = $(textArea).attr('id');
                    var answer = tinymce.get(idTextArea).getContent();
                    var answer64 = utf8_to_b64(answer);
                    var questionElement = $(element).parent().parent().parent();
                    var question = $(questionElement).data('question-item');

                    $('body').openPageLoader();
                    $.post(questionAnswerContainer.configuration.viewModel.ActionSaveSelfAnswer, { idQuestion: question.IdQuestion, content64: answer64, answerType: type, idInstance: questionAnswerContainer.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            console.log("connected", response);
                            if (response.IsSuccessful) {
                                var questionElement = $($(element).parent().parent());
                                questionAnswerContainer.handler.setQuestionHtml(response.Result);
                                $(questionElement).hide();
                            }

                            changeValuesFromServerChildren(response.ViewItems, [ ".question-answer" ]);

                            $('body').closePageLoader();
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                            $('body').closePageLoader();
                        });
                },
                saveAnswerWithOfficial: function (element) {
                    var questionElement = $(element).parent().parent().parent();
                    var question = $(questionElement).data('question-item');
                    var answers = $('input[name=' + question.IdQuestion + ']:checked');
                    var idAnswers = [];
                    for (var i = 0; i < answers.length; i++) {
                        var answer = answers[i];
                        idAnswers.push($(answer).attr("id"));
                    }

                    $('body').openPageLoader();
                    $.post(questionAnswerContainer.configuration.viewModel.ActionSaveAnswerWithOfficialAnswer, { idQuestion: question.IdQuestion, idAnswers: idAnswers, idInstance: questionAnswerContainer.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            console.log("connected", response);
                            if (response.IsSuccessful) {
                                $($(element).parent().parent()).hide();
                            }

                            changeValuesFromServerChildren(response.ViewItems, [ ".question-answer" ]);

                            $('body').closePageLoader();

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                            $('body').closePageLoader();
                        });
                },
                clickedButton: function (element) {
                    
                },
                setCategory: function (category) {
                    defaultCateogry = category;
                }
            }
        });
        $(this).addClass('question-answer');

        questionAnswerContainer = $(this).data("question-answer-config");

        if (questionAnswerContainer.configuration.viewModel == undefined || questionAnswerContainer.configuration.viewModel == null) {
            $.post(questionAnswerContainer.configuration.actionQuestionAnswerInit)
                .done(function (response) {
                    questionAnswerContainer.configuration.viewModel = response.Result;
                    changeValuesFromServerChildren(questionAnswerContainer.viewModel.ViewItems, [ ".question-answer" ]);
                    questionAnswerContainer.handler.createControl(this);
                    if (questionAnswerContainer.configuration.events !== undefined && questionAnswerContainer.configuration.events !== null &&
                        questionAnswerContainer.configuration.events.createCompleted !== undefined && questionAnswerContainer.configuration.events.createCompleted !== null) {
                        questionAnswerContainer.configuration.events.createCompleted(questionAnswerContainer);
                    }
                    $('body').closePageLoader();
                    
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        } else {
            questionAnswerContainer.handler.createControl(this);
            questionAnswerContainer.configuration.events.createCompleted(questionAnswerContainer);
            $('body').closePageLoader();
        }
        
    };

})(jQuery);



var componentTemplate = `
<div id="question-viewer-splitter" style="height:100%">
    <div class="questions">
    </div>
    <div class="doc-viewer">
        <iframe src="" style="width:100%;height:100%" border="0" />
    </div>
</div>
    
`;

var questionTemplate = `
    <div class="question" style="margin-left:20px;" id="@ID_QUESTIION@">
        <div style="border-radius: 5px; background: #e6eeff; padding: 4px;" >
            <span class="numbering" style="width:@WIDTH@px;display: inline-block;">
            @ID@.
            </span>
            <span><button class="btn btn-xs btn-info" onClick="questionAnswerContainer.handler.selectQuestion(this)" title="Select question in referenced text"><i class="fa fa-share-square-o" aria-hidden="true"></i></button></span>
            <span><button class="btn btn-xs btn-info" onClick="questionAnswerContainer.handler.showSolution(this)" title="Show correct answer(s)"><i class="fa fa-check-square" aria-hidden="true"></i></button></span>
            <span class="badge @STATE_STYLE@ state-badge">@QUESTION_STATE@</span>
            <span class="badge badge-light category-badge">@LAST_CATEGORY_NUMBER@</span>
            <span class="badge badge-light category-count">@CATEGORY_COUNT@</span>
            <span class="question-text">
            <label>
            @HTML@
            </label>
            </span>
        </div>
    </div>
`;

var answerContainerTemplate = `
<div style="margin-left:@MARGIN_LEFT@px;margin-top:5px; margin-bottom:5px;">
<div class="possible-answers"></div>
<div>
<button class="btn btn-primary" onClick="questionAnswerContainer.handler.saveAnswerWithOfficial(this)" title="Save Answer(s)">Save</button>
</div>
</div>
`

var correctAnswerContainerTemplate = `
<div style="margin-left:@MARGIN_LEFT@px border-radius: 5px; background: #e6eeab; padding: 4px;" class="correct-answer">
</div>
`

var possibleAnswerTemplateRadio = `<p><input type="radio" id="@ID_ANSWER@" name="@ID_QUESTION@" value="@VALUE@">&nbsp;@ANSWER@</p>`;

var possibleAnswerTemplateCheck = `<p><input type="checkbox" id="@ID_ANSWER@" value="@VALUE@">&nbsp;@ANSWER@</p>`;

var correctAnswerTemplate = `<p><i class="fa fa-check-square" aria-hidden="true"></i>&nbsp;@ANSWER@</p>`;


var selfAnswerContainerTemplate = `
<div style="margin-left:@MARGIN_LEFT@px;margin-top:5px; margin-bottom:5px;">
<div class="self-answer"></div>
<div class="btn-group">
<button class="btn btn-secondary" onClick="questionAnswerContainer.handler.saveSelfAnswer(this, 0)" title="Save Answer(s)">I don't know</button>
<button class="btn btn-secondary" onClick="questionAnswerContainer.handler.saveSelfAnswer(this, 1)" title="Save Answer(s)">I know</button>
<button class="btn btn-secondary" onClick="questionAnswerContainer.handler.saveSelfAnswer(this, 2)" title="Save Answer(s)">Internalized</button>
<button class="btn btn-secondary" onClick="questionAnswerContainer.handler.saveSelfAnswer(this, 3)" title="Save Answer(s)">Official</button>
</div>
</div>
`

var htmlEditorTemplate = `
    <textarea id="answer_@ID_QUESTION@"></textarea>
`;