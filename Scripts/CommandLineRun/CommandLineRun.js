﻿(function ($) {
    $.fn.createCommandLineRun = function (configuration) {
        var commandlineRunComponent = {
            configuration: configuration,
            data: {

            },
            elements: {
            },
            components: {
            },
            handler: {
                createComponent: function (commandlineRunComponent) {
                    commandlineRunComponent.elements.commandLineRunElement = $(commandlineRunComponent.templates.commandLineRun);
                    $(commandlineRunComponent.elements.commandlineRunContainer).append(commandlineRunComponent.elements.commandLineRunElement);

                    changeValuesFromServerChildren(commandlineRunComponent.configuration.viewModel.ViewItems, commandlineRunComponent.configuration.selectorPath);
                },
                validateReference: function (commandlineRunComponent, oItems) {

                }
            },
            templates: {
                commandLineRun:
                    `
                        <div class="splitter">
                            <div class="object-tree">
                                
                            </div>
                            <div>
                                <div class="container script-container">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label class="lblName"></label>
                                        </div>
                                        <div class="col-lg-10">
                                            <input class="inpName" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label class="lblProgramingLanguage"></label>
                                        </div>
                                        <div class="col-lg-10">
                                            <input class="inpProgramingLanguage" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label class="lblEncoding"></label>
                                        </div>
                                        <div class="col-lg-10">
                                            <input class="inpEncoding" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label class="lblCodeParsed"></label>
                                        </div>
                                        <div class="col-lg-10">
                                            <pre class="scriptParsed"></pre>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label class="lblCodeRaw"></label>
                                        </div>
                                        <div class="col-lg-10">
                                            <pre class="scriptRaw"></pre>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `
                }
            }
        }

        commandlineRunComponent.elements.commandlineRunContainer = this;
        $(commandlineRunComponent.elements.commandlineRunContainer).addClass('command-line-run');
        $(commandlineRunComponent.elements.commandlineRunContainer).data('command-line-run-component', commandlineRunComponent);

        $.post(commandlineRunComponent.configuration.actionInit, { idInstance: commandlineRunComponent.configuration.IdInstance, selectorPath: commandlineRunComponent.configuration.selectorPath })
            .done(function (response) {
                // stuff
                console.log(response);
                if (response.IsSuccessful) {
                    var selectorPath = response.SelectorPath;
                    var element = getElementsBySelectorPath(selectorPath);
                    var commandlineRunComponent = $(element).data('command-line-run-component');
                    commandlineRunComponent.configuration.viewModel = response.Result;
                    commandlineRunComponent.handler.createComponent(commandlineRunComponent);
                } else {

                }

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    }
})(jQuery);