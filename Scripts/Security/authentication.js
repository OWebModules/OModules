﻿var authenticationWindowElement;
var dataAuthentication;
(function ($) {
    $.fn.createAuthentication = function (configuration, authenticationHandler) {
        dataAuthentication = 'authentication-window-config';
        $(this).data(dataAuthentication, {
            elements: {
                authenticationWindowElement: null,
                authenticationWindow: null
            },
            configuration: configuration,
            handler: {
                authenticationHandler: authenticationHandler
            }
        });



        $(this).addClass('authentication-window');

        var authenticationWindowElement = $(this).data(dataAuthentication);

        authenticationWindowElement.elements.authenticationWindowElement = $(`
<div class="authenticationWindow">
        <div class="toolbarAuthentication"></div>
        
            <div class="form-group">
                <label >Your Password</label>
                <input type="password" class="form-control inputAuthenticationPassword" placeholder="Password">
            </div>
        

        <div style="width: 100%; height: 30px; ">
            <div style="float: left; margin: 4px;">
                <div class="alert alert-danger alertWrongPassword" role="alert">

                </div>
            </div>
            <div style="float: right; margin: 4px">
                <button class="k-button buttonOkAuthentication">OK</button>&nbsp;
            </div>
        </div>
    </div>
    `);
        $('body').append($(authenticationWindowElement.elements.authenticationWindowElement));

        $(authenticationWindowElement.elements.authenticationWindowElement).kendoWindow({
            width: "326px",
            title: "Your Password",
            visible: false,
            closable: false,
            actions: {},
            close: function () {

            }
        }).data("kendoWindow").center();

        authenticationWindowElement.elements.authenticationWindow = $(authenticationWindowElement.elements.authenticationWindowElement).data("kendoWindow");
        $(authenticationWindowElement.elements.authenticationWindowElement).find('.alertWrongPassword').hide();

        $(authenticationWindowElement.elements.authenticationWindowElement).find('.buttonOkAuthentication').click(function () {
            $(authenticationWindowElement.elements.authenticationWindowElement).find('.alertWrongPassword').hide();
            $(authenticationWindowElement.elements.authenticationWindowElement).find('.alertWrongPassword').html('');
            var password = $(authenticationWindowElement.elements.authenticationWindowElement).find('.inputAuthenticationPassword').val();
            $('body').openPageLoader();
            $.post(authenticationWindowElement.configuration.viewModel.ActionValidatePassword, { idInstance: authenticationWindowElement.configuration.viewModel.IdInstance, password: password })
                .done(function (response) {
                    if (response.IsSuccessful) {

                        authenticationWindowElement.elements.authenticationWindow.close();
                        authenticationWindowElement.handler.authenticationHandler(true);
                        
                    } else {

                        var alert = getViewItemValue(response.ViewItems[0], "Content", "");
                        $(authenticationWindowElement.elements.authenticationWindowElement).find('.alertWrongPassword').html(alert);
                        $(authenticationWindowElement.elements.authenticationWindowElement).find('.alertWrongPassword').show();
                    }
                    $('body').closePageLoader();
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                    $('body').closePageLoader();

                    var alert = getViewItemValue(authenticationWindowElement.configuration.viewModel.alertWrongPassword, "Caption", "");
                    $(authenticationWindowElement.elements.authenticationWindowElement).find('.alertWrongPassword').html(alert);
                    $(authenticationWindowElement.elements.authenticationWindowElement).find('.alertWrongPassword').show();
                });
        });

        $(authenticationWindowElement.elements.authenticationWindowElement).find('.inputAuthenticationPassword').on("keyup", function (event) {
            // Cancel the default action, if needed
            event.preventDefault();
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
                // Trigger the button element with a click
                $(authenticationWindowElement.elements.authenticationWindowElement).find('.buttonOkAuthentication').click();
            }
        });

        $.post(authenticationWindowElement.configuration.actionAuthenticationInit)
            .done(function (response) {
                authenticationWindowElement.configuration.viewModel = response.Result;
                var isAuthenticated = !getViewItemValue(authenticationWindowElement.configuration.viewModel.authenticationWindow, "Other", false);
                if (!isAuthenticated) {
                    authenticationWindowElement.elements.authenticationWindow.open();
                    $('#' + authenticationWindowElement.configuration.viewModel.alertWrongPassword.ViewItemId).hide();
                } else {
                    authenticationWindowElement.handler.authenticationHandler(true);
                }
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

        return authenticationWindowElement;
    };
})(jQuery);