﻿(function ($) {
    $.fn.createInternetQuelle = function (configuration) {
        var dataInternetQuelle = 'literaturquelle-internet-config';
        var selectorInternetQuelle = configuration.selector;
        $(this).data(dataInternetQuelle, {
            elements: {
                internetQuelleContainerElement: null
            },
            configuration: configuration,
            handler: {
                validateReference: function (idObject, validateReferenceCallback) {
                    var oItem = {
                        GUID: idObject
                    };
                    var url = $(selectorInternetQuelle).data(dataInternetQuelle).configuration.actionValidateReference;
                    var idInstance = $(selectorInternetQuelle).data(dataInternetQuelle).configuration.idInstance;

                    $.post(url, { refItem: oItem, idInstance: idInstance })
                        .done(function (response) {
                            console.log("connected", response);

                            if (response.ResultType === 1) {
                                var askForCreation = getViewItem(response.ViewItems, "askForCreation");
                                if ($(selectorInternetQuelle).data(dataInternetQuelle).configuration.openCreationWindow !== null) {
                                    $(selectorInternetQuelle).data(dataInternetQuelle).configuration.openCreationWindow();
                                }
                            }
                            changeValuesFromServerChildren(response.ViewItems, [selectorInternetQuelle]);
                            if (validateReferenceCallback !== undefined && validateReferenceCallback !== null) {
                                validateReferenceCallback();
                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                            $('body').closePageLoader();
                        });
                }, validateAppliedReference: function (idObject) {
                    var container = $(selectorInternetQuelle);
                    var viewItems = container.data('literaturquelle-internet-config').configuration.viewModel.ViewItems;
                    var viewItem = getViewItem(viewItems, "hiddenReference");
                    $('body').openPageLoader();
                    changeValue(viewItem, "Other", idObject, true, function (response) {
                        $('body').closePageLoader();
                        changeValuesFromServerChildren(response.ViewItems, ['.internet-quelle']);
                    });
                }

            }
        });

        $(this).addClass('internet-quelle');
        var internetQuelleElement = $(this).data(dataInternetQuelle);

        internetQuelleElement.elements.internetQuelleContainerElement = this;
        $(internetQuelleElement.elements.internetQuelleContainerElement).addClass('k-content');
        $(internetQuelleElement.elements.internetQuelleContainerElement).html('');

        var urlLabelValue = getViewItemsValue(internetQuelleElement.configuration.viewModel.ViewItems, 'labelUrl', 'Content');
        var url = getViewItemsValue(internetQuelleElement.configuration.viewModel.ViewItems, 'inputUrl', 'Content');

        var creatorLabelValue = getViewItemsValue(internetQuelleElement.configuration.viewModel.ViewItems, 'labelCreator', 'Content');
        var creator = getViewItemsValue(internetQuelleElement.configuration.viewModel.ViewItems, 'inputCreator', 'Content');

        var downloadStampLabelValue = getViewItemsValue(internetQuelleElement.configuration.viewModel.ViewItems, 'labelDownloadstamp', 'Content');
        var downloadStamp = getViewItemsValue(internetQuelleElement.configuration.viewModel.ViewItems, 'inputDownloadstamp', 'Content');

        var nameLabelValue = getViewItemsValue(internetQuelleElement.configuration.viewModel.ViewItems, 'labelName', 'Content');
        var name = getViewItemsValue(internetQuelleElement.configuration.viewModel.ViewItems, 'inputName', 'Content');

        if (nameLabelValue === undefined || nameLabelValue === null) {
            nameLabelValue = 'Name:';
        }

        if (name === undefined || name === null) {
            name = '';
        }

        if (urlLabelValue === undefined || urlLabelValue === null) {
            urlLabelValue = 'Url:';
        }

        if (url === undefined || url === null) {
            url = '';
        }

        if (creatorLabelValue === undefined || creatorLabelValue === null) {
            creatorLabelValue = 'Creator:';
        }

        if (creator == undefined || creator === null) {
            creator = '';
        }

        if (downloadStampLabelValue === undefined || downloadStampLabelValue === null) {
            downloadStampLabelValue = 'Creator:';
        }

        if (downloadStamp === undefined || downloadStamp === null) {
            downloadStamp = '';
        }




        var nameContainer = $('<div class="form-group"></div>');
        $(internetQuelleElement.elements.internetQuelleContainerElement).append(nameContainer);

        var nameLabelCreator = $('<label for=".inputName" class="col-form-label labelName">' + nameLabelValue + '</label>');
        $(nameContainer).append(nameLabelCreator);

        var nameInput = $('<input class="form-control inputName" type="text" value="' + name + '" />');
        $(nameContainer).append(nameInput);

        var urlContainer = $('<div class="form-group"></div>');
        $(internetQuelleElement.elements.internetQuelleContainerElement).append(urlContainer);

        var spanUrl = $('<span></span>');
        $(urlContainer).append(spanUrl);

        var urlLabelUrl = $('<label for=".inputUrl" class="col-form-label labelUrl">' + urlLabelValue + '</label>');
        $(spanUrl).append(urlLabelUrl);
        var urlButtonUrl = $('<button class="k-default k-button buttonUrl"> <i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></button>')
        $(spanUrl).append(urlButtonUrl);
        var urlInputUrl = $('<input class="form-control inputUrl" type="text" value="' + url + '" readonly />');
        $(urlContainer).append(urlInputUrl);

        var creatorContainer = $('<div class="form-group"></div>');
        $(internetQuelleElement.elements.internetQuelleContainerElement).append(creatorContainer);

        var spanCreator = $('<span></span>');
        $(creatorContainer).append(spanCreator);

        var creatorLabelCreator = $('<label for=".inputCreator" class="col-form-label labelCreator">' + creatorLabelValue + '</label>');
        $(spanCreator).append(creatorLabelCreator);
        var creatorButtonCreator = $('<button class="k-default k-button buttonCreator"> <i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></button>')
        $(spanCreator).append(creatorButtonCreator);
        var creatorInputCreator = $('<input class="form-control inputCreator" type="text" value="' + creator + '" />');
        $(creatorContainer).append(creatorInputCreator);

        var downloadStampContainer = $('<div class="form-group"></div>');
        $(internetQuelleElement.elements.internetQuelleContainerElement).append(downloadStampContainer);

        var downloadStampLabelCreator = $('<label for=".inputCreator" class="col-form-label labelDownloadstamp">' + downloadStampLabelValue + '</label>');
        $(downloadStampContainer).append(downloadStampLabelCreator);

        var downloadStampInput = $('<input class="form-control inputDownloadstamp" type="text" value="' + downloadStamp + '" />');
        $(downloadStampContainer).append(downloadStampInput);

        var hiddenIdClassUrl = $('<input class="hiddenIdClassUrl" type="hidden" />');
        $(internetQuelleElement.elements.internetQuelleContainerElement).append(hiddenIdClassUrl);

        var hiddenIdClassPartner = $('<input class="hiddenIdClassPartner" type="hidden" />');
        $(internetQuelleElement.elements.internetQuelleContainerElement).append(hiddenIdClassPartner);

        var configurationObjectList = {
            actionObjectListInit: internetQuelleElement.configuration.viewModel.ActionObjectListInit,
            templateSelector: "#template",
            managerConfig: viewModel.managerConfig,
            useIdParent: true,
            callbackGridCreated: function (gridItem) {

            },
            handler: {
                initCompleteHandler: function () {
                    //viewModel.getUrlParameters();,
                    var senderIdNew = $('.object-list-container').data('object-list-config').configuration.viewModel.IdEndpoint;
                    addSenderToIgnoreList(senderIdNew, sendersIgnoreInternetQuelle);
                },
                applyItem: function (items) {

                }
            }
        };

        internetQuelleElement.elements.windowContainerElement = $('<div class="window-container-object-list"></div>');

        $(internetQuelleElement.elements.internetQuelleContainerElement).parent().parent().append(internetQuelleElement.elements.windowContainerElemen);
        var objectListWindow = $('<div class="window-object-list"></div>');
        $(internetQuelleElement.elements.windowContainerElement).append(objectListWindow);
        $(objectListWindow).append($('<div class= "object-list-container"></div>'));

        $(internetQuelleElement.elements.windowContainerElement).kendoWindow({
            width: '1000px',
            height: '800px',
            title: 'Objects',
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Minimize",
                "Maximize",
                "Close"
            ]
        }).data("kendoWindow").center();

        $(internetQuelleElement.elements.windowContainerElement).find('.object-list-container').createObjectList(configurationObjectList);

        $(selectorInternetQuelle).find('.inputName').on('change', function () {
            var text = $(this).val();
            var container = $(this).closest('.internet-quelle');
            var viewItems = container.data('literaturquelle-internet-config').configuration.viewModel.ViewItems;
            var viewItem = getViewItem(viewItems, "inputName");
            $('body').openPageLoader();
            changeValue(viewItem, "Content", text, true, function () {
                $('body').closePageLoader();
                changeValuesFromServerChildren(response, ['.internet-quelle']);
            });
        });

        $(selectorInternetQuelle).find('.k-button').kendoButton({
            click: function (e) {
                if ($(e.sender.element).hasClass('buttonUrl')) {
                    $(e.sender.element).toggleMode(false);

                    if ($(e.sender.element).hasClass('k-primary')) {
                        var container = $(e.sender.element).closest('.internet-quelle');
                        var idClass = container.find('.hiddenIdClassUrl').val();
                        var configuration = container.data('literaturquelle-internet-config').configuration;
                        configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                            ChannelId: configuration.channelSelectedClass,
                            SenderId: configuration.managerConfig.idEndpoint,
                            SessionId: configuration.managerConfig.idSession,
                            UserId: configuration.idUser,
                            OItems: [{ GUID: idClass }]
                        });
                    }

                    $(internetQuelleElement.elements.windowContainerElement).find('.object-list-container').data('object-list-config').handler.setIdParent(idClass);
                    $(internetQuelleElement.elements.windowContainerElement).data("kendoWindow").open();

                } else if ($(e.sender.element).hasClass('buttonCreator')) {
                    $(e.sender.element).toggleMode(false);

                    if ($(e.sender.element).hasClass('k-primary')) {
                        var container = $(e.sender.element).closest('.internet-quelle');
                        var idClass = container.find('.hiddenIdClassPartner').val();
                        var configuration = container.data('literaturquelle-internet-config').configuration;
                        configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                            ChannelId: configuration.channelSelectedClass,
                            SenderId: configuration.managerConfig.idEndpoint,
                            SessionId: configuration.managerConfig.idSession,
                            UserId: configuration.idUser,
                            OItems: [{ GUID: idClass }]
                        });

                        $(internetQuelleElement.elements.windowContainerElement).find('.object-list-container').data('object-list-config').handler.setIdParent(idClass);
                        $(internetQuelleElement.elements.windowContainerElement).data("kendoWindow").open();
                    }
                }
            }
        });

        $(selectorInternetQuelle).find('.inputDownloadstamp').kendoDateTimePicker({
            format: 'dd.MM.yyyy HH:mm:ss',
            change: function (e) {
                var value = this.value();
                value = moment(value).tz('Europe/Berlin').format("YYYY-MM-DD HH:mm:ss");
                console.log('Todesdatum', value);
                var container = $(e.sender.element).closest('.internet-quelle');
                var viewItems = container.data('literaturquelle-internet-config').configuration.viewModel.ViewItems;
                var viewItem = getViewItem(viewItems, "inputDownloadstamp");
                $('body').openPageLoader();
                changeValue(viewItem, "Content", value, true, function () {
                    $('body').closePageLoader();
                    changeValuesFromServerChildren(response, ['.internet-quelle']);
                });

            }
        });

        changeValuesFromServerChildren($(selectorInternetQuelle).data(dataInternetQuelle).configuration.viewModel.ViewItems, [selectorInternetQuelle]);

        if ($(this).data(dataInternetQuelle).configuration.createdCallback !== undefined && $(this).data(dataInternetQuelle).configuration.createdCallback !== null) {
            $(this).data(dataInternetQuelle).configuration.createdCallback($(this).data(dataInternetQuelle));
        }

    };
})(jQuery);