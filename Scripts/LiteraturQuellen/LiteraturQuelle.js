﻿(function ($) {
    $.fn.createLiteraturQuelleCreate = function (configuration) {
        var dataLiteraturQuelleCreate = 'literaturquelle-create-config';
        var selectorLiteraturQuelleCreate = configuration.selector;

        $(this).data(dataLiteraturQuelleCreate, {
            elements: {
                literaturQuelleCreateContainerElement: null,
                
            },
            configuration: configuration,
            handler: {
                validateReference: function (idObject, validateReferenceCallback) {

                }
            }
        });

        $(this).addClass('literaturquelle-create');
        var literaturQuelleCreateElement = $(this).data(dataLiteraturQuelleCreate);

        literaturQuelleCreateElement.elements.literaturQuelleCreateContainerElement = this;
        $(literaturQuelleCreateElement.elements.literaturQuelleCreateContainerElement).addClass('k-content');
        $(literaturQuelleCreateElement.elements.literaturQuelleCreateContainerElement).html('');

        var literaturQuelleCreatorElement = $(`<div style="overflow: hidden;" class="literaturquelle-creator-window-container">
            <div class="literaturquelle-creator">
                <div class="k-content">
                    <div class="form-group">
                        <label class="labelName col-form-label">Name</label>
                        <input class="form-control inputName" type="text" placeholder="Name" />
                    </div>
                </div>
                <div class="k-content">
                    <div class="form-group">
                        <label class="labelQuellType col-form-label">Type</label>
                        <input class="form-control dropDownQuellType" type="text" placeholder="Name" />
                    </div>
                </div>
                <div class="btn-group" role="group" aria-label="">
                    <button type="button" class="btn btn-secondary buttonSave">Save</button>
                </div>
            </div>
        </div>`);

        $(literaturQuelleCreateElement.elements.literaturQuelleCreateContainerElement).append(literaturQuelleCreatorElement);

        $.post(literaturQuelleCreateElement.configuration.actionLiteraturQuelleCreatorInit)
            .done(function (response) {
                literaturQuelleCreateElement.configuration.viewModel = response.Result;
                
                $.post(literaturQuelleCreateElement.configuration.viewModel.ActionGetDropDownConfigQuellTypes, { idClass: literaturQuelleCreateElement.configuration.viewModel.idClassQuellTypes })
                    .done(function (response) {
                        var dropDownConfig = response.DropDownConfig;
                        jQuery.extend(dropDownConfig, {
                            filter: "contains",
                            suggest: true
                        });

                        $.extend(dropDownConfig, {
                            change: function (e) {

                            },
                            dataBound: function (e) {
                                
                            }
                        });

                        $($(literaturQuelleCreateElement.elements.literaturQuelleCreateContainerElement).find('.dropDownQuellType')[0]).kendoDropDownList(dropDownConfig);
                        literaturQuelleCreateElement.configuration.viewModel.quellTypes = $($(literaturQuelleCreateElement.elements.literaturQuelleCreateContainerElement).find('.dropDownQuellType')[1]).data("kendoDropDownList")
                        literaturQuelleCreateElement.configuration.viewModel.quellTypes.enable(false);

                        if (literaturQuelleCreateElement.configuration.createCallback !== undefined && literaturQuelleCreateElement.configuration.createCallback !== null) {
                            literaturQuelleCreateElement.configuration.createCallback(literaturQuelleCreateElement);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    };
})(jQuery);