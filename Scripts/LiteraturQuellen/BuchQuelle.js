﻿(function ($) {
    $.fn.createbuchQuelle = function (configuration) {
        var databuchQuelle = 'literaturquelle-buch-config';
        var selectorbuchQuelle = configuration.selector;
        $(this).data(databuchQuelle, {
            elements: {
                buchQuelleContainerElement: null
            },
            configuration: configuration,
            handler: {
                validateReference: function (idObject, validateReferenceCallback) {
                    var oItem = {
                        GUID: idObject
                    };
                    var url = $(selectorbuchQuelle).data(databuchQuelle).configuration.actionValidateReference;
                    var idInstance = $(selectorbuchQuelle).data(databuchQuelle).configuration.idInstance;

                    $.post(url, { refItem: oItem, idInstance: idInstance })
                        .done(function (response) {
                            console.log("connected", response);

                            if (response.ResultType === 1) {
                                var askForCreation = getViewItem(response.ViewItems, "askForCreation");
                                if ($(selectorbuchQuelle).data(databuchQuelle).configuration.openCreationWindow !== null) {
                                    $(selectorbuchQuelle).data(databuchQuelle).configuration.openCreationWindow();
                                }
                            }
                            changeValuesFromServerChildren(response.ViewItems, [ selectorbuchQuelle ]);
                            if (validateReferenceCallback !== undefined && validateReferenceCallback !== null) {
                                validateReferenceCallback();
                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                            $('body').closePageLoader();
                        });
                }, validateAppliedReference: function (idObject) {
                    var container = $(selectorbuchQuelle);
                    var viewItems = container.data('literaturquelle-buch-config').configuration.viewItems;
                    var viewItem = getViewItem(viewItems, "hiddenReference");
                    $('body').openPageLoader();
                    changeValue(viewItem, "Other", idObject, true, function (response) {
                        $('body').closePageLoader();
                        changeValuesFromServerChildren(response.ViewItems, ['.buch-quelle']);
                    });
                }

            }
        });

        $(this).addClass('buch-quelle');
        var buchQuelleElement = $(this).data(databuchQuelle);

        buchQuelleElement.elements.buchQuelleContainerElement = this;
        $(buchQuelleElement.elements.buchQuelleContainerElement).addClass('k-content');
        $(buchQuelleElement.elements.buchQuelleContainerElement).html('');

        var literaturLabelValue = getViewItemsValue(buchQuelleElement.configuration.viewItems, 'labelLiteratur', 'Content');
        var literatur = getViewItemsValue(buchQuelleElement.configuration.viewItems, 'inputLiteratur', 'Content');

        var seiteLabelValue = getViewItemsValue(buchQuelleElement.configuration.viewItems, 'labelSeite', 'Content');
        var seite = getViewItemsValue(buchQuelleElement.configuration.viewItems, 'inputSeite', 'Content');

        var nameLabelValue = getViewItemsValue(buchQuelleElement.configuration.viewItems, 'labelName', 'Content');
        var name = getViewItemsValue(buchQuelleElement.configuration.viewItems, 'inputName', 'Content');

        if (nameLabelValue === undefined || nameLabelValue === null) {
            nameLabelValue = 'Name:';
        }

        if (name === undefined || name === null) {
            name = '';
        }

        if (literaturLabelValue === undefined || literaturLabelValue === null) {
            literaturLabelValue = 'Literatur:';
        }

        if (literatur === undefined || literatur === null) {
            literatur = '';
        }

        if (seiteLabelValue === undefined || seiteLabelValue === null) {
            seiteLabelValue = 'Seite:';
        }

        if (seite === undefined || seite === null) {
            seite = '';
        }


        var nameContainer = $('<div class="form-group"></div>');
        $(buchQuelleElement.elements.buchQuelleContainerElement).append(nameContainer);

        var nameLabelCreator = $('<label for=".inputName" class="col-form-label labelName">' + nameLabelValue + '</label>');
        $(nameContainer).append(nameLabelCreator);

        var nameInput = $('<input class="form-control inputName" type="text" value="' + name + '" />');
        $(nameContainer).append(nameInput);

        var literaturContainer = $('<div class="form-group"></div>');
        $(buchQuelleElement.elements.buchQuelleContainerElement).append(literaturContainer);

        var spanLiteratur = $('<span></span>');
        $(literaturContainer).append(spanLiteratur);

        var lietreaturLabel = $('<label for=".inputLiteratur" class="col-form-label labelLiteratur">' + literaturLabelValue + '</label>');
        $(spanLiteratur).append(lietreaturLabel);
        var literaturButton = $('<button class="k-default k-button buttonLiteratur"> <i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></button>')
        $(spanLiteratur).append(literaturButton);
        var literaturInput = $('<input class="form-control inputLiteratur" type="text" value="' + literatur + '" readonly />');
        $(literaturContainer).append(literaturInput);

        var seiteContainer = $('<div class="form-group"></div>');
        $(buchQuelleElement.elements.buchQuelleContainerElement).append(seiteContainer);

        var spanSeite = $('<span></span>');
        $(seiteContainer).append(spanSeite);

        var seiteLabelSeite = $('<label for=".inputSeite" class="col-form-label labelSeite">' + seiteLabelValue + '</label>');
        $(spanSeite).append(seiteLabelSeite);
        
        var seiteInputSeite = $('<input class="form-control inputSeite" type="text" value="' + seite + '" />');
        $(seiteContainer).append(seiteInputSeite);

        var hiddenIdClassUrl = $('<input class="hiddenIdClassLiteratur" type="hidden" />');
        $(buchQuelleElement.elements.buchQuelleContainerElement).append(hiddenIdClassUrl);

        $(selectorbuchQuelle).find('.inputName').on('change', function () {
            var text = $(this).val();
            var container = $(this).closest('.buch-quelle');
            var viewItems = container.data('literaturquelle-buch-config').configuration.viewItems;
            var viewItem = getViewItem(viewItems, "inputName");
            $('body').openPageLoader();
            changeValue(viewItem, "Content", text, true, function () {
                $('body').closePageLoader();
                changeValuesFromServerChildren(response, ['.buch-quelle']);
            });
        });

        $(selectorbuchQuelle).find('.inputSeite').on('change', function () {
            var text = $(this).val();
            var container = $(this).closest('.buch-quelle');
            var viewItems = container.data('literaturquelle-buch-config').configuration.viewItems;
            var viewItem = getViewItem(viewItems, "inputSeite");
            $('body').openPageLoader();
            changeValue(viewItem, "Content", text, true, function () {
                $('body').closePageLoader();
                changeValuesFromServerChildren(response, ['.buch-quelle']);
            });
        });

        $(selectorbuchQuelle).find('.k-button').kendoButton({
            click: function (e) {
                if ($(e.sender.element).hasClass('buttonLiteratur')) {
                    $(e.sender.element).toggleMode(false);

                    if ($(e.sender.element).hasClass('k-primary')) {
                        var container = $(e.sender.element).closest('.buch-quelle');
                        var idClass = container.find('.hiddenIdClassLiteratur').val();
                        var configuration = container.data('literaturquelle-buch-config').configuration;
                        configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                            ChannelId: configuration.channelSelectedClass,
                            SenderId: configuration.managerConfig.idEndpoint,
                            SessionId: configuration.managerConfig.idSession,
                            UserId: configuration.idUser,
                            OItems: [{ GUID: idClass }]
                        });
                    }

                } 
            }
        });

        changeValuesFromServerChildren($(selectorbuchQuelle).data(databuchQuelle).configuration.viewItems, [selectorbuchQuelle]);

        if ($(this).data(databuchQuelle).configuration.createdCallback !== undefined && $(this).data(databuchQuelle).configuration.createdCallback !== null) {
            $(this).data(databuchQuelle).configuration.createdCallback($(this).data(databuchQuelle));
        }

    };
})(jQuery);