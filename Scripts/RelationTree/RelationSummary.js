﻿(function ($) {
    $.fn.createRelationSummary = function (configuration) {
        $(this).data('relation-summary-config', {
            configuration: configuration,
            elements: {
                summaryContainer: null
            },
            validateReference: function (idObject, validateReferenceCallback) {

            }
        });

        $(this).addClass("relation-summary");

        $('.relation-summary').data('relation-summary-config').elements.summaryContainer = $("<div class='relation-summary-container'></div>");
        $(this).append($('.relation-summary').data('relation-summary-config').elements.summaryContainer);

        if ($('.relation-summary').data('relation-summary-config').configuration != undefined) {

            $('.relation-summary').data('relation-summary-config').configuration.createdCallback();
        }
    }
}) (jQuery);