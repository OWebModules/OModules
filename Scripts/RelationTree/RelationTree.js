﻿
var relationTreeItem;
(function ($) {
    $.fn.oModuleRelationTree = function (treeConfig) {
        $(this).data("oModuleRelationTreeConfig", treeConfig)
        var treeView = $(this);

        $.post(treeConfig.actionTreeConfig)
            .done(function (response) {
                // stuff

                console.log(response);
                $.extend(response.dataSource.transport.read, {
                    data: function () {
                        return { idClass: treeView.data("oModuleRelationTreeConfig").idClass };
                    }
                })
                response.dataSource = new kendo.data.HierarchicalDataSource(response.dataSource);
                $.extend(response, { template: kendo.template($("#treeview-template").html()) });
                $.extend(response,
                    {
                        select: treeView.data("oModuleRelationTreeConfig").selectCallback
                    });
                $.extend(response,
                    {
                        dataBound: relationTreeItem.treeViewDataBound,
                        change: relationTreeItem.treeViewChange
                    });
                treeView.kendoTreeView(response);
                //$(this).data("kendoTreeView").dataSource.read({ idClass: idClass });

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    };

    $.fn.createRelationTree = function (configuration) {
        relationTreeItem = {
            configuration: configuration,
            relationTreeElement: $(this),
            relTreeStateMachine: {
                Init: "Init",
                DataInit: "DataInit",
                OtherRightTreeInit: "OtherRightTreeInit",
                LoadState: "Init"
            },
            components: {
            },
            data: {
            },
            listenTreeNode: null,
            checkLoad: function (state) {
                console.log('state', state);
                if ($('.k-treeview-lines').children().length === 4 && relationTreeItem.idObject !== '') {

                    relationTreeItem.relTreeStateMachine.loadState = relationTreeItem.relTreeStateMachine.DataInit;
                    //checkRefresh();  
                    relationTreeItem.treeElement.data("kendoTreeView").dataSource.read({ idObject: relationTreeItem.idObject });
                } else if (relationTreeItem.relTreeStateMachine.loadState === relationTreeItem.relTreeStateMachine.DataInit) {
                    relationTreeItem.treeNodeAttribute = relationTreeItem.Tree
                        .findByUid($($('.k-treeview-lines').children()[0]).data('uid'));
                    relationTreeItem.treeNodeLeftRight = relationTreeItem.Tree
                        .findByUid($($('.k-treeview-lines').children()[1]).data('uid'));
                    relationTreeItem.treeNodeRightLeft = relationTreeItem.Tree
                        .findByUid($($('.k-treeview-lines').children()[2]).data('uid'));
                    relationTreeItem.treeNodeLeftRightOther = relationTreeItem.Tree
                        .findByUid($($('.k-treeview-lines').children()[3]).data('uid'));

                    relationTreeItem.treeNodeRightLeftOther = relationTreeItem.Tree
                        .findByUid($($('.k-treeview-lines').children()[4]).data('uid'));

                    relationTreeItem.relTreeStateMachine.loadState = relationTreeItem.relTreeStateMachine.OtherRightTreeInit;
                    relationTreeItem.getTreeNodeRelRightOther(relationTreeItem.idObject);
                } else if (relationTreeItem.relTreeStateMachine.loadState === relationTreeItem.relTreeStateMachine.OtherRightTreeInit && $('.k-treeview-lines').children().length === 0) {
                    relationTreeItem.relTreeStateMachine.loadState = relationTreeItem.relTreeStateMachine.DataInit;
                    relationTreeItem.treeElement.data("kendoTreeView").dataSource.read();
                } else if (relationTreeItem.relTreeStateMachine.loadState === relationTreeItem.relTreeStateMachine.OtherRightTreeInit) {
                    $(relationTreeItem.relationTreeElement).find(".btn").unbind('click', relationTreeItem.clickHandlerBtn);
                    $(relationTreeItem.relationTreeElement).find(".btn").bind('click', relationTreeItem.clickHandlerBtn);
                } 
            },
            checkListenNode: function (items, itemType) {
                var listenNode = relationTreeItem.treeElement.find('.btn-warning').closest("li");
                var dataItemListen = relationTreeItem.treeElement.data("kendoTreeView").dataItem(listenNode);
                if (dataItemListen !== undefined) {
                    relationTreeItem.relateItems(dataItemListen, items, itemType);
                }
            },
            relateItems: function (dataItemListen, items, itemType) {
                var orderId = relationTreeItem.orderIdElem.value();
                var setOrderId = relationTreeItem.isToggledSetNextOrderId();
                var sendItem = {
                    relNode: {
                        IdLeft: dataItemListen.IdLeft,
                        NameLeft: dataItemListen.NameLeft,
                        IdRight: dataItemListen.IdRight,
                        NameRight: dataItemListen.NameRight,
                        IdRelationType: dataItemListen.IdRelationType,
                        NameRelationType: dataItemListen.NameRelationType,
                        NodeType: dataItemListen.NodeType,
                        Count: dataItemListen.Count,
                        Min: dataItemListen.Min,
                        MaxForw: dataItemListen.MaxForw,
                        MaxBackw: dataItemListen.MaxBackw
                    },
                    items: items,
                    itemType: itemType,
                    setOrderId: setOrderId,
                    orderId: orderId
                };

                $.post(relationTreeItem.configuration.viewModel.ActionCheckListenNode, sendItem, itemType)
                    .done(function (response) {
                        // stuff
                        dataItemListen.Count = response.item.Count;
                        dataItemListen.NodeName = response.item.NodeName;
                        var node = relationTreeItem.treeElement.data("kendoTreeView").findByUid(dataItemListen.uid);
                        var nodeName = htmlEncode(dataItemListen.NodeName);
                        relationTreeItem.treeElement.data("kendoTreeView").text(node[0], ' ');
                        relationTreeItem.treeElement.data("kendoTreeView").text(node[0], nodeName);
                        var span = node.find('span');

                        $(span[1]).css('color', response.item.ForeColor);

                        $(relationTreeItem.relationTreeElement).find(".btn").unbind('click', relationTreeItem.clickHandlerBtn);
                        $(relationTreeItem.relationTreeElement).find(".btn").bind('click', relationTreeItem.clickHandlerBtn);
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            },
            treeViewDataBound: function (e) {

                this.expand('.k-item');

                relationTreeItem.checkLoad();
            },
            idObject: "",
            treeNodeAttribute: null,
            setIdObject: function (idObject) {
                relationTreeItem.idObject = idObject;
                relationTreeItem.checkViewState();

            },
            clickHandlerBtn: function (e) {
                var selected = $(this).closest('li');
                relationTreeItem.dataItemSelected = relationTreeItem.Tree.dataItem(selected);
                var dataItemLast = null;
                var ctrlPressed = event.ctrlKey;

                if (relationTreeItem.listenTreeNode !== undefined) {
                    dataItemLast = relationTreeItem.Tree.dataItem(relationTreeItem.listenTreeNode);


                    if (ctrlPressed && typeof(dataItemLast) !== 'undefined') {
                        relationTreeItem.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                            ChannelId: relationTreeItem.configuration.viewModel.ChannelAddAttribute,
                            SenderId: relationTreeItem.configuration.managerConfig.idEndpoint,
                            SessionId: relationTreeItem.configuration.managerConfig.idSession,
                            ReceiverId: relationTreeItem.configuration.viewModel.IdSender,
                            ViewId: relationTreeItem.configuration.viewModel.idView,
                            UserId: relationTreeItem.configuration.viewModel.IdUser,
                            GenericParameterItems: [
                                dataItemLast
                            ]
                        });
                    }
                }

                // Attribute
                if (relationTreeItem.dataItemSelected.NodeType === 0) {
                    console.log("Attribute", relationTreeItem.dataItemSelected);

                    var idObject = relationTreeItem.dataItemSelected.IdLeft;
                    var idAttributeType = relationTreeItem.dataItemSelected.IdRight;

                    if (typeof relationTreeItem.configuration.events !== 'undefined' && relationTreeItem.configuration.events !== null &&
                        typeof relationTreeItem.configuration.events.addAttribute !== 'undefined' && relationTreeItem.configuration.events.addAttribute !== null) {
                        relationTreeItem.configuration.events.addAttribute(relationTreeItem, idObject, idAttributeType);
                    }
                }
                else if (relationTreeItem.dataItemSelected.IsParent === true && relationTreeItem.dataItemSelected.NodeType === 4) {
                    //getTreeNodeRelRightOther();
                }
                // Other
                else {
                    console.log("None Attribute", relationTreeItem.dataItemSelected);
                    var btn = '';
                    if (ctrlPressed) {
                        if (relationTreeItem.listenTreeNode !== undefined) {
                            btn = $(relationTreeItem.listenTreeNode).find(".btn");
                            btn.removeClass("btn-warning");
                            btn.addClass("btn-primary");
                        }

                        if (dataItemLast !== undefined && dataItemLast.uid === relationTreeItem.dataItemSelected.uid) {
                            relationTreeItem.listenTreeNode = null;
                        } else {
                            btn = $(selected).find(".btn");
                            btn.removeClass("btn-primary");
                            btn.addClass("btn-warning");
                            relationTreeItem.listenTreeNode = selected;
                        }
                    }

                    var dataItem = relationTreeItem.Tree.dataItem(relationTreeItem.listenTreeNode);
                    if (typeof dataItem === 'undefined') {
                        dataItem = relationTreeItem.dataItemSelected;
                    }
                    if (dataItem.NodeType === 0) {
                        if (relationTreeItem.listenTreeNode !== undefined && relationTreeItem.listenTreeNode !== null && ctrlPressed) {
                            relationTreeItem.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                ChannelId: relationTreeItem.configuration.channelSelectedClassNode,
                                SenderId: relationTreeItem.configuration.managerConfig.idEndpoint,
                                SessionId: relationTreeItem.configuration.managerConfig.idSession,
                                ReceiverId: relationTreeItem.configuration.viewModel.IdSender,
                                ViewId: relationTreeItem.configuration.viewModel.idView,
                                UserId: relationTreeItem.configuration.viewModel.IdUser,
                                OItems: [
                                    {
                                        GUID: dataItem.IdRight
                                    }
                                ]
                            });
                        }
                    }
                    else if (dataItem.NodeType === 1) {
                        if (relationTreeItem.listenTreeNode !== undefined && relationTreeItem.listenTreeNode !== null && ctrlPressed) {
                            relationTreeItem.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                ChannelId: relationTreeItem.configuration.channelSelectedClassNode,
                                SenderId: relationTreeItem.configuration.managerConfig.idEndpoint,
                                SessionId: relationTreeItem.configuration.managerConfig.idSession,
                                ReceiverId: relationTreeItem.configuration.viewModel.IdSender,
                                ViewId: relationTreeItem.configuration.viewModel.idView,
                                UserId: relationTreeItem.configuration.viewModel.IdUser,
                                OItems: [
                                    {
                                        GUID: dataItem.IdRight
                                    }
                                ]
                            });
                        }
                        if (!ctrlPressed) {
                            relationTreeItem.data.classItem = { GUID: dataItem.IdRight, Type: "Class" };
                            relationTreeItem.components.clipboardComponent.handler.validateReference(relationTreeItem.components.clipboardComponent, relationTreeItem.data.classItem);
                            
                            
                        }
                    } else if (dataItem.NodeType === 2) {
                        if (relationTreeItem.listenTreeNode !== undefined && relationTreeItem.listenTreeNode !== null && ctrlPressed) {
                            relationTreeItem.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                ChannelId: relationTreeItem.configuration.channelSelectedClassNode,
                                SenderId: relationTreeItem.configuration.managerConfig.idEndpoint,
                                SessionId: relationTreeItem.configuration.managerConfig.idSession,
                                ReceiverId: relationTreeItem.idSender,
                                ViewId: relationTreeItem.idView,
                                UserId: relationTreeItem.idUser,
                                OItems: [
                                    {
                                        GUID: dataItem.IdLeft
                                    }
                                ]
                            });
                        }
                        if (!ctrlPressed) {
                            relationTreeItem.data.classItem = { GUID: dataItem.IdLeft, Type: "Class" };
                            relationTreeItem.components.clipboardComponent.handler.validateReference(relationTreeItem.components.clipboardComponent, relationTreeItem.data.classItem);
                            
                        }
                    } else if (dataItem.NodeType === 3) {
                        if (ctrlPressed && dataItem.IdRight !== null) {
                            relationTreeItem.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                ChannelId: relationTreeItem.configuration.channelSelectedClassNode,
                                SenderId: relationTreeItem.configuration.managerConfig.idEndpoint,
                                SessionId: relationTreeItem.configuration.managerConfig.idSession,
                                ReceiverId: relationTreeItem.configuration.viewModel.IdSender,
                                ViewId: relationTreeItem.configuration.viewModel.idView,
                                UserId: relationTreeItem.configuration.viewModel.IdUser,
                                OItems: [
                                    {
                                        GUID: dataItem.IdRight
                                    }
                                ]
                            });
                        }
                        if (!ctrlPressed) {
                            relationTreeItem.components.clipboardComponent.handler.validateReference(relationTreeItem.components.clipboardComponent, null);

                        }
                    }

                }
            },
            checkViewState: function () {
                relationTreeItem.isEnabledListen(relationTreeItem.idObject !== undefined);


            },
            isEnabledTree: function (isEnabled) {
                relationTreeItem.treeElement.data("kendoTreeView").enable(isEnabled);
            },
            isEnabledListen: function (isEnabled) {
                relationTreeItem.relationTreeElement.find(".is-listen").data("kendoButton").enable(isEnabled);
            },
            isVisibleListen: function (isVisible) {
                if (isVisible) {
                    relationTreeItem.relationTreeElement.find(".is-listen").show();
                } else {
                    relationTreeItem.relationTreeElement.find(".is-listen").hide();
                }

            },
            isEnabledNextOrderId: function (isEnabled) {
                relationTreeItem.relationTreeElement.find(".next-order-id").data("kendoNumericTextBox").enable(isEnabled);
            },
            isVisibleNextOrderId: function (isVisible) {
                if (isVisible) {
                    relationTreeItem.relationTreeElement.find(".next-order-id").show();
                } else {
                    relationTreeItem.relationTreeElement.find(".next-order-id").hide();
                }

            },
            isEnabledSetNextOrderId: function (isEnabled) {
                relationTreeItem.relationTreeElement.find(".set-next-order-id").data("kendoButton").enable(isEnabled);
            },
            _isToggledListen: false,
            isToggledListen: function (set) {
                if (set !== undefined) {
                    relationTreeItem._isToggledListen = !relationTreeItem._isToggledListen;

                    if (relationTreeItem._isToggledListen) {
                        relationTreeItem.relationTreeElement.find(".is-listen").addClass('k-primary');
                    } else {
                        relationTreeItem.relationTreeElement.find(".is-listen").removeClass('k-primary');
                    }
                }

                return relationTreeItem._isToggledListen;

            },
            _isToggledSetNextOrderId: false,
            isToggledSetNextOrderId: function (set) {
                if (set !== undefined) {
                    relationTreeItem._isToggledSetNextOrderId = !relationTreeItem._isToggledSetNextOrderId;

                    if (relationTreeItem._isToggledSetNextOrderId) {
                        relationTreeItem.relationTreeElement.find(".set-next-order-id").addClass('k-primary')
                    } else {
                        relationTreeItem.relationTreeElement.find(".set-next-order-id").removeClass('k-primary')
                    }
                }

                return relationTreeItem._isToggledSetNextOrderId;

            },
            toolbarElement: null,
            treeElement: null,
            Tree: null,
            orderIdElem: 0,
            validateReference: function (idReference) {

                if (relationTreeItem.idObject !== undefined && relationTreeItem.idObject !== "") {
                    relationTreeItem.treeElement.data("kendoTreeView").dataSource.data([]);
                    relationTreeItem.treeNodeAttribute = undefined;

                }

                relationTreeItem.setIdObject(idReference);

                relationTreeItem.checkLoad();

            },
            getTreeNodeRelRightOther: function (idObject) {
                relationTreeItem.treeElement.data("kendoTreeView").enable(relationTreeItem.treeNodeRightLeftOther, false);
                $.post(relationTreeItem.configuration.viewModel.ActionGetTreeItemsRightLeftOther, { idObject: idObject })
                    .done(function (response) {

                        // stuff
                        if (response.length > 0) {
                            relationTreeItem.Tree.append(response, relationTreeItem.treeNodeRightLeftOther);
                        }
                        relationTreeItem.treeElement.data("kendoTreeView").enable(relationTreeItem.treeNodeRightLeftOther, true);


                    })
                    .fail(function (jqxhr, textStatus, error) {

                        // stuff
                    });
            }
        }


        $(this).data("relationtree-item", relationTreeItem);

        relationTreeItem.toolbarElement = $("<div class='toolbar'></div>");
        relationTreeItem.relationTreeElement.append(relationTreeItem.toolbarElement);
        relationTreeItem.treeElement = $("<div class='treeview'></div>");
        relationTreeItem.relationTreeElement.append(relationTreeItem.treeElement);

        relationTreeItem.toolbarElement.kendoToolBar({
            items: [
                {
                    template: '<button class="is-listen k-button"><i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></button>',

                },
                {
                    template: '<input class="next-order-id" type= "number" title= "numeric" value= "0" min= "0" step= "1" />'

                },
                {
                    template: '<button class="set-next-order-id k-button"><i class="fa fa-sort-amount-asc" aria-hidden="true"></i></button>',

                },
                { type: "separator" },
                {
                    type: "buttonGroup",
                    buttons: [
                        { text: "All", id: "all", togglable: true, group: "node-filter", selected: true },
                        { text: "Related", id: "related", togglable: true, group: "node-filter" },
                        { text: "Unrelated", id: "unrelated", togglable: true, group: "node-filter" },
                        { text: "Wrong", id: "wrong", togglable: true, group: "node-filter" }
                    ]
                }
            ],
            toggle: function (e) {
                var parentNodes = $('.k-in').closest('li').parent();
                if (parentNodes.length > 0) {
                    for (var ix = 0; ix < parentNodes.length; ix++) {
                        var parentNode = parentNodes[ix];
                        if ($(parentNode).hasClass('k-treeview-lines')) continue;
                        var childNodes = $(parentNode).find('li');
                        if (e.id === 'all') {
                            $(parentNode).children('.k-item').show();
                        }
                        else if (e.id === 'related') {
                            for (var jx = 0; jx < childNodes.length; jx++) {
                                var childNode = childNodes[jx];
                                if ($(childNode).find('.fa-circle').length === 0) {
                                    $(childNode).hide();
                                } else {
                                    $(childNode).show();
                                }
                            }

                        }
                        else if (e.id === 'unrelated') {
                            for (var jx = 0; jx < childNodes.length; jx++) {
                                var childNode = childNodes[jx];
                                if ($(childNode).find('.fa-circle').length > 0) {
                                    $(childNode).hide();
                                } else {
                                    $(childNode).show();
                                }
                            }
                        }
                        else if (e.id === 'wrong') {
                            for (var jx = 0; jx < childNodes.length; jx++) {
                                var childNode = childNodes[jx];
                                if ($(childNode).find('span[style*="color:sandybrown"]').length > 0) {
                                    $(childNode).show();
                                }
                                else {
                                    $(childNode).hide();
                                }

                            }
                        }
                    }
                }

            }
        });

        relationTreeItem.toolbarElement.find('.k-button').kendoButton({
            click: function (e) {

                if ($(e.event.target).closest(".k-button").hasClass('is-listen')) {
                    relationTreeItem.isToggledListen(true);
                }
                else if ($(e.event.target).closest(".k-button").hasClass('set-next-order-id')) {
                    relationTreeItem.isToggledSetNextOrderId(true);
                }
            }
        });

        relationTreeItem.relationTreeElement.find('.next-order-id').kendoNumericTextBox({ format: "#", decimals: 0 });

        relationTreeItem.orderIdElem = $(jQuery.grep(relationTreeItem.relationTreeElement.find('.next-order-id'), function (n, i) {
            return ($(n).data('kendoNumericTextBox') !== undefined);
        })).data('kendoNumericTextBox');



        $.post(relationTreeItem.configuration.actionRelationTreeInit)
            .done(function (response) {
                relationTreeItem.configuration.viewModel = response.Result;
                $.post(relationTreeItem.configuration.viewModel.ActionGetTreeConfig)
                    .done(function (response) {
                        // stuff

                        console.log(response);

                        response.dataSource = new kendo.data.HierarchicalDataSource(response.dataSource);
                        $.extend(response, { template: kendo.template($("#treeview-template").html()) });
                        $.extend(response,
                            {
                                select: function (e) {

                                    var nodeItem = relationTreeItem.Tree.dataItem(e.node);
                                    var parentItem = relationTreeItem.Tree.dataItem(relationTreeItem.Tree.parent(e.node));

                                    if (parentItem === undefined) return;
                                    if (parentItem.IdNode == relationTreeItem.Tree.dataItem(relationTreeItem.Tree.findByUid($(relationTreeItem.treeNodeAttribute[0]).data('uid'))).IdNode) {
                                        console.log("Selecting Attribute", nodeItem);
                                        var nodeItemToSend = {
                                            IdLeft: nodeItem.IdLeft,
                                            NameLeft: nodeItem.NameLeft,
                                            IdRight: nodeItem.IdRight,
                                            NameRight: nodeItem.NameRight,
                                            IdRelationType: nodeItem.IdRelationType,
                                            NameRelationType: nodeItem.NameRelationType,
                                            NodeType: nodeItem.NodeType
                                        };
                                        relationTreeItem.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                            ChannelId: relationTreeItem.configuration.viewModel.ChannelSelectedRelationNode,
                                            SenderId: relationTreeItem.configuration.managerConfig.idEndpoint,
                                            ReceiverId: relationTreeItem.configuration.viewModel.IdSender,
                                            SessionId: relationTreeItem.configuration.managerConfig.idSession,
                                            UserId: relationTreeItem.configuration.viewModel.IdUser,
                                            GenericParameterItems: [nodeItemToSend]
                                        });

                                        relationTreeItem.configuration.treeSelectCallback(nodeItemToSend);
                                    } else if (parentItem.IdNode == relationTreeItem.Tree.dataItem(relationTreeItem.Tree.findByUid($(relationTreeItem.treeNodeLeftRight[0]).data('uid'))).IdNode) {
                                        console.log("Selecting Left-Right", nodeItem);
                                        var nodeItemToSend = {
                                            IdLeft: nodeItem.IdLeft,
                                            NameLeft: nodeItem.NameLeft,
                                            IdRight: nodeItem.IdRight,
                                            NameRight: nodeItem.NameRight,
                                            IdRelationType: nodeItem.IdRelationType,
                                            NameRelationType: nodeItem.NameRelationType,
                                            NodeType: nodeItem.NodeType
                                        }
                                        relationTreeItem.configuration.treeSelectCallback(nodeItemToSend);

                                    } else if (parentItem.IdNode == relationTreeItem.Tree.dataItem(relationTreeItem.Tree.findByUid($(relationTreeItem.treeNodeRightLeft[0]).data('uid'))).IdNode) {
                                        console.log("Selecting Right-Left", nodeItem);
                                        var nodeItemToSend = {
                                            IdLeft: nodeItem.IdLeft,
                                            NameLeft: nodeItem.NameLeft,
                                            IdRight: nodeItem.IdRight,
                                            NameRight: nodeItem.NameRight,
                                            IdRelationType: nodeItem.IdRelationType,
                                            NameRelationType: nodeItem.NameRelationType,
                                            NodeType: nodeItem.NodeType
                                        };

                                        relationTreeItem.configuration.treeSelectCallback(nodeItemToSend)
                                    } else if (parentItem.IdNode == relationTreeItem.Tree.dataItem(relationTreeItem.Tree.findByUid($(relationTreeItem.treeNodeLeftRightOther[0]).data('uid'))).IdNode) {
                                        var nodeItemToSend = {
                                            IdLeft: nodeItem.IdLeft,
                                            NameLeft: nodeItem.NameLeft,
                                            IdRight: nodeItem.IdRight,
                                            NameRight: nodeItem.NameRight,
                                            IdRelationType: nodeItem.IdRelationType,
                                            NameRelationType: nodeItem.NameRelationType,
                                            NodeType: nodeItem.NodeType
                                        }
                                        relationTreeItem.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                            ChannelId: relationTreeItem.configuration.viewModel.ChannelSelectedRelationNode,
                                            SenderId: relationTreeItem.configuration.managerConfig.idEndpoint,
                                            ReceiverId: relationTreeItem.configuration.viewModel.IdSender,
                                            SessionId: relationTreeItem.configuration.managerConfig.idSession,
                                            UserId: relationTreeItem.configuration.viewModel.IdUser,
                                            GenericParameterItems: [nodeItemToSend]
                                        });

                                        relationTreeItem.configuration.treeSelectCallback(nodeItemToSend);
                                    } else if (parentItem.IdNode == relationTreeItem.Tree.dataItem(relationTreeItem.Tree.findByUid($(relationTreeItem.treeNodeRightLeftOther[0]).data('uid'))).IdNode) {
                                        var nodeItemToSend = {
                                            IdLeft: nodeItem.IdLeft,
                                            NameLeft: nodeItem.NameLeft,
                                            IdRight: nodeItem.IdRight,
                                            NameRight: nodeItem.NameRight,
                                            IdRelationType: nodeItem.IdRelationType,
                                            NameRelationType: nodeItem.NameRelationType,
                                            NodeType: nodeItem.NodeType
                                        }
                                        relationTreeItem.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                            ChannelId: relationTreeItem.configuration.viewModel.ChannelSelectedRelationNode,
                                            SenderId: relationTreeItem.configuration.managerConfig.idEndpoint,
                                            SessionId: relationTreeItem.configuration.managerConfig.idSession,
                                            ReceiverId: relationTreeItem.configuration.viewModel.IdSender,
                                            UserId: relationTreeItem.configuration.viewModel.IdUser,
                                            GenericParameterItems: [nodeItemToSend]
                                        });
                                        relationTreeItem.configuration.treeSelectCallback(nodeItemToSend);
                                    }

                                }
                            });
                        $.extend(response,
                            {
                                dataBound: relationTreeItem.treeViewDataBound,
                                change: relationTreeItem.treeViewChange
                            });
                        relationTreeItem.Tree = relationTreeItem.treeElement.kendoTreeView(response).data("kendoTreeView");
                        relationTreeItem.isEnabledTree(true);


                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });



                var configurationObjectList = {
                    actionObjectListInit: relationTreeItem.configuration.viewModel.ActionObjectListInit,
                    templateSelector: "#template",
                    managerConfig: viewModel.managerConfig,
                    useIdParent: true,
                    callbackGridCreated: function (gridItem) {

                    },
                    handler: {
                        initCompleteHandler: function () {
                            //viewModel.getUrlParameters();,
                            var senderIdNew = $('.object-list-container').data('object-list-config').configuration.viewModel.IdEndpoint;
                            addSenderToIgnoreList(senderIdNew, sendersIgnoreRelationEditor);
                        }
                    },
                    events: {
                        applyItems: function (items, parentComponent) {
                            $(relationTreeItem.windowContainerElement).data("kendoWindow").close();
                            relationTreeItem.relateItems(relationTreeItem.dataItemSelected, items, "Object");
                        }
                    }
                };

                relationTreeItem.windowContainerElement = $('<div class="window-container-relationtree"></div>');

                $(relationTreeItem.relationTreeElement).parent().parent().append(relationTreeItem.windowContainerElement);
                var relationTreeWindow = $('<div class="window-relationtree"></div>');
                $(relationTreeItem.windowContainerElement).append(relationTreeWindow);
                $(relationTreeWindow).append($('<div class= "object-list-container"></div>'));

                $(relationTreeItem.windowContainerElement).kendoWindow({
                    width: '1000px',
                    height: '800px',
                    title: 'Objects',
                    visible: false,
                    modal: true,
                    actions: [
                        "Pin",
                        "Minimize",
                        "Maximize",
                        "Close"
                    ]
                }).data("kendoWindow").center();


                $(relationTreeItem.windowContainerElement).find('.object-list-container').createObjectList(configurationObjectList);

                $("#windowClipboard").kendoWindow({
                    width: '1000px',
                    height: '800px',
                    title: 'Clipboard',
                    visible: false,
                    modal: true,
                    close: function (e) {
                        if (typeof relationTreeItem.data.appliedClipboardItems === 'undefined' || relationTreeItem.data.appliedClipboardItems == null) {
                            if (typeof relationTreeItem.data.classItem !== 'undefined' && relationTreeItem.data.classItem !== null) {
                                $(relationTreeItem.windowContainerElement).find('.object-list-container').data('object-list-config').handler.setIdParent(relationTreeItem.data.classItem.GUID);
                                $(relationTreeItem.windowContainerElement).data("kendoWindow").open();
                            }
                        }
                        else {
                            relationTreeItem.relateItems(relationTreeItem.dataItemSelected, relationTreeItem.data.appliedClipboardItems);
                        }
                        relationTreeItem.data.appliedClipboardItems = null;
                    },
                    actions: [
                        "Pin",
                        "Minimize",
                        "Maximize",
                        "Close"
                    ]
                }).data("kendoWindow").center();

                relationTreeItem.components.windowClipboard = $("#windowClipboard").data("kendoWindow");

                var configurationClipboard = {
                    selectorPath: ["#windowClipboard"],
                    actionInit: viewModel.ActionConfig.ActionClipboardInit,
                    managerConfig: viewModel.managerConfig,
                    events: {
                        applyItems: function (clipboardComponent, oItems) {
                            relationTreeItem.data.appliedClipboardItems = oItems;
                            relationTreeItem.components.windowClipboard.close();
                        },
                        validatedReference: function (clipboardComponent) {
                            if (relationTreeItem.components.clipboardComponent.data.itemsExist) {
                                relationTreeItem.components.windowClipboard.open();
                            } else {
                                $(relationTreeItem.windowContainerElement).find('.object-list-container').data('object-list-config').handler.setIdParent(relationTreeItem.data.classItem.GUID);
                                $(relationTreeItem.windowContainerElement).data("kendoWindow").open();
                            }
                        },
                        createdComponent: function (clipboardComponent) {
                            relationTreeItem.components.clipboardComponent = clipboardComponent;
                            relationTreeItem.checkLoad(relationTreeItem.relTreeStateMachine.ClipboardComplete);
                        }
                    }
                }

                $('#windowClipboard').createClipboard(configurationClipboard);
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    }
})(jQuery);