﻿var disconnectingDialog;

function registerHierarchy(idEndpoint, idParent) {
    viewModel.managerConfig.moduleComHub.server.registerHierarchy(idEndpoint, idParent);
}

function initWebsocket(viewModel, idSession, idEndpoint, idUser, connectCallback, disconnectCallback) {
    viewModel.managerConfig = {
        idSession: null,
        idEndpoint: null,
        endPoints: [],
        tryingToReconnect: false,
        connection: null,
        moduleComHub: null,
        disconnectCallback: disconnectCallback,
        connectCallback: connectCallback,
        idUser: null
    }
    viewModel.managerConfig.idSession = idSession;
    viewModel.managerConfig.idEndpoint = idEndpoint;
    viewModel.managerConfig.endPoints = [];
    viewModel.managerConfig.tryingToReconnect = false;
    viewModel.managerConfig.connection = $.connection;
    viewModel.managerConfig.moduleComHub = viewModel.managerConfig.connection.moduleComHub;
    viewModel.managerConfig.idUser = idUser

    viewModel.addEndpoint = function (idChannel) {
        viewModel.managerConfig.endPoints.push({
            ChannelTypeId: idChannel,
            SessionId: viewModel.managerConfig.idSession,
            EndPointId: viewModel.managerConfig.idEndpoint,
            EndpointType: 0,
            UserId: viewModel.managerConfig.idUser
        });
    }

    viewModel.websocketStarted = function() {
        for (var ixEndpoint in viewModel.managerConfig.endPoints) {
            var endPoint = viewModel.managerConfig.endPoints[ixEndpoint];
            viewModel.managerConfig.moduleComHub.server.registerEndpoint(endPoint);
        }

        if (viewModel.managerConfig.connectCallback != undefined) {
            viewModel.managerConfig.connectCallback();
        }
    }

    viewModel.disconnecting = function() {
        viewModel.managerConfig.connection.hub.reconnecting(function () {
            viewModel.managerConfig.tryingToReconnect = true;
        });

        viewModel.managerConfig.connection.hub.reconnected(function () {
            viewModel.managerConfig.tryingToReconnect = false;
        });

        viewModel.managerConfig.connection.hub.disconnected(function () {
            if (viewModel.managerConfig.tryingToReconnect) {
                viewModel.notifyUserOfDisconnect(); // Your function to notify user.
            }
        });
    }

    viewModel.notifyUserOfDisconnect = function() {
        
        if (viewModel.managerConfig.disconnectCallback != null) {
            viewModel.managerConfig.disconnectCallback();
        }
        viewModel.disconnectingDialog.kendoDialog({
            width: "400px",
            title: "Eventhandler disconnected",
            closable: false,
            modal: true,
            content: "<p>Your eventhandler is disconnected.<p>",
            actions: [
                { text: 'Try to reconnect', primary: true }
            ],
            close: viewModel.disconnecting
        });
        $(viewModel.disconnectingDialog).data("kendoDialog").open();
    }

    viewModel.startWebsocket = function(messageHandler) {
        
        viewModel.managerConfig.moduleComHub.client.addNewMessageToPage = messageHandler;
        viewModel.managerConfig.connection.hub.start({ pingInterval: 10000 }).done(viewModel.websocketStarted);

        $('body').append("<div id='disconnectDialog'></div>");
        viewModel.disconnectingDialog = $('#disconnectDialog');
        viewModel.disconnecting();
    }
}

function addSenderToIgnoreList(senderIdNew, sendersToCheck) {
    if (typeof sendersToCheck !== 'undefined') {
        if ((sendersToCheck.length === 0) || !$.grep(sendersToCheck, function (senderId) {
            return (senderId === senderIdNew);
        })) {

            sendersToCheck.push(senderIdNew);
        }
    }
}
function checkSendeserForIgnore(sendersToCheck, message) {
    var result = ($.grep(sendersToCheck, function (senderId) {
        if (message.SenderId === senderId)
            return true;
        else
            return false;
    }));
    return (result.length > 0);
}

    
