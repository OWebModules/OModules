﻿(function ($) {
    $.fn.createMultiSelect = function (configuration) {
        $(this).data('onto-multiselect-config', {
            configuration: configuration,
            elements: {
                container: null,
                multiSelect: null,
                kenoMultiSelect: null
            }


        });

        $(this).data('onto-multiselect-config').elements.container = $(this);
        $(this).addClass('onto-multiselect');

        $(this).html('');

        $(this).data('onto-multiselect-config').elements.multiSelect = $('<select kendo-multi-select k-options="selectOptions" k-ng-model="selectedIds"></select>');
        $(this).append($(this).data('onto-multiselect-config').elements.multiSelect);
        var lastMultiSelect = $(this);

        $.post($(this).data('onto-multiselect-config').configuration.actionGetMultiSelectConfig,{
            IdObject: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdObject,
            IdRelationType: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdRelationType,
            IdClassOther: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdClassOther,
            IdDirection: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdDirection
        })
            .done(function (response) {
                if (response.IsSuccessful) {

                    //$.extend(response.Result.dataSource.transport.read, {
                    //    data: function () {
                    //        return {
                    //            IdObject: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdObject,
                    //            IdRelationType: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdRelationType,
                    //            IdClassOther: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdClassOther,
                    //            IdDirection: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdDirection
                    //        };
                    //    }
                    //});

                    $.extend(response.Result, {
                        popup: {
                            appendTo: lastMultiSelect.data('onto-multiselect-config').elements.container
                        }
                    });

                    if (response.Result.noDataTemplate !== undefined) {
                        response.Result.noDataTemplate = $("#" + response.Result.noDataTemplate).html();
                    }

                    $.extend(response.Result.dataSource.transport, {
                        parameterMap: function (options, operation) {
                            if (operation !== "read" && options.models) {
                                return {
                                    models: kendo.stringify(options.models),
                                    IdObject: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdObject,
                                    IdRelationType: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdRelationType,
                                    IdClassOther: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdClassOther,
                                    IdDirection: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdDirection
                                };
                            } else if (operation === "read") {
                                return {
                                    IdObject: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdObject,
                                    IdRelationType: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdRelationType,
                                    IdClassOther: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdClassOther,
                                    IdDirection: lastMultiSelect.data('onto-multiselect-config').configuration.request.IdDirection
                                };
                            }
                        }
                    });

                    var dataSource = new kendo.data.DataSource(response.Result.dataSource);
                    response.Result.dataSource = dataSource;
                    lastMultiSelect.data('onto-multiselect-config').elements.kendoMultiSelect = lastMultiSelect.find('select').kendoMultiSelect(
                        response.Result);
                }

         
                if (lastMultiSelect.data('onto-multiselect-config').configuration.getGridConfigCompleted !== undefined) {
                    lastMultiSelect.data('onto-multiselect-config').configuration.getGridConfigCompleted();
                }
                


            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });


    };
})(jQuery);