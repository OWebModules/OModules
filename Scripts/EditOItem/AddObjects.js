﻿var addObjectRowsContainer;
var classAddSingleObjectConfig = "add-single-object-config";
var classAddSingleObjectElement = "add-single-object";
var parentAddObjectsSelector = ".add-objects-form-container";
(function ($) {
    $.fn.createAddSingleObject = function (configuration) {
        $('body').openPageLoader();
        $(this).data(classAddSingleObjectConfig, {
            elements: {
                addObjectRowsContainerElement: null,
                addSingleObjectElement: null,
                addObjectsContainer: null
            },
            configuration: configuration,
            handler: {
                createControl: function(component) {
                    var objecSaveComponent = $(objectSaveTemplate);
                    component.elements.addObjectsContainer = objecSaveComponent;
                    component.addObjectRowsContainerElement.append(objecSaveComponent);
                    $(addObjectRowsContainer.elements.addObjectsContainer).find('.k-button').kendoButton();
                    changeValuesFromServerChildren(addObjectRowsContainer.configuration.viewModel.ViewItems, [parentAddObjectsSelector]);
                    component.handler.checkRows();
                    changeValuesFromServerChildren(addObjectRowsContainer.configuration.viewModel.AddObjectRows, [parentAddObjectsSelector]);
                    addObjectRowsContainer.handler.setButtonPosition();

                    if (typeof component.configuration.events !== 'undefined' &&
                        addObjectRowsContainer.configuration.events !== null &&
                        component.configuration.events.createCompleted !== undefined &&
                        addObjectRowsContainer.configuration.events.createCompleted !== null) {
                        component.configuration.events.createCompleted(addObjectRowsContainer);
                    }
                },
                setButtonPosition: function () {
                    //var buttonGroupWidth = $(parentAddObjectsSelector).find(".save-button-container").width();
                    var buttonWidth = $(parentAddObjectsSelector).find(".applySaved").width();
                    buttonWidth += $(parentAddObjectsSelector).find(".removeRow").width();
                    buttonWidth += $(parentAddObjectsSelector).find(".saveObjects").width();
                    
                    var inputPosition = $(parentAddObjectsSelector).find(".addObjectRow0").position();
                    var inputLeft = inputPosition.left;
                    var inputHeight = $(parentAddObjectsSelector).find(".addObjectRow0").height() + 10;
                    var inputTop = inputPosition.top - 4;
                    var inputWidth = $(parentAddObjectsSelector).find(".addObjectRow0").width();

                    var buttonGroupLeft = inputLeft + inputWidth - buttonWidth;
                    $(parentAddObjectsSelector).find(".save-button-container").width(buttonWidth)
                    $(parentAddObjectsSelector).find(".save-button-container").css('left', buttonGroupLeft + 'px');
                    $(parentAddObjectsSelector).find(".save-button-container").css('top', inputTop + 'px');
                    $(parentAddObjectsSelector).find(".save-button-container").find(".btn").css('height', inputHeight + 'px');

                },
                checkOrUncheckApply: function (element) {
                    $(element).toggleMode(true);
                },
                checkRows: function () {
                    for (var ix in addObjectRowsContainer.configuration.viewModel.AddObjectRows) {
                        var addObjectRow = addObjectRowsContainer.configuration.viewModel.AddObjectRows[ix];
                        var objectRows = $(addObjectRowsContainer.elements.addObjectsContainer)
                            .find('.' + addObjectRow.ViewItemId);
                        if (objectRows.length === 0) {
                            var template = b64_to_utf8(getViewItemValue(addObjectRow, "Template", ""));
                            
                            var rowItem = $(template);
                            $(addObjectRowsContainer.elements.addObjectsContainer).append(rowItem);
                            objectRows = $(addObjectRowsContainer.elements.addObjectsContainer)
                                .find('.' + addObjectRow.ViewItemId);
                        }
                    }

                    var objectRowsToCheck = $(addObjectRowsContainer.elements.addObjectsContainer)
                        .find('.' + addObjectRowsContainer.configuration.viewModel.AddObjectsPrefix);

                    for (var ix = 0; ix < objectRowsToCheck.length; ix++) {
                        var objectRowToCheck = objectRowsToCheck[ix];
                        var objectRowsExist = jQuery.grep(addObjectRowsContainer.configuration.viewModel.AddObjectRows, function (row) {
                            return $(objectRowToCheck).hasClass(row.ViewItemId);
                        });
                        if (objectRowsExist.length == 0) {
                            $(objectRowToCheck).parent().remove();
                        }
                    }
                    
                },
                removeEmptyObjectLines: function () {

                    $('body').openPageLoader();
                    $.post(addObjectRowsContainer.configuration.viewModel.ActionRemoveEmptyObjectsRow, { idInstance: addObjectRowsContainer.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            addObjectRowsContainer.configuration.viewModel.AddObjectRows = response.AddObjectRows;
                            changeValuesFromServerChildren(response.ViewItems, [ parentAddObjectsSelector ]);
                            addObjectRowsContainer.handler.checkRows();
                            changeValuesFromServerChildren(response.AddObjectRows, [ parentAddObjectsSelector ]);
                            $('body').closePageLoader();

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                keyPress: function (e, element) {
                    if (e.key === 'Tab' || e.keyCode === 9) {
                        var viewItem = $(element).data('viewItem');
                        for (var ix in addObjectRowsContainer.configuration.viewModel.AddObjectRows) {
                            var addObjectRow = addObjectRowsContainer.configuration.viewModel.AddObjectRows[ix];
                            if (addObjectRow.ViewItemId == viewItem.ViewItemId) {
                                break;
                            }
                        }

                        if (ix == addObjectRowsContainer.configuration.viewModel.AddObjectRows.length - 1) {
                            $('body').openPageLoader();
                            $.post(addObjectRowsContainer.configuration.viewModel.ActionAddAddObjectsRow, { idInstance: addObjectRowsContainer.configuration.viewModel.IdInstance })
                                .done(function (response) {
                                    addObjectRowsContainer.configuration.viewModel.AddObjectRows = response.AddObjectRows;
                                    changeValuesFromServerChildren(response.ViewItems, [ parentAddObjectsSelector ]);
                                    addObjectRowsContainer.handler.checkRows();
                                    changeValuesFromServerChildren(response.AddObjectRows, [ parentAddObjectsSelector ]);
                                    $('body').closePageLoader();

                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    // stuff
                                });
                        }
                        //addObjectRowsContainer.handler.addObjectLine(element);
                    }
                },
                checkName: function(element) {
                    var viewItem = $(element).data('viewItem');
                    if (typeof viewItem !== 'undefined' && viewItem !== null) {
                        var value = $(element).val();
                        $('body').openPageLoader();
                        changeValue(viewItem,
                            "Content",
                            value,
                            true,
                            function changed(response) {
                                changeValuesFromServerChildren(response.ViewItems, [ parentAddObjectsSelector ]);
                                $('body').closePageLoader();
                            },
                            addObjectRowsContainer.configuration.viewModel.IdInstance);
                    }
                },
                paste: function (element) {
                    var viewItemId = $(element).data('viewItem').ViewItemId;
                    
                    var value = (event.clipboardData || window.clipboardData).getData('text');
                    var value64 = utf8_to_b64(value);
                    $.post(addObjectRowsContainer.configuration.viewModel.ActionPasteText, { pasted: value64, viewItemId: viewItemId, idInstance: addObjectRowsContainer.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            changeValuesFromServerChildren(response.ViewItems, [ parentAddObjectsSelector ]);
                            addObjectRowsContainer.configuration.viewModel.AddObjectRows = response.AddObjectRows;
                            addObjectRowsContainer.handler.checkRows();
                            changeValuesFromServerChildren(response.AddObjectRows, [ parentAddObjectsSelector ]);
                            $('body').closePageLoader();

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                saveObjects: function () {
                    $.post(addObjectRowsContainer.configuration.viewModel.ActionSaveObjects, { idInstance: addObjectRowsContainer.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            changeValuesFromServerChildren(response.ViewItems, [ parentAddObjectsSelector ]);
                            addObjectRowsContainer.configuration.viewModel.AddObjectRows = response.AddObjectRows;
                            addObjectRowsContainer.handler.checkRows();
                            changeValuesFromServerChildren(response.AddObjectRows, [ parentAddObjectsSelector ]);
                            addObjectRowsContainer.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                ChannelId: addObjectRowsContainer.configuration.viewModel.ChannelAddedObjects,
                                SenderId: addObjectRowsContainer.configuration.viewModel.IdEndpoint,
                                SessionId: addObjectRowsContainer.configuration.viewModel.IdInstance,
                                UserId: addObjectRowsContainer.configuration.viewModel.IdUser,
                                OItems: response.SavedObjects
                            });
                            if ($(parentAddObjectsSelector).find(".applySaved").getCheckedMode([ parentAddObjectsSelector ])) {
                                if (typeof addObjectRowsContainer.configuration.events !== 'undefined' && typeof addObjectRowsContainer.configuration.events.applyItems !== 'undefined') {
                                    addObjectRowsContainer.configuration.events.applyItems(response.SavedObjects);
                                }
                                else {
                                    addObjectRowsContainer.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                        ChannelId: addObjectRowsContainer.configuration.viewModel.ChannelAppliedObjects,
                                        SenderId: addObjectRowsContainer.configuration.viewModel.IdEndpoint,
                                        SessionId: addObjectRowsContainer.configuration.viewModel.IdInstance,
                                        UserId: addObjectRowsContainer.configuration.viewModel.IdUser,
                                        OItems: response.SavedObjects
                                    });
                                }
                                
                            }
                            $('body').closePageLoader();

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                validateReference: function (oItem, initName) {
                    $('body').openPageLoader();
                    $.post(addObjectRowsContainer.configuration.viewModel.ActionValidateReference, { refItem: oItem, initName: initName, idInstance: addObjectRowsContainer.configuration.viewModel.IdInstance})
                        .done(function (response) {
                            changeValuesFromServerChildren(response.ViewItems, [  "." + classAddSingleObjectElement ]);
                            addObjectRowsContainer.configuration.viewModel.AddObjectRows = response.AddObjectRows;
                            addObjectRowsContainer.handler.checkRows();
                            changeValuesFromServerChildren(response.AddObjectRows, [ "." + classAddSingleObjectElement ]);
                            $('body').closePageLoader();

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                }
            }
        });
        $(this).addClass(classAddSingleObjectElement);

        addObjectRowsContainer = $(this).data(classAddSingleObjectConfig);
        addObjectRowsContainer.addObjectRowsContainerElement = this;
        if (typeof addObjectRowsContainer.configuration.viewModel === 'undefined' || addObjectRowsContainer.configuration.viewModel === null) {
            $.post(addObjectRowsContainer.configuration.actionAddSingleObjectInit)
                .done(function (response) {
                    addObjectRowsContainer.configuration.viewModel = response.Result;
                    changeValuesFromServerChildren(addObjectRowsContainer.configuration.viewModel.ViewItems, [ "." + classAddSingleObjectElement ]);
                    addObjectRowsContainer.handler.createControl(addObjectRowsContainer);
                    $('body').closePageLoader();

                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        } else {
            addObjectRowsContainer.handler.createControl(addObjectRowsContainer);
            $('body').closePageLoader();
        }
    };
})(jQuery);

var objectSaveTemplate = `
<div class="add-objects-form-container">
    
    <div class="save-button-container btn-group" role="group">
        <button class="btn btn-secondary k-button applySaved" 
            onClick="javascript:addObjectRowsContainer.handler.checkOrUncheckApply(this);"><i class="fa fa-check-circle" aria-hidden="true"></i></button>
        <button class="btn btn-secondary k-button removeRow" 
            onClick="javascript:addObjectRowsContainer.handler.removeEmptyObjectLines();"><i class="fa fa-minus" aria-hidden="true"></i></button>
        <button class="btn btn-secondary k-button saveObjects"
            onClick="javascript:addObjectRowsContainer.handler.saveObjects();"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
    </div>
</div>
`