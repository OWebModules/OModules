﻿(function ($) {
    $.fn.createEditAttribute = function (configuration) {
        var editAttributeComponent = {
            data: {
                componentClass: 'edit-attribute-component',
                dataTypeIdBool: 'dd858f27d5e14363a5c33e561e432333',
                dataTypeIdDouble: 'a1244d0e187f46ee85742fc334077b7d',
                dataTypeIdDateTime: '905fda81788f4e3d83293e55ae984b9e',
                dataTypeIdLong: '3a4f5b7bda754980933efbc33cc51439',
                dataTypeIdString: '64530b52d96c4df186fe183f44513450',
                idAttribute: null,
                idAttributeType: null,
                idObject: null,
                objectAttribute: null,
                object: null,
                attributeType: null
            },
            components: {
            },
            elements: {
            },
            configuration: configuration,
            handler: {
                createComponent: function (editAttributeComponent) {
                    $(editAttributeComponent.elements.parentElement).html('');
                    editAttributeComponent.elements.editAttributeArea = $(editAttributeComponent.templates.editAttributeArea);
                    $(editAttributeComponent.elements.editAttributeArea).data(editAttributeComponent.data.componentClass, editAttributeComponent);
                    $(editAttributeComponent.elements.parentElement).append(editAttributeComponent.elements.editAttributeArea);

                    editAttributeComponent.elements.editBoolAttribute = $(editAttributeComponent.templates.editBoolAttribute);
                    $(editAttributeComponent.elements.parentElement).find('.attribute-edit').append(editAttributeComponent.elements.editBoolAttribute);
                    editAttributeComponent.data.boolBorder = $(editAttributeComponent.elements.editBoolAttribute).find('.boolValue').css('border');
                    $(editAttributeComponent.elements.editBoolAttribute).find('.boolValue').on('change', function () {
                        $(this).css('border', 'groove 6px blue');
                        var editAttributeComponent = $(this).closest('.edit-attribute-area').data('edit-attribute-component');
                        if (typeof editAttributeComponent.data.boolEditTimeout !== 'undefined') {
                            clearTimeout(editAttributeComponent.data.boolEditTimeout);
                        }
                        editAttributeComponent.data.boolEditTimeout = setTimeout(function (editAttributeComponent, value) {
                            $.post(editAttributeComponent.configuration.viewModel.ActionSaveBoolAttribute, { value: value, idInstance: editAttributeComponent.configuration.viewModel.IdInstance })
                                .done(function (response) {
                                    if (response.IsSuccessful) {
                                        var element = getElementsBySelectorPath(response.SelectorPath);
                                        var editAttributeComponent = $(element).data('edit-attribute-component');

                                        $(editAttributeComponent.elements.editBoolAttribute).find('.boolValue').css('border', editAttributeComponent.data.boolBorder);

                                        if (typeof editAttributeComponent.configuration.events !== 'undefined' && editAttributeComponent.configuration.events !== null &&
                                            typeof editAttributeComponent.configuration.events.changedValue !== 'undefined' && editAttributeComponent.configuration.events.changedValue !== null) {
                                            editAttributeComponent.configuration.events.changedValue(editAttributeComponent, response.Result, "Val_Bool", response.Result.Val_Bool, response.Result.Val_Name);
                                        }
                                    }
                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    // stuff
                                });

                        }, 500, editAttributeComponent, $(this).is(':checked'));
                    });

                    editAttributeComponent.elements.editDoubleAttribute = $(editAttributeComponent.templates.editDoubleAttribute);
                    $(editAttributeComponent.elements.parentElement).find('.attribute-edit').append(editAttributeComponent.elements.editDoubleAttribute);
                    editAttributeComponent.data.doubleBorder = $(editAttributeComponent.elements.editDoubleAttribute).find('.doubleValue').css('border');
                    $(editAttributeComponent.elements.editDoubleAttribute).find('.doubleValue').on('change', function () {
                        $(this).css('border', 'groove 6px blue');
                        var editAttributeComponent = $(this).closest('.edit-attribute-area').data('edit-attribute-component');
                        if (typeof editAttributeComponent.data.doubleEditTimeout !== 'undefined') {
                            clearTimeout(editAttributeComponent.data.doubleEditTimeout);
                        }
                        editAttributeComponent.data.doubleEditTimeout = setTimeout(function (editAttributeComponent, value) {
                            $.post(editAttributeComponent.configuration.viewModel.ActionSaveDoubleAttribute, { value: value, idInstance: editAttributeComponent.configuration.viewModel.IdInstance })
                                .done(function (response) {
                                    if (response.IsSuccessful) {
                                        var element = getElementsBySelectorPath(response.SelectorPath);
                                        var editAttributeComponent = $(element).data('edit-attribute-component');

                                        $(editAttributeComponent.elements.editDoubleAttribute).find('.doubleValue').css('border', editAttributeComponent.data.doubleBorder);
                                        if (typeof editAttributeComponent.configuration.events !== 'undefined' && editAttributeComponent.configuration.events !== null &&
                                            typeof editAttributeComponent.configuration.events.changedValue !== 'undefined' && editAttributeComponent.configuration.events.changedValue !== null) {
                                            editAttributeComponent.configuration.events.changedValue(editAttributeComponent, response.Result, "Val_Real", response.Result.Val_Real, response.Result.Val_Name);
                                        }
                                    }
                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    // stuff
                                });

                        }, 500, editAttributeComponent, $(this).val());
                    });

                    editAttributeComponent.elements.editDateTimeAttribute = $(editAttributeComponent.templates.editDateTimeAttribute);
                    $(editAttributeComponent.elements.parentElement).find('.attribute-edit').append(editAttributeComponent.elements.editDateTimeAttribute);
                    $(editAttributeComponent.elements.editDateTimeAttribute).find('.dateTimeValue').kendoDateTimePicker({
                        format: 'dd.MM.yyyy HH:mm:ss'
                    });
                    editAttributeComponent.data.dateTimeBorder = $(editAttributeComponent.elements.editDateTimeAttribute).find('.dateTimeValue').css('border');
                    $(editAttributeComponent.elements.editDateTimeAttribute).find('.dateTimeValue.k-input').on('change', function () {
                        $(this).css('border', 'groove 6px blue');
                        var editAttributeComponent = $(this).closest('.edit-attribute-area').data('edit-attribute-component');
                        if (typeof editAttributeComponent.data.dateTimeEditTimeout !== 'undefined') {
                            clearTimeout(editAttributeComponent.data.dateTimeEditTimeout);
                        }
                        editAttributeComponent.data.dateTimeEditTimeout = setTimeout(function (editAttributeComponent, value) {
                            editAttributeComponent.data.lastDatTimeValue = kendo.toString(value, "dd.MM.yyyy HH:mm:ss");
                            $.post(editAttributeComponent.configuration.viewModel.ActionSaveDateTimeAttribute, { value: editAttributeComponent.data.lastDatTimeValue, idInstance: editAttributeComponent.configuration.viewModel.IdInstance })
                                .done(function (response) {
                                    if (response.IsSuccessful) {
                                        var element = getElementsBySelectorPath(response.SelectorPath);
                                        var editAttributeComponent = $(element).data('edit-attribute-component');

                                        $(editAttributeComponent.elements.editDateTimeAttribute).find('.dateTimeValue').css('border', editAttributeComponent.data.dateTimeBorder);

                                        if (typeof editAttributeComponent.configuration.events !== 'undefined' && editAttributeComponent.configuration.events !== null &&
                                            typeof editAttributeComponent.configuration.events.changedValue !== 'undefined' && editAttributeComponent.configuration.events.changedValue !== null) {
                                            editAttributeComponent.configuration.events.changedValue(editAttributeComponent, response.Result, "Val_DateTime", response.Result.Val_DateTime, response.Result.Val_Name);
                                        }
                                    }
                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    // stuff
                                });

                        }, 500, editAttributeComponent, $(this).val());
                    });

                    editAttributeComponent.elements.editLongAttribute = $(editAttributeComponent.templates.editLongAttribute);
                    $(editAttributeComponent.elements.parentElement).find('.attribute-edit').append(editAttributeComponent.elements.editLongAttribute);
                    editAttributeComponent.data.longBorder = $(editAttributeComponent.elements.editLongAttribute).find('.longValue').css('border');
                    $(editAttributeComponent.elements.editLongAttribute).find('.longValue').on('change', function () {
                        $(this).css('border', 'groove 6px blue');
                        var editAttributeComponent = $(this).closest('.edit-attribute-area').data('edit-attribute-component');
                        if (typeof editAttributeComponent.data.longEditTimeout !== 'undefined') {
                            clearTimeout(editAttributeComponent.data.longEditTimeout);
                        }
                        editAttributeComponent.data.longEditTimeout = setTimeout(function (editAttributeComponent, value) {
                            $.post(editAttributeComponent.configuration.viewModel.ActionSaveLongAttribute, { value: value, idInstance: editAttributeComponent.configuration.viewModel.IdInstance })
                                .done(function (response) {
                                    if (response.IsSuccessful) {
                                        var element = getElementsBySelectorPath(response.SelectorPath);
                                        var editAttributeComponent = $(element).data('edit-attribute-component');

                                        $(editAttributeComponent.elements.editLongAttribute).find('.longValue').css('border', editAttributeComponent.data.longBorder);
                                        if (typeof editAttributeComponent.configuration.events !== 'undefined' && editAttributeComponent.configuration.events !== null &&
                                            typeof editAttributeComponent.configuration.events.changedValue !== 'undefined' && editAttributeComponent.configuration.events.changedValue !== null) {
                                            editAttributeComponent.configuration.events.changedValue(editAttributeComponent, response.Result, "Val_Long", response.Result.Val_Long, response.Result.Val_Name);
                                        }
                                    }
                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    // stuff
                                });

                        }, 500, editAttributeComponent, $(this).val());
                    });

                    editAttributeComponent.elements.editStringAttribute = $(editAttributeComponent.templates.editStringAttribute);
                    $(editAttributeComponent.elements.parentElement).find('.attribute-edit').append(editAttributeComponent.elements.editStringAttribute);
                    editAttributeComponent.data.stringBorder = $(editAttributeComponent.elements.editStringAttribute).find('.stringValue').css('border');
                    $(editAttributeComponent.elements.editStringAttribute).find('.stringValue').on('input paste', function () {
                        $(this).css('border', 'groove 6px blue');
                        var editAttributeComponent = $(this).closest('.edit-attribute-area').data('edit-attribute-component');
                        if (typeof editAttributeComponent.data.stringEditTimeout !== 'undefined') {
                            clearTimeout(editAttributeComponent.data.stringEditTimeout);
                        }
                        editAttributeComponent.data.stringEditTimeout = setTimeout(function (editAttributeComponent, value) {
                            editAttributeComponent.data.lastStringValue = value;
                            var b64Value = utf8_to_b64(editAttributeComponent.data.lastStringValue)
                            $.post(editAttributeComponent.configuration.viewModel.ActionSaveStringAttribute, { contentB64: b64Value, idInstance: editAttributeComponent.configuration.viewModel.IdInstance })
                                .done(function (response) {
                                    if (response.IsSuccessful) {
                                        var element = getElementsBySelectorPath(response.SelectorPath);
                                        var editAttributeComponent = $(element).data('edit-attribute-component');

                                        $(editAttributeComponent.elements.editStringAttribute).find('.stringValue').css('border', editAttributeComponent.data.stringBorder);
                                        if (typeof editAttributeComponent.configuration.events !== 'undefined' && editAttributeComponent.configuration.events !== null &&
                                            typeof editAttributeComponent.configuration.events.changedValue !== 'undefined' && editAttributeComponent.configuration.events.changedValue !== null) {
                                            editAttributeComponent.configuration.events.changedValue(editAttributeComponent, response.Result, "Val_String", response.Result.Val_String, response.Result.Val_Name);
                                        }
                                    }
                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    // stuff
                                });

                        }, 500, editAttributeComponent, $(this).val());
                    });

                    $(editAttributeComponent.elements.editBoolAttribute).hide();
                    $(editAttributeComponent.elements.editDoubleAttribute).hide();
                    $(editAttributeComponent.elements.editDateTimeAttribute).hide();
                    $(editAttributeComponent.elements.editLongAttribute).hide();
                    $(editAttributeComponent.elements.editStringAttribute).hide();

                    if (typeof editAttributeComponent.configuration.events !== 'undefined' && editAttributeComponent.configuration.events !== null &&
                        typeof editAttributeComponent.configuration.events.createdComponent !== 'undefined' && editAttributeComponent.configuration.events.createdComponent !== null) {
                        editAttributeComponent.configuration.events.createdComponent(editAttributeComponent);
                    }
                },
                validateReference: function (editAttributeComponent, idAttribute, idObject, idAttributeType) {
                    editAttributeComponent.data.idAttribute = idAttribute;
                    editAttributeComponent.data.idAttributeType = idAttributeType;
                    editAttributeComponent.data.idObject = idObject;

                    $.post(editAttributeComponent.configuration.viewModel.ActionGetAttribute, { idAttribute: idAttribute, idAttributeType: idAttributeType, idObject: idObject, idInstance: editAttributeComponent.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            if (response.isOk) {
                                var selectorPath = response.SelectorPath;
                                var element = getElementsBySelectorPath(selectorPath);
                                var editAttributeComponent = $(element).data('edit-attribute-component');

                                editAttributeComponent.data.objectAttribute = response.ObjectAttribute;
                                editAttributeComponent.data.object = response.Object;
                                editAttributeComponent.data.attributeType = response.AttributeType;

                                if (editAttributeComponent.data.objectAttribute !== null) {
                                    if (editAttributeComponent.data.attributeType.GUID_Parent == editAttributeComponent.data.dataTypeIdBool) {
                                        $(editAttributeComponent.elements.editBoolAttribute).show();
                                        if (editAttributeComponent.data.objectAttribute.Val_Bool) {
                                            $(editAttributeComponent.elements.editBoolAttribute).find('.boolValue').attr("checked", true);
                                        } else {
                                            $(editAttributeComponent.elements.editBoolAttribute).find('.boolValue').attr("checked", false);
                                        }

                                    } else if (editAttributeComponent.data.attributeType.GUID_Parent == editAttributeComponent.data.dataTypeIdDouble) {
                                        $(editAttributeComponent.elements.editDoubleAttribute).show();
                                        $(editAttributeComponent.elements.editDoubleAttribute).find('.doubleValue').val(editAttributeComponent.data.objectAttribute.Val_Real);
                                    } else if (editAttributeComponent.data.attributeType.GUID_Parent == editAttributeComponent.data.dataTypeIdDateTime) {
                                        $(editAttributeComponent.elements.editDateTimeAttribute).show();
                                        $(editAttributeComponent.elements.editDateTimeAttribute).find('.dateTimeValue.k-input').data("kendoDateTimePicker").value(editAttributeComponent.data.objectAttribute.Val_Datetime);
                                    } else if (editAttributeComponent.data.attributeType.GUID_Parent == editAttributeComponent.data.dataTypeIdLong) {
                                        $(editAttributeComponent.elements.editLongAttribute).show();
                                        $(editAttributeComponent.elements.editLongAttribute).find('.longValue').val(editAttributeComponent.data.objectAttribute.Val_Lng);
                                    } else if (editAttributeComponent.data.attributeType.GUID_Parent == editAttributeComponent.data.dataTypeIdString) {
                                        $(editAttributeComponent.elements.editStringAttribute).show();
                                        $(editAttributeComponent.elements.editStringAttribute).find('.stringValue').val(editAttributeComponent.data.objectAttribute.Val_String);
                                    }
                                }
                                else {
                                    if (editAttributeComponent.data.attributeType.GUID_Parent == editAttributeComponent.data.dataTypeIdBool) {
                                        $(editAttributeComponent.elements.editBoolAttribute).show();

                                    } else if (editAttributeComponent.data.attributeType.GUID_Parent == editAttributeComponent.data.dataTypeIdDouble) {
                                        $(editAttributeComponent.elements.editDoubleAttribute).show();
                                    } else if (editAttributeComponent.data.attributeType.GUID_Parent == editAttributeComponent.data.dataTypeIdDateTime) {
                                        $(editAttributeComponent.elements.editDateTimeAttribute).show();
                                    } else if (editAttributeComponent.data.attributeType.GUID_Parent == editAttributeComponent.data.dataTypeIdLong) {
                                        $(editAttributeComponent.elements.editLongAttribute).show();
                                    } else if (editAttributeComponent.data.attributeType.GUID_Parent == editAttributeComponent.data.dataTypeIdString) {
                                        $(editAttributeComponent.elements.editStringAttribute).show();
                                    }
                                }
                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                    
                    if (typeof editAttributeComponent.configuration.events !== 'undefined' && editAttributeComponent.configuration.events !== null &&
                        typeof editAttributeComponent.configuration.events.validatedReference !== 'undefined' && editAttributeComponent.configuration.events.validatedReference !== null) {
                        editAttributeComponent.configuration.events.validatedReference(editAttributeComponent);
                    }
                }
            },
            templates: {
                editAttributeArea: `
                    <div class="edit-attribute-area">
                        <div class="k-content attribute-edit">
                            
                        </div>
                    </div>
                `,
                editBoolAttribute: `
                    <ul class="fieldlist">
                        <li>
                            <span>
                                <input type="checkbox" class="k-checkbox boolValue">
                                <label class="k-checkbox-label attributeLabel"></label>                
                            </span>
                        </li>
                    </ul>
                `,
                editDateTimeAttribute: `
                    <ul class="fieldlist">
                        <li>
                            <span>
                                <input class="dateTimeValue" title="datetimepicker" style="width: 100%;" />
                                <label class="attributeLabel"></label>
                            </span>
                        </li>
                    </ul>
                `,
                editDoubleAttribute: `
                    <ul class="fieldlist">
                        <li>
                            <span>
                                <input class="doubleValue" type="number"/>
                                <label class="attributeLabel"></label>
                            </span>
                        </li>
                    </ul>
                `,
                editLongAttribute: `
                    <ul class="fieldlist">
                        <li>
                            <span>
                                <input class="longValue" type="number"/>
                                <label class="attributeLabel"></label>
                            </span>
                        </li>
                    </ul>
                `,
                editStringAttribute: `
                    <ul class="fieldlist">
                        <li>
                            <span>
                                <textarea class="stringValue" rows="10"></textarea>
                                <label class="attributeLabel"></label>
                            </span>
                        </li>
                    </ul>
                `
            }
        }

        editAttributeComponent.elements.parentElement = this;
        $(editAttributeComponent.elements.parentElement).data(editAttributeComponent.data.componentClass, editAttributeComponent);

        $.post(editAttributeComponent.configuration.actionInit, {
            idInstance: editAttributeComponent.configuration.IdInstance,
            selectorPath: editAttributeComponent.configuration.selectorPath })
            .done(function (response) {
                // stuff
                console.log(response);
                if (response.IsSuccessful) {
                    var selectorPath = response.SelectorPath;
                    var element = getElementsBySelectorPath(selectorPath);
                    var editAttributeComponent = $(element).data('edit-attribute-component');
                    editAttributeComponent.configuration.viewModel = response.Result;
                    editAttributeComponent.handler.createComponent(editAttributeComponent);
                } else {

                }

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    }
})(jQuery);