﻿(function ($) {
    $.fn.createWrikeSyncList = function (configuration) {
        $(this).data('wrike-sync-list-config', {
            configuration: configuration,
            elements: {
                gridElement: null,
                telerikGrid: null
            },
            dataBoundHandler: null,
            gridChangeHandler: null,
            clickButtonHandler: null,

            validateReference: function (idObject, validateReferenceCallback) {
                $('.wrike-sync-list').openPageLoader();
                $('.wrike-sync-list').data('wrike-sync-list-config').idObject = idObject;
                var refItem = {
                    GUID: idObject
                }
                $.post($('.wrike-sync-list').data('wrike-sync-list-config').configuration.actionValidateReference, {
                    refItem: refItem,
                    idInstance: $('.wrike-sync-list').data('wrike-sync-list-config').configuration.idInstance
                })
                    .done(function (response) {
                        console.log(response);
                        if (response.IsSuccessful) {
                            $('.wrike-sync-list').data('wrike-sync-list-config').elements.telerikGrid.dataSource.read();

                        }

                        if (validateReferenceCallback !== undefined) {
                            validateReferenceCallback();
                        }
                        $('.wrike-sync-list').closePageLoader();
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            }
        });

        $(this).addClass("wrike-sync-list");

        $('.wrike-sync-list').data('wrike-sync-list-config').elements.gridElement = $("<div class='wrike-sync-grid'></div>");
        $(this).append($('.wrike-sync-list').data('wrike-sync-list-config').elements.gridElement);

        $.post($('.wrike-sync-list').data('wrike-sync-list-config').configuration.actionGetGridConfig)
            .done(function (response) {

                $.extend(response.dataSource, {
                    batch: true
                });

                $.extend(response.dataSource.transport.read, {
                    data: function () {
                        
                            return {
                                IdInstance: $('.wrike-sync-list').data('wrike-sync-list-config').configuration.idInstance
                            }
                        

                    }
                })

                $.extend(response.dataSource.transport.update,
                    {
                        complete: function (e) {
                            console.log(e);
                        }
                    }
                );

                var dataSource = new kendo.data.DataSource(response.dataSource);
                response.dataSource = dataSource;
                response.dataBound = $('.wrike-sync-list').data('wrike-sync-list-config').dataBoundHandler;

                $.extend(response, {
                    change: $('.wrike-sync-list').data('wrike-sync-list-config').gridChangeHandler,
                    filterable: {
                        mode: "row"
                    }
                });
                $('.wrike-sync-list').data('wrike-sync-list-config').elements.telerikGrid = $('.wrike-sync-list').find('.wrike-sync-grid').kendoGrid(response).data("kendoGrid");
                $('.k-button').kendoButton({
                    click: $('.wrike-sync-list').data('wrike-sync-list-config').clickApplyButtonHandler
                });


                $('.wrike-sync-list').data('wrike-sync-list-config').elements.telerikGrid.wrapper.find(".k-pager")
                    .before('<a href="#" class="k-link prev">Prev</a>')
                    .after('<a href="#" class="k-link next">Next</a>')
                    .parent()
                    .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                    .delegate("a.prev", "click", function () {
                        dataSource.page(dataSource.page() - 1);
                    })
                    .delegate("a.next", "click", function () {
                        dataSource.page(dataSource.page() + 1);
                    })
                    .delegate(".pagesize input", "change", function () {
                        dataSource.page(parseInt(this.value, 10));
                    })






                


            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });


        if ($('.wrike-sync-list').data('wrike-sync-list-config').configuration != undefined) {

            $('.wrike-sync-list').data('wrike-sync-list-config').configuration.createdCallback();
        }
    }
})(jQuery);