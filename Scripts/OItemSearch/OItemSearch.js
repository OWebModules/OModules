﻿(function ($) {
    $.fn.createSearchArea = function (removable, id) {
        var oItemSearchItem = $('.search-container').data('oitem-search-config');
        var divItem = $(this);

        if (id == undefined) {
            id = "_" + guid();
        }

        var rowItem = $('<div class="row"></div>');
        divItem.append(rowItem);

        var colItem = $('<div class="col-sm-12"></div>');
        rowItem.append(colItem);

        var wellItem = $('<div class="well well-sm fullWidth"></div>');
        colItem.append(wellItem);

        var rowItem2 = $('<div class="row"></div>')
        wellItem.append(rowItem2);

        var colItemSearchButtons = $('<div class="col-sm-1 searchButtons"></div>');

        rowItem2.append(colItemSearchButtons);

        var colItemSearchText = $('<div class="col-sm-9 searchText"></div>');

        rowItem2.append(colItemSearchText);

        var colItemSearchActions = $('<div class="col-sm-1 searchActions"></div>');

        rowItem2.append(colItemSearchActions);

        var buttonGroup = $('<div class="btn-group pull-right" role="group" >');
        colItemSearchActions.append(buttonGroup);

        var removeButton = $('<button class="removeButton"><i class="fa fa-times" aria-hidden="true"></i></button>');
        if (removable) {
            buttonGroup.append(removeButton);
        }

        var addButton = $('<button class="addButton"><i class="fa fa-plus" aria-hidden="true"></i></button>')
        buttonGroup.append(addButton);

        wellItem.attr('id', id);

        var classButton = $('<button class="classButton searchButton"><i class="fa fa-puzzle-piece" aria-hidden="true"></i></button>');
        var objectButton = $('<button class="objectButton searchButton"><i class="fa fa-cubes" aria-hidden="true"></i></button>');
        var relationTypeButton = $('<button class="relationTypeButton searchButton"><i class="fa fa-share-alt-square" aria-hidden="true"></i></button>');
        var attributeTypeButton = $('<button class="attributeTypeButton searchButton"><i class="fa fa-sitemap" aria-hidden="true"></i></button>');
        var leftRightTypeButton = $('<button class="leftRightButton searchButton"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></button>');
        var rightLeftTypeButton = $('<button class="rightLeftButton searchButton"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i></button>');


        var table = $('<table></table>');
        colItemSearchButtons.append(table);

        var row1 = $('<tr></tr>');
        var row2 = $('<tr></tr>');

        table.append(row1);
        table.append(row2);

        var classCell = $('<td></td>');
        classCell.append(classButton);

        var objectCell = $('<td></td>');
        objectCell.append(objectButton);

        var relationTypeCell = $('<td></td>');
        relationTypeCell.append(relationTypeButton);

        var attributeTypeCell = $('<td></td>');
        attributeTypeCell.append(attributeTypeButton);

        var leftRightTypeCell = $('<td></td>');
        leftRightTypeCell.append(leftRightTypeButton);

        var rightLeftTypeCell = $('<td></td>');
        rightLeftTypeCell.append(rightLeftTypeButton);

        row1.append(classCell);
        row2.append(objectCell);

        row1.append(attributeTypeCell);
        row2.append(relationTypeCell);

        row1.append(leftRightTypeCell);
        row2.append(rightLeftTypeCell);

        wellItem.find('.classButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id'),
                type: 'classButton'
            };

            console.log(item);

            $.post(oItemSearchItem.configuration.actionGetSearchRequestItem, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                .done(function (response) {
                    if (response.IsSuccessful) {
                        var searchButtonArea = $('#' + response.SearchItem.id).find('.searchButtons');
                        var button = searchButtonArea.find('.' + response.SearchItem.type);
                        var icon = $(button.html());
                        icon.addClass('fa-2x');
                        searchButtonArea.html('');
                        searchButtonArea.append(icon);

                        var searchTextArea = $('#' + response.SearchItem.id).find('.searchText');
                        var dropDownInput = $("<input class='classDropDown' />");

                        searchTextArea.append(dropDownInput);
                        jQuery.extend(response.SearchItem.dropDownConfig, {
                            filter: "contains",
                            suggest: true
                        });

                        $.extend(response.SearchItem.dropDownConfig, {
                            change: function (e) {
                                var wellItem = $(this.element).closest('.well');
                                var value = this.value();
                                var item = {
                                    id: wellItem.attr('id'),
                                    searchText: null,
                                    type: 'Class'
                                };

                                if (value !== undefined && value !== '' && value !== null) {
                                    item.searchText = value;
                                }


                                $.post(oItemSearchItem.configuration.actionSearchClassChanged, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                                    .done(function (response) {
                                        if (response.IsSuccessful && response.GetData) {

                                            if (oItemSearchItem.loadingTimer !== null) {
                                                clearTimeout(oItemSearchItem.loadingTimer);
                                            }

                                            oItemSearchItem.loadingTimer = setTimeout(oItemSearchItem.loadData, oItemSearchItem.configuration);
                                        }
                                    })
                                    .fail(function (jqxhr, textStatus, error) {
                                        // stuff
                                    });
                            }
                        });
                        searchTextArea.find('.classDropDown').kendoComboBox(response.SearchItem.dropDownConfig);


                    }
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        });

        wellItem.find('.objectButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id'),
                type: 'objectButton'
            };

            console.log(item);

            $.post(oItemSearchItem.configuration.actionGetSearchRequestItem, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                .done(function (response) {
                    if (response.IsSuccessful) {
                        var searchButtonArea = $('#' + response.SearchItem.id).find('.searchButtons');
                        var button = searchButtonArea.find('.' + response.SearchItem.type);
                        var icon = $(button.html());
                        icon.addClass('fa-2x');
                        searchButtonArea.html('');
                        searchButtonArea.append(icon);

                        var searchTextArea = $('#' + response.SearchItem.id).find('.searchText');
                        var input = $('<input class="objectIdOrName fullWidth" placeholder="GUID or Name" /input>');
                        input.on("input", function () {
                            if (oItemSearchItem.searchTimer !== null) {
                                clearTimeout(oItemSearchItem.searchTimer);
                            }
                            oItemSearchItem.searchElement = this;

                            oItemSearchItem.searchTimer = setTimeout(function () {
                                var value = $(oItemSearchItem.searchElement).val();

                                var wellItem = $(oItemSearchItem.searchElement).closest('.well');
                                var item = {
                                    id: wellItem.attr('id'),
                                    type: 'Object',
                                    searchText: null
                                };

                                if (value !== undefined && value !== '' && value !== null) {
                                    item.searchText = value;
                                }

                                console.log(item);

                                $.post(oItemSearchItem.configuration.actionSearchItemTextChanged, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                                    .done(function (response) {
                                        if (response.IsSuccessful && response.GetData) {



                                            oItemSearchItem.loadingTimer = setTimeout(oItemSearchItem.loadData, oItemSearchItem.loadingTimeout);
                                        }
                                    })
                                    .fail(function (jqxhr, textStatus, error) {
                                        // stuff
                                    });
                            }, 300);
                        });
                        searchTextArea.append(input);
                    }
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        });

        wellItem.find('.relationTypeButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id'),
                type: 'relationTypeButton'
            };

            console.log(item);

            $.post(oItemSearchItem.configuration.actionGetSearchRequestItem, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                .done(function (response) {
                    if (response.IsSuccessful) {
                        var searchButtonArea = $('#' + response.SearchItem.id).find('.searchButtons');
                        var button = searchButtonArea.find('.' + response.SearchItem.type);
                        var icon = $(button.html());
                        icon.addClass('fa-2x');
                        searchButtonArea.html('');
                        searchButtonArea.append(icon);

                        var searchTextArea = $('#' + response.SearchItem.id).find('.searchText');
                        var table = $('<table class="fullWidth"></table>');
                        searchTextArea.append(table);

                        var tr1 = $('<tr></tr>');
                        table.append(tr1);

                        var td1 = $('<td></td>');
                        tr1.append(td1);

                        var tr2 = $('<tr></tr>');
                        table.append(tr2);

                        var td2 = $('<td class="valueCol"></td>');
                        tr2.append(td2);

                        var dropDownInput = $("<input class='relationTypeDropDown' />");

                        td1.append(dropDownInput);

                        jQuery.extend(response.SearchItem.dropDownConfig, {
                            filter: "contains",
                            suggest: true
                        });

                        $.extend(response.SearchItem.dropDownConfig, {
                            change: function (e) {
                                var wellItem = $(this.element).closest('.well');
                                var value = this.value();
                                var item = {
                                    id: wellItem.attr('id'),
                                    idRelationType: null,
                                    type: 'RelationType'
                                };
                                if (value === undefined || value === '' || value === null) {
                                    item.idRelationType = null;
                                }
                                else {
                                    item.idRelationType = value;
                                }


                                $.post(oItemSearchItem.configuration.actionSearchRelationTypeChanged, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                                    .done(function (response) {
                                        if (response.IsSuccessful && response.GetData) {

                                            if (oItemSearchItem.loadingTimer !== null) {
                                                clearTimeout(oItemSearchItem.loadingTimer);
                                            }

                                            oItemSearchItem.loadingTimer = setTimeout(oItemSearchItem.loadData, oItemSearchItem.loadingTimeout);
                                        }
                                    })
                                    .fail(function (jqxhr, textStatus, error) {
                                        // stuff
                                    });
                            }
                        });
                        searchTextArea.find('.relationTypeDropDown').kendoComboBox(response.SearchItem.dropDownConfig);


                    }
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        });

        wellItem.find('.attributeTypeButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id'),
                type: 'attributeTypeButton'
            };

            console.log(item);

            $.post(oItemSearchItem.configuration.actionGetSearchRequestItem, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                .done(function (response) {
                    if (response.IsSuccessful) {
                        var searchButtonArea = $('#' + response.SearchItem.id).find('.searchButtons');
                        var button = searchButtonArea.find('.' + response.SearchItem.type);
                        var icon = $(button.html());
                        icon.addClass('fa-2x');
                        searchButtonArea.html('');
                        searchButtonArea.append(icon);

                        var searchTextArea = $('#' + response.SearchItem.id).find('.searchText');
                        var table = $('<table class="fullWidth"></table>');
                        searchTextArea.append(table);

                        var tr1 = $('<tr></tr>');
                        table.append(tr1);

                        var td1 = $('<td></td>');
                        tr1.append(td1);

                        var tr2 = $('<tr></tr>');
                        table.append(tr2);

                        var td2 = $('<td class="valueCol"></td>');
                        tr2.append(td2);

                        var dropDownInput = $("<input class='attributeTypeDropDown' />");

                        td1.append(dropDownInput);

                        jQuery.extend(response.SearchItem.dropDownConfig, {
                            filter: "contains",
                            suggest: true
                        });

                        $.extend(response.SearchItem.dropDownConfig, {
                            change: function (e) {
                                var wellItem = $(this.element).closest('.well');
                                var value = this.value();
                                var item = {
                                    id: wellItem.attr('id'),
                                    idRelationType: null,
                                    type: 'AttributeType'
                                };
                                if (value === undefined || value === '' || value === null) {
                                    item.idAttributeType = null;
                                }
                                else {
                                    item.idAttributeType = value;
                                }


                                $.post(oItemSearchItem.configuration.actionSearchAttributeTypeChanged, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                                    .done(function (response) {
                                        if (response.IsSuccessful && response.GetData) {

                                            if (oItemSearchItem.loadingTimer !== null) {
                                                clearTimeout(oItemSearchItem.loadingTimer);
                                            }

                                            oItemSearchItem.loadingTimer = setTimeout(oItemSearchItem.loadData, oItemSearchItem.loadingTimeout);
                                        }
                                    })
                                    .fail(function (jqxhr, textStatus, error) {
                                        // stuff
                                    });
                            }
                        });
                        searchTextArea.find('.attributeTypeDropDown').kendoComboBox(response.SearchItem.dropDownConfig);


                    }
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        });

        wellItem.find('.leftRightButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id'),
                type: 'leftRightButton'
            };

            console.log(item);


            $.post(oItemSearchItem.configuration.actionGetSearchRequestItem, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                .done(function (response) {
                    if (response.IsSuccessful) {
                        var searchButtonArea = $('#' + response.SearchItem.id).find('.searchButtons');
                        var button = searchButtonArea.find('.' + response.SearchItem.type);
                        var icon = $(button.html());
                        icon.addClass('fa-2x');
                        searchButtonArea.html('');
                        searchButtonArea.append(icon);

                        var searchTextArea = $('#' + response.SearchItem.id).find('.searchText');
                        var input = $('<input class="idRelOrName fullWidth" placeholder="GUID or Name" /input>');
                        searchTextArea.append(input);
                        input.on("input", function () {
                            var wellItem = $(this).closest('.well');
                            var item = {
                                id: wellItem.attr('id'),
                                type: response.SearchItem.type,
                                searchText: $(this).val()
                            };

                            console.log(item);

                            $.post(oItemSearchItem.configuration.actionSearchItemTextChanged, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                                .done(function (response) {
                                    if (response.IsSuccessful && response.GetData) {



                                        oItemSearchItem.loadingTimer = setTimeout(oItemSearchItem.loadData, oItemSearchItem.loadingTimeout);
                                    }
                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    // stuff
                                });
                        });



                    }
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        });

        wellItem.find('.rightLeftButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id'),
                type: 'rightLeftButton'
            };

            console.log(item);

            $.post(oItemSearchItem.configuration.actionGetSearchRequestItem, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                .done(function (response) {
                    if (response.IsSuccessful) {
                        var searchButtonArea = $('#' + response.SearchItem.id).find('.searchButtons');
                        var button = searchButtonArea.find('.' + response.SearchItem.type);
                        var icon = $(button.html());
                        icon.addClass('fa-2x');
                        searchButtonArea.html('');
                        searchButtonArea.append(icon);

                        var searchTextArea = $('#' + response.SearchItem.id).find('.searchText');
                        var input = $('<input class="idRelOrName fullWidth" placeholder="GUID or Name" /input>');
                        searchTextArea.append(input);
                        input.on("input", function () {
                            var wellItem = $(this).closest('.well');
                            var item = {
                                id: wellItem.attr('id'),
                                type: response.SearchItem.type,
                                searchText: $(this).val()
                            };

                            console.log(item);

                            $.post(oItemSearchItem.configuration.actionSearchItemTextChanged, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                                .done(function (response) {
                                    if (response.IsSuccessful && response.GetData) {



                                        oItemSearchItem.loadingTimer = setTimeout(oItemSearchItem.loadData, oItemSearchItem.loadingTimeout);
                                    }
                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    // stuff
                                });
                        });



                    }
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        });

        wellItem.find('.removeButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id')
            };

            $.post(oItemSearchItem.configuration.actionRemoveSearchItemArea, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                .done(function (response) {
                    if (response.IsSuccessful) {
                        if (response.SearchItem.remove) {
                            var wellItem = $('#' + response.SearchItem.id);
                            var rowItem = wellItem.closest('.row');
                            rowItem.remove();
                            oItemSearchItem.searchCriteriaRowElement.createSearchArea(true, response.SearchItem.id);
                        } else {

                            oItemSearchItem.searchCriteriaRowElement.createSearchArea(true, response.SearchItem.id);
                        }

                    }
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });

        });

        wellItem.find('.addButton').on('click', function () {
            var item = {
                id: guid()
            };

            $.post(oItemSearchItem.configuration.actionAddSearchItemArea, { idInstance: oItemSearchItem.configuration.idInstance, searchItem: item })
                .done(function (response) {
                    if (response.IsSuccessful) {
                        oItemSearchItem.searchCriteriaRowElement.createSearchArea(true, response.SearchItem.id);

                    }
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        });
    };

    $.fn.createOItemSearch = function (configuration) {
        $(this).data('oitem-search-config', {
            configuration: configuration,
            searchContainerElement: this,
            searchAreaElement: null,
            searchCriteriaRowElement: null,
            panelBarElement: null,
            searchCountElement: null,
            resultToolbarElement: null,
            searchGrid: null,
            gridItem: null,
            gridRefresh: function () {
                oItemSearchItem.gridItem.dataSource.read();
            },
            gridChangeHandler: function (e) {
                var tr = this.select().parent();
                var dataItem = this.dataItem(tr);
                if (dataItem === undefined || dataItem === null) return;

                oItemSearchItem.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                    ChannelId: oItemSearchItem.configuration.channelSelectedObject,
                    SenderId: oItemSearchItem.configuration.idEndpoint,
                    SessionId: oItemSearchItem.configuration.idSession,
                    UserId: oItemSearchItem.configuration.idUser,
                    OItems: [{ GUID: dataItem.IdItem }]
                });
            },
            itemsToApply: [],
            loadingTimer: null,
            loadData: function () {
                clearTimeout(oItemSearchItem.loadingTimer);
                oItemSearchItem.searchCountElement.text('0');
                oItemSearchItem.gridRefresh();
            }


        });

        var oItemSearchItem = $(this).data('oitem-search-config');

        oItemSearchItem.searchContainerElement = this;
        oItemSearchItem.searchAreaElement = $('<div class="searchArea"></div>"');

        oItemSearchItem.searchContainerElement.append(oItemSearchItem.searchAreaElement);

        var containerFluid = $('<div class="container-fluid"></div>');
        $(oItemSearchItem.searchAreaElement).append(containerFluid);

        oItemSearchItem.searchCriteriaRowElement = $('<div class="row searchCriteriaRow"></div>');

        containerFluid.append(oItemSearchItem.searchCriteriaRowElement);

        var row = $('<div class="row"></div>');

        containerFluid.append(row);

        var col = $('<div class="col-sm-12"></div>');

        row.append(col);

        oItemSearchItem.panelBarElement = $('<ul class="searchPanelBar"></ul>');

        col.append(oItemSearchItem.panelBarElement);

        var li = $('<li class="k-state-active"></li>');

        oItemSearchItem.panelBarElement.append(li);

        var span = $('<span class="searchResultPanel"></span>');
        span.append($('<span>Result:&nbsp;</span>'));

        li.append(span);

        oItemSearchItem.searchCountElement = $('<span class="badge searchResultCount">0</span>')

        li.append(oItemSearchItem.searchCountElement);

        var div = $('<div class="gridParent"></div>');
        li.append(div);

        oItemSearchItem.resultToolbarElement = $('<div class="resultToolbar"></div>');

        div.append(oItemSearchItem.searchToolbarElement);

        oItemSearchItem.searchGrid = $('<div class="searchGrid"></div >');

        div.append(oItemSearchItem.searchGrid);

        oItemSearchItem.panelBarElement.kendoPanelBar({
            expandMode: "single"
        });

        oItemSearchItem.resultToolbarElement.kendoToolBar({
            items: [
                {
                    template: '<button class="searchApplyButton"><i class="fa fa-check-circle" aria-hidden="true"></i></button>'
                }
            ]
        });

        oItemSearchItem.resultToolbarElement.find('.searchApplyButton').kendoButton({
            enable: false,
            click: function (e) {
                var oItems = [];
                for (var i in oItemSearchItem.itemsToApply) {
                    var itemToApply = oItemSearchItem.itemsToApply[i];
                    var oItem = { GUID: itemToApply.IdItem };
                    oItems.push(oItem);
                }

                oItemSearchItem.configuration.moduleComHub.server.sendInterServiceMessage({
                    ChannelId: oItemSearchItem.configuration.channelAppliedObjects,
                    SenderId: oItemSearchItem.configuration.idEndpoint,
                    SessionId: oItemSearchItem.configuration.idSession,
                    UserId: oItemSearchItem.configuration.IdUser,
                    OItems: oItems
                });
            }
        });

        var initSearchItem = {
            id: "_" + guid(),
            add: true
        };
        $.post(oItemSearchItem.configuration.actionInitSearchItems, { idInstance: oItemSearchItem.configuration.idInstance })
            .done(function (response) {
                if (response.IsSuccessful) {
                    initSearchItem = response.Result;
                    oItemSearchItem.searchCriteriaRowElement.createSearchArea(true, initSearchItem.id);

                    $.post(oItemSearchItem.configuration.actionGetGridConfig)
                        .done(function (response) {
                            console.log(response);

                            $.extend(response.dataSource.transport.read, {
                                data: function () {
                                    return { idInstance: oItemSearchItem.configuration.idInstance };
                                }
                            })

                            //$.extend(response, {
                            //    filterable: {
                            //        mode: "row"
                            //    }
                            //});

                            var dataSource = new kendo.data.DataSource(response.dataSource);
                            response.dataSource = dataSource;
                            response.dataBound = function () {

                                oItemSearchItem.searchCountElement.text(oItemSearchItem.gridItem.dataSource.total());

                                $('.applyCellContent').on("change", function () {

                                    var dataItem = oItemSearchItem.gridItem.dataItem($(this).closest('tr'));
                                    dataItem.ApplyItem = $(this).is(':checked');
                                    if (dataItem.ApplyItem) {
                                        oItemSearchItem.itemsToApply.push(dataItem);
                                    } else {
                                        oItemSearchItem.itemsToApply = jQuery.grep(oItemSearchItem.itemsToApply, function (item) {
                                            return item.uid != dataItem.uid;
                                        });
                                    }
                                });

                            };

                            $.extend(response, {
                                change: oItemSearchItem.gridChangeHandler,
                                height: "500px"
                            });

                            if (response.rowTemplate != undefined) {
                                response.rowTemplate = kendo.template($("#" + response.rowTemplate).html());
                            }

                            oItemSearchItem.gridItem = oItemSearchItem.searchGrid.kendoGrid(response).data("kendoGrid");

                            oItemSearchItem.gridItem.wrapper.find(".k-pager")
                                .before('<a href="#" class="k-link prev">Prev</a>')
                                .after('<a href="#" class="k-link next">Next</a>')
                                .parent()
                                .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                                .delegate("a.prev", "click", function () {
                                    dataSource.page(dataSource.page() - 1);
                                })
                                .delegate("a.next", "click", function () {
                                    dataSource.page(dataSource.page() + 1);
                                })
                                .delegate(".pagesize input", "change", function () {
                                    dataSource.page(parseInt(this.value, 10));
                                })


                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                }
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
        

        
        

        
    };
})(jQuery);