﻿var pathBarItemCount = 0;

(function ($) {
    $.fn.createOTreeBar = function () {
        var elementId = this[0].id
        $("#" + elementId).jqxToolBar({
            width: "100%", height: 35, tools: '',
            initTools: function (type, index, tool, menuToolIninitialization) {
                if (type == "toggleButton") {
                    var icon = $("<div class='jqx-editor-toolbar-icon jqx-editor-toolbar-icon-" + theme + " buttonIcon'></div>");
                }
                switch (index) {
                    case 0:
                        var button = $("<div>Root</div>");
                        tool.append(button);
                        button.jqxButton({ height: 15 });
                        break;
                    case 2:
                        icon.addClass("jqx-editor-toolbar-icon-underline jqx-editor-toolbar-icon-underline-" + theme);
                        icon.attr("title", "Underline");
                        tool.append(icon);
                        break;
                }
            }

        });
        
            
    }

    $.fn.configureOTreePath = function (pathNodes) {
        var elementId = this[0].id

        for (var tix = pathBarItemCount - 1; tix >= 1; tix--) {
            $("#" + elementId).jqxToolBar('destroyTool', tix);
        }
        var font = $("#" + elementId).detectFont();

        if (pathBarItemCount == 0) {
            $("#" + elementId).jqxToolBar('addTool', 'input', 'last', true, function (type, tool, menuToolInitialization) {
                tool.jqxInput({ width: 130, placeHolder: "Enter search..." });
                tool.on("change", function () {
                    SendStringPropertyChangeWithValue("Text_Search", tool.val());
                });
            });
        }
        
        for (var ix in pathNodes) {
            var pathNode = pathNodes[ix];
            
            $("#" + elementId).jqxToolBar('addTool', 'button', 'last', true, function (type, tool, menuToolInitialization) {
                var myCanvas = document.createElement('canvas');
                //var myCanvas = document.getElementById('myCanvas')
                var width = measureText(myCanvas, pathNode.Name, font) * 1.8;
                if (menuToolInitialization == true) {
                    width = "100%";
                }
                tool.text(pathNode.Name);
                tool.id = pathNode.Id;
                tool.jqxButton({ width: width });
                tool.on("click", function (event) {
                    SendStringPropertyChangeWithValue("Text_SelectedNodeId", tool.id);
                });
            });

        }
        pathBarItemCount = pathNodes.length + 1;
    }

    //$.fn.createOntoTree = function (nodeUrl, requestUrl, requestParams) {
    //    var elementId = this[0].id
    //    var source =
    //        {
    //            datatype: "json",
    //            datafields: [
    //                { name: 'id' },
    //                { name: 'label' },
    //                { name: 'html' }
    //            ],
    //            id: 'id',
    //            url: requestUrl, data: requestParams
    //        };
    //    var dataAdapter = new $.jqx.dataAdapter(source);
    //    $("#" + elementId).jqxListBox({ source: dataAdapter, displayMember: "label", valueMember: "id", width: "100%", height: "600px" });
    //    $('#' + elementId).on('select', function (event) {
    //        var args = event.args;
    //        var item = $('#' + elementId).jqxListBox('getItem', args.index);
    //        if (item != null) {
    //            $.post("./ClassTree/SelectClass", { IdClass: item.value, SearchTerm: null, OnlyPathItems: true })
    //                .done(function (response) {
    //                    // stuff

    //                    console.log(response);
    //                    $('#list').createOntoTree(response.treeNodes, "@Url.Action("SelectClass")", { IdClass: null, SearchTerm: null, OnlyPathItems: true });
    //                })
    //                .fail(function (jqxhr, textStatus, error) {
    //                    // stuff
    //                });
    //            SendStringPropertyChangeWithValue("Text_SelectedNodeId", item.value);
    //        }
    //    });
        
    //}

    $.fn.createKendoTreeList = function (config) {
        var treeList = $(this);

        var dataSource = new kendo.data.TreeListDataSource(config.dataSource);

        treeList.kendoTreeList({
            dataSource: dataSource,
            width: config.width,
            height: config.height,
            filterable: config.filterable,
            sortable: config.sortable,
            columns: config.columns
        });
    }
})(jQuery);