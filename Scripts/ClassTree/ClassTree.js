﻿function clickAddClass(classButton) {
    var classTreeComponent = $(classButton.closest('.class-tree')).data('class-tree-component');
    var idClass = $(classButton).data('idclass');
}
function clickEditClass(classButton) {
    var classTreeComponent = $(classButton.closest('.class-tree')).data('class-tree-component');
    var idClass = $(classButton).data('idclass');
}
function clickApplyClass(classButton) {
    var classTreeComponent = $(classButton.closest('.class-tree')).data('class-tree-component');
    var idClass = $(classButton).data('idclass');
    classTreeComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
        ChannelId: classTreeComponent.configuration.viewModel.ChannelConfig.ChannelAppliedClass,
        SenderId: classTreeComponent.configuration.managerConfig.idEndpoint,
        UserId: classTreeComponent.configuration.viewModel.idUser,
        SessionId: classTreeComponent.configuration.managerConfig.idSession,
        OItems: [{ GUID: idClass }]
    });
}

(function ($) {
    $.fn.createClassTree = function (configuration) {
        var classTreeComponent = {
            idClass: null,
            searchTrem: null,
            elements: {
                classTree: null
            },
            configuration: configuration,
            handler: {
                createComponent: function (classTreeComponent) {
                    $(classTreeComponent.elements.classTree).openPageLoader();
                    $.post(classTreeComponent.configuration.viewModel.ActionConfig.GetClassPath, { IdClass: classTreeComponent.idClass, SearchTerm: classTreeComponent.searchTerm, OnlyPathItems: false, idInstance: classTreeComponent.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            // stuff
                            console.log(response);
                            var selectorPath = response.SelectorPath;
                            var element = getElementsBySelectorPath(selectorPath);
                            var classTreeComponent = $(element).find('.class-tree').data('class-tree-component');

                            var classTreeNavbar = classTreeComponent.elements.classTree.find('.classtree-navbar');
                            classTreeNavbar.html('');
                            var classTreeList = classTreeComponent.elements.classTree.find('.classtree-treelist');
                            classTreeList.html('');

                            var ol = $(classTreeComponent.templates.breadCrumbTemplate);
                            classTreeNavbar.append(ol)

                            var breadCrumbSearch = $(classTreeComponent.templates.breadCrumbSearchTemplate);
                            ol.append(breadCrumbSearch);
                            var title = "";
                            for (var ix in response.Result.SearchNodes) {
                                var searchNode = response.Result.SearchNodes[ix];
                                var template = classTreeComponent.templates.breadCrumbItemTemplate;
                                template = template.replace('@ID@', searchNode.GUID).replace('@CONTENT@', searchNode.Name);
                                var li = $(template);
                                if (ix == 0) {
                                    var html = classTreeComponent.templates.comButtonTemplateClass1.replace("@ID@", searchNode.GUID)
                                    li.append(html);
                                } else if (ix == response.Result.SearchNodes.length - 1) {
                                    var html = classTreeComponent.templates.comButtonTemplateClass1 + classTreeComponent.templates.comButtonTemplateClass2;
                                    html = html.replaceAll("@ID@", searchNode.GUID)
                                    li.append(html);
                                }
                                title += '\\' + searchNode.Name
                                ol.append(li);
                            }

                            if (classTreeComponent.configuration.setDocumentTitle) {
                                document.title = title;
                            }

                            breadCrumbSearch.find('.breadcrumb-search').change(function () {
                                console.log($(this).val());
                                classTreeComponent.idClass = null;
                                classTreeComponent.searchTerm = $(this).val();
                                classTreeComponent.handler.createComponent(classTreeComponent);

                            });

                            var dataSource = new kendo.data.DataSource({
                                data: response.Result.TreeNodes
                            });

                            $(classTreeList).kendoListView({
                                dataSource: dataSource,
                                template: classTreeComponent.templates.classListTemplate,
                                autoBind: false
                            });


                            dataSource.read(); // "read()" will fire the "change" event of the dataSource and the widget will be bound
                            $('.btn-link').on('click', function () {
                                console.log(this);
                                var classTreeComponent = $(this).closest('.class-tree').data('class-tree-component');
                                var idClass = $(this).attr('id');
                                var nameClass = $(this)[0].innerText;
                                classTreeComponent.idClass = idClass;
                                classTreeComponent.searchTerm = null;
                                classTreeComponent.handler.createComponent(classTreeComponent);

                                if (typeof classTreeComponent.configuration.events.selectedClass !== 'undefined' && classTreeComponent.configuration.events.selectedClass !== null) {
                                    classTreeComponent.configuration.events.selectedClass(idClass, nameClass, classTreeComponent);
                                } else {
                                    classTreeComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                        ChannelId: classTreeComponent.configuration.viewModel.ChannelConfig.ChannelSelectedClass,
                                        SenderId: classTreeComponent.configuration.managerConfig.idEndpoint,
                                        UserId: classTreeComponent.configuration.viewModel.idUser,
                                        SessionId: classTreeComponent.configuration.managerConfig.idSession,
                                        OItems: [{ GUID: classTreeComponent.idClass }]
                                    });
                                }

                            });

                            $(classTreeComponent.elements.classTree).closePageLoader();
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });

                    if (typeof classTreeComponent.configuration.events !== 'undefined' && classTreeComponent.configuration.events !== null &&
                        typeof classTreeComponent.configuration.events.createdComponent !== 'undefined' && classTreeComponent.configuration.events.createdComponent !== null) {
                        classTreeComponent.configuration.events.createdComponent(classTreeComponent);
                    }
                },
                validateReference: function () {

                }
            },
            templates: {
                classTreeTemplate: `
                    <div class="class-tree">
                        <div class="classtree-navbar"></div>
                        <div class="classtree-treelist"></div>
                    </div>
                `,
                breadCrumbTemplate: `
                    <ol class="breadcrumb"></ol>
                `,
                breadCrumbItemTemplate: `
                    <li class="breadcrumb-item"><button id="@ID@" class="btn-link">@CONTENT@</button></li>
                `,
                breadCrumbSearchTemplate: `
                    <li class="breadcrumb-item"><input type="text" class="form-control breadcrumb-search" placeholder="search-string"></li>
                `,
                classListTemplate: `
                    <div><button id='#:Id#' class='btn-link'>#:Name#</button>(#:ChildCount#)
                    </div>
                `,
                comButtonTemplateClass1: `
                  <button class='btn btn-primary btn-xs btn-add-class' onclick='clickAddClass(this)' data-idclass='@ID@'><i class='fa fa-plus-circle smallFont' aria-hidden='true'></i></button>  
                `,
                comButtonTemplateClass2: `
                    <button class='btn btn-primary btn-xs btn-edit-class' onclick='clickEditClass(this)' data-idclass='@ID@'><i class='fa fa-pencil-square-o smallFont' aria-hidden='true'></i></button>
                    <button class='btn btn-primary btn-xs btn-apply-class' onclick='clickApplyClass(this)' data-idclass='@ID@'><i class='fa fa-check-circle smallFont' aria-hidden='true'></i></button>
                `
            }
        }


        classTreeComponent.elements.classTree = $(classTreeComponent.templates.classTreeTemplate);
        $(classTreeComponent.elements.classTree).data('class-tree-component', classTreeComponent);
        $(this).append(classTreeComponent.elements.classTree);
        
        $.post(classTreeComponent.configuration.actionInit, { idInstance: classTreeComponent.configuration.IdInstance, selectorPath: classTreeComponent.configuration.selectorPath })
            .done(function (response) {
                // stuff
                console.log(response);
                if (response.IsSuccessful) {
                    var selectorPath = response.SelectorPath;
                    var element = getElementsBySelectorPath(selectorPath);
                    var classTreeComponent = $(element).find('.class-tree').data('class-tree-component');
                    classTreeComponent.configuration.viewModel = response.Result;
                    classTreeComponent.handler.createComponent(classTreeComponent);
                } else {

                }

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
        
    }
})(jQuery);