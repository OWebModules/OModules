﻿(function ($) {
    $.fn.createChecklist = function (configuration) {
        var checklistComponent = {
            configuration: configuration,
            data: {
                stateViewItems: []
            },
            elements: {
            },
            components: {
            },
            handler: {
                createComponent: function (checklistComponent) {
                    checklistComponent.handler.triggerEvents.createComponent(checklistComponent, false);
                    checklistComponent.elements.checklist = $(checklistComponent.templates.reportViewer);
                    $(checklistComponent.elements.checklistContainer).append(checklistComponent.elements.checklist);
                    checklistComponent.handler.triggerEvents.createComponent(checklistComponent, true);
                },
                validateReference: function (checklistComponent, oItem) {
                    $.post(checklistComponent.configuration.viewModel.ActionValidateReference, { idInstance: checklistComponent.configuration.viewModel.IdInstance, refItem: oItem })
                        .done(function (response) {
                            // stuff
                            console.log(response);
                            if (response.IsSuccessful) {
                                var selectorPath = response.SelectorPath;
                                var element = getElementsBySelectorPath(selectorPath);
                                var checklistComponent = $(element).data('checklist-component');
                                checklistComponent.data.checkList = response.Result;
                                checklistComponent.components.reportViewer = $('.report-viewer').createReportViewer({
                                    setDocumentTitle: true,
                                    selector: ".report-viewer",
                                    additionalFields: checklistComponent.data.checkList.AdditionalReportFields,
                                    actionGetData: checklistComponent.configuration.viewModel.ActionGetChecklistData,
                                    additionalIdViewModel: checklistComponent.configuration.viewModel.IdInstance,
                                    actionReportInit: checklistComponent.configuration.viewModel.ActionReportInit,
                                    managerConfig: checklistComponent.configuration.managerConfig,
                                    initCompleteHandler: function (reportViewerElement) {
                                        var element = $(reportViewerElement.elements.reportViewerContainerElement).closest('.checklist');
                                        var checklistComponent = $(element).data('checklist-component');
                                        reportViewerElement.handler.validateReference({
                                            GUID: checklistComponent.data.checkList.IdReport,
                                            Name: checklistComponent.data.checkList.NameReport
                                        });
                                    },
                                    events: {
                                        dataBound: function (reportViewerElement) {
                                            var checklistComponent = $(reportViewerElement.elements.reportViewerContainerElement).parent().data('checklist-component');
                                            changeValuesFromServerChildren(checklistComponent.data.stateViewItems, [reportViewerElement.configuration.selector]);
                                            $(reportViewerElement.elements.reportViewerContainerElement).find('.state-select').unbind('click');
                                            $(reportViewerElement.elements.reportViewerContainerElement).find('.state-select').on('click', function () {
                                                if ($(this).hasClass('openEntries')) {
                                                    $(this).toggleMode(true, null, checklistComponent.configuration.viewModel.IdInstance, function (result) {
                                                        changeValuesFromServerChildren(result.ViewItems, [reportViewerElement.configuration.selector]);
                                                        checklistComponent.data.stateViewItems = result.ViewItems;
                                                    });

                                                } else if ($(this).hasClass('doneEntries')) {
                                                    $(this).toggleMode(true, null, checklistComponent.configuration.viewModel.IdInstance, function (result) {
                                                        changeValuesFromServerChildren(result.ViewItems, [reportViewerElement.configuration.selector]);
                                                        checklistComponent.data.stateViewItems = result.ViewItems;
                                                    });

                                                } else if ($(this).hasClass('editEntries')) {
                                                    $(this).toggleMode(true, null, checklistComponent.configuration.viewModel.IdInstance, function (result) {
                                                        changeValuesFromServerChildren(result.ViewItems, [reportViewerElement.configuration.selector]);
                                                        checklistComponent.data.stateViewItems = result.ViewItems;
                                                    });

                                                } else if ($(this).hasClass('cancelledEntries')) {
                                                    $(this).toggleMode(true, null, checklistComponent.configuration.viewModel.IdInstance, function (result) {
                                                        changeValuesFromServerChildren(result.ViewItems, [reportViewerElement.configuration.selector]);
                                                        checklistComponent.data.stateViewItems = result.ViewItems;
                                                    });

                                                }

                                                reportViewerElement.handler.reloadReport();
                                            });

                                            $(reportViewerElement.elements.reportViewerContainerElement).find('.success-entry').unbind('click');
                                            $(reportViewerElement.elements.reportViewerContainerElement).find('.success-entry').on('click', function () {
                                                var row = $(this).closest("tr");
                                                var checklistComponent = $(row).closest('.checklist').data('checklist-component');
                                                var viewItemIdButton = checklistComponent.configuration.viewModel.doneEntries.ViewItemId;

                                                kendo.prompt("Please, enter a log-message:", "log-message").then(function (message) {
                                                    checklistComponent.handler.updateChecklistItem(row, checklistComponent, viewItemIdButton, message);    
                                                }, function () {
                                                    
                                                })
                                                

                                            });

                                            $(reportViewerElement.elements.reportViewerContainerElement).find('.edit-entry').unbind('click');
                                            $(reportViewerElement.elements.reportViewerContainerElement).find('.edit-entry').on('click', function () {
                                                var row = $(this).closest("tr");
                                                var checklistComponent = $(row).closest('.checklist').data('checklist-component');
                                                var viewItemIdButton = checklistComponent.configuration.viewModel.editEntries.ViewItemId;

                                                kendo.prompt("Please, enter a log-message:", "log-message").then(function (message) {
                                                    checklistComponent.handler.updateChecklistItem(row, checklistComponent, viewItemIdButton, message);
                                                }, function () {

                                                })
                                            });

                                            $(reportViewerElement.elements.reportViewerContainerElement).find('.error-entry').unbind('click');
                                            $(reportViewerElement.elements.reportViewerContainerElement).find('.error-entry').on('click', function () {
                                                var row = $(this).closest("tr");
                                                var checklistComponent = $(row).closest('.checklist').data('checklist-component');
                                                var viewItemIdButton = checklistComponent.configuration.viewModel.cancelledEntries.ViewItemId;

                                                kendo.prompt("Please, enter a log-message:", "log-message").then(function (message) {
                                                    checklistComponent.handler.updateChecklistItem(row, checklistComponent, viewItemIdButton, message);
                                                }, function () {

                                                })
                                            });
                                        }
                                    }
                                });
                                
                            } else {
                                $('.execution-log').outputMessage({
                                    error: response.Message,
                                    clearTimeout: {
                                        info: true,
                                        warning: true,
                                        error: false,
                                        critical: false
                                    },
                                    colors: {
                                        backColorInfo: "#eae8e8",
                                        backColorWarning: "black",
                                        backColorError: "black",
                                        backColorCritical: "red",
                                        foreColorInfo: "black",
                                        foreColorError: "red",
                                        foreColorWarning: "yellow",
                                        foreColorCritical: "black"
                                    }
                                });
                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            $('.execution-log').outputMessage({
                                error: error,
                                clearTimeout: {
                                    info: true,
                                    warning: true,
                                    error: false,
                                    critical: false
                                },
                                colors: {
                                    backColorInfo: "#eae8e8",
                                    backColorWarning: "black",
                                    backColorError: "black",
                                    backColorCritical: "red",
                                    foreColorInfo: "black",
                                    foreColorError: "red",
                                    foreColorWarning: "yellow",
                                    foreColorCritical: "black"
                                }
                            });
                        });
                },
                updateChecklistItem: function (row, checklistComponent, viewItemIdButton, message) {
                    var reportViewerComponent = checklistComponent.components.reportViewer;
                    var dataItem = reportViewerComponent.elements.gridItem.dataItem(row);
                    var idReference = dataItem[checklistComponent.data.checkList.NameReportField];

                    $('body').openPageLoader();
                    message = utf8_to_b64(message);
                    $.post(checklistComponent.configuration.viewModel.ActionUpdateChecklistEntryState, { idInstance: checklistComponent.configuration.viewModel.IdInstance, idReference: idReference, viewItemIdButton: viewItemIdButton, message: message })
                        .done(function (response) {
                            // stuff
                            
                            if (response.IsSuccessful) {
                                var element = getElementsBySelectorPath(response.SelectorPath);
                                var checklistComponent = $(element).data('checklist-component');
                                var reportViewerComponent = checklistComponent.components.reportViewer;
                                reportViewerComponent.elements.gridItem.removeRow(row);

                            } else {
                                $('.onto-status-bar').data('messageout-config').showError(response.Message, 5000);
                            }
                            $('body').closePageLoader();

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                triggerEvents: {
                    createComponent: function (checklistComponent, finishState) {
                        if (finishState == true) {
                            if (typeof checklistComponent.configuration.events !== 'undefined' && checklistComponent.configuration.events !== null &&
                                typeof checklistComponent.configuration.events.createdComponent !== 'undefined' && checklistComponent.configuration.events.createdComponent !== null) {
                                checklistComponent.configuration.events.createdComponent(checklistComponent);
                            }
                        } else {
                            if (typeof checklistComponent.configuration.events !== 'undefined' && checklistComponent.configuration.events !== null &&
                                typeof checklistComponent.configuration.events.createComponent !== 'undefined' && checklistComponent.configuration.events.createComponent !== null) {
                                checklistComponent.configuration.events.createComponent(checklistComponent);
                            }
                        }
                    },
                    validateReference: function (checklistComponent, finishState) {
                        if (finishState == true) {
                            if (typeof checklistComponent.configuration.events !== 'undefined' && checklistComponent.configuration.events !== null &&
                                typeof checklistComponent.configuration.events.validateReference !== 'undefined' && checklistComponent.configuration.events.validateReference !== null) {
                                checklistComponent.configuration.events.validateReference(checklistComponent, oItem);
                            }
                        } else {
                            if (typeof checklistComponent.configuration.events !== 'undefined' && checklistComponent.configuration.events !== null &&
                                typeof checklistComponent.configuration.events.validatedReference !== 'undefined' && checklistComponent.configuration.events.validatedReference !== null) {
                                checklistComponent.configuration.events.validatedReference(checklistComponent, oItem);
                            }
                        }
                    }
                }
            },
            templates: {
                reportViewer: `
                    <div class="report-viewer"></div>
                `
            }
        }

        checklistComponent.elements.checklistContainer = this;
        $(checklistComponent.elements.checklistContainer).addClass('checklist');
        $(checklistComponent.elements.checklistContainer).data('checklist-component', checklistComponent);

        $.post(checklistComponent.configuration.actionInit, { idInstance: checklistComponent.configuration.IdInstance, selectorPath: checklistComponent.configuration.selectorPath })
            .done(function (response) {
                // stuff
                console.log(response);
                if (response.IsSuccessful) {
                    var selectorPath = response.SelectorPath;
                    var element = getElementsBySelectorPath(selectorPath);
                    var checklistComponent = $(element).data('checklist-component');
                    checklistComponent.configuration.viewModel = response.Result;

                    checklistComponent.data.stateViewItems = $.grep(checklistComponent.configuration.viewModel.ViewItems, function (item) {
                        return item.ViewItemId == checklistComponent.configuration.viewModel.openEntries.ViewItemId ||
                            item.ViewItemId == checklistComponent.configuration.viewModel.doneEntries.ViewItemId ||
                            item.ViewItemId == checklistComponent.configuration.viewModel.editEntries.ViewItemId ||
                            item.ViewItemId == checklistComponent.configuration.viewModel.cancelledEntries.ViewItemId;
                    });
                    checklistComponent.handler.createComponent(checklistComponent);
                } else {

                }

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    }
})(jQuery);