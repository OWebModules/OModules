﻿var selectorToolBoxExecutionLog;
var toolBoxExecutionLogElement;
(function ($) {
    
    $.fn.createToolBoxExecutionLog = function (configuration) {
        selectorToolBoxExecutionLog = configuration.selector;
        $(this).data("execution-log-grid-config", {
            elements: {
                ExecutionLogContainerElement: null,
                gridContainer: null,
                grid: null,
                gridItem: null,
                dataItem: null,
                dataSource: null
            },
            configuration: configuration,
            handler: {
                reloadExecutionLog: function () {
                    $(selectorToolBoxExecutionLog).data("execution-log-grid-config").elements.gridItem.dataSource.read();
                },
                gridChangeHandler: function () {
                    var selectedRows = this.select();

                    if (selectedRows.length !== 1) {
                        return;
                    }

                    reportViewerElement.elements.dataItemRow = reportViewerElement.elements.gridItem.select();
                    reportViewerElement.elements.dataItem = reportViewerElement.elements.gridItem.dataItem(reportViewerElement.elements.gridItem.select());
                }
            }
        });

        $(this).addClass("execution-log-grid");

        toolBoxExecutionLogElement = $(this).data("execution-log-grid-config");

        toolBoxExecutionLogElement.elements.ExecutionLogContainerElement = this[0];
        $(toolBoxExecutionLogElement.elements.ExecutionLogContainerElement).html('');

        toolBoxExecutionLogElement.elements.gridContainer = $('<div class="execution-log-grid-container" style="height:100%"></div>');
        $(toolBoxExecutionLogElement.elements.ExecutionLogContainerElement).append(toolBoxExecutionLogElement.elements.gridContainer);
        toolBoxExecutionLogElement.elements.toolbar = $('<div class="execution-log-grid-toolbar"></div>');
        $(toolBoxExecutionLogElement.elements.gridContainer).append(toolBoxExecutionLogElement.elements.toolbar);
        toolBoxExecutionLogElement.elements.grid = $('<div class="execution-log-grid-grid"></div>');
        $(toolBoxExecutionLogElement.elements.gridContainer).append(toolBoxExecutionLogElement.elements.grid);


        $.post(toolBoxExecutionLogElement.configuration.actionExecutionLogInit)
            .done(function (response) {
                toolBoxExecutionLogElement.configuration.viewModel = response.Result;
                
                $.post(toolBoxExecutionLogElement.configuration.viewModel.ActionGetGridConfig)
                    .done(function (response) {
                        
                        $.extend(response.Result.dataSource.transport.read, {
                            data: function () {
                                return { idInstance: toolBoxExecutionLogElement.configuration.viewModel.IdInstance };
                            }
                        });
                        
                        $.extend(response.Result.dataSource, {
                            batch: true
                        });

                        $.extend(response.Result.dataSource.transport.read,
                            {
                                complete: function (e) {
                                    console.log(e);
                                }
                            }
                        );

                        var dataSource = new kendo.data.DataSource(response.Result.dataSource);
                        dataSource.bind("error", function (e) {
                            console.log(e);
                            toolBoxExecutionLogElement.elements.gridItem.cancelChanges();
                            var dialogElement = $('#dialog');
                            dialogElement.kendoDialog({
                                width: "300px",
                                height: "150px",
                                title: "Value",
                                closable: true,
                                modal: false,
                                content: "An error occured!",
                                actions: []
                            });

                            dialogElement.data("kendoDialog").open();
                        });
                        response.Result.dataSource = dataSource;
                        response.Result.dataBound = function () {


                        };
                        $.extend(response.Result, {
                            change: toolBoxExecutionLogElement.handler.gridChangeHandler,
                            filterable: {
                                mode: "row"
                            }
                        });
                        
                        toolBoxExecutionLogElement.elements.gridItem = $(toolBoxExecutionLogElement.elements.grid).kendoGrid(response.Result).data("kendoGrid");

                        toolBoxExecutionLogElement.handler.reloadExecutionLog();

                        if (toolBoxExecutionLogElement.configuration.callbackGridCreated !== undefined && toolBoxExecutionLogElement.configuration.callbackGridCreated !== null) {
                            toolBoxExecutionLogElement.configuration.callbackGridCreated(toolBoxExecutionLogElement);
                        }

                        $('body').closePageLoader();

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    };
})(jQuery);