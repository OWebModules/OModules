﻿var selectorToolBoxBatchGrid;
var toolBoxBatchGridElement;
(function ($) {
    
    $.fn.createToolBoxBatchGrid = function (configuration) {
        selectorToolBoxBatchGrid = configuration.selector;
        $(this).data("batch-grid-config", {
            elements: {
                batchGridContainerElement: null,
                gridContainer: null,
                grid: null,
                gridItem: null,
                dataItem: null,
                dataSource: null
            },
            configuration: configuration,
            handler: {
                reloadBatchGrid: function () {
                    $(selectorToolBoxBatchGrid).data("batch-grid-config").elements.gridItem.dataSource.read();
                },
                gridChangeHandler: function () {
                    var selectedRows = this.select();

                    if (selectedRows.length !== 1) {
                        return;
                    }

                    toolBoxBatchGridElement.elements.dataItemRow = toolBoxBatchGridElement.elements.gridItem.select();
                    toolBoxBatchGridElement.elements.dataItem = toolBoxBatchGridElement.elements.gridItem.dataItem(toolBoxBatchGridElement.elements.gridItem.select());

                    
                    toolBoxBatchGridElement.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: toolBoxBatchGridElement.configuration.viewModel.ChannelModuleSelected,
                        SenderId: toolBoxBatchGridElement.configuration.managerConfig.idEndpoint,
                        SessionId: toolBoxBatchGridElement.configuration.managerConfig.idSession,
                        ReceiverId: toolBoxBatchGridElement.configuration.viewModel.IdSender,
                        ViewId: toolBoxBatchGridElement.configuration.viewModel.View.IdView,
                        UserId: toolBoxBatchGridElement.configuration.viewModel.IdUser,
                        GenericParameterItems: [
                            toolBoxBatchGridElement.elements.dataItem
                        ]
                    });

                    if (toolBoxBatchGridElement.configuration.selectedFunction !== undefined && toolBoxBatchGridElement.configuration.selectedFunction !== null) {
                        toolBoxBatchGridElement.configuration.selectedFunction(toolBoxBatchGridElement.elements.dataItem);
                    }
                }
            }
        });

        $(this).addClass("batch-grid");

        toolBoxBatchGridElement = $(this).data("batch-grid-config");

        toolBoxBatchGridElement.elements.batchGridContainerElement = this[0];
        $(toolBoxBatchGridElement.elements.batchGridContainerElement).html('');

        toolBoxBatchGridElement.elements.gridContainer = $('<div class="batch-grid-container" style="height:100%"></div>');
        $(toolBoxBatchGridElement.elements.batchGridContainerElement).append(toolBoxBatchGridElement.elements.gridContainer);
        toolBoxBatchGridElement.elements.toolbar = $('<div class="batch-grid-toolbar"></div>');
        $(toolBoxBatchGridElement.elements.gridContainer).append(toolBoxBatchGridElement.elements.toolbar);
        toolBoxBatchGridElement.elements.grid = $('<div class="batch-grid-grid"></div>');
        $(toolBoxBatchGridElement.elements.gridContainer).append(toolBoxBatchGridElement.elements.grid);


        $.post(toolBoxBatchGridElement.configuration.actionBatchGridInit)
            .done(function (response) {
                toolBoxBatchGridElement.configuration.viewModel = response.Result;
                
                $.post(toolBoxBatchGridElement.configuration.viewModel.ActionGetGridConfig)
                    .done(function (response) {
                        
                        $.extend(response.Result.dataSource.transport.read, {
                            data: function () {
                                return { idInstance: toolBoxBatchGridElement.configuration.viewModel.IdInstance };
                            }
                        });
                        
                        $.extend(response.Result.dataSource, {
                            batch: true
                        });

                        $.extend(response.Result.dataSource.transport.read,
                            {
                                complete: function (e) {
                                    console.log(e);
                                }
                            }
                        );

                        var dataSource = new kendo.data.DataSource(response.Result.dataSource);
                        dataSource.bind("error", function (e) {
                            console.log(e);
                            toolBoxBatchGridElement.elements.gridItem.cancelChanges();
                            var dialogElement = $('#dialog');
                            dialogElement.kendoDialog({
                                width: "300px",
                                height: "150px",
                                title: "Value",
                                closable: true,
                                modal: false,
                                content: "An error occured!",
                                actions: []
                            });

                            dialogElement.data("kendoDialog").open();
                        });
                        response.Result.dataSource = dataSource;
                        response.Result.dataBound = function () {


                        };
                        $.extend(response.Result, {
                            change: toolBoxBatchGridElement.handler.gridChangeHandler,
                            filterable: {
                                mode: "row"
                            }
                        });
                        
                        toolBoxBatchGridElement.elements.gridItem = $(toolBoxBatchGridElement.elements.grid).kendoGrid(response.Result).data("kendoGrid");

                        toolBoxBatchGridElement.handler.reloadBatchGrid();

                        if (toolBoxBatchGridElement.configuration.callbackGridCreated !== undefined && toolBoxBatchGridElement.configuration.callbackGridCreated !== null) {
                            toolBoxBatchGridElement.configuration.callbackGridCreated(toolBoxBatchGridElement.elements.gridItem);
                        }

                        $('body').closePageLoader();

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    };
})(jQuery);