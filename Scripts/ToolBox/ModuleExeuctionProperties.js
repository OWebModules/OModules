﻿var selectorModuleExecutionProperties;
var toolBoxModuleExecutionPropertiesElement;
(function ($) {
    $.fn.createModuleExecutionProperties = function (configuration) {
        selectorModuleExecutionProperties = configuration.selector;
        $(this).data("module-execution-properties-config", {
            elements: {
                moduleExecutionPropertiesContainerElement: null,
                moduleExecutionPropertiesElement: null,
                toolBarElement: null,
                toolBar: null,
                dropDown: null
            },
            idModule: null,
            idFunction: null,
            configuration: configuration,
            handler: {
                viewModelInitialized: function () {
                    createStatusBar(toolBoxModuleExecutionPropertiesElement.configuration.viewModel.PageInitState);
                    var configurationJsonEditor = {
                        selector: ".json-editor",
                        actionJsonEditorInit: toolBoxModuleExecutionPropertiesElement.configuration.viewModel.ActionJsonEditorInit,
                        managerConfig: toolBoxModuleExecutionPropertiesElement.configuration.managerConfig,
                        initCompleteHandler: function (jsonEditorItem) {
                            toolBoxModuleExecutionPropertiesElement.elements.jsonEditorItem = jsonEditorItem;
                            toolBoxModuleExecutionPropertiesElement.elements.jsonEditorItem.handler.clearJsonEditor();
                        }
                    };
                    $(toolBoxModuleExecutionPropertiesElement.elements.moduleExecutionPropertiesContainerElement).find('.json-editor').createToolBoxJsonEditor(configurationJsonEditor);

                    $.post(toolBoxModuleExecutionPropertiesElement.configuration.viewModel.ActionGetDropDownConfig)
                        .done(function (response) {
                            $.extend(response.dataSource.transport.read, {
                                data: function () {
                                    return { idModule: toolBoxModuleExecutionPropertiesElement.idModule, idFunction: toolBoxModuleExecutionPropertiesElement.idFunction, idInstance: toolBoxModuleExecutionPropertiesElement.configuration.viewModel.IdInstance };
                                }
                            });

                            response.dataBound = function () {

                            };

                            response.change = function (e) {
                                toolBoxModuleExecutionPropertiesElement.elements.jsonEditorItem.handler.clearJsonEditor();
                                var idModuleConfig = this.value();
                                if (idModuleConfig === "NewConfiguration") {
                                    changeValue(toolBoxModuleExecutionPropertiesElement.configuration.viewModel.saveConfiguration, "Enable", true, true, function (response) {
                                        changeValuesFromServerChildren(response.ViewItems, [ selectorModuleExecutionProperties ]);
                                    }, toolBoxModuleExecutionPropertiesElement.configuration.viewModel.IdInstance);

                                    changeValue(toolBoxModuleExecutionPropertiesElement.configuration.viewModel.executeConfiguration, "Enable", false, true, function (response) {
                                        changeValuesFromServerChildren(response.ViewItems, [ selectorModuleExecutionProperties ]);
                                    }, toolBoxModuleExecutionPropertiesElement.configuration.viewModel.IdInstance);

                                    changeValue(toolBoxModuleExecutionPropertiesElement.configuration.viewModel.deleteConfiguration, "Enable", false, true, function (response) {
                                        changeValuesFromServerChildren(response.ViewItems, [ selectorModuleExecutionProperties ]);
                                    }, toolBoxModuleExecutionPropertiesElement.configuration.viewModel.IdInstance);
                                }
                                else {
                                    $.post(toolBoxModuleExecutionPropertiesElement.configuration.viewModel.ActionMetaUrl, { idModuleConfiguration: idModuleConfig, idInstance: toolBoxModuleExecutionPropertiesElement.configuration.viewModel.IdInstance })
                                        .done(function (response) {
                                            

                                        })
                                        .fail(function (jqxhr, textStatus, error) {
                                            // stuff
                                        });
                                    

                                    changeValue(toolBoxModuleExecutionPropertiesElement.configuration.viewModel.saveConfiguration, "Enable", false, true, function (response) {
                                        changeValuesFromServerChildren(response.ViewItems, [ selectorModuleExecutionProperties ]);
                                    }, toolBoxModuleExecutionPropertiesElement.configuration.viewModel.IdInstance);

                                    changeValue(toolBoxModuleExecutionPropertiesElement.configuration.viewModel.executeConfiguration, "Enable", true, true, function (response) {
                                        changeValuesFromServerChildren(response.ViewItems, [ selectorModuleExecutionProperties ]);
                                    }, toolBoxModuleExecutionPropertiesElement.configuration.viewModel.IdInstance);

                                    changeValue(toolBoxModuleExecutionPropertiesElement.configuration.viewModel.deleteConfiguration, "Enable", true, true, function (response) {
                                        changeValuesFromServerChildren(response.ViewItems, [ selectorModuleExecutionProperties ]);
                                    }, toolBoxModuleExecutionPropertiesElement.configuration.viewModel.IdInstance);


                                    var oItem = {
                                        GUID: idModuleConfig
                                    };


                                    toolBoxModuleExecutionPropertiesElement.elements.jsonEditorItem.handler.validateReference(oItem);
                                }
                                
                            };

                            $(toolBoxModuleExecutionPropertiesElement.elements.toolBarElement).find(".configurationDropdown").kendoDropDownList(response);

                            toolBoxModuleExecutionPropertiesElement.configuration.viewModel.ActionMetaUrl = response.MetaUrl;

                            toolBoxModuleExecutionPropertiesElement.elements.dropDown = $($(toolBoxModuleExecutionPropertiesElement.elements.toolBarElement).find(".configurationDropdown")[1]).data("kendoDropDownList");

                            if (toolBoxModuleExecutionPropertiesElement.configuration.initCompleteHandler !== undefined && toolBoxModuleExecutionPropertiesElement.configuration.initCompleteHandler !== null) {
                                toolBoxModuleExecutionPropertiesElement.configuration.initCompleteHandler(toolBoxModuleExecutionPropertiesElement);
                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                    
                },
                selectedFunction: function (functionItem) {
                    toolBoxModuleExecutionPropertiesElement.elements.jsonEditorItem.handler.clearJsonEditor();
                    toolBoxModuleExecutionPropertiesElement.idModule = functionItem.Id;
                    toolBoxModuleExecutionPropertiesElement.idFunction = functionItem.IdFunction;
                    toolBoxModuleExecutionPropertiesElement.elements.dropDown.dataSource.read();
                    changeValue(toolBoxModuleExecutionPropertiesElement.configuration.viewModel.configurationDropdown, "Enable", true, true, function (response) {
                        changeValuesFromServerChildren(response.ViewItems, [ selectorModuleExecutionProperties ]);
                    }, toolBoxModuleExecutionPropertiesElement.configuration.viewModel.IdInstance);
                }
            }
            
        });

        $(this).addClass("module-execution-properties-container");

        toolBoxModuleExecutionPropertiesElement = $(this).data("module-execution-properties-config");

        toolBoxModuleExecutionPropertiesElement.elements.moduleExecutionPropertiesContainerElement = this[0];
        $(toolBoxModuleExecutionPropertiesElement.elements.moduleExecutionPropertiesContainerElement).html('');

        toolBoxModuleExecutionPropertiesElement.elements.toolBarElement = $('<div class="tool-bar"></div>');

        $(toolBoxModuleExecutionPropertiesElement.elements.moduleExecutionPropertiesContainerElement).append(toolBoxModuleExecutionPropertiesElement.elements.toolBarElement);

        $(toolBoxModuleExecutionPropertiesElement.elements.toolBarElement).kendoToolBar({
            items: [
                {
                    template: '<label>Configuration</label>'
                },
                {
                    template: '<input class="configurationDropdown" style="width: 250px;" disabled/>'

                },
                {
                    type: "separator"
                },
                {
                    template: '<button class="k-button saveConfiguration" disabled><i class="fa fa-file-o" aria-hidden="true"></i></button>'

                },
                {
                    template: '<button class="k-button deleteConfiguration" disabled><i class="fa fa-trash" aria-hidden="true"></i></button>'

                },
                {
                    template: '<button class="k-button executeConfiguration" disabled><i class="fa fa-play" aria-hidden="true"></i></button>'

                }
            ]
        
        });

        toolBoxModuleExecutionPropertiesElement.elements.jsonEditorElement = $('<div class="json-editor"></div>');
        $(toolBoxModuleExecutionPropertiesElement.elements.moduleExecutionPropertiesContainerElement).append(toolBoxModuleExecutionPropertiesElement.elements.jsonEditorElement);

        toolBoxModuleExecutionPropertiesElement.elements.toolBar = $(toolBoxModuleExecutionPropertiesElement.elements.toolBarElement).data('kendoToolBar');
        $(toolBoxModuleExecutionPropertiesElement.elements.toolBarElement).find('.k-button').kendoButton({
            click: function (e) {
                console.log(e.sender);
                var configItem = toolBoxJsonEditorElement.configuration.viewModel.editItem;
                if ($(e.sender.element).hasClass("executeConfiguration")) {
                    $.post(toolBoxModuleExecutionPropertiesElement.configuration.viewModel.ActionExecute, { idConfiguration: configItem.Item.ConfigurationId, idInstance: toolBoxModuleExecutionPropertiesElement.configuration.viewModel.IdInstance })
                        .done(function (response) {
                           
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                }
            },
            enable: false
        });

        if (toolBoxModuleExecutionPropertiesElement.configuration.viewModel !== undefined && toolBoxModuleExecutionPropertiesElement.configuration.viewModel !== null) {
            toolBoxModuleExecutionPropertiesElement.handler.viewModelInitialized();
        }
        else {
            $.post(toolBoxModuleExecutionPropertiesElement.configuration.actionModuleExecutionPropertiesInit, { idParentInstance: toolBoxModuleExecutionPropertiesElement.configuration.idParentInstance})
                .done(function (response) {
                    toolBoxModuleExecutionPropertiesElement.configuration.viewModel = response.Result;
                    if (toolBoxModuleExecutionPropertiesElement.handler.viewModelInitialized !== undefined && toolBoxModuleExecutionPropertiesElement.handler.viewModelInitialized !== null) {
                        toolBoxModuleExecutionPropertiesElement.handler.viewModelInitialized();
                    }

                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        }
    };
})(jQuery);