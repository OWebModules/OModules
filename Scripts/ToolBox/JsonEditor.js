﻿var selectorToolBoxJsonEditor;
var toolBoxJsonEditorElement;
(function ($) {
    
    $.fn.createToolBoxJsonEditor = function (configuration) {
        selectorToolBoxJsonEditor = configuration.selector;
        $(this).data("json-editor-config", {
            elements: {
                jsonEditorContainerElement: null,
                jsonEditorElement: null
            },
            configuration: configuration,
            timers: {
            },
            handler: {
                changeJSON: function (json) {
                    toolBoxJsonEditorElement.configuration.viewModel.json = json;
                    if (toolBoxJsonEditorElement.timers.timer !== undefined && toolBoxJsonEditorElement.timers.timer !== null) {
                        clearTimeout(toolBoxJsonEditorElement.timers.timer);
                    }
                    toolBoxJsonEditorElement.timers.timer = setTimeout(function () {
                        $(toolBoxJsonEditorElement.elements.jsonEditorContainerElement).openPageLoader();
                        changeValue(toolBoxJsonEditorElement.configuration.viewModel.editItemRaw, "Content", utf8_to_b64(JSON.stringify(json)), true, function (response) {
                            if (response.IsSuccessful) {
                                $('.onto-status-bar').data('messageout-config').showMessage(response.ResultMessage, 2000);
                            } else {
                                $('.onto-status-bar').data('messageout-config').showError(response.ResultMessage, 2000);
                            }
                            $(toolBoxJsonEditorElement.elements.jsonEditorContainerElement).closePageLoader();
                        }, toolBoxJsonEditorElement.configuration.viewModel.IdInstance);
                    }, 500);
                },
                onEditable: function () {
                    return { field: false, value: true };
                },
                onCreateMenu: function (items, node) {
                    
                    return [];
                },
                clearJsonEditor: function () {
                    $(toolBoxJsonEditorElement.elements.jsonEditorElement[0]).html('');
                },
                validateReference: function (refItem, esIndexItemRequest) {
                    $.post(toolBoxJsonEditorElement.configuration.viewModel.ActionValidateReference, { refItem: refItem, getEsIndexItemRequest: esIndexItemRequest, idInstance: toolBoxJsonEditorElement.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            console.log("connected", response);
                            
                            changeValuesFromServerChildren(response.ViewItems, [ selectorToolBoxJsonEditor ]);

                            toolBoxJsonEditorElement.configuration.viewModel.editItemRaw = getViewItem(response.ViewItems, "editItem");
                            toolBoxJsonEditorElement.configuration.viewModel.editItem = {
                                Id: getViewItemValue(toolBoxJsonEditorElement.configuration.viewModel.editItemRaw, "Id", ""),
                                Item: JSON.parse(b64_to_utf8(getViewItemValue(toolBoxJsonEditorElement.configuration.viewModel.editItemRaw, "Content", "")))
                            };

                            if (toolBoxJsonEditorElement.configuration.viewModel.schemaItemRaw !== undefined && toolBoxJsonEditorElement.configuration.viewModel.schemaItemRaw !== null) {
                                toolBoxJsonEditorElement.configuration.viewModel.schemaItemRaw = getViewItem(response.ViewItems, "schemaItem");
                                toolBoxJsonEditorElement.configuration.viewModel.schemaItem = {
                                    Id: getViewItemValue(toolBoxJsonEditorElement.configuration.viewModel.schemaItemRaw, "Id", ""),
                                    Item: JSON.parse(b64_to_utf8(getViewItemValue(toolBoxJsonEditorElement.configuration.viewModel.schemaItemRaw, "Content", "")))
                                };
                            }

                            // create the editor
                            toolBoxJsonEditorElement.editorOptions = {
                                mode: 'tree',
                                modes: ['tree', 'preview'], // allowed modes
                                name: "jsonContent",
                                sortObjectKeys: true,
                                schema: toolBoxJsonEditorElement.configuration.viewModel.schemaItem.Item,
                                onEditable: toolBoxJsonEditorElement.handler.onEditable,
                                onCreateMenu: toolBoxJsonEditorElement.handler.onCreateMenu,
                                onEvent: function (item, event) {
                                    console.log(item);
                                    console.log(event);
                                    if (event.type === 'keydown') {
                                        event.key = '*';
                                    }
                                }
                            };
                            $(toolBoxJsonEditorElement.elements.jsonEditorElement[0]).html('');
                            toolBoxJsonEditorElement.jsonEditor = new JSONEditor(toolBoxJsonEditorElement.elements.jsonEditorElement[0], toolBoxJsonEditorElement.editorOptions);

                            // get json
                            toolBoxJsonEditorElement.jsonEditor.set(toolBoxJsonEditorElement.configuration.viewModel.editItem.Item);
                            toolBoxJsonEditorElement.jsonEditor.options.onChangeJSON = toolBoxJsonEditorElement.handler.changeJSON;
                            $('body').closePageLoader();
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                            $('body').closePageLoader();
                        });
                },
                viewModelInitialized: function () {
                    createStatusBar(toolBoxJsonEditorElement.configuration.viewModel.PageInitState);
                    if (toolBoxJsonEditorElement.configuration.initCompleteHandler !== undefined && toolBoxJsonEditorElement.configuration.initCompleteHandler !== null) {
                        toolBoxJsonEditorElement.configuration.initCompleteHandler(toolBoxJsonEditorElement);
                    }
                }
            }
        });

        $(this).addClass("json-editor");

        toolBoxJsonEditorElement = $(this).data("json-editor-config");

        toolBoxJsonEditorElement.elements.jsonEditorContainerElement = this[0];
        $(toolBoxJsonEditorElement.elements.jsonEditorContainerElement).html('');
        toolBoxJsonEditorElement.elements.jsonEditorElement = $('<form><div class="json-editor-element"></div></form>');
        $(toolBoxJsonEditorElement.elements.jsonEditorContainerElement).append(toolBoxJsonEditorElement.elements.jsonEditorElement);

        if (toolBoxJsonEditorElement.configuration.viewModel !== undefined && toolBoxJsonEditorElement.configuration.viewModel !== null) {
            if (toolBoxJsonEditorElement.handler.viewModelInitialized !== undefined && toolBoxJsonEditorElement.handler.viewModelInitialized !== null) {
                toolBoxJsonEditorElement.handler.viewModelInitialized();
            }
        }
        else {
            $.post(toolBoxJsonEditorElement.configuration.actionJsonEditorInit)
                .done(function (response) {
                    toolBoxJsonEditorElement.configuration.viewModel = response.Result;
                    if (toolBoxJsonEditorElement.handler.viewModelInitialized !== undefined && toolBoxJsonEditorElement.handler.viewModelInitialized !== null) {
                        toolBoxJsonEditorElement.handler.viewModelInitialized();
                    }

                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        }
    };
})(jQuery);