﻿(function ($) {
    $.fn.createClipboard = function (configuration) {
        var clipboardComponent = {
            elements: {
            },
            components: {
            },
            data: {
                itemsPreApply: [],
                addApplyItem: function (clipboardComponent, item) {
                    clipboardComponent.data.itemsPreApply.push(item);
                    clipboardComponent.handler.checkViewState(clipboardComponent);
                },
                removeApplyItem: function (clipboardComponent, item) {
                    clipboardComponent.data.itemsPreApply = jQuery.grep(clipboardComponent.data.itemsPreApply, function (itemInArray) {
                        return itemInArray.IdInstance != item.IdInstance
                    });

                    clipboardComponent.handler.checkViewState(clipboardComponent);
                }
            },
            configuration: configuration,
            stateMachine: {
                state: 0,
                states: {
                    createComponent: 2,
                    createdComponent: 4,
                    createGrid: 8,
                    createdGrid: 16,
                    validateReference: 32,
                    validatedReference: 64,
                    dataBound: 128
                },
                hasState: function (component, state) {
                    if ((component.stateMachine.state & state) == state) {
                        return true;
                    }
                    else {
                        return false;
                    }
                },
                addState: function (component, state) {
                    component.stateMachine.state += state;
                    component.stateMachine.checkState(component);
                },
                removeState: function (component, state) {
                    component.stateMachine.state -= state;
                    component.stateMachine.checkState(component);
                },
                checkState: function (component) {
                    if (component.stateMachine.hasState(component, component.stateMachine.states.createComponent) &&
                        !component.stateMachine.hasState(component, component.stateMachine.states.createdComponent)) {

                        $('body').openPageLoader();


                    }
                    if (component.stateMachine.hasState(component, component.stateMachine.states.createComponent) &&
                        component.stateMachine.hasState(component, component.stateMachine.states.createdComponent)) {

                        component.stateMachine.removeState(component, component.stateMachine.states.createComponent);
                        $.post(component.configuration.viewModel.ActionGridCreated, { idInstance: component.configuration.viewModel.IdInstance })
                            .done(function (response) {

                                var element = getElementsBySelectorPath(response.SelectorPath);
                                changeValuesFromServerChildren(response.ViewItems, response.SelectorPath);
                                var clipboardComponent = $(element).data('ontology-clipboard');

                                if (typeof clipboardComponent.configuration.events !== 'undefined' && clipboardComponent.configuration.events !== null &&
                                    typeof clipboardComponent.configuration.events.createdComponent !== 'undefined' && clipboardComponent.configuration.events.createdComponent !== null) {
                                    clipboardComponent.configuration.events.createdComponent(clipboardComponent);
                                }
                                $('body').closePageLoader();

                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });
                        

                        
                    }

                    if (component.stateMachine.hasState(component, component.stateMachine.states.validateReference) &&
                        !component.stateMachine.hasState(component, component.stateMachine.states.validatedReference)) {

                        $('body').openPageLoader();
                    }

                    if (component.stateMachine.hasState(component, component.stateMachine.states.validateReference) &&
                        component.stateMachine.hasState(component, component.stateMachine.states.validatedReference)) {
                        component.stateMachine.removeState(component, component.stateMachine.states.validateReference);
                        $('body').closePageLoader();
                    }

                }
            },
            handler: {
                createComponent: function (clipboardComponent) {
                    clipboardComponent.stateMachine.addState(clipboardComponent, clipboardComponent.stateMachine.states.createComponent);
                    $(clipboardComponent.elements.parent).addClass('ontology-clipboard');
                    $(clipboardComponent.elements.parent).html('');
                    var toolbar = $(clipboardComponent.templates.clipboardToolbarTemplate);
                    clipboardComponent.elements.toolbar = toolbar[0];
                    $(clipboardComponent.elements.parent).append(toolbar);

                    var gridContainer = $(clipboardComponent.templates.clopboardGridTemplate);
                    clipboardComponent.elements.gridContainer = gridContainer[0];
                    $(clipboardComponent.elements.parent).append(gridContainer);

                    var toolbarClass = clipboardComponent.configuration.viewModel.toolbar.ViewItemId;
                    var listenClass = clipboardComponent.configuration.viewModel.isListen.ViewItemId;
                    var applyClass = clipboardComponent.configuration.viewModel.apply.ViewItemId;
                    var removeClass = clipboardComponent.configuration.viewModel.removeItems.ViewItemId;
                    var clearClipbaordClass = clipboardComponent.configuration.viewModel.clearClipboard.ViewItemId;

                    clipboardComponent.elements.toolbar = $(clipboardComponent.elements.parent).find("." + toolbarClass);
                    $(clipboardComponent.elements.toolbar).kendoToolBar({
                        items: [
                            {
                                template: '<button class="k-button ' + listenClass + '"><i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></button>',

                            },
                            {
                                template: '<button class="k-button ' + applyClass + '"><i class="fa fa-check-circle-o" aria-hidden="true"></i></button>',

                            },
                            {
                                template: '<button class="k-button ' + removeClass + '"><i class="fa fa-trash" aria-hidden="true"></i></button>',
                            },
                            {
                                template: '<button class="k-button ' + clearClipbaordClass + '"><i class="fa fa-eraser" aria-hidden="true"></i></button>',
                            }
                        ]
                    });

                    $(clipboardComponent.elements.parent).find('.k-button').kendoButton({
                        click: clipboardComponent.handler.clickedButton
                    });

                    $.post(clipboardComponent.configuration.viewModel.ActionGetGridConfig, { idInstance: clipboardComponent.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            var element = getElementsBySelectorPath(response.SelectorPath);
                            var clipboardComponent = $(element).data('ontology-clipboard');
                            $.extend(response.Result.dataSource.transport.read, {
                                data: function () {
                                    return { idInstance: clipboardComponent.configuration.viewModel.IdInstance };
                                }
                            })

                            var dataSource = new kendo.data.DataSource(response.Result.dataSource);
                            response.Result.dataSource = dataSource;
                            response.Result.dataBound = function () {

                                var clipboardComponent = $(this.element).parent().parent().data('ontology-clipboard');
                                $(clipboardComponent.elements.grid).find('.chkbxApply').change(function () {
                                    var row = $(this).closest('tr');
                                    var clipboardComponent = $(this).closest('.grid').parent().parent().data('ontology-clipboard');
                                    var item = clipboardComponent.components.gridItem.dataItem(row);
                                    var applied = $(this).prop("checked");
                                    item.Apply = applied;
                                    if (item.Apply) {
                                        clipboardComponent.data.addApplyItem(clipboardComponent, item);
                                    } else {
                                        clipboardComponent.data.removeApplyItem(clipboardComponent,item);
                                    }

                                    console.log(item);
                                });
                                clipboardComponent.handler.gridBound(clipboardComponent);

                            };
                            clipboardComponent.elements.grid = $(element).find('.' + clipboardComponent.configuration.viewModel.grid.ViewItemId);
                            clipboardComponent.components.gridItem = $(clipboardComponent.elements.grid).kendoGrid(response.Result).data("kendoGrid");

                            clipboardComponent.components.gridItem.wrapper.find(".k-pager")
                                .before('<a href="#" class="k-link prev">Prev</a>')
                                .after('<a href="#" class="k-link next">Next</a>')
                                .parent()
                                .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                                .delegate("a.prev", "click", function () {
                                    dataSource.page(dataSource.page() - 1);
                                })
                                .delegate("a.next", "click", function () {
                                    dataSource.page(dataSource.page() + 1);
                                })
                                .delegate(".pagesize input", "change", function () {
                                    dataSource.page(parseInt(this.value, 10));
                                });

                            clipboardComponent.elements.grid.find('.header-checkbox').change(function (ev) {
                                var checked = ev.target.checked;
                                clipboardComponent.elements.grid.find('.chkbxApply').each(function (idx, item) {
                                    $(item).click();
                                });
                            });

                            clipboardComponent.stateMachine.addState(clipboardComponent, clipboardComponent.stateMachine.states.createdComponent);

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });

                    changeValuesFromServerChildren(clipboardComponent.configuration.viewModel.ViewItems, clipboardComponent.configuration.viewModel.SelectorPath);

                   
                },
                validateReference: function (clipboardComponent, oItem) {
                    clipboardComponent.data.itemsPreApply = [];
                    clipboardComponent.data.refItem = oItem;
                    clipboardComponent.stateMachine.addState(clipboardComponent, clipboardComponent.stateMachine.states.validateReference);

                    $.post(clipboardComponent.configuration.viewModel.ActionValidateReference, { idInstance: clipboardComponent.configuration.viewModel.IdInstance, refItem: clipboardComponent.data.refItem })
                        .done(function (response) {
                            // stuff
                            console.log(response);
                            if (response.IsSuccessful) {
                                var selectorPath = response.SelectorPath;
                                var element = getElementsBySelectorPath(selectorPath);
                                var clipboardComponent = $(element).data('ontology-clipboard');
                                clipboardComponent.data.itemsExist = response.Result;
                                clipboardComponent.handler.reloadGrid(clipboardComponent);

                                clipboardComponent.stateMachine.addState(clipboardComponent, clipboardComponent.stateMachine.states.validatedReference);
                                if (typeof clipboardComponent.configuration.events !== 'undefined' && clipboardComponent.configuration.events !== null &&
                                    typeof clipboardComponent.configuration.events.validatedReference !== 'undefined' && clipboardComponent.configuration.events.validatedReference !== null) {
                                    clipboardComponent.configuration.events.validatedReference(clipboardComponent);
                                }
                            } else {

                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });

                    
                },
                init: function (clipboardComponent) {
                    $.post(clipboardComponent.configuration.actionInit, { idInstance: clipboardComponent.configuration.IdInstance, selectorPath: clipboardComponent.configuration.selectorPath })
                        .done(function (response) {
                            // stuff
                            console.log(response);
                            if (response.IsSuccessful) {
                                var selectorPath = response.SelectorPath;
                                var element = getElementsBySelectorPath(selectorPath);
                                var clipboardComponent = $(element).data('ontology-clipboard');
                                clipboardComponent.configuration.viewModel = response.Result;
                                clipboardComponent.handler.createComponent(clipboardComponent);
                            } else {

                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                reloadGrid: function (clipboardComponent) {
                    clipboardComponent.components.gridItem.dataSource.read();
                },
                checkViewState: function (clipboardComponent) {
                    $(clipboardComponent.elements.toolbar).find("." + clipboardComponent.configuration.viewModel.apply.ViewItemId).setEnabled(clipboardComponent.data.itemsPreApply.length > 0, true);
                    $(clipboardComponent.elements.toolbar).find("." + clipboardComponent.configuration.viewModel.removeItems.ViewItemId).setEnabled(clipboardComponent.data.itemsPreApply.length > 0, true);

                },
                clickedButton: function (e) {
                    var button = $(e.event.target).closest(".k-button");
                    var clipboardComponent = $(e.sender.element).closest('.ontology-clipboard').data('ontology-clipboard');
                    if (button.hasClass(clipboardComponent.configuration.viewModel.isListen.ViewItemId)) {
                        $(button).toggleMode(true);
                    } else if (button.hasClass(clipboardComponent.configuration.viewModel.apply.ViewItemId)) {

                        var items = clipboardComponent.data.itemsPreApply;
                        var oItems = [];
                        for (var i = 0; i < items.length; i++) {
                            var item = items[i];

                            var oItem = {
                                GUID: item.Id,
                                Name: item.Name,
                                GUID_Parent: item.IdParent,
                                Type: item.Type
                            };
                            oItems.push(oItem);
                        }
                        if (typeof clipboardComponent.configuration.events !== 'undefined' && clipboardComponent.configuration.events !== null &&
                            typeof clipboardComponent.configuration.events.applyItems !== 'undefined' && clipboardComponent.configuration.events.applyItems !== null) {
                            clipboardComponent.configuration.events.applyItems(clipboardComponent, oItems);
                        }
                        else {
                            clipboardComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                ChannelId: clipboardComponent.configuration.viewModel.ChannelAppliedObjects,
                                SenderId: clipboardComponent.configuration.managerConfig.idEndpoint,
                                UserId: clipboardComponent.configuration.viewModel.idUser,
                                SessionId: clipboardComponent.configuration.managerConfig.idSession,
                                OItems: oItems
                            });
                        }
                    } else if (button.hasClass(clipboardComponent.configuration.viewModel.removeItems.ViewItemId)) {

                    } else if (button.hasClass(clipboardComponent.configuration.viewModel.clearClipboard.ViewItemId)) {

                        $.post(clipboardComponent.configuration.viewModel.ActionClearClipboard, { idInstance: clipboardComponent.configuration.viewModel.IdInstance })
                            .done(function (response) {
                                var selectorPath = response.SelectorPath;
                                var element = getElementsBySelectorPath(selectorPath);
                                var clipboardComponent = $(element).data('ontology-clipboard');
                                console.log("connected", response);
                                clipboardComponent.handler.reloadGrid(clipboardComponent);
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                                $('body').closePageLoader();
                            });
                    }

                },
                addedItems: function (clipboardComponent) {
                    clipboardComponent.handler.reloadGrid(clipboardComponent);
                    
                },
                addItems: function (clipboardComponent, items, senderId) {
                    $.post(clipboardComponent.configuration.viewModel.ActionAddItems, { oItems: items, idInstance: clipboardComponent.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            console.log("connected", response);
                            var selectorPath = response.SelectorPath;
                            var element = getElementsBySelectorPath(selectorPath);
                            var clipboardComponent = $(element).data('ontology-clipboard');
                            clipboardComponent.handler.gridBound(clipboardComponent);
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                            $('#' + viewModel.outputResult.ViewItemId).html(error);
                            $('body').closePageLoader();
                        });
                },
                gridBound: function (clipboardComponent) {
                    $.post(clipboardComponent.configuration.viewModel.ActionGridBound, { idInstance: clipboardComponent.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            // stuff
                            console.log(response);
                            if (response.IsSuccessful) {
                                var selectorPath = response.SelectorPath;
                                changeValuesFromServerChildren(response.ViewItems, selectorPath);
                            } else {

                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                }
            },
            templates: {
                clipboardToolbarTemplate: `
                    <div class="toolbar"></div>
                `,
                clopboardGridTemplate: `
                    <div class="gridParent">
                        <div class="grid"></div>
                    </div>
                `

            }
        }
        clipboardComponent.elements.parent = this[0];
        $(this).data('ontology-clipboard', clipboardComponent);
        clipboardComponent.handler.init(clipboardComponent);
        
    };
})(jQuery);