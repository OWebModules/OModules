﻿(function ($) {
    $.fn.createCalendar = function (configuration) {
        var calendarContainer = $('<div class="omodule-calendar-container"></div>');
        $(this).append(calendarContainer);
        var calendar = $('<div class="omodules-calendar"></div>');
        $(calendarContainer).append(calendar);
        $(calendarContainer).data('calendar-config', {
            configuration: configuration,
            elements: {
                scheduler: calendar
            },
            clickButtonHandler: function (e) {
            }
        });

        var templateMonthEvent = '<script id="monthEventTemplate" type="text/x-kendo-template">< div class="smallText" >#: title # (#: kendo.toString(start, "HH:mm") # - #: kendo.toString(end, "HH:mm") #)</div></script >';
        var templateWeekEvent = '<script id="dayWeekEventTemplate" type="text/x-kendo-template"><div>#: title # (#: kendo.toString(start, "HH:mm") # - #: kendo.toString(end, "HH:mm") #)</div ></script >';
        $('body').append(templateMonthEvent);
        $('body').append(templateWeekEvent);

        var calendarConfig = calendarContainer.data('calendar-config');

        $.post(calendarConfig.configuration.actionGetDataSource, { idInstance: calendarConfig.configuration.idInstance })
            .done(function (response) {
                var calendarElement = $('.omodule-calendar-container');
                var calendarConfig = $(calendarElement).data('calendar-config');

                $.extend(response.transport.read, {
                    data: function () {
                        return { idInstance: calendarConfig.configuration.idInstance };
                    }
                });

                $.extend(response.transport, {
                    parameterMap: function (options, operation) {
                        if (operation !== "read") {
                            options.Start = moment(options.Start).tz('Europe/Berlin').format("YYYY-MM-DD HH:mm:ss");
                            options.End = moment(options.End).tz('Europe/Berlin').format("YYYY-MM-DD HH:mm:ss");
                            return options;

                        }
                    }
                });

                console.log(JSON.stringify(response));

                //var dataSource = new kendo.data.SchedulerDataSource(response);
                var scheduler = $('.omodules-calendar').kendoScheduler({
                    date: new Date(),
                    //startTime: new Date(viewModel.startTime),
                    majorTimeHeaderTemplate: kendo.template("<strong>#=kendo.toString(date, 'HH')#</strong><sup>00</sup>"),
                    height: '100%',
                    views: [
                        { type: calendarConfig.configuration.viewTemplateDay.type, selected: calendarConfig.configuration.viewTemplateDay.selected, dateHeaderTemplate: kendo.template(calendarConfig.configuration.viewTemplateDay.dateHeaderTemplate), eventTemplate: kendo.template($("#dayWeekEventTemplate").html()) },
                        { type: calendarConfig.configuration.viewTemplateWorkWeek.type, selected: calendarConfig.configuration.viewTemplateWorkWeek.selected, dateHeaderTemplate: kendo.template(calendarConfig.configuration.viewTemplateWorkWeek.dateHeaderTemplate), eventTemplate: kendo.template($("#dayWeekEventTemplate").html()) },
                        { type: calendarConfig.configuration.viewTemplateWeek.type, selected: calendarConfig.configuration.viewTemplateWeek.selected, dateHeaderTemplate: kendo.template(calendarConfig.configuration.viewTemplateWeek.dateHeaderTemplate), eventTemplate: kendo.template($("#dayWeekEventTemplate").html()) },
                        { type: calendarConfig.configuration.viewTemplateMonth.type, selected: calendarConfig.configuration.viewTemplateMonth.selected, dateHeaderTemplate: kendo.template(calendarConfig.configuration.viewTemplateMonth.dateHeaderTemplate), eventTemplate: kendo.template($("#monthEventTemplate").html()) }
                    ],
                    dataSource: response,
                    filter: {
                        logic: "or",
                        filters: [
                            { field: "ownerId", operator: "eq", value: 1 },
                            { field: "ownerId", operator: "eq", value: 2 }
                        ]
                    },
                    dataBound: function (e) {
                        $('body').closePageLoader();
                    },
                    edit: function (e) {
                        console.log("edit", e.event);
                        if (e.event.taskId !== '') {
                            var appointment = {
                                GUID: e.event.taskId,
                                Name: e.event.title,
                                GUID_Parent: calendarConfig.configuration.idParentAppointment,
                                Type: viewModel.TypeObject
                            };

                            viewModel.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                ChannelId: calendarConfig.configuration.channelSelectedObject,
                                SenderId: calendarConfig.configuration.managerConfig.idEndpoint,
                                SessionId: calendarConfig.configuration.managerConfig.idSession,
                                UserId: calendarConfig.configuration.idUser,
                                ReceiverId: calendarConfig.configuration.idSender,
                                ViewId: calendarConfig.configuration.idView,
                                OItems: [appointment]

                            });
                        }
                    }
                });
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    };
})(jQuery);