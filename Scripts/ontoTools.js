﻿var dataTypes = {
    idDataTypeBool: "dd858f27d5e14363a5c33e561e432333",
    idDataTypeDateTime: "905fda81788f4e3d83293e55ae984b9e",
    idDataTypeInt: "3a4f5b7bda754980933efbc33cc51439",
    idDataTypeDouble: "a1244d0e187f46ee85742fc334077b7d",
    idDataTypeString: "64530b52d96c4df186fe183f44513450"
};

var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, rowdata) {
    if (value < 20) {
        return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #ff0000;">' + value + '</span>';
    }
    else {
        return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #008000;">' + value + '</span>';
    }
}

var linkrenderer = function (row, column, value) {
    if (value.indexOf('#') != -1) {
        value = value.substring(0, value.indexOf('#'));
    }
    var format = { target: '"_blank"' };
    var html = $.jqx.dataFormat.formatlink(value, format);
    return html;
}

var exitMessage = 'Do you really want to leave?';
var closeMessageSet = false;
/**
 * Generates a GUID string.
 * @returns {String} The generated GUID.
 * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
 * @author Slavik Meltser (slavik@meltser.info).
 * @link http://slavik.meltser.info/?p=142
 */
function guid() {
    function _p8(s) {
        var p = (Math.random().toString(16) + "000000000").substr(2, 8);
        return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

function htmlDecode(input, dest) {
    var e = document.getElementById(dest);
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function getUrlParameters() {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    return sURLVariables;
}

function createMenu(element, dataSource) {


    var dataAdapter = new $.jqx.dataAdapter(dataSource);
    dataAdapter.dataBind();
    var records = dataAdapter.getRecordsHierarchy('Id', 'ParentId', 'items', [{ name: 'Name', map: 'label' }]);

    $("#" + element).jqxMenu({ width: '100%', height: '30px', source: records });
    $("#" + element).css('visibility', 'visible');

    $('#' + element).on('itemclick', function (event) {
        // get the clicked LI element.
        var element = event.args;
        var id = element.id;
        SendStringCommand('ClickedMenuItem', 'Id', id);
    });

    // disable the default browser's context menu.
    $(document).on(element, function (e) {
        return false;
    });
    function isRightClick(event) {
        var rightclick;
        if (!event) var event = window.event;
        if (event.which) rightclick = (event.which === 3);
        else if (event.button) rightclick = (event.button === 2);
        return rightclick;
    }

    for (var ix in dataSource.localdata) {
        var menuEntry = dataSource.localdata[ix];
        if (menuEntry.IsVisible) {
            $('#' + menuEntry.Id).show();
        }
        else {
            $('#' + menuEntry.Id).hide();
        }

        $('#' + element).jqxMenu('disable', menuEntry.Id, !menuEntry.IsEnabled);
    }
}

function closePageLoaders(viewItems, selectorPath) {
    var element = getElementsBySelectorPath(selectorPath);
    viewItems.forEach(viewItem => {
        $($(element).find('.' + viewItem.ViewItemId)).closePageLoader();
    });
}

function changeValuesFromServerChildren(viewItems, selectorPath) {

    var element = getElementsBySelectorPath(selectorPath);
    if (element.length == 0) return;

    for (var ix in viewItems) {
        var viewItem = viewItems[ix];
        var selectorPathElement = [];
        selectorPath.forEach(sP => {
            selectorPathElement.push(sP);
        });
        selectorPathElement.push('.' + viewItem.ViewItemId);
        var element = getElementsBySelectorPath(selectorPathElement);
        if (element.length == 0) continue;
        var viewItemData = $(element).data("ViewItem");
        if (viewItemData === undefined) {
            $(element).data("viewItem", viewItem);
        }
        for (var jx in viewItem.ViewItemValues) {
            var viewItemValue = viewItem.ViewItemValues[jx];
            if (viewItem.ViewItemClass === "Window" &&
                viewItemValue.ViewItemType === "Caption") {
                document.title = viewItemValue.Value;
            } else if (viewItemValue.ViewItemType === "Enable") {
                setEnabled(selectorPath, viewItem, viewItemValue.Value, false);

            } else if (viewItemValue.ViewItemType === "Visible") {
                setVisible(selectorPath, viewItem, viewItemValue.Value, false);

            } else if (viewItemValue.ViewItemType === "Checked") {
                setCheckedMode(selectorPath, viewItem, viewItemValue.Value, false);


            } else if (viewItemValue.ViewItemType === "Content" && viewItem.ViewItemClass === "Window") {
                document.title = viewItemValue.Value;
            } else if (viewItemValue.ViewItemType === "Content") {
                setContent(selectorPath, viewItem, viewItemValue.Value, false);

            } else if (viewItemValue.ViewItemType === "PlaceHolder") {
                setPlaceholder(selectorPath, viewItem, viewItemValue.Value, false);

            } else if (viewItemValue.ViewItemType === "AddClass") {
                $(element).addClass(viewItemValue.Value);
            } else if (viewItemValue.ViewItemType === "RemoveClass") {
                $(element).removeClass(viewItemValue.Value);
            } else if (viewItemValue.ViewItemType === "Caption") {
                $(element).attr('title', viewItemValue.Value);
            } else if (viewItemValue.ViewItemType === "Tooltip") {
                $(element).attr('title', viewItemValue.Value);
            } else if (viewItemValue.ViewItemType === "CSS") {
                var value = viewItemValue.Value;
                if (value.Value === null) {
                    value.Value = '';
                }
                $(element).css(value.Key, value.Value);
            } 

            //if (typeof viewModel !== 'undefined') {
            //    if (viewModel.viewItemChanged !== undefined) {
            //        viewModel.viewItemChanged(viewItem, viewItemValue.ViewItemType);
            //    }
            //}

        }

    }
}

function changeValuesFromServer(viewItemValues) {
    console.log(viewItemValues);
    for (var ix in viewItemValues) {
        var viewItem = viewItemValues[ix];
        for (var jx in viewItem.ViewItemValues) {
            var viewItemValue = viewItem.ViewItemValues[jx];
            if (viewItem.ViewItemClass === "Window" &&
                viewItemValue.ViewItemType === "Caption") {
                document.title = viewItemValue.Value;
            } else if (viewItemValue.ViewItemType === "Enable") {
                $('#' + viewItem.ViewItemId).setEnabled(viewItemValue.Value, false);
            } else if (viewItemValue.ViewItemType === "Visible") {
                $('#' + viewItem.ViewItemId).setVisible(viewItemValue.Value, false);
            } else if (viewItemValue.ViewItemType === "Checked") {
                $('#' + viewItem.ViewItemId).setCheckedMode(viewItemValue.Value, false);
            } else if (viewItemValue.ViewItemType === "Content" && viewItem.ViewItemClass === "Window") {
                document.title = viewItemValue.Value;
            } else if (viewItemValue.ViewItemType === "Content") {
                $('#' + viewItem.ViewItemId).setContent(viewItemValue.Value, false);
            } else if (viewItemValue.ViewItemType === "PlaceHolder") {
                $('#' + viewItem.ViewItemId).setPlaceHolder(viewItemValue.Value, false);
            } else if (viewItemValue.ViewItemType === "AddClass") {
                $('#' + viewItem.ViewItemId).addClass(viewItemValue.Value);
            } else if (viewItemValue.ViewItemType === "RemoveClass") {
                $('#' + viewItem.ViewItemId).removeClass(viewItemValue.Value);
            } else if (viewItemValue.ViewItemType === "Caption") {
                $('#' + viewItem.ViewItemId).attr('title', viewItemValue.Value);
            }

            if (viewModel != undefined && viewModel.viewItemChanged != undefined) {
                viewModel.viewItemChanged(viewItem, viewItemValue.ViewItemType);
            }
        }

    }
}

function updateViewItems(viewItems) {
    for (var ix in viewItems) {
        var viewItem = viewItems[ix];

        for (var jx in viewItem.ViewItemValues) {
            var viewItemValue = viewItem.ViewItemValues[jx];

            if (viewItemValue.ViewItemType === "Enable") {
                $('#' + viewItem.ViewItemId).setEnabled(viewItemValue.Value, true);
            }
            else if (viewItemValue.ViewItemType === "Visible") {
                $('#' + viewItem.ViewItemId).setVisible(viewItemValue.Value, true);
            }
            else if (viewItemValue.ViewItemType === "Checked") {
                $('#' + viewItem.ViewItemId).setCheckedMode(viewItemValue.Value, true);
            }
            else if (viewItemValue.ViewItemType === "Content") {
                $('#' + viewItem.ViewItemId).setContent(viewItemValue.Value, true);
            }
        }
    }
}

function getViewItemValue(viewItem, viewItemType, defaultValue) {

    var value = defaultValue;
    var viewItemValue = $.grep(viewItem.ViewItemValues, function (e) { return e.ViewItemType === viewItemType; });
    if (viewItemValue.length > 0) {
        value = viewItemValue[0].Value;
    } 
    return value;
}

function getViewItem(viewItems, viewItemId) {
    var viewItem = $.grep(viewItems, function (e) { return e.ViewItemId === viewItemId });
    if (viewItem.length === 1) {
        return viewItem[0];
    }
    else {
        return null;
    }
}
function getViewItemsValue(viewItems, viewItemid, viewItemType) {

    var viewItem = $.grep(viewItems, function (e) { return e.ViewItemId === viewItemid });

    if (viewItem.length === 0) return null;

    var viewItemValue = $.grep(viewItem[0].ViewItemValues, function (e) { return e.ViewItemType === viewItemType; });

    if (viewItemValue.length === 0) return null;

    return viewItemValue[0].Value;
}

function changeValue(viewItem, changeType, value, updateServer, serverChangeCallback, idInstance) {
    if (viewItem === undefined) return false;
    var viewItemValue = $.grep(viewItem.ViewItemValues, function (e) { return e.ViewItemType === changeType; });
    if (viewItemValue.length > 0) {
        viewItemValue[0].Value = value;
    } else {
        viewItemValue = {
            ViewItemType: changeType,
            Value: value
        };

        viewItem.ViewItemValues.push(viewItemValue)
    }

    viewItem.LastValue = {
        ViewItemType: changeType,
        Value: value
    };

    if (changeType == "") {
    }

    if (!updateServer) return;

    if (idInstance === undefined || idInstance === null) {
        idInstance = viewModel.IdInstance;
    }
    $.post(viewItem.ActionChangeValue, { viewItem: viewItem, idInstance: idInstance })
        .done(function (response) {
            console.log(response);
            if (response.IsSuccessful) {
                
                if (serverChangeCallback !== undefined && serverChangeCallback !== null) {
                    serverChangeCallback(response);
                } else {
                    changeValuesFromServer(response.ViewItems);
                }
            } else {
                $('.onto-status-bar').data('messageout-config').showError(response.ResultMessage, 600);
            }
        })
        .fail(function (jqxhr, textStatus, error) {
            // stuff
        });
}

function changeValueWithSelectorPath(selectorPath, viewItem, changeType, value, updateServer, serverChangeCallback, idInstance) {
    if (viewItem === undefined) return false;
    var viewItemValue = $.grep(viewItem.ViewItemValues, function (e) { return e.ViewItemType === changeType; });
    if (viewItemValue.length > 0) {
        viewItemValue[0].Value = value;
    } else {
        viewItemValue = {
            ViewItemType: changeType,
            Value: value
        };

        viewItem.ViewItemValues.push(viewItemValue)
    }

    viewItem.LastValue = {
        ViewItemType: changeType,
        Value: value
    };

    if (!updateServer) return;

    if (idInstance === undefined || idInstance === null) {
        idInstance = viewModel.IdInstance;
    }
    $.post(viewItem.ActionChangeValue, { viewItem: viewItem, idInstance: idInstance })
        .done(function (response) {
            console.log(response);
            if (response.IsSuccessful) {
                changeValuesFromServer(response.ViewItems);
                if (serverChangeCallback !== undefined && serverChangeCallback !== null) {
                    serverChangeCallback(response);
                }
            } else {
                $('.onto-status-bar').data('messageout-config').showError(response.ResultMessage, 600);
            }
        })
        .fail(function (jqxhr, textStatus, error) {
            // stuff
        });
}

function b64EncodeUnicode(str) {
    // first we use encodeURIComponent to get percent-encoded UTF-8,
    // then we convert the percent encodings into raw bytes which
    // can be fed into btoa.
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
        function toSolidBytes(match, p1) {
            return String.fromCharCode('0x' + p1);
        }));
}

function b64DecodeUnicode(str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(atob(str).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}

/**
* Detects the font of an element from the font-family css attribute by comparing the font widths on the element
* @link http://stackoverflow.com/questions/15664759/jquery-how-to-get-assigned-font-to-element
*/

function setPlaceholder(selectorPath, viewItem, placeholder, updateServer) {

    var element = getElementsBySelectorPath(selectorPath);
    $(element).find('.' + viewItem.ViewItemId).attr('Placeholder', placeholder);
    changeValue(viewItem, "Placeholder", placeholder, updateServer);
}

function setContent(selectorPath, viewItem, content, updateServer) {

    var element = getElementsBySelectorPath(selectorPath);
    if (element.length == 0) return;

    $(element).find('.' + viewItem.ViewItemId).changeValue("Content", content, updateServer);


    if (viewItem.ViewItemType === 'CSS') {

        $(element).find('.' + viewItem.ViewItemId).css(content.Key, content.Valule);
        return;
    }
    if ($(element).find('.' + viewItem.ViewItemId).hasClass('k-button')) {
        $(element).find('.' + viewItem.ViewItemId).text(content);
    }
    else if (viewItem.ViewItemClass === 'NumericInput') {
        var numerictextbox = $(element).find('.' + viewItem.ViewItemId).data("kendoNumericTextBox");
        if (numerictextbox === undefined) return;
        numerictextbox.value(content);
    }
    else if (viewItem.ViewItemClass === 'DatePicker') {


        var val = kendo.parseDate(content);
        var kendoDatePicker = $(element).find('.' + viewItem.ViewItemId).data("kendoDatePicker");
        if (kendoDatePicker === undefined) return;
        kendoDatePicker.value(val);
    }
    else if (viewItem.ViewItemClass === 'DateTimeInput') {
        var val = kendo.parseDate(content);
        var items = $(element).find('.' + viewItem.ViewItemId);
        if (items.length === 1) {
            var kendoDateTimePicker = items.data("kendoDateTimePicker");
        } else if (items.length > 1) {
            kendoDateTimePicker = $($(element).find('.' + viewItem.ViewItemId)[1]).data("kendoDateTimePicker");
        }

        kendoDateTimePicker.value(val);
    }
    else if (viewItem.ViewItemClass === 'Input') {
        $(element).find('.' + viewItem.ViewItemId).val(content);
    }
    else if (viewItem.ViewItemClass === "KendoDropDownList") {
        var kendoDropDown = $(element).find('.' + viewItem.ViewItemId).data("kendoDropDownList");
        if (kendoDropDown === undefined) {
            kendoDropDown = $(element).parent().data("kendoDropDownList");
        };
        if (kendoDropDown === undefined) {
            return;
        }
        kendoDropDown.value(content);
    }
    else if (viewItem.ViewItemClass === "Label") {
        $(element).find('.' + viewItem.ViewItemId).html(content);
    }
    else if (viewItem.ViewItemClass === "TextArea") {
        $(element).find('.' + viewItem.ViewItemId).val(content);
    }
    else if (viewItem.ViewItemClass === "Container") {
        $(element).find('.' + viewItem.ViewItemId).html(content);
    }

    changeValue(viewItem, "Content", content, updateServer);
}

function setCheckedMode(selectorPath, viewItem, checked, updateServer) {
    var element = getElementsBySelectorPath(selectorPath);
    if (element.length == 0) return;

    if (checked) {
        $(element).find('.' + viewItem.ViewItemId).addClass('k-primary')
        $(element).find('.' + viewItem.ViewItemId).prop('checked', true);
    }
    else {
        $(element).find('.' + viewItem.ViewItemId).removeClass('k-primary')
        $(element).find('.' + viewItem.ViewItemId).prop('checked', false);
    }

    changeValue(viewItem, "Checked", checked, updateServer);
}


function setVisible(selectorPath, viewItem, visible, updateServer) {

    var element = getElementsBySelectorPath(selectorPath);
    if (element.length == 0) return;
    if (viewItem.ViewItemClass === "KendoDropDownList") {
        var dropDownList = $(element).find('.' + viewItem.ViewItemId).data("kendoDropDownList");
        if (dropDownList === undefined) {
            dropDownList = $($(element).find('.' + viewItem.ViewItemId)[0]).data("kendoDropDownList");
        }

        if (dropDownList === undefined) {
            dropDownList = $($(element).find('.' + viewItem.ViewItemId)[1]).data("kendoDropDownList");
        }

        if (dropDownList === undefined) return;

        if (visible) {
            dropDownList.wrapper.show();
        } else {
            dropDownList.wrapper.hide();
        }
    } else {
        if (visible) {
            $(element).find('.' + viewItem.ViewItemId).show();
        } else {
            $(element).find('.' + viewItem.ViewItemId).hide();
        }
    }

    changeValue(viewItem, "Visible", visible, updateServer);
}

function setEnabled(selectorPath, viewItem, enable, updateServer) {

    var element = getElementsBySelectorPath(selectorPath);
    if (element.length == 0) {
        return;
    }
    $(element).find('.' + viewItem.ViewItemId).attr('readonly', !enable);
    var viewItemsCheck = $(element).find('.' + viewItem.ViewItemId);
    if (viewItemsCheck.length == 0) return;
    var dataCheck = null
    if (viewItemsCheck.length > 0) {
        viewItemCheck = viewItemsCheck[0];
        dataCheck = $(viewItemsCheck[0]).data("ViewItem");
    }
    if (typeof viewItemCheck === 'undefined' || viewItemCheck === null) {
        return;
    }
    
    if (typeof dataCheck === 'undefined' || dataCheck === null) {
        $(viewItemCheck).data("viewItem", viewItem);
        dataCheck = $(viewItemCheck).data("viewItem");
    }

    var type = $(element).prop("type");
    if (type === 'checkbox') {
        if (enable) {
            $(viewItemCheck).removeAttr('disabled');
        } else {
            $(viewItemCheck).attr('disabled', true);
        }

    }

    if ($(viewItemCheck).hasClass('k-button')) {
        var button = $(viewItemCheck).data("kendoButton");
        if (button === undefined) return;
        button.enable(enable);
    }
    else if (viewItem.ViewItemClass === 'Input') {

        //var textBox = $('#' + viewItem.ViewItemId);

        //textBox.attr('disabled', !enable);
        //if (enable) {
        //    textBox.removeClass('input-disabled')
        //}
        //else {
        //    textBox.addClass('input-disabled')
        //}

        $(viewItemCheck).attr('readonly', !enable);
    }
    else if (viewItem.ViewItemClass === 'Radio') {
        $(viewItemCheck).attr('disabled', !enable);
    }
    else if (viewItem.ViewItemClass === 'NumericInput') {
        var numerictextbox = $(viewItemCheck).data("kendoNumericTextBox");
        if (numerictextbox === undefined) return;
        numerictextbox.readonly(!enable);

    }
    else if (viewItem.ViewItemClass === 'DatePicker') {
        //var datepicker = $('#' + viewItem.ViewItemId).data("kendoDatePicker");

        //datepicker.enable(enable);

    }
    else if (viewItem.ViewItemClass === 'DateTimeInput') {
        
        if (viewItemsCheck.length === 1) {
            var kendoDateTimePicker = $(viewItemsCheck)[0].data("kendoDateTimePicker");
        } else if (viewItemsCheck.length > 1) {
            kendoDateTimePicker = $(viewItemsCheck).find('input').data("kendoDateTimePicker");
        }
        if (kendoDateTimePicker === undefined) return;
        kendoDateTimePicker.readonly(!enable);
        //var datetimepicker = $('#' + viewItem.ViewItemId).data("kendoDateTimePicker");

        //datetimepicker.enable(enable);

    }
    else if (viewItem.ViewItemClass === "FileUpload") {
        var kendoUpload = $(viewItemCheck).data("kendoUpload");
        if (kendoUpload === undefined) return;
        kendoUpload.enable(enable);
    }
    else if (viewItem.ViewItemClass === "KendoDropDownList") {
        var kendoDropDown = $(viewItemCheck).data("kendoDropDownList");
        if (kendoDropDown === undefined) {
            kendoDropDown = $($(viewItemsCheck).find('.' + viewItem.ViewItemId)[0]).data("kendoDropDownList");
        }

        if (kendoDropDown === undefined) {
            kendoDropDown = $($(viewItemsCheck).find('.' + viewItem.ViewItemId)[1]).data("kendoDropDownList");
        }
        if (kendoDropDown === undefined) return;
        kendoDropDown.enable(enable);
    }

    //changeValue(viewItem, "Enabled", enable, updateServer);
}

(function ($) {
    $.fn.changeValue = function (changeType, value, updateServer, idInstance, callback) {
        var viewItem = $(this).data("viewItem");
        console.log('changeValue', {
            viewItem: viewItem,
            changeType: changeType,
            value: value,
            updateServer: updateServer
        });
        changeValue(viewItem, changeType, value, updateServer, callback, idInstance);
    }
    $.fn.getCheckedMode = function (selectorPath) {
        var viewItem = $(this).data("viewItem");
        if (viewItem === undefined) return false;

        if (viewItem.ViewItemClass === 'Radio') {
            if (typeof selectorPath !== 'undefined') {
                var element = getElementsBySelectorPath(selectorPath);
                if (element.length == 0) {
                    return $('#' + viewItem.ViewItemId).prop('checked');
                }
                return $(element).find('.' + viewItem.ViewItemId).prop('checked');
            } else {
                return $('#' + viewItem.ViewItemId).prop('checked');
            }
            
        }
        else {
            if (typeof selectorPath !== 'undefined') {
                var element = getElementsBySelectorPath(selectorPath);
                if (element.length == 0) {
                    return $('#' + viewItem.ViewItemId).hasClass('k-primary');
                }
                return $(element).find('.' + viewItem.ViewItemId).hasClass('k-primary');
            } else {
                return $('#' + viewItem.ViewItemId).hasClass('k-primary');
            }
            
        }

    }
    $.fn.getEnabled = function () {
        var viewItem = $(this).data("viewItem");
        if (viewItem === undefined) return false;
        return $('#' + viewItem.ViewItemId).hasClass('k-state-disabled');
    }
    $.fn.setEnabled = function (enable, updateServer, viewItem) {
        if (typeof (viewItem) === 'undefined') {
            viewItem = $(this).data("viewItem");
        } else {
            $(this).data('viewItem', viewItem);
        }
        if (typeof (viewItem) === 'undefined') return;
        $(this).changeValue("Enable", enable, updateServer);
        $(this).attr('readonly', !enable);
        var type = $(this).prop("type");
        if (type === 'checkbox') {
            if (enable) {
                $(this).removeAttr('disabled');
            } else {
                $(this).attr('disabled', true);
            }

        }

        if ($(this).hasClass('k-button')) {
            var button = $(this).data("kendoButton");
            if (button === undefined) return;
            button.enable(enable);
        }
        else if (viewItem.ViewItemClass === 'Input') {

            //var textBox = $('#' + viewItem.ViewItemId);

            //textBox.attr('disabled', !enable);
            //if (enable) {
            //    textBox.removeClass('input-disabled')
            //}
            //else {
            //    textBox.addClass('input-disabled')
            //}

            $(this).attr('readonly', !enable);
        }
        else if (viewItem.ViewItemClass === 'Radio') {
            $(this).attr('disabled', !enable);
        }
        else if (viewItem.ViewItemClass === 'NumericInput') {
            var numerictextbox = $(this).data("kendoNumericTextBox");
            if (numerictextbox === undefined) return;
            numerictextbox.readonly(!enable);

        }
        else if (viewItem.ViewItemClass === 'DatePicker') {
            //var datepicker = $('#' + viewItem.ViewItemId).data("kendoDatePicker");

            //datepicker.enable(enable);

        }
        else if (viewItem.ViewItemClass === 'DateTimeInput') {
            var dateTimeInput = $(this).data("kendoDateTimePicker");
            if (dateTimeInput === undefined) return;
            dateTimeInput.readonly(!enable);
            //var datetimepicker = $('#' + viewItem.ViewItemId).data("kendoDateTimePicker");

            //datetimepicker.enable(enable);

        }
        else if (viewItem.ViewItemClass === "FileUpload") {
            var kendoUpload = $(this).data("kendoUpload");
            if (kendoUpload === undefined) return;
            kendoUpload.enable(enable);
        }
        else if (viewItem.ViewItemClass === "KendoDropDownList") {
            var kendoDropDown = $(this).data("kendoDropDownList");
            if (kendoDropDown === undefined) return;
            kendoDropDown.enable(enable);
        }
    }

    $.fn.setPlaceHolder = function (placeholder, updateServer) {
        var viewItem = $(this).data("viewItem");
        if (viewItem === undefined) return;
        $(this).changeValue("PlaceHolder", placeholder, updateServer);
        $(this).attr('Placeholder', placeholder);

    }

    $.fn.getContent = function () {
        var viewItem = $(this).data("viewItem");
        if (viewItem === undefined) return null;

        if ($(this).hasClass('k-button')) {
            return $(this).text();
        }
        else if (viewItem.ViewItemClass === 'NumericInput') {
            var numerictextbox = $(this).data("kendoNumericTextBox");
            if (numerictextbox === undefined) return null;
            return numerictextbox.value();
        }
        else if (viewItem.ViewItemClass === 'DatePicker') {



            var kendoDatePicker = $(this).data("kendoDatePicker");
            if (kendoDatePicker === undefined) return null;

            var val = kendo.toString(kendo.parseDate(kendoDatePicker.value()), 'yyyy-MM-dd');
            return val;
        }
        else if (viewItem.ViewItemClass === 'DateTimeInput') {
            var kendoDateTimePicker = $(this).data("kendoDateTimePicker");
            if (kendoDateTimePicker === undefined) return null;

            var val = kendo.toString(kendo.parseDate(kendoDateTimePicker.value()), 'yyyy-MM-dd hh:mm:ss');
            return val;
        }
        else if (viewItem.ViewItemClass === 'Input') {
            return $(this).val();
        }
        else if (viewItem.ViewItemClass === "KendoDropDownList") {
            var kendoDropDown = $(this).data("kendoDropDownList");
            if (kendoDropDown === undefined) return null;
            return kendoDropDown.value();
        }
        else if (viewItem.ViewItemClass === "Label") {
            return $(this).html();
        }
        else if (viewItem.ViewItemClass === "TextArea") {
            return $(this).val();
        }
        else if (viewItem.ViewItemClass === "CheckBox") {
            return $(this).is(':checked');
        }
        return null;
    }

    $.fn.setContent = function (content, updateServer) {
        var viewItem = $(this).data("viewItem");
        if (viewItem === undefined) return;


        $(this).changeValue("Content", content, updateServer);


        if (viewItem.ViewItemType === 'CSS') {

            $(this).css(content.Key, content.Valule);
            return;
        }
        if ($(this).hasClass('k-button')) {
            $(this).text(content);
        }
        else if (viewItem.ViewItemClass === 'NumericInput') {
            var numerictextbox = $(this).data("kendoNumericTextBox");
            if (numerictextbox === undefined) return;
            numerictextbox.value(content);
        }
        else if (viewItem.ViewItemClass === 'DatePicker') {


            var val = kendo.parseDate(content);
            var kendoDatePicker = $(this).data("kendoDatePicker");
            if (kendoDatePicker === undefined) return;
            kendoDatePicker.value(val);
        }
        else if (viewItem.ViewItemClass === 'DateTimeInput') {
            var val = kendo.parseDate(content);
            var kendoDateTimePicker = $(this).data("kendoDateTimePicker");
            if (kendoDateTimePicker === undefined) return;
            kendoDateTimePicker.value(val);
        }
        else if (viewItem.ViewItemClass === 'Input') {
            $(this).val(content);
        }
        else if (viewItem.ViewItemClass === "KendoDropDownList") {
            var kendoDropDown = $(this).data("kendoDropDownList");
            if (kendoDropDown === undefined) return;
            kendoDropDown.value(content);
        }
        else if (viewItem.ViewItemClass === "Label") {
            $(this).html(content);
        }
        else if (viewItem.ViewItemClass === "TextArea") {
            $(this).val(content);
        }
        else if (viewItem.ViewItemClass === "Container") {
            $(this).html(content);
        }

    }

    $.fn.isToggled = function () {
        if ($(this).hasClass('k-primary')) {
            return true;
        }
        else {
            return false;
        }
    }
    $.fn.toggleMode = function (updateServer, checked, idInstance, callback) {
        var toggleMode = false;
        if (typeof checked !== 'undefined' && checked !== null) {
            toggleMode = checked;
        }
        else {
            toggleMode = !$(this).hasClass('k-primary');
        }
        
        if (toggleMode) {
            $(this).addClass('k-primary')
        }
        else {
            $(this).removeClass('k-primary')
        }

        $(this).changeValue("Checked", toggleMode, updateServer, idInstance, callback);
        return toggleMode;
    }

    $.fn.setCheckedMode = function (checked, updateServer) {
        console.log('Checked (' + checked + ')', $(this));
        if (checked) {
            $(this).addClass('k-primary')
            $(this).prop('checked', true);
        }
        else {
            $(this).removeClass('k-primary')
            $(this).prop('checked', false);
        }
        $(this).changeValue("Checked", checked, updateServer);
    }

    $.fn.setVisible = function (isVisible, updateServer) {

        console.log($(this).attr("id") + ' (Visible)', {
            isVisible: isVisible,
            updateServer: updateServer
        });
        if (isVisible) {

            $(this).show();
        } else {
            $(this).hide();
        }
        $(this).changeValue("Visible", isVisible, updateServer);
    }

    $.fn.getViewItemValue = function (viewItemType) {

        var viewItem = $(this).data('viewItem');
        var viewItemValue = $.grep(viewItem.ViewItemValues, function (e) { return e.ViewItemType === viewItemType; });
        return viewItemValue;
    }

    $.fn.exists = function () {
        return this.length !== 0;
    }

    $.fn.resizeToFit = function (configuration) {

        if (configuration === undefined) {
            configuration = {
                width: true,
                height: true
            }
        }

        var $element = $(this);

        var position = $element.position();
        var possibleHeight = window.innerHeight;
        var possibleWidth = window.innerWidth;

        var width = possibleWidth - position.left - 15;
        var height = possibleHeight - position.top - 18;

        if (configuration.width) {
            $element.css('width', width + 'px');
        }

        if (configuration.height) {
            $element.css('height', height + 'px');
        }

    }

    $.fn.detectFont = function () {
        var fontfamily = $(this).css('font-family');
        var fonts = fontfamily.split(',');
        if (fonts.length === 1)
            return fonts[0];

        var element = $(this);
        var detectedFont = null;
        fonts.forEach(function (font) {
            var clone = $('<span>wwwwwwwwwwwwwwwlllllllliiiiii</span>').css({ 'font-family': fontfamily, 'font-size': '70px', 'display': 'inline', 'visibility': 'hidden' }).appendTo('body');
            var dummy = $('<span>wwwwwwwwwwwwwwwlllllllliiiiii</span>').css({ 'font-family': font, 'font-size': '70px', 'display': 'inline', 'visibility': 'hidden' }).appendTo('body');
            //console.log(clone, dummy, fonts, font, clone.width(), dummy.width());
            if (clone.width() === dummy.width())
                detectedFont = font;
            clone.remove();
            dummy.remove();
        });

        return detectedFont;
    }

    $.fn.addRow = function (rows) {

        var elementId = this[0].id;
        var value = $('#' + elementId).jqxGrid('addrow', null, rows);
    }

    $.fn.getSelectedText = function () {

        var elem = this[0];
        if (elem.tagName === "TEXTAREA" ||
            (elem.tagName === "INPUT" && elem.type === "text")) {
            return elem.value.substring(elem.selectionStart,
                elem.selectionEnd);
            // or return the return value of Tim Down's selection code here
        }
        return null;

    }
    $.fn.copyValToClipboard = function () {
        // create hidden text element, if it doesn't already exist
        var type = $(this).attr('type');
        $(this).attr('type', 'text');
        var targetId = "_hiddenCopyText_";
        var isInput = this[0].tagName === "INPUT" || this[0].tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            // can just use the original source element for the selection and copy
            target = this[0];
            origSelectionStart = this[0].selectionStart;
            origSelectionEnd = this[0].selectionEnd;
        } else {
            // must use a temporary form element for the selection and copy
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = this[0].textContent;
        }
        // select the content
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch (e) {
            succeed = false;
        }
        // restore original focus
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }

        if (isInput) {
            // restore prior selection
            this[0].setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            // clear temporary content
            target.textContent = "";
        }
        ontoLog(succeed);
        $(this).attr('type', type);
        return succeed;
    }

    $.fn.showWait = function () {
        var element = this[0];
        var id = 'wait' + element.id;
        var rect = element.getBoundingClientRect();
        var width = (rect.right - rect.left);
        var height = (rect.bottom - rect.top);


        var div = document.createElement('div');
        div.id = id;
        div.setAttribute('class', 'popupToolTipItem w3-animate-opacity');
        div.style.position = "fixed";
        div.style.left = rect.left.toString() + "px";
        div.style.top = rect.top.toString() + "px";
        div.style.width = width.toString() + "px";
        div.style.height = height.toString() + "px";
        div.style.zIndex = 999999;
        div.innerHTML = '<center>...</center>';
        div.style.opacity = '.8';
        div.style.background = 'white';

        document.body.appendChild(div);

        var childElement = div.firstChild;
        rect = childElement.getBoundingClientRect();
        var paddingLeft = (width / 2) - ((rect.right - rect.left) / 2);
        var paddingTop = (height / 2) - ((rect.bottom - rect.top) / 2);

        childElement.style.marginTop = paddingTop + "px";
        childElement.style.marginLeft = paddingLeft + "px";

        div.style.borderRadius = "5px";
        //div.style.width = width.toString() + "px";
        //div.style.height = height.toString() + "px";

    }

    $.fn.removeWait = function () {
        var element = this[0];
        var id = 'wait' + element.id;
        var element = document.getElementById(id);
        if (element != undefined) {
            element.parentElement.removeChild(element);
        }
    }

    $.fn.showToolTip = function (html, forecolor, backcolor) {
        var element = this[0];
        var rect = element.getBoundingClientRect();
        var width = (rect.right - rect.left);
        var height = (rect.bottom - rect.top);


        var div = document.createElement('div');
        div.setAttribute('class', 'popupToolTipItem w3-animate-opacity');
        div.style.position = "fixed";
        div.style.left = rect.left.toString() + "px";
        div.style.top = rect.top.toString() + "px";
        div.style.width = width.toString() + "px";
        div.style.height = height.toString() + "px";
        div.style.zIndex = 999999;
        div.innerHTML = html;
        div.style.color = forecolor;
        div.style.opacity = '.8';
        div.style.background = backcolor;

        document.body.appendChild(div);

        var childElement = div.firstChild;
        rect = childElement.getBoundingClientRect();
        var paddingLeft = (width / 2) - ((rect.right - rect.left) / 2);
        var paddingTop = (height / 2) - ((rect.bottom - rect.top) / 2);

        childElement.style.marginTop = paddingTop + "px";
        childElement.style.marginLeft = paddingLeft + "px";

        div.style.borderRadius = "5px";
        //div.style.width = width.toString() + "px";
        //div.style.height = height.toString() + "px";

        if (backcolor != undefined) {
            div.style.background = backcolor;
        }

        toolTipTimer = setTimeout(hideToolTips, 600);


    }
    $.fn.createMenu = function (dataSource, isContextMenu, itemCount) {

        var elementId = this[0].id
        var dataAdapter = new $.jqx.dataAdapter(dataSource);
        var height = itemCount * 30
        var sHeight = height + 'px'

        dataAdapter.dataBind();
        var records = dataAdapter.getRecordsHierarchy('Id', 'ParentId', 'items', [{ name: 'Name', map: 'label' }, { name: 'Id', map: 'id' }]);

        if (isContextMenu) {
            $("#" + elementId).jqxMenu({ width: '120px', height: sHeight, source: records, autoOpenPopup: false, mode: 'popup' });
        }
        else {
            $("#" + elementId).jqxMenu({ width: '120px', height: sHeight, source: records });
        }

        $("#" + elementId).css('visibility', 'visible');

        $('#' + elementId).on('itemclick', function (event) {
            // get the clicked LI element.
            var element = event.args;
            var id = element.id;
            SendStringCommand('ClickedMenuItem', 'Id', id);
        });

        // disable the default browser's context menu.
        if (isContextMenu) {
            $(document).on('contextmenu', function (e) {
                return false;
            });
        }


        for (var ix in dataSource.localdata) {
            var menuEntry = dataSource.localdata[ix];
            if (menuEntry.IsVisible) {
                $('#' + menuEntry.Id).show();
            }
            else {
                $('#' + menuEntry.Id).hide();
            }

            $('#' + elementId).jqxMenu('disable', menuEntry.Id, !menuEntry.IsEnabled);
        }

    }

    $.fn.createKendoGrid = function (config) {

        var grid = $(this);
        if (grid.hasClass('k-grid')) {
            grid.data("kendoGrid").destroy(); // destroy the Grid

            grid.empty(); // empty the Grid content (inner HTML)
        }

        if (config.dataBoundFunc != undefined) {
            config.gridConfig.dataBound = config.dataBoundFunc;
        }

        if (config.changeFunc != undefined) {
            config.gridConfig.change = config.changeFunc;
        }

        var dataSource = new kendo.data.DataSource({
            transport: config.gridConfig.dataSource.transport,
            change: config.dataSourceChnageFunc,
            pageSize: config.gridConfig.dataSource.pageSize
        });

        config.gridConfig.dataSource = dataSource;


        if (config.gridConfig.rowTemplate != undefined) {
            grid.kendoGrid({
                dataSource: config.gridConfig.dataSource,
                height: config.gridConfig.height,
                width: config.gridConfig.width,
                groupable: config.gridConfig.groupable,
                sortable: config.gridConfig.sortable,
                autoBind: config.gridConfig.autoBind,
                selectable: config.gridConfig.selectable,
                pageable: config.gridConfig.pageable,
                columns: config.gridConfig.columns,
                filterable: config.gridConfig.filterable,
                dataBound: config.gridConfig.dataBound,
                change: config.gridConfig.change,
                editable: config.gridConfig.editable,
                rowTemplate: kendo.template($("#" + config.gridConfig.rowTemplate).html())
            });
        } else {
            grid.kendoGrid({
                dataSource: config.gridConfig.dataSource,
                height: config.gridConfig.height,
                width: config.gridConfig.width,
                groupable: config.gridConfig.groupable,
                sortable: config.gridConfig.sortable,
                autoBind: config.gridConfig.autoBind,
                selectable: config.gridConfig.selectable,
                pageable: config.gridConfig.pageable,
                columns: config.gridConfig.columns,
                filterable: config.gridConfig.filterable,
                dataBound: config.gridConfig.dataBound,
                editable: config.gridConfig.editable,
                change: config.gridConfig.change
            });
        }



        setTimeout(function () {
            dataSource.read();
        }, 500);
    };

    $.fn.createGrid = function (dataAdapter, columnConfig, standardFilter, pagerrenderer, notPageable, loadCompleteFunction, selectionMode) {
        var elementId = this[0].id

        var pageable = true;
        var pageNum = 0;
        var pageSize = 20;

        if (selectionMode === undefined) {
            selectionMode = 'singlecell';
        }

        if (notPageable != undefined) {
            pageable = !notPageable;
        }
        if (pagerrenderer != undefined) {
            $("#" + elementId).jqxGrid(
                {
                    source: dataAdapter,
                    width: "100%",
                    height: "100%",
                    pageable: pageable,
                    filterable: true,
                    sortable: true,
                    altrows: true,
                    enabletooltips: true,
                    editable: true,
                    autoshowfiltericon: true,
                    showfilterrow: true,
                    columnsresize: true,
                    pagerrenderer: pagerrenderer,
                    selectionmode: selectionMode,
                    columns: columnConfig,
                    ready: loadCompleteFunction,
                    pagesizeoptions: ['5', '10', '20', '50', '100', '500', '1000']

                });
        }
        else {
            $("#" + elementId).jqxGrid(
                {
                    source: dataAdapter,
                    width: "100%",
                    height: "100%",
                    pageable: pageable,
                    filterable: true,
                    sortable: true,
                    altrows: true,
                    enabletooltips: true,
                    editable: true,
                    autoshowfiltericon: true,
                    showfilterrow: true,
                    columnsresize: true,
                    selectionmode: selectionMode,
                    columns: columnConfig,
                    ready: loadCompleteFunction,
                    pagesizeoptions: ['5', '10', '20', '50', '100', '500', '1000']

                });
        }








        $("#" + elementId).show();
        initialized = true;
    }

    $.fn.outputMessage = function (configuration) {
        
        var element = $(this);
        if (typeof (configuration.colors) === 'undefined') {
            configuration.colors = {
                backColorInfo: "#393838",
                backColorWarning: "#393838",
                backColorError: "#393838",
                backColorCritical: "red",
                foreColorInfo: "white",
                foreColorError: "red",
                foreColorWarning: "yellow",
                foreColorCritical: "black"
            }
        }
        
        if (typeof (configuration.clearTimeout) === 'undefined') {
            configuration.clearTimeout = {
                info: false,
                warning: true,
                error: false,
                critical: false
            };
        }

        if (typeof (configuration.error) !== 'undefined') {
            configuration.message.MessageType = 3;
            configuration.message.Message = configuration.error;
        }

        var clearTimeout = false;

        switch (configuration.message.MessageType) {
            case 1:
                $('.execution-log').html(configuration.message.Message);
                $(element).css('background-color', configuration.colors.backColorInfo);
                $(element).css('color', configuration.colors.foreColorInfo);
                if (configuration.clearTimeout.info) clearTimeout = true;
                break;
            case 2:
                $('.execution-log').html(configuration.message.Message);
                $(element).css('background-color', configuration.colors.backColorWarning);
                $(element).css('color', configuration.colors.foreColorWarning);
                if (configuration.clearTimeout.warning) clearTimeout = true;
                break;
            case 3:
                $('.execution-log').html(configuration.message.Message);
                $(element).css('background-color', configuration.colors.backColorError);
                $(element).css('color', configuration.colors.foreColorError);
                if (configuration.clearTimeout.error) clearTimeout = true;
                break;
            case 4:
                $('.execution-log').html(configuration.message.Message);
                $(element).css('background-color', configuration.colors.backColorCritical);
                $(element).css('color', configuration.colors.foreColorCritical);
                if (configuration.clearTimeout.critical) clearTimeout = true;
                break;
        }

        if (clearTimeout) {
            setTimeout(function (element, colors) {
                $(element).html('')
                $(element).css('background-color', colors.backColorInfoToError);
                $(element).css('color', colors.foreColorInfo);
            }, 2000, $('.execution-log'), configuration.colors);
        }
    }
})(jQuery);

function isRightClick(event) {
    var rightclick;
    if (!event) var event = window.event;
    if (event.which) rightclick = (event.which === 3);
    else if (event.button) rightclick = (event.button === 2);
    return rightclick;
}


function setViewItemVisibility(viewItem) {
    if (viewItem.ViewItemType === 'Visible') {

        viewItemId = viewItem.ViewItemId
        if (viewItemId === '@classItem@') {
            if (viewItem.LastValue === true) {
                $(viewItem.ViewItemClass).show()
            }
            else {
                $(viewItem.ViewItemClass).hide()
            }
        }
        else {
            if (viewItem.LastValue === true) {
                $('#' + viewItem.ViewItemId).show()
            }
            else {
                $('#' + viewItem.ViewItemId).hide()
            }
        }

    }

}

function setViewItemReadonlyState(viewItem) {

    $('#' + viewItem.ViewItemId).attr('readonly', viewItem.LastValue);
    if (!viewItem.LastValue) {
        $('#' + viewItem.ViewItemId).removeClass('readonly');
    }
    else {
        $('#' + viewItem.ViewItemId).addClass('readonly');

    }

}


function SetCloseMessage(viewItem) {
    if (viewItem.ViewItemId === 'exitMessage') {
        exitMessage = viewItem.LastValue;
        if (exitMessage === undefined || exitMessage === '') {
            if (closeMessageSet) {
                $(window).unbind('beforeunload');
                closeMessageSet = false;
            }

        }
        else {
            if (!closeMessageSet) {
                closeMessageSet = true;
                $(window).bind('beforeunload', function () {

                    return exitMessage;
                });
            }

        }


    }
}


function measureText(canvas, text, font) {
    var ctx = canvas.getContext("2d");
    ctx.font = font;
    return ctx.measureText(text).width;
}

class ViewItem {
    constructor() {
        this.ViewItemId = '';
        this.ViewItemType = '';
        this.ViewItemClass = '';
        this.ViewItemValue = null;
    }


}

function hideToolTips() {
    var elements = document.getElementsByClassName('popupToolTipItem');
    if (elements.length > 0) {
        for (var ix in elements) {
            if (isElement(elements[ix])) {
                elements[ix].parentElement.removeChild(elements[ix]);
            }

        }
    }


    clearTimeout(toolTipTimer);
}

/**
     * PRODUCTION
 * Return true if object parameter is a DOM Element and false otherwise.
 *
 * @param {object} Object to test
 * @return {boolean}
 */
isElement = function (a) { try { return a.constructor.__proto__.prototype.constructor.name ? !0 : !1 } catch (b) { return !1 } };

$.fn.visibleHeight = function () {
    var elBottom, elTop, scrollBot, scrollTop, visibleBottom, visibleTop;
    scrollTop = $(window).scrollTop();
    scrollBot = scrollTop + $(window).height();
    elTop = this.offset().top;
    elBottom = elTop + this.outerHeight();
    visibleTop = elTop < scrollTop ? scrollTop : elTop;
    visibleBottom = elBottom > scrollBot ? scrollBot : elBottom;
    return visibleBottom - visibleTop
}

function htmlEncode(value) {
    return $('<div/>').text(value).html();
}

function htmlDecode(value) {
    return $('<div/>').html(value).text();
}

function b64DecodeUnicode(str) {
    return decodeURIComponent(Array.prototype.map.call(atob(str), function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(''))
}

function isValidURL(str) {
    var a = document.createElement('a');
    a.href = str;
    return (a.host !== "");
}

function utf8_to_b64(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}

function b64_to_utf8(str) {
    return decodeURIComponent(escape(window.atob(str)));
}

function selectAndCopy(element) {
    var td = $(element).closest("td");
    $(td).CopyToClipboard();

}