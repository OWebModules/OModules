﻿(function ($) {
    $.fn.createVoucherAddComponent = function (configuration) {
        $(this).data('voucher-add-config', {
            elements: {

            },
            components: {

            },
            data: {

            },
            configuration: configuration,
            viewModel: null,
            handler: {
                createComponent: function (voucherAddComponent, element) {
                    voucherAddComponent.elements.voucherAddArea = element;
                    voucherAddComponent.elements.voucherAddContainer = $(voucherAddComponent.templates.componentTemplate);
                    $(voucherAddComponent.elements.voucherAddArea).append(voucherAddComponent.elements.voucherAddContainer);
                    voucherAddComponent.elements.grid = $(voucherAddComponent.elements.voucherAddArea).find('.newVoucherGrid')
                    voucherAddComponent.handler.triggerEvents.createComponent(voucherAddComponent, true);
                    voucherAddComponent.handler.initViewModel(voucherAddComponent);
                },
                initViewModel: function (voucherAddComponent) {
                    voucherAddComponent.handler.triggerEvents.initViewModel(voucherAddComponent, false);
                    $.post(voucherAddComponent.configuration.actions.init, { idInstance: voucherAddComponent.configuration.idInstance, selectorPath: voucherAddComponent.configuration.selectorPath })
                        .done(function (response) {
                            var voucherAddComponent = $(getElementsBySelectorPath(response.SelectorPath)).data('voucher-add-config');
                            voucherAddComponent.configuration.viewModel = response.Result;
                            changeValuesFromServerChildren(response.ViewItems, voucherAddComponent.configuration.selectorPath);
                            voucherAddComponent.handler.triggerEvents.initViewModel(voucherAddComponent, true);
                            voucherAddComponent.handler.createGrid(voucherAddComponent);
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                getVoucherParserButtonHtml: function (voucherAddComponent) {
                    var buttonTemplate = voucherAddComponent.templates.voucherButtonTemplate;
                    var buttonGroupHtml = '';
                    for (var ix in voucherAddComponent.configuration.viewModel.VoucherParserList) {
                        var voucherParser = voucherAddComponent.configuration.viewModel.VoucherParserList[ix];
                        var buttonHtml = buttonTemplate.replace("@ID@", voucherParser.IdParser);
                        var buttonHtml = buttonHtml.replace("@NAME@", voucherParser.NameParser);
                        buttonGroupHtml += buttonHtml;
                    }
                    return buttonGroupHtml;
                },
                createGrid: function (voucherAddComponent) {
                    voucherAddComponent.handler.triggerEvents.createGrid(voucherAddComponent, false);
                    $.post(voucherAddComponent.configuration.viewModel.ActionGetGridConfig, { idInstance: voucherAddComponent.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            var voucherAddComponent = $(getElementsBySelectorPath(response.SelectorPath)).data('voucher-add-config');

                            $.extend(response.Result.dataSource.transport.read, {
                                component: voucherAddComponent
                            });
                            $.extend(response.Result.dataSource.transport.read, {
                                data: function () {
                                    return {
                                        idInstance: this.component.configuration.viewModel.IdInstance
                                    };
                                }
                            })

                            $.extend(response.Result.dataSource, {
                                batch: true
                            });

                            $.extend(response.Result.dataSource.transport.update,
                                {
                                    complete: function (e) {
                                        console.log(e);
                                    }
                                }
                            );

                            $.extend(response.Result.dataSource.transport.destroy,
                                {
                                    complete: function (e) {
                                        console.log(e);
                                    }
                                }
                            );

                            $.extend(response.Result.dataSource.transport.read,
                                {
                                    complete: function (e) {
                                        console.log(e);
                                    }
                                }
                            );

                            voucherAddComponent.data.dataSource = new kendo.data.DataSource(response.Result.dataSource);
                            voucherAddComponent.data.dataSource.bind("error", function (e) {
                                console.log(e);
                                //$('.object-list').data('object-list-config').elements.gridItem.cancelChanges();
                                //var dialogElement = $('#dialog');
                                //dialogElement.kendoDialog({
                                //    width: "300px",
                                //    height: "150px",
                                //    title: "Value",
                                //    closable: true,
                                //    modal: false,
                                //    content: "An error occured!",
                                //    actions: []
                                //});

                                //dialogElement.data("kendoDialog").open();
                            });

                            var parserButtonHtml = voucherAddComponent.handler.getVoucherParserButtonHtml(voucherAddComponent);
                            var gridToolbarHtml = voucherAddComponent.templates.gridToolbarTemplate;
                            gridToolbarHtml = gridToolbarHtml.replace("@PARSERBUTTON@", parserButtonHtml);
                            response.Result.toolbar = kendo.template($(gridToolbarHtml).html())
                            $(voucherAddComponent.elements.grid).kendoGrid(response.Result);

                            voucherAddComponent.handler.attachHandlerToGridButtons(voucherAddComponent);
                            voucherAddComponent.components.grid = $(voucherAddComponent.elements.grid).data('kendoGrid');
                            
                            changeValuesFromServerChildren(response.ViewItems, voucherAddComponent.configuration.selectorPath);
                            voucherAddComponent.handler.triggerEvents.createGrid(voucherAddComponent, true);
                        })
                },
                reloadGrid: function (voucherAddComponent) {

                },
                validateReference: function (voucherAddComponent) {

                },
                attachHandlerToGridButtons: function (voucherAddComponent) {
                    $(voucherAddComponent.elements.grid).find('.k-header').find('.k-button').kendoButton();
                },
                triggerEvents:
                {
                    createComponent: function (voucherAddComponent, finishState) {
                        if (finishState == true) {
                            if (typeof voucherAddComponent.configuration.events !== 'undefined' && voucherAddComponent.configuration.events !== null &&
                                typeof voucherAddComponent.configuration.events.createdComponent !== 'undefined' && voucherAddComponent.configuration.events.createdComponent !== null) {
                                voucherAddComponent.configuration.events.createdComponent(voucherAddComponent);
                            }
                        } else {
                            if (typeof voucherAddComponent.configuration.events !== 'undefined' && voucherAddComponent.configuration.events !== null &&
                                typeof voucherAddComponent.configuration.events.createComponent !== 'undefined' && voucherAddComponent.configuration.events.createComponent !== null) {
                                voucherAddComponent.configuration.events.createComponent(voucherAddComponent);
                            }
                        }
                    },
                    initViewModel: function (voucherAddComponent, finishState) {
                        if (finishState == true) {
                            if (typeof voucherAddComponent.configuration.events !== 'undefined' && voucherAddComponent.configuration.events !== null &&
                                typeof voucherAddComponent.configuration.events.initViewModelFinish !== 'undefined' && voucherAddComponent.configuration.events.initViewModelFinish !== null) {
                                voucherAddComponent.configuration.events.initViewModelFinish(voucherAddComponent);
                            }
                        } else {
                            if (typeof voucherAddComponent.configuration.events !== 'undefined' && voucherAddComponent.configuration.events !== null &&
                                typeof voucherAddComponent.configuration.events.initViewModel !== 'undefined' && voucherAddComponent.configuration.events.initViewModel !== null) {
                                voucherAddComponent.configuration.events.initViewModel(voucherAddComponent);
                            }
                        }
                    },
                    createGrid: function (voucherAddComponent, finishState) {
                        if (finishState == true) {
                            if (typeof voucherAddComponent.configuration.events !== 'undefined' && voucherAddComponent.configuration.events !== null &&
                                typeof voucherAddComponent.configuration.events.createdGrid !== 'undefined' && voucherAddComponent.configuration.events.createdGrid !== null) {
                                voucherAddComponent.configuration.events.createdGrid(voucherAddComponent);
                            }
                        } else {
                            if (typeof voucherAddComponent.configuration.events !== 'undefined' && voucherAddComponent.configuration.events !== null &&
                                typeof voucherAddComponent.configuration.events.createGrid !== 'undefined' && voucherAddComponent.configuration.events.createGrid !== null) {
                                voucherAddComponent.configuration.events.createGrid(voucherAddComponent);
                            }
                        }
                    }

                }
            },
            templates: {
                componentTemplate: `
                    <div class="voucherAddContainer">
                        <div class="voucher-add-header">
                            <label>Voucher-Text:</label>
                            <textarea class="voucherInput"></textarea>
                        </div>
                        <div class="voucher-add-body">
                            <div class="buttonGroupSelectedVoucherParser"></div>
                            <div class="newVoucherGrid"></div>
                        </div>
                    </div>
                    `,
                gridToolbarTemplate: `
                    <script type="x-kendo/template" class="voucher-add-grid-toolbar">
                        <div class="btn-group" role="group">
                          @PARSERBUTTON@
                        </div>
                        <div class="btn-group" role="group">
                          <button type="button" class="btn btn-primary k-button buttonSave">Save</button>
                          <button type="button" class="btn btn-primary k-button buttonClear">Clear</button>
                        </div>
                    </script>
                    `,
                voucherButtonTemplate: `
                    <button id="@ID@" type="button" class="btn btn-primary k-button buttonParse" disabled>Parse @NAME@</button>
                    `
            }
        });

        var voucherAddComponent = $(this).data('voucher-add-config');
        voucherAddComponent.handler.triggerEvents.createComponent(voucherAddComponent, false);
        voucherAddComponent.handler.createComponent(voucherAddComponent, this);
    };
})(jQuery);