﻿(function ($) {
    $.fn.createVideoPlayer = function (configuration) {
        var videoPlayerComponent = {
            configuration: configuration,
            elements: {
            },
            data: {
            },
            components: {
            },
            handler: {
                createComponent: function (videoPlayerComponent) {
                    videoPlayerComponent.elements.videoSplitter = $(videoPlayerComponent.templates.splitContainer);
                    
                    $(elements).append(videoPlayerComponent.elements.videoSplitter);
                    $(videoPlayerComponent.elements.videoSplitter)
                        .kendoSplitter({
                            orientation: "horizontal",
                            panes: [
                                { size: "80%" },
                                { size: "20%", collapsible: true, collapsed: false }
                            ]
                        });
                    videoPlayerComponent.elements.listViewElement = $($(elements).find('.playlist'));
                    videoPlayerComponent.elements.videoPlayerElement = $($(elements).find('.video-player'));

                    videoPlayerComponent.components.listViewComponent = $(videoPlayerComponent.elements.listViewElement).kendoListView({
                        dataSource: videoPlayerComponent.data.dataSource,
                        selectable: true,
                        scrollable: true,
                        template: kendo.template($("#template").html()),
                        change: function () {
                            var videoPlayerComponent = $($(this)[0].element).parent().parent().data('videoPlayerComponent-component');
                            var index = this.select().index();
                            var dataItem = this.dataSource.view()[index];


                            var mediaItem = {
                                title: dataItem.NameMultimediaItem,
                                poster: dataItem.PosterUrl,
                                source: dataItem.FileUrl
                            };
                            var videoPlayer = videoPlayerComponent.elements.videoPlayerElement;
                            $(videoPlayer).data("media-item", dataItem);
                            $(videoPlayer).data("kendoMediaPlayer").media(mediaItem);
                            if (typeof videoPlayerComponent.configuration.events !== 'undefined' && videoPlayerComponent.configuration.events !== null &&
                                typeof videoPlayerComponent.configuration.events.changed !== 'undefined' && videoPlayerComponent.configuration.events.changed !== null) {
                                videoPlayerComponent.configuration.events.changed(videoPlayerComponent, dataItem);
                            }
                        },
                        dataBound: function () {

                        }
                    });

                    $(videoPlayerComponent.elements.videoPlayerElement).kendoMediaPlayer({
                        autoPlay: false,
                        navigatable: true,
                        ready: function () {
                            var videoPlayer = $($(this)[0].element).parent().find('.k-mediaplayer-media');
                                //$('.k-mediaplayer-media');
                            $(videoPlayer).on('ended', function () {
                                
                            });

                            $(videoPlayer).on('play', function () {
                                
                            });

                            $(videoPlayer).on('pause', function () {
                                
                            });
                            $(videoPlayer).on('timeupdate', function () {
                                //var videoPlayer = $($(this).parent()).data("kendoMediaPlayer");
                                currentTime = this.currentTime;
                                var videoPlayerComponent = $(this).closest(".video-player-container").data('videoPlayerComponent-component');
                                videoPlayerComponent.data.currentTime = currentTime;
                            });
                            videoPlayer = $($(this)[0].element).parent().find('.video-player')
                            if ($(videoPlayer).data("kendoMediaPlayer").options.autoPlay) {
                                $(videoPlayer).data("kendoMediaPlayer").play();
                            }
                        }

                    });

                    videoPlayerComponent.components.kendoMediaPlayer = $(videoPlayerComponent.elements.videoPlayerElement).data("kendoMediaPlayer");

                    if (typeof videoPlayerComponent.configuration.events !== 'undefined' && videoPlayerComponent.configuration.events !== null &&
                        typeof videoPlayerComponent.configuration.events.completed !== 'undefined' && videoPlayerComponent.configuration.events.completed !== null) {
                        videoPlayerComponent.configuration.events.completed(videoPlayerComponent);
                    }
                },
                validateReference: function (videoPlayerComponent, idItem) {
                    $.post(videoPlayerComponent.viewModel.ActionConfig.ActionValidateReference, { refItem: { GUID: idItem }, idInstance: videoPlayerComponent.viewModel.IdInstance })
                        .done(function (response) {
                            console.log("connected", response);
                            var element = getElementsBySelectorPath(response.SelectorPath);
                            var videoPlayerComponent = $(element).data('videoPlayerComponent-component');
                            for (var i in response.Result.MediaListItems) {
                                var mediaItem = response.Result.MediaListItems[i];
                                videoPlayerComponent.data.dataSource.add(mediaItem);
                            }

                            $(videoPlayerComponent.elements.listViewElement).data('kendoListView').refresh();
                            if (typeof videoPlayerComponent.configuration.events !== 'undefined' && videoPlayerComponent.configuration.events !== null &&
                                typeof videoPlayerComponent.configuration.events.validatedReference !== 'undefined' && videoPlayerComponent.configuration.events.validatedReference !== null) {
                                videoPlayerComponent.configuration.events.validatedReference(videoPlayerComponent);
                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                seek: function (videoPlayerComponent, second, pause) {
                    videoPlayerComponent.components.kendoMediaPlayer.seek(second * 1000);
                    if (typeof pause !== 'undefined' && pause) {
                        videoPlayerComponent.components.kendoMediaPlayer.pause();
                    } else {
                        videoPlayerComponent.components.kendoMediaPlayer.play();
                    }
                }, pause: function (videoPlayerComponent) {
                    videoPlayerComponent.components.kendoMediaPlayer.pause();
                }, play: function (videoPlayerComponent) {
                    videoPlayerComponent.components.kendoMediaPlayer.play();
                }, isPaused: function (videoPlayerComponent) {
                    return videoPlayerComponent.components.kendoMediaPlayer.isPaused();
                }
            },
            templates: {
                splitContainer: `
                    <div class="video-splitter" style="height:100%">
                        <div class="video-player"></div>
                        <div class="k-list-container playlist-container playlist"><ul class="playlist" class="k-list"></ul></div>
                    </div>
                `
            }
        }

        videoPlayerComponent.data.dataSource = new kendo.data.DataSource({ data: [] });
        var elements = getElementsBySelectorPath(configuration.selectorPath);
        $(elements).data('videoPlayerComponent-config', configuration);
        $(elements).data('videoPlayerComponent-component', videoPlayerComponent);

        if (typeof configuration.viewModel == 'undefined' || configuration.viewModel == null) {
            $.post(configuration.actions.init, { selectorPath: configuration.selectorPath, idItem: configuration.idItem, typeItem: configuration.typeItem })
                .done(function (response) {
                    var selectorPath = response.Result.SelectorPath;
                    var elements = getElementsBySelectorPath(selectorPath);
                    var videoPlayerComponent = $(elements).data('videoPlayerComponent-component');

                    videoPlayerComponent.viewModel = response.Result;
                    videoPlayerComponent.handler.createComponent(videoPlayerComponent);
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        } else {
            var videoPlayerComponent = $(elements).data('videoPlayerComponent-component');
            videoPlayerComponent.handler.createComponent(videoPlayerComponent);
        }
    }
})(jQuery);