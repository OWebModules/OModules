﻿var selectorMediaList;
var dataMediaList;
var mediaListElement;

(function ($) {
    $.fn.createMediaList = function (configuration) {
        dataMediaList = 'media-list-config';
        selectorMediaList = configuration.selector;
        $(this).data(dataMediaList, {
            elements: {
                mediaListContainerElement: null,
                gridContainer: null,
                toolbar: null,
                grid: null,
                gridItem: null,
                uploadWindowContainer: null,
                playerWindowContainer: null,
                dataItem: null
            },
            _itemsPreApply: null,
            configuration: configuration,
            handler: {
                validateReference: function (idObject, validateReferenceCallback) {
                    var oItem = {
                        GUID: idObject
                    };
                    var url = $(selectorMediaList).data(dataMediaList).configuration.viewModel.ActionConfig.ActionValidateReference;
                    var idInstance = $(selectorMediaList).data(dataMediaList).configuration.viewModel.IdInstance;

                    $.post(url, { refItem: oItem, idInstance: idInstance })
                        .done(function (response) {
                            console.log("connected", response);

                            changeValuesFromServerChildren(response.ViewItems, [selectorMediaList]);
                            //if (selectorInternetQuelle != undefined) {
                            //    
                            //}
                            if (validateReferenceCallback !== undefined && validateReferenceCallback !== null) {
                                validateReferenceCallback();
                            }
                            //$(selectorMediaList).data(dataMediaList).elements.gridItem.dataSource.add(mediaListElement);
                            $(selectorMediaList).data(dataMediaList).elements.gridItem.dataSource.read();
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                            $('body').closePageLoader();
                        });
                }, validateSaveFiles: function (fileItems) {
                    var idInstance = $(selectorMediaList).data(dataMediaList).configuration.viewModel.IdInstance;
                    $.post($(selectorMediaList).data(dataMediaList).configuration.viewModel.ActionConfig.ActionSaveFiles, { fileItems: fileItems, idInstance: idInstance })
                        .done(function (response) {
                            console.log("connected", response);
                            if (response.IsSuccessful) {
                                for (var ix in response.Result) {
                                    var mediaListItem = response.Result[ix];

                                    $(selectorMediaList).data(dataMediaList).elements.gridItem.dataSource.add(mediaListItem);
                                }
                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                loadItems: function () {
                    var data = JSON.stringify($(selectorMediaList).data(dataMediaList).elements.gridItem.dataSource.view());
                    $.post($(selectorMediaList).data(dataMediaList).configuration.viewModel.ActionConfig.ActionSaveSessionData, { data: data, idInstance: $(selectorMediaList).data(dataMediaList).configuration.viewModel.IdInstance, extension: "json" })
                        .done(function (response) {
                            console.log("connected", response);
                            if (response.IsSuccessful) {
                                configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                    ChannelId: configuration.channelMediaList,
                                    SenderId: configuration.managerConfig.idEndpoint,
                                    SessionId: configuration.managerConfig.idSession,
                                    UserId: configuration.idUser,
                                    GenericParameterItems: [$(selectorMediaList).data(dataMediaList).configuration.viewModel.IdInstance]
                                });
                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                reloadGrid: function () {
                    $(selectorMediaList).data(dataMediaList).elements.gridItem.dataSource.read();
                },
                gridChangeHandler: function () {
                    var selectedRows = this.select();

                    if (selectedRows.length !== 1) {
                        return;
                    }
                    
                    mediaListElement.elements.dataItemRow = mediaListElement.elements.gridItem.select();
                    mediaListElement.elements.dataItem = mediaListElement.elements.gridItem.dataItem(mediaListElement.elements.gridItem.select());
                }

            }
        });



        var htmlUploadWindowTemplate = `
<div>
<div class="media-list-upload-window">
    <div class="media-list-upload-window-header">
        <span>
            @TITLE@
        </span>
    </div>
    <div style="overflow: hidden;" class="media-list-upload-window-content">
        <iframe class="media-list-upload-window-uploadeiframe" src="about:blank"></iframe>
    </div>
</div>
</div>`

        var htmlPlayerWindowTemplate = `
<div>
    <div class="media-list-player-window">
        <div class="media-list-player-window-header">
            <span>
                @TITLE@
            </span>
        </div>
        <div style="overflow: hidden;" class="media-list-player-window-content">
            <iframe class="media-list-player-window-playeriframe" src="about:blank"></iframe>
        </div>
    </div>
</div>`

        var htmlToolbarTemplate = '<div class="media-list-toolbar"></div>';
        var htmlListGridTemplate = `
<div class="media-list-gridparent" >
    <div class="media-list-grid">

    </div>
</div>`;

        $(this).addClass('media-list');

        var mediaListElement = $(this).data(dataMediaList);

        mediaListElement.elements.mediaListContainerElement = this[0];
        $(mediaListElement.elements.mediaListContainerElement).html('');

        var uploadTitle = getViewItemsValue(mediaListElement.configuration.viewItems, 'labelUpload', 'Content');
        var playerTitle = getViewItemsValue(mediaListElement.configuration.viewItems, 'labelPlayer', 'Content');

        var uploadWindowContainer = $(htmlUploadWindowTemplate.replace("@TITLE@", uploadTitle));
        mediaListElement.elements.uploadWindowContainer = uploadWindowContainer[0];
        mediaListElement.elements.mediaListContainerElement.append(mediaListElement.elements.uploadWindowContainer);

        var playerWindowContainer = $(htmlPlayerWindowTemplate.replace("@TITLE@", playerTitle));
        mediaListElement.elements.playerWindowContainer = playerWindowContainer[0];
        mediaListElement.elements.mediaListContainerElement.append(mediaListElement.elements.playerWindowContainer);

        $(mediaListElement.elements.playerWindowContainer).find('.media-list-player-window').kendoWindow({
            width: '1024px',
            height: '600px',
            title: 'New Item',
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Minimize",
                "Maximize",
                "Close"
            ]
        }).data("kendoWindow").center();


        $(mediaListElement.elements.uploadWindowContainer).find('.media-list-upload-window').kendoWindow({
            width: '1024px',
            height: '600px',
            title: 'New Item',
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Minimize",
                "Maximize",
                "Close"
            ]
        }).data("kendoWindow").center();

        mediaListElement.elements.gridContainer = $('<div class="medialist-grid-container" style="height:100%"></div>');
        $(mediaListElement.elements.mediaListContainerElement).append(mediaListElement.elements.gridContainer);
        mediaListElement.elements.toolbar = $('<div class="medialist-toolbar"></div>');
        $(mediaListElement.elements.gridContainer).append(mediaListElement.elements.toolbar);
        mediaListElement.elements.grid = $('<div class="medialist-grid"></div>');
        $(mediaListElement.elements.gridContainer).append(mediaListElement.elements.grid);


        $(mediaListElement.elements.toolbar).kendoToolBar({
            items: [
                {
                    template: '<button class="uploadItems k-button"><i class="fa fa-plus-square-o" aria-hidden="true"></i></button>',

                },
                {
                    template: '<button class="removeFromList k-button"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>',

                },
                {
                    template: '<button class="applyItem k-button"><i class="fa fa-check-circle" aria-hidden="true"></i></button>',

                },
                {
                    template: '<button class="openPlayer k-button"><i class="fa fa-play-circle-o" aria-hidden="true"></i></button>',

                },
                {
                    template: '<button class="downloadMediaList k-button"><i class="fa fa-download" aria-hidden="true"></i></button>',

                },
                {
                    template: '<label class="nameOItem"></Label>',
                }
            ]
        });

        $(mediaListElement.elements.toolbar).find('.k-button').kendoButton({
            click: function () {
                if ($(this.element).hasClass('applyItem')) {
                    console.log(mediaListElement._itemsPreApply);
                    var appliedOItems = [];
                    for (var ix in mediaListElement._itemsPreApply) {
                        var mediaListItem = mediaListElement._itemsPreApply[ix];

                        appliedOItems.push({
                            GUID: mediaListItem.IdMultimediaItem,
                            Name: mediaListItem.NameMultimediaItem,
                            GUID_Parent: mediaListItem.IdClassMultimediaItem,
                            Type: "Object"
                        });


                    }
                    mediaListElement.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: mediaListElement.configuration.channelAppliedObjects,
                        SenderId: mediaListElement.configuration.managerConfig.idEndpoint,
                        SessionId: mediaListElement.configuration.managerConfig.idSession,
                        UserId: mediaListElement.configuration.idUser,
                        OItems: appliedOItems
                    });
                } else if ($(this.element).hasClass('openPlayer')) {
                    if (mediaListElement.configuration.viewModel.View.IdView === mediaListElement.configuration.viewModel.mediaListPDFViewId) {
                        if (mediaListElement.elements.dataItem !== undefined && mediaListElement.elements.dataItem !== null) {
                            window.open(mediaListElement.elements.dataItem.FileUrl, "_blank");
                        }
                    } else if (mediaListElement.configuration.viewModel.View.IdView == mediaListElement.configuration.viewModel.mediaListAudioViewId) {


                        window.open(mediaListElement.configuration.viewModel.ActionConfig.ActionAudioPlayer + '?Sender=' + mediaListElement.configuration.viewModel.IdInstance, '_blank');
                    } else if (mediaListElement.configuration.viewModel.View.IdView == mediaListElement.configuration.viewModel.mediaListVideoViewId) {

                        window.open(mediaListElement.configuration.viewModel.ActionConfig.ActionVideoPlayer + '?Sender=' + mediaListElement.configuration.viewModel.IdInstance, '_blank');
                    } else if (mediaListElement.configuration.viewModel.View.IdView == mediaListElement.configuration.viewModel.mediaListImagesViewId) {
                        
                        window.open(mediaListElement.configuration.viewModel.ActionConfig.ActionImageViewer + '?Sender=' + mediaListElement.configuration.viewModel.IdInstance, '_blank');
                    }
                } else if ($(this.element).hasClass('uploadItems')) {
                    var uploadWindow = $('.media-list-upload-window').data("kendoWindow");
                    uploadWindow.open();
                    $('.media-list-upload-window-uploadeiframe').attr('src', mediaListElement.configuration.viewModel.ActionConfig.ActionFileUpload + '?Sender=' + mediaListElement.configuration.idParentInstance);
                } else if ($(this.element).hasClass('downloadMediaList')) {
                    var url = mediaListElement.configuration.viewModel.ActionConfig.ActionDownloadMediaList;
                    var sort = mediaListElement.elements.gridItem.dataSource.sort();

                    var sortStr = "";
                    if (sort.length > 0) {
                        if (sort[0].field == "OrderId") {
                            sortStr = "&Sort=";

                            if (sort[0].dir == "asc") {
                                sortStr += "asc";
                            } else {
                                sortStr += "desc";
                            }

                        }
                    }

                    url = url + "?idInstance=" + mediaListElement.configuration.viewModel.IdInstance + sortStr;
                    window.open(url, "_blank");
                }
            }
        });

        $.post(mediaListElement.configuration.actionMediaListPDFInit)
            .done(function (response) {
                mediaListElement.configuration.viewModel = response.Result;
                createStatusBar(mediaListElement.configuration.viewModel.PageInitState);
                $.post(mediaListElement.configuration.viewModel.ActionConfig.ActionInitializeViewItems, { idInstance: response.Result })
                    .done(function (response) {
                        changeValuesFromServerChildren(response.ViewItems, [selectorMediaList]);
                        

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
                
                $.post(mediaListElement.configuration.viewModel.ActionConfig.ActionGetGridConfig)
                    .done(function (response) {

                        $.extend(response.dataSource.transport.read, {
                            data: function () {
                                return { idInstance: mediaListElement.configuration.viewModel.IdInstance };
                            }
                        });

                        $.extend(response.dataSource, {
                            batch: true
                        });

                        var dataSource = new kendo.data.DataSource(response.dataSource);
                        response.dataSource = dataSource;
                        response.dataBound = function () {
                            //$('.chkbxApply').change(viewModel.checkedItemChange);
                            if (mediaListElement.elements.gridItem.dataSource.total() > 0) {
                                //$("#" + viewModel.downloadMediaList.ViewItemId).setEnabled(true, true);
                            } else {
                                //$("#" + viewModel.downloadMediaList.ViewItemId).setEnabled(false, true);
                            }
                        };


                        $.extend(response, {
                            change: mediaListElement.handler.gridChangeHandler,
                            cellClose: function (e) {
                                e.model.IsSaved = false;
                                viewModel.gridItem.saveChanges();
                            }
                        });

                        mediaListElement.elements.gridItem = $(mediaListElement.elements.grid).kendoGrid(response).data("kendoGrid");

                        mediaListElement.elements.gridItem.wrapper.find(".k-pager")
                            .before('<a href="#" class="k-link prev">Prev</a>')
                            .after('<a href="#" class="k-link next">Next</a>')
                            .parent()
                            .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                            .delegate("a.prev", "click", function () {
                                dataSource.page(dataSource.page() - 1);
                            })
                            .delegate("a.next", "click", function () {
                                dataSource.page(dataSource.page() + 1);
                            })
                            .delegate(".pagesize input", "change", function () {
                                dataSource.page(parseInt(this.value, 10));
                            })

                        //viewModel.reloadGrid();
                        mediaListElement.configuration.createdCallback(mediaListElement);


                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
        
        
    };
})(jQuery);