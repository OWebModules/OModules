﻿var imageMapContainer;
(function ($) {
    $.fn.createImageMapComponent = function (configuration) {
        $('body').openPageLoader();
        $(this).data("image-map-config", {
            elements: {
                imageMapContainerElement: null,
                imageMapElement: null
            },
            configuration: configuration,
            handler: {
                validateReference: function (oItem) {
                    $('body').openPageLoader();
                    imageMapContainer.configuration.viewModel.oItem = oItem;
                    imageMapContainer.handler.createControl(imageMapContainer.elements.imageMapContainerElement);
                    $.post(imageMapContainer.configuration.viewModel.ActionValidateReference, { refItem: oItem, idInstance: imageMapContainer.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            console.log("connected", response);
                            changeValuesFromServerChildren(response.ViewItems, [".image-map"]);
                            var imageMap = response.Result;
                            $(imageMapContainer.elements.imageMapContainerElement).html('');
                            viewModel.sendInterServiceMessage = function (idObject) {
                                imageMapContainer.configuration.viewModel.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                    ChannelId: imageMapContainer.configuration.viewModel.ChannelSelectedObject,
                                    SenderId: imageMapContainer.configuration.viewModel.managerConfig.idEndpoint,
                                    SessionId: imageMapContainer.configuration.viewModel.managerConfig.idSession,
                                    UserId: imageMapContainer.configuration.viewModel.managerConfig.idUser,
                                    OItems: [
                                        {
                                            GUID: idObject
                                        }]
                                });
                            }
                            var containerHtml = componentTemplate.replace("@SRC@", imageMap.ImageUrl).replace("@ALT@", imageMap.NameImageMap).replace("@MAP@", imageMap.NameImageMap).replace("@MAP@", imageMap.NameImageMap);
                            var areaHtml = '';
                            for (var ix in imageMap.ImageAreas) {
                                var imageArea = imageMap.ImageAreas[ix];
                                var areaHtml = areaHtml + areaTemplate.replace("@SHAPE@", imageArea.NameShape).replace("@COORDS@", imageArea.Coords).replace("@ALT@", imageArea.NameImageArea).replace("@ID_OBJECT@", imageArea.IdObject).replace("@ID_OBJECT@", imageArea.IdObject).replace("@HREF@", imageArea.NameImageArea);
                            }

                            containerHtml = containerHtml.replace("@AREAS@", areaHtml);

                            imageMapContainer.elements.imageMapElement = $(containerHtml);
                            $(imageMapContainer.elements.imageMapContainerElement).append(imageMapContainer.elements.imageMapElement);
                            
                            
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                            $('body').closePageLoader();
                        });
                },
                createControl: function (element) {
                    $('body').openPageLoader();
                    imageMapContainer.elements.imageMapContainerElement = $(element);
                    $(element).html('');

                },
                clickedButton: function (element) {

                }
            }
        });
        $(this).addClass('image-map');

        imageMapContainer = $(this).data("image-map-config");

        if (imageMapContainer.configuration.viewModel == undefined || imageMapContainer.configuration.viewModel == null) {
            $.post(imageMapContainer.configuration.ActionImageMapInit)
                .done(function (response) {
                    imageMapContainer.configuration.viewModel = response.Result;
                    changeValuesFromServerChildren(imageMapContainer.viewModel.ViewItems, [".image-map"]);
                    imageMapContainer.handler.createControl(this);
                    if (imageMapContainer.configuration.events !== undefined && imageMapContainer.configuration.events !== null &&
                        imageMapContainer.configuration.events.createCompleted !== undefined && imageMapContainer.configuration.events.createCompleted !== null) {
                        imageMapContainer.configuration.events.createCompleted(imageMapContainer);
                    }
                    $('body').closePageLoader();

                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        } else {
            imageMapContainer.handler.createControl(this);
            imageMapContainer.configuration.events.createCompleted(imageMapContainer);
            $('body').closePageLoader();
        }

    };

})(jQuery);

var componentTemplate = `
<img src="@SRC@" alt="@ALT@" usemap="#@MAP@">

<map name="@MAP@">
  @AREAS@
</map>
`

var areaTemplate = `
<area shape="@SHAPE@" coords="@COORDS@" alt="@ALT@" data-id-object="@ID_OBJECT@" href="javascript:viewModel.sendInterServiceMessage('@ID_OBJECT@');">
`
