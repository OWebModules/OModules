﻿function pauseVideoByBookmark(element) {
    var handler = $(element).createOrGetMediaBookmarkHandler();
    handler.stage = 'pause';
}

function listenForReferenceByBookmark(element) {
    var bookmarkItem = $(element).data('bookmark-item');
    var mediaBookmarkManagerComponent = $(element).data('mediaBookmarkManager-component');
    var bookmarkHandler = $(element).createOrGetMediaBookmarkHandler(bookmarkItem, mediaBookmarkManagerComponent);
    if (bookmarkHandler.listenOrOpen) {
        $(element).toggleMode(true);
    }
    else {
        bookmarkHandler.mediaBookmarkManagerComponent.data.playerIsPaused = bookmarkHandler.mediaBookmarkManagerComponent.components.videoPlayerComponent.handler.isPaused(bookmarkHandler.mediaBookmarkManagerComponent.components.videoPlayerComponent);
        if (!mediaBookmarkManagerComponent.data.playerIsPaused) {
            bookmarkHandler.mediaBookmarkManagerComponent.components.videoPlayerComponent.handler.pause(bookmarkHandler.mediaBookmarkManagerComponent.components.videoPlayerComponent);
        }
        bookmarkHandler.mediaBookmarkManagerComponent.components.classObjectSelector.open();
    }
}

function showReferences(element) {
    var bookmarkItem = $(element).closest('.bookmarks');
    var mediaBookmarkManagerComponent = $(element).closest('.media-bookmark-manager').data('mediaBookmarkManager-component');
    mediaBookmarkManagerComponent.elements.selectedMediaBookmark = element;
    var dataItem = mediaBookmarkManagerComponent.components.listView.dataItem(bookmarkItem);
    mediaBookmarkManagerComponent.handler.addReferenceDetail(mediaBookmarkManagerComponent, dataItem);
    mediaBookmarkManagerComponent.handler.toggleReferenceWindow(mediaBookmarkManagerComponent, bookmarkItem);

    $(mediaBookmarkManagerComponent.elements.selectedMediaBookmark).data('bookmark-item', bookmarkItem);
    $(mediaBookmarkManagerComponent.elements.selectedMediaBookmark).data('mediaBookmarkManager-component', mediaBookmarkManagerComponent);
    
}

function removeReference(element, idBookmark, idReference) {
    var mediaBookmarkManagerComponent = $(element).data('mediaBookmarkManager-component');
    $.post(mediaBookmarkManagerComponent.viewModel.ActionRemoveReference, { idMediaBookmark: idBookmark, idReference: idReference, idInstance: mediaBookmarkManagerComponent.viewModel.IdInstance })
        .done(function (response) {
            if (response.IsSuccessful) {
                var element = getElementsBySelectorPath(response.SelectorPath);
                var mediaBookmarkManagerComponent = $(element).data('mediaBookmarkManager-component');
                var bookMarks = jQuery.grep(mediaBookmarkManagerComponent.data.dataSource.data(), function (element, ix) {
                    return element.IdBookmark == response.Result.GUID_Related;
                });

                if (bookMarks.length > 0) {
                    var bookMark = bookMarks[0];
                    bookMark.References = jQuery.grep(bookMark.References, function (referenceItem, ixReferenceItem) {
                        return (referenceItem.GUID != response.Result.GUID)
                    });
                    mediaBookmarkManagerComponent.data.dataSource.sort({ field: "Second", dir: "asc" });
                    mediaBookmarkManagerComponent.handler.addReferenceDetail(mediaBookmarkManagerComponent, bookMark);
                }
            }
            else {
                $('.execution-log').html(response.Message);
            }
        })
        .fail(function (jqxhr, textStatus, error) {
            // stuff
        });
}

function sendReference(element, idReference) {
    var mediaBookmarkManagerComponent = $(element).data('mediaBookmarkManager-component');
    mediaBookmarkManagerComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
        ChannelId: mediaBookmarkManagerComponent.viewModel.ChannelSelectedObject,
        SenderId: mediaBookmarkManagerComponent.configuration.managerConfig.idEndpoint,
        SessionId: mediaBookmarkManagerComponent.configuration.managerConfig.idSession,
        UserId: mediaBookmarkManagerComponent.viewModel.IdUser,
        ViewId: mediaBookmarkManagerComponent.viewModel.View.IdView,
        OItems: [
            {
                GUID: idReference,
            }
        ]
    });
}

function toggleListenBookmark(element, reset) {
    var bookmarkItem = $(element).data('bookmark-item');
    var mediaBookmarkManagerComponent = $(element).data('mediaBookmarkManager-component');
    var bookmarkHandler = $(bookmarkItem).createOrGetMediaBookmarkHandler(bookmarkItem, mediaBookmarkManagerComponent);

    if (typeof reset !== 'undefined' && reset) {
        $(element).html('<i class="fa fa-folder-open-o" aria-hidden="true"></i>');
        bookmarkHandler.listenOrOpen = true;
        return;
    }
    if (event.altKey) {
        $(element).html('<i class="fa fa-assistive-listening-systems" aria-hidden="true"></i>');
        bookmarkHandler.listenOrOpen = true;
    } else {
        $(element).html('<i class="fa fa-folder-open-o" aria-hidden="true"></i>');
        bookmarkHandler.listenOrOpen = false;
    }

}

function removeMediaBookmark(element) {

    kendo.confirm("Are you sure that you want to delete the bookmark?").then(function () {
        var bookmarkItem = $(element).closest('.bookmarks');
        var mediaBookmarkManagerComponent = $(element).closest('.media-bookmark-manager').data('mediaBookmarkManager-component');
        var dataItem = mediaBookmarkManagerComponent.components.listView.dataItem(bookmarkItem);

        $.post(mediaBookmarkManagerComponent.viewModel.ActionDeleteMediaBookmark, { idMediaBookmark: dataItem.IdBookmark, idInstance: mediaBookmarkManagerComponent.viewModel.IdInstance })
            .done(function (response) {
                if (response.IsSuccessful) {
                    mediaBookmarkManagerComponent.data.dataSource.remove(dataItem);
                }
                else {
                    $('.execution-log').html(response.Message);
                }
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    }, function () {
        
    });
}

(function ($) {
    $.fn.createOrGetMediaBookmarkHandler = function (bookmarkItem, mediaBookmarkManagerComponent) {
        if (typeof bookmarkItem === 'undefined') {
            bookmarkItem = $(this).closest('.bookmarks');
        }
        if (typeof mediaBookmarkManagerComponent === 'undefined') {
            mediaBookmarkManagerComponent = $(this).closest('.media-bookmark-manager').data('mediaBookmarkManager-component');
        }
        var bookmarkHandler = $(bookmarkItem).data('action-handler');
        if (typeof bookmarkHandler === 'undefined') {
            var dataItem = mediaBookmarkManagerComponent.components.listView.dataItem(bookmarkItem);
            bookmarkHandler = {
                dataItem: dataItem,
                mediaBookmarkManagerComponent: mediaBookmarkManagerComponent,
                stage: '',
                listenOrOpen: true
            };
            $(bookmarkItem).data('action-handler', bookmarkHandler);
        }

        return bookmarkHandler;
    }
    $.fn.createMediaBookmarkManagerVideo = function (configuration) {
        var mediaBookmarkManagerComponent = {
            configuration: configuration,
            elements: {
            },
            data: {
            },
            components: {
            },
            handler: {
                createComponent: function (mediaBookmarkManagerComponent) {

                    if (mediaBookmarkManagerComponent.viewModel.PageInitState.IsError) return;
                    var elements = getElementsBySelectorPath(mediaBookmarkManagerComponent.configuration.selectorPath);
                    var dataSource = new kendo.data.DataSource([]);
                    mediaBookmarkManagerComponent.data.dataSource = dataSource;
                    mediaBookmarkManagerComponent.elements.listView = $(mediaBookmarkManagerComponent.templates.listViewTemplate);
                    $(elements).append(mediaBookmarkManagerComponent.elements.listView);

                    $(mediaBookmarkManagerComponent.elements.listView).kendoListView({
                        dataSource: dataSource,
                        selectable: "single",
                        scrollable: "endless",
                        height: "100%",
                        dataBound: function (e) {
                            if (e.action == "rebind") {
                                for (var ix = 0; ix < e.items.length; ix++) {
                                    var item = e.items[ix];
                                    $("#" + item.IdBookmark).find(".reference").find(".badge").html(item.References.length);
                                }
                                
                            }
                        },
                        change: function (e) {
                            var selectedItems = e.sender.select();

                            var bookmarkHandler = $(selectedItems).createOrGetMediaBookmarkHandler();
                            
                            var stage = bookmarkHandler.stage;

                            if (!bookmarkHandler.mediaBookmarkManagerComponent.data.noSelect) {
                                bookmarkHandler.mediaBookmarkManagerComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                    ChannelId: bookmarkHandler.mediaBookmarkManagerComponent.viewModel.ChannelSelectedObject,
                                    SenderId: bookmarkHandler.mediaBookmarkManagerComponent.configuration.managerConfig.idEndpoint,
                                    SessionId: bookmarkHandler.mediaBookmarkManagerComponent.configuration.managerConfig.idSession,
                                    UserId: bookmarkHandler.mediaBookmarkManagerComponent.viewModel.IdUser,
                                    ViewId: bookmarkHandler.mediaBookmarkManagerComponent.viewModel.View.IdView,
                                    OItems: [
                                        {
                                            GUID: bookmarkHandler.dataItem.IdBookmark,
                                            Name: bookmarkHandler.dataItem.NameBookmark
                                        }
                                    ]
                                });
                            }
                            
                            if (stage === '') {
                                bookmarkHandler.mediaBookmarkManagerComponent.components.videoPlayerComponent.handler.seek(bookmarkHandler.mediaBookmarkManagerComponent.components.videoPlayerComponent, bookmarkHandler.dataItem.Second, false);
                            } else if (stage === 'pause') {
                                bookmarkHandler.mediaBookmarkManagerComponent.components.videoPlayerComponent.handler.seek(bookmarkHandler.mediaBookmarkManagerComponent.components.videoPlayerComponent, bookmarkHandler.dataItem.Second, true);   
                            } else if (stage === 'remove') {

                            }
                            bookmarkHandler.stage = '';
                        },
                        template: kendo.template($("#templateBookmarkList").html()),
                        filterable: {
                            field: "NameBookmark" //filtering for "text" field
                        }
                    });

                    var windowOptions = {
                        draggable: true,
                        resizable: false,
                        width: "600px",
                        height: "400px",
                        title: "References",
                        visible: false,
                        close: function () {
                        }
                    };

                    mediaBookmarkManagerComponent.elements.listViewDetail = $(mediaBookmarkManagerComponent.templates.listViewDetailTemplate);
                    $(elements).append(mediaBookmarkManagerComponent.elements.listViewDetail);
                    $(mediaBookmarkManagerComponent.elements.listViewDetail).kendoWindow(windowOptions);
                    mediaBookmarkManagerComponent.components.listViewDetail = $(mediaBookmarkManagerComponent.elements.listViewDetail).data('kendoWindow');

                    mediaBookmarkManagerComponent.components.listView = $(mediaBookmarkManagerComponent.elements.listView).data("kendoListView");
                    mediaBookmarkManagerComponent.elements.classObjectSelectorWindow = $(mediaBookmarkManagerComponent.templates.classObjectSelectorWindowTemplate);
                    $(elements).append(mediaBookmarkManagerComponent.elements.classObjectSelectorWindow);
                    $($(elements).find('.class-object-selector-window')).kendoWindow({
                        width: '1024px',
                        height: '600px',
                        title: 'New Item',
                        visible: false,
                        modal: true,
                        actions: [
                            "Pin",
                            "Minimize",
                            "Maximize",
                            "Close"
                        ]
                    }).data("kendoWindow").center();

                    mediaBookmarkManagerComponent.elements.classObjectSelector = $(mediaBookmarkManagerComponent.templates.classObjectSelectorTemplate);
                    $('.class-object-selector-window').append(mediaBookmarkManagerComponent.elements.classObjectSelector);

                    mediaBookmarkManagerComponent.components.classObjectSelector = $('.class-object-selector-window').data('kendoWindow');

                    $(mediaBookmarkManagerComponent.elements.classObjectSelector).find('.class-tree-container').data('mediaBookmarkManager-component', mediaBookmarkManagerComponent);
                    var selectorPath = ["#idSplitterOModules", ".class-tree-container"];

                    var configuration = {
                        setDocumentTitle: false,
                        actionInit: viewModel.ActionInitClassTree,
                        selectorPath: selectorPath,
                        managerConfig: viewModel.managerConfig,
                        events: {
                            selectedClass: function (idClass, nameClass, classTreeComponent) {
                                var element = getElementsBySelectorPath(classTreeComponent.configuration.viewModel.SelectorPath);
                                var mediaBookmarkManagerComponent = $(element).data('mediaBookmarkManager-component');
                                mediaBookmarkManagerComponent.components.objectListComponent.handler.setIdParent(idClass);
                            },
                            createdComponent: function (classTreeComponent) {
                                var element = getElementsBySelectorPath(classTreeComponent.configuration.viewModel.SelectorPath);
                                var mediaBookmarkManagerComponent = $(element).data('mediaBookmarkManager-component');
                                mediaBookmarkManagerComponent.components.classTreeComponent = classTreeComponent;
                            }
                        }
                    }
                    $('#idSplitterOModules').find('.class-tree-container').createClassTree(configuration);

                    var configurationObjectList = {
                        parentComponent: mediaBookmarkManagerComponent,
                        actionObjectListInit: viewModel.ActionObjectListLinit,
                        templateSelector: "#templateObjectList",
                        managerConfig: viewModel.managerConfig,
                        useIdParent: true,
                        handler: {
                            initCompleteHandler: function (objectListComponent, parentComponent) {
                                parentComponent.components.objectListComponent = objectListComponent;
                            }
                        },
                        events: {
                            selectedObject: function (items, parentComponent) {

                            },
                            applyItems: function (items, parentComponents) {
                                idObjects = [];
                                for (var ix in items) {
                                    idObjects.push(items[ix].GUID);
                                }
                                var selected = parentComponents.components.listView.select();
                                var dataItem = parentComponents.components.listView.dataItem(selected);

                                mediaBookmarkManagerComponent.components.classObjectSelector.close();

                                $.post(mediaBookmarkManagerComponent.viewModel.ActionRelateBookmark, { idMediaBookmark: dataItem.IdBookmark, idObjects: idObjects, idInstance: mediaBookmarkManagerComponent.viewModel.IdInstance })
                                    .done(function (response) {
                                        var bookMarks = jQuery.grep(mediaBookmarkManagerComponent.data.dataSource.data(), function (element, ix) {
                                            return element.IdBookmark == response.Result.IdBookmark;
                                        });
                                        if (bookMarks.length > 0) {
                                            var bookmark = bookMarks[0];
                                            bookmark.set("References", response.Result.References);
                                            mediaBookmarkManagerComponent.handler.addReferenceDetail(mediaBookmarkManagerComponent, bookmark);
                                            if (!mediaBookmarkManagerComponent.data.playerIsPaused) {
                                                mediaBookmarkManagerComponent.components.videoPlayerComponent.handler.play(mediaBookmarkManagerComponent.components.videoPlayerComponent);
                                                mediaBookmarkManagerComponent.data.playerIsPaused = false;
                                            }
                                        }
                                    })
                                    .fail(function (jqxhr, textStatus, error) {
                                        // stuff
                                    });
                            }
                        }
                    };

                    $("#idSplitterOModules")
                        .kendoSplitter({
                            orientation: "horizontal",
                            panes: [
                                { size: "25%" },
                                { collapsible: false }
                            ]
                        });
                    $('.object-list-container').createObjectList(configurationObjectList);

                    if (typeof mediaBookmarkManagerComponent.configuration.events !== 'undefined' && mediaBookmarkManagerComponent.configuration.events !== null &&
                        typeof mediaBookmarkManagerComponent.configuration.events.completed !== 'undefined' && mediaBookmarkManagerComponent.configuration.events.completed !== null) {
                        mediaBookmarkManagerComponent.configuration.events.completed(mediaBookmarkManagerComponent);
                    }
                },
                validateReference: function (mediaBookmarkManagerComponent, idMediaItem, idReference) {
                    mediaBookmarkManagerComponent.data.idMediaItem = idMediaItem;
                    $.post(mediaBookmarkManagerComponent.viewModel.ActionValidateReference, { idMediaItem: idMediaItem, idReference: idReference, idInstance: mediaBookmarkManagerComponent.viewModel.IdInstance })
                        .done(function (response) {

                            var selectorPath = response.SelectorPath;
                            var elements = getElementsBySelectorPath(selectorPath);
                            var mediaBookmarkManagerComponent = $(elements).data('mediaBookmarkManager-component');
                            var dataSource = new kendo.data.DataSource([]);
                            mediaBookmarkManagerComponent.data.dataSource = dataSource;
                            mediaBookmarkManagerComponent.components.listView.setDataSource(mediaBookmarkManagerComponent.data.dataSource);
                            var bookmarks = response.Result;
                            for (var i = 0; i < bookmarks.length; i++) {
                                var bookMark = bookmarks[i];
                                mediaBookmarkManagerComponent.data.dataSource.add(bookMark);
                                mediaBookmarkManagerComponent.data.dataSource.sort({ field: "Second", dir: "asc" });
                            }
                            var overlayWindow = mediaBookmarkManagerComponent.elements.overlayWindow.data("kendoWindow");
                            overlayWindow.open();
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                    if (typeof mediaBookmarkManagerComponent.configuration.events !== 'undefined' && mediaBookmarkManagerComponent.configuration.events !== null &&
                        typeof mediaBookmarkManagerComponent.configuration.events.validatedReference !== 'undefined' && mediaBookmarkManagerComponent.configuration.events.validatedReference !== null) {
                        mediaBookmarkManagerComponent.configuration.events.validatedReference(mediaBookmarkManagerComponent);
                    }


                },
                checkBookmarks: function (mediaBookmarkManagerComponent, oItems) {
                    if (oItems.length != 1) return;

                    var oItem = oItems[0];

                    var bookMarks = jQuery.grep(mediaBookmarkManagerComponent.data.dataSource.data(), function (element, ix) {
                        return element.IdBookmark == oItem.GUID;
                    })

                    if (bookMarks.length > 0) {
                        var bookmark = bookMarks[0];
                        var elements = $('#' + bookmark.IdBookmark);
                        if (elements.length > 0) {
                            mediaBookmarkManagerComponent.data.noSelect = true;
                            mediaBookmarkManagerComponent.components.listView.select(elements[0]);
                        }
                    }
                },
                createOverlay: function (mediaBookmarkManagerComponent, videoSelectorPath) {
                    var elements = getElementsBySelectorPath(videoSelectorPath);
                    var videoPlayerComponent = $(elements).data('videoPlayerComponent-component');
                    mediaBookmarkManagerComponent.elements.overlayWindow = $(mediaBookmarkManagerComponent.templates.videoOverlayTemplate);
                    $(elements).append(mediaBookmarkManagerComponent.elements.overlayWindow);

                    var windowOptions = {
                        draggable: true,
                        resizable: false,
                        width: "200px",
                        height: "70px",
                        title: "Bookmark",
                        visible: false,
                        close: function () {
                        }
                    };

                    $(mediaBookmarkManagerComponent.elements.overlayWindow).kendoWindow(windowOptions);
                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-bookmark').data('videoPlayerComponent-component', videoPlayerComponent);
                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-bookmark').data('mediaBookmarkManager-component', mediaBookmarkManagerComponent);
                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-backward').data('videoPlayerComponent-component', videoPlayerComponent);
                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-backward').data('mediaBookmarkManager-component', mediaBookmarkManagerComponent);
                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-play').data('videoPlayerComponent-component', videoPlayerComponent);
                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-play').data('mediaBookmarkManager-component', mediaBookmarkManagerComponent);
                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-pause').data('videoPlayerComponent-component', videoPlayerComponent);
                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-pause').data('mediaBookmarkManager-component', mediaBookmarkManagerComponent);
                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-forward').data('videoPlayerComponent-component', videoPlayerComponent);
                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-forward').data('mediaBookmarkManager-component', mediaBookmarkManagerComponent);
                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-send').data('videoPlayerComponent-component', videoPlayerComponent);
                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-send').data('mediaBookmarkManager-component', mediaBookmarkManagerComponent);
                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-bookmark').on('click', function () {
                        var videoPlayerComponent = $(this).data('videoPlayerComponent-component');
                        var videoPlayer = videoPlayerComponent.elements.videoPlayerElement;
                        var mediaBookmarkManagerComponent = $(this).data('mediaBookmarkManager-component');
                        var mediItem = $(videoPlayer).data("media-item");
                        var name = "pos: " + videoPlayerComponent.data.currentTime;
                        var request = {
                            nameBookmark: name,
                            pos: videoPlayerComponent.data.currentTime.toString(),
                            idMultimediaItem: mediItem.IdMultimediaItem,
                            idInstance: mediaBookmarkManagerComponent.viewModel.IdInstance,
                            logState: mediaBookmarkManagerComponent.viewModel.logstatePosition
                        }
                        mediaBookmarkManagerComponent.data.playerIsPaused = mediaBookmarkManagerComponent.components.videoPlayerComponent.handler.isPaused(mediaBookmarkManagerComponent.components.videoPlayerComponent);
                        if (!mediaBookmarkManagerComponent.data.playerIsPaused) {
                            mediaBookmarkManagerComponent.components.videoPlayerComponent.handler.pause(mediaBookmarkManagerComponent.components.videoPlayerComponent);
                        }

                        $.post(mediaBookmarkManagerComponent.viewModel.ActionCreateMediaBookmark, { request: request })
                            .done(function (response) {
                                if (response.IsSuccessful) {
                                    var selectorPath = response.SelectorPath;
                                    var elements = getElementsBySelectorPath(selectorPath);
                                    var mediaBookmarkManagerComponent = $(elements).data('mediaBookmarkManager-component');
                                    for (var ix = 0; ix < response.Result.References.length; ix++) {
                                        var reference = dataItem.References[ix];
                                        mediaBookmarkManagerComponent.handler.addReferenceRow(mediaBookmarkManagerComponent, response.Result, reference);
                                    }
                                    mediaBookmarkManagerComponent.data.dataSource.add(response.Result);
                                    mediaBookmarkManagerComponent.data.dataSource.sort({ field: "Second", dir: "asc" });
                                    if (!mediaBookmarkManagerComponent.data.playerIsPaused) {
                                        mediaBookmarkManagerComponent.components.videoPlayerComponent.handler.play(mediaBookmarkManagerComponent.components.videoPlayerComponent);
                                        mediaBookmarkManagerComponent.data.playerIsPaused = false;
                                    }
                                }
                                else {
                                    $('.execution-log').html(response.Message);
                                }
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });

                    });

                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-backward').on('click', function () {
                        var videoPlayerComponent = $(this).data('videoPlayerComponent-component');
                        var newPos = 0;
                        if (videoPlayerComponent.data.currentTime > 5) {
                            newPos = videoPlayerComponent.data.currentTime - 5;
                        }
                        var isPaused = videoPlayerComponent.handler.isPaused(videoPlayerComponent);

                        videoPlayerComponent.handler.seek(videoPlayerComponent, newPos, isPaused);

                    });

                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-play').on('click', function () {
                        var videoPlayerComponent = $(this).data('videoPlayerComponent-component');
                        var isPaused = videoPlayerComponent.handler.isPaused(videoPlayerComponent);

                        if (isPaused) {
                            videoPlayerComponent.handler.play(videoPlayerComponent);
                        }


                    });

                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-pause').on('click', function () {
                        var videoPlayerComponent = $(this).data('videoPlayerComponent-component');
                        var isPaused = videoPlayerComponent.handler.isPaused(videoPlayerComponent);

                        if (!isPaused) {
                            videoPlayerComponent.handler.pause(videoPlayerComponent);
                        }


                    });

                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-forward').on('click', function () {
                        var videoPlayerComponent = $(this).data('videoPlayerComponent-component');

                        var newPos = videoPlayerComponent.data.currentTime + 5;

                        var isPaused = videoPlayerComponent.handler.isPaused(videoPlayerComponent);

                        videoPlayerComponent.handler.seek(videoPlayerComponent, newPos, isPaused);

                    });

                    $(mediaBookmarkManagerComponent.elements.overlayWindow).find('.btn-send').on('click', function () {
                        var mediaBookmarkComponent = $(this).data('mediaBookmarkManager-component');

                        var oItems = [
                            {
                                GUID: mediaBookmarkComponent.data.idMediaItem,
                                Type: "Object"
                            }
                        ];

                        mediaBookmarkComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                            ChannelId: mediaBookmarkComponent.viewModel.ChannelSelectedObject,
                            SenderId: mediaBookmarkComponent.configuration.managerConfig.idEndpoint,
                            SessionId: mediaBookmarkComponent.configuration.managerConfig.idSession,
                            UserId: mediaBookmarkComponent.viewModel.IdUser,
                            ViewId: mediaBookmarkComponent.viewModel.View.IdView,
                            OItems: oItems
                        });

                    });

                    $(document).keydown(function (e) {
                        var videoPlayerComponent = $('.btn-play').data('videoPlayerComponent-component');

                        switch (e.keyCode) {
                            case 37:
                                var newPos = 0;
                                if (videoPlayerComponent.data.currentTime > 5) {
                                    newPos = videoPlayerComponent.data.currentTime - 5;
                                }
                                var isPaused = videoPlayerComponent.handler.isPaused(videoPlayerComponent);

                                videoPlayerComponent.handler.seek(videoPlayerComponent, newPos, isPaused);
                                break;
                            case 39:
                                var newPos = videoPlayerComponent.data.currentTime + 5;

                                var isPaused = videoPlayerComponent.handler.isPaused(videoPlayerComponent);

                                videoPlayerComponent.handler.seek(videoPlayerComponent, newPos, isPaused);
                                break;
                        }
                    });


                },
                setVideoPlayerComponent: function (mediaBookmarkManagerComponent, videoPlayerComponent) {
                    mediaBookmarkManagerComponent.components.videoPlayerComponent = videoPlayerComponent;
                },
                selectBookmarkByReference: function (mediaBookmarkManagerComponent, idReference) {
                    var dataItems = mediaBookmarkManagerComponent.components.listView.dataItems();
                    var bookMarks = jQuery.grep(dataItems, function (dataItem, iDataItem) {
                        var reference = jQuery.grep(dataItem.References, function (reference, iRef) {
                            return (reference.GUID == idReference);
                        })

                        return (reference.length == 1);
                    });

                    if (bookMarks.length > 0) {
                        var idBookmark = bookMarks[0].IdBookmark;
                        var elements = $('#' + idBookmark);
                        if (elements.length > 0) {
                            mediaBookmarkManagerComponent.components.listView.select(elements[0]);
                        }
                    }

                    return (bookMarks.length > 0);
                },
                addReferenceDetail: function (mediaBookmarkManagerComponent, bookMark) {
                    var bookmarkItem = $(mediaBookmarkManagerComponent.elements.selectedMediaBookmark).closest('.bookmarks');
                    mediaBookmarkManagerComponent.elements.referenceDetail = $(mediaBookmarkManagerComponent.templates.referenceTemplate);
                    $(mediaBookmarkManagerComponent.elements.listViewDetail).html("");
                    $(mediaBookmarkManagerComponent.elements.listViewDetail).append(mediaBookmarkManagerComponent.elements.referenceDetail);
                    $(mediaBookmarkManagerComponent.elements.referenceDetail).find('.listen').data('bookmark-item', bookmarkItem);
                    $(mediaBookmarkManagerComponent.elements.referenceDetail).find('.listen').data('mediaBookmarkManager-component', mediaBookmarkManagerComponent);


                    for (var ix = 0; ix < bookMark.References.length; ix++) {
                        var reference = bookMark.References[ix];
                        mediaBookmarkManagerComponent.handler.addReferenceRow(mediaBookmarkManagerComponent, bookMark, reference);
                    }
                },
                addReferenceRow: function (mediaBookmarkManagerComponent, bookmark, reference) {
                    var template = mediaBookmarkManagerComponent.templates.templateReferenceRow;
                    template = template.replace(/@REFERENCE_NAME@/g, reference.Name);
                    template = template.replace(/@ID_BOOKMARK@/g, bookmark.IdBookmark);
                    template = template.replace(/@ID_REFERENCE@/g, reference.GUID);
                    var referenceRow = $(template);
                    $(mediaBookmarkManagerComponent.elements.referenceDetail).find('.reference-table').append(referenceRow);
                    $(referenceRow).find('.remove-reference').data('mediaBookmarkManager-component', mediaBookmarkManagerComponent);
                    $(referenceRow).find('.send-reference').data('mediaBookmarkManager-component', mediaBookmarkManagerComponent);
                    mediaBookmarkManagerComponent.data.dataSource.sort({ field: "Second", dir: "asc" });
                },
                toggleReferenceWindow: function (mediaBookmarkManagerComponent, bookmarkItem) {
                    mediaBookmarkManagerComponent.components.listViewDetail.center();
                    if (mediaBookmarkManagerComponent.components.listViewDetail.element.is(":hidden")) {
                        mediaBookmarkManagerComponent.components.listViewDetail.open();
                    } else {
                        mediaBookmarkManagerComponent.components.listViewDetail.close();
                    }
                    var handler = $(mediaBookmarkManagerComponent.elements.selectedMediaBookmark).createOrGetMediaBookmarkHandler(bookmarkItem, mediaBookmarkManagerComponent);
                    handler.stage = 'pause';
                }
            },
            templates: {
                listViewTemplate: `
                    <div class="list-view"></div>
                `,
                listViewDetailTemplate: `
                    <div class="list-view-detail"></div>
                `,
                pagerTemplate: `
                    <div class="k-pager-wrap pager"></div>
                `,
                videoOverlayTemplate: `
                    <div class="video-overlay" style="positiion: absolute;">
                        <div class="overlay-window">
                            <div style="text-align: center;">
                                <div class="btn-group" role="group" aria-label="Navigation group">
                                    <button class="btn btn-primary btn-xs btn-backward"><i class="fa fa-undo" aria-hidden="true"></i> -5</button>
                                    <button class="btn btn-primary btn-xs btn-play"><i class="fa fa-play" aria-hidden="true"></i></button>
                                    <button class="btn btn-primary btn-xs btn-pause"><i class="fa fa-pause" aria-hidden="true"></i></button>
                                    <button class="btn btn-primary btn-xs btn-forward">+5 <i class="fa fa-repeat" aria-hidden="true"></i></button>
                                </div>
                                <div class="btn-group" role="group" aria-label="Bookmark group">
                                    <button class="btn btn-primary btn-xs btn-bookmark"><i class="fa fa-bookmark" aria-hidden="true"></i></button>
                                </div>
                                <div class="btn-group" role="group" aria-label="Bookmark group">
                                    <button class="btn btn-primary btn-xs btn-send"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                `,
                classObjectSelectorWindowTemplate: `
                <div class="class-object-selector">
                    <div class="class-object-selector-window"></div
                </div>
                `,
                classObjectSelectorTemplate: `
                <div id="idSplitterOModules">
                    <div>
                        <div class="class-tree-container">
                        </div>
                    </div>
                    <div>
                        <div class="object-list-container">
                            <div class="object-list"></div>
                        </div>
                    </div>
                </div>
                `,
                referenceTemplate: `
                <div>
                    <button class="listen k-button" onClick="javascript:listenForReferenceByBookmark(this);" onmouseenter="javascript:toggleListenBookmark(this);" onmouseleave="javascript:toggleListenBookmark(this, true);"><i class="fa fa-folder-open-o" aria-hidden="true"></i></button>
                    <table class="reference-table">

                    </table>
                </div>
                `,
                templateReferenceRow: `
                <tr class="@ID_REFERENCE@">
                    <td>@REFERENCE_NAME@</td>
                    <td><button class="btn btn-xs btn-danger remove-reference" onclick="javascript:removeReference(this, '@ID_BOOKMARK@','@ID_REFERENCE@')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                    <td><button class="btn btn-xs btn-danger send-reference" onclick="javascript:sendReference(this,'@ID_REFERENCE@')"><i class="fa fa-paper-plane" aria-hidden="true"></i></button></td>
                </tr>
                `
            }
        }
        var elements = getElementsBySelectorPath(configuration.selectorPath);
        $(elements).data('mediaBookmarkManager-config', configuration);
        $(elements).data('mediaBookmarkManager-component', mediaBookmarkManagerComponent);

        $.post(configuration.actions.init, { selectorPath: configuration.selectorPath, idMediaItem: configuration.idItem, typeItem: configuration.typeItem, idViewModel: configuration.idInstance })
            .done(function (response) {
                var selectorPath = response.Result.SelectorPath;
                var elements = getElementsBySelectorPath(selectorPath);
                var mediaBookmarkManagerComponent = $(elements).data('mediaBookmarkManager-component');

                mediaBookmarkManagerComponent.viewModel = response.Result;
                createStatusBar(mediaBookmarkManagerComponent.viewModel.PageInitState);
                mediaBookmarkManagerComponent.handler.createComponent(mediaBookmarkManagerComponent);
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

    };
})(jQuery);


viewModel.messageFromWebsocket = function (name, message) {
    // Add the message to the page.
    console.log(message);

    if (message.ChannelId == viewModel.ChannelControllerMessage) {
        $('.execution-log').html(message.GenericParameterItems[0].Message);
    }

    if (message.ChannelId == viewModel.ChannelSelectedObject) {
        var bookmarkSelected = viewModel.mediaBookmarkManagerComponent.handler.selectBookmarkByReference(viewModel.mediaBookmarkManagerComponent, message.OItems[0].GUID);
        if (!bookmarkSelected) {
            viewModel.mediaBookmarkManagerComponent.handler.checkBookmarks(viewModel.mediaBookmarkManagerComponent, message.OItems);
        }

    }

    if (checkSendeserForIgnore(viewModel.sendersToIgnore, message)) return;

    if (message.ChannelId == viewModel.ChannelSelectedObject) {
        viewModel.validateReference(message.OItems[0].GUID)

    }
}

viewModel.getUrlParameters = function () {
    urlParameterConfig.initialize();
    if (urlParameterConfig.objectParam !== undefined) {
        viewModel.validateReference(urlParameterConfig.objectParam)
    }

    if (viewModel.mediaType == "Video") {
        viewModel.stateMachine.addState(viewModel.stateMachine.states.creatingVideoPlayer);
        $('.video-player-container').createVideoPlayer({
            selectorPath: ['.video-player-container'],
            setWindowTitle: true,
            actions: {
                init: viewModel.ActionVideoPlayerInit
            },
            events: {
                completed: function (component) {
                    viewModel.videoPlayerComponent = component;
                    var selectorPath = [];
                    for (var i = 0; i < component.configuration.selectorPath.length; i++) {
                        var selector = component.configuration.selectorPath[i];
                        selectorPath.push(selector);
                    }

                    viewModel.stateMachine.addState(viewModel.stateMachine.states.createdVideoPlayer);
                    viewModel.stateMachine.addState(viewModel.stateMachine.states.validatingVideoPlayer);
                    viewModel.videoPlayerComponent.handler.validateReference(viewModel.videoPlayerComponent, urlParameterConfig.objectParam);
                },
                validatedReference: function (videoPlayerComponent) {
                    viewModel.stateMachine.addState(viewModel.stateMachine.states.validatedVideoPlayer);
                },
                changed: function (videoPlayerComponent, mediaItem) {
                    viewModel.mediaBookmarkManagerComponent.handler.validateReference(viewModel.mediaBookmarkManagerComponent, mediaItem.IdMultimediaItem, mediaItem.IdRef);
                    if (typeof viewModel.mediaBookmarkManagerComponent.configuration.events !== 'undefined' && viewModel.mediaBookmarkManagerComponent.configuration.events !== null &&
                        typeof viewModel.mediaBookmarkManagerComponent.configuration.events.changedMediaItem !== 'undefined' && viewModel.mediaBookmarkManagerComponent.configuration.events.changedMediaItem !== null) {
                        viewModel.mediaBookmarkManagerComponent.configuration.events.changedMediaItem(viewModel.mediaBookmarkManagerComponent, mediaItem);
                    }
                }
            }
        });
    }
}