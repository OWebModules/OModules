﻿viewModel._itemsPreApply = [];

var cellCloseHandler = function (e) {
    e.model.IsSaved = false;
    viewModel.gridItem.saveChanges();
}

viewModel.validateReference = function (idRef) {
    $('.loader').show();
    $.post(viewModel.ActionConfig.ActionValidateReference, { refItem: { GUID: idRef }, idInstance: viewModel.IdInstance })
        .done(function (response) {
            console.log("connected", response);
            changeValuesFromServer(response.ViewItems);
            viewModel.gridItem.dataSource.read();
        })
        .fail(function (jqxhr, textStatus, error) {
            // stuff
        });
}

viewModel.getUrlParameters = function () {
    urlParameterConfig.initialize();

    if (urlParameterConfig.objectParam != undefined) {
        viewModel.validateReference(urlParameterConfig.objectParam);

    }
}

viewModel.messageFromWebsocket = function (name, message) {
    // Add the message to the page.
    console.log(message);

    
    if (message.ChannelId == viewModel.ChannelConfig.ChannelSelectedObject) {
        if (!$('#' + viewModel.isListen.ViewItemId).getCheckedMode()) return;
        viewModel.validateReference(message.OItems[0].GUID);
    } else if (message.ChannelId == viewModel.ChannelConfig.ChannelUploadedFiles) {
        var fileItems = message.OItems;
        $.post(viewModel.ActionConfig.ActionSaveFiles, { fileItems: fileItems, idInstance: viewModel.IdInstance })
            .done(function (response) {
                console.log("connected", response);
                if (response.IsSuccessful) {
                    for (var ix in response.Result) {
                        var mediaListItem = response.Result[ix];

                        viewModel.gridItem.dataSource.add(mediaListItem);
                    }
                }
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    } else if (message.ChannelId === viewModel.ChannelConfig.ChannelViewReady && message.ReceiverId === viewModel.IdInstance) {
        var data = JSON.stringify(viewModel.gridItem.dataSource.view());
        $.post(viewModel.ActionConfig.ActionSaveSessionData, { data: data, idInstance: viewModel.IdInstance, extension: "json" })
            .done(function (response) {
                console.log("connected", response);
                if (response.IsSuccessful) {
                    viewModel.managerConfig.moduleComHub.server.sendInterServiceMessage({
                        ChannelId: viewModel.ChannelConfig.ChannelMediaList,
                        SenderId: viewModel.managerConfig.idEndpoint,
                        SessionId: viewModel.managerConfig.idSession,
                        ReceiverId: viewModel.idSender,
                        UserId: viewModel.IdUser,
                        ViewId: viewModel.View.IdView,
                        GenericParameterItems: [viewModel.IdInstance]
                    });
                }
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
        
    }




}

viewModel.gridChangeHandler = function (e) {
    var selectedRows = this.select();

    if (selectedRows.length !== 1) {
        return;
    }
    viewModel.dataItemRow = viewModel.gridItem.select();
    viewModel.dataItem = viewModel.gridItem.dataItem(viewModel.gridItem.select());

};

viewModel.reloadGrid = function () {
    viewModel.gridItem.dataSource.read();

}

viewModel.setDataItems = function () {
    $("#" + viewModel.uploadMediaWindow.ViewItemId).data("viewItem", viewModel.uploadMediaWindow);
    $("#" + viewModel.playerWindow.ViewItemId).data("viewItem", viewModel.playerWindow);
    $("#" + viewModel.uploadItems.ViewItemId).data("viewItem", viewModel.uploadItems);
    $("#" + viewModel.removeFromList.ViewItemId).data("viewItem", viewModel.removeFromList);
    $("#" + viewModel.applyItem.ViewItemId).data("viewItem", viewModel.applyItem);
    $("#" + viewModel.openPlayer.ViewItemId).data("viewItem", viewModel.openPlayer);
    $("#" + viewModel.isListen.ViewItemId).data("viewItem", viewModel.isListen);
    $("#" + viewModel.nameOItem.ViewItemId).data("viewItem", viewModel.nameOItem);
    $("#" + viewModel.downloadMediaList.ViewItemId).data("viewItem", viewModel.downloadMediaList);
}

function init() {

    

    $('#' + viewModel.uploadMediaWindow.ViewItemId).kendoWindow({
        width: '1024px',
        height: '600px',
        title: 'New Item',
        visible: false,
        modal: true,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();


    $('#' + viewModel.playerWindow.ViewItemId).kendoWindow({
        width: '1024px',
        height: '600px',
        title: 'New Item',
        visible: false,
        modal: true,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();

    $('#' + viewModel.toolbar.ViewItemId).kendoToolBar({
        items: [
            {
                template: '<button id="' + viewModel.uploadItems.ViewItemId + '" class="k-button"><i class="fa fa-plus-square-o" aria-hidden="true"></i></button>',

            },
            {
                template: '<button id="' + viewModel.removeFromList.ViewItemId + '" class="k-button"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>',

            },
            {
                template: '<button id="' + viewModel.applyItem.ViewItemId + '" class="k-button"><i class="fa fa-check-circle" aria-hidden="true"></i></button>',

            },
            {
                template: '<button id="' + viewModel.openPlayer.ViewItemId + '" class="k-button"><i class="fa fa-play-circle-o" aria-hidden="true"></i></button>',

            },
            {
                template: '<button id="' + viewModel.isListen.ViewItemId + '" class="k-button"><i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></button>',

            },
            {
                template: '<button id="' + viewModel.downloadMediaList.ViewItemId + '" class="k-button"><i class="fa fa-download" aria-hidden="true"></i></button>',

            },
            {
                template: '<label id="' + viewModel.nameOItem.ViewItemId + '"></Label>',
            }
        ]
    });

    $('.k-button').kendoButton({
        click: function () {
            if ($(this.element).attr('id') == viewModel.isListen.ViewItemId) {
                $('#' + viewModel.isListen.ViewItemId).toggleMode(true);
            } else if ($(this.element).attr('id') == viewModel.applyItem.ViewItemId) {
                console.log(viewModel._itemsPreApply);
                var appliedOItems = [];
                for (var ix in viewModel._itemsPreApply) {
                    var mediaListItem = viewModel._itemsPreApply[ix];

                    appliedOItems.push({
                        GUID: mediaListItem.IdMultimediaItem,
                        Name: mediaListItem.NameMultimediaItem,
                        GUID_Parent: mediaListItem.IdClassMultimediaItem,
                        Type: "Object"
                    });


                }
                viewModel.managerConfig.moduleComHub.server.sendInterServiceMessage({
                    ChannelId: viewModel.ChannelConfig.ChannelAppliedObjects,
                    SenderId: viewModel.managerConfig.idEndpoint,
                    SessionId: viewModel.managerConfig.idSession,
                    UserId: viewModel.IdUser,
                    OItems: appliedOItems
                });
            } else if ($(this.element).attr('id') == viewModel.openPlayer.ViewItemId) {
                if (viewModel.View.IdView == viewModel.mediaListPDFViewId) {
                    if (viewModel.dataItem !== undefined) {
                        window.open(viewModel.dataItem.FileUrl, "_blank");
                    }
                } else if (viewModel.View.IdView == viewModel.mediaListAudioViewId) {
                    //$('#' + viewModel.iframePlayer.ViewItemId).attr('src', viewModel.ActionConfig.ActionAudioPlayer + '?Sender=' + viewModel.IdInstance);
                    //var playerWindow = $('#' + viewModel.playerWindow.ViewItemId).data("kendoWindow");
                    //playerWindow.open();

                    window.open(viewModel.ActionConfig.ActionAudioPlayer + '?Sender=' + viewModel.IdInstance, '_blank');
                } else if (viewModel.View.IdView == viewModel.mediaListVideoViewId) {
                    //$('#' + viewModel.iframePlayer.ViewItemId).attr('src', viewModel.ActionConfig.ActionAudioPlayer + '?Sender=' + viewModel.IdInstance);
                    //var playerWindow = $('#' + viewModel.playerWindow.ViewItemId).data("kendoWindow");
                    //playerWindow.open();

                    window.open(viewModel.ActionConfig.ActionVideoPlayer + '?Sender=' + viewModel.IdInstance, '_blank');
                } else if (viewModel.View.IdView == viewModel.mediaListImagesViewId) {
                    //$('#' + viewModel.iframePlayer.ViewItemId).attr('src', viewModel.ActionConfig.ActionAudioPlayer + '?Sender=' + viewModel.IdInstance);
                    //var playerWindow = $('#' + viewModel.playerWindow.ViewItemId).data("kendoWindow");
                    //playerWindow.open();

                    window.open(viewModel.ActionConfig.ActionImageViewer + '?Sender=' + viewModel.IdInstance, '_blank');
                }
            } else if ($(this.element).attr('id') == viewModel.uploadItems.ViewItemId) {
                var uploadWindow = $('#' + viewModel.uploadMediaWindow.ViewItemId).data("kendoWindow");
                uploadWindow.open();
                $('#' + viewModel.iframeUpload.ViewItemId).attr('src', viewModel.ActionConfig.ActionFileUpload + '?Sender=' + viewModel.IdInstance);
            } else if ($(this.element).attr('id') == viewModel.downloadMediaList.ViewItemId) {
                var url = viewModel.ActionConfig.ActionDownloadMediaList;
                var sort = viewModel.gridItem.dataSource.sort();

                var sortStr = "";
                if (sort.length > 0) {
                    if (sort[0].field == "OrderId") {
                        sortStr = "&Sort=";

                        if (sort[0].dir == "asc") {
                            sortStr += "asc";
                        } else {
                            sortStr += "desc";
                        }

                    }
                }

                url = url + "?idInstance=" + viewModel.IdInstance + sortStr;
                window.open(url, "_blank");
            }
        }
    });

    viewModel.setDataItems();

    viewModel.addApplyItem = function (item) {
        viewModel._itemsPreApply.push(item);
    };
    viewModel.removeApplyItem = function (item) {
        viewModel._itemsPreApply = jQuery.grep(viewModel._itemsPreApply, function (itemInArray) {
            return itemInArray.IdInstance != item.IdInstance
        });

    };

    viewModel.checkedItemChange = function () {
        var row = $(this).closest('tr');
        var item = viewModel.gridItem.dataItem(row);
        var applied = $(this).prop("checked");
        item.Apply = applied;
        if (item.Apply) {
            viewModel.addApplyItem(item);
        } else {
            viewModel.removeApplyItem(item);
        }

        console.log(item);
    }

    $.post(viewModel.ActionConfig.ActionGetGridConfig)
        .done(function (response) {

            $.extend(response.dataSource.transport.read, {
                data: function () {
                    return { idInstance: viewModel.IdInstance };
                },
            })

            $.extend(response.dataSource, {
                batch: true
            });

            var dataSource = new kendo.data.DataSource(response.dataSource);
            response.dataSource = dataSource;
            response.dataBound = function () {
                $('.chkbxApply').change(viewModel.checkedItemChange);
                if (viewModel.gridItem.dataSource.total() > 0) {
                    $("#" + viewModel.downloadMediaList.ViewItemId).setEnabled(true, true);
                } else {
                    $("#" + viewModel.downloadMediaList.ViewItemId).setEnabled(false, true);
                }
            };

           
            $.extend(response, {
                change: viewModel.gridChangeHandler,
                cellClose: cellCloseHandler
            });

            viewModel.gridItem = $("#" + viewModel.grid.ViewItemId).kendoGrid(response).data("kendoGrid");

            viewModel.gridItem.wrapper.find(".k-pager")
                .before('<a href="#" class="k-link prev">Prev</a>')
                .after('<a href="#" class="k-link next">Next</a>')
                .parent()
                .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                .delegate("a.prev", "click", function () {
                    dataSource.page(dataSource.page() - 1);
                })
                .delegate("a.next", "click", function () {
                    dataSource.page(dataSource.page() + 1);
                })
                .delegate(".pagesize input", "change", function () {
                    dataSource.page(parseInt(this.value, 10));
                })

            //viewModel.reloadGrid();


        })
        .fail(function (jqxhr, textStatus, error) {
            // stuff
        });

    viewModel.connectCallback = function () {
        $.post(viewModel.ActionConfig.ActionConnected, { idInstance: viewModel.IdInstance })
            .done(function (response) {
                console.log("connected", response);
                changeValuesFromServer(response);
                viewModel.getUrlParameters();
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    }

    viewModel.disconnectCallback = function () {
        $.post(viewModel.ActionConfig.ActionDisconnected, { idInstance: viewModel.IdInstance })
            .done(function (response) {
                console.log("disconnected", response);
                changeValuesFromServer(response);
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    }

    

    for (var ix in viewModel.ViewItems) {
        var viewItem = viewModel.ViewItems[ix];

        for (var jx in viewItem.ViewItemValues) {
            var viewItemValue = viewItem.ViewItemValues[jx];

            if (viewItemValue.ViewItemType == "Enable") {
                $('#' + viewItem.ViewItemId).setEnabled(viewItemValue.Value, false);
            }
            else if (viewItemValue.ViewItemType == "Visible") {
                $('#' + viewItem.ViewItemId).setVisible(viewItemValue.Value, false);
            }
            else if (viewItemValue.ViewItemType == "Checked") {
                $('#' + viewItem.ViewItemId).setCheckedMode(viewItemValue.Value, false);
            }
            else if (viewItem.ViewItemId == viewModel.windowTitle.ViewItemId && viewItemValue.ViewItemType == "Content") {
                var title = viewItemValue.Value;
                document.title = title;
            }
        }
    }

    initWebsocket(viewModel, viewModel.IdSession, viewModel.IdInstance, viewModel.IdUser, viewModel.connectCallback, viewModel.disconnectCallback);

    viewModel.addEndpoint(viewModel.ChannelConfig.ChannelSelectedObject);
    viewModel.addEndpoint(viewModel.ChannelConfig.ChannelUploadedFiles);
    viewModel.addEndpoint(viewModel.ChannelConfig.ChannelViewReady);

    viewModel.startWebsocket(viewModel.messageFromWebsocket);

    createStatusBar(viewModel.PageInitState);
}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    //errorOccured(error, url, line);
}