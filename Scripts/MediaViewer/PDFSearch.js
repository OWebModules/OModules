﻿(function ($) {
    $.fn.createPDFSearch = function (configuration) {
        var pdfSearch = {
            configuration: configuration,
            elements: {
                gridElement: null,
                grid: null
            },
            handler: {
                gridChangeHandler: function () {

                }
            }
        };

        // Reset content of root-element
        $(this).html('');

        // Create element of grid
        pdfSearch.elements.gridElement = $('<div class="pdf-search-grid"></div>');

        // Add class to root-element
        $(this).addClass('pdf-search');
        $(this).append(pdfSearch.elements.gridElement);
        $(pdfSearch.elements.gridElement).data('pdf-search-component', pdfSearch);
        $(this).openPageLoader();

        if (pdfSearch.configuration.viewModel === null) {
            $.post(pdfSearch.configuration.actionPDFSearchInit)
                .done(function (response) {
                    console.log("connected", response);
                    pdfSearch.viewModel = response.Result;

                    $.post(pdfSearch.viewModel.ActionGetGridConfigSearch)
                        .done(function (response) {
                            $.extend(response.dataSource.transport.read, {
                                data: function () {
                                    return {
                                        idInstance: pdfSearch.viewModel.IdInstance,
                                        search: $(pdfSearch.elements.gridElement).find('.k-grid-search').find('input').val()
                                    };
                                }
                            });

                            var dataSource = new kendo.data.DataSource(response.dataSource);
                            response.dataSource = dataSource;
                            response.dataBound = function () {
                                $('.pdf-search-grid').mark($(pdfSearch.elements.gridElement).find('.k-grid-search').find('input').val());

                                $('.pdf-search-grid').find('.pdf-search-ref-edit').click(function () {
                                    var pdfSearch = $(this).closest('.pdf-search-grid').data('pdf-search-component')
                                    var row = $(this).closest('tr');
                                    var item = pdfSearch.elements.grid.dataItem(row);
                                    var url = pdfSearch.viewModel.ActionRelationEditor;
                                    url += "?Object=" + item.IdReference;
                                    window.open(url, "_blank");
                                });

                                $('.pdf-search-grid').find('.button-module-starter-pdf-reference').click(function () {
                                    var pdfSearch = $(this).closest('.pdf-search-grid').data('pdf-search-component')
                                    var row = $(this).closest('tr');
                                    var item = pdfSearch.elements.grid.dataItem(row);
                                    var url = pdfSearch.viewModel.ActionModuleStarter;
                                    url += "?Object=" + item.IdReference;
                                    window.open(url, "_blank");
                                });
                            };
                            $.extend(response, {
                                change: pdfSearch.handler.gridChangeHandler,
                                filterable: {
                                    mode: "row"
                                }
                            });


                            pdfSearch.elements.grid = $(pdfSearch.elements.gridElement).kendoGrid(response).data("kendoGrid");

                            $('body').closePageLoader();
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });

                    changeValuesFromServerChildren(response.ViewItems, [".pdf-search-grid"]);
                    if (pdfSearch.configuration.validateReferenceCallback !== undefined && pdfSearch.configuration.validateReferenceCallback !== null) {
                        pdfSearch.configuration.validateReferenceCallback();
                    }
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                    $('body').closePageLoader();
                });
        };
    }
}) (jQuery);