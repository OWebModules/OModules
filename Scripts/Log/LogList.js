﻿var logListElement;
(function ($) {
    $.fn.createLogList = function (configuration, createCallBack) {
        var dataLogList = 'log-list-config';
        var selectorLogList = configuration.selector;

        $(this).data(dataLogList, {
            elements: {
                logListContainerElement: null,
                grid: null,
                gridItem: null
            },
            configuration: configuration,
            handler: {
                createCallBack: createCallBack,
                validateReference: function (idObject, validateReferenceCallback) {
                    var oItem = {
                        GUID: idObject
                    };
                    var url = $(selectorLogList).data(dataLogList).configuration.viewModel.ActionValidateReference;
                    var idInstance = $(selectorLogList).data(dataLogList).configuration.viewModel.IdInstance;

                    $.post(url, { refItem: oItem, idInstance: idInstance })
                        .done(function (response) {
                            console.log("connected", response);
                            
                            changeValuesFromServerChildren(response.ViewItems, [selectorLogList]);
                            $(selectorLogList).data(dataLogList).handler.reloadGrid();
                            if (validateReferenceCallback !== undefined && validateReferenceCallback !== null) {
                                validateReferenceCallback();
                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                            $('body').closePageLoader();
                        });
                },
                reloadGrid: function () {
                    $(selectorLogList).data(dataLogList).elements.gridItem.dataSource.read();
                },
                gridChangeHandler: function (e) {

                }
            }
        });

        $(this).addClass('log-list');
        logListElement = $(this).data(dataLogList);
        logListElement.elements.logListContainerElement = this[0];
        $(logListElement.elements.logListContainerElement).html('');
        logListElement.elements.grid = $('<div class=".log-list-grid"></div>');
        $(logListElement.elements.logListContainerElement).append(logListElement.elements.grid);

        $.post(logListElement.configuration.actionLogListInit)
            .done(function (response) {
                logListElement.configuration.viewModel = response;
                $.post(logListElement.configuration.viewModel.ActionGetGridConfig)
                    .done(function (response) {
                        $.extend(response.Result.dataSource.transport.read, {
                            data: function () {
                                return { idInstance: logListElement.configuration.viewModel.IdInstance };
                            }
                        })

                        var dataSource = new kendo.data.DataSource(response.Result.dataSource);
                        response.Result.dataSource = dataSource;
                        response.Result.dataBound = function () {

                        };
                        $.extend(response.Result, {
                            change: logListElement.handler.gridChangeHandler,
                            filterable: {
                                mode: "row"
                            }
                        });


                        logListElement.elements.gridItem = $(logListElement.elements.grid).kendoGrid(response.Result).data("kendoGrid");

                        if (logListElement.handler.createCallBack !== undefined && logListElement.handler.create !== null) {
                            logListElement.handler.createCallBack(logListElement);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
        

    };
})(jQuery);