﻿(function ($) {
    $.fn.createClassAttributeEditor = function (configuration) {
        $(this).data("class-attribute-editor", {
            elements: {
                classAttributeEditor: null,
                classAttributeEditorItem: null,
                classAttributeEditorToolbar: null,
                grid: null
            },
            components: {
                grid: null
            },
            data: {
                oItem: null
            },
            configuration: configuration,
            viewModel: null,
            handler: {
                createComponent: function (element) {

                    var classAttributeEditorComponent = $(element).data("class-attribute-editor");
                    classAttributeEditorComponent.handler.triggerEvents.createComponent(classAttributeEditorComponent, false);
                    $(element).addClass("class-attribute-editor");

                    classAttributeEditorComponent.elements.classAttributeEditor = $(element);
                    classAttributeEditorComponent.elements.classAttributeEditorItem = $(classAttributeEditorComponent.templates.componentTemplate);
                    $(classAttributeEditorComponent.elements.classAttributeEditor).html('');
                    $(classAttributeEditorComponent.elements.classAttributeEditor).append(classAttributeEditorComponent.elements.classAttributeEditorItem);
                    $(classAttributeEditorComponent.elements.classAttributeEditor).find('.toolbar').kendoToolBar({
                        items: [
                            {
                                template: '<button class="addAttribute k-button"><i class="fa fa-plus" aria-hidden="true"></i></button>'
                            },
                            {
                                template: '<button class="removeAttribute k-button"><i class="fa fa-trash" aria-hidden="true"></i></button>'
                            }
                        ]
                    });

                    classAttributeEditorComponent.elements.grid = $(classAttributeEditorComponent.elements.classAttributeEditor).find('.gridItem');

                    $(classAttributeEditorComponent.elements.classAttributeEditor).find(".k-button").kendoButton({
                        click: function (e) {

                        }
                    })

                    classAttributeEditorComponent.handler.initViewModel(classAttributeEditorComponent);
                },
                initViewModel: function (classAttributeEditorComponent) {
                    $.post(classAttributeEditorComponent.configuration.actionInit, { idInstance: classAttributeEditorComponent.configuration.idInstance, selectorPath: classAttributeEditorComponent.configuration.selectorPath })
                        .done(function (response) {
                            var classAttributeEditorComponent = $(getElementsBySelectorPath(response.SelectorPath)).data('class-attribute-editor');
                            classAttributeEditorComponent.configuration.viewModel = response.Result;
                            changeValuesFromServerChildren(response.ViewItems, classAttributeEditorComponent.configuration.selectorPath);
                            classAttributeEditorComponent.handler.createGrid(classAttributeEditorComponent);
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                createGrid: function (classAttributeEditorComponent) {
                    $.post(classAttributeEditorComponent.configuration.viewModel.ActionGetGridConfig, { idInstance: classAttributeEditorComponent.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            var classAttributeEditorComponent = $(getElementsBySelectorPath(response.SelectorPath)).data('class-attribute-editor');

                            if (classAttributeEditorComponent.elements.dataSource === undefined ||
                                classAttributeEditorComponent.elements.dataSource === null) {
                                classAttributeEditorComponent.elements.dataSource = new kendo.data.DataSource(response.Result.dataSource);

                            } else {
                                $(classAttributeEditorComponent.elements.grid).data("kendoGrid").destroy(); // destroy the Grid

                                $(classAttributeEditorComponent.elements.grid).empty(); // empty the Grid content (inner HTML)
                            }

                            $.extend(response.Result.dataSource.transport.read, {
                                component: classAttributeEditorComponent
                            });
                            $.extend(response.Result.dataSource.transport.read, {
                                data: function () {
                                    return {
                                        idInstance: this.component.configuration.viewModel.IdInstance,
                                        idClass: this.component.data.oItem.GUID
                                    };
                                }
                            })

                            response.Result.dataSource = classAttributeEditorComponent.elements.dataSource;
                            $.extend(response.dataSource, {
                                batch: true
                            });

                            $.extend(response.Result.dataSource.transport.update,
                                {
                                    complete: function (e) {
                                        console.log(e);
                                    }
                                }
                            );

                            $.extend(response.Result.dataSource.transport.destroy,
                                {
                                    complete: function (e) {
                                        console.log(e);
                                    }
                                }
                            );

                            $.extend(response.Result.dataSource.transport.read,
                                {
                                    complete: function (e) {
                                        console.log(e);
                                    }
                                }
                            );

                            response.Result.dataSource.bind("error", function (e) {
                                console.log(e);
                                $('.object-list').data('object-list-config').elements.gridItem.cancelChanges();
                                var dialogElement = $('#dialog');
                                dialogElement.kendoDialog({
                                    width: "300px",
                                    height: "150px",
                                    title: "Value",
                                    closable: true,
                                    modal: false,
                                    content: "An error occured!",
                                    actions: []
                                });

                                dialogElement.data("kendoDialog").open();
                            });

                            $(classAttributeEditorComponent.elements.grid).kendoGrid(response.Result);
                            classAttributeEditorComponent.components.grid = $(classAttributeEditorComponent.elements.grid).data('kendoGrid');
                            classAttributeEditorComponent.handler.triggerEvents.createComponent(classAttributeEditorComponent, true);
                        })
                },
                reloadGrid: function (classAttributeEditorComponent) {
                    classAttributeEditorComponent.components.grid.dataSource.read();
                },
                validateReference: function (oItem, classAttributeEditorComponent) {
                    classAttributeEditorComponent.data.oItem = oItem;
                    classAttributeEditorComponent.handler.reloadGrid(classAttributeEditorComponent);
                },
                triggerEvents: {
                    createComponent: function (classAttributeEditorComponent, finishState) {
                        if (finishState == true) {
                            if (typeof classAttributeEditorComponent.configuration.events !== 'undefined' && classAttributeEditorComponent.configuration.events !== null &&
                                typeof classAttributeEditorComponent.configuration.events.createdComponent !== 'undefined' && classAttributeEditorComponent.configuration.events.createdComponent !== null) {
                                classAttributeEditorComponent.configuration.events.createdComponent(classAttributeEditorComponent);
                            }
                        } else {
                            if (typeof classAttributeEditorComponent.configuration.events !== 'undefined' && classAttributeEditorComponent.configuration.events !== null &&
                                typeof classAttributeEditorComponent.configuration.events.createComponent !== 'undefined' && classAttributeEditorComponent.configuration.events.createComponent !== null) {
                                classAttributeEditorComponent.configuration.events.createComponent(classAttributeEditorComponent);
                            }
                        }
                    },
                }
            },
            templates: {
                componentTemplate: `
                    <div class="classAttributeEditor">
                        <div class="toolbar">
                        </div>
                        <div class="gridItem">
                        </div>
                    </div>
                `
            }
        });

        $(this).data('class-attribute-editor').handler.createComponent(this);
    };
})(jQuery);