﻿(function ($) {
    $.fn.createObjectRelationTree = function (configuration) {
        var objectRelationTreeComponent = {
            configuration: configuration,
            data: {

            },
            elements: {
                parent: null,
                element: null
            },
            components: {

            },
            stateMachine: {
                state: 0,
                states: {
                    createComponent: 2,
                    createdComponent: 4,
                    validateReference: 8,
                    validatedReference: 16,
                    loadTreeConfig: 32,
                    loadedTreeConfig: 64,
                    loadData: 128,
                    loadedData: 256
                },
                hasState: function (component, state) {
                    if ((component.stateMachine.state & state) == state) {
                        return true;
                    }
                    else {
                        return false;
                    }
                },
                addState: function (component, state) {
                    component.stateMachine.state += state;
                    component.stateMachine.checkState(component);
                },
                removeState: function (component, state) {
                    component.stateMachine.state -= state;
                    component.stateMachine.checkState(component);
                },
                checkState: function (component) {
                    if (component.stateMachine.hasState(component, component.stateMachine.states.createComponent) &&
                        !component.stateMachine.hasState(component, component.stateMachine.states.createdComponent)) {

                        $('body').openPageLoader();
                        component.triggerEvents.createComponent(component, false);


                    }
                    if (component.stateMachine.hasState(component, component.stateMachine.states.createComponent) &&
                        component.stateMachine.hasState(component, component.stateMachine.states.createdComponent)) {

                        component.stateMachine.removeState(component, component.stateMachine.states.createComponent);
                        $('body').closePageLoader();
                        component.triggerEvents.createComponent(component, true);

                    }

                    if (component.stateMachine.hasState(component, component.stateMachine.states.validateReference) &&
                        !component.stateMachine.hasState(component, component.stateMachine.states.validatedReference)) {

                        $('body').openPageLoader();
                        component.triggerEvents.createComponent(component, false);
                    }

                    if (component.stateMachine.hasState(component, component.stateMachine.states.validateReference) &&
                        component.stateMachine.hasState(component, component.stateMachine.states.validatedReference)) {
                        component.stateMachine.removeState(component, component.stateMachine.states.validateReference);
                        $('body').closePageLoader();
                        component.triggerEvents.createComponent(component, true);
                    }

                }
            },
            handler: {
                createComponent: function (component) {
                    component.stateMachine.addState(component, component.stateMachine.states.createComponent);
                    component.elements.treeView = $(component.templates.treeTemplate);
                    $(component.elements.containerElement).append(component.elements.treeView);
                    $.post(component.configuration.viewModel.ActionGetTreeConfig, { idInstance: component.configuration.idInstance })
                        .done(function (response) {
                            // stuff
                            var element = getElementsBySelectorPath(response.SelectorPath);
                            var component = $(element).data('component');
                            console.log(response);
                            var treeConfig = response.Result;
                            treeConfig.dataSource.transport.selectorPath = component.configuration.selectorPath;
                            treeConfig.dataSource.transport.parameterMap = function (data, type) {
                                var element = getElementsBySelectorPath(response.SelectorPath);
                                var component = $(element).data('component');
                                var parameterExtend = {
                                    "idInstance": component.configuration.idInstance,
                                    "idReference": component.data.idReference
                                }
                                return parameterExtend;
                            };
                            $.extend(treeConfig, { template: kendo.template(component.templates.treeNode) });
                            treeConfig.dataSource = new kendo.data.HierarchicalDataSource(treeConfig.dataSource);
                            $.extend(treeConfig,
                                {
                                    select: function (e) {

                                        var component = $(e.node).closest('.controlArea').data('component');

                                        var nodeItem = component.components.Tree.dataItem(e.node);
                                        if (typeof nodeItem.SelectChannel === 'undefined' || nodeItem.SelectChannel === null) {
                                            return;
                                        }

                                        var oItems = [
                                            {
                                                GUID: nodeItem.NodeId,
                                                Name: nodeItem.NodeName
                                            }
                                        ];
                                        component.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                            ChannelId: nodeItem.SelectChannel,
                                            SenderId: component.configuration.managerConfig.idEndpoint,
                                            SessionId: component.configuration.managerConfig.idSession,
                                            ReceiverId: component.configuration.viewModel.idSender,
                                            ViewId: component.configuration.viewModel.View.IdView,
                                            UserId: component.configuration.viewModel.IdUser,
                                            OItems: oItems
                                        });

                                    }
                                });
                            $.extend(treeConfig,
                                {
                                    dataBound: component.handler.dataBound,
                                    change: component.handler.change
                                });


                            component.components.Tree = $(component.elements.treeView).kendoTreeView(treeConfig).data("kendoTreeView");
                            component.stateMachine.addState(component, component.stateMachine.states.createdComponent);
                            component.stateMachine.addState(component, component.stateMachine.states.loadData);

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                dataBound: function () {
                    var component = $(this)[0].element.closest('.controlArea').data('component');
                    component.stateMachine.addState(component, component.stateMachine.states.loadedData);
                    this.expand('.k-item');
                    
                    $(component.elements.treeView).find('.btn-apply-item').unbind('click');
                    $(component.elements.treeView).find('.btn-apply-item').on('click', function () {
                        var component = $(this).closest('.controlArea').data('component');
                        var nodeItem = component.components.Tree.dataItem($(this).closest('li.k-item'));

                        var oItems = [
                            {
                                GUID: nodeItem.NodeId,
                                Name: nodeItem.NodeName
                            }
                        ];
                        component.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                            ChannelId: nodeItem.ApplyChannel,
                            SenderId: component.configuration.managerConfig.idEndpoint,
                            SessionId: component.configuration.managerConfig.idSession,
                            ReceiverId: component.configuration.viewModel.idSender,
                            ViewId: component.configuration.viewModel.View.IdView,
                            UserId: component.configuration.viewModel.IdUser,
                            OItems: oItems
                        });
                    });

                    $(component.elements.treeView).find('.btn-add-item').unbind('click');
                    $(component.elements.treeView).find('.btn-add-item').on('click', function () {
                        var component = $(this).closest('.controlArea').data('component');
                        component.data.nodeItem = component.components.Tree.dataItem($(this).closest('li.k-item'));

                        component.data.classItem = {
                            GUID: component.data.nodeItem.NodeId
                        };
                        component.components.clipboardComponent.handler.validateReference(component.components.clipboardComponent, component.data.classItem);
                    });
                },
                change: function () {
                    var component = $(this)[0].element.closest('.controlArea').data('component');
                    viewModel.treeViewChange = function (e) {
                        var selected = this.select();
                        console.log(selected);

                    }
                },
                validateReference: function (component, idObject) {
                    component.data.idReference = idObject;
                    component.components.Tree.dataSource.read();
                },
                init: function (containerElement, component) {
                    component.elements.containerElement = containerElement;
                    $(containerElement).data('component', component);
                    $(containerElement).addClass('object-relation-tree');

                    $.post(component.configuration.actionInit, { idInstance: component.configuration.idInstance, selectorPath: component.configuration.selectorPath })
                        .done(function (response) {
                            // stuff
                            console.log(response);
                            if (response.IsSuccessful) {
                                var selectorPath = response.SelectorPath;
                                var element = getElementsBySelectorPath(selectorPath);
                                var component = $(element).data('component');
                                component.configuration.viewModel = response.Result;
                                component.handler.createComponent(component);

                                var configurationObjectList = {
                                    actionObjectListInit: component.configuration.viewModel.ActionObjectListInit,
                                    templateSelector: "#template",
                                    parentComponent: component,
                                    managerConfig: component.configuration.managerConfig,
                                    useIdParent: true,
                                    callbackGridCreated: function (gridItem) {

                                    },
                                    handler: {
                                        initCompleteHandler: function () {
                                            //viewModel.getUrlParameters();,
                                            var senderIdNew = $('.object-list-container').data('object-list-config').configuration.viewModel.IdEndpoint;
                                            addSenderToIgnoreList(senderIdNew, sendersIgnoreRelationEditor);
                                        }
                                    },
                                    events: {
                                        applyItems: function (items, parentComponent) {
                                            $(parentComponent.elements.objectWindowContainer).data("kendoWindow").close();
                                            parentComponent.handler.relateItems(parentComponent, items);
                                        }
                                    }
                                };

                                component.elements.objectWindowContainer = $(component.templates.objectWindowContainer);

                                $(component.elements.containerElement).append(component.elements.objectWindowContainer);
                                var objectWindow = $(component.templates.objectWindow);
                                $(component.elements.objectWindowContainer).append(objectWindow);
                                $(objectWindow).append($(component.templates.objectListContainer));

                                $(component.elements.objectWindowContainer).kendoWindow({
                                    width: '1000px',
                                    height: '800px',
                                    title: 'Objects',
                                    visible: false,
                                    modal: true,
                                    actions: [
                                        "Pin",
                                        "Minimize",
                                        "Maximize",
                                        "Close"
                                    ]
                                }).data("kendoWindow").center();


                                $(component.elements.objectWindowContainer).find('.object-list-container').createObjectList(configurationObjectList);
                                component.elements.clipboardWindow = $(component.templates.windowClipboardContainer);
                                $(component.elements.containerElement).append(component.elements.clipboardWindow);
                                $(component.elements.clipboardWindow).data('component', component);

                                $(component.elements.clipboardWindow).kendoWindow({
                                    width: '1000px',
                                    height: '800px',
                                    title: 'Clipboard',
                                    visible: false,
                                    modal: true,
                                    close: function (e) {
                                        var component = $(e.sender.element).data('component')
                                        if (typeof component.data.appliedClipboardItems === 'undefined' || component.data.appliedClipboardItems == null) {
                                            if (typeof component.data.classItem !== 'undefined' && component.data.classItem !== null) {
                                                $(component.elements.objectWindowContainer).find('.object-list-container').data('object-list-config').handler.setIdParent(component.data.classItem.GUID);
                                                $(component.elements.objectWindowContainer).data("kendoWindow").open();
                                            }
                                        }
                                        else {
                                            component.relateItems(component.data.dataItemSelected, component.data.appliedClipboardItems);
                                        }
                                        component.data.appliedClipboardItems = null;
                                    },
                                    actions: [
                                        "Pin",
                                        "Minimize",
                                        "Maximize",
                                        "Close"
                                    ]
                                }).data("kendoWindow").center();

                                component.components.windowClipboard = $(component.elements.clipboardWindow).data("kendoWindow");

                                var configurationClipboard = {
                                    seclectorPathParent: component.configuration.selectorPath,
                                    selectorPath: ".windowContainerClipboard",
                                    actionInit: component.configuration.viewModel.ActionClipboardInit,
                                    managerConfig: component.configuration.managerConfig,
                                    events: {
                                        applyItems: function (clipboardComponent, oItems) {
                                            var parentComponent = getElementsBySelectorPath(clipboardComponent.configuration.seclectorPathParent).data("component");
                                            parentComponent.data.appliedClipboardItems = oItems;
                                            parentComponent.components.windowClipboard.close();
                                        },
                                        validatedReference: function (clipboardComponent) {
                                            var parentComponent = getElementsBySelectorPath(clipboardComponent.configuration.seclectorPathParent).data("component");
                                            if (parentComponent.components.clipboardComponent.data.itemsExist) {
                                                parentComponent.components.windowClipboard.open();
                                            } else {
                                                $(parentComponent.elements.objectWindowContainer).find('.object-list-container').data('object-list-config').handler.setIdParent(parentComponent.data.classItem.GUID);
                                                $(parentComponent.elements.objectWindowContainer).data("kendoWindow").open();
                                            }
                                        },
                                        createdComponent: function (clipboardComponent) {
                                            var parentComponent = getElementsBySelectorPath(clipboardComponent.configuration.seclectorPathParent).data("component");
                                            parentComponent.components.clipboardComponent = clipboardComponent;
                                            //parentComponent.checkLoad(relationTreeItem.relTreeStateMachine.ClipboardComplete);
                                        }
                                    }
                                }

                                $(component.elements.clipboardWindow).createClipboard(configurationClipboard);
                            } else {

                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                relateItems: function (parentComponent, items) {
                    var addingNode = parentComponent.data.nodeItem;

                    $.post(parentComponent.configuration.viewModel.ActionRelateItems, { idInstance: parentComponent.configuration.idInstance, idShowObject: addingNode.IdReference, items: items })
                        .done(function (response) {
                            // stuff
                            console.log(response);
                            

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                }
            },
            triggerEvents: {
                createComponent: function (component, finishState) {
                    if (finishState == true) {
                        if (typeof component.configuration.events !== 'undefined' && component.configuration.events !== null &&
                            typeof component.configuration.events.createdComponent !== 'undefined' && component.configuration.events.createdComponent !== null) {
                            component.configuration.events.createdComponent(component);
                        }
                    } else {
                        if (typeof component.configuration.events !== 'undefined' && component.configuration.events !== null &&
                            typeof component.configuration.events.createComponent !== 'undefined' && component.configuration.events.createComponent !== null) {
                            component.configuration.events.createComponent(component);
                        }
                    }
                },
                validateReference: function (component, oItem, finishState) {
                    if (finishState == true) {
                        if (typeof component.configuration.events !== 'undefined' && component.configuration.events !== null &&
                            typeof component.configuration.events.validateReference !== 'undefined' && component.configuration.events.validateReference !== null) {
                            component.configuration.events.validateReference(component, oItem);
                        }
                    } else {
                        if (typeof component.configuration.events !== 'undefined' && component.configuration.events !== null &&
                            typeof component.configuration.events.validatedReference !== 'undefined' && component.configuration.events.validatedReference !== null) {
                            component.configuration.events.validatedReference(component, oItem);
                        }
                    }
                },
                dataBound: function (component, finishState) {
                    if (finishState == true) {
                        if (typeof component.configuration.events !== 'undefined' && component.configuration.events !== null &&
                            typeof component.configuration.events.dataBinding !== 'undefined' && component.configuration.events.dataBinding !== null) {
                            component.configuration.events.dataBinding(component);
                        }
                    } else {
                        if (typeof component.configuration.events !== 'undefined' && component.configuration.events !== null &&
                            typeof component.configuration.events.dataBound !== 'undefined' && component.configuration.events.dataBound !== null) {
                            component.configuration.events.dataBound(component);
                        }
                    }
                }

            },
            templates: {
                treeTemplate: `
                <div class="tree"></div>
                `,
                treeNode: `
                # if (typeof item.FontClass1 !== 'undefined' && item.FontClass1 !== null) {#
                    <span><i class="#: item.FontClass1 #" aria-hidden="true"></i></span>
                # } #
                # if (item.IsEncoded) {#
                    #= b64_to_utf8(item.NodeName) #
                # } else { #
                    #= item.NodeName #
                # } #
                
                # if (typeof item.ApplyChannel !== 'undefined' && item.ApplyChannel !== null) {#
                    <span><button class="btn btn-primary btn-xs btn-apply-item"><i class="fa fa-check-square" aria-hidden="true"></i></button></span>
                # } #
                # if (item.EnableAddObjects) {#
                    <span><button class="btn btn-primary btn-xs btn-add-item"><i class="fa fa fa-plus-square" aria-hidden="true"></i></button></span>
                # } #
                `,
                objectWindowContainer: '<div class="window-container-objects"></div>',
                objectWindow: '<div class="window-objects"></div>',
                objectListContainer: '<div class= "object-list-container"></div>',
                windowClipboardContainer: `
                <div class="windowContainerClipboard">
                    <div class="windowClipboard"></div>
                </div>
                `
            }
               

        }

        objectRelationTreeComponent.handler.init(this, objectRelationTreeComponent);
    }
})(jQuery);