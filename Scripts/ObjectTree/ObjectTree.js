﻿function clickAdd(element) {
    var objectTreeComponent = $(element).closest('.object-tree').data('object-tree-component');
    var selected = $(element).closest('li');
    objectTreeComponent.data.selectedDataItem = objectTreeComponent.components.Tree.dataItem(selected);
    objectTreeComponent.components.objectListWindow.open();
}

function clickApply(element) {
    var objectTreeComponent = $(element).closest('.object-tree').data('object-tree-component');
    var dataItem = objectTreeComponent.components.Tree.dataItem(element);
    var oItem = {
        GUID: dataItem.NodeId
    };
    objectTreeComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
        ChannelId: objectTreeComponent.configuration.viewModel.ChannelAppliedObjects,
        SenderId: objectTreeComponent.configuration.managerConfig.idEndpoint,
        SessionId: objectTreeComponent.configuration.managerConfig.idSession,
        ReceiverId: objectTreeComponent.configuration.viewModel.idSender,
        ViewId: objectTreeComponent.configuration.viewModel.View.IdView,
        UserId: objectTreeComponent.configuration.viewModel.IdUser,
        OItems: [oItem]
    });
}

(function ($) {
    $.fn.createObjectTree = function (configuration) {
        $('body').openPageLoader();
        var objectTreeComponent = {
            configuration: configuration,
            stateMachine:
            {
                state: {
                    initialized: true,
                    treeInitialized: false,
                    treeLoadedFirst: false,
                    treeLoaded: false,
                    dropDownInitialized: false,
                    relationTypeSelectedAuto: false,
                    relationTypeSelected: false,
                    referenceRecognized: false,
                    addHandlerAdded: false,
                    componentCreated: false
                },
                onStateChange: function (objectTreeComponent) {

                    if (!objectTreeComponent.stateMachine.state.treeInitialized ||
                        !objectTreeComponent.stateMachine.state.dropDownInitialized ||
                        !objectTreeComponent.stateMachine.state.treeLoadedFirst) {
                        $('body').closePageLoader();
                    }
                    else {
                        if (objectTreeComponent.stateMachine.state.treeInitialized &&
                            objectTreeComponent.stateMachine.state.dropDownInitialized &&
                            objectTreeComponent.stateMachine.state.treeLoadedFirst &&
                            !objectTreeComponent.stateMachine.state.componentCreated) {
                            objectTreeComponent.stateMachine.state.componentCreated = true;
                            objectTreeComponent.handler.triggerEvents.createComponent(objectTreeComponent, true);
                        } else if (objectTreeComponent.stateMachine.state.referenceRecognized &&
                            objectTreeComponent.stateMachine.state.dropDownInitialized &&
                            !objectTreeComponent.stateMachine.state.relationTypeSelectedAuto) {
                            objectTreeComponent.stateMachine.autoSelectRelationType(objectTreeComponent);
                        } else if (objectTreeComponent.stateMachine.state.referenceRecognized &&
                            objectTreeComponent.stateMachine.state.relationTypeSelectedAuto &&
                            !objectTreeComponent.stateMachine.state.treeLoaded) {
                            objectTreeComponent.stateMachine.loadTree(objectTreeComponent);
                        } else if (objectTreeComponent.stateMachine.state.referenceRecognized &&
                            objectTreeComponent.stateMachine.state.relationTypeSelectedAuto &&
                            objectTreeComponent.stateMachine.state.treeLoaded &&
                            !objectTreeComponent.stateMachine.state.addHandlerAdded) {
                            objectTreeComponent.stateMachine.addAddHandler(objectTreeComponent);
                        }  else if (objectTreeComponent.stateMachine.state.relationTypeSelected) {

                            objectTreeComponent.stateMachine.recognizeReference(objectTreeComponent, objectTreeComponent.data.idReference, objectTreeComponent.data.refIsClass);
                        }
                        $('body').closePageLoader();
                    }

                },
                treeInitialized: function (objectTreeComponent) {
                    objectTreeComponent.stateMachine.state.treeInitialized = true;
                    objectTreeComponent.stateMachine.onStateChange(objectTreeComponent);
                },
                treeLoaded: function (objectTreeComponent) {
                    if (objectTreeComponent.stateMachine.state.treeLoadedFirst) {
                        objectTreeComponent.stateMachine.state.treeLoaded = true;
                    } else {
                        objectTreeComponent.stateMachine.state.treeLoadedFirst = true;
                    }

                    objectTreeComponent.stateMachine.onStateChange(objectTreeComponent);
                },
                dropDownInitialized: function (objectTreeComponent) {
                    objectTreeComponent.stateMachine.state.dropDownInitialized = true;
                    objectTreeComponent.stateMachine.onStateChange(objectTreeComponent);
                },
                relationTypeSelectedAuto: function (objectTreeComponent) {
                    objectTreeComponent.stateMachine.state.relationTypeSelectedAuto = true;
                    objectTreeComponent.stateMachine.onStateChange(objectTreeComponent);
                },
                relationTypeSelected: function (objectTreeComponent) {
                    objectTreeComponent.stateMachine.state.relationTypeSelected = true;
                    objectTreeComponent.stateMachine.onStateChange(objectTreeComponent);
                },
                referenceRecognized: function (objectTreeComponent) {
                    objectTreeComponent.stateMachine.state.referenceRecognized = true;
                    objectTreeComponent.stateMachine.onStateChange(objectTreeComponent);
                },
                addHandlerAdded: function (objectTreeComponent) {
                    $('body').openPageLoader();
                    objectTreeComponent.stateMachine.state.addHandlerAdded = true;
                    objectTreeComponent.stateMachine.onStateChange(objectTreeComponent);
                },
                loadTree: function (objectTreeComponent) {
                    $('body').openPageLoader();
                    if (typeof objectTreeComponent.data.idReference === 'undefined') {
                        objectTreeComponent.stateMachine.state.treeLoaded = true;
                        objectTreeComponent.stateMachine.onStateChange(objectTreeComponent);
                    }
                    else {
                        objectTreeComponent.stateMachine.state.treeLoaded = false;
                        objectTreeComponent.components.Tree.dataSource.read({ idInstance: objectTreeComponent.configuration.viewModel.IdInstance, idRelationType: objectTreeComponent.data.relationType.GUID });
                    }
                },
                recognizeReference: function (objectTreeComponent, idReference, refIsClass) {
                    $('body').openPageLoader();
                    objectTreeComponent.stateMachine.state.referenceRecognized = false;
                    objectTreeComponent.stateMachine.state.relationTypeSelectedAuto = false;
                    objectTreeComponent.stateMachine.state.treeLoaded = false;
                    objectTreeComponent.stateMachine.state.addHandlerAdded = false;
                    objectTreeComponent.data.idReference = idReference;
                    objectTreeComponent.data.refIsClass = refIsClass;

                    if (objectTreeComponent.data.idReference !== undefined) {
                        $.post(objectTreeComponent.configuration.viewModel.ActionValidateReference, { refItem: { GUID: objectTreeComponent.data.idReference }, refIsClass: objectTreeComponent.data.refIsClass, idInstance: objectTreeComponent.configuration.viewModel.IdInstance })
                            .done(function (response) {
                                var element = getElementsBySelectorPath(response.SelectorPath);
                                var objectTreeComponent = $(element).data('object-tree-component')

                                console.log("connected", response);
                                if (response.IsSuccessful) {
                                    objectTreeComponent.data.idClass = getViewItemsValue(response.ViewItems, objectTreeComponent.configuration.viewModel.classItem.ViewItemId, "Other");
                                    objectTreeComponent.components.objectListComponent.handler.setIdParent(objectTreeComponent.data.idClass);
                                    changeValuesFromServerChildren(response.ViewItems, response.SelectorPath);
                                    objectTreeComponent.data.relationType = getViewItemsValue(response.ViewItems, 'dropdRelationType', "Other");
                                    if (!objectTreeComponent.stateMachine.state.relationTypeSelected) {


                                    } else {
                                        objectTreeComponent.stateMachine.state.relationTypeSelected = false;
                                    }


                                    objectTreeComponent.stateMachine.referenceRecognized(objectTreeComponent);
                                }
                                
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });
                    }
                    else {
                        objectTreeComponent.stateMachine.referenceRecognized(objectTreeComponent);
                        objectTreeComponent.stateMachine.relationTypeSelectedAuto(objectTreeComponent);
                        objectTreeComponent.components.relationTypeList.value(null);
                        $('body').closePageLoader();
                    }
                },
                autoSelectRelationType: function (objectTreeComponent) {
                    objectTreeComponent.stateMachine.state.relationTypeSelectedAuto = true;
                    if (objectTreeComponent.data.relationType !== undefined && objectTreeComponent.data.relationType !== null) {
                        objectTreeComponent.components.relationTypeList.value(objectTreeComponent.data.relationType.GUID);
                    }
                    objectTreeComponent.stateMachine.relationTypeSelectedAuto(objectTreeComponent);
                },
                addAddHandler: function (objectTreeComponent) {
                    objectTreeComponent.stateMachine.state.addHandlerAdded = false;
                    //$('.btn-add-object').unbind("click", viewModel.clickAdd);
                    //$('.btn-add-object').bind("click", viewModel.clickAdd);

                    //$('.btn-apply-object').unbind("click", viewModel.clickApply);
                    //$('.btn-apply-object').bind("click", viewModel.clickApply);

                    $(objectTreeComponent.elements.objectTreeContainer).find('.dropdRelationType').setEnabled(true);
                    $(objectTreeComponent.elements.objectTreeContainer).find('.inpFilter').setEnabled(true);
                    $(objectTreeComponent.elements.objectTreeContainer).find('.isToggledNextId').setEnabled(true);
                    objectTreeComponent.stateMachine.addHandlerAdded(objectTreeComponent);
                }

            },
            data: {
                checkedNodes: []
            },
            elements: {
            },
            components: {
            },
            handler: {
                createComponent: function (objectTreeComponent) {
                    objectTreeComponent.handler.triggerEvents.createComponent(objectTreeComponent, false);
                    objectTreeComponent.elements.objectTreeArea = $(objectTreeComponent.templates.objectTreeTemplate)
                    $(objectTreeComponent.elements.objectTreeContainer).append(objectTreeComponent.elements.objectTreeArea);
                    $(objectTreeComponent.elements.objectTreeContainer).find('.treeToolbar').kendoToolBar({
                        items: [
                            {
                                template: '<button class="k-button isListen"><i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></button>',
                            },
                            {
                                template: "<input class='dropdRelationType' style='width: 150px;' />",
                                overflow: "never"
                            },
                            {
                                template: '<input class="k-textbox inpFilter" placeholder="filter..." />'
                            },
                            {
                                template: '<button class="k-button isToggledNextId"><i class="fa fa-sort-amount-asc" aria-hidden="true"></button>'
                            },
                            {
                                template: '<button class="k-button applyItems"><i class="fa fa-check-circle" aria-hidden="true"></button>'
                            }

                        ],
                        overflowOpen: function (e) {

                        },
                        overflowClose: function (e) {
                        },
                        open: function (e) {
                        },
                        close: function (e) {
                        }
                    });

                    $.post(objectTreeComponent.configuration.viewModel.ActionGetTreeConfig, { idInstance: objectTreeComponent.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            // stuff
                            var element = getElementsBySelectorPath(response.SelectorPath);
                            var objectTreeComponent = $(element).data('object-tree-component');
                            console.log(response);

                            objectTreeComponent.data.treeViewDataSource = new kendo.data.HierarchicalDataSource(response.Result.dataSource);
                            response.Result.dataSource = objectTreeComponent.data.treeViewDataSource;
                            $.extend(response.Result,
                                {
                                    select: function (e) {
                                        var objectTreeComponent = $(e.sender.element).closest('.object-tree').data('object-tree-component');
                                        var nodeItem = objectTreeComponent.components.Tree.dataItem(e.node);
                                        var parentItem = objectTreeComponent.components.Tree.dataItem(objectTreeComponent.components.Tree.parent(e.node));

                                        var oItems = [
                                            {
                                                GUID: nodeItem.NodeId,
                                                Name: nodeItem.NodeName
                                            }
                                        ];

                                        if (typeof objectTreeComponent.configuration.events !== 'undefined' && objectTreeComponent.configuration.events !== null &&
                                            typeof objectTreeComponent.configuration.events.selectedObject !== 'undefined' && objectTreeComponent.configuration.events.selectedObject !== null) {
                                            objectTreeComponent.configuration.events.selectedObject(objectTreeComponent, oItems);
                                        } else {
                                            objectTreeComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                                ChannelId: objectTreeComponent.configuration.viewModel.ChannelSelectedObject,
                                                SenderId: objectTreeComponent.configuration.managerConfig.idEndpoint,
                                                SessionId: objectTreeComponent.configuration.managerConfig.idSession,
                                                ReceiverId: objectTreeComponent.configuration.viewModel.idSender,
                                                ViewId: objectTreeComponent.configuration.viewModel.View.IdView,
                                                UserId: objectTreeComponent.configuration.viewModel.IdUser,
                                                OItems: oItems
                                            });
                                        }
                                        

                                    }
                                });
                            $.extend(response.Result,
                                {
                                    dataBound: objectTreeComponent.handler.treeViewDataBound,
                                    change: objectTreeComponent.handler.treeViewChange
                                });
                            $.extend(response.Result, { template: kendo.template($(objectTreeComponent.templates.scriptTreeTemplate).html()) });
                            $.extend(response.Result, {
                                checkboxes: {
                                    checkChildren: true
                                },
                                check: objectTreeComponent.handler.onCheck
                            });

                            objectTreeComponent.components.Tree = $(objectTreeComponent.elements.objectTreeArea).find('.objectTreeView').kendoTreeView(response.Result).data("kendoTreeView");
                            
                            objectTreeComponent.stateMachine.treeInitialized(objectTreeComponent);
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });

                    $.post(objectTreeComponent.configuration.viewModel.ActionGetRelationTypeDropDownConfig, { idInstance: objectTreeComponent.configuration.viewModel.IdInstance, selectorPath: objectTreeComponent.configuration.selectorPath })
                        .done(function (response) {
                            console.log(response);
                            var element = getElementsBySelectorPath(response.SelectorPath);
                            var objectTreeComponent = $(element).data('object-tree-component');
                            jQuery.extend(response.Result, {
                                filter: "contains",
                                suggest: true
                            });

                            $.extend(response.Result, {
                                change: function (e) {

                                },
                                select: function (e) {
                                    var objectTreeComponent = $(e.sender.element).closest('.object-tree').data('object-tree-component');
                                    var dataItem = this.dataItem(e.item);
                                    console.log(dataItem);
                                    objectTreeComponent.data.idRelationType = dataItem.Value;
                                    objectTreeComponent.stateMachine.relationTypeSelected(objectTreeComponent);
                                }
                            });

                            $(objectTreeComponent.elements.objectTreeContainer).find('input.dropdRelationType').kendoDropDownList(response.Result);
                            objectTreeComponent.components.relationTypeList = $(objectTreeComponent.elements.objectTreeContainer).find('input.' + objectTreeComponent.configuration.viewModel.dropdRelationType.ViewItemId).data("kendoDropDownList")
                            objectTreeComponent.stateMachine.dropDownInitialized(objectTreeComponent);
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });

                    $(objectTreeComponent.elements.objectTreeContainer).find('.inpFilter').keyup(function (e) {
                        var objectTreeComponent = $(this).closest('.object-tree').data('object-tree-component');
                        if (typeof objectTreeComponent.data.filterTimeout !== 'undefined') {
                            clearTimeout(objectTreeComponent.data.filterTimeout);
                        }

                        objectTreeComponent.data.filterTimeout = setTimeout(function () {
                            var value = $(objectTreeComponent.elements.objectTreeContainer).find('.inpFilter').val();
                            $.post(objectTreeComponent.configuration.viewModel.ActionSearchTree, { searchText: value, idInstance: objectTreeComponent.configuration.viewModel.IdInstance })
                                .done(function (response) {
                                    var element = getElementsBySelectorPath(response.SelectorPath);
                                    var objectTreeComponent = $(element).data('object-tree-component');
                                    var foundNodes = response.Result;
                                    var expandableNodes = jQuery.grep(foundNodes, function (node, ixNode) {
                                        return (node.Expand == true);
                                    });
                                    while (expandableNodes.length > 0) {
                                        var newExpandableNodes = [];
                                        for (var i = 0; i < expandableNodes.length; i++) {
                                            var expandableNode = expandableNodes[i];
                                            var node = objectTreeComponent.data.treeViewDataSource.get(expandableNode.Node.NodeId);
                                            if (typeof node !== 'undefined') {
                                                var nodesToExpand = objectTreeComponent.components.Tree.findByUid(node.uid);
                                                objectTreeComponent.components.Tree.expand(nodesToExpand);
                                            } else {
                                                newExpandableNodes.push(expandableNode);
                                            }
                                        }

                                        expandableNodes = newExpandableNodes;
                                    }

                                    objectTreeComponent.data.treeViewDataSource.filter({ field: "NodeName", operator: "contains", value: $(objectTreeComponent.elements.objectTreeContainer).find('.inpFilter').val() });
                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    // stuff
                                });
                        }, 300, objectTreeComponent);
                    });

                    $(objectTreeComponent.elements.objectTreeContainer).find('.k-button').kendoButton();

                    $(objectTreeComponent.elements.objectTreeContainer).find('.k-button').click(function () {
                        var objectTreeComponent = $(this).closest('.object-tree').data('object-tree-component');
                        if ($(this).hasClass(objectTreeComponent.configuration.viewModel.isListen.ViewItemId)) {
                            $(objectTreeComponent.elements.objectTreeContainer).find('.' + objectTreeComponent.configuration.viewModel.isListen.ViewItemId).toggleMode(true);
                        } else if ($(this).hasClass(objectTreeComponent.configuration.viewModel.applyItems.ViewItemId)) {
                            var itemsToApply = [];

                            if (objectTreeComponent.data.checkedNodes.length > 0) {
                                for (var i = 0; i < objectTreeComponent.data.checkedNodes.length; i++) {
                                    var checkedNode = objectTreeComponent.data.checkedNodes[i];
                                    var checkedDataItem = objectTreeComponent.components.Tree.dataItem(checkedNode);
                                    var oItem = {
                                        GUID: checkedDataItem.NodeId
                                    }
                                    itemsToApply.push(oItem);
                                }

                                if (typeof objectTreeComponent.configuration.events !== 'undefined' && objectTreeComponent.configuration.events !== null &&
                                    typeof objectTreeComponent.configuration.events.appliedObjects !== 'undefined' && objectTreeComponent.configuration.events.appliedObjects !== null) {
                                    objectTreeComponent.configuration.events.appliedObjects(objectTreeComponent, oItems);
                                } else {
                                    objectTreeComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                        ChannelId: objectTreeComponent.configuration.viewModel.ChannelAppliedObjects,
                                        SenderId: objectTreeComponent.configuration.managerConfig.idEndpoint,
                                        SessionId: objectTreeComponent.configuration.managerConfig.idSession,
                                        ReceiverId: objectTreeComponent.configuration.viewModel.idSender,
                                        ViewId: objectTreeComponent.configuration.viewModel.View.IdView,
                                        UserId: objectTreeComponent.configuration.viewModel.IdUser,
                                        OItems: itemsToApply
                                    });
                                }
                            }
                        } else if ($(this).hasClass(objectTreeComponent.configuration.viewModel.isToggledNextId.ViewItemId)) {
                            $(objectTreeComponent.elements.objectTreeContainer).find('.' + objectTreeComponent.configuration.viewModel.isToggledNextId.ViewItemId).toggleMode(true);
                        }

                    })

                    $('.objectListDialog').kendoWindow({
                        width: '1000px',
                        height: '800px',
                        title: 'Objects',
                        visible: false,
                        modal: true,
                        actions: [
                            "Pin",
                            "Minimize",
                            "Maximize",
                            "Close"
                        ]
                    }).data("kendoWindow").center();

                    objectTreeComponent.components.objectListWindow = $('.objectListDialog').data('kendoWindow')

                    var configurationObjectList = {
                        parentComponent: objectTreeComponent,
                        actionObjectListInit: objectTreeComponent.configuration.viewModel.ActionObjectListInit,
                        templateSelector: "#templateObjectList",
                        managerConfig: objectTreeComponent.configuration.managerConfig,
                        useIdParent: true,
                        handler: {
                            initCompleteHandler: function (objectListComponent, parentComponent) {
                                //viewModel.getUrlParameters();,
                                parentComponent.components.objectListComponent = objectListComponent;
                            }
                        },
                        events: {
                            applyItems: function (items, parentComponent) {
                                parentComponent.components.objectListWindow.close();
                                parentComponent.handler.saveRelations(parentComponent, items);
                            }
                        }
                    };

                    $('.object-list-container').createObjectList(configurationObjectList);

                    changeValuesFromServerChildren(objectTreeComponent.configuration.viewModel.ViewItems, objectTreeComponent.configuration.selectorPath);
                },
                treeViewDataBound: function (e) {
                    var objectTreeComponent = $(e.sender.element).closest('.object-tree').data('object-tree-component');
                    if (typeof objectTreeComponent.configuration.expandAll !== 'undefined' && objectTreeComponent.configuration.expandAll) {
                        objectTreeComponent.components.Tree.expand(".k-item");
                    }
                    objectTreeComponent.stateMachine.treeLoaded(objectTreeComponent);
                },
                validateReference: function (objectTreeComponent, oItem) {
                    $('body').openPageLoader();
                },
                onCheck: function () {
                    var objectTreeComponent = $(event.srcElement).closest('.object-tree').data('object-tree-component');
                    var nodesToExpand = $(objectTreeComponent.elements.objectTreeContainer).find(".k-item input[type=checkbox]:checked").closest(".k-item");
                    objectTreeComponent.components.Tree.expand(nodesToExpand);
                    objectTreeComponent.data.checkedNodes = $(objectTreeComponent.elements.objectTreeContainer).find(".k-item input[type=checkbox]:checked").closest(".k-item");
                    
                    if (objectTreeComponent.data.checkedNodes.length > 0) {
                        $(objectTreeComponent.elements.objectTreeContainer).find('.' + objectTreeComponent.configuration.viewModel.applyItems.ViewItemId).setEnabled(true);
                    } else {
                        $(objectTreeComponent.elements.objectTreeContainer).find('.' + objectTreeComponent.configuration.viewModel.applyItems.ViewItemId).setEnabled(false);
                    }
                },
                getToggledNextIdCheck: function (objectTreeComponent) {
                    return $(objectTreeComponent.elements.objectTreeContainer).find('.isToggledNextId').getCheckedMode(objectTreeComponent.configuration.selectorPath);
                },
                isListening: function (objectTreeComponent) {
                    return $(objectTreeComponent.elements.objectTreeContainer).find('.isListen').getCheckedMode(objectTreeComponent.configuration.selectorPath);
                },
                saveRelations: function (objectTreeComponent, childItems) {
                    var nextId = objectTreeComponent.handler.getToggledNextIdCheck(objectTreeComponent);
                    var parentNode = {
                        NodeId: objectTreeComponent.data.selectedDataItem.NodeId,
                        NodeName: objectTreeComponent.data.selectedDataItem.NodeName
                    };
                    $.post(objectTreeComponent.configuration.viewModel.ActionSaveRelations, { parentNode: parentNode, childItems: childItems, nextOrderId: nextId, idInstance: objectTreeComponent.configuration.viewModel.IdInstance })
                        .done(function (response) {
                            // stuff
                            var element = getElementsBySelectorPath(response.SelectorPath);
                            var objectTreeComponent = $(element).data('object-tree-component');
                            console.log(response);
                            if (response.IsSuccessful) {
                                var selectedNode = objectTreeComponent.components.Tree.select();
                                for (var i in response.Result) {
                                    var newNode = response.Result[i];
                                    objectTreeComponent.components.Tree.append(newNode, selectedNode);
                                }
                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                checkMessage(objectTreeComponent, message) {
                    if (!objectTreeComponent.components.isListening()) return;

                    if (message.ChannelId == viewModel.ChannelSelectedClass) {

                        if (message.OItems != undefined) {
                            var objectItem = message.OItems[0];
                            objectTreeComponent.stateMachine.recognizeReference(objectItem.GUID, true);
                        }
                    } else if (message.ChannelId == viewModel.ChannelSelectedObject) {

                        if (message.OItems != undefined) {
                            var objectItem = message.OItems[0];
                            objectTreeComponent.stateMachine.recognizeReference(objectItem.GUID, false);
                        }
                    }
                },
                checkParameters(objectTreeComponent, urlParameterConfig) {
                    if (urlParameterConfig.classParam != undefined) {
                        objectTreeComponent.stateMachine.recognizeReference(viewModel.objectTreeComponent, urlParameterConfig.classParam, true);

                    } else if (urlParameterConfig.objectParam != undefined) {
                        objectTreeComponent.stateMachine.recognizeReference(viewModel.objectTreeComponent, urlParameterConfig.objectParam, false);
                    }
                },
                checkObject(objectTreeComponent, idObject) {
                    objectTreeComponent.stateMachine.recognizeReference(viewModel.objectTreeComponent, idObject, false);
                },
                checkClass(objectTreeComponent, idClass) {
                    objectTreeComponent.stateMachine.recognizeReference(viewModel.objectTreeComponent, idClass, true);
                },
                toggleMarkNode: function (nodeId, objectTreeComponent, toggleMode) {
                    var toggle = true;
                    var node = $(objectTreeComponent.components.Tree.findByUid(objectTreeComponent.data.treeViewDataSource.get(nodeId).uid)).find('.k-in')[0];
                    if (typeof $(node).css('background-color') !== 'undefined' && $(node).css('background-color') !== null &&
                        $(node).css('background-color') === "rgb(255, 255, 0)") {
                        toggle = false;
                    }

                    if (typeof toggleModel !== 'undefined' && toggleMode !== null) {
                        toggle = toggleMode
                    }

                    if (toggle) {
                        $(node).css('background-color', 'yellow')
                    } else {
                        $(node).css('background-color', '')
                    }
                    
                },
                filterTree: function (objectTreeComponent, values, isId) {
                    if (typeof values === 'undefined') return;
                    if (typeof isId === 'undefined') isId = false;

                    var filter = {
                        logic: "or",
                        filters: []
                    };
                    for (var ix = 0; ix < values.length; ix++) {
                        var value = values[ix]
                        if (isId) {
                            filter.filters.push({ field: "NodeId", operator: "contains", value: value })
                        } else {
                            filter.filters.push({ field: "NodeName", operator: "contains", value: value })
                        }
                    }
                    
                    objectTreeComponent.data.treeViewDataSource.filter(filter);
                },
                triggerEvents: {
                    createComponent: function (objectTreeComponent, finishState) {
                        if (finishState == true) {
                            if (typeof objectTreeComponent.configuration.events !== 'undefined' && objectTreeComponent.configuration.events !== null &&
                                typeof objectTreeComponent.configuration.events.createdComponent !== 'undefined' && objectTreeComponent.configuration.events.createdComponent !== null) {
                                objectTreeComponent.configuration.events.createdComponent(objectTreeComponent);
                            }
                        } else {
                            if (typeof objectTreeComponent.configuration.events !== 'undefined' && objectTreeComponent.configuration.events !== null &&
                                typeof objectTreeComponent.configuration.events.createComponent !== 'undefined' && objectTreeComponent.configuration.events.createComponent !== null) {
                                objectTreeComponent.configuration.events.createComponent(objectTreeComponent);
                            }
                        }
                    },
                    validateReference: function (objectTreeComponent, oItem, finishState) {
                        if (finishState == true) {
                            if (typeof objectTreeComponent.configuration.events !== 'undefined' && objectTreeComponent.configuration.events !== null &&
                                typeof objectTreeComponent.configuration.events.validateReference !== 'undefined' && objectTreeComponent.configuration.events.validateReference !== null) {
                                objectTreeComponent.configuration.events.validateReference(objectTreeComponent, oItem);
                            }
                        } else {
                            if (typeof objectTreeComponent.configuration.events !== 'undefined' && objectTreeComponent.configuration.events !== null &&
                                typeof objectTreeComponent.configuration.events.validatedReference !== 'undefined' && objectTreeComponent.configuration.events.validatedReference !== null) {
                                objectTreeComponent.configuration.events.validatedReference(objectTreeComponent, oItem);
                            }
                        }
                    },
                }
            },
            templates: {
                objectTreeTemplate: `
<div class="controlArea">
    <div class="treeToolbar">

    </div>
    <div class="objectTreeView">

    </div>
    <div class="objectListDialog">
        <div class="object-list-container">
            <div class="object-list"></div>
        </div>
    </div>
</div>
         `,
                scriptTreeTemplate: `
<script id="treeview-template" type="text/kendo-ui-template">
    #: item.NodeName #
    <button class="btn btn-primary btn-xs btn-add-object" onclick="clickAdd(this)"><i class="fa fa-plus-circle smallFont" aria-hidden="true"></i></button>
    <button class="btn btn-primary btn-xs btn-apply-object" onclick="clickApply(this)"><i class="fa fa-check-circle smallFont" aria-hidden="true"></i></button>

</script>
                `
            }
        }

        objectTreeComponent.elements.objectTreeContainer = this;
        $(objectTreeComponent.elements.objectTreeContainer).addClass('object-tree');
        $(objectTreeComponent.elements.objectTreeContainer).data('object-tree-component', objectTreeComponent);

        $.post(objectTreeComponent.configuration.actionInit, { idInstance: objectTreeComponent.configuration.IdInstance, selectorPath: objectTreeComponent.configuration.selectorPath })
            .done(function (response) {
                // stuff
                console.log(response);
                if (response.IsSuccessful) {
                    var selectorPath = response.SelectorPath;
                    var element = getElementsBySelectorPath(selectorPath);
                    var objectTreeComponent = $(element).data('object-tree-component');
                    objectTreeComponent.configuration.viewModel = response.Result;
                    objectTreeComponent.handler.createComponent(objectTreeComponent);
                } else {

                }

            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    };
})(jQuery);