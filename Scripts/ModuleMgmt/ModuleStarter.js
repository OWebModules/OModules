﻿(function ($) {
    $.fn.createModuleStarter = function (configuration) {

        $(this).data('modulestarter-config', {
            modulesContainerElement: null,
            toolbarElement: null,
            gridContainerElement: null,
            gridElement: null,
            configuration: configuration,
            lastClickedController: {
                NameController: null,
                IdObject: null,
                NameView: null
            },
            openView: null,
            gridItem: null,
            gridChangeHandler: null,
            idObject: configuration.idObject,
            gridRefresh: null,
            validateReference: function (idObject, validateReferenceCallback) {
                moduleStarterItem.configuration.idObject = idObject;
                moduleStarterItem.idObject = idObject;
                $.post(moduleStarterItem.configuration.viewModel.ActionConfig.ActionValidateReference, { idObject: idObject })
                    .done(function (response) {
                        console.log(response);
                        if (response.IsSuccessful) {

                            if (moduleStarterItem.configuration.showIdObject === undefined) {
                                $(moduleStarterItem.modulesContainerElement).find('.modulesNameObject').html(response.Result.Name);
                            } else {
                                $(moduleStarterItem.modulesContainerElement).find('.modulesNameObject').val(response.Result.Name);
                            }

                            moduleStarterItem.idObject = response.Result.GUID;
                            moduleStarterItem.gridRefresh();

                            $('.id-object').val(response.Result.GUID);
                        }

                        if (validateReferenceCallback !== undefined) {
                            validateReferenceCallback();
                        }

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
                moduleStarterItem.gridRefresh();
            },
            initStarter: function () {
                var labelObjectCaption = "Object:";

                var labelObject = jQuery.grep(moduleStarterItem.configuration.viewModel.GuiEntries, function (e) {
                    return e.NameGuiEntry === "labelObject";
                });

                if (labelObject.length > 0) {
                    labelObjectCaption = labelObject[0].GuiCaptions[0].NameGuiCaption;
                }

                if (moduleStarterItem.configuration.showIdObject !== undefined && moduleStarterItem.configuration.showIdObject) {
                    $(moduleStarterItem.toolbarElement).kendoToolBar({
                        items: [
                            {
                                template: '<label class="modulesLabelObjectCaption">' + labelObjectCaption + '</label>'
                            },
                            {
                                template: '<input type="text" class="modulesNameObject" readonly size="50" />'
                            },
                            {
                                type: "separator"
                            },
                            {
                                template: '<button class="k-button modulesButtonNewWindow"><i class="fa fa-external-link-square" aria-hidden="true"></i></button>'

                            },
                            {
                                template: '<button class="k-button modulesButtonRelationEditor"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>'
                            },
                            {
                                type: "separator"
                            },
                            {
                                template: '<label class="k-label">Id Object:</label>'
                            },
                            {
                                template: '<input type="text" class="k-input id-object" readonly size="30" />'
                            }
                        ]
                    });
                } else {
                    $(moduleStarterItem.toolbarElement).kendoToolBar({
                        items: [
                            {
                                template: '<label class="modulesLabelObjectCaption">' + labelObjectCaption + '</label>'
                            },
                            {
                                template: '<label class="modulesNameObject">-</label>'
                            },
                            {
                                type: "separator"
                            },
                            {
                                template: '<button class="k-button modulesButtonRelationEditor"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>'
                            },
                            {
                                template: '<button class="k-button modulesButtonNewWindow"><i class="fa fa-external-link-square" aria-hidden="true"></i></button>'

                            }
                        ]
                    });
                }


                $(moduleStarterItem.modulesContainerElement).find('.modulesButtonNewWindow').toggleMode();

                $.post(moduleStarterItem.configuration.viewModel.ActionConfig.ActionGetGridConfig)
                    .done(function (response) {
                        console.log(response);
                        $.extend(response.dataSource.transport.read, {
                            data: function () {
                                return { idInstance: moduleStarterItem.configuration.viewModel.IdInstance, idObject: moduleStarterItem.idObject };
                            }
                        });

                        var dataSource = new kendo.data.DataSource(response.dataSource);
                        response.dataSource = dataSource;

                        $.extend(response, {
                            change: moduleStarterItem.gridChangeHandler,
                            dataBound: function () {
                                $('.modules-link-button').on('click', function (e) {
                                    moduleStarterItem.openView(this);
                                });
                                if (moduleStarterItem.configuration.isActionsAttached === undefined || moduleStarterItem.configuration.isActionsAttached === null) {
                                    moduleStarterItem.configuration.isActionsAttached = true;


                                    $('.modulesButtonRelationEditor').on('click', function (e) {
                                        var link = moduleStarterItem.configuration.viewModel.ActionConfig.ActionRelationEditor;
                                        var paramsAdded = false;
                                        if (moduleStarterItem.configuration.idObject !== null && moduleStarterItem.configuration.idObject !== null) {
                                            link += '?Object=' + moduleStarterItem.configuration.idObject;
                                            paramsAdded = true;
                                        }
                                        if (moduleStarterItem.configuration.sender !== undefined && moduleStarterItem.configuration.sender !== null) {
                                            if (paramsAdded) {
                                                link += "&";
                                            } else {
                                                link += "?";
                                            }
                                            link += "Sender=" + moduleStarterItem.configuration.sender;
                                        }
                                        window.open(link, "_blank");
                                    });
                                }

                                moduleStarterItem.configurationEmbeddedNameEdit = {
                                    selectorPath: ['.modulesNameObject'],
                                    idItem: moduleStarterItem.idObject,
                                    typeItem: 'Object',
                                    showWithoutEvent: true,
                                    actions: {
                                        init: moduleStarterItem.configuration.viewModel.ActionConfig.ActionEmbeddedNameEditInit
                                    },
                                    events: {
                                        completed: function (component) {
                                            var selectorPath = [];
                                            for (var i = 0; i < component.configuration.selectorPath.length; i++) {
                                                var selector = component.configuration.selectorPath[i];
                                                selectorPath.push(selector);
                                            }
                                            selectorPath.push('.cancel-edit');
                                        },
                                        savedName: function (selectorPath, name) {

                                            foundElements = getElementsBySelectorPath(selectorPath);
                                            if (foundElements.length > 0) {
                                                $(foundElements).html(name);
                                                $('.modulesNameObject').createEmbeddedNameEdit(moduleStarterItem.configurationEmbeddedNameEdit);
                                            }

                                        }
                                    }
                                }
                                $('.modulesNameObject').createEmbeddedNameEdit(moduleStarterItem.configurationEmbeddedNameEdit );

                            }
                        });

                        moduleStarterItem.gridItem = $(moduleStarterItem.gridElement).kendoGrid(response).data("kendoGrid");

                        moduleStarterItem.gridItem.wrapper.find(".k-pager")
                            .before('<a href="#" class="k-link prev">Prev</a>')
                            .after('<a href="#" class="k-link next">Next</a>')
                            .parent()
                            .append('<span class="pagesize" style="margin-left:40px">Page To: <input /></span>')
                            .delegate("a.prev", "click", function () {
                                dataSource.page(dataSource.page() - 1);
                            })
                            .delegate("a.next", "click", function () {
                                dataSource.page(dataSource.page() + 1);
                            })
                            .delegate(".pagesize input", "change", function () {
                                dataSource.page(parseInt(this.value, 10));
                            });

                        moduleStarterItem.gridRefresh();

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
            }
        });

        var moduleStarterItem = $(this).data('modulestarter-config');

        moduleStarterItem.gridRefresh = function () {
            moduleStarterItem.gridItem.dataSource.read();
        };

        moduleStarterItem.gridChangeHandler = function (e) {
            var currentDataItem = moduleStarterItem.gridItem.dataItem(this.select());
            if (currentDataItem !== undefined) {
                console.log(currentDataItem);


            }
        };

        

        moduleStarterItem.openView = function (e) {
            var dataItem = moduleStarterItem.gridItem.dataItem($(e).closest('tr'));
            console.log(dataItem);
            moduleStarterItem.lastClicked = dataItem;

            var link = moduleStarterItem.configuration.viewModel.baseUrl + moduleStarterItem.lastClicked.NameController + '/' + moduleStarterItem.lastClicked.NameView;

            var paramsAdded = false;
            if (moduleStarterItem.lastClicked.IdObject !== undefined && moduleStarterItem.lastClicked.IdObject !== null) {
                link += '?Object=' + moduleStarterItem.lastClicked.IdObject;
                paramsAdded = true;
            }
            if (moduleStarterItem.configuration.sender !== undefined && moduleStarterItem.configuration.sender !== null) {
                if (paramsAdded) {
                    link += "&";
                } else {
                    link += "?";
                }
                link += "Sender=" + moduleStarterItem.configuration.sender;
            }

            var buttonNewWindows = $(moduleStarterItem.modulesContainerElement).find('.modulesButtonNewWindow');

            if (buttonNewWindows.isToggled()) {
                window.open(link, "_blank");
            } else {
                document.location = link;
            }

            $.post(moduleStarterItem.configuration.viewModel.ActionConfig.ActionSetViewToClass, { idObject: dataItem.IdObject, idView: dataItem.IdView })
                .done(function (response) {
                    console.log(response);
                    if (response.IsSuccessful) {


                        moduleStarterItem.gridRefresh();
                    }



                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        };

        moduleStarterItem.modulesContainerElement = this;
        $(moduleStarterItem.modulesContainerElement).html('');

        moduleStarterItem.toolbarElement = $('<div class="modulesToolbar"></div>');
        $(moduleStarterItem.modulesContainerElement).append(moduleStarterItem.toolbarElement);

        moduleStarterItem.gridContainerElement = $('<div class="modulesGridContainer" style="height: calc(100vh - 270px) !important;"></div>');
        $(moduleStarterItem.modulesContainerElement).append(moduleStarterItem.gridContainerElement);

        moduleStarterItem.gridElement = $('<div class="modulesGird"></div>');
        $(moduleStarterItem.gridContainerElement).append(moduleStarterItem.gridElement);

        if (moduleStarterItem.configuration.viewModel == undefined || moduleStarterItem.configuration.viewModel == null) {
            $.post(moduleStarterItem.configuration.actionModuleStarter)
                .done(function (response) {
                    moduleStarterItem.configuration.viewModel = response.Result;
                    moduleStarterItem.initStarter();

                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        } else {
            moduleStarterItem.initStarter();
        }
        
        
    };
}) (jQuery);