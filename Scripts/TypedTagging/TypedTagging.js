﻿(function ($) {
    $.fn.createTypedTaggingList = function (configuration) {

        $(this).data('typed-tagging-list-config', {
            configuration: configuration,
            validateReference: function (idObject, validateReferenceCallback) {
                $('.typed-tagging-list').openPageLoader();
                $('.typed-tagging-list').find('.ref-panel').html('');
                $('.typed-tagging-list').data('typed-tagging-list-config').idObject = idObject;
                if ($('.typed-tagging-list').data('typed-tagging-list-config').idObject === undefined || $('.typed-tagging-list').data('typed-tagging-list-config').idObject === null) return;
                var refItem = {
                    GUID: idObject
                }
                $.post($('.typed-tagging-list').data('typed-tagging-list-config').configuration.actionValidateReference, {
                    refItem: refItem,
                    idInstance: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.idInstance})
                    .done(function (response) {
                        console.log(response);
                        if (response.IsSuccessful) {
                            $('.typed-tagging-list').data('typed-tagging-list-config').getTags($('.typed-tagging-list').data('typed-tagging-list-config').idObject);
                            
                        }

                        if (validateReferenceCallback !== undefined) {
                            validateReferenceCallback();
                        }

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });

            },
            getTags: function (idRef) {
                $('.typed-tagging-list').openPageLoader();
                $.post($('.typed-tagging-list').data('typed-tagging-list-config').configuration.actionGetTags, { idReference: idRef, idInstance: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.idInstance })
                    .done(function (response) {
                        console.log("connected", response);
                        for (var ix in response.Tags) {
                            var reference = response.Tags[ix];
                            var id = reference.Id;
                            var name = reference.Name;
                            var tags = reference.Tags;

                            var refContainer = $('#' + id);
                            if (refContainer.length === 0) {

                                var refHtml = `<button data-toggle="collapse" data-target="#{id}" class="btn btn-default btn-block"><p class ="refTitle">{name}<span id="{id}_badge" class="badge" style="margin-left:8px">{tagsCount}</span></p></button>
                            <div id="{id}" class ="collapse tagContainer">
                            </div>`;

                                refHtml = refHtml.replace('{id}', id).replace('{id}', id).replace('{name}', name).replace('{id}', id).replace('{tagsCount}', tags.length);
                                refContainer = $(refHtml);
                                $('.typed-tagging-list').find('.ref-panel').append(refContainer);
                            }

                            var tagContainer = $('#' + id);

                            for (var jx in tags) {
                                var tag = tags[jx];


                                var list = $('<li></li>');

                                var well = $('<div class="itemContainer" id="' + tag.IdTag + '"></div>')
                                var buttonToolbar = $('<div class="btn-toolbar mb-3 btn-default" role="toolbar" aria-label="Toolbar with button groups">')
                                var buttonGroup = $('<div class="btn-group mr-2 float-left" role="group" aria-label="First group"></div>')
                                var openDiv = $('<button type="button" class="btn btn-default btnOpen"><i class="fa fa-external-link fa-lg" aria-hidden="true"></i></button>')
                                var applyDiv = $('<button type="button" class="btn btn-default btnApply"><i class="fa fa-check-square-o fa-lg" aria-hidden="true"></i></button>')
                                var editDiv = $('<button type="button" class="btn btn-default btnEdit"><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i></button>')
                                var sendDiv = $('<button type="button" class="btn btn-default btnSend">' + tag.NameTag + '</button>')
                                $(sendDiv).data('encodedname', tag.EncodedNameTag);
                                $(sendDiv).kendoTooltip({
                                    position: "top",
                                    animation: {
                                        open: {
                                            effects: "zoom",
                                            duration: 150
                                        }
                                    },
                                    showAfter: 500,
                                    width: "500",
                                    autoHide: true,
                                    content: function (e) {
                                        var result = $(e.target).html();
                                        var encodedName = $(e.target).data('encodedname');
                                        if (typeof (encodedName) !== 'undefined') {
                                            result = b64_to_utf8(encodedName);
                                        }
                                        return result;
                                    }
                                }).data("kendoTooltip");

                                $(list).append(well);
                                $(well).append(buttonGroup);
                                $(buttonGroup).append(openDiv);
                                $(buttonGroup).append(applyDiv);
                                $(buttonGroup).append(editDiv);
                                $(buttonGroup).append(sendDiv);
                                $(tagContainer).append(list);

                                $(well).data('tag', tag);
                            }


                        }
                        $(".collapse").on('shown.bs.collapse', function (e) {
                            var classId = $(e.target).attr("id");
                            var oItems = [{
                                GUID: classId
                            }];
                            $('.typed-tagging-list').data('typed-tagging-list-config').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                ChannelId: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.channelSelectedClass,
                                SenderId: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.managerConfig.idEndpoint,
                                SessionId: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.managerConfig.idSession,
                                UserId: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.idUser,
                                OItems: oItems
                            });
                        });
                        $('.typed-tagging-list').closePageLoader();
                        $('.btn').click(function () {
                            var div = $(this).closest('.itemContainer');
                            var tag = div.data('tag');

                            if ($(this).hasClass('btnOpen')) {
                                window.open(tag.UrlModuleStarter, '_blank');
                            } else if ($(this).hasClass('btnApply')) {
                                items = [
                                    {
                                        GUID: tag.IdTypedTag
                                    }
                                ];
                                $('.typed-tagging-list').data('typed-tagging-list-config').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                    ChannelId: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.channelAppliedObjects,
                                    SenderId: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.managerConfig.idEndpoint,
                                    SessionId: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.managerConfig.idSession,
                                    UserId: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.idUser,
                                    OItems: items
                                });

                                if ($('.typed-tagging-list').data('typed-tagging-list-config').configuration.applyItems !== undefined && $('.typed-tagging-list').data('typed-tagging-list-config').configuration.applyItems !== null) {
                                    $('.typed-tagging-list').data('typed-tagging-list-config').configuration.applyItems(items);
                                }
                                
                            } else if ($(this).hasClass('btnEdit')) {
                                window.open($('.typed-tagging-list').data('typed-tagging-list-config').configuration.actionRelationEditor + '?Object=' + tag.IdTag, '_blank');
                            } else if ($(this).hasClass('btnSend')) {
                                items = [
                                    {
                                        GUID: tag.IdTag
                                    }
                                ];
                                $('.typed-tagging-list').data('typed-tagging-list-config').configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                    ChannelId: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.channelSelectedObject,
                                    SenderId: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.managerConfig.idEndpoint,
                                    SessionId: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.managerConfig.idSession,
                                    UserId: $('.typed-tagging-list').data('typed-tagging-list-config').configuration.idUser,
                                    OItems: items
                                });
                            }
                            console.log(tag);
                        });

                        $('.typed-tagging-list').find('.ref-panel').css('overflow-y', 'auto');
                        $('.typed-tagging-list').find('.ref-panel').css('overflow', 'auto');
                        refPanelHeadHeight = 90;
                        $('.typed-tagging-list').find('.ref-panel').css('height', window.innerHeight - refPanelHeadHeight);
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });



            }
        });


        $(this).addClass("typed-tagging-list-config");
        var refPanel = $("<ul class='ref-panel'></ul>");
        $(this).append(refPanel);

        if ($('.typed-tagging-list').data('typed-tagging-list-config').configuration !== undefined) {

            $('.typed-tagging-list').data('typed-tagging-list-config').configuration.createdCallback();
        }



        
    };
}) (jQuery);