﻿var autoRelatorContainer = "";
(function ($) {
    $.fn.createAutoRelator = function (configuration) {
        $(this).data("auto-relator", {
            elements: {
                autoRelatorContainerElement: null,
                autoRelatorElement: null
            },
            configuration: configuration,
            viewModel: null,
            handler: {
                validateReference: function (oItem) {
                    if (autoRelatorContainer.viewModel.oItem !== undefined && oItem.GUID === autoRelatorContainer.viewModel.oItem.GUID) {
                        autoRelatorContainer.viewModel.oItem.Val_String = oItem.Val_String;
                        if (autoRelatorContainer.viewModel.oItem.Val_String !== undefined && autoRelatorContainer.viewModel.oItem.Val_String !== null) {
                            
                            return;
                        }

                    }
                    autoRelatorContainer.viewModel.oItem = oItem;

                    autoRelatorContainer.viewModel.oItem.Val_String = htmlDecode(autoRelatorContainer.viewModel.oItem.Val_String);
                    autoRelatorContainer.handler.validateReferenceObjectName(autoRelatorContainer.viewModel.oItem.GUID, autoRelatorContainer.viewModel.oItem.Val_String);
                    $('.typed-tagging-list').data('typed-tagging-list-config').validateReference(autoRelatorContainer.viewModel.oItem.GUID);
                },
                validateText: function (value) {
                    autoRelatorContainer.elements.autoRelatorContainerElement.find('.textAreaUserStories').val($('<textarea />').html(value).text());
                    autoRelatorContainer.viewModel.textChanged = true;
                },
                toggleDisplayType: function (e) {
                    if (e === undefined) {
                        $('.analyzedText').show();
                        $('.foundItem').show();
                        $('.classField').hide();
                    }
                    else {
                        var button = $(e);
                        if (!button.hasClass('btn-primary')) {
                            $('.btnDisplayType').removeClass('btn-primary');
                            button.addClass('btn-primary');

                            if (button.attr('id') === "btnShowTextWithItems") {
                                $('.analyzedText').show();
                                $('.foundItem').show();
                                $('.classField').hide();
                            } else if (button.attr('id') === "btnShowOnlySelected") {
                                $('.analyzedText').hide();
                                $('.foundItem').hide();
                                $('.classField').hide();
                                var parent = $('.btn-success').parent();
                                parent.find('.foundItem').show();
                                parent.find('.classField').show();
                            } else {
                                $('.analyzedText').hide();
                                $('.foundItem').show();
                                $('.classField').show();
                            }
                        }
                    }
                },
                checkSaveTags: function (e) {

                    if ($(e).find('.buttonRel.btn-success').length === 0) {
                        $(e).find('.saveItems').prop("disabled", true);
                    } else {
                        $(e).find('.saveItems').prop("disabled", false);
                    }
                },
                saveOntoRelations: function (e) {
                    var panel = $(e).closest(".panel");
                    var selected = panel.find('.btn-success');
                    var ids = [];
                    selected.map(function () {
                        ids.push($(this).attr('id'));
                    });

                    $.post(autoRelatorContainer.viewModel.ActionSaveOntoRelations, { idInstance: autoRelatorContainer.viewModel.IdInstance, idsOntologyJoins: ids })
                        .done(function (response) {
                            console.log("connected", response);
                            if (response.IsSuccessful) {

                                panel.find('.btn-success').removeClass('btn-success').addClass('btn-default');

                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                }, saveItems: function (e) {
                    var panel = $(e).closest(".panel");
                    var selected = panel.find('.btn-success');
                    var ids = [];
                    selected.map(function () {
                        ids.push($(this).attr('id'));
                    });
                    var idRelationType = autoRelatorContainer.viewModel.relationTypeList.value();
                    var autoRelatorConfiguration = $(e).closest('.auto-relator').data('auto-relator').configuration;
                    if ($(e).closest('.auto-relator').find('.btnTypedTag').hasClass('btn-success')) {
                        idRelationType = null;
                    }
                    var orderId = $($(e).closest('.auto-relator').find('.inpOrderId')[2]).data("kendoNumericTextBox").value();
                    $.post(autoRelatorContainer.viewModel.ActionSaveRelatorTags, { idInstance: autoRelatorContainer.viewModel.IdInstance, idsTextParts: ids, idRelationType: idRelationType, orderId: orderId })
                        .done(function (response) {
                            console.log("connected", response);
                            if (response.IsSuccessful) {
                                var oItems = [];
                                for (var ix in response.ResultItem.TypedTags) {
                                    var typedTag = response.ResultItem.TypedTags[ix];

                                    oItems.push({ GUID: typedTag.IdTypedTag });
                                }

                                if (autoRelatorConfiguration.applyItems !== undefined && autoRelatorConfiguration.applyItems !==  null) {
                                    autoRelatorConfiguration.applyItems(oItems);
                                }
                                
                                //autoRelatorConfiguration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                //    ChannelId: autoRelatorContainer.viewModel.ChannelAppliedObjects,
                                //    SenderId: autoRelatorConfiguration.managerConfig.idEndpoint,
                                //    SessionId: autoRelatorConfiguration.managerConfig.idSession,
                                //    ReceiverId: autoRelatorContainer.viewModel.idSender,
                                //    ViewId: autoRelatorContainer.viewModel.View.IdView,
                                //    UserId: autoRelatorContainer.viewModel.IdUser,
                                //    OItems: oItems
                                //});

                                autoRelatorConfiguration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                    ChannelId: autoRelatorContainer.viewModel.ChannelSavedTypeTag,
                                    SenderId: autoRelatorConfiguration.managerConfig.idEndpoint,
                                    SessionId: autoRelatorConfiguration.managerConfig.idSession,
                                    ReceiverId: autoRelatorContainer.viewModel.idSender,
                                    ViewId: autoRelatorContainer.viewModel.View.IdView,
                                    UserId: autoRelatorContainer.viewModel.IdUser,
                                    OItems: oItems
                                });
                                $('.typed-tagging-list').data('typed-tagging-list-config').validateReference(autoRelatorContainer.viewModel.oItem.GUID);


                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                saveSelection: function () {
                    var orderId = $(autoRelatorContainer.elements.autoRelatorContainerElement.find('.inpOrderId')[2]).data("kendoNumericTextBox").value();
                    autoRelatorContainer.viewModel.selected.text = b64EncodeUnicode(autoRelatorContainer.viewModel.selected.text);
                    $.post(autoRelatorContainer.viewModel.ActionSaveNewObject, { idInstance: autoRelatorContainer.viewModel.IdInstance, request: autoRelatorContainer.viewModel.selected, orderId: orderId })
                        .done(function (response) {
                            console.log("connected", response);
                            if (response.IsSuccessful) {
                                var oItems = [];
                                for (var ix in response.ResultItem.TypedTags) {
                                    var typedTag = response.ResultItem.TypedTags[ix];

                                    oItems.push({ GUID: typedTag.IdTypedTag });
                                }

                                if (autoRelatorContainer.configuration.applyItems !== undefined && autoRelatorContainer.configuration.applyItems !== null) {
                                    autoRelatorContainer.configuration.applyItems(oItems);
                                }

                                autoRelatorContainer.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                                    ChannelId: autoRelatorContainer.viewModel.ChannelSavedTypeTag,
                                    SenderId: autoRelatorContainer.configuration.managerConfig.idEndpoint,
                                    SessionId: autoRelatorContainer.configuration.managerConfig.idSession,
                                    ReceiverId: autoRelatorContainer.viewModel.idSender,
                                    ViewId: autoRelatorContainer.viewModel.View.IdView,
                                    UserId: autoRelatorContainer.viewModel.IdUser,
                                    OItems: oItems
                                });

                                $('.typed-tagging-list').data('typed-tagging-list-config').validateReference(viewModel.oItem.GUID);

                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                checkSaveSelection: function () {
                    var selectedText = autoRelatorContainer.elements.autoRelatorContainerElement.find('.textAreaUserStories').getSelectedText();
                    console.log(selectedText);
                    var valueClass = autoRelatorContainer.viewModel.classList.value();
                    var indexClass = autoRelatorContainer.viewModel.classList.selectedIndex,
                        dataItemClass;
                    var valueRelationType = autoRelatorContainer.viewModel.relationTypeList.value();

                    autoRelatorContainer.viewModel.selected = {
                        text: selectedText,
                        idClass: valueClass,
                        idRelationType: valueRelationType,
                        saveTypedTag: autoRelatorContainer.elements.autoRelatorContainerElement.find('.btnTypedTag').hasClass('btn-success')
                    };

                    var html = "";
                    var length = autoRelatorContainer.elements.autoRelatorContainerElement.find('.textAreaUserStories').val().length;
                    var badgeHtml = '<span class="badge badge-light">' + length + '</span>:&nbsp;';
                    if (autoRelatorContainer.viewModel.selected.text !== '') {
                        html = autoRelatorContainer.viewModel.selected.text;
                        
                    } else {
                        html = autoRelatorContainer.elements.autoRelatorContainerElement.find('.textAreaUserStories').val();
                    }
                    html = badgeHtml + html;
                    autoRelatorContainer.elements.autoRelatorContainerElement.find('.outputAutoComplete').html(html);

                    //if (indexClass == 0) return;
                    dataItemClass = autoRelatorContainer.viewModel.classList.dataItem(indexClass);




                    if (selectedText !== "" && dataItemClass !== undefined) {
                        autoRelatorContainer.elements.autoRelatorContainerElement.find('.btnSaveObject').attr('disabled', false);

                    }
                    else {
                        autoRelatorContainer.elements.autoRelatorContainerElement.find('.btnSaveObject').attr('disabled', true);
                    }

                    $('#autoCompleteArea').hide();
                    autoRelatorContainer.viewModel.textChanged = true;



                    if (autoRelatorContainer.viewModel.timeoutWordInput !== undefined) {
                        clearTimeout(autoRelatorContainer.viewModel.timeoutWordInput);
                    }
                },
                createResultAreaOnto: function (response, jqElem) {
                    var lines = "";
                    for (var i in response.AutoRelationAnaylze) {
                        var line = "<p>";
                        var autoRelationAnalyze = response.AutoRelationAnaylze[i];
                        var ixText = 0;

                        var ontologyJoin = autoRelationAnalyze.OntologyJoin;

                        var textPart = null;
                        var name = "";
                        if (ontologyJoin.OItem3 !== undefined && ontologyJoin.OItem3 !== null) {
                            name = ontologyJoin.OItem3.RefItem.Name;
                        }

                        if (autoRelationAnalyze.TextPart1 !== undefined && autoRelationAnalyze.TextPart1 !== null) {
                            textPart = autoRelationAnalyze.TextPart1;
                            name = textPart.Text + ' [' + ontologyJoin.OItem1.RefItem.Name + '] -> ' + name + ' -> ' + autoRelatorContainer.viewModel.nameOfObject;
                        } else {
                            textPart = autoRelationAnalyze.TextPart2;
                            name = autoRelatorContainer.viewModel.nameOfObject + ' -> ' + name + ' -> ' + textPart.Text + ' [' + ontologyJoin.OItem2.RefItem.Name + ']';
                        }

                        var colorClass = "btn-primary";

                        if (autoRelationAnalyze.Exists) {
                            colorClass = "btn-default";
                        }

                        line += "<span><button id='" + autoRelationAnalyze.Id + "' class='btn " + colorClass + " btn-xs buttonRel foundItem' >" + name + "</button></span>";

                        ixText = textPart.IxStart + textPart.IxEnd;

                        line += "</p>";
                        lines += line;




                    }

                    jqElem.html('');
                    var panel = $('<div class="panel panel-default"></div>');
                    var panelHeader = $('<div class="panel-heading clearfix"></div>');
                    var inputGroup = $('<div class="input-group"></div>')

                    panelHeader.append(inputGroup);
                    var buttonGroup = $('<div class="btn-group"></div>');

                    inputGroup.append(buttonGroup);
                    var buttonUse = $('<button class="btn btn-xs saveItems" onClick="autoRelatorContainer.handler.saveOntoRelations(this)"><i class="fa fa-check-square" aria-hidden="true"></i></button>');
                    buttonGroup.append(buttonUse);

                    var buttonShowTextWithItems = $('<button id="btnShowTextWithItems" class="btn btn-xs btnDisplayType btn-primary" onClick="autoRelatorContainer.handler.toggleDisplayType(this)">Text with items</button>');
                    buttonGroup.append(buttonShowTextWithItems);

                    var buttonShowItems = $('<button id="btnShowItems" class="btn btn-xs btnDisplayType btn-secondary" onClick="autoRelatorContainer.handler.toggleDisplayType(this)">Items</button>');
                    buttonGroup.append(buttonShowItems);

                    var buttonOnlySelected = $('<button id="btnShowOnlySelected" class="btn btn-xs btnDisplayType btn-secondary" onClick="autoRelatorContainer.handler.toggleDisplayType(this)">Selected Items</button>');
                    buttonGroup.append(buttonOnlySelected);

                    panel.append(panelHeader);

                    var template = $('<div class="templateArea" style="height: 250px;width: 100%;overflow: auto;white-space: nowrap;">' + lines + '</div>')
                    panel.append(template);

                    jqElem.append(panel);

                    autoRelatorContainer.handler.toggleDisplayType();

                    jqElem.find('.buttonRel').click(function () {

                        if ($(this).hasClass('btn-primary')) {
                            $(this).removeClass('btn-primary');
                            $(this).addClass('btn-success');
                        } else if ($(this).hasClass('btn-success')) {
                            $(this).removeClass('btn-success');
                            $(this).addClass('btn-primary');

                        }

                        autoRelatorContainer.handler.checkSaveTags();
                    });

                    autoRelatorContainer.handler.checkSaveTags();
                },
                createResultArea: function (response, jqElem) {
                    var lines = "";
                    for (var i in response.AutoRelationAnaylze) {
                        var line = "<p>";
                        var autoRelationAnalyze = response.AutoRelationAnaylze[i];
                        var ixText = 0;
                        var text = "";
                        for (var j in autoRelationAnalyze.RangeRefItems) {
                            var rangeRefItem = autoRelationAnalyze.RangeRefItems[j];

                            if (rangeRefItem.FoundInText) {
                                text = response.AnalyzeText.substring(ixText, rangeRefItem.IxStart - 1);
                            }
                            line += "<span class='analyzedText'>" + text + "</span>";
                            line += "<span><div class='btn-group mr-2 float-left' role='group' aria-label='First group'><button id='" + rangeRefItem.IdTextPart + "' class='btn btn-primary btn-xs buttonRel foundItem'>" + rangeRefItem.Text + "</button>";
                            line += "<button class='btn btnRelEditor btn-default btn-xs' data-object-id='" + rangeRefItem.IdObject + "'><i class='fa fa-pencil-square-o fa-lg' aria-hidden='true'></i></button></div><span class='classField'>(" + rangeRefItem.NameClass + ")</span></span>";
                            ixText = rangeRefItem.IxEnd + 1;

                        }
                        if (ixText < response.AnalyzeText.length - 1) {
                            text = response.AnalyzeText.substring(ixText, response.AnalyzeText.length);
                            line += "<span class='analyzedText'>" + text + "</span>";
                        }
                        line += "</p>";
                        lines += line;
                    }

                    jqElem.html('');
                    autoRelatorContainer.elements.searchProviderNavigation = $(searchProviderNavigationTemplate);
                    $(jqElem).append(autoRelatorContainer.elements.searchProviderNavigation);
                    var panel = $('<div class="panel panel-default"></div>');
                    var panelHeader = $('<div class="panel-heading clearfix"></div>');
                    var inputGroup = $('<div class="input-group"></div>')

                    panelHeader.append(inputGroup);
                    var buttonGroup = $('<div class="btn-group"></div>');

                    inputGroup.append(buttonGroup);
                    var buttonUse = $('<button class="btn btn-xs saveItems" onClick="autoRelatorContainer.handler.saveItems(this)"><i class="fa fa-check-square" aria-hidden="true"></i></button>');
                    buttonGroup.append(buttonUse);

                    var buttonShowTextWithItems = $('<button id="btnShowTextWithItems" class="btn btn-xs btnDisplayType btn-primary" onClick="autoRelatorContainer.handler.toggleDisplayType(this)">Text with items</button>');
                    buttonGroup.append(buttonShowTextWithItems);

                    var buttonShowItems = $('<button id="btnShowItems" class="btn btn-xs btnDisplayType btn-secondary" onClick="autoRelatorContainer.handler.toggleDisplayType(this)">Items</button>');
                    buttonGroup.append(buttonShowItems);

                    var buttonOnlySelected = $('<button id="btnShowOnlySelected" class="btn btn-xs btnDisplayType btn-secondary" onClick="autoRelatorContainer.handler.toggleDisplayType(this)">Selected Items</button>');
                    buttonGroup.append(buttonOnlySelected);

                    panel.append(panelHeader);

                    var template = $('<div class="templateArea">' + lines + '</div>');
                    panel.append(template);

                    jqElem.append(panel);

                    autoRelatorContainer.handler.toggleDisplayType();

                    jqElem.find('.buttonRel').click(function () {

                        if ($(this).hasClass('btn-primary')) {
                            $(this).removeClass('btn-primary');
                            $(this).addClass('btn-success');
                        } else {
                            $(this).removeClass('btn-success');
                            $(this).addClass('btn-primary');

                        }

                        autoRelatorContainer.handler.checkSaveTags();
                    });

                    jqElem.find('.btnRelEditor').click(function () {
                        var idObject = $(this).data("object-id");
                        window.open(autoRelatorContainer.viewModel.ActionEditObject + '?Object=' + idObject, '_blank');
                    });

                    autoRelatorContainer.handler.checkSaveTags();

                    if (!response.SearchByGUID) {
                        $.post(autoRelatorContainer.viewModel.ActionGetSearchProviders, { idInstance: autoRelatorContainer.viewModel.IdInstance })
                            .done(function (response) {
                                console.log("connected", response);
                                if (response.IsSuccessful) {
                                    for (var i = 0; i < response.Result.length; i++) {
                                        var searchProvider = response.Result[i];
                                        var html = searchProviderLink.replace("@SEARCH_PROVIDER_ID@", searchProvider.ProviderId).replace("@SEARCH_PROVIDER_ID@", searchProvider.ProviderId);
                                        html = html.replace("@SEARCH_PROVIDER@", searchProvider.ProviderName).replace("@SEARCH_PROVIDER@", searchProvider.ProviderName);
                                        html = html.replace("@COUNT@", "...");
                                        autoRelatorContainer.elements.searchProviderNavigation.append(html);
                                        $.post(autoRelatorContainer.viewModel.ActionSearch, { providerId: searchProvider.ProviderId, text: autoRelatorContainer.configuration.searchText, idInstance: autoRelatorContainer.viewModel.IdInstance })
                                            .done(function (response) {
                                                console.log("connected", response);
                                                if (response.IsSuccessful) {
                                                    var element = $(autoRelatorContainer.elements.autoRelatorContainerElement).find("[name='" + response.Result.SearchProviderId + "']");
                                                    $(element).html(response.Result.CountSearchResultItems + ' ' + response.Result.SearchProvider + ' results');

                                                }
                                            })
                                            .fail(function (jqxhr, textStatus, error) {
                                                // stuff
                                            });
                                    }

                                }
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });
                    }

                    $('.foundItem').kendoTooltip({
                        position: "top",
                        animation: {
                            open: {
                                effects: "zoom",
                                duration: 150
                            }
                        },
                        showAfter: 500,
                        width: "500",
                        autoHide: true,
                        content: {
                            url: autoRelatorContainer.viewModel.ActionGetTransformedNames
                        },
                        requestStart: function (e) {
                            var idsTextParts = [];
                            var idItem = $(e.target).attr('id');
                            if (typeof idItem === 'undefined') {
                                e.options.url = '';
                            } else {
                                idsTextParts.push(idItem);
                                e.options.type = "POST";
                                e.options.data = { idsTextParts: idsTextParts, idInstance: autoRelatorContainer.viewModel.IdInstance }
                            }
                        }, contentLoad: function () {
                            var tooltipItem = $.parseJSON(this.content.html());

                            if (tooltipItem.Result.length > 0) {
                                var encodedContent = tooltipItem.Result[0].Val_String
                                var decodedContent = b64_to_utf8(encodedContent);
                                const options = { defaultProtocol: 'https' };
                                decodedContent = linkifyHtml(decodedContent, options);
                                $(this.content).html(decodedContent);

                            }
                            else {
                                $(this.content).html('');
                            }

                        }
                    }).data("kendoTooltip");

                },
                parseText: function (text) {
                    $('body').openPageLoader();
                    autoRelatorContainer.configuration.searchText = text;
                    $.post(autoRelatorContainer.viewModel.ActionParseText, { idInstance: autoRelatorContainer.viewModel.IdInstance, text: autoRelatorContainer.configuration.searchText, useOntology: false, useName: autoRelatorContainer.viewModel.textChanged })
                        .done(function (response) {
                            console.log("connected", response);
                            $('body').closePageLoader();

                            autoRelatorContainer.handler.createResultArea(response, autoRelatorContainer.elements.autoRelatorContainerElement.find('.outputParse'));
                            $('body').closePageLoader();
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            $('body').closePageLoader();
                            // stuff
                        });
                },
                parseTextExplicit: function () {
                    $('body').openPageLoader();
                    autoRelatorContainer.elements.autoRelatorContainerElement.find('.outputAutoComplete').html('');
                    autoRelatorContainer.elements.autoRelatorContainerElement.find('.outputParse').html('');
                    var text = '';
                    if (autoRelatorContainer.viewModel.selected !== undefined && autoRelatorContainer.viewModel.selected !== null && autoRelatorContainer.viewModel.selected.text !== undefined && autoRelatorContainer.viewModel.selected.text !== null && autoRelatorContainer.viewModel.selected.text !== '') {
                        text = b64EncodeUnicode(autoRelatorContainer.viewModel.selected.text);
                    } else {
                        text = b64EncodeUnicode(autoRelatorContainer.elements.autoRelatorContainerElement.find('.textAreaUserStories').val());
                    }

                    autoRelatorContainer.handler.parseText(text);
                },
                parseTextOntology: function () {
                    autoRelatorContainer.elements.autoRelatorContainerElement.find('.outputAutoComplete').openPageLoader();
                    autoRelatorContainer.elements.autoRelatorContainerElement.find('.outputAutoComplete').html('');
                    autoRelatorContainer.elements.autoRelatorContainerElement.find('.outputParse').html('');
                    autoRelatorContainer.configuration.searchText = b64EncodeUnicode(autoRelatorContainer.elements.autoRelatorContainerElement.find('.textAreaUserStories').val());
                    $.post(autoRelatorContainer.viewModel.ActionParseText, { idInstance: autoRelatorContainer.viewModel.IdInstance, text: autoRelatorContainer.configuration.searchText, useOntology: true, useName: false })
                        .done(function (response) {
                            console.log("connected", response);
                            $('body').closePageLoader();

                            autoRelatorContainer.viewModel.createResultAreaOnto(response, autoRelatorContainer.elements.autoRelatorContainerElement.find('.outputAutoComplete'));
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                },
                openSearchResult: function (idProvider) {
                    
                    var providerLink = $("[name='" + idProvider + "']");
                    var providerName = $(providerLink).data('name');
                    autoRelatorContainer.configuration.idProvider = idProvider;
                    autoRelatorContainer.elements.gridItem.dataSource.read();
                    var dialog = $(autoRelatorContainer.elements.windowSearchResult).data("kendoWindow");
                    dialog.title(providerName);
                    dialog.open();

                },
                selectResultItem: function (element) {
                    var row = $(element).closest('tr');
                    var item = autoRelatorContainer.elements.gridItem.dataItem(row);

                    var oItems = [{
                        GUID: item.IdSearchItem,
                        Name: item.NameSearchItem,
                        GUID_Parent: item.IdParentSearchItem,
                        Type: "Object"
                    }];

                    if (autoRelatorContainer.configuration.selectItems !== undefined && autoRelatorContainer.configuration.selectItems !== null) {
                        autoRelatorContainer.configuration.selectItems(oItems);
                    }
                },
                selectClassItem: function (classId) {
                    autoRelatorContainer.viewModel.classList.value(classId);
                    autoRelatorContainer.viewModel.classList.trigger("change");
                },
                applyResultItem: function (element) {
                    var row = $(element).closest('tr');
                    var item = autoRelatorContainer.elements.gridItem.dataItem(row);

                    var oItems = [{
                        GUID: item.IdSearchItem,
                        Name: item.NameSearchItem,
                        GUID_Parent: item.IdParentSearchItem,
                        Type: "Object"
                    }];

                    if (autoRelatorContainer.configuration.applyItems !== undefined && autoRelatorContainer.configuration.applyItems !== null) {
                        autoRelatorContainer.configuration.applyItems(oItems);
                    }
                    $(autoRelatorContainer.elements.windowSearchResult).data("kendoWindow").close();
                },
                validateReferenceObjectName: function (idRef, content) {
                    $('#iFrameTypedTaggingList').attr('src', viewModel.ActionTypedTaggingList + '?Object=' + viewModel.oItem.GUID + "&Sender=" + viewModel.IdInstance);
                    $('body').openPageLoader();
                    autoRelatorContainer.elements.autoRelatorContainerElement.find('.textAreaUserStories').unbind('click');
                    autoRelatorContainer.viewModel.textChanged = false;
                    $.post(autoRelatorContainer.viewModel.ActionValidateReference, { idRefItem: idRef, content: content, idInstance: autoRelatorContainer.viewModel.IdInstance })
                        .done(function (response) {
                            console.log("connected", response);
                            autoRelatorContainer.viewModel.nameOfObject = getViewItemsValue(response.ViewItems, autoRelatorContainer.viewModel.objectName.ViewItemId, "Content");
                            autoRelatorContainer.elements.autoRelatorContainerElement.find('.objectName').html(autoRelatorContainer.viewModel.nameOfObject);
                            changeValuesFromServerChildren(response.ViewItems, [ ".auto-relator" ]);

                            $('body').closePageLoader();

                            if (typeof autoRelatorContainer.viewModel.oItem.Val_String !== 'undefined' && autoRelatorContainer.viewModel.oItem.Val_String !== null && autoRelatorContainer.viewModel.oItem.Val_String !== '') {
                                autoRelatorContainer.elements.autoRelatorContainerElement.find('.textAreaUserStories').val($('<textarea />').html(autoRelatorContainer.viewModel.oItem.Val_String).text());
                                autoRelatorContainer.viewModel.textChanged = true;


                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                }
            }
        });
        $(this).addClass('auto-relator');

        autoRelatorContainer = $(this).data("auto-relator");

        autoRelatorContainer.elements.autoRelatorContainerElement = $(this);
        autoRelatorContainer.elements.autoRelatorElement = $(componentTemplate);
        $(autoRelatorContainer.elements.autoRelatorContainerElement).html('');
        $(autoRelatorContainer.elements.autoRelatorContainerElement).append(autoRelatorContainer.elements.autoRelatorElement);

        $(document).on('mousemove', 'textarea', function (e) {
            var a = $(this).offset().top + $(this).outerHeight() - 16,	//	top border of bottom-right-corner-box area
                b = $(this).offset().left + $(this).outerWidth() - 16;	//	left border of bottom-right-corner-box area
            $(this).css({
                cursor: e.pageY > a && e.pageX > b ? 'nw-resize' : ''
            });
        })
            //  the following simple make the textbox "Auto-Expand" as it is typed in
            .on('keyup', 'textarea', function (e) {
                $(this).resizeTextArea(e);
            });

        $.fn.resizeTextArea = function (e) {
            //  the following will help the text expand as typing takes place
            while ($(this).outerHeight() < this[0].scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
                $(this).height($(this).height() + 1);
            }
        };

        $(autoRelatorContainer.elements.autoRelatorElement).find('.btnSaveObject').click(autoRelatorContainer.handler.saveSelection);
        $(autoRelatorContainer.elements.autoRelatorElement).find('.btnTypedTag').html("Typed-Tag");
        $(autoRelatorContainer.elements.autoRelatorElement).find('.btnTypedTag').click(function () {
            if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).addClass('btn-default');
                $(this).html('Relation-Type');
                $(this).closes('.auto-relator');
                autoRelatorContainer.viewModel.relationTypeList.enable(true);

            } else {
                $(this).addClass('btn-success');
                $(this).removeClass('btn-default');
                $(this).html('Typed-Tag');
                autoRelatorContainer.viewModel.relationTypeList.enable(false);
            }


        });

        $.post(autoRelatorContainer.configuration.actionAutoRelatorInit)
            .done(function (response) {
                autoRelatorContainer.viewModel = response.Result;
                changeValuesFromServerChildren(autoRelatorContainer.viewModel.ViewItems, [ ".auto-relator" ]);
                $(autoRelatorContainer.elements.autoRelatorElement).find('.inpOrderId').kendoNumericTextBox({
                    format: "n0",
                    min: 0,
                    decimals: 0
                });

                $(autoRelatorContainer.elements.autoRelatorElement).find('.btnInsertOntology').click(function () {
                    $.post(autoRelatorContainer.viewModel.ActionGetOntoTemplate, { idInstance: autoRelatorContainer.viewModel.IdInstance })
                        .done(function (response) {
                            console.log(response);
                            var html = '';
                            for (var ix in response.Result) {
                                var template = response.Result[ix];

                                html += template + '\r\n';
                            }

                            $(autoRelatorContainer.elements.autoRelatorElement).find('.textAreaUserStories').val(html);
                            $(autoRelatorContainer.elements.autoRelatorElement).find('.textAreaUserStories').resizeTextArea();
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });

                });

                $.post(autoRelatorContainer.viewModel.ActionGetClassDropDownConfig, { idInstance: autoRelatorContainer.viewModel.IdInstance })
                    .done(function (response) {
                        console.log(response);
                        jQuery.extend(response, {
                            filter: "contains",
                            suggest: true
                        });

                        $.extend(response, {
                            change: function (e) {
                                var idClass = this.value();

                                changeValue(autoRelatorContainer.viewModel.inpClass, "SelectedIndex", idClass, true, null, autoRelatorContainer.viewModel.IdInstance);
                            }
                        });

                        $(autoRelatorContainer.elements.autoRelatorElement).find('.inpClass').kendoDropDownList(response);
                        autoRelatorContainer.viewModel.classList = $($(autoRelatorContainer.elements.autoRelatorElement).find('.inpClass')[1]).data("kendoDropDownList");
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });

                $.post(autoRelatorContainer.viewModel.ActionGetRelationTypeDropDownConfig, { idInstance: autoRelatorContainer.viewModel.IdInstance })
                    .done(function (response) {
                        console.log(response);
                        jQuery.extend(response.Result, {
                            filter: "contains",
                            suggest: true
                        });

                        $.extend(response.Result, {
                            change: function (e) {

                            }
                        });

                        $(autoRelatorContainer.elements.autoRelatorElement).find('.inpRelation').kendoDropDownList(response.Result);
                        autoRelatorContainer.viewModel.relationTypeList = $($(autoRelatorContainer.elements.autoRelatorElement).find('.inpRelation')[1]).data("kendoDropDownList");
                        autoRelatorContainer.viewModel.relationTypeList.enable(false);
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });

                $(autoRelatorContainer.elements.autoRelatorElement).find('.btnSearchAll').on("click", autoRelatorContainer.handler.parseTextExplicit);
                $(autoRelatorContainer.elements.autoRelatorElement).find('.btnSearchOntology').on("click", autoRelatorContainer.handler.parseTextOntology);

                $(autoRelatorContainer.elements.autoRelatorElement).find('.textAreaUserStories').bind("input paste keydown click focus select", function () {
                    autoRelatorContainer.handler.checkSaveSelection();
                });

                $(autoRelatorContainer.elements.autoRelatorElement).find('.textAreaUserStories').bind("change", function () {
                    //viewModel.AutoParseText();
                    
                });

                $(autoRelatorContainer.elements.autoRelatorElement).find('.inpAutoComplete').autocomplete({
                    serviceUrl: autoRelatorContainer.viewModel.ActionGetAutoCompletes + '?idInstance=' + autoRelatorContainer.viewModel.IdInstance,
                    dataType: "json",
                    deferRequestBy: 300,
                    noCache: true,
                    //lookup: countriesArray,
                    lookupFilter: function (suggestion, originalQuery, queryLowerCase) {
                        var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
                        return re.test(suggestion.value);
                    },
                    onSelect: function (suggestion) {
                        var text = "";
                        $(autoRelatorContainer.elements.autoRelatorElement).find('.inpAutoComplete_hidden').val(suggestion.data);
                        text = b64EncodeUnicode(autoRelatorContainer.elements.autoRelatorContainerElement.find('.inpAutoComplete_hidden').val());
                        autoRelatorContainer.handler.parseText(text);
                    },
                    onHint: function (hint) {

                    },
                    onInvalidateSelection: function () {

                    },
                    onSearchStart: function (query) {
                        $(autoRelatorContainer.elements.autoRelatorElement).find('.inpAutoComplete').css('border-color', 'coral');
                    },
                    onSearchComplete: function (query, suggestions) {
                        $(autoRelatorContainer.elements.autoRelatorElement).find('.inpAutoComplete').css('border-color', '');
                    }
                });

                var configurationTypedTaggingList = {
                    actionGetTags: autoRelatorContainer.viewModel.ActionGetTagsTypedTaggingList,
                    actionRelationEditor: autoRelatorContainer.viewModel.ActionEditObjectTypedTaggingList,
                    actionValidateReference: autoRelatorContainer.viewModel.ActionValidateReferenceTypedTaggingList,
                    actionGetTransformedNames: autoRelatorContainer.viewModel.ActionGetTransformedNames,
                    channelAppliedObjects: autoRelatorContainer.viewModel.ChannelAppliedObjects,
                    channelSelectedObject: autoRelatorContainer.viewModel.ChannelSelectedObject,
                    channelSelectedClass: autoRelatorContainer.viewModel.ChannelSelectedClass,
                    idInstance: autoRelatorContainer.viewModel.IdInstance,
                    idEndpoint: autoRelatorContainer.viewModel.IdEndpoint,
                    idSession: autoRelatorContainer.viewModel.IdSession,
                    idUser: autoRelatorContainer.viewModel.IdUser,
                    managerConfig: autoRelatorContainer.configuration.managerConfig,
                    createdCallback: function () {
                        if (autoRelatorContainer.configuration.complete !== undefined) {
                            autoRelatorContainer.configuration.complete(autoRelatorContainer);
                        }
                    },
                    applyItems: function (oItems) {
                        if (autoRelatorContainer.configuration.applyItems !== undefined && autoRelatorContainer.configuration.applyItems !== null) {
                            autoRelatorContainer.configuration.applyItems(oItems);
                        }
                    }
                };

                $('.typed-tagging-list').createTypedTaggingList(configurationTypedTaggingList);

                $.post(autoRelatorContainer.viewModel.ActionGetGridConfigSearchProviderResult)
                    .done(function (response) {
                        autoRelatorContainer.elements.windowSearchResult = $(windowTemplate);
                        $(autoRelatorContainer.elements.autoRelatorContainerElement).append(autoRelatorContainer.elements.windowSearchResult);
                        autoRelatorContainer.elements.windowSearchResult.kendoWindow({
                            width: '800px',
                            height: '600px',
                            title: 'Select Search Provider Item',
                            visible: false,
                            modal: true,
                            actions: [
                                "Pin",
                                "Minimize",
                                "Maximize",
                                "Close"
                            ]
                        }).data("kendoWindow").center();

                        $.extend(response.dataSource.transport.read, {
                            data: function () {
                                return { idProvider: autoRelatorContainer.configuration.idProvider, idInstance: autoRelatorContainer.viewModel.IdInstance };
                            }
                        });

                        $.extend(response.dataSource.transport.read,
                            {
                                complete: function (e) {
                                    console.log(e);
                                }
                            }
                        );

                        var dataSource = new kendo.data.DataSource(response.dataSource);
                        
                        response.dataSource = dataSource;
                        response.dataBound = function () {

                        };
                        
                        autoRelatorContainer.elements.gridItem = $(autoRelatorContainer.elements.windowSearchResult).find('.search-povider-result-grid').kendoGrid(response).data("kendoGrid");
                        $(autoRelatorContainer.elements.windowSearchResult).find('.k-button').kendoButton({
                            //click: autoRelatorContainer.handler.clickFunctionButtonHandler
                        });

                       

                        $('body').closePageLoader();

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });
                
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });

        return autoRelatorContainer;
    };

})(jQuery);

var saveComponentTemplate = `
<div id="save-reference-window">
    <div class="k-content">
        
    </div>
</div>
`

var componentTemplate = `
<div class="panel panel-default panelUserStories">
    <div class="panel-heading"><span class="labelUserStories"></span></div>
    <div class="panel-body">
        <div class="container container-full">
            <div class="row row-full">
                <div class="col-lg-11">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button class="btn btn-default btnInsertOntology" type="button" title="Insert Ontology-Templates"><i class="fa fa-file-code-o " aria-hidden="true"></i></button>

                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <input class="form-control inpAutoComplete" type="text" size="800"/>
                            <input class="inpAutoComplete_hidden" type="hidden"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <textarea class="form-control textAreaUserStories"></textarea>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button class="btn btn-default btnSearchAll" type="button" title="Analyze Text"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    <button class="btn btn-default btnSearchOntology" type="button"><i class="fa fa-search" aria-hidden="true"></i><i class="fa fa-cogs" aria-hidden="true"></i></button>
                                    <button class="btn btn-default btnSaveObject" type="button" title="Save Object"><i class="fa fa-save" aria-hidden="true"></i></button>
                                </div>

                                <input type="text" class="form-control itemCombo inpClass" />

                                <div class="input-group-btn">

                                    <button class="btn btn-success btnTypedTag" type="button" title=""></button>

                                </div>
                                <input type="text" class="form-control itemCombo inpRelation" />
                                <div class="input-group-btn">
                                    <label class="form-control lblOrderId">Order:</label>
                                </div>
                                <input type="number" value="1" class="form-control numericUpDown inpOrderId" />
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="outputAutoComplete">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="outputParse">

                            </div>
                        </div>
                    </div>

                </div>
                
            </div>
        </div>
        <div class="row row-full">
            <div class="col-lg-11 row-full">
                <div class="restHeight typed-tagging-list">
                        
                </div>
            </div>
        </div>
    </div>

</div>`;

var searchProviderNavigationTemplate = `
<div class="search-provider-container">
</div>
`;

var searchProviderLink = `
<span><a href="javascript:autoRelatorContainer.handler.openSearchResult('@SEARCH_PROVIDER_ID@');" name="@SEARCH_PROVIDER_ID@" data-name="@SEARCH_PROVIDER@">@COUNT@ @SEARCH_PROVIDER@ results</a> |
`;

var windowTemplate = `
    <div class='search-provider-window'>
        <div class="k-content search-povider-result-grid-container">
            <div class="search-povider-result-grid"></div>
        </div>
        <div class='message-orderid'></div>
    </div>
`;