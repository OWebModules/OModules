﻿(function ($) {
    $.fn.createItemComBar = function (configuration) {
        var itemComBarComponent = {
            configuration: configuration,
            elements: {
            },
            data: {
            },
            handler: {
                createComponent: function (itemComBarComponent) {
                    if (!itemComBarComponent.configuration.showWithoutEvent) {
                        itemComBarComponent.handler.mouseOver(elements);
                    }

                    $(elements).on('mouseleave', function () {
                        var itemComBarComponent = $(this).parent().data('itemComBar-component');
                        if (typeof itemComBarComponent.data.timeOut !== 'undefined' && itemComBarComponent.data.timeOut != null) {
                            clearTimeout(itemComBarComponent.data.timeOut);
                        }
                    });


                    var sendElement = $(itemComBarComponent.templates.sendTemplate);
                    sendElement.hide();
                    var applyElement = $(itemComBarComponent.templates.applyTemplate);
                    applyElement.hide();
                    var cancelElement = $(itemComBarComponent.templates.cancelTemplate);
                    cancelElement.hide();
                    itemComBarComponent.elements.sendElement = sendElement;
                    itemComBarComponent.elements.applyElement = applyElement;
                    itemComBarComponent.elements.cancelElement = cancelElement;
                    $($(elements).parent()).append(sendElement);
                    $($(elements).parent()).append(applyElement);
                    $($(elements).parent()).append(cancelElement);

                    $(applyElement).on('click', function () {
                        var itemComBarComponent = $(this).parent().data('itemComBar-component');
                        itemComBarComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                            ChannelId: itemComBarComponent.viewModel.ChannelAppliedItem,
                            SenderId: itemComBarComponent.configuration.managerConfig.idEndpoint,
                            UserId: itemComBarComponent.viewModel.IdUser,
                            SessionId: itemComBarComponent.configuration.managerConfig.idSession,
                            OItems: [{ GUID: itemComBarComponent.viewModel.RefItem.GUID }]
                        });
                    });
                    $(sendElement).on('click', function () {
                        var itemComBarComponent = $(this).parent().data('itemComBar-component');
                        itemComBarComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                            ChannelId: itemComBarComponent.viewModel.ChannelSelectedObject,
                            SenderId: itemComBarComponent.configuration.managerConfig.idEndpoint,
                            UserId: itemComBarComponent.viewModel.IdUser,
                            SessionId: itemComBarComponent.configuration.managerConfig.idSession,
                            OItems: [{ GUID: itemComBarComponent.viewModel.RefItem.GUID }]
                        });
                    });
                    $(cancelElement).on('click', function () {
                        var itemComBarComponent = $(this).parent().data('itemComBar-component');

                        itemComBarComponent.handler.hideElements();

                    });

                    if (typeof itemComBarComponent.configuration.events !== 'undefined' && itemComBarComponent.configuration.events !== null &&
                        typeof itemComBarComponent.configuration.events.completed !== 'undefined' && itemComBarComponent.configuration.events.completed !== null) {
                        itemComBarComponent.configuration.events.completed(itemComBarComponent);
                    }
                },
                mouseOver: function (element) {
                    var itemComBarComponent = $(element).parent().data('itemComBar-component');
                    var elements = getElementsBySelectorPath(itemComBarComponent.viewModel.SelectorPath);
                    if (elements.find('.item-com-bar').is(':visible')) {
                        return;
                    }
                    if (typeof itemComBarComponent.data.timeOut !== 'undefined' && itemComBarComponent.data.timeOut != null) {
                        clearTimeout(itemComBarComponent.data.timeOut);
                    }
                    itemComBarComponent.data.timeOut = setTimeout(function () {
                        var elements = getElementsBySelectorPath(itemComBarComponent.viewModel.SelectorPath);
                        var position = elements.offset();
                        var sendItem = elements.parent().find('.send-item');
                        var applyItem = elements.parent().find('.apply-item');
                        var cancelEdit = elements.parent().find('.cancel-edit');

                        $(sendItem).css('position', 'absolute');
                        $(sendItem).css('z-index', 99);
                        $(sendItem).css('top', elements[0].offsetTop);
                        $(sendItem).css('left', position.left + 2 + elements.width());
                        $(sendItem).show();

                        $(applyItem).css('position', 'absolute');
                        $(applyItem).css('z-index', 99);
                        $(applyItem).css('top', elements[0].offsetTop);
                        $(applyItem).css('left', position.left + 2 + $(sendItem).outerWidth() + elements.width());
                        $(applyItem).show();

                        $(cancelEdit).css('position', 'absolute');
                        $(cancelEdit).css('z-index', 99);
                        $(cancelEdit).css('top', elements[0].offsetTop);
                        $(cancelEdit).css('left', position.left + 2 + $(sendItem).outerWidth() + $(applyItem).outerWidth() + elements.width());
                        $(cancelEdit).show();

                    }, 300, itemComBarComponent);
                },
                hideElements: function () {
                    $(itemComBarComponent.elements.sendElement).hide();
                    $(itemComBarComponent.elements.applyElement).hide();
                    $(itemComBarComponent.elements.cancelElement).hide();
                }
            },
            templates: {
                sendTemplate: `
                    <button class='btn btn-info btn-xs send-item'><i class="fa fa-share-square-o" aria-hidden="true"></i></button>
                `,
                applyTemplate: `
                    <button class='btn btn-info btn-xs apply-item'><i class="fa fa-check-circle-o" aria-hidden="true"></i></button>
                `,
                cancelTemplate: `
                    <button class='btn btn-info btn-xs cancel-edit'><i class="fa fa-ban" aria-hidden="true"></i></button>
                `
            }
        }
        var elements = getElementsBySelectorPath(configuration.selectorPath);
        $(elements).parent().data('itemComBar-config', configuration);
        $(elements).parent().data('itemComBar-component', itemComBarComponent);

        if (typeof configuration.viewModel == 'undefined' || configuration.viewModel == null) {
            $.post(configuration.actions.init, { selectorPath: configuration.selectorPath, idItem: configuration.idItem, typeItem: configuration.typeItem })
                .done(function (response) {
                    var selectorPath = response.SelectorPath;
                    var elements = getElementsBySelectorPath(selectorPath);
                    var itemComBarComponent = $(elements).parent().data('itemComBar-component');

                    itemComBarComponent.viewModel = response;
                    itemComBarComponent.handler.createComponent(itemComBarComponent);
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        } else {
            var itemComBarComponent = $(elements).parent().data('itemComBar-component');
            itemComBarComponent.handler.createComponent(itemComBarComponent);
        }


    };
})(jQuery);

(function ($) {
    $.fn.createItemComToolBar = function (configuration) {
        var itemComBarComponent = {
            configuration: configuration,
            elements: {
            },
            data: {
                isListen: function (itemComBarComponent) {
                    var elements = getElementsBySelectorPath(itemComBarComponent.configuration.selectorPath);
                    return $(elements).find('.listen-item').isToggled();
                }
            },
            handler: {
                createComponent: function (itemComBarComponent) {
                    var elements = getElementsBySelectorPath(itemComBarComponent.configuration.selectorPath);
                    $(elements).kendoToolBar({
                        items: [
                            {
                                template: '<button class="k-button listen-item"><i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></button>',

                            },
                            {
                                template: '<button class="k-button send-item"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>',

                            },
                            {
                                template: '<button class="k-button apply-item"><i class="fa fa-check-square" aria-hidden="true"></i></button>',

                            },
                            {
                                type: "separator"
                            },
                            {
                                template: '<label class="ref-item-name-label"></label>'
                            }
                        ]
                    });

                    var listenElement = $(elements).find('.listen-item');
                    var applyElement = $(elements).find('.apply-item');
                    var sendElement = $(elements).find('.send-item');

                    itemComBarComponent.elements.listenElement = listenElement;
                    itemComBarComponent.elements.applyElement = applyElement;
                    itemComBarComponent.elements.sendElement = sendElement;

                    $(listenElement).on('click', function () {
                        var itemComBarComponent = $(this).parent().parent().data('itemComBar-component');
                        var elements = getElementsBySelectorPath(itemComBarComponent.configuration.selectorPath);
                        $(elements).find('.listen-item').toggleMode(true);
                    });
                    $(applyElement).on('click', function () {
                        var itemComBarComponent = $(this).parent().parent().data('itemComBar-component');
                        itemComBarComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                            ChannelId: itemComBarComponent.viewModel.ChannelAppliedObject,
                            SenderId: itemComBarComponent.configuration.managerConfig.idEndpoint,
                            UserId: itemComBarComponent.viewModel.IdUser,
                            SessionId: itemComBarComponent.configuration.managerConfig.idSession,
                            OItems: [ itemComBarComponent.data.refItem ]
                        });
                    });
                    $(sendElement).on('click', function () {
                        var itemComBarComponent = $(this).parent().parent().data('itemComBar-component');
                        itemComBarComponent.configuration.managerConfig.moduleComHub.server.sendInterServiceMessage({
                            ChannelId: itemComBarComponent.viewModel.ChannelSelectedObject,
                            SenderId: itemComBarComponent.configuration.managerConfig.idEndpoint,
                            UserId: itemComBarComponent.viewModel.IdUser,
                            SessionId: itemComBarComponent.configuration.managerConfig.idSession,
                            OItems: [ itemComBarComponent.data.refItem ]
                        });
                    });

                    itemComBarComponent.handler.toggleEnable(applyElement, false);
                    itemComBarComponent.handler.toggleEnable(sendElement, false);
                    $(elements).find('.listen-item').toggleMode(true);

                    if (typeof itemComBarComponent.configuration.events !== 'undefined' && itemComBarComponent.configuration.events !== null &&
                        typeof itemComBarComponent.configuration.events.completed !== 'undefined' && itemComBarComponent.configuration.events.completed !== null) {
                        itemComBarComponent.configuration.events.completed(itemComBarComponent);
                    }
                },
                toggleEnable: function (element, enable) {
                    $(element).prop('disabled', !enable);
                    if (enable) {
                        $(element).removeClass('k-state-disabled');
                    } else {
                        $(element).addClass('k-state-disabled');
                    }
                },
                stopListen: function (itemComBarComponent) {
                    var elements = getElementsBySelectorPath(itemComBarComponent.configuration.selectorPath);
                    $(elements).find('.listen-item').setCheckedMode(false, true);
                },
                startListen: function (itemComBarComponent) {
                    var elements = getElementsBySelectorPath(itemComBarComponent.configuration.selectorPath);
                    $(elements).find('.listen-item').setCheckedMode(true, true);
                },
                validateReference: function (itemComBarComponent, idItem) {
                    if (!itemComBarComponent.elements.listenElement.isToggled()) {
                        return;
                    }
                    $('body').openPageLoader();

                    var refItem = null;
                    if (typeof (idItem) !== 'undefined' && idItem != null) {
                        var refItem = {
                            GUID: idItem,
                            Type: 'Object'
                        };
                    }

                    $.post(itemComBarComponent.viewModel.ActionValidateReference, { refItem: refItem, idInstance: itemComBarComponent.viewModel.IdInstance })
                        .done(function (response) {
                            var selectorPath = response.SelectorPath;
                            var elements = getElementsBySelectorPath(selectorPath);
                            var applyElement = $(elements).find('.apply-item');
                            var sendElement = $(elements).find('.send-item');

                            var itemComBarComponent = $(elements).data('itemComBar-component');
                            itemComBarComponent.data.refItem = response.Result;
                            var label = "";
                            var windowTitle = "";
                            if (itemComBarComponent.data.refItem != null) {
                                label = itemComBarComponent.data.refItem.Additional1;
                                windowTitle = itemComBarComponent.data.refItem.Additional2;

                                itemComBarComponent.handler.toggleEnable(applyElement, true);
                                itemComBarComponent.handler.toggleEnable(sendElement, true);
                            }
                            $(elements).find('.ref-item-name-label').html(label);
                            if (itemComBarComponent.configuration.setWindowTitle) {
                                document.title = windowTitle;
                            }
                            $('body').closePageLoader();
                            if (typeof itemComBarComponent.configuration.events !== 'undefined' && itemComBarComponent.configuration.events !== null &&
                                typeof itemComBarComponent.configuration.events.validatedReference !== 'undefined' && itemComBarComponent.configuration.events.validatedReference !== null) {

                                if (itemComBarComponent.data.refItem != null) {
                                    itemComBarComponent.handler.stopListen(itemComBarComponent);
                                } else {
                                    itemComBarComponent.handler.startListen(itemComBarComponent);
                                }
                                itemComBarComponent.configuration.events.validatedReference(itemComBarComponent.data.refItem);
                            }
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                        });
                    
                }
            }
        }
        var elements = getElementsBySelectorPath(configuration.selectorPath);
        $(elements).data('itemComBar-config', configuration);
        $(elements).data('itemComBar-component', itemComBarComponent);
        itemComBarComponent.elements.itemComBarElement = $(this);

        if (typeof configuration.viewModel == 'undefined' || configuration.viewModel == null) {
            $.post(configuration.actions.init, { selectorPath: configuration.selectorPath, idItem: configuration.idItem, typeItem: configuration.typeItem })
                .done(function (response) {
                    var selectorPath = response.SelectorPath;
                    var elements = getElementsBySelectorPath(selectorPath);
                    var itemComBarComponent = $(elements).data('itemComBar-component');

                    itemComBarComponent.viewModel = response;
                    itemComBarComponent.handler.createComponent(itemComBarComponent);
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        } else {
            var itemComBarComponent = $(elements).data('itemComBar-component');
            itemComBarComponent.handler.createComponent(itemComBarComponent);
        }


    };
})(jQuery);