﻿(function ($) {
    $.fn.createEmbeddedNameEdit = function (configuration) {
        var nameEditComponent = {
            configuration: configuration,
            elements: {
            },
            data: {
            },
            handler: {
                createComponent: function (nameEditComponent) {
                    $(elements).on('mousedown', function () {

                        var nameEditComponent = $(this).data('embeddedNameEdit-component');
                        nameEditComponent.handler.checkEnableSaveName(nameEditComponent);
                        var elements = getElementsBySelectorPath(nameEditComponent.viewModel.SelectorPath);
                        if (elements.find('.name-editor').is(':visible')) {
                            return;
                        }
                        setTimeout(function () {
                            var elements = getElementsBySelectorPath(nameEditComponent.viewModel.SelectorPath);
                            var position = elements.offset();
                            var nameEditor = elements.find('.name-editor');
                            var saveName = elements.find('.save-name');
                            var cancelEdit = elements.find('.cancel-edit');

                            $(nameEditor).css('position', 'absolute');
                            $(nameEditor).css('z-index', 99);
                            $(nameEditor).css('top', elements[0].offsetTop);
                            $(nameEditor).css('left', position.left-2);
                            $(nameEditor).width(elements.width());
                            $(nameEditor).show();
                            $(nameEditor).val(nameEditComponent.viewModel.RefItem.Name);
                            nameEditComponent.data.name = nameEditComponent.viewModel.RefItem.Name;

                            $(saveName).css('position', 'absolute');
                            $(saveName).css('z-index', 99);
                            $(saveName).css('top', elements[0].offsetTop);
                            $(saveName).css('left', position.left + 2 + elements.width());
                            $(saveName).show();

                            $(cancelEdit).css('position', 'absolute');
                            $(cancelEdit).css('z-index', 99);
                            $(cancelEdit).css('top', elements[0].offsetTop);
                            $(cancelEdit).css('left', position.left + 2 + $(saveName).outerWidth() + elements.width());
                            $(cancelEdit).show();

                        }, 300, nameEditComponent);

                    });

                    var editorElement = $(nameEditComponent.templates.editorTemplate);
                    editorElement.hide();
                    var saveElement = $(nameEditComponent.templates.saveTemplate);
                    saveElement.hide();
                    var cancelElement = $(nameEditComponent.templates.cancelTemplate);
                    cancelElement.hide();
                    nameEditComponent.elements.editorElement = editorElement;
                    nameEditComponent.elements.saveElement = saveElement;
                    nameEditComponent.elements.cancelElement = cancelElement;
                    $(elements).append(editorElement);
                    $(elements).append(saveElement);
                    $(elements).append(cancelElement);
                    nameEditComponent.handler.checkEnableSaveName(nameEditComponent);

                    $(editorElement).on('input', function () {
                        var nameEditComponent = $(this).parent().data('embeddedNameEdit-component');
                        nameEditComponent.data.name = $(this).val();
                        nameEditComponent.handler.checkEnableSaveName(nameEditComponent);
                    });

                    $(cancelElement).on('click', function () {
                        var nameEditComponent = $(this).parent().data('embeddedNameEdit-component');
                        var close = true;
                        if (nameEditComponent.viewModel.RefItem.Name !== nameEditComponent.data.name) {
                            if (!confirm('Do you want to close editor without save?')) {
                                close = false;
                            }
                        }

                        if (!close) return;

                        nameEditComponent.handler.hideElements();

                    });
                    $(saveElement).on('click', function () {
                        nameEditComponent = $(this).parent().data('embeddedNameEdit-component');
                        var name = nameEditComponent.data.name;

                        var refItem = {
                            GUID: nameEditComponent.viewModel.RefItem.GUID,
                            Name: name,
                            GUID_Parent: nameEditComponent.viewModel.RefItem.GUID_Parent,
                            Type: 'Object'
                        }
                        $.post(nameEditComponent.viewModel.ActionSaveName, { refItem: refItem, idInstance: nameEditComponent.viewModel.IdInstance })
                            .done(function (response) {
                                nameEditComponent.handler.hideElements();
                                nameEditComponent.viewModel.RefItem = response.Result;
                                nameEditComponent.configuration.events.savedName(nameEditComponent.viewModel.SelectorPath, nameEditComponent.viewModel.RefItem.Name);
                            })
                            .fail(function (jqxhr, textStatus, error) {
                                // stuff
                            });
                    });

                    if (typeof nameEditComponent.configuration.events !== 'undefined' && nameEditComponent.configuration.events !== null &&
                        typeof nameEditComponent.configuration.events.completed !== 'undefined' && nameEditComponent.configuration.events.completed !== null) {
                        nameEditComponent.configuration.events.completed(nameEditComponent);
                    }
                },
                hideElements: function () {
                    $(nameEditComponent.elements.editorElement).hide();
                    $(nameEditComponent.elements.saveElement).hide();
                    $(nameEditComponent.elements.cancelElement).hide();
                },
                checkEnableSaveName: function (component) {
                    var nameEditComponent = component;
                    if (typeof nameEditComponent.data == 'undefined' || nameEditComponent.data == null ||
                        typeof nameEditComponent.viewModel == 'undefined' || nameEditComponent.viewModel == null ||
                        typeof nameEditComponent.viewModel.RefItem == 'undefined' || nameEditComponent.viewModel.RefItem == null) return;


                    if (nameEditComponent.data.name !== nameEditComponent.viewModel.RefItem.Name) {
                        $(nameEditComponent.elements.saveElement).prop("disabled", false);
                    } else {
                        $(nameEditComponent.elements.saveElement).prop("disabled", true);
                    }
                }
            },
            templates: {
                editorTemplate: `
                    <input class='name-editor' type='text'>
                `,
                cancelTemplate: `
                    <button class='btn btn-warning btn-xs cancel-edit'><i class="fa fa-ban" aria-hidden="true"></i></button>
                `,
                saveTemplate: `
                    <button class='btn btn-success btn-xs save-name'><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                `
            }
        }
        var elements = getElementsBySelectorPath(configuration.selectorPath);
        $(elements).data('embeddedNameEdit-config', configuration);
        $(elements).data('embeddedNameEdit-component', nameEditComponent);
        
        if (typeof configuration.viewModel == 'undefined' || configuration.viewModel == null) {
            $.post(configuration.actions.init, { selectorPath: configuration.selectorPath, idItem: configuration.idItem, typeItem: configuration.typeItem })
                .done(function (response) {
                    var selectorPath = response.SelectorPath;
                    var elements = getElementsBySelectorPath(selectorPath);
                    var nameEditComponent = $(elements).data('embeddedNameEdit-component');

                    nameEditComponent.viewModel = response;
                    nameEditComponent.handler.createComponent(nameEditComponent);
                })
                .fail(function (jqxhr, textStatus, error) {
                    // stuff
                });
        } else {
            var nameEditComponent = $(elements).data('embeddedNameEdit-component');
            nameEditComponent.handler.createComponent(nameEditComponent);
        }

        
    };
})(jQuery);