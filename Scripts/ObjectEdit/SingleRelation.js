﻿(function ($) {
    $.fn.createSingleRelation = function (configuration) {
        var dataSingleRelation = 'single-relation-config';
        var selectorSingleRelation = configuration.selector;
        $(this).data(dataSingleRelation, {
            elements: {
                singleRelationContainerElement: null,
                singleRelationButton: null
            },
            configuration: configuration,
            handler: {
                validateReference: function (idObject, validateReferenceCallback) {
                    var oItem = {
                        GUID: idObject
                    };
                    var idRelationType = $(selectorSingleRelation).data(dataSingleRelation).configuration.idRelationType;
                    var idDirection = $(selectorSingleRelation).data(dataSingleRelation).configuration.idDirection;
                    var idParentRelation = $(selectorSingleRelation).data(dataSingleRelation).configuration.idParentRelation;
                    var url = $(selectorSingleRelation).data(dataSingleRelation).configuration.viewModel.ActionValidateReference;
                    var idInstance = $(selectorSingleRelation).data(dataSingleRelation).configuration.viewModel.IdInstance;

                    $.post(url, { refItem: oItem, idRelationType: idRelationType, idDirection: idDirection, idParentRelation: idParentRelation, idInstance: idInstance })
                        .done(function (response) {
                            console.log("connected", response);

                            changeValuesFromServerChildren(response, [selectorSingleRelation]);
                            var classItem = getViewItemsValue(response, "relationButton", "AddClass"); 
                            $($(selectorSingleRelation).data(dataSingleRelation).elements.singleRelationButton).html('<i class="' + classItem + '" aria-hidden="true"></i>');
                            if (validateReferenceCallback !== undefined && validateReferenceCallback !== null) {
                                validateReferenceCallback();
                            }
                            
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            // stuff
                            $('body').closePageLoader();
                        });
                }
            }
        });

        $(this).addClass('single-relation');
        var singleRelationElement = $(this).data(dataSingleRelation);

        singleRelationElement.elements.singleRelationContainerElement = this;
        $(singleRelationElement.elements.singleRelationContainerElement).addClass('k-content');
        $(singleRelationElement.elements.singleRelationContainerElement).html('');

        var singleRelationButton = $('<button class="k-button btn btn-sm relationButton"><i class="fa fa-chain-broken" aria-hidden="true"></i></button>');
        singleRelationElement.elements.singleRelationButton = singleRelationButton;

        $(singleRelationElement.elements.singleRelationContainerElement).append(singleRelationButton);

        changeValuesFromServerChildren($(selectorSingleRelation).data(dataSingleRelation).configuration.viewItems, [selectorSingleRelation]);

        if ($(this).data(dataSingleRelation).configuration.createdCallback !== undefined && $(this).data(dataSingleRelation).configuration.createdCallback !== null) {
            $(this).data(dataSingleRelation).configuration.createdCallback($(this).data(dataSingleRelation));
        }

        $(singleRelationElement.elements.singleRelationContainerElement).find('.k-button').kendoButton({
            click: function () {
                
            }
        });

        $.post(singleRelationElement.configuration.actionSingleRelationInit)
            .done(function (response) {
                singleRelationElement.configuration.viewModel = response;

                $.post(singleRelationElement.configuration.viewModel.ActionInitializeViewItems, { idInstance: response.Result })
                    .done(function (response) {
                        changeValuesFromServerChildren(response.ViewItems, [selectorSingleRelation]);


                    })
                    .fail(function (jqxhr, textStatus, error) {
                        // stuff
                    });

                
            })
            .fail(function (jqxhr, textStatus, error) {
                // stuff
            });
    }
})(jQuery);