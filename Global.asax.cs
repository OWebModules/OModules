﻿using OModules.Controllers;
using OModules.Hubs;
using OModules.Notifications;
using OModules.Services;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace OModules
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected string sessionDataPath;

        protected void Application_Start()
        {
            var logger = new Logger();
            logger.Info("Startup application.");

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            

            sessionDataPath = Path.Combine(Server.MapPath("~/Resources/UserGroupRessources"), "Sessions");
            if (Directory.Exists(sessionDataPath))
            {
                foreach (var item in Directory.GetDirectories(sessionDataPath))
                {
                    Directory.Delete(item, true);
                }
            }
            
        }

        protected void Session_Start(Object sender, EventArgs e)
        {
            Session["init"] = 0;
            SessionController.Sessions.Add(Session.SessionID);
        }

        protected void Session_End(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(sessionDataPath)) return;
            var cacheItemsToRemove = MemoryCache.Default.Where(cacheItem => cacheItem.Key.StartsWith(Session.SessionID)).ToList();
            cacheItemsToRemove.ForEach(cacheItem =>
            {
                MemoryCache.Default.Remove(cacheItem.Key);
            });
            var filePath = Path.Combine(sessionDataPath, $"_{Session.SessionID}");
            if (Directory.Exists(filePath))
            {
                foreach (var item in Directory.GetFiles(filePath))
                {
                    File.Delete(item);
                }
                Directory.Delete(filePath);
            }
            
            SessionController.Sessions.Remove(Session.SessionID);
        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            
            var raisedException = Server.GetLastError();
            var logger = new Logger();

            logger.Error(raisedException.Message);
        }
    }
}
