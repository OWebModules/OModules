﻿using OModules.Primitives;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Connectors;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using OModules.Controllers;
using OntoMsg_Module;

namespace OModules
{
    public partial class Startup
    {
        public void LoadModules()
        {
            var path = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
            var configPath = System.IO.Path.Combine(path, @"Config\Config_ont.xml");
            var viewConfigConnector = new ViewConfigConnector(configPath);

            var moduleList = WebViews.Config.LocalData.GetObjectList.Where(obj => obj.GUID_Parent == WebViews.Config.LocalData.Class_Module.GUID);
            var functionList = WebViews.Config.LocalData.GetObjectList.Where(obj => obj.GUID_Parent == WebViews.Config.LocalData.Class_Module_Function.GUID);
            var controllerList = WebViews.Config.LocalData.GetObjectList.Where(obj => obj.GUID_Parent == WebViews.Config.LocalData.Class_OWebModulesController.GUID);
            var viewList = WebViews.Config.LocalData.GetObjectList.Where(obj => obj.GUID_Parent == WebViews.Config.LocalData.Class_WebsocketView.GUID);

            var viewItems = (from view in viewList
                              join controllerToViewRel in WebViews.Config.LocalData.GetObjectRelList on view.GUID equals controllerToViewRel.ID_Other
                              join controller in controllerList on controllerToViewRel.ID_Object equals controller.GUID
                              join controllerToModule in WebViews.Config.LocalData.GetObjectRelList on controller.GUID equals controllerToModule.ID_Object
                              join module in moduleList on controllerToModule.ID_Other equals module.GUID
                              select new ViewMetaItem
                              {
                                  IdView = view.GUID,
                                  NameView = view.Name,
                                  IdController = controller.GUID,
                                  NameController = controller.Name,
                                  IdModule = module.GUID,
                                  NameModule = module.Name
                              }).ToList();

            viewItems.ForEach(viewItem =>
            {
                viewItem.ModuleFunctions = (from moduleToFunction in WebViews.Config.LocalData.GetObjectRelList.Where(mod => mod.ID_Object == viewItem.IdModule)
                                            join functionItem in functionList on moduleToFunction.ID_Other equals functionItem.GUID
                                            select functionItem).ToList();
            });

            var viewConfig = new CreateViewListResult
            {
                ViewList = viewItems,
                ModuleFunctions = functionList.ToList()
            };

            HttpRuntime.Cache.Insert(CacheKeys.ViewConfig, viewConfig);
        }

    }
}